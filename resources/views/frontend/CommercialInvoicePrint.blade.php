<html>

<head>
    <title></title>
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('icomoon/style.css') }}
    {{ HTML::style('css/style.css') }}
    <style>
        @page {
            margin: 0;
        }

        body {}

        * {
            font-size: 10px;
            color: #000;
            font-weight: 600;
        }

        .w150 {
            width: 150px;
        }

        .w250 {
            width: 250px;
        }

        .grey {
            color: #555;

        }

        .dt_grey_bg {
            background: #ff0;
            color: #f00;
            display: inline-block;
            padding: 2px;
        }

        .print_page {
            width: 270mm;
            height: 210mm;
            padding: 15px;
            box-sizing: border-box;
            /* border: 1px solid #000; */
            margin: auto;
            color: #000;
        }

        .print_table {
            width: 100%;
        }

        .print_table th,
        .print_table td {
            border: 1px solid #000;
            padding: 5px;
        }

        .logo {
            height: 80px;
        }

        .invoice_block {
            border: .5mm solid #000;
            border-radius: 5px;
            padding: 5px;
            overflow: hidden;
            color: #f00;
            background: #81ecec;
        }

        .invoice_block span {
            display: block;
            float: left;
            background: #ccc;
            margin: -5px 0 -5px -5px;
            padding: 10px;
            color: #fff;
        }

        .product_row * {
            font-size: 8px;
            font-weight: bold;
        }

        .product_row.product_heading * {
            text-align: center;
        }

        .product_row th,
        .product_row td {
            padding: 2px;
        }

        .footer_row td {
            border-right: 0;
            border-left: 0;
            border-bottom: 0;
        }

        @media print {
            @page {
                size: landscape
            }

            .print_page {
                margin: 0;
                width: 100%;
            }
        }
    </style>
</head>

<body style="background: #FFFFFF">
    <div class="print_page">
        <table class="print_table">
            <tr>
                <td colspan="22">
                    <div class="clearfix">
                        <div style="text-align: center">
                            <div class="title_vendor grey">
                                <h3>{{ $GetVendorData->vendor_org_name }}</h3>
                            </div>
                            {{ $GetVendorData->vendor_address }}<br>
                            IEC NR.

                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="22" style="padding: 0px">
                    <div class="clearfix">
                        <div style="text-align: center; font-size: 30px">
                            COMMERCIAL INVOICE CUM PACKING LIST
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="text-center" colspan="3">Purchase Order Number</td>
                <td class="text-center" colspan="3">Purchase Order Date</td>
                <td class="text-center" colspan="5">Order Reference No.</td>
                <td class="text-center" colspan="4">GHP Vendor Code</td>
                <td class="text-center" colspan="4">Invoice Number</td>
                <td class="text-center" colspan="3">Invoice Date</td>
            </tr>
            <tr>
                <td class="text-center" colspan="3">
                    <strong>
                        @php
                            
                            $PoInvoice = App\Models\PoInvoice::where('po_invoice_id', $id)->first();
                            $VendorData = App\Models\Vendor::where('vendor_uid', $PoInvoice->po_invoice_vendor_id)->first();
                            $PoNo = '';
                            $PoDate = '';
                            $PiNo = '';
                            $PoRefNo = '';
                            foreach (explode(',', $PoInvoice->po_invoice_po) as $d) {
                                $GetPoData = App\Models\POModel::find($d);
                                $PoNo .= '2021 - ' . $d . ',';
                                $PoDate .= date('d-M-Y', strtotime($GetPoData->po_date)) . ',';
                                $PiNo = $GetPoData->po_pi_id + 100;
                                $PoRefNo = $GetPoData->pi_order_ref;
                            }
                        @endphp
                        {{ $PoNo }}
                    </strong>
                </td>
                <td class="text-center" colspan="3"><b>{{ $PoDate }}</b></td>
                <td class="text-center" colspan="5" style="background: #fd79a8;"><strong>
                        GHP-202122-{{ $PiNo }}
                        @if ($PoRefNo != '')
                            <br> {{ $PoRefNo }}
                        @endif
                    </strong></td>
                <td class="text-center" colspan="4"><b>{{ $VendorData->vendor_code }}</b></td>
                <td class="text-center" colspan="4"><strong>{{ $GetInvoiceData->po_invoice_invoice_no }}</strong>
                </td>
                <td class="text-center" colspan="3">
                    <strong>{{ date('d-m-Y', strtotime($GetInvoiceData->po_invoice_invoice_date)) }}</strong></td>
            </tr>
            <tr>
                <td class="text-center" colspan="5">Consignee Name and Address</td>
                <td class="text-center" colspan="5">Notifiy Party Name and Address</td>
                <td class="text-center" colspan="5">Buyer Name and Address</td>
                <td class="text-center" colspan="7">FSC Status</td>
            </tr>
            <tr>
                <td class="text-center" colspan="5"><strong>TO BE ADVISED</strong> </td>
                <td class="text-center" colspan="5"><strong>TO BE ADVISED</strong> </td>
                <td class="text-center" colspan="5"><strong>Global Home Products Pte. Ltd. <br>20 CECIL STREET,#05 -
                        03 PLUS,SINGAPORE(049705)</strong></td>
                <td class="text-center" colspan="7">
                    <b> FSC Licence Number : </b>{{ $GetInvoiceData->po_invoice_fsc_license }}<br>
                    <b> FSC Certificate Code : </b> {{ $GetInvoiceData->po_invoice_fsc_certificate_code }}</b>
                </td>
            </tr>
            <tr>
                <td class="text-center" colspan="3">Container Number</td>
                <td class="text-center" colspan="2">Size</td>
                <td class="text-center" colspan="2">Line Seal</td>
                <td class="text-center" colspan="3">Custom Seal Number</td>
                <td class="text-center" colspan="5">Vessel / Voyage</td>
                <td class="text-center" colspan="4">ETD</td>
                <td class="text-center" colspan="3">ETA</td>
            </tr>
            <tr>
                <td class="text-center" colspan="3"><strong>{{ $GetInvoiceData->po_invoice_container }}</strong>
                </td>
                <td class="text-center" colspan="2"><b>{{ $GetInvoiceData->po_invoice_container_size }}</b></td>
                <td class="text-center" colspan="2"><strong>{{ $GetInvoiceData->po_invoice_line_seel }}</strong>
                </td>
                <td class="text-center" colspan="3"><b>{{ $GetInvoiceData->po_invoice_custom_seal }}</b></td>
                <td class="text-center" colspan="5"><strong>{{ $GetInvoiceData->po_invoice_voyage }}</strong></td>
                <td class="text-center" colspan="4">
                    <strong>{{ date('d-m-Y', strtotime($GetInvoiceData->po_invoice_etd)) }}</strong></td>
                <td class="text-center" colspan="3">
                    <strong>{{ date('d-m-Y', strtotime($GetInvoiceData->po_invoice_eta)) }}</strong></td>
            </tr>
            <tr>
                <td class="text-center" colspan="3">Port of loading</td>
                <td class="text-center" colspan="2">Port of Discharge</td>
                <td class="text-center" colspan="2">Final Destination</td>
                <td class="text-center" colspan="4">Country of the Final Distination</td>
                <td class="text-center" colspan="2">Country of Origin of Goods</td>
                <td class="text-center" colspan="3">Terms of Delivery</td>
                <td class="text-center" colspan="6">Terms of Payment</td>
            </tr>
            <tr>
                <td class="text-center" colspan="3"><strong></strong> </td>
                <td class="text-center" colspan="2"><b></b></td>
                <td class="text-center" colspan="2"><strong></strong></td>
                <td class="text-center" colspan="4"><b></b></td>
                <td class="text-center" colspan="2"><strong></strong></td>
                <td class="text-center" colspan="3"><strong>{{ $PODATA->vquote_price_term }}</strong></td>
                <td class="text-center" colspan="6"><strong>{{ $PODATA->vquote_payment }}% Advance And
                        {{ 100 - $PODATA->vquote_payment }}% T.T. AGAINST MAIL COPY OF DOCS</strong></td>
            </tr>
            <tr class="product_row product_heading">
                <th style="background: #ccc;">S.No.</th>
                <th style="background: #ccc;">Case No.</th>
                <th style="background: #ccc;">Product Code</th>
                <th style="background: #ccc;">SKU</th>
                <th style="background: #ccc;">Barcode</th>
                <th style="background: #ccc;">Picture</th>
                <th style="width: 80px; background: #ccc;">Description</th>
                <th style="background: #ccc;"colspan="3">PRODUCT SIZE IN<br>CM (LxWxH)</th>
                <th style="background: #ecc5b9;">Qty <br>in<br>PC's</th>
                <th style="background: #ccc;">Unit<br>Price<br>(USD)</th>
                <th style="background: #ccc;">Total<br>Amount<br>(USD)</th>
                <th style="background: #ccc;">Ttl <br>Pkt</th>
                <th style="background: #ccc;">N.W.<br>Wd.</th>
                <th style="background: #ccc;">N.W.<br>Irn.</th>
                <th style="background: #ccc;">T. N.W.<br>Kgs</th>
                <th style="background: #ccc;">GW<br> PKt<br>Kgs</th>
                <th style="background: #ccc;">Ttl GW<br>Kgs</th>
                <th style="background: #ccc;">MEAS<br>(M3)</th>
                <th style="background: #ccc;">Ttl<br>MEAS<br>(M3)</th>
                <th style="background: #ccc;">HS<br>Code</th>
                <!-- <th style="background: #ff0;">NW/Pc.</th>
                   <th style="background: #ff0;">GW/Pkt.</th> -->
            </tr>
            @php
                $TotalQty = 0;
                $TotalCartoon = 0;
                $TotalCBM = 0;
                $TotalNetWeight = 0;
                $TotalGrossWeight = 0;
                $TotalAmount = 0;
                $TotalAdvance = 0;
            @endphp
            @foreach ($InvoiceData as $InvoicePcs)
                @php
                    
                    $TotalQty += $InvoicePcs->pip_qty;
                    $TotalCartoon += $InvoicePcs->pip_TotalPackets;
                    $TotalCBM += $InvoicePcs->pip_TotalMeas;
                    $TotalNetWeight += $InvoicePcs->pip_TotalNetWeightKG;
                    $TotalGrossWeight += $InvoicePcs->pip_TotalGrossWeight;
                    $TotalAmount += $InvoicePcs->pip_qty * $InvoicePcs->pip_price;
                    
                    $GetProductDtail = \App\Models\POProduct::where('popro_po_id', $GetInvoiceData->po_invoice_po)
                        ->where('popro_pid', $InvoicePcs->exs_po_invoice_products_product_id)
                        ->first();
                    $GetProduct = \App\Models\Product::find($InvoicePcs->exs_po_invoice_products_product_id);
                    
                @endphp
                <tr class="product_row product_row product_heading">
                    <td>1</td>
                    <td style="color: #f00;">{{ $InvoicePcs->pip_case_no }}</td>
                    <td>{{ $GetProduct->product_code }}</td>
                    <td>{{ @$GetProductDtail->popro_sku }}</td>
                    <td>{{ @$GetProductDtail->popro_barcode }}</td>
                    <td><img src="{{ url('imgs/products/' . $GetProduct->product_image) }}" alt=""
                            style="width: 70px; height: 50px; object-fit: contain;"></td>
                    <td>
                        {{ $GetProduct->product_name }}<br>
                        {{ $GetProduct->product_type }} Version<br>
                        {{ $GetProduct->product_mp }}xCarton<br>
                        {{ $GetProduct->product_material_wood }}
                        <u><b>Packing Dimension</b></u><br>
                        @php
                            $PkgDArr = json_decode($InvoicePcs->pip_PkgDimL);
                            $PkgDWArr = json_decode($InvoicePcs->pip_PkgDimW);
                            $PkgDHArr = json_decode($InvoicePcs->pip_PkgDimH);
                        @endphp
                        {{ implode(',', $PkgDArr) }} L x {{ implode(',', $PkgDWArr) }} W x
                        {{ implode(',', $PkgDHArr) }} H
                    </td>
                    <td>{{ $GetProduct->product_l_cm }}</td>
                    <td>{{ $GetProduct->product_w_cm }}</td>
                    <td>{{ $GetProduct->product_h_cm }}</td>
                    <td style="background: #ecc5b9;">{{ $InvoicePcs->pip_qty }}</td>
                    <td>{{ $InvoicePcs->pip_price }}</td>
                    <td>{{ number_format($InvoicePcs->pip_subtotal, 2) }}</td>
                    <td>{{ $InvoicePcs->pip_TotalPackets }}</td>
                    <td>{{ $InvoicePcs->pip_NWwWd }}</td>
                    <td>{{ $InvoicePcs->pip_NWIrn }}</td>
                    <td>{{ number_format($InvoicePcs->pip_TotalNetWeightKG, 2) }}</td>
                    <td>{{ $InvoicePcs->pip_GWPktKgs }}</td>
                    <td>{{ $InvoicePcs->pip_TotalGrossWeight }}</td>
                    @php
                        $PipMass = json_decode($InvoicePcs->pip_Meas);
                    @endphp

                    <td>{!! implode('<br>', $PipMass) !!}</td>
                    <td>{{ $InvoicePcs->pip_TotalMeas }}</td>
                    <td>{{ $InvoicePcs->pip_HSCode }}</td>
                    <!-- <td style="background: #ff0;">31.30</td>
                   <td style="background: #ff0;">35.20</td> -->
                </tr>
            @endforeach
            <tr>
                <td colspan="22" style="background: #ecc5b9;"></td>
            </tr>
            <tr>
                <td colspan="2" style="font-size: 8px;">TOTAL QTY </td>
                <td colspan="2" style="font-size: 8px;">TOTAL CARTON</td>
                <td colspan="2" style="font-size: 8px;">TOTAL CBM</td>
                <td colspan="2" style="font-size: 8px;">TOTAL NET WEIGHT</td>
                <td colspan="3" style="font-size: 8px;">TOTAL GROSS WEIGHT</td>
                <td colspan="2" style="font-size: 8px;">NET INVOICE VALUE</td>
                <td colspan="3" style="font-size: 8px;">LESS : ADVANCE PAID</td>
                <td colspan="3" style="font-size: 8px;">LESS : DISCOUNT</td>
                <td colspan="3" style="font-size: 8px;">BALANCE DUE AMOUNT</td>
            </tr>
            @php
                $TotalAdvance = ($PODATA->vquote_payment * $TotalAmount) / 100;
                $TotalDue = $TotalAmount - $TotalAdvance;
            @endphp
            <tr>
                <td colspan="2" style="font-size: 8px;">{{ $TotalQty }} Pcs. / Set</td>
                <td colspan="2" style="font-size: 8px;">{{ $TotalCartoon }}</td>
                <td colspan="2" style="font-size: 8px;">{{ number_format($TotalCBM, 2) }}</td>
                <td colspan="2" style="font-size: 8px;">{{ $TotalNetWeight }} Kgs.</td>
                <td colspan="3" style="font-size: 8px;">{{ $TotalGrossWeight }} Kgs.</td>
                <td colspan="2" style="font-size: 8px;">$ {{ number_format($TotalAmount, 2) }}</td>
                <td colspan="3" style="font-size: 8px;">$ {{ number_format($TotalAdvance, 2) }}</td>
                <td colspan="3" style="font-size: 8px;">$ 0</td>
                <td colspan="3" style="font-size: 8px;">$ {{ number_format($TotalDue, 2) }}</td>
            </tr>

            </tr>
            <tr class="" style="border: 1px solid #000;">
                <td colspan="7" valign="top" style="font-size: 8px;">

                    Declaration <strong><u>"STATMENT ON ORIGIN"</u></strong><br><br><br>
                    <div style="text-align: left;">
                        The exporter “<b>{{ $GetVendorData->vendor_org_name }}</b> ,
                        <b>{{ $GetInvoiceData->po_invoice_statement_origin }}” </b>of the products covered by this
                        document declares that, except where otherwise clearly indicated, these products are of India
                        preferential origin according to rules of origin of the Generalized System
                        <br>
                    </div>
                </td>
                <td colspan="7" style="font-size: 8px;" valign="top"><br><br><br>
                    <strong>"ALL PRODUCT ARE MADE OF"</strong> wood species name "<b>FSC</b> status"<br>
                </td>
                <td colspan="8" style="font-size: 8px;" valign="top">
                    <strong>FOR <u>{{ $GetVendorData->vendor_org_name }}</u></strong><br>
                    <br><br>
                    <br><br>

                    AUTHORISED SIGNATORY
                </td>
            </tr>
        </table>
    </div>
    {{ HTML::script('js/jquery.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/owl.carousel.min.js') }}
    {{ HTML::script('js/popper.min.js') }}
    {{ HTML::script('js/custom.js') }}
</body>

</html>
