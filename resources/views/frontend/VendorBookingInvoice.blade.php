<html>

<head>
    <title>BOOKING INVOICE</title>
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('icomoon/style.css') }}
    {{ HTML::style('css/style.css') }}
    <style>
        @page {
            margin: 0;
        }

        body {}

        * {
            font-size: 10px;
            color: #000;
            font-weight: 600;
        }

        .w150 {
            width: 150px;
        }

        .w250 {
            width: 250px;
        }

        .grey {
            color: #555;
        }

        .dt_grey_bg {
            background: #ff0;
            color: #f00;
            display: inline-block;
            padding: 2px;
        }

        .print_page {
            width: 270mm;
            height: 210mm;
            padding: 15px;
            box-sizing: border-box;
            /* border: 1px solid #000; */
            margin: auto;
            color: #000;
        }

        .print_table {
            width: 100%;
        }

        .print_table th,
        .print_table td {
            border: 1px solid #000;
            padding: 5px;
        }

        .logo {
            height: 80px;
        }

        .invoice_block {
            border: .5mm solid #000;
            border-radius: 5px;
            padding: 5px;
            overflow: hidden;
            color: #f00;
            background: #81ecec;
        }

        .invoice_block span {
            display: block;
            float: left;
            background: #ccc;
            margin: -5px 0 -5px -5px;
            padding: 10px;
            color: #fff;
        }

        .product_row * {
            font-size: 8px;
            font-weight: bold;
        }

        .product_row.product_heading * {
            text-align: center;
        }

        .product_row th,
        .product_row td {
            padding: 2px;
        }

        .footer_row td {
            border-right: 0;
            border-left: 0;
            border-bottom: 0;
        }

        @media print {
            @page {
                size: landscape
            }

            .print_page {
                margin: 0;
                width: 100%;
            }
        }
    </style>
</head>

<body style="background: #FFFFFF">
    <div class="print_page">
        <table class="print_table">
            <tr>
                <td colspan="22">
                    <div class="clearfix">
                        <div style="text-align: center">
                            <div class="title_vendor grey">{{ $GetVendorData->vendor_org_name }}</div>
                            <strong>{{ $GetVendorData->vendor_address }}</strong><br>
                            IEC NR.

                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="22" style="padding: 0px">
                    <div class="clearfix">
                        <div style="text-align: center; font-size: 30px">
                            BOOKING INVOICE
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="text-center" colspan="3">Purchase Order Number</td>
                <td class="text-center" colspan="3">Purchase Order Date</td>
                <td class="text-center" colspan="5">Order Reference Nr.</td>
                <td class="text-center" colspan="4">GHP Vendor Code</td>
                <td class="text-center" colspan="4">Invoice Number</td>
                <td class="text-center" colspan="3">Invoice Date</td>
            </tr>
            <tr>
                <td class="text-center" colspan="3">
                    <strong>
                        <!--1508{{ str_pad($PODATA->po_id, 2, '0', STR_PAD_LEFT) }}-->


                        @php
                            
                            $PoInvoice = App\Models\PoBookingInvoice::where('po_invoice_id', $id)->first();
                            
                            $PoNo = '';
                            foreach (explode(',', $PoInvoice->po_invoice_po) as $d) {
                                $PoNo .= '2021 - ' . $d . ',';
                            }
                        @endphp
                        {{ $PoNo }}
                    </strong>
                </td>
                <td class="text-center" colspan="3"><b>{{ date('d-M-Y', strtotime($PODATA->po_date)) }}</b></td>
                <td class="text-center" colspan="5" style="background: #fd79a8;"><strong>
                        GHP-201819-{{ sprintf('%03d', $PODATA->po_pi_id) }}
                        @if ($PODATA->pi_order_ref != '')
                            <br> {{ $PODATA->pi_order_ref }}
                        @endif
                    </strong></td>
                <td class="text-center" colspan="4"><b>{{ $PODATA->vendor_code }}</b></td>
                <td class="text-center" colspan="4"><strong>{{ $GetInvoiceData->po_invoice_invoice_no }}</strong>
                </td>
                <td class="text-center" colspan="3">
                    <strong>{{ date('d-m-Y', strtotime($GetInvoiceData->po_invoice_invoice_date)) }}</strong>
                </td>
            </tr>
            <tr>
                <td class="text-center" colspan="5">Consignee Name and Address</td>
                <td class="text-center" colspan="5">Notifiy Party Name and Address</td>
                <td class="text-center" colspan="5">Buyer Name and Address</td>
                <td class="text-center" colspan="7">FSC Status</td>
            </tr>
            <tr>
                <td class="text-center" colspan="5">
                    <strong>{{ $PIDATA->user_name }}</strong><br>
                    {{ nl2br($PIDATA->user_address) }}<br>
                    {{ $PIDATA->state_name }}, {{ $PIDATA->country_name }}
                </td>
                <td class="text-center" colspan="5">
                    <strong>{{ $PIDATA->buyer_np_consignee_name }}</strong><br>
                    {{ nl2br($PIDATA->buyer_np_address) }}
                </td>

                <td class="text-center" colspan="5"><strong>Global Home Products Pte. Ltd.1,20 CECIL STREET,#05 - 03
                        PLUS,<br>SINGAPORE (049705)</strong></td>
                <td class="text-center" colspan="7">
                    FSC Licence Number : <b>{{ $GetInvoiceData->po_invoice_fsc_license }}</b><br>
                    FSC Certificate Code : <b>{{ $GetInvoiceData->po_invoice_fsc_certificate_code }}</b>
                </td>
            </tr>
            <tr>
                <td class="text-center" colspan="3">Container Number</td>
                <td class="text-center" colspan="2">Size</td>
                <td class="text-center" colspan="2">Line Seal</td>
                <td class="text-center" colspan="3">Custom Seal Number</td>
                <td class="text-center" colspan="5">Vessel / Voyage</td>
                <td class="text-center" colspan="4">ETD</td>
                <td class="text-center" colspan="3">ETA</td>
            </tr>
            <tr>
                <td class="text-center" colspan="3"><strong>{{ $GetInvoiceData->po_invoice_container }}</strong>
                </td>
                <td class="text-center" colspan="2"><b>{{ $GetInvoiceData->po_invoice_container_size }}</b></td>
                <td class="text-center" colspan="2"><strong>{{ $GetInvoiceData->po_invoice_line_seel }}</strong>
                </td>
                <td class="text-center" colspan="3"><b>{{ $GetInvoiceData->po_invoice_custom_seal }}</b></td>
                <td class="text-center" colspan="5"><strong>{{ $GetInvoiceData->po_invoice_voyage }}</strong></td>
                <td class="text-center" colspan="4">
                    <strong>{{ date('d-m-Y', strtotime($GetInvoiceData->po_invoice_etd)) }}</strong>
                </td>
                <td class="text-center" colspan="3">
                    <strong>{{ date('d-m-Y', strtotime($GetInvoiceData->po_invoice_eta)) }}</strong>
                </td>
            </tr>
            <tr>
                <td class="text-center" colspan="3">Port of loading</td>
                <td class="text-center" colspan="2">Port of Discharge</td>
                <td class="text-center" colspan="2">Final Destination</td>
                <td class="text-center" colspan="4">Country of the Final Distination</td>
                <td class="text-center" colspan="2">Country of Origin of Goods</td>
                <td class="text-center" colspan="3">Terms of Delivery</td>
                <td class="text-center" colspan="6">Terms of Payment</td>
            </tr>
            @php
                $GetDataPortCountry = DB::table('ports')
                    ->where('port_name', $PIDATA->quote_loading_port)
                    ->first();
                $GetLiveCountry = DB::table('countries')
                    ->where('country_id', $GetDataPortCountry->port_country)
                    ->first();
            @endphp
            <tr>
                <td class="text-center" colspan="3"><strong>{{ $PIDATA->quote_loading_port }},
                        {{ $GetLiveCountry->country_short_name }}</strong> </td>

                @php
                    $GetDataPortCountry = DB::table('ports')
                        ->where('port_name', $PIDATA->quote_discharge_port)
                        ->first();
                    $GetLiveCountry = DB::table('countries')
                        ->where('country_id', $GetDataPortCountry->port_country)
                        ->first();
                @endphp

                <td class="text-center" colspan="2"><b>{{ $PIDATA->quote_discharge_port }},
                        {{ $GetLiveCountry->country_short_name }}</b></td>
                <td class="text-center" colspan="2"><strong>{{ $PODATA->vquote_delivery_port }}</strong></td>
                <td class="text-center" colspan="4"><b>{{ $PIDATA->quote_first_destination_country }}</b></td>
                <td class="text-center" colspan="2"><strong>{{ $PIDATA->quote_origin_country }}</strong></td>

                @php
                    $GetDataPortCountry = DB::table('ports')
                        ->where('port_name', $PIDATA->quote_loading_port)
                        ->first();
                    $GetLiveCountry = DB::table('countries')
                        ->where('country_id', $GetDataPortCountry->port_country)
                        ->first();
                @endphp

                <td class="text-center" colspan="3"><strong>{{ $PIDATA->quote_price_term }} -
                        {{ $PIDATA->quote_loading_port }}, {{ $GetLiveCountry->country_short_name }}</strong></td>
                <td class="text-center" colspan="6"><strong>{{ $PODATA->vquote_payment }}% Advance And
                        {{ 100 - $PODATA->vquote_payment }}% T.T. AGAINST MAIL COPY OF DOCS</strong></td>
            </tr>
            <tr class="product_row product_heading">
                <th style="background: #ccc;">S.No.</th>
                <th style="background: #ccc;">Case No.</th>
                <th style="background: #ccc;">Product Code</th>
                <th style="background: #ccc;">SKU</th>
                <th style="background: #ccc;">Barcode</th>
                <th style="background: #ccc;">Picture</th>
                <th style="width: 80px; background: #ccc;">Description</th>
                <th style="background: #ccc;"colspan="3">PRODUCT SIZE IN<br>CM (HxWxD)</th>
                <th style="background: #ecc5b9;">Qty <br>in<br>PC's</th>
                <th style="background: #ccc;">Unit<br>Price<br>(USD)</th>
                <th style="background: #ccc;">Total<br>Amount<br>(USD)</th>
                <th style="background: #ccc;">Ttl <br>Pkt</th>
                <th style="background: #ccc;">N.W.<br>Wd.</th>
                <th style="background: #ccc;">N.W.<br>Irn.</th>
                <th style="background: #ccc;">T. N.W.<br>Kgs</th>
                <th style="background: #ccc;">GW<br> PKt<br>Kgs</th>
                <th style="background: #ccc;">Ttl GW<br>Kgs</th>
                <th style="background: #ccc;">MEAS<br>(M3)</th>
                <th style="background: #ccc;">Ttl<br>MEAS<br>(M3)</th>
                <th style="background: #ccc;">HS<br>Code</th>
                <!-- <th style="background: #ff0;">NW/Pc.</th>
                   <th style="background: #ff0;">GW/Pkt.</th> -->
            </tr>
            @php
                $TotalQty = 0;
                $TotalCartoon = 0;
                $TotalCBM = 0;
                $TotalNetWeight = 0;
                $TotalGrossWeight = 0;
                $TotalAmount = 0;
                $TotalAdvance = 0;
            @endphp
            @foreach ($InvoiceData as $InvoicePcs)
                @php
                    $TotalQty += $InvoicePcs->pip_qty;
                    $TotalCartoon += $InvoicePcs->pip_TotalPackets;
                    $TotalCBM += $InvoicePcs->pip_TotalMeas;
                    $TotalNetWeight += $InvoicePcs->pip_TotalNetWeightKG;
                    $TotalGrossWeight += $InvoicePcs->pip_TotalGrossWeight;
                    $TotalAmount += $InvoicePcs->pip_qty * $InvoicePcs->pip_price;
                    
                    $GetProductDtail = \App\Models\POProduct::where('popro_po_id', $GetInvoiceData->po_invoice_po)
                        ->where('popro_pid', $InvoicePcs->exs_po_invoice_products_product_id)
                        ->first();
                    $GetProduct = \App\Models\Product::find($InvoicePcs->exs_po_invoice_products_product_id);
                @endphp
                <tr class="product_row product_row product_heading">
                    <td>1</td>
                    <td style="color: #f00;">{{ $InvoicePcs->pip_case_no }}</td>
                    <td>{{ $GetProduct->product_code }}</td>
                    <td>{{ @$GetProductDtail->popro_sku }}</td>
                    <td>{{ @$GetProductDtail->popro_barcode }}</td>
                    <td><img src="{{ url('imgs/products/' . $GetProduct->product_image) }}" alt=""
                            style="width: 70px; height: 50px; object-fit: contain;"></td>
                    <td>
                        {{ $GetProduct->product_name }}<br>
                        {{ $GetProduct->product_type }} Version<br>
                        {{ $GetProduct->product_mp }}xCarton<br>
                        {{ $GetProduct->product_material_wood }}
                        <u>Packing Dimension</u><br>
                        @php
                            $LL = json_decode($InvoicePcs->pip_PkgDimL);
                            $LW = json_decode($InvoicePcs->pip_PkgDimW);
                            $LH = json_decode($InvoicePcs->pip_PkgDimH);
                            $Meas = json_decode($InvoicePcs->pip_PkgDimL);
                        @endphp
                        {{ implode(',', $LL) }} L x {{ implode(',', $LW) }} W x {{ implode(',', $LH) }} H
                    </td>

                    <td>{{ $GetProduct->product_l_cm }} </td>
                    <td>{{ $GetProduct->product_w_cm }}</td>
                    <td>{{ $GetProduct->product_h_cm }}</td>

                    <td style="background: #ecc5b9;">{{ $InvoicePcs->pip_qty }}</td>
                    <td>{{ $InvoicePcs->pip_price }}</td>
                    <td>{{ number_format($InvoicePcs->pip_subtotal, 2) }}</td>
                    <td>{{ $InvoicePcs->pip_TotalPackets }}</td>
                    <td>{{ number_format($InvoicePcs->pip_NWwWd, 2) }}</td>
                    <td>{{ number_format($InvoicePcs->pip_NWIrn, 2) }}</td>
                    <td>{{ number_format($InvoicePcs->pip_TotalNetWeightKG, 2) }}</td>
                    <td>{{ number_format($InvoicePcs->pip_GWPktKgs, 2) }}</td>
                    <td>{{ number_format($InvoicePcs->pip_TotalGrossWeight, 2) }}</td>
                    @php
                        $PipMass = json_decode($InvoicePcs->pip_Meas);
                    @endphp
                    <td>{!! implode('<br>', $PipMass) !!} </td>
                    <td>{{ number_format($InvoicePcs->pip_TotalMeas, 2) }}</td>
                    <td>{{ $InvoicePcs->pip_HSCode }}</td>
                    <!-- <td style="background: #ff0;">31.30</td>
                   <td style="background: #ff0;">35.20</td> -->
                </tr>
            @endforeach
            <tr>
                <td colspan="22" style="background: #ecc5b9;"></td>
            </tr>
            <tr>
                <td colspan="2" style="font-size: 8px;">TOTAL QTY </td>
                <td colspan="2" style="font-size: 8px;">TOTAL CARTON</td>
                <td colspan="2" style="font-size: 8px;">TOTAL CBM</td>
                <td colspan="2" style="font-size: 8px;">TOTAL NET WEIGHT</td>
                <td colspan="3" style="font-size: 8px;">TOTAL GROSS WEIGHT</td>
                <td colspan="2" style="font-size: 8px;">NET INVOICE VALUE</td>
                <td colspan="3" style="font-size: 8px;">LESS : ADVANCE PAID</td>
                <td colspan="3" style="font-size: 8px;">LESS : DISCOUNT</td>
                <td colspan="3" style="font-size: 8px;">BALANCE DUE AMOUNT</td>
            </tr>
            @php
                $TotalAdvance = ($PODATA->vquote_payment * $TotalAmount) / 100;
                $TotalDue = $TotalAmount - $TotalAdvance;
            @endphp
            <tr>
                <td colspan="2" style="font-size: 8px;">{{ $TotalQty }} Pcs. / Set</td>
                <td colspan="2" style="font-size: 8px;">{{ $TotalCartoon }}</td>
                <td colspan="2" style="font-size: 8px;">{{ number_format($TotalCBM, 2) }}</td>
                <td colspan="2" style="font-size: 8px;">{{ $TotalNetWeight }} Kgs.</td>
                <td colspan="3" style="font-size: 8px;">{{ $TotalGrossWeight }} Kgs.</td>
                <td colspan="2" style="font-size: 8px;">$ {{ number_format($TotalAmount, 2) }}</td>
                <td colspan="3" style="font-size: 8px;">$ {{ number_format($TotalAdvance, 2) }}</td>
                <td colspan="3" style="font-size: 8px;">$ 0</td>
                <td colspan="3" style="font-size: 8px;">$ {{ number_format($TotalDue, 2) }}</td>
            </tr>

            </tr>
            <tr class="" style="border: 1px solid #000;">
                <td colspan="7" valign="top" style="font-size: 8px;">

                    Declaration <strong><u>"STATMENT ON ORIGIN"</u></strong><br><br><br>
                    <div style="text-align: center;">
                        {{ $GetInvoiceData->po_invoice_statement_origin }} <br>
                    </div>
                </td>
                <td colspan="7" style="font-size: 8px;" valign="top"><br><br><br>
                    <strong>"ALL PRODUCT ARE MADE OF"</strong> wood species name "<b>FSC</b> status"<br>
                </td>
                <td colspan="8" style="font-size: 8px;" valign="top">
                    <strong>FOR <u>{{ $GetVendorData->vendor_org_name }}</u></strong><br>
                    <br><br>
                    <br><br>

                    AUTHORISED SIGNATORY
                </td>
            </tr>
        </table>
    </div>
    {{ HTML::script('js/jquery.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/owl.carousel.min.js') }}
    {{ HTML::script('js/popper.min.js') }}
    {{ HTML::script('js/custom.js') }}
</body>

</html>
