<section class="page-header mb-3">
    <div class="container-fluid">
        <div>
            <div>
                <h1>Quote ID #{{ sprintf('%06d', $record->quote_number) }}</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="{{ url('/') }}">Dashboard</a></li>
                    <li><a href="{{ url('quote') }}">Quotes</a></li>
                    <li class="active">Quote ID #{{ sprintf('%06d', $record->quote_number) }}</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <div class="card">
        <h3 class="card-title">Quotation Details for Quote ID #{{ sprintf('%06d', $record->quote_number) }}</h3>
        @php
            $added_by = '';
        @endphp
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>S.No.</th>
                        <th>Product Image</th>
                        <th width="400">Product Description</th>
                        <!-- <th class="text-center">Dimension [LxWxH]<br><small>(in cms)</small></th>
                        <th>CBM Per Pc.</th> -->
                        @foreach ($quotes as $q)
                            <th class="text-center">
                                Revision {{ $q->quote_version }} <br>
                                <small>({{ ucwords($q->quote_added_by) }})</small>
                                @php
                                    $added_by = $q->quote_added_by;
                                @endphp
                            </th>
                            <!-- <th>Qty. {{ $q->quote_version }}</th> -->
                            <!-- <th>Total CBM {{ $q->quote_version }}</th>
                        <th>Subtotal</th>
                        <th>Special Remark</th> -->
                        @endforeach
                        <th>Approval</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $sn = $totQty = $totPrice = $totCBM = 0;
                    @endphp
                    @foreach ($products as $p)
                        @php
                            $dir = 'imgs/products/';
                            $image = 'imgs/no-image.png';
                            if (!empty($p->product_image)) {
                                $image = $dir . $p->product_image;
                            }
                            $subtotal = $p->qpro_price * $p->qpro_qty;
                            $cbm_total = $p->product_cbm * $p->qpro_qty;
                            $totQty += $p->qpro_qty;
                            $totPrice += $subtotal;
                            $totCBM += $cbm_total;
                        @endphp
                        <tr>
                            <td>{{ ++$sn }}</td>
                            <td width="70"><img src="{{ url($image) }}" alt="{{ $p->product_name }}"
                                    style="width: 70px; height: 70px; object-fit: contain;"> </td>
                            <td>
                                <div class="nowrap">
                                    <strong>Name:</strong> {{ $p->product_name }}
                                </div>
                                <div class="nowrap">
                                    <strong>Code:</strong> {{ $p->product_code }}
                                </div>
                                <div class="nowrap">
                                    <strong>Dimension:</strong>
                                    {{ "{$p->product_l_cm} x {$p->product_w_cm} x {$p->product_h_cm}" }} cms
                                </div>
                                <div class="nowrap">
                                    <strong>CBM:</strong> {{ number_format($p->product_cbm, 6) }}
                                </div>
                            </td>
                            @php $flag = FALSE; @endphp
                            @foreach ($quotes as $q)
                                @php
                                    // $qp = $p->qpros;
                                    $qp = DB::table('quote_products')
                                        ->where('qpro_pid', $p->product_id)
                                        ->where('qpro_qid', $q->quote_id)
                                        ->first();
                                    // print_r($qp); die;
                                    if ($qp->qpro_is_approved == 'Y') {
                                        $flag = true;
                                    }
                                    $cbm_total = $p->product_cbm * $qp->qpro_qty;
                                    $subtotal = $qp->qpro_price * $qp->qpro_qty;
                                @endphp
                                <td @if ($flag) class="bg-success text-white" @endif>
                                    <div class="nowrap">
                                        <strong>Price:</strong> {{ $qp->qpro_price }}
                                    </div>
                                    <div class="nowrap">
                                        <strong>Qty:</strong> {{ $qp->qpro_qty }}
                                    </div>
                                    <div class="nowrap">
                                        <strong>CBM:</strong> {{ number_format($cbm_total, 6) }}
                                    </div>
                                    <div>
                                        <strong>Remarks:</strong><br>{{ $qp->qpro_remark }}
                                        {{ $qp->qpro_is_approved }}
                                    </div>
                                </td>
                                <!-- <td>{{ number_format($cbm_total, 6) }}</td>
                            <td>{{ $subtotal }}</td>
                            <td>{{ $qp->qpro_remark }}</td> -->
                            @endforeach
                            <th>
                                <label class="switch">
                                    <input type="checkbox" rel="approve_qpro"
                                        data-url="{{ url('ajax/approve_qpro/' . $qp->qpro_qid . '/' . $qp->qpro_pid) }}"
                                        @if ($added_by == $profile->user_role) disabled @endif
                                        @if ($qp->qpro_is_approved == 'Y') checked disabled @endif>
                                    <span class="slider"></span>
                                </label>
                            </th>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <!-- <tr>
                        <th colspan="6">TOTAL</th>
                        <th>{{ $totQty }}</th>
                        <th>{{ number_format($totCBM, 6) }}</th>
                        <th>{{ $totPrice }}</th>
                        <th colspan="2"></th>
                    </tr> -->
                </tfoot>
            </table>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <p>&nbsp;</p>
                <p>We consider following volume parameter to accept any order:</p>
                <!-- <div> 1 x 40 HC = 65 - 67 cbms </div>
                <div> 1 x 40 Std = 50 - 55 cbms </div>
                <div> 1 x 20 ft = 28 - 30 cbms </div> -->
                <table class="table table-bordered">
                    <tr>
                        <th width="50">Case I.</th>
                        <td>Upto 68 cbms</td>
                    </tr>
                    <tr>
                        <th>Case II. LCL Ship</th>
                        <td>Less than 20 cbms</td>
                    </tr>
                    <tr>
                        <th>Case III. 20 ft. std.</th>
                        <td>20 - 28 cbms</td>
                    </tr>
                    <tr>
                        <th>Case IV. 40 ft. std.</th>
                        <td>28 - 58 cbms</td>
                    </tr>
                    <tr>
                        <th>Case V. 40 HC. std.</th>
                        <td>58 - 68 cbms</td>
                    </tr>
                </table>
                <p>&nbsp;</p>
            </div>
            <div class="col-sm-2">

            </div>
            <div class="col-sm-6">
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <table class="table table-bordered">
                    <tr>
                        <th colspan="2">Desirable Terms</th>
                    </tr>
                    <tr>
                        <th>Price Term</th>
                        <td>{{ $record->quote_price_term }}</td>
                    </tr>
                    <tr>
                        <th>Port Of Delivery</th>
                        <td>{{ $record->quote_delivery_port }}</td>
                    </tr>
                    <tr>
                        <th>Currency</th>
                        <td>{{ $record->currency_short_name }} ({{ $record->currency_sign }})</td>
                    </tr>
                    <tr>
                        <th>Delivery</th>
                        <td>{{ $record->quote_delivery_days }}</td>
                    </tr>
                    <tr>
                        <th>Payment</th>
                        <td>{{ $record->quote_payment }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
