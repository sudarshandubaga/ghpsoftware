<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>{{ !empty($edit->quote_id) ? 'Revise' : 'Add' }} Quote</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="{{ url('quote') }}">Quote</a></li>
                    <li class="active">{{ !empty($edit->quote_id) ? 'Revise' : 'Add' }} Quote</li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <a href="{{ url('quote') }}" title="View Quote" class="btn btn-default" data-toggle="tooltip"> <i class="icon-eye"></i> View Quote</a>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">

    @if (\Session::has('danger'))
        <div class="alert alert-danger">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-8">
            <form data-session="quote_cart" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                	<h3 class="card-title">
                		<div class="mr-auto">Quotation Details</div>
                        <div class="ml-auto">
                            <a href="#save-data" class="text-white" title="Save" data-toggle="tooltip"> <i class="icon-save"></i> Save</a>
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i class="icon-refresh"></i> </a>
                        </div>
                	</h3>
                    @if(empty($edit->quote_id))
                    	<div class="row">
                    		<div class="col">
                    			<div class="form-group">
                                    <label>Buyer</label>
                        			<select class="form-control" name="record[quote_uid]">
                                        <option value="">Choose an option</option>
                                        @foreach($buyers as $c)
                                        <option value="{{ $c->user_id }}" @if($c->user_id == $uid) selected @endif>{{ $c->user_name.' ('.$c->user_login.')' }}</option>
                                        @endforeach
                                    </select>
                                </div>
                    		</div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Quotation Date</label>
                                    <input type="text" name="record[quote_date]" value="{{ !empty($edit->quote_date) ? $edit->quote_date : '' }}" placeholder="yyyy-mm-dd" class="form-control datepicker_no_future" required autocomplete="new-password" readonly>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Currency</label>
                                    <select class="form-control" name="record[quote_currency]">
                                        @foreach($currencies as $c)
                                        <option value="{{ $c->currency_id }}" @if($c->currency_id == @$edit->quote_currency) selected @endif>{{ $c->currency_short_name.' ('.$c->currency_sign.')' }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Price Term</label>
                                    <select class="form-control" name="record[quote_price_term]">
                                        <option value="FOB" @if(@$edit->quote_price_term == 'FOB') selected @endif>FOB</option>
                                        <option value="FOR" @if(@$edit->quote_price_term == 'FOR') selected @endif>FOR</option>
                                        <option value="CIF" @if(@$edit->quote_price_term == 'CIF') selected @endif>CIF</option>
                                    </select>
                                </div>
                            </div>
                    	</div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Port of Delivery</label>
                                    <select class="form-control" name="record[quote_delivery_port]">
                                        <option value="">Select Port</option>
                                        @foreach($ports as $p)
                                        <option value="{{ $p->port_name }}" @if($p->port_name == @$edit->quote_delivery_port) selected @endif>{{ $p->port_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Delivery Days</label>
                                    <select class="form-control" id="days_dropdown">
                                        <option value="45 Days" @if(@$edit->quote_delivery_days == '45 Days') selected @endif>45 Days</option>
                                        <option value="55 Days" @if(@$edit->quote_delivery_days == '55 Days') selected @endif>55 Days</option>
                                        <option value="65 Days" @if(@$edit->quote_delivery_days == '65 Days') selected @endif>65 Days</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <input type="text" name="record[quote_delivery_days]" value="{{ !empty($edit->quote_delivery_days) ? $edit->quote_delivery_days : '45 Days' }}" class="form-control" id="days_text">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Payment</label>
                                    <select class="form-control" name="record[quote_payment]">
                                        <option value="On Presentation of Documents" @if(@$edit->quote_payment == 'On Presentation of Documents') selected @endif>On Presentation of Documents</option>
                                        <option value="T.T. 30+70" @if(@$edit->quote_payment == 'T.T. 30+70') selected @endif>T.T. 30+70</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Quotation ID</label>
                                    <input type="text" name="record[quote_number]" value="{{ $edit->quote_number }}" readonly class="form-control">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Revised From [Revision No.]</label>
                                    <div class="form-control" readonly>
                                        {{ $edit->quote_version }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Revision Comment</label>
                                    <input type="text" name="record[quote_revision_comment]" class="form-control" placeholder="Revision Comment">
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="card">
                	<h3 class="card-title">Product / Item Details</h3>
                    <div id="cartResponse">
                        @php $total_field = 'quote_total'; $total_cbm_field = 'quote_total_cbm'; $cart_name = 'quote_cart'; @endphp
                        @include('frontend.template.cart_table')
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <h3 class="card-title">Add More Product</h3>
                <form class="cart_form" action="{{ url('ajax/add_to_cart/quote_cart') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label>Product</label>
                        <select class="form-control" name="cart[cart_pid]" required>
                            <option value="">Select Product</option>
                            @foreach($allproducts as $p)
                            <option value="{{ $p->product_id }}">{{ $p->product_name.' '.$p->product_code }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Qty</label>
                        <input type="number" name="cart[cart_qty]" value="1" min="1" class="form-control" required>
                    </div>
                    <button type="submit" name="button" class="btn btn-primary btn-block">Add To Quote</button>
                </form>
            </div>
        </div>
    </div>
</div>