<div style="margin:30px 0px 30px 0px;"></div>
<div class="container-fluid">
	<div class="row">
	     <!--1st block-->
	    <div class="col-lg-8" style="border:#000 solid 0px;">
	        <div class="row">
        	    <div class="col-lg-4">
        		    <div class="card mt-3">
        		        <h3 class="card-title6">Enquiries</h3>
        		        <div>
            				<div class="text-center dash-icon">
            					<i class="icon-web" style="color: #15a7aa;"></i>
            				</div>
            			</div>
        		        <ul class="list-group">
        				    <li class="list-group-item">
        						<a href="quote" class="list-item-link">Quotes</a><br>
        						<small>(Total  Quotes)</small> : <b>15</b>
        					</li> 
        					<li class="list-group-item">
        						<!--<a href="#" class="list-item-link"></a><br>-->
        						<!--<small>(Total Assign Products)</small> : <b>16</b>-->
        					</li>
        				</ul>
        		    </div>
        		</div>
                <div class="col-lg-4">
        		    <div class="card mt-3">
        		        <h3 class="card-title1">Product</h3>
        		        <div>
            				<div class="text-center dash-icon">
            					<i class="icon-product-hunt" style="color: #fea11e;"></i>
            				</div>
            			</div>
        		        <ul class="list-group">
        				  	<li class="list-group-item">
        						<a href="buyer-product/{{ $profile->user_id }}" class="list-item-link">Total Product</a><br>
        							<small>(Total Assign Products)</small> : <b>11</b>
        					</li>
        					<li class="list-group-item">
        						<!--<a href="#" class="list-item-link"></a><br>-->
        						<!--<small>(Total Assign Products)</small> : <b>16</b>-->
        					</li>
        				</ul>
        		    </div>
        		</div>
        		<div class="col-lg-4">
        		    <div class="card mt-3">
        		        <h3 class="card-title2">Total PI</h3>
        		        <div>
            				<div class="text-center dash-icon">
            					<i class="icon-file-text" style="color: #6acf6f;"></i>
            				</div>
            			</div>
        		        <ul class="list-group">
        				  	<li class="list-group-item">
        						<a href="performa-invoice" class="list-item-link">PI#</a><br>
        						<small>(Total Assign Products)</small> : <b>15</b>
        					</li> 
        						<li class="list-group-item">
        						<!--<a href="#" class="list-item-link"></a><br>-->
        						<!--<small>(Total Assign Products)</small> : <b>16</b>-->
        					</li>
        				</ul>
        		    </div>
        		</div>
        	</div>
	        <div class="row">
    	    	<div class="col-lg-4">
    		    <div class="card mt-3">
    		        <h3 class="card-title3">Logistics</h3>
    		        <div>
        				<div class="text-center dash-icon">
        					<i class="icon-file-text" style="color: #e47164;"></i>
        				</div>
        			</div>
    		        <ul class="list-group">
    					<li class="list-group-item">
    						<a href="view-logistic" class="list-item-link">logistics</a><br>
    						<small>(Total Details )</small> : 10<b></b>
    					</li> 
    						<li class="list-group-item">
    						<!--<a href="#" class="list-item-link"></a><br>-->
    						<!--<small>(Total Assign Products)</small> : <b>16</b>-->
    					</li>
    				</ul>
    		    </div>
    		    </div>
    		    <div class="col-lg-4">
    		    <div class="card mt-3">
    		        <h3 class="card-title8">Sample Sheet</h3>
    		        <div>
        				<div class="text-center dash-icon">
        					<i class="icon-list" style="color: #e1e250de;"></i>
        				</div>
        			</div>
    		        <ul class="list-group">
    					<li class="list-group-item">
    						<a href="Samplesheet" class="list-item-link">Sample Sheet</a><br>
    						<small>(Total Details )</small> : x<b></b>
    					</li> 
    						<li class="list-group-item">
    						<!--<a href="#" class="list-item-link"></a><br>-->
    						<!--<small>(Total Assign Products)</small> : <b>16</b>-->
    					</li>
    				</ul>
    		    </div>
        		</div>
        		 <div class="col-lg-4">
        		    <div class="card mt-3">
        		        <h3 class="card-title5">GHP Invoice</h3>
        		        <div>
            				<div class="text-center dash-icon">
            					<i class="icon-file-text" style="color: #fdbeb6;"></i>
            				</div>
            			</div>
        		        <ul class="list-group">
        					<li class="list-group-item">
        						<a href="ghp-invoice-list" class="list-item-link">GHP Invoice</a><br>
        						<small>(Total Details )</small> : 10<b></b>
        					</li> 
        						<li class="list-group-item">
        						<!--<a href="#" class="list-item-link"></a><br>-->
        						<!--<small>(Total Assign Products)</small> : <b>16</b>-->
        					</li>
        				</ul>
        		    </div>
        		</div>
        	</div>
	    </div>
	    <!--2nd block-->
	    <div class="col-lg-4" style="border:#ccc solid 0px;">
	           <div class="card mt-3">
		        <h3 class="card-title5">Reports</h3>
		        <div>
    				<div class="text-center dash-icon">
    					<i class="icon-pencil" style="color: #fdbeb6;;"></i>
    				</div>
    			</div>
    			
    			@php
    			$TotalPIValue = 0;
    			$AdvnaceAmont = 0;
    			$TotalShippedValue = 0;
    			foreach($PIRecord as $PI){
    			    $products = App\Models\PIProduct::join('products AS p', 'p.product_id', 'pipro_pid')->join('quote_products AS qpro', 'pipro_pid', 'qpro_pid')->where('pipro_pi_id', $PI->pi_id)->where('qpro_qid', $PI->quote_id)->get();
    			    
    			    $GT = 0;
    			    foreach($products as $p){
                        $SubTotal = $p->qpro_price * $p->qpro_qty;
                        $SubCBM = $p->product_cbm * $p->qpro_qty;
                        $GT += $SubTotal;
                        $TotalPIValue += $SubTotal;
                    }
                    
                    $GetLogisticData = \App\Models\Logistics::where("exs_logistics_pi", "like", "%".$PI->pi_id."%")->first();
                   
                    
                    if($GetLogisticData != ""){
                        if($GetLogisticData->track_status == 3){
                        $PI->pi_id . "=====".$GT;
                        
                            $TotalShippedValue += $GT;
                            $AdvnaceAmont += ($PI->quote_payment * $GT) / 100;
                           
                        }
                    }
    			}
    			@endphp
    			
    			@php
    			$InWater = 0;
    			$Shipped = 0;
    			$BlSurrnederDue = 0;
    			$TotalContainer = 0;
    			foreach($PIRecord as $PI){
                    $GetLogisticData = \App\Models\Logistics::where("exs_logistics_pi", "like", "%".$PI->pi_id."%")->first();
                   
                    if($GetLogisticData != ""){
                        $TotalContainer++;
                        
                        if($GetLogisticData->VesselSailingETD <= date("Y-m-d") && $GetLogisticData->VesselSailingETA >= date("Y-m-d")){
                            $InWater++;
                        }
                        
                        $GetLogisticData->BLSurrenderCopySharedWith."========".$GetLogisticData->PaymentReceived;
                       
                        if($GetLogisticData->BLSurrenderCopySharedWith <= date("Y-m-d") && ($GetLogisticData->PaymentReceived >= date("Y-m-d") || $GetLogisticData->PaymentReceived != "")){
                            $BlSurrnederDue++;
                        }
                        
                        if($GetLogisticData->PaymentReceived <= date("Y-m-d") && $GetLogisticData->PaymentReceived != ""){
                            $Shipped++;
                        }
                    }
    			}
    			@endphp
    			
		        <ul class="list-group">
					<li class="list-group-item">
						<a href="#" class="list-item-link"></a><br>
						 <b> Over all PI# Value :<u>${{ number_format($TotalPIValue, 2) }}</u></b><br><br>
						<b>Total Shipped : <u> ${{ number_format($TotalShippedValue, 2) }}</u></b><br><br>
						<b>Amount of Order-in-hand : <u> ${{ number_format($TotalPIValue - $TotalShippedValue, 2) }}</u></b><br><br>
						<b>Total Adv. Of outstanding PI# Value : <u> ${{ number_format($AdvnaceAmont, 2) }}</u></b><br>
					</li>
					<li class="list-group-item">
						<a href="#" class="list-item-link"></a><br>
						 <b>Total Container :<u> {{ $TotalContainer }}</u></b><br><br>
						 <b>Stake on Water :<u> {{ $InWater }}</u></b><br><br>
						 <b>BL Surrender Due :<u> {{ $BlSurrnederDue }}</u></b><br><br>
						 <b>Shipped :<u> {{ $Shipped }}</u></b><br><br>
					</li>
					<li class="list-group-item">
						<a href="#" class="list-item-link"></a><br>
						 <b>Total GHP Invoice Value :<u> 2,56,838</u></b><br><br>
						  
					</li>
				</ul>
		    </div>
		  <!--    <div class="card mt-3">-->
		  <!--      <h3 class="card-title4">Profile</h3>-->
		  <!--      <div>-->
    <!--				<div class="text-center dash-icon">-->
    <!--					<i class="icon-user" style="color: #d37fe8;"></i>-->
    <!--				</div>-->
    <!--			</div>-->
		  <!--      <ul class="list-group">-->
				<!--	<li class="list-group-item">-->
				<!--		<a href="user/profile/buyer" class="list-item-link">Profile</a><br>-->
						 
				<!--	</li> -->
				<!--		<li class="list-group-item">-->
						<!--<a href="#" class="list-item-link"></a><br>-->
						<!--<small>(Total Assign Products)</small> : <b>16</b>-->
				<!--	</li>-->
				<!--</ul>-->
		  <!--  </div>-->
	    </div>
 
	</div>
</div>