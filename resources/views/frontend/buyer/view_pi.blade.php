<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>View PI</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">View Proforma  Invoices</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid">
    
    
    <form>
    <div class="card mb-5 mt-5">
        <h3 class="card-title">Filter Product</h3>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Order Ref.</label>
                    <input type="text" name="SearchByOrderRef" value="{{ @$_GET['SearchByOrderRef'] }}" class="form-control">
                </div>
            </div>
    
            <div class="col">
                <div class="form-group">
                    <label>PI Number</label>
                    <input type="text" name="SearchByPI" value="{{ @$_GET['SearchByPI'] }}" class="form-control">
                </div>
            </div>
            
            <div class="col">
                <div class="form-group">
                    <label>Status</label>
                    <select name="SearchSttaus" class="form-control">
                        <option value="">All</option>
                        <option @if(@$_GET['SearchSttaus'] == "0") selected @endif value="0">Open</option>
                        <option @if(@$_GET['SearchSttaus'] == "1") selected @endif value="1">Close</option>
                    </select>
                </div>
            </div>
    
            <div class="col">
                <div class="form-group">
                    <label>&nbsp;</label>
                    <button type="submit" class="btn btn-primary form-control">Filter</button>
                </div>
            </div>
        </div>
    </div>
    </form>
    

    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">List Proforma  Invoices (PI)</div>
                        <div class="ml-auto">
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i class="icon-refresh"></i> </a>
                        </div>
                    </h3>
                    <div class="basic-info-two">

                        @if(!$records->isEmpty())
                        <div class="table-responsive" style="height: 600px; border: #000 solid 0px;">
                            <table class="table table-bordered table-hover table-header-fix">
                                <thead>
                                <tr>
                                    <th style="width: 80px;">Sr. No.</th>
                                    <th>Quote ID</th>
                                    <th>Order Ref./ PI No.</th>
                                    <!--<th>Customer Info</th>-->
                                    <th>Total Amount</th>
                                    <!--<th>Adv. Amount</th>-->
                                    <th>Total CBM</th>
                                    <th>Date</th>
                                    <th>Created At</th>
                                    <th>Actions</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $sn = $records->firstItem(); @endphp
                                @foreach($records as $rec)
                                
                                @php
                                
                                $RowPINo = sprintf("%s-%03d", 'GHP-201819', $rec->pi_id);
                                $CheckStatus = DB::table('logistics')->where("exs_logistics_pi", "like", "%".$RowPINo."%")->count();
                                
                                
                                $PISatus = 0;
                                
                                if($CheckStatus > 0){
                                    $CheckStatus = DB::table('logistics')->where("exs_logistics_pi", "like", "%".$RowPINo."%")->first();
                                    if($CheckStatus->VesselSailingETD < date("Y-m-d")){
                                        $PISatus = 1;
                                    }
                                }
                                
                                @endphp
                                
                                    <tr @if($PISatus == 0) class="alert alert-success" @else class="alert alert-danger" @endif>
                                        <td>{{ $sn++ }}.</td>
                                        <td>{{ sprintf("%s-%03d", 'GHP-202122', $rec->pi_id+100) }}</td>
                                        <td>
                                            <div class="row mb-1">
                                                <div class="col-4">
                                                    <strong>Orfer Ref. :</strong>
                                                </div>
                                                <div class="col-8">
                                                    @if($rec->pi_order_ref != "")
                                                        {{ $rec->pi_order_ref }}
                                                    @endif

                                                    @if($rec->pi_order_ref == "")
                                                        {{ sprintf("%s-%03d", 'GHP-202122', $rec->pi_id+100) }}
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row mb-1">
                                                <div class="col-4">
                                                    <strong>PI No. :</strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ sprintf("%s-%03d", 'GHP-202122', $rec->pi_id+100) }}
                                                </div>
                                            </div>
                                        <!--<td>-->
                                        <!--    <div class="row mb-1">-->
                                        <!--        <div class="col-4">-->
                                        <!--            <strong>Name:</strong>-->
                                        <!--        </div>-->
                                        <!--        <div class="col-8">-->
                                        <!--            {{ $rec->user_name }}-->
                                        <!--        </div>-->
                                        <!--    </div>-->
                                        <!--    <div class="row mb-1">-->
                                        <!--        <div class="col-4">-->
                                        <!--            <strong>Mobile:</strong>-->
                                        <!--        </div>-->
                                        <!--        <div class="col-8">-->
                                        <!--            {{ $rec->user_mobile }}-->
                                        <!--        </div>-->
                                        <!--    </div>-->
                                        <!--    <div class="row mb-1">-->
                                        <!--        <div class="col-4">-->
                                        <!--            <strong>Email:</strong>-->
                                        <!--        </div>-->
                                        <!--        <div class="col-8">-->
                                        <!--            {{ $rec->user_email }}-->
                                        <!--        </div>-->
                                        <!--    </div>-->
                                        <!--</td>-->
                                        <td>{{ $rec->currency_sign }} {{ number_format($rec->quote_total,2) }}</td>
                                         <!--<td>{{ $rec->currency_sign }} {{ number_format($rec->quote_total,2) }}</td>-->
                                        <td>{{ $rec->quote_total_cbm }}</td>
                                        <td>{{ date("d/m/Y", strtotime($rec->pi_date)) }}</td>
                                        <td>{{ date("d/m/Y h:i A", strtotime($rec->pi_created_on)) }}</td>
                                        <td>
                                            <div class="mb-1">
                                                <a href="{{ url('performa-invoice/print/'.$rec->pi_id) }}" title="Print PI" data-toggle="tooltip" target="_blank"><i class="icon-print"></i> Print</a>
                                            </div>
                                        </td>
                                        <td>
                                            @if($PISatus == 0)
                                                Open
                                            @else
                                                Close
                                            @endif
                                            
                                            <br>
                                            
                                            @if($rec->pi_ship_status == 0)
                                                Not Shipped
                                            @else
                                                Shipped
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                         </div>
                        {{ $records->links() }}
                        @else
                        <div class="no_records_found">
                          No records found yet.
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
