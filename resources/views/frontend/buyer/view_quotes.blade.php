<style>
    /* tabble header fix*/
.table-header-fix{

  text-align: left;
  position: relative;
  border-collapse: collapse;
}
.table-header-fix th td
{
    padding: 0.25rem;
}
.table-header-fix tr red th {
  background: red;
  color: white;
}

.table-header-fix th {
  background: #ECECEC;
  position: sticky;
  top: 0; /* Don't forget this, required for the stickiness */
  box-shadow: 0 2px 2px -1px rgba(0, 0, 0, 0.4);
}
</style>
<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>View Quotes</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">View Quotes</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">List Quotes</div>
                        <div class="ml-auto">
                            <a href="" class="text-white" title="Remove" data-toggle="tooltip"> <i class="icon-trash-o"></i> </a>
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i class="icon-refresh"></i> </a>
                        </div>
                    </h3>
                    <div class="basic-info-two">

                        @if(!$records->isEmpty()) 
                        <div class="table-responsive" style="height: 600px; border: #000 solid 0px;">
                            <table class="table table-bordered table-hover table-header-fix">
                                <thead>
                                <tr>
                                    <th style="width: 50px;">
                                        <label class="animated-checkbox">
                                            <input type="checkbox" class="checkall">
                                            <span class="label-text"></span>
                                        </label>
                                    </th>
                                    <th style="width: 80px;">Sr. No.</th>
                                    <th>Quote ID</th>
                                    <th>Revision No.</th>
                                    <!--<th>Customer Info</th>-->
                                    <th>Total Amount</th>
                                    <th>Total CBM</th>
                                    <th>Date</th>
                                    <th>Last Modified</th>
                                    <th>Added By</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $sn = $records->firstItem(); @endphp
                                @foreach($records as $rec)
                                    <tr>
                                        <td>
                                            <label class="animated-checkbox">
                                                <input type="checkbox" name="check[]" value="{{ $rec->user_id  }}" class="check">
                                                <span class="label-text"></span>
                                            </label>
                                        </td>
                                        <td>{{ $sn++ }}.</td>
                                        <td>{{ sprintf("%s%06d", $site->setting_quote_prefix, $rec->quote_number) }}</td>
                                        <td>{{ $rec->quote_version }}</td>
                                        <!--<td>-->
                                        <!--    <div class="row mb-1">-->
                                        <!--        <div class="col-4">-->
                                        <!--            <strong>Name:</strong>-->
                                        <!--        </div>-->
                                        <!--        <div class="col-8">-->
                                        <!--            {{ $rec->user_name }}-->
                                        <!--        </div>-->
                                        <!--    </div>-->
                                        <!--    <div class="row mb-1">-->
                                        <!--        <div class="col-4">-->
                                        <!--            <strong>Mobile:</strong>-->
                                        <!--        </div>-->
                                        <!--        <div class="col-8">-->
                                        <!--            {{ $rec->user_mobile }}-->
                                        <!--        </div>-->
                                        <!--    </div>-->
                                        <!--    <div class="row mb-1">-->
                                        <!--        <div class="col-4">-->
                                        <!--            <strong>Email:</strong>-->
                                        <!--        </div>-->
                                        <!--        <div class="col-8">-->
                                        <!--            {{ $rec->user_email }}-->
                                        <!--        </div>-->
                                        <!--    </div>-->
                                        <!--</td>-->
                                        <td>{{ $rec->currency_sign }} {{ number_format($rec->quote_total,2) }}</td>
                                        <td>{{ $rec->quote_total_cbm }}</td>
                                        <td>{{ date("d/m/Y", strtotime($rec->quote_date)) }}</td>
                                        <td>{{ date("d/m/Y h:i A", strtotime($rec->quote_updated_on)) }}</td>
                                        <td>{{ ucwords($rec->quote_added_by) }}</td>
                                        <td>
                                            <div class="mb-1">
                                                <a href="{{ url('quote/info/'.$rec->quote_id) }}" title="View Details" data-toggle="tooltip"><i class="icon-eye"></i> View Details</a>
                                            </div>
                                            <!-- <div class="mb-1">
                                                <a href="{{ url('quote/revisions/'.$rec->quote_id) }}" title="View Revision Details" data-toggle="tooltip"><i class="icon-file-text"></i> View Revision Details</a>
                                            </div> -->
                                            <div class="mb-1">
                                                <a href="#" title="View Revision Details" data-toggle="tooltip"><i class="icon-file-text"></i> View Revision Details</a>
                                            </div>
                                            <div class="mb-1">
                                                <a href="{{ url('quote/add/'.$rec->quote_enq_id.'/'.$rec->quote_id) }}" title="View Details" data-toggle="tooltip"><i class="icon-repeat"></i> Revise</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                         </div>
                        {{ $records->links() }}
                        @else
                        <div class="no_records_found">
                          No records found yet.
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
