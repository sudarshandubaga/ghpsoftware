<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Profile Page</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li> 
                    <li class="active">Profile Page<b> / </b> <strong style="color:#ea6b5c"> {{ @$edit->user_name }}</strong></li>
                </ul>
            </div> 
        </div>
    </div>
</section>
<div class="container-fluid">
    <form method="post">
        @csrf
        @if (\Session::has('danger'))
            <div class="alert alert-danger">
                {!! \Session::get('danger') !!}</li>
            </div>
        @endif
        <div class="card">
        	<h3 class="card-title">
        		<div class="mr-auto">Login Details</div>
        	</h3>
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>Username</label>
            			<input type="text" name="user[user_login]" placeholder="Username" class="form-control" value="{{ @$edit->user_login }}" required @if(!empty($edit->user_login)) readonly @endif autocomplete="off">
                    </div>
        		</div>
        		
        		<div class="col">
        			<div class="form-group">
                        <label>Buyer Code</label>
            			<input type="text" readonly name="buyer[buyer_code]" placeholder="Buyer Code" class="form-control" value="{{ @$buyer_edit->buyer_code }}" required autocomplete="off">
                    </div>
        		</div>
        		
                <div class="col">
        			<div class="form-group">
                        <label>Email <small>(to login into web portal)</small></label>
            			<input type="email" readonly name="user[user_email]" placeholder="Email Address" class="form-control" value="{{ @$edit->user_email }}" required autocomplete="off">
                    </div>
        		<!--</div>-->
          <!--      <div class="col">-->
          <!--          <div class="form-group">-->
          <!--              <label>Password</label>-->
          <!--              <input type="password" name="user[user_password]" placeholder="Password" class="form-control" @if(empty($edit->user_password)) required @endif autocomplete="new-password">-->
          <!--          </div>-->
          <!--      </div>-->
        	</div>
        </div> 
        <div class="card">
        	<h3 class="card-title"> Personal Details</h3>
            <div class="row">
            	<div class="col-3">
            		<label>Consignee Name</label>
            	</div>
            	<div class="col-9">
            		<div class="form-group">
            			<input type="text"  readonly name="user[user_name]" value="{{ @$edit->user_name }}" class="form-control">
            		</div>
            	</div>
            </div>
            <div class="row">
            	<div class="col-3">
            		<label>Address</label>
            	</div>
            	<div class="col-9">
            		<div class="form-group">
            			<input type="text"  readonly name="user[user_address]" value="{{ @$edit->user_address }}" class="form-control">
            		</div>
            	</div>
            </div>
            <div class="row">
            	<div class="col-3">
            		<label>Country</label>
            	</div>
            	<div class="col-3">
            		<div class="form-group">
                        <select name="user[user_country]" class="form-control country" data-target="#buyerState">
                            <option value="">Select Country</option>
                            @foreach($countries as $country)
                            <option  readonly value="{{ $country->country_id }}" @if(@$edit->user_country == $country->country_id) selected @endif>{{ $country->country_name }}</option>
                            @endforeach
                        </select>
                    </div>
            	</div>
            	<div class="col-3 text-center">
            		<label>State / Region</label>
            	</div>
            	<div class="col-3">
            		<div class="form-group">
                        <select class="form-control" name="user[user_state]" class="form-control" id="buyerState">
                            <option value="">Select State / Region</option>
                            @foreach($states as $state)
                                <option  readonly value="{{ $state->state_id }}" @if(@$edit->user_state == $state->state_id) selected @endif>{{ $state->state_name }}</option>
                            @endforeach
                        </select>
            		</div>
            	</div>
            </div>
            <div class="row">
            	<div class="col-3">
            		<label>Mobile Number</label>
            	</div>
            	<div class="col-3">
            		<div class="form-group">
            			<input type="text"  readonly name="user[user_mobile]" value="{{ @$edit->user_mobile }}" class="form-control" required>
            		</div>
            	</div>
            	<div class="col-3 text-center">
            		<label>FAX</label>
            	</div>
            	<div class="col-3">
            		<div class="form-group">
            			<input type="text"  readonly name="user[user_fax]" value="{{ @$edit->user_fax }}" class="form-control" required>
            		</div>
            	</div>
            </div>
        </div>
        <div class="card">
            <h3 class="card-title">
                <label><input type="checkbox"  readonly name="buyer[buyer_notify_party]" value="Y" class="hide_show_toggle" @if(@$buyer_edit->buyer_notify_party == "Y") checked @endif> Buyer Notify Party Details</label>
            </h3>
            <div class="card-body @if(empty($buyer_edit->buyer_notify_party) || $buyer_edit->buyer_notify_party == 'N') d-none @endif">
                <div class="row">
                    <div class="col-3">
                        <label>Consignee Name</label>
                    </div>
                    <div class="col-9">
                        <div class="form-group">
                            <input type="text"  readonly name="buyer[buyer_np_consignee_name]" value="{{ @$buyer_edit->buyer_np_consignee_name }}" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <label>Address</label>
                    </div>
                    <div class="col-9">
                        <div class="form-group">
                            <input type="text"  readonly name="buyer[buyer_np_address]" value="{{ @$buyer_edit->buyer_np_address }}" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <label>Country</label>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <select name="buyer[buyer_np_country]" class="form-control country" data-target="#buyerNpState">
                                <option value="">Select Country</option>
                                @foreach($countries as $country)
                                <option  readonly value="{{ $country->country_id }}" @if(@$buyer_edit->buyer_np_country == $country->country_id) selected @endif>{{ $country->country_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-3 text-center">
                        <label>State / Region</label>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <select class="form-control"  readonly name="buyer[buyer_np_state]" class="form-control" id="buyerNpState">
                                <option readonly value="">Select State / Region</option>
                                @foreach($np_states as $state)
                                <option  readonly value="{{ $state->state_id }}" @if(@$buyer_edit->buyer_np_state == $state->state_id) selected @endif>{{ $state->state_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <label>Mobile Number</label>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <input type="text"  readonly name="buyer[buyer_np_mobile]" value="{{ @$buyer_edit->buyer_np_mobile }}" class="form-control">
                        </div>
                    </div>
                    <div class="col-3 text-center">
                        <label>FAX</label>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <input type="text"  readonly name="buyer[buyer_np_fax]" value="{{ @$buyer_edit->buyer_np_fax }}" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
        	<h3 class="card-title">Bank Details</h3>
        	<div class="row">
                <div class="col">
                    <div>
                        <h5>Bank Details</h5>
                        <div class="form-group">
                            <label>Bank Name</label>
                            <input type="text"  readonly name="buyer[buyer_bank_name]" value="{{ @$buyer_edit->buyer_bank_name }}" placeholder="Bank Name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Bank Address</label>
                            <input type="text" readonly  name="buyer[buyer_bank_address]" value="{{ @$buyer_edit->buyer_bank_address }}" placeholder="Bank Address" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Bank A/C No.</label>
                            <input type="text"  readonly name="buyer[buyer_bank_account]" value="{{ @$buyer_edit->buyer_bank_account }}" placeholder="Bank A/C No." class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Bank Country Code</label>
                            <input type="text"  readonly name="buyer[buyer_country_code]" value="{{ @$buyer_edit->buyer_country_code }}" placeholder="Bank Country Code" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Bank Swift Code</label>
                            <input type="text"  readonly name="buyer[buyer_swift_code]" value="{{ @$buyer_edit->buyer_swift_code }}" placeholder="Bank Swift Code" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Holder Name</label>
                            <input type="text"  readonly name="buyer[buyer_holder_name]" value="{{ @$buyer_edit->buyer_holder_name }}" placeholder="Holder Name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Holder Address</label>
                            <input type="text"  readonly name="buyer[buyer_holder_addr]" value="{{ @$buyer_edit->buyer_holder_addr }}" placeholder="Holder Address" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div>
                        <h5>
                            <label> <input type="checkbox" name="buyer[buyer_inter_bank]" value="Y" @if(@$buyer_edit->buyer_inter_bank == "Y") checked @endif class="hide_show_toggle2"> Intermediate Bank Details</label>
                        </h5>
                        <div class="toggle_body @if(empty($buyer_edit->buyer_inter_bank) || $buyer_edit->buyer_inter_bank == 'N') d-none @endif">
                            <div class="form-group">
                                <label>Bank Name</label>
                                <input type="text"  readonly name="buyer[buyer_ib_bank_name]" value="{{ @$buyer_edit->buyer_ib_bank_name }}" placeholder="Bank Name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Bank Address</label>
                                <input type="text"  readonly name="buyer[buyer_ib_bank_addr]" value="{{ @$buyer_edit->buyer_ib_bank_addr }}" placeholder="Bank Address" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Bank Swift Code</label>
                                <input type="text"  readonly name="buyer[buyer_ib_bank_swift_code]" value="{{ @$buyer_edit->buyer_ib_bank_swift_code }}" placeholder="Bank Swift Code" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Remark</label>
                                <textarea style="height: 295px"  readonly name="buyer[buyer_ib_remark]" placeholder="Remark" class="form-control">{!! @$buyer_edit->buyer_ib_remark !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
        	<h3 class="card-title">
                <label><input type="checkbox" readonly name="buyer[buery_forwarder_details]" value="Y" @if(@$buyer_edit->buery_forwarder_details == 'Y') checked @endif class="hide_show_toggle"> Forwarder Details</label>
            </h3>
            <div class="card-body @if(empty($buyer_edit->buery_forwarder_details) || $buyer_edit->buery_forwarder_details == 'N') d-none @endif">
                <div class="row">
                    <div class="col-3">
                        <label>Company Name</label>
                    </div>
                    <div class="col-9">
                        <div class="form-group">
                            <input type="text" readonly name="buyer[buyer_company_name]" value="{{ @$buyer_edit->buyer_company_name }}" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-1">1</div>
                    <div class="col">
                        <div class="form-group">
                            <input type="text" readonly name="buyer[buyer_contact_name1]" value="{{ @$buyer_edit->buyer_contact_name1 }}" class="form-control" placeholder="Contact Person Name">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input type="text" readonly name="buyer[buyer_designation1]" value="{{ @$buyer_edit->buyer_designation1 }}" class="form-control" placeholder="Designation">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input type="text" readonly name="buyer[buyer_mobile1]" value="{{ @$buyer_edit->buyer_mobile1 }}" class="form-control" placeholder="Mobile No.">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input type="text" readonly name="buyer[buyer_email1]" value="{{ @$buyer_edit->buyer_email1 }}" class="form-control" placeholder="E-mail Address">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-1">2</div>
                    <div class="col">
                        <div class="form-group">
                            <input type="text" readonly name="buyer[buyer_contact_name2]" value="{{ @$buyer_edit->buyer_contact_name2 }}" class="form-control" placeholder="Contact Person Name">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input type="text" readonly name="buyer[buyer_designation2]" value="{{ @$buyer_edit->buyer_designation2 }}" class="form-control" placeholder="Designation">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input type="text" readonly name="buyer[buyer_mobile2]" value="{{ @$buyer_edit->buyer_mobile2 }}" class="form-control" placeholder="Mobile No.">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input type="text" readonly name="buyer[buyer_email2]" value="{{ @$buyer_edit->buyer_email2 }}" class="form-control" placeholder="E-mail Address">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <label>Remarks</label>
                    </div>
                    <div class="col-9">
                        <div class="form-group">
                            <textarea readonly name="buyer[buyer_remark]" rows="5" class="form-control">{!! @$buyer_edit->buyer_remark !!}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="container-fluid">
    dfed
</div>
