<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>View Buyer Products</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">List Buyer Products</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter Product</h3>
    <div class="row">
        <div class="col-10">
            <form>
                <div class="row">
                    <div class="col">
                        <div class="row">
                            <div class="col-sm-3"><label>Product Name</label></div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="text" name="SearchByName" value="{{ @$_GET['SearchByName'] }}"
                                        class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col">

                        <div class="row">
                            <div class="col-sm-3"><label>Product Code</label></div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="text" name="SearchByCode" value="{{ @$_GET['SearchByCode'] }}"
                                        class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--<div class="col">-->
                    <!--    <div class="row">-->
                    <!--        <div class="col-sm-3"><label>Buyer Refrence</label></div>-->
                    <!--        <div class="col-sm-9">-->
                    <!--            <div class="form-group">-->
                    <!--                <input type="text" name="SearchByText" value="{{ @$_GET['SearchByText'] }}" class="form-control">-->
                    <!--            </div>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</div>-->

                    <div class="col">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary form-control">Filter</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-2">
            <div class="form-group">
                <div class="col">

                    <form action="{{ route('exportBuyerProduct', $uid) }}" method="post">
                        {{ csrf_field() }}
                        <div class="">
                            <input type="hidden" name="SearchByName" value="{{ request('SearchByName') }}">
                            <input type="hidden" name="SearchByCode" value="{{ request('SearchByCode') }}">
                            <input type="hidden" name="SearchByText" value="{{ request('SearchByText') }}">
                            <!-- <label>&nbsp;</label> -->
                            <input type="submit" class="btn btn-primary form-control" value="Export">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">List Buyer Products</div>
                        <div class="ml-auto">
                            <a href="" class="ml-auto text-white" title="Remove Selected" data-toggle="tooltip">
                                <i class="icon-trash-o"></i> </a>
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i
                                    class="icon-refresh"></i> </a>
                        </div>
                    </h3>
                    <div class="basic-info-two">

                        @if (!$records->isEmpty())
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 50px;">
                                                <label class="animated-checkbox">
                                                    <input type="checkbox" class="checkall">
                                                    <span class="label-text"></span>
                                                </label>
                                            </th>
                                            <th style="width: 80px;">Sr. No.</th>
                                            <th>Product Image</th>
                                            <th>Product Name</th>
                                            <th>Code</th>
                                            <th>Barcode</th>
                                            <th>SKU</th>
                                            <!--<th>Buyer Reference</th>-->
                                            <th>Buyer Description</th>
                                            <th>FNG Com. in (%)</th>
                                            <th>PHI Com. in (%) </th>
                                            <!--<th>GHP Com. in (%) </th>-->
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $sn = $records->firstItem(); @endphp
                                        @foreach ($records as $rec)
                                            <tr>
                                                <td>
                                                    <label class="animated-checkbox">
                                                        <input type="checkbox" name="check[]"
                                                            value="{{ $rec->bpro_id }}" class="check">
                                                        <span class="label-text"></span>
                                                    </label>
                                                </td>
                                                <td>{{ $sn++ }}.</td>
                                                <td style="width: 100px;"><img
                                                        src="{{ url('imgs/products/' . @$rec->product->product_image) }}"
                                                        alt="" style="max-width: 100px;"> </td>
                                                <td>{{ @$rec->product->product_name }}</td>
                                                <td>{{ @$rec->product->product_code }}</td>
                                                <td>
                                                    <input type="text" value="{{ $rec->bpro_barcode }}"
                                                        class="form-control barcode" style="width: 100%;"
                                                        placeholder="Barcode">
                                                </td>
                                                <td>
                                                    <input type="text" value="{{ $rec->bpro_sku }}"
                                                        class="form-control sku" style="width: 100%;"
                                                        placeholder="SKU">
                                                </td>
                                                <!--<td>{{ @$rec->product->product_text }}</td>-->
                                                <td>
                                                    <input type="text" value="{{ $rec->bpro_description }}"
                                                        class="form-control desc" style="width: 100%;"
                                                        placeholder="Buyer Description">
                                                </td>
                                                <td>
                                                    <input type="text" value="{{ $rec->salesman1_comm }}"
                                                        class="form-control comm1" style="width: 100%;"
                                                        placeholder="FNG Com. in (%)">
                                                </td>
                                                <td>
                                                    <input type="text" value="{{ $rec->salesman2_comm }}"
                                                        class="form-control comm2" style="width: 100%;"
                                                        placeholder="PHI Com. in (%)">
                                                </td>
                                                <!--<td>-->
                                                <!--    <input type="text" value="{{ $rec->salesman3_comm }}" class="form-control comm3" style="width: 100%;" placeholder="GHP Com. in (%)">-->
                                                <!--</td>-->
                                                <td>
                                                    <button type="button"
                                                        class="btn btn-success btn-sm btn-block btn-bpro-save">Save</button>
                                                    <BR>
                                                    <a href="{{ URL('product/single/') }}/{{ @$rec->product->product_code }}"
                                                        target="_blank"><button type="button"
                                                            class="btn btn-success btn-sm btn-block">Details</button></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{ $records->links() }}
                        @else
                            <div class="no_records_found">
                                No records found yet.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
