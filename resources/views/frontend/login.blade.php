<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>GHP Software</title>

    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('icomoon/style.css') }}
    {{ HTML::style('css/style.css') }}

    <link rel="icon" href="{{ url('imgs/logo1.png') }}">
</head>

<body>
    <section class="login-page">
        <div>
            <form id="loginForm" method="post" action="{{ url('ajax/user_login') }}">
                @csrf
                <div class="logo_login mb-3">
                    <img src="{{ asset('imgs/GHP_Logo_New_copy.jpg') }}" class="img-thumbnail_logo" alt="GHP">
                </div>

                <div class="sec">
                    <!--<h3 class="text-center mb-3">Login</h3>-->
                    <!--<hr>-->
                    <div class="form-msg"></div>
                    <div class="form-group">
                        <!-- <label>Username</label> -->
                        <input type="text" name="record[user_login]" class="form-control rad" placeholder="Username"
                            autocomplete="off" required>
                    </div>

                    <div class="form-group">
                        <!--<label>Password</label>-->
                        <input type="password" name="record[user_password]" class="form-control rad"
                            placeholder="Password" autocomplete="new-password" required>
                    </div>

                    <div class="third">
                        <button type="submit" class="btn btn-login btn-block third">Sign In</button>
                    </div>


                </div>
            </form>
        </div>
    </section>

    {{ HTML::script('js/jquery.min.js') }}
    {{ HTML::script('js/popper.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/sweetalert.min.js') }}
    {{ HTML::script('js/validation.js') }}
    {{ HTML::script('admin/tinymce/js/tinymce/tinymce.min.js') }}
    {{ HTML::script('admin/js/main.js') }}
</body>

</html>
