<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Inventory</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Inventory</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!--<form>-->
<!--<div class="card mb-5 mt-5">-->
<!--    <h3 class="card-title">Filter Product</h3>-->
<!--    <div class="row">-->
<!--        <div class="col">-->
<!--            <div class="row">-->
<!--                <div class="col-sm-3"><label>Item Code</label></div>-->
<!--                <div class="col-sm-9">-->
<!--                    <div class="form-group">-->
<!--                        <input type="text" name="SearchByItemCode" value="{{ @$_GET['SearchByItemCode'] }}" class="form-control">-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->


<!--        <div class="col">-->
<!--            <div class="row">-->
<!--                <div class="col-sm-3"><label>Product Title</label></div>-->
<!--                <div class="col-sm-9">-->
<!--                    <div class="form-group">-->
<!--                        <input type="text" name="SearchByProTitle" value="{{ @$_GET['SearchByProTitle'] }}" class="form-control">-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->



<!--        <div class="col">-->

<!--            <div class="row">-->
<!--                <div class="col-sm-3"><label>Status</label></div>-->
<!--                <div class="col-sm-9">-->
<!--                    <div class="form-group">-->
<!--                        <select name="SearchByStatus" class="form-control">-->
<!--                            <option value="">All</option>-->
<!--                            <option @if (@$_GET['SearchByStatus'] == '1') selected @endif value="1">Open</option>-->
<!--                            <option @if (@$_GET['SearchByStatus'] == '2') selected @endif value="2">Custom Clearance Process</option>-->
<!--                            <option @if (@$_GET['SearchByStatus'] == '3') selected @endif value="3"> Shipped</option>-->
<!--                        </select>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->

<!--        <div class="col">-->
<!--            <div class="form-group">-->
<!--                <button type="submit" class="btn btn-primary form-control">Filter</button>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--</form>-->
<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter Product</h3>
    <div class="row">
        <div class="col-10">
            <form>
                <div class="row">
                    <!-- <div class="col">
                    <div class="form-group">
                        <label>Customer Name</label>
                        <input type="text" name="SearchByName" value="{{ @$_GET['SearchByName'] }}" class="form-control">
                    </div>
                </div> -->
                    <div class="col">
                        <div class="form-group">
                            <label>Select Buyer</label>
                            <select name="SearchBuyer" id="" class="select2 form-control">
                                <option value="">Select All Buyer</option>
                                @if (!empty($buyers))
                                    @foreach ($buyers as $buyer)
                                        <option value="{{ $buyer->user_id }}"
                                            @if (@$_GET['SearchBuyer'] == $buyer->user_id) selected @endif>
                                            {{ ucwords(strtolower($buyer->user_name)) }}</option>
                                    @endforeach
                                @endif
                            </select>
                            {{-- <input type="text" name="SearchByName" value="{{ @$_GET['SearchByName'] }}" class="form-control"> --}}
                        </div>
                    </div>



                    <div class="col">
                        <div class="form-group">
                            <label>Item Code</label>
                            <div class="form-group">
                                <input type="text" name="SearchByItemCode" value="{{ @$_GET['SearchByItemCode'] }}"
                                    class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label>Product Title</label>
                            <div class="form-group">
                                <input type="text" name="SearchByProTitle" value="{{ @$_GET['SearchByProTitle'] }}"
                                    class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="">Status</label>
                            <select name="SearchByStatus" class="form-control">
                                <option value="">All</option>
                                <option @if (@$_GET['SearchByStatus'] == '1') selected @endif value="1">Open</option>
                                <option @if (@$_GET['SearchByStatus'] == '2') selected @endif value="2">Custom
                                    Clearance Process</option>
                                <option @if (@$_GET['SearchByStatus'] == '3') selected @endif value="3"> Shipped
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="col">
                        <div class="form-group">
                            <label>&nbsp;</label>
                            <button type="submit" class="btn btn-primary form-control">Filter</button>
                        </div>
                    </div>

                </div>

            </form>
        </div>
        <div class="col-2">
            <form action="{{ route('exportInventory') }}" method="post">
                {{ csrf_field() }}
                <div class="">
                    <input type="hidden" name="SearchBuyer" value="{{ request('SearchBuyer') }}">
                    <input type="hidden" name="SearchByStatus" value="{{ request('SearchByStatus') }}">
                    <input type="hidden" name="SearchByProTitle" value="{{ request('SearchByProTitle') }}">
                    <label>&nbsp;</label>
                    <input type="submit" class="btn btn-primary form-control" value="Export">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">Inventory OverView </div>
                    </h3>
                    <div class="basic-info-two">

                        @if (!$records->isEmpty())
                            <div class="table-responsive" style="height: 500px; border: #000 solid 0px;">
                                <table class="table table-bordered table-hover table-header-fix">
                                    <thead>
                                        <tr style="background: #dec4c4;text-align: center;">
                                            <th style="width: 80px;">Sr. No.</th>
                                            <th>GHP Item Code</th>
                                            <th>Product Image</th>
                                            <th>Product Details</th>
                                            <th>CBM</th>
                                            <th>GHP MRP</th>
                                            <th>Buyer</th>
                                            <th>GHP PI# <b>/</b> Order Reference</th>
                                            <th>PI# Confirmation Date</th>
                                            <th>PI# Delivery Date</th>
                                            <th>Buyer PO#</th>
                                            <th>PI# QTY</th>
                                            <th>Buyer SKU</th>
                                            <th>Buyer Barcode</th>
                                            <th>FNG Com. in (%)</th>
                                            <th>PHI Com. in (%)</th>
                                            <th>GHP Com. in (%)</th>
                                            <th>Total CBM</th>
                                            <th>PI# Price</th>
                                            <th>Total PI#</th>
                                            <th>Assign Vendor Name</th>
                                            <th>GHP Vendor PO#</th>
                                            <th>PO# Date</th>
                                            <th>PO# Delivery</th>
                                            <th>Vendor Price PO#</th>
                                            <th>Shipped QTY.</th>
                                            <th>Balance (Due) QTY.</th>
                                            <th>CBM</th>
                                            <th>Status</th>
                                            <th>GHP Invoice No.</th>
                                            <th>Container No.</th>
                                            <th>E.T.D.</th>
                                            <th>E.T.A.</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $sn = 1; @endphp
                                        @foreach ($records as $rec)
                                            @php
                                                $PINumber = sprintf('%s-%03d', 'GHP-201819', $rec->pi_id);
                                                
                                                $web_edit = \DB::connection('mysql2')
                                                    ->table('products')
                                                    ->where('product_id', $rec->popro_pid)
                                                    ->first();
                                                $categories = \DB::connection('mysql2')
                                                    ->table('categories')
                                                    ->where('category_id', $web_edit->product_category)
                                                    ->first();
                                                $SubCategories = \DB::connection('mysql2')
                                                    ->table('categories')
                                                    ->where('category_id', $web_edit->product_subcategory)
                                                    ->first();
                                                $SubCategories1 = \DB::connection('mysql2')
                                                    ->table('categories')
                                                    ->where('category_id', $web_edit->product_subcategory2)
                                                    ->first();
                                                
                                                $Finish = \DB::table('finish')
                                                    ->where('finish_id', $rec->product_finish_main)
                                                    ->first();
                                                $Buyer = \DB::table('users')
                                                    ->where('user_id', $rec->quote_uid)
                                                    ->first();
                                                
                                                $GHPInvoice = \DB::table('ghp_invoice')
                                                    ->where('exs_ghp_invoice_po_id', $rec->po_id)
                                                    ->first();
                                                $Logsitic = \DB::table('logistics')
                                                    ->where('exs_logistics_pi', 'like', '%' . $PINumber . '%')
                                                    ->first();
                                                
                                                $BuyerProductsData = \DB::table('buyer_products')
                                                    ->where('bpro_uid', $rec->quote_uid)
                                                    ->where('bpro_pid', $rec->popro_pid)
                                                    ->first();
                                                
                                                $products = \App\Models\PIProduct::join('quote_products AS qpro', 'pipro_pid', 'qpro_pid')
                                                    ->where('pipro_pi_id', $rec->pi_id)
                                                    ->where('qpro_qid', $rec->quote_id)
                                                    ->where('pipro_pid', $rec->popro_pid)
                                                    ->first();
                                                
                                                $InvoicedProducts = \App\Models\PoInvoiceProduct::where('invoice_po_no', $rec->po_id)
                                                    ->where('exs_po_invoice_products_product_id', $rec->popro_pid)
                                                    ->first();
                                                
                                                $POproducts = \App\Models\POProduct::where('popro_po_id', $rec->po_id)
                                                    ->where('popro_pid', $rec->popro_pid)
                                                    ->first();
                                                
                                                $VenQuote = DB::table('vquote_products')
                                                    ->where('vqpro_pid', $POproducts->popro_pid)
                                                    ->where('vqpro_qid', $rec->po_qid)
                                                    ->first();
                                                
                                                $Log = DB::table('logistics')
                                                    ->where('exs_logistics_pi', $rec->pi_id)
                                                    ->first();
                                                $ShipQty = \App\Models\PIProduct::where('pipro_pi_id', $rec->pi_id)->first();
                                                
                                                $MatType1 = \DB::table('material_type')
                                                    ->where('material_type_id', $rec->materialtype1)
                                                    ->first();
                                                $MatType2 = \DB::table('material_type')
                                                    ->where('material_type_id', $rec->materialtype2)
                                                    ->first();
                                                $Mtrl1 = \DB::table('material')
                                                    ->where('material_id', $rec->material1)
                                                    ->first();
                                                $Mtrl2 = \DB::table('material')
                                                    ->where('material_id', $rec->material2)
                                                    ->first();
                                                $Fnsh1 = \DB::table('finish')
                                                    ->where('finish_id', $rec->finish1)
                                                    ->first();
                                                $Fnsh2 = \DB::table('finish')
                                                    ->where('finish_id', $rec->finish2)
                                                    ->first();
                                                
                                            @endphp
                                            <td style="width: 80px;text-align: center;vertical-align: middl">
                                                {{ $sn++ }}.</td>
                                            <td>{{ $rec->product_code }}</td>
                                            <td style="width: 100px;"><img
                                                    src="{{ url('imgs/products/' . @$rec->product->product_image) }}"
                                                    alt="" style="max-width: 100px;"> </td>
                                            <td style="white-space: nowrap;">
                                                <strong>{{ $rec->product_name }}</strong><br>
                                                <b>L</b> {{ $rec->product_l_inch }} X <b>W</b>
                                                {{ $rec->product_w_inch }} X <b>H</b> {{ $rec->product_h_inch }}
                                                (Inch)</br> <BR>
                                                <strong>Material:</strong> {{ @$Mtrl1->material_name }} /
                                                {{ @$Mtrl2->material_name }}<br>
                                                <strong>Material Type:</strong> {{ @$MatType1->material_type_name }} /
                                                {{ @$MatType2->material_type_name }}<br>
                                                <strong>Finish :</strong> {{ @$Fnsh1->finish_title }} /
                                                {{ @$Fnsh2->finish_title }}<br>

                                                <!--</td> -->
                                                <!--<td style="white-space: nowrap;">-->
                                                <!--    <strong>{{ $rec->product_name }}</strong><br>-->

                                                <!--    <b>L</b> {{ $rec->product_l_inch }} X <b>W</b> {{ $rec->product_w_inch }} X <b>H</b> {{ $rec->product_h_inch }} (Inch)</br> <BR>-->
                                                <!--    <strong>Material:</strong> {{ @$Mtrl1->material_name }} / {{ @$Mtrl2->material_name }}<br>-->
                                                <!--    <strong>Material Type:</strong> {{ @$MatType1->material_type_name }} / {{ @$MatType2->material_type_name }}<br>-->
                                                <!--    <strong>Finish :</strong> {{ @$Fnsh1->finish_title }} / {{ @$Fnsh2->finish_title }}<br>-->

                                                <!--</td> -->
                                            <td>{{ number_format($rec->product_cbm, 3) }} </td>
                                            <td><b>$</b>{{ $rec->product_mrp }}</td>
                                            <td style="white-space: nowrap;">{{ $Buyer->user_name }}</td>
                                            <td style="white-space: nowrap;">
                                                GHP-202122-{{ sprintf('%03d', $rec->pi_id + 100) }} <b>/</b>
                                                {{ $rec->pi_order_ref }}</td>
                                            <td style="white-space: nowrap;">
                                                {{ date('d-M-Y', strtotime(@$rec->pi_confirm_date)) }} </td>
                                            <td style="white-space: nowrap;">
                                                {{ date('d-M-Y', strtotime(@$rec->pi_delivery_date)) }} </td>
                                            <td style="white-space: nowrap;"> {{ $rec->pi_order_ref }}</td>
                                            <td>{{ @$products->qpro_qty }}</td>
                                            <td style="white-space: nowrap;">{{ $rec->popro_sku }}</td>
                                            <td style="white-space: nowrap;">{{ $rec->popro_barcode }}</td>
                                            <td>{{ @$BuyerProductsData->salesman1_comm }} %</td>
                                            <td>{{ @$BuyerProductsData->salesman2_comm }} %</td>
                                            <td>{{ @$BuyerProductsData->salesman3_comm }} %</td>
                                            <td>{{ @$rec->product_cbm * @$products->qpro_qty }}</td>
                                            <td><b>$</b>{{ @number_format($products->qpro_price, 2) }}</td>
                                            <td><b>$</b>{{ number_format(@$products->qpro_price * @$products->qpro_qty, 2) }}
                                            </td>
                                            <td style="white-space: nowrap;">{{ $rec->user_name }}</td>
                                            <td style="white-space: nowrap;">
                                                2021{{ str_pad($rec->po_id + 100, 2, '0', STR_PAD_LEFT) }}</td>
                                            <td style="white-space: nowrap;">
                                                {{ date('d-M-Y', strtotime(@$rec->po_created_on)) }}</td>
                                            <td style="white-space: nowrap;">
                                                {{ date('d-M-Y', strtotime(@$rec->po_delivery_days)) }}</td>
                                            <td><b>$</b>{{ $VenQuote ? number_format($VenQuote->vqpro_price, 2) : '0' }}
                                            </td>
                                            <td>{{ @$InvoicedProducts->pip_qty }}</td>
                                            <td>{{ @$products->qpro_qty - @$InvoicedProducts->pip_qty }}</td>
                                            <td>{{ @$InvoicedProducts->pip_TotalMeas }}</td>
                                            <td>
                                                @if (@$Logsitic->track_status == 1)
                                                    <div style="background: #cce5ff;">Open</div>
                                                @endif

                                                @if (@$Logsitic->track_status == 2)
                                                    <div style="background: #f77c88;">Custom Clearance Process</div>
                                                @endif

                                                @if (@$Logsitic->track_status == 3)
                                                    <div style="background: #d4edda;"> Shipped</div>
                                                @endif
                                            </td>
                                            <td>{{ @$InvoicedProducts->exs_po_invoice_products_inv_id }}</td>
                                            <td>{{ @$Logsitic->ContainerNr }}</td>
                                            <td style="white-space: nowrap;">
                                                {{ date('d-M-Y', strtotime(@$Logsitic->AgreedETD)) }}</td>
                                            <td style="white-space: nowrap;">
                                                {{ date('d-M-Y', strtotime(@$Logsitic->VesselSailingETA)) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            @if ($FileterApplied == 0)
                                {
                                {{ $records->appends(request()->query())->links() }}
                            @endif
                        @else
                            <div class="no_records_found">
                                No records found yet.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
