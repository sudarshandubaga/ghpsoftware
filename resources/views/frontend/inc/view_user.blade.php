<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>View {{ $role_name }}</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">{{ $role_name }}</li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <a href="{{ url('user/add/'.$role) }}" title="Add New" class="btn btn-default" data-toggle="tooltip"> <i class="icon-plus-circle"></i> Add New</a>
                </div>
            </div>
        </div>
    </div>
</section>


<form>
<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter User</h3>
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col-sm-2"><label>Consignee Name</label></div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <input type="text" name="SearchByName" value="{{ @$_GET['SearchByName'] }}" class="form-control">
                    </div>
                </div>
                <div class="col-sm-2"><label>Buyer Name</label></div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <input type="text" name="SearchByName1" value="{{ @$_GET['SearchByName1'] }}" class="form-control">
                    </div>
                </div>
            </div> 
        </div>
        <div class="col">
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control">Filter</button>
            </div>
        </div>
    </div>
</div>
</form>


<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">List {{ $role_name }}s</div>
                        <div class="ml-auto">
                            <a href="" class="text-white" title="Remove" data-toggle="tooltip"> <i class="icon-trash-o"></i> </a>
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i class="icon-refresh"></i> </a>
                        </div>
                    </h3>
                    <div class="basic-info-two">
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                            {!! \Session::get('success') !!}
                        </div>
                        @endif
                        @if(!$records->isEmpty())
                        {{ $records->links() }}
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 50px;">
                                        <label class="animated-checkbox">
                                            <input type="checkbox" class="checkall">
                                            <span class="label-text"></span>
                                        </label>
                                    </th>
                                    <th style="width: 80px;">Sr. No.</th>
                                    <th>User Details</th>
                                    <th>Organisation Bank Details</th>
                                    <th>Forwarder Details</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $sn = $records->firstItem(); @endphp
                                @foreach($records as $rec)
                                    <tr>
                                        <td>
                                            <label class="animated-checkbox">
                                                <input type="checkbox" name="check[]" value="{{ $rec->user_id  }}" class="check">
                                                <span class="label-text"></span>
                                            </label>
                                        </td>
                                        <td>{{ $sn++ }}.</td>
                                        <td>
                                             <div class="row">
                                                <div class="col-5">
                                                    <strong>Buyer : </strong>
                                                </div>
                                                <div class="col-7">
                                                    {{ $rec->user_main_name }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-5">
                                                    <strong>Consignee : </strong>
                                                </div>
                                                <div class="col-7">
                                                    {{ $rec->user_name }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-5">
                                                    <strong>Mobile No. : </strong>
                                                </div>
                                                <div class="col-7">
                                                    {{ $rec->user_mobile }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-5">
                                                    <strong>Email : </strong>
                                                </div>
                                                <div class="col-7">
                                                    {{ $rec->user_email }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-5">
                                                    <strong>Login Name : </strong>
                                                </div>
                                                <div class="col-7">
                                                    {{ $rec->user_login }}
                                                </div>
                                            </div>
                                        </td>
                                        @php
                                        $BuyerProfile = DB::table('buyers')->where('buyer_uid', $rec->user_id)->first();
                                        @endphp
                                        <td>
                                            <div class="row">
                                                <div class="col-6">
                                                    <strong>Bank Name : </strong>
                                                </div>
                                                <div class="col-6">
                                                    {{ !empty($BuyerProfile->buyer_bank_name) ? $BuyerProfile->buyer_bank_name : '' }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-7">
                                                    <strong>Bank Address : </strong>
                                                </div>
                                                <div class="col-5">
                                                    {{ !empty($BuyerProfile->buyer_bank_address) ? $BuyerProfile->buyer_bank_address: '' }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-6">
                                                    <strong>Bank A/C No. : </strong>
                                                </div>
                                                <div class="col-6">
                                                    {{ !empty($BuyerProfile->buyer_bank_account) ? $BuyerProfile->buyer_bank_account :'' }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-7">
                                                    <strong>Bank Swift Code : </strong>
                                                </div>
                                                <div class="col-5">
                                                    {{ !empty($BuyerProfile->buyer_swift_code) ? $BuyerProfile->buyer_swift_code :'' }}
                                                </div>
                                            </div>
                                              <br><u>Intermediate Bank Details</u> <br><br>
                                            <div class="row">
                                                <div class="col-7">
                                                    <strong>Bank Name : </strong>
                                                </div>
                                                <div class="col-5">
                                                    {{ !empty($BuyerProfile->buyer_ib_bank_name) ? $BuyerProfile->buyer_ib_bank_name: '' }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-6">
                                                    <strong>Bank Address : </strong>
                                                </div>
                                                <div class="col-6">
                                                    {{ !empty($BuyerProfile->buyer_ib_bank_addr) ? $BuyerProfile->buyer_ib_bank_addr : '' }}
                                                </div>
                                            </div>
                                             <div class="row">
                                                <div class="col-6">
                                                    <strong>Bank A/C No. : </strong>
                                                </div>
                                                <div class="col-6">
                                                    {{ !empty($BuyerProfile->buyer_ib_bank_addr) ? $BuyerProfile->buyer_ib_bank_addr : '' }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-7">
                                                    <strong>Bank Swift Code: </strong>
                                                </div>
                                                <div class="col-5">
                                                    {{ !empty($BuyerProfile->buyer_ib_bank_swift_code) ? $BuyerProfile->buyer_ib_bank_swift_code : '' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-4">
                                                    <strong>Name : </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ !empty($BuyerProfile->buyer_company_name) ? $BuyerProfile->buyer_company_name: '' }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <strong>Contact Person: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ !empty($BuyerProfile->buyer_contact_name1) ? $BuyerProfile->buyer_contact_name1 :'' }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <strong>Designation : </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{!empty( $BuyerProfile->buyer_designation1) ? $BuyerProfile->buyer_designation1:'' }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <strong>Mobile No. : </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ !empty($BuyerProfile->buyer_mobile1) ? $BuyerProfile->buyer_mobile1 :'' }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <strong>Email : </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ !empty($BuyerProfile->buyer_email1) ? $BuyerProfile->buyer_email1:'' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="margin-bottom: 10px;">
                                                <a href="{{ url('user/add/'.$role.'/'.$rec->user_id) }}" title="Edit" data-toggle="tooltip"><i class="icon-pencil1"></i></a>
                                            </div>
                                            <div style="margin-bottom: 10px;">
                                            @if($rec->user_is_enabled == 'Y')
                                                <a href="{{ url('user/change_status/'.$role.'/'.$rec->user_id) }}"  class="btn btn-sm text-white btn-success mb-1">Active</a>
                                            @else
                                            <a href="{{ url('user/change_status/'.$role.'/'.$rec->user_id) }}" class="btn-danger btn btn-sm text-white mb-1">Deactive</a>
                                            @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                         </div>
                        {{ $records->links() }}
                        @else
                        <div class="no_records_found">
                          No records found yet.
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- <div class="col-sm-4">
                <div class="card warning">
                    <h3 class="card-title">Instructions</h3>
                    <div>
                        <ul>
                            <li><i class="icon-check-square"></i> Admin can view users details</li>
                            <li><i class="icon-check-square"></i> Admin can go to add user page</li>
                            <li><i class="icon-check-square"></i> Admin can go to edit user's information</li>
                        </ul>
                    </div>
                </div>
            </div> -->
        </div>
    </form>
</div>
