<style rel="css">
    #searchVendor {
        background-image: url(https://www.w3schools.com/css/searchicon.png);
        background-position: 10px 10px;
        background-repeat: no-repeat;
        height: 46px;
        padding-left: 40px;
    }
</style>
{{-- <link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
<script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script> --}}

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Vendor Grid List Page</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Vendor Grid List</li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <!-- <a href="{{ url('user/add/') }}" title="Add New" class="btn btn-default" data-toggle="tooltip"> <i class="icon-plus-circle"></i> Add New</a> -->
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <form method="post" id="vendor_list" onkeydown="return event.key != 'Enter';">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <span>Vendor Grid List</span>
                        <div class="ml-auto"></div>
                    </h3>
                    <div class="basic-info-two">
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                            {!! \Session::get('success') !!}
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group ui-widget">
                                    <select name="" id="" class="form-control get_products select2">
                                        <option value="">Select Vendor</option>
                                        @if(!empty($vendors))
                                        @foreach($vendors as $vendor)
                                        <option value="{{ $vendor->user_id }}">{{ $vendor->user_name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="">
                                    {{-- <button type="button" id="export_grid" class="float-right btn btn-sm btn-primary">Export</button> --}}
                                </div>
                            </div>
                        </div>
                        <div class="updateData">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


<script>
$(document).ready(function(){
    $("#export_grid").hide();
    $(".get_products").on("change", function(){
        var id = $(this).val();
        $("#export_grid").hide();
        $.ajax({
            url: '{{ route("vendor_grid_products") }}',
            method:"post",
            data: {id: id, "_token": "{{ csrf_token() }}" },
            success: function(response){
                $(".updateData").html(response.html);
                if(response.products.length >= 1){
                    $("#export_grid").show();
                }
            }
        })
    })

    $("#export_grid").on("click", function(){
        var id = $(".get_products").val();

        $.ajax({
            url: '{{ route("export_vendor_grid_list") }}',
            method:"post",
            data: {id: id, "_token": "{{ csrf_token() }}" },
            success: function(response){


                console.logO(response);
            }
        })
    })
})
</script>
