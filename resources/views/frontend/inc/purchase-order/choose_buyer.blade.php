<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Choose PO</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Choose</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    @if (\Session::has('danger'))
        <div class="alert alert-danger">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif
    <div class="card">
        <h3 class="card-title">
            <div class="mr-auto">Login Details</div>
        </h3>

        <div class="row">
            <div class="col">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Buyer</label>
                            <select class="form-control" onChange="ShowBuyerPO(this.value)">
                                <option value="">Select</option>
                                @foreach ($BuyerArray as $key => $BA)
                                    <option value="{{ $key }}">{{ $BA }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <form method="get" action="{{ URL('purchase-order/create-invoice') }}">
                    <div id="LoadPO" style="max-height: 500px; overflow: auto;"></div>
                    <div class="mt-3 d-none">
                        <button class="btn btn-danger">Generate Invoice</button>
                    </div>
                </form>
            </div>
            <div class="col-1">
                <div style="width: 1px; background: #ddd; height: 100%; margin:auto;"></div>
            </div>
            <div class="col">
                <div id="checked_pos"></div>
            </div>
        </div>
    </div>
</div>


<script>
    function ShowBuyerPO(ID) {
        $.ajax({
            url: "{{ URL('LoadBuyerPO') }}/" + ID,
            type: "GET",
            contentType: false,
            cache: false,
            processData: false,
            success: function(data, textStatus, jqXHR) {
                $("#LoadPO").next().removeClass("d-none");
                $("#LoadPO").html(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }

    $(function() {
        $(document).on("click", ".pos_check", function(res) {
            let html = `<h3>Selected PO</h3>`;

            $(".pos_check:checked").each(function() {
                html +=
                    `<div class="border py-2 px-3">${$(this).data('label')}</div>`;
            });

            $("#checked_pos").html(html);
        });
    })
</script>
