<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Booking Invoice Cum Packing List</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="{{ url('quote') }}">Booking Invoice Cum Packing List</a></li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <a href="{{ url('quote') }}" title="View Quote" class="btn btn-default" data-toggle="tooltip"> <i
                            class="icon-eye"></i> View Quote</a>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function LoadExtras(id) {
        pkgDmlL = "";
        pkgDmlW = "";
        pkgDmlH = "";
        pkgDmlMass = "";

        MasterCarton = parseInt($("#PrdPcsCtn" + id).val());
        for (i = 1; i <= MasterCarton; i++) {
            pkgDmlL += '<input type="text" name="InvoiceProducts[' + id +
                '][PkgDimL][]" value="0" required id="PrdPkgDimL' + id + '_' + i + '" onBlur="UpdateAll(' + id +
                ')" style="width: 50px; margin-bottom: 5px">';
            pkgDmlW += '<input type="text" name="InvoiceProducts[' + id +
                '][PkgDimW][]" value="0" required id="PrdPkgDimW' + id + '_' + i + '" onBlur="UpdateAll(' + id +
                ')" style="width: 50px; margin-bottom: 5px">';
            pkgDmlH += '<input type="text" name="InvoiceProducts[' + id +
                '][PkgDimH][]" value="0" required id="PrdPkgDimH' + id + '_' + i + '" onBlur="UpdateAll(' + id +
                ')" style="width: 50px; margin-bottom: 5px">';
            pkgDmlMass += '<input type="text" name="InvoiceProducts[' + id +
                '][Meas][]" value="0" required id="PrdPkgDimMeas' + id + '_' + i + '" onBlur="UpdateAll(' + id +
                ')" style="width: 50px; margin-bottom: 5px">';
        }

        $("#LoadPkgDimL" + id).html(pkgDmlL);
        $("#LoadPkgDimW" + id).html(pkgDmlW);
        $("#LoadPkgDimH" + id).html(pkgDmlH);
        $("#LoadPkgDimHMass" + id).html(pkgDmlMass);
    }
</script>
<div class="container-fluid">

    @if (\Session::has('danger'))
        <div class="alert alert-danger">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <form data-session="pi_cart" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{ implode(',', $POIDS) }}" name="record[po_invoice_po]">
                <button class="btn btn-primary">Save</button>

                <div class="clearfix">&nbsp;</div>

                <div class="card">
                    <h3 class="card-title">
                        <div class="mr-auto">PI Details</div>
                    </h3>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Invoice No.</label>
                                <input type="text" {{-- name="record[po_invoice_invoice_no]" --}} class="form-control"
                                    placeholder="BWS[AUTO-GEN-CODE]" readonly>
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label>Invoice Date</label>
                                <input type="date" name="record[po_invoice_invoice_date]" class="form-control"
                                    required>
                                <!--<input type="date" name="record[po_invoice_invoice_date]" class="form-control" readonly required>-->
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Statement on Origin</label>
                                <input type="text" name="record[po_invoice_statement_origin]" class="form-control"
                                    required>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Container Size</label>
                                <input type="text" name="record[po_invoice_container_size]" class="form-control"
                                    maxlength="11" required>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>FSC License Number</label>
                                <input type="text" name="record[po_invoice_fsc_license]" class="form-control"
                                    required>
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label>FSC Certificate Code</label>
                                <input type="text" name="record[po_invoice_fsc_certificate_code]"
                                    class="form-control" required>
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label>Container Number</label>
                                <input type="text" name="record[po_invoice_container]" class="form-control">
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label>Line Seal Number</label>
                                <input type="text" name="record[po_invoice_line_seel]" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Custom Seal Number</label>
                                <input type="text" name="record[po_invoice_custom_seal]" class="form-control">
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label>Vessel / Voyage</label>
                                <input type="text" name="record[po_invoice_voyage]" class="form-control">
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label>ETD</label>
                                <input type="text" name="record[po_invoice_etd]" class="form-control datepicker"
                                    readonly>
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label>ETA</label>
                                <input type="text" name="record[po_invoice_eta]" class="form-control datepicker"
                                    readonly>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <h3 class="card-title">Product / Item Details</h3>
                    <div class="table-responsive" style="max-height: 500px;">
                        <table class="table table-bordered table-hover table-header-fix" style="background: #FFF">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Product Po No</th>
                                    <th>Product Image</th>
                                    <th>Case No.</th>
                                    <th>Ordered Qty</th>
                                    <th>Invoiced Qty</th>
                                    <th>Unit Price</th>
                                    <th>Total Price</th>
                                    <th>Pcs / ctn</th>
                                    <th>Total Pkt</th>
                                    <th>N. W. Wd /PCs</th>
                                    <th>N. W. Irn /PCs</th>
                                    <th>T.Nw Kg</th>
                                    <th>GW/Pkt Kgs</th>
                                    <th>Ttl Gw Kg</th>
                                    <th>Pkg Dim L</th>
                                    <th>Pkg Dim W</th>
                                    <th>Pkg Dim H</th>
                                    <th>Meas</th>
                                    <th>Ttl Meas</th>
                                    <th>HS Code</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $sn = $totQty = $totPrice = $totCBM = 0;
                                @endphp
                                @foreach ($products as $Pindex => $p)
                                    @php
                                        $dir = 'imgs/products/';
                                        $image = 'imgs/no-image.png';
                                        if (!empty($p->product_image)) {
                                            $image = $dir . $p->product_image;
                                        }
                                        
                                        $GetInvoicePro = App\Models\PoBookingInvoiceProduct::where('exs_po_invoice_products_product_id', $p->popro_pid)
                                            ->where('po_invoice_qid', $p->po_qid)
                                            ->sum('pip_qty');
                                        
                                        $RemaingQty = $p->popro_qty - $GetInvoicePro;
                                        
                                        $SubTotal = $p->popro_price * $p->popro_qty;
                                        $GetTotalPkt = $p->product_mp * $p->popro_qty;
                                        if ($p->product_ip > 1) {
                                            $GetTotalPkt = $p->popro_qty / $p->product_ip;
                                        }
                                    @endphp

                                    @if ($RemaingQty > 0)
                                        <tr>
                                            <input type="hidden" name="InvoiceProducts[{{ $Pindex }}][id]"
                                                value="{{ $p->popro_pid }}">
                                            <input type="hidden" value="{{ $p->product_ip }}"
                                                id="PrdInnerPkt{{ $Pindex }}">
                                            <input type="hidden" value="{{ $p->po_qid }}"
                                                name="InvoiceProducts[{{ $Pindex }}][po_invoice_qid]">

                                            <input type="hidden" value="{{ $p->popro_po_id }}"
                                                name="InvoiceProducts[{{ $Pindex }}][invoice_po_no]">

                                            <td>{{ ++$sn }}</td>
                                            <td>{{ $p->pono }}</td>
                                            <td>
                                                <img src="{{ url($image) }}" alt="{{ $p->product_name }}"
                                                    style="max-width: 70px;">
                                                {{ $p->product_name }}
                                            </td>
                                            <td><input type="text"
                                                    name="InvoiceProducts[{{ $Pindex }}][caseno]" required></td>
                                            <td>{{ $p->popro_qty }}</td>
                                            <td><input type="text"
                                                    name="InvoiceProducts[{{ $Pindex }}][qty]"
                                                    value="{{ $RemaingQty }}" required
                                                    id="PrdQty{{ $Pindex }}"
                                                    onBlur="UpdateAll('{{ $Pindex }}')" style="width: 50px">
                                            </td>
                                            <td><input type="text" value="{{ $p->popro_price }}"
                                                    name="InvoiceProducts[{{ $Pindex }}][price]" readonly
                                                    id="PrdPrice{{ $Pindex }}"
                                                    onBlur="UpdateAll('{{ $Pindex }}')" style="width: 100px">
                                            </td>
                                            <td><input type="text" value="{{ $SubTotal }}"
                                                    name="InvoiceProducts[{{ $Pindex }}][subtotal]" readonly
                                                    id="PrdSubTotal{{ $Pindex }}"
                                                    onBlur="UpdateAll('{{ $Pindex }}')" style="width: 100px">
                                            </td>
                                            <td>
                                                <input type="text"
                                                    name="InvoiceProducts[{{ $Pindex }}][Pcsctn]"
                                                    value="{{ $p->product_mp }}" required
                                                    id="PrdPcsCtn{{ $Pindex }}"
                                                    onBlur="UpdateAll('{{ $Pindex }}')" style="width: 50px">
                                            </td>
                                            <td>
                                                <input type="text"
                                                    name="InvoiceProducts[{{ $Pindex }}][TotalPackets]"
                                                    value="{{ $GetTotalPkt }}" required readonly
                                                    id="PrdTotalPacket{{ $Pindex }}"
                                                    onBlur="UpdateAll('{{ $Pindex }}')" style="width: 50px">
                                            </td>
                                            <td>
                                                <input type="text"
                                                    name="InvoiceProducts[{{ $Pindex }}][NWwWd]" value="0"
                                                    required id="PrdNwwWd{{ $Pindex }}"
                                                    onBlur="UpdateAll('{{ $Pindex }}')" style="width: 50px">
                                            </td>
                                            <td>
                                                <input type="text"
                                                    name="InvoiceProducts[{{ $Pindex }}][NWIrn]" value="0"
                                                    required id="PrdNwIrn{{ $Pindex }}"
                                                    onBlur="UpdateAll('{{ $Pindex }}')" style="width: 50px">
                                            </td>
                                            <td>
                                                <input type="text"
                                                    name="InvoiceProducts[{{ $Pindex }}][TotalNetWeightKG]"
                                                    value="0" required readonly
                                                    id="PrdTotalNWKg{{ $Pindex }}"
                                                    onBlur="UpdateAll('{{ $Pindex }}')" style="width: 50px">
                                            </td>
                                            <td>
                                                <input type="text"
                                                    name="InvoiceProducts[{{ $Pindex }}][GWPktKgs]"
                                                    value="0" required id="PrdGwPktKg{{ $Pindex }}"
                                                    onBlur="UpdateAll('{{ $Pindex }}')" style="width: 50px">
                                            </td>
                                            <td>
                                                <input type="text"
                                                    name="InvoiceProducts[{{ $Pindex }}][TotalGrossWeight]"
                                                    value="0" required readonly
                                                    id="PrdTotalGwWg{{ $Pindex }}"
                                                    onBlur="UpdateAll('{{ $Pindex }}')" style="width: 50px">
                                            </td>

                                            <td id="LoadPkgDimL{{ $Pindex }}">
                                                <input type="text"
                                                    name="InvoiceProducts[{{ $Pindex }}][PkgDimL]"
                                                    value="0" required id="PrdPkgDimL{{ $Pindex }}"
                                                    onBlur="UpdateAll('{{ $Pindex }}')" style="width: 50px">
                                            </td>
                                            <td id="LoadPkgDimW{{ $Pindex }}">
                                                <input type="text"
                                                    name="InvoiceProducts[{{ $Pindex }}][PkgDimW]"
                                                    value="0" required id="PrdPkgDimW{{ $Pindex }}"
                                                    onBlur="UpdateAll('{{ $Pindex }}')" style="width: 50px">
                                            </td>
                                            <td id="LoadPkgDimH{{ $Pindex }}"><input type="text"
                                                    name="InvoiceProducts[{{ $Pindex }}][PkgDimH]"
                                                    value="0" required id="PrdPkgDimH{{ $Pindex }}"
                                                    onBlur="UpdateAll('{{ $Pindex }}')" style="width: 50px">
                                            </td>
                                            <td id="LoadPkgDimHMass{{ $Pindex }}"><input type="text"
                                                    name="InvoiceProducts[{{ $Pindex }}][Meas]" value="0"
                                                    required readonly id="PrdMeas{{ $Pindex }}"
                                                    onBlur="UpdateAll('{{ $Pindex }}')" style="width: 50px">
                                            </td>

                                            <td><input type="text"
                                                    name="InvoiceProducts[{{ $Pindex }}][TotalMeas]"
                                                    value="0" required readonly
                                                    id="PrdTotalCBM{{ $Pindex }}"
                                                    onBlur="UpdateAll('{{ $Pindex }}')" style="width: 50px">
                                            </td>
                                            <td><input type="text"
                                                    name="InvoiceProducts[{{ $Pindex }}][HSCode]"
                                                    style="width: 50px"></td>
                                        </tr>
                                    @endif
                                    <script>
                                        LoadExtras(<?php echo $Pindex; ?>)
                                    </script>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <!-- <tr>
                        <th colspan="6">TOTAL</th>
                        <th>{{ $totQty }}</th>
                        <th>{{ number_format($totCBM, 6) }}</th>
                        <th>{{ $totPrice }}</th>
                        <th colspan="2"></th>
                    </tr> -->
                            </tfoot>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function UpdateAll(id) {
        Qty = parseInt($("#PrdQty" + id).val());
        Price = parseFloat($("#PrdPrice" + id).val());

        MasterCarton = parseInt($("#PrdPcsCtn" + id).val());
        InnerCarton = parseInt($("#PrdInnerPkt" + id).val());

        NWWood = parseFloat($("#PrdNwwWd" + id).val());
        NWIrn = parseFloat($("#PrdNwIrn" + id).val());

        GWPktKG = parseFloat($("#PrdGwPktKg" + id).val());

        PkgDimL = parseFloat($("#PrdPkgDimL" + id).val());
        PkgDimW = parseFloat($("#PrdPkgDimW" + id).val());
        PkgDimH = parseFloat($("#PrdPkgDimH" + id).val());

        CalculateSubPrice = Qty * Price;
        $("#PrdSubTotal" + id).val(CalculateSubPrice);

        CalculateTotalPacket = Qty * MasterCarton;
        if (InnerCarton > 1) {
            CalculateTotalPacket = InnerCarton / Qty;
        }

        $("#PrdTotalPacket" + id).val(CalculateTotalPacket);

        TotalNWKg = (NWWood + NWIrn) * Qty;
        $("#PrdTotalNWKg" + id).val(TotalNWKg);

        TotalGwKG = GWPktKG * Qty;
        $("#PrdTotalGwWg" + id).val(TotalGwKG);

        Meas = ((PkgDimL * PkgDimW * PkgDimH) / 1000000).toFixed(5);
        $("#PrdMeas" + id).val(Meas);

        TotalMeas = (Meas * CalculateTotalPacket).toFixed(5);
        $("#PrdTotalCBM" + id).val(TotalMeas);



        TotalMeas = 0;
        TotalRowMas = 0;

        for (i = 1; i <= MasterCarton; i++) {

            PkgDimL = parseFloat($("#PrdPkgDimL" + id + "_" + i).val());
            PkgDimW = parseFloat($("#PrdPkgDimW" + id + "_" + i).val());
            PkgDimH = parseFloat($("#PrdPkgDimH" + id + "_" + i).val());

            Meas = ((PkgDimL * PkgDimW * PkgDimH) / 1000000).toFixed(5);
            $('#PrdPkgDimMeas' + id + '_' + i).val(Meas);

            TotalRowMas += ((PkgDimL * PkgDimW * PkgDimH) / 1000000);
        }

        TotalMeas = (TotalRowMas * Qty).toFixed(5);
        $("#PrdTotalCBM" + id).val(TotalMeas);
    }
</script>
