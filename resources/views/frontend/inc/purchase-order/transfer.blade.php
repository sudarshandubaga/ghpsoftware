@extends('frontend.layouts.app')

@section('main_section')
    <section class="page-header mb-3">
        <div class="container-fluid">
            <div class="clearfix">
                <div class="float-left">
                    <h1>PO Transfer</h1>
                    <ul class="breadcrumbs clearfix">
                        <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                        <li><a href="{{ route('purchase-order.index') }}">Purchase Orders</a></li>
                        <li class="active">PO Transfer</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <h3 class="card-title">Current PO</h3>
                        <div>
                            <table class="table">
                                <tr>
                                    <th>#PO</th>
                                    <td>{{ '2021-' . ($purchaseOrder->po_id + 100) }}</td>
                                </tr>
                                <tr>
                                    <th>Vendor</th>
                                    <td>{{ @$purchaseOrder->vender->vendor_org_name }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    @if ($purchaseOrder->products)
                        <div class="card">
                            <h3 class="card-title">Products</h3>
                            <div style="max-height: 400px; overflow: auto">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>ITEM DESCRIPTION</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($purchaseOrder->products as $i => $p)
                                            <tr>
                                                <td>{{ $i + 1 }}</td>
                                                <td>
                                                    <div>
                                                        <strong>
                                                            {{ $p->product_name }}
                                                        </strong>
                                                    </div>
                                                    <div>
                                                        {{ $p->product_code }} x {{ $p->pivot->popro_qty }}
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-primary btn-sm rounded cart-btn"
                                                        data-pid="{{ $p->product_id }}" data-item='{!! json_encode($p->pivot) !!}'>
                                                        ADD
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-lg-8">
                    @if (!empty($products))
                        <form action="{{ route('po.transfer.store', $purchaseOrder) }}" method="post">
                            @csrf
                            <div class="card">
                                <h3 class="card-title">New PO</h3>
                                <div class="row align-items-end">
                                    <div class="col-lg-10 col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-4 mb-3">
                                                <label for="vendor_id" class="form-label">Select Vendor</label>
                                                <select name="vendor_id" id="vendor_id" class="form-control" required>
                                                    <option value>Select Vendor</option>
                                                    @foreach ($vendors as $id => $name)
                                                        <option value="{{ $id }}">{{ $name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-4 mb-3">
                                                <label for="po_delivery_date" class="form-label">Delivery Date</label>
                                                <input type="date" name="po_delivery_date" id="po_delivery_date"
                                                    placeholder="Select Date" class="form-control" required>
                                            </div>
                                            <div class="col-sm-4 mb-3">
                                                <label for="po_delivery_days" class="form-label">Delivery Days</label>
                                                <input type="number" name="po_delivery_days" id="po_delivery_days"
                                                    placeholder="Enter Delivery Days" class="form-control" required>
                                            </div>
                                            <div class="col-sm-4 mb-3">
                                                <label for="po_date" class="form-label">PO Date</label>
                                                <input type="date" name="po_date" id="po_date"
                                                    placeholder="Enter PO Date" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-3 text-right">
                                        <button type="submit"
                                            class="btn btn-primary btn-block btn-sm rounded-pill">Submit</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <h3 class="card-title">Products</h3>
                                <div>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>ITEM DESCRIPTION</th>
                                                <th>QTY</th>
                                                <th>PRICE</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($products as $index => $p)
                                                <input type="hidden" name="product[{{ $index }}][popro_sku]"
                                                    value="{{ $p->popro_sku }}">
                                                <input type="hidden" name="product[{{ $index }}][popro_barcode]"
                                                    value="{{ $p->popro_barcode }}">
                                                <input type="hidden" name="product[{{ $index }}][popro_buyer_desc]"
                                                    value="{{ $p->popro_buyer_desc }}">
                                                <input type="hidden" name="product[{{ $index }}][popro_pid]"
                                                    value="{{ $p->product_id }}">

                                                <input type="hidden" name="cbm[{{ $p->product_id }}]"
                                                    value="{{ $p->product_cbm }}">

                                                <tr>
                                                    <td>{{ $index + 1 }}.</td>
                                                    <td>
                                                        <div>
                                                            <strong>
                                                                {{ $p->product_name }}
                                                            </strong>
                                                        </div>
                                                        <div>{{ $p->product_code }}</div>
                                                    </td>
                                                    <td>
                                                        <input type="number"
                                                            name="product[{{ $index }}][popro_qty]"
                                                            class="form-control" value="{{ $p->popro_qty }}" min="1"
                                                            max="{{ $p->max_qty ?: 1 }}">
                                                    </td>
                                                    <td>
                                                        <input type="text"
                                                            name="product[{{ $index }}][popro_price]"
                                                            class="form-control" value="{{ $p->popro_price }}"
                                                            min="0">
                                                    </td>
                                                    <td>
                                                        <a href="">
                                                            {{-- &times; --}}
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    @else
                        <div class="card rounded">
                            <div class="card-body text-center">
                                <h3>No Record(s) Found.</h3>
                                <p>Please Add Item From Left Side.</p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extra_scripts')
    <script>
        $(function() {
            $(document).on("click", ".cart-btn", function() {
                let id = $(this).data('pid'),
                    item = $(this).data('item');

                $.ajax({
                    url: "{{ route('purchase-order.cart.store') }}",
                    method: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id,
                        item
                    },
                    success: function(res) {
                        location.reload();
                    }
                });
            });
        });
    </script>
@endsection
