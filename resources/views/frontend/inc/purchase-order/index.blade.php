<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>View PO</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">View Purchase Order</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter PURCHASE ORDER</h3>
    <div class="row">
        <div class="col-10">
            <form>
                <div class="row">
                    <div class="col-lg">
                        <div class="row">
                            <div class="col-sm-5"><label>Po No</label></div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="text" name="SearchBypno" value="{{ @$_GET['SearchBypno'] }}"
                                        class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    @if ($profile->user_role == 'vendor')
                        <div class="col-lg">
                            <div class="form-group">
                                <label>Select Buyer</label>
                                <select name="SearchBuyer" id="" class="select2 form-control">
                                    <option value="">Select Buyer</option>
                                    @if (!empty($buyers))
                                        @foreach ($buyers as $id => $name)
                                            <option value="{{ $id }}"
                                                @if (@$_GET['SearchBuyer'] == $id) selected @endif>
                                                {{ ucwords(strtolower($name)) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    @else
                        <div class="col-lg">
                            <div class="form-group">
                                <label>Select Vendor</label>
                                <select name="SearchVendor" id="" class="select2 form-control">
                                    <option value="">Select Vendor</option>
                                    @if (!empty($vendors))
                                        @foreach ($vendors as $vendor)
                                            <option value="{{ $vendor->user_id }}"
                                                @if (@$_GET['SearchVendor'] == $vendor->user_id) selected @endif>
                                                {{ ucwords(strtolower($vendor->user_name)) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    @endif

                    <div class="col-lg">
                        <div class="row">
                            <div class="col-sm-5"><label> PI No</label></div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="text" name="SearchByPINo" value="{{ @$_GET['SearchByPINo'] }}"
                                        class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="row">
                            <div class="col-sm-5"><label> Status</label></div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <select name="SearchStatus" id="" class="select2 form-control">
                                        <option value="">Select Status</option>
                                        <option value="0" @if (@$_GET['SearchStatus'] == '0') selected @endif>Running
                                        </option>
                                        <option value="1" @if (@$_GET['SearchStatus'] == '1') selected @endif>Shipped
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="form-group">
                            <div class="col-sm-5"><label> </label></div>
                            <button type="submit" class="btn btn-primary form-control">Filter</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-2">

            <div class="form-group">
                <div class="col">

                    <form action="{{ route('exportPo') }}" method="post">
                        {{ csrf_field() }}
                        <div class="">
                            <input type="hidden" name="SearchBypno" value="{{ request('SearchBypno') }}">
                            <input type="hidden" name="SearchVendor" value="{{ request('SearchVendor') }}">
                            <input type="hidden" name="SearchByPINo" value="{{ request('SearchByPINo') }}">
                            <input type="hidden" name="SearchStatus" value="{{ request('SearchStatus') }}">
                            <label>&nbsp;</label>
                            <input type="submit" class="btn btn-primary form-control" value="Export">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">List Purchase Order (PO)</div>
                        <div class="ml-auto">

                            @if ($profile->user_role == 'vendor')
                                <a class="text-white" href="{{ url('purchase-order/choose-buyer') }}"><i
                                        class="icon-plus"></i></a>
                                &nbsp;
                            @endif

                            <a href="" class="text-white" title="Remove" data-toggle="tooltip"> <i
                                    class="icon-trash-o"></i> </a>
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i
                                    class="icon-refresh"></i> </a>
                        </div>
                    </h3>
                    <div class="basic-info-two">

                        @if (!$records->isEmpty())
                            <div class="table-responsive" style="height: 500px; border: #000 solid 0px;">
                                <table class="table table-bordered table-hover table-header-fix">
                                    <thead>
                                        <tr>
                                            <th style="width: 50px;">
                                                <label class="animated-checkbox">
                                                    <input type="checkbox" class="checkall">
                                                    <span class="label-text"></span>
                                                </label>
                                            </th>
                                            <th style="width: 50px;">Sr. No.</th>
                                            <th style="width: 80px;">PO No</th>
                                            <th>Quote ID</th>
                                            <th>Order Ref./ PI No.</th>
                                            <th>Customer Info</th>
                                            <th>Total Amount</th>
                                            <th>Total CBM</th>
                                            <th style="width: 100px;">Date</th>
                                            <th>Created At</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $sn = $records->firstItem(); @endphp
                                        @foreach ($records as $rec)
                                            @php
                                                $GetPIData = \App\Models\PIModel::find($rec->po_pi_id);
                                                $po_invoice_count = \App\Models\PoInvoice::where('po_invoice_po', $rec->po_id)->count();
                                            @endphp
                                            <tr
                                                style="background-color: {{ $po_invoice_count > 0 ? '#d4edda' : '#f8d7da' }}">
                                                <td>
                                                    <label class="animated-checkbox">
                                                        <input type="checkbox" name="check[]"
                                                            value="{{ $rec->user_id }}" class="check">
                                                        <span class="label-text"></span>
                                                    </label>
                                                </td>
                                                <td>{{ $sn++ }}.</td>
                                                <td>2021-{{ $rec->po_id + 100 }}</td>
                                                <td>{{ sprintf('%s%04d', $site->setting_quote_prefix, $rec->vquote_number) }}
                                                </td>
                                                <td>
                                                    <div class="row mb-1">
                                                        <div class="col-5">
                                                            <strong>Orfer Ref. :</strong>
                                                        </div>
                                                        <div class="col-7">
                                                            {{ @$GetPIData->pi_order_ref }}
                                                        </div>
                                                    </div>

                                                    <div class="row mb-1">
                                                        <div class="col-5">
                                                            <strong>PI No. :</strong>
                                                        </div>
                                                        <div class="col-7">
                                                            {{ sprintf('%s-%03d', 'GHP-202122', $GetPIData->pi_id + 100) }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    @if ($profile->user_role == 'vendor')
                                                        <div class="mb-1">
                                                            {{ $rec->buyer_code }}
                                                        </div>
                                                    @else
                                                        <div class="mb-1">
                                                            {{ $rec->user_name }}
                                                        </div>
                                                        <div class="mb-1">
                                                            {{ $rec->user_mobile }}
                                                        </div>
                                                        <div class="mb-1">
                                                            {{ $rec->user_email }}
                                                        </div>
                                                    @endif
                                                </td>
                                                <!-- <td><strong>{{ $rec->currency_sign }}</strong> {{ number_format($rec->vquote_total, 2) }}</td> -->
                                                <td><strong>{{ $rec->currency_sign }}</strong>
                                                    {{ number_format($rec->po_total, 2) }}</td>
                                                <td>{{ number_format($rec->vquote_total_cbm, 3) }}</td>
                                                <td>{{ date('d-M-Y', strtotime($rec->po_date)) }}</td>
                                                <td>{{ date('d-M-Y h:i A', strtotime($rec->po_created_on)) }}</td>
                                                <td>
                                                    <div class="mb-1">
                                                        @if ($po_invoice_count > 0)
                                                            <div class="mb-2 font-weight-bold">Shipped</div>
                                                        @else
                                                            <div class="mb-2 font-weight-bold">Running</div>
                                                        @endif
                                                        <a href="{{ url('purchase-order/print/' . $rec->po_id) }}"
                                                            title="Print PO" data-toggle="tooltip" target="_blank"><i
                                                                class="icon-print"></i> Print</a>
                                                        <br>
                                                        <a href="{{ url('purchase-order-sample/print/' . $rec->po_id) }}"
                                                            title="Print PO for QC" data-toggle="tooltip"
                                                            target="_blank"><i class="icon-print"></i> QC</a>
                                                        <br>
                                                        <!--<a  href="{{ url('purchase-order/copy/' . $rec->po_id) }}"  title="Copy PI" data-toggle="tooltip" target="_blank"><i class="icon-print"></i> copy</a>-->
                                                        <!--<br>-->
                                                        <a href="{{ url('purchase-order/edit/' . $rec->po_id) }}"
                                                            title="Edit PI" data-toggle="tooltip">
                                                            <i class="icon-pen"></i>edit
                                                        </a>
                                                        <a href="{{ route('po.transfer', $rec) }}"
                                                            class="btn btn-sm btn-outline-dark">
                                                            Transfer PO
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-3">
                                {{ $records->appends(request()->query())->links() }}
                            </div>
                        @else
                            <div class="no_records_found">
                                No records found yet.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
