<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Shipping Line</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Shipping Line</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
	<div class="card mb-5 mt-5">
	    
	    @if(Session::has('Success'))
        <div class="alert alert-success mb-5" role="alert">{!!Session::get('Success')!!}</div>
        @endif
        
        @if(Session::has('Danger'))
        <div class="alert alert-danger mb-5" role="alert">{!!Session::get('Danger')!!}</div>
        @endif
        
		<h3 class="card-title">Add Shipping Line</h3>
	     <form method="post">
	     	@csrf
		     <div class="row">
		         <div class="col-sm-6">
		             
		               <div class="row">
		                 <div class="col-sm-3">
		                     <label>
		                         Shipping Line Name
		                     </label>
		                 </div>

		                 <div class="col-sm-9">
		                      <div class="form-group">
		                          <input type="text" name="record[shipping_line_name]" value="{{ @$edit->shipping_line_name }}" class="form-control"  required="">
		                      </div>
		                 </div>
		             </div>
		             
		             
		             <div class="row">
		                 <div class="col-sm-3">
		                     <label>
		                         Shipping Line Mail
		                     </label>
		                 </div>

		                 <div class="col-sm-9">
		                      <div class="form-group">
		                          <input type="text" name="record[shipping_line_mail]" value="{{ @$edit->shipping_line_mail }}" class="form-control"  required="">
		                      </div>
		                 </div>
		             </div>
		             
		             
		             <div class="row">
		                 <div class="col-sm-3">
		                     <label>
		                         Shipping Line Web Address
		                     </label>
		                 </div>

		                 <div class="col-sm-9">
		                      <div class="form-group">
		                          <input type="text" name="record[shipping_line_web_add]" value="{{ @$edit->shipping_line_web_add }}" class="form-control"  required="">
		                      </div>
		                 </div>
		             </div>
		         </div>

		         <div class="col-sm-2">

		         </div>

		         <div class="col-sm-2">
		         	<div class="form-group">
		                <button type="submit" class="btn btn-primary form-control">SAVE</button>
		            </div>
		         </div>
		     </div>
		 </form>
	</div>
	<div class="card mb-5 mt-5">    
        
		<h3 class="card-title">Search Sipping Line</h3>
		<div class="row">
			<div class="col-9">

				<form>
					<div class="row">
						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-3">
									<label>
										Name
									</label>
								</div>
	   
								<div class="col-sm-9">
									 <div class="form-group">
										 <input type="text" name="shipping_line_name" value="{{ request('shipping_line_name') }}" class="form-control" >
									 </div>
								</div>
							</div>
						</div>
	   
						<div class="col-sm-4">
							<div class="form-group">
								<button type="submit" class="btn btn-primary form-control">SEARCH</button>
							</div>
							<!-- <div class="form-group">
								 <a href="{{ url('finish') }}" class="btn btn-default text-center form-control">ADD</a>
							</div> -->
						</div>
					</div>
				</form>
			</div>
			<div class="col-3">
			<div class="form-group">
						<div class="col">
							
							<form action="{{ route('exportShippingLine') }}" method="post">
								{{ csrf_field() }}          
								<div class="" >
									<input type="hidden" name="shipping_line_name" value="{{request('shipping_line_name')}}">
									
									<input type="submit" class="btn btn-primary form-control" value="Export">
								</div>       
							</form>
						</div>
					</div>
			</div>
		</div>
	</div>
	<form method="post">
		<div class="card">
			<h3 class="card-title">
				<div class="mr-auto">View Shipping Line</div>
		        <a href="" class="ml-auto text-white" title="Remove Selected" data-toggle="tooltip">
		            <i class="icon-trash-o"></i>
		        </a>
			</h3>
	    	@csrf
		    @if(!$records->isEmpty())
		    <div class="table-responsive">
			    <table class="table table-bordered">
			          <thead>
			               <tr>
			                    <th style="width: 50px;">
			                        <label class="animated-checkbox">
			                            <input type="checkbox" class="checkall">
			                            <span class="label-text"></span>
			                        </label>
			                    </th>
			                   <th style="width: 50px;">S.No.</th>
			                   <th>Name</th>
			                   <th>Mail</th>
			                   <th>Web Address</th>
			                   <th>Action</th>
			               </tr>
			          </thead>

			          <tbody>
			          		@php $sn = $records->firstItem(); @endphp
			          		@foreach($records as $rec)
			               	<tr>
		                        <td>
		                            <label class="animated-checkbox">
		                                <input type="checkbox" name="check[]" value="{{ $rec->shipping_line_id  }}" class="check">
		                                <span class="label-text"></span>
		                            </label>
		                        </td>
								<td>{{ $sn++ }}</td>
								<td>{{ $rec->shipping_line_name }}</td>
								<td>{{ $rec->shipping_line_mail }}</td>
								<td>{{ $rec->shipping_line_web_add }}</td>
								<td class="icon-cent">
									<a href="{{ url('shipping-line/'.$rec->shipping_line_id) }}" class="pencil"><i class="icon-pencil" title="Edit"></i></a>
								</td>
			               	</tr>
			               @endforeach
			          </tbody>
			    </table>
			</div>
			{{ $records->links() }}
		    @else
		    <div class="no_records_found">
		      No records found yet.
		    </div>
			@endif
		</div>
	</form>
</div>