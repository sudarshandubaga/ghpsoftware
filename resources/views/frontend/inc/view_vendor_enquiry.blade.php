<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>View Vendor Enquiries</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">View Vendor Enquiries</li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <!-- <a href="{{ url('user/add/') }}" title="Add New" class="btn btn-default" data-toggle="tooltip"> <i class="icon-plus-circle"></i> Add New</a> -->
                </div>
            </div>
        </div>
    </div>
</section>
<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter VENDOR ENQUIRIES</h3>
    <div class="row">
        <div class="col-12">
            <form>
                <div class="row">

                    <div class="col-lg">
                        <div class="form-group">
                            <label>Select Vendor</label>
                            <select name="SearchVendor" id="" class="select2 form-control">
                                <option value="">Select Vendor</option>
                                @if (!empty($vendors))
                                    @foreach ($vendors as $vendor)
                                        <option value="{{ $vendor->user_id }}"
                                            @if (@$_GET['SearchVendor'] == $vendor->user_id) selected @endif>
                                            {{ ucwords(strtolower($vendor->user_name)) }}</option>
                                    @endforeach
                                @endif
                            </select>
                            {{-- <input type="text" name="SearchByName" value="{{ @$_GET['SearchByName'] }}" class="form-control"> --}}
                        </div>
                    </div>

                    <div class="col-lg">
                        <div class="form-group">
                            <div class="col-sm-5"><label> </label></div>
                            <button type="submit" class="btn btn-primary form-control">Filter</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- <div class="col-2">

            <div class="form-group">
                <div class="col">
                    
                    <form action="{{ route('exportPo') }}" method="post">
                        {{ csrf_field() }}
                        <div class="" >
                            <input type="hidden" name="SearchBypno" value="{{ request('SearchBypno') }}">
                            <input type="hidden" name="SearchVendor" value="{{ request('SearchVendor') }}">
                            <input type="hidden" name="SearchByPINo" value="{{ request('SearchByPINo') }}">
                            <input type="hidden" name="SearchStatus" value="{{ request('SearchStatus') }}">
                            <label>&nbsp;</label>
                            <input type="submit" class="btn btn-primary form-control" value="Export">
                        </div>
                    </form>
                </div>
            </div>
        </div> -->
    </div>
</div>
<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">List Vendor Enquiries</div>
                        <div class="ml-auto">
                            <a href="" class="text-white" title="Remove" data-toggle="tooltip"> <i
                                    class="icon-trash-o"></i> </a>
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i
                                    class="icon-refresh"></i> </a>
                        </div>
                    </h3>
                    <div class="basic-info-two">
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                {!! \Session::get('success') !!}
                            </div>
                        @endif
                        @if (!$records->isEmpty())
                            <div class="table-responsive" style="height: 500px; border: #000 solid 0px;">
                                <table class="table table-bordered table-hover table-header-fix">
                                    <thead>
                                        <tr>
                                            <th style="width: 50px;">
                                                <label class="animated-checkbox">
                                                    <input type="checkbox" class="checkall">
                                                    <span class="label-text"></span>
                                                </label>
                                            </th>
                                            <th style="width: 80px;">Sr. No.</th>
                                            <th>Quote ID</th>
                                            @if ($profile->user_role == 'admin')
                                                <th>Vendor Name</th>
                                                <th>Vendor Mobile</th>
                                                <th>Vendor Email</th>
                                            @endif
                                            <th>Date</th>
                                            <th>More</th>
                                            @if ($profile->user_role == 'vendor')
                                                <th>Action</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $sn = $records->firstItem(); @endphp
                                        @foreach ($records as $rec)
                                            @php
                                                $is_quoted = DB::connection('mysql')
                                                    ->table('vendor_quotes')
                                                    ->where('vquote_enq_id', $rec->venq_id)
                                                    ->select('vquote_id')
                                                    ->first();
                                            @endphp
                                            <tr>
                                                <td>
                                                    <label class="animated-checkbox">
                                                        <input type="checkbox" name="check[]"
                                                            value="{{ $rec->venq_id }}" class="check">
                                                        <span class="label-text"></span>
                                                    </label>
                                                </td>
                                                <td>{{ $sn++ }}.</td>
                                                <td> {{ sprintf('GHP_QT_%06d', $rec->venq_id) }} </td>
                                                @if ($profile->user_role == 'admin')
                                                    <td> {{ $rec->vendor_org_name }} </td>
                                                    <td>{{ $rec->vendor_tel }}</td>
                                                    <td>{{ $rec->vendor_email }}</td>
                                                @endif
                                                @php
                                                    $val = !empty($rec->venq_created_on) ? $rec->venq_created_on : $rec->venq_updated_on;
                                                @endphp
                                                <td>{{ 'DT.' . date('d.m.Y h:i A', strtotime($val)) }}</td>
                                                <td class="text-center">
                                                    <a href="#oproduct_{{ $rec->venq_id }}" rel="product_toggle"><i
                                                            class="icon-plus-circle text-success"
                                                            style="font-size: 24px;"></i></a>
                                                </td>
                                                @if ($profile->user_role == 'vendor')
                                                    <td>
                                                        @if (empty($is_quoted))
                                                            <a href="{{ url('vendor_quote/add/' . $rec->venq_id) }}"
                                                                class="btn btn-sm btn-info">Create Quote</a>
                                                        @else
                                                            <span class="text-success">Quotation Created</span>
                                                        @endif
                                                    </td>
                                                @endif
                                            </tr>
                                            @php
                                                $products = DB::connection('mysql')
                                                    ->table('products AS p')
                                                    ->join('vendor_enqproducts AS vp', 'p.product_id', 'vp.vpro_pid')
                                                    ->where('vpro_enqid', $rec->venq_id)
                                                    ->get();
                                            @endphp
                                            <tr id="oproduct_{{ $rec->venq_id }}" style="display: none;">
                                                <td colspan="9">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>Image</th>
                                                                <th>Name</th>
                                                                <th>CBM</th>
                                                                <th>Qty</th>
                                                                <th>CBM Subtotal</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @php
                                                                $totCBM = $totQty = 0;
                                                            @endphp
                                                            @foreach ($products as $p)
                                                                <tr>
                                                                    <td width="120">
                                                                        <img src="{{ url('imgs/products/' . $p->product_image) }}"
                                                                            alt="{{ $p->product_name }}"
                                                                            style="width: 100px;">
                                                                    </td>
                                                                    <td>{{ $p->product_name }}</td>
                                                                    <td>{{ $p->product_cbm }}</td>
                                                                    <td>{{ $p->vpro_qty }}</td>
                                                                    <td>{{ $cbm = number_format($p->product_cbm * $p->vpro_qty, 6) }}
                                                                    </td>
                                                                </tr>
                                                                @php
                                                                    $totCBM += $cbm;
                                                                    $totQty += $p->vpro_qty;
                                                                @endphp
                                                            @endforeach
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="3">TOTAL</td>
                                                                <td>{{ $totQty }}</td>
                                                                <td>{{ number_format($totCBM, 6) }}</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{ $records->links() }}
                        @else
                            <div class="no_records_found">
                                No records found yet.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- <div class="col-sm-4">
                <div class="card warning">
                    <h3 class="card-title">Instructions</h3>
                    <div>
                        <ul>
                            <li><i class="icon-check-square"></i> Admin can view users details</li>
                            <li><i class="icon-check-square"></i> Admin can go to add user page</li>
                            <li><i class="icon-check-square"></i> Admin can go to edit user's information</li>
                        </ul>
                    </div>
                </div>
            </div> -->
        </div>
    </form>
</div>
