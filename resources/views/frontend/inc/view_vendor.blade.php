<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>View {{ $role_name }}</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">{{ $role_name }}</li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <a href="{{ url('user/add/'.$role) }}" title="Add New" class="btn btn-default" data-toggle="tooltip"> <i class="icon-plus-circle"></i> Add New</a>
                </div>
            </div>
        </div>
    </div>
</section>

<form>
<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter User</h3>
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col-sm-3"><label>Name</label></div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <input type="text" name="SearchByName" value="{{ @$_GET['SearchByName'] }}" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="row">
                <div class="col-sm-3"><label>Vendor Code</label></div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <input type="text" name="SearchByCode" value="{{ @$_GET['SearchByCode'] }}" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control">Filter</button>
            </div>
        </div>
    </div>
</div>
</form>


<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">List {{ $role_name }}s</div>
                        <div class="ml-auto">
                            <a href="" class="text-white" title="Remove" data-toggle="tooltip"> <i class="icon-trash-o"></i> </a>
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i class="icon-refresh"></i> </a>
                        </div>
                    </h3>
                    <div class="basic-info-two">
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                            {!! \Session::get('success') !!}
                        </div>
                        @endif
                        @if(!$records->isEmpty())
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 50px;">
                                        <label class="animated-checkbox">
                                            <input type="checkbox" class="checkall">
                                            <span class="label-text"></span>
                                        </label>
                                    </th>
                                    <th style="width: 80px;">Sr. No.</th>
                                    <th>User Details</th>
                                    <th>Organisation Details</th>
                                    <th>Organisation Bank Details</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $sn = $records->firstItem(); @endphp
                                @foreach($records as $rec)

                                    <tr>
                                        <td>
                                            <label class="animated-checkbox">
                                                <input type="checkbox" name="check[]" value="{{ $rec->user_id  }}" class="check">
                                                <span class="label-text"></span>
                                            </label>
                                        </td>
                                        <td>{{ $sn++ }}.</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-4" style="white-space: nowrap;">
                                                    <strong>Name: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ $rec->user_name }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4" style="white-space: nowrap;">
                                                    <strong>Mobile : </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_tel }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4" style="white-space: nowrap;">
                                                    <strong>Email ID: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_email }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4" style="white-space: nowrap;">
                                                    <strong>Login: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ $rec->user_login }}
                                                </div>
                                            </div> 
                                             <div class="row">
                                                <div class="col-4" style="white-space: nowrap;">
                                                    <strong>Code: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_code }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4" style="white-space: nowrap;">
                                                    <strong>Short N. : </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_short_name }}
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-4">
                                                    <strong>Organisation Name: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_org_name }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <strong>Organisation Address: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_address }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <strong>City: </strong>
                                                </div>
                                                <div class="col-2">
                                                    {{ @$rec->vendor->vendor_city }}
                                                </div>
                                                <div class="col-2">
                                                    <strong>State: </strong>
                                                </div>
                                                <div class="col-4">
                                                    {{ @$rec->vendor->vendor_state }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <strong>Organisation Type: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_company_type }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <strong>Organisation Website: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_website }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <strong>Import-Export License No.: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_iel_no }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <strong>VAT/GST No.: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_gstin }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <strong>TIN / TAN No.: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_tin }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <strong>Turn Over: </strong>
                                                </div>
                                                <div class="col-8">
                                                    ₹ {{ @$rec->vendor->vendor_turnover_inr }}, &nbsp; &nbsp; $ {{ @$rec->vendor->vendor_turnover_usd }}, &nbsp; &nbsp; € {{ @$rec->vendor->vendor_turnover_eur }}, &nbsp; &nbsp; £ {{ @$rec->vendor->vendor_turnover_pound }}
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-4" style="white-space: nowrap;">
                                                    <strong>Holder Name: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_bank_holder_name }}
                                                </div>
                                            </div>
                                           <div class="row">
                                                <div class="col-4" style="white-space: nowrap;">
                                                    <strong>Bank Name: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_bank_name }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4" style="white-space: nowrap;">
                                                    <strong>Bank Address: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_bank_addr }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4" style="white-space: nowrap;">
                                                    <strong>Bank A/C No.: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_bank_acc }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4" style="white-space: nowrap;">
                                                    <strong>Bank Swift Code: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_bank_swift_code }}
                                                </div>
                                            </div>
                                            <div class="">
                                                &nbsp;
                                                <br>
                                                <strong><u>Intermediate Bank Details</u> <br></strong>
                                            </div>
                                            <div class="row">
                                                <div class="col-4" style="white-space: nowrap;">
                                                    <strong>Bank Name: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_ib_name }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4" style="white-space: nowrap;">
                                                    <strong>Bank Address: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_ib_addr }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4" style="white-space: nowrap;">
                                                    <strong>Bank Swift Code: </strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ @$rec->vendor->vendor_ib_swift_code }}
                                                </div>
                                            </div>
                                        </td>

                                        <td>
                                            <div style="margin-bottom: 10px;">
                                                <a href="{{ url('user/add/'.$role.'/'.$rec->user_id) }}" title="Edit" data-toggle="tooltip"><i class="icon-pencil1"></i></a>
                                            </div>
                                            <div style="margin-bottom: 10px;">
                                            @if($rec->user_is_enabled == 'Y')
                                                <a href="{{ url('user/change_status/'.$role.'/'.$rec->user_id) }}"  class="btn btn-sm text-white btn-success mb-1">Active</a>
                                            @else
                                            <a href="{{ url('user/change_status/'.$role.'/'.$rec->user_id) }}" class="btn-danger btn btn-sm text-white mb-1">Deactive</a>
                                            @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                         </div>
                        {{ $records->links() }}
                        @else
                        <div class="no_records_found">
                          No records found yet.
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- <div class="col-sm-4">
                <div class="card warning">
                    <h3 class="card-title">Instructions</h3>
                    <div>
                        <ul>
                            <li><i class="icon-check-square"></i> Admin can view users details</li>
                            <li><i class="icon-check-square"></i> Admin can go to add user page</li>
                            <li><i class="icon-check-square"></i> Admin can go to edit user's information</li>
                        </ul>
                    </div>
                </div>
            </div> -->
        </div>
    </form>
</div>
