<style rel="css">
    #searchVendor {
        background-image: url(https://www.w3schools.com/css/searchicon.png);
        background-position: 10px 10px;
        background-repeat: no-repeat;
        height: 46px;
        padding-left: 40px;
    }
</style>
<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Buyer Products</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Add Buyer Products</li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <!-- <a href="{{ url('user/add/') }}" title="Add New" class="btn btn-default" data-toggle="tooltip"> <i class="icon-plus-circle"></i> Add New</a> -->
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <form method="post" id="vendor_list" onkeydown="return event.key != 'Enter';">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <span>Buyer Products</span>
                        <div class="ml-auto"></div>
                    </h3>
                    <div class="basic-info-two">
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                            {!! \Session::get('success') !!}
                        </div>
                        @endif
                        <div class="row">
                              <div class="col-sm-4">
                                <div class="form-group ui-widget" >
                                    <select name="" id="" class="form-control get_products select2">
                                        <option value="" >Select</option>
                                        @if(!empty($vendors))
                                          @foreach($vendors as $vendor)
                                            <option value="{{ $vendor->user_id }}">{{ $vendor->user_name }}</option>
                                          @endforeach
                                        @endif
                                    </select>
                                </div>
                              </div>
                        </div>
                        <div class="table-responsive table-header-fix" style="height: 600px;">
                            <table class="table table-bordered" id="table_Data" style="display:none;">
                                <thead>
                                    <tr>
                                        <!-- <th style="width: 50px;">
                                            <label class="animated-checkbox">
                                                <input type="checkbox" class="quote_checkall">
                                                <span class="label-text">All</span>
                                            </label>
                                        </th> -->
                                        <th class="text-center w-50">Products Name</th>
                                        <th class="text-center w-50">Products Drawing</th>
                                        <th class="text-center w-50">Products Data File</th>
                                        <th class="text-center w-50">Products Assembly</th>
                                        <th class="text-center w-50">Products Shipping Marks</th>
                                        <th class="text-center w-50">Products Barcode/Tag</th>
                                        <th class="text-center w-50">Products Care instruction</th>
                                        <th class="text-center w-50">Logo</th>
                                        <th class="text-center w-50">Products label</th>
                                        <th class="text-center w-50">Products Cartoon labelling</th>
                                        <th class="text-center w-50">Other Warning Lable safety Label</th>
                                        <th class="text-center" colspan="" style="text-align: center;text-transform: uppercase;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                            <h4 class="text-center" style="display:none;">No Data Found!</h4>
                         </div> 
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
  
  $(document).on('change','.get_products',function(){
        var id = $(this).val();
        if(id != ''){
                $.ajax({
                    url: "{{ route('get_products') }}",
                    method: 'post',
                    data: { id:id,"_token": "{{ csrf_token() }}" },
                    success: function(result){
                        if(result){
                            if(result.length>0){
                              $('#table_Data').css({'display':'block'});
                              $('.table-responsive h4').css({ 'display':'none' });
                            }else{
                              $('#table_Data').css({'display':'none'});
                              $('.table-responsive h4').css({ 'display':'block' });
                            }
                            var html='';
                            $("#table_Data tbody tr").remove();
                            for(var key in result) {
                            var value = result[key];
                                html+=`<tr>
                                        <td>
                                            <img src="${value.product_image}" style="max-width: 80px;">
                                            <strong>${value.product_name}</strong>
                                            <strong>${value.product_code}</strong>
                                            <input type="hidden" value="${value.product_id}" class="product_id">
                                            <input type="hidden" value="${value.bpro_uid}" class="user_id">
                                        </td>
                                        <td class="text-center">
                                            <div class="form-group">
                                                <label>Product Drawing</label>
                                                <input type="file" name="" class="form-control" value="" id="product-drawing" required="">
                                            </div>
                                        </td>
                                        <td>    
                                            <div class="form-group">
                                                <label>Product Data File</label>
                                                <input type="file" name="" class="form-control" value="" id="product-data-file" required="">
                                            </div>
                                            </td>
                                            <td>
                                            <div class="form-group">
                                                <label>Product Assembly</label>
                                                <input type="file" name="" class="form-control" value="" id="product-assembly" required="">
                                            </div>
                                            </td>
                                            <td>
                                            <div class="form-group">
                                                <label>Product Shipping Marks</label>
                                                <input type="file" name="" class="form-control" value="" id="product-shipping-marks" required="">
                                            </div>
                                            </td>
                                            <td>
                                            <div class="form-group">
                                                <label>Product Barcode/Tag</label>
                                                <input type="file" name="" class="form-control" value="" id="product-barcode" required="">
                                            </div>
                                            </td>
                                            <td>
                                            <div class="form-group">
                                                <label>Product Care Instruction</label>
                                                <input type="file" name="" class="form-control" value="" id="product-care-Instruction" required="">
                                            </div>
                                            </td>
                                            <td >
                                            <div class="form-group" style="width:110px;">
                                                <label>Logo</label>
                                                <input type="file" name="" class="form-control" value="" id="product-logo" required="">
                                            </div>
                                            </td>
                                            <td>
                                            <div class="form-group" style="width:110px;">
                                                <label>Product Label</label>
                                                <input type="file" name="" class="form-control" value="" id="product-label" required="">
                                            </div>
                                            </td>
                                            <td>
                                            <div class="form-group">
                                                <label>Product Cartoon labelling</label>
                                                <input type="file" name="" class="form-control" value="" id="product-cartoon-labelling" required="">
                                            </div>
                                            </td>
                                            <td>
                                            <div class="form-group">
                                                <label>Other Warning Label Safety Label</label>
                                                <input type="file" name="" class="form-control" value="" id="product-other-warning" required="">
                                            </div>
                                            </td>
                                            <td>
                                            <div class="form-group submit_buyer_btn">
                                                <a type="submit" class="btn text-white btn-primary form-control submit_buyer_products ">Save</a>
                                            </div>
                                            </td>
                                    </tr>
                                    `;
                            }
                            $("#table_Data tbody").append(html);
                        } else {

                        }
                    }
                });
            } else {
                toastr.error('id is required!');
            }
    });

    $(document).on('click','.submit_buyer_products',function(){
        var formData = new FormData();
        var $this = $(this);
        formData.append('product_drawing',$this.parents('tr').find('#product-drawing')[0].files[0]);
        formData.append('product_data_file',$this.parents('tr').find('#product-data-file')[0].files[0]);
        formData.append('product_assembly',$this.parents('tr').find('#product-assembly')[0].files[0]);
        formData.append('product_shipping_marks',$this.parents('tr').find('#product-shipping-marks')[0].files[0]);
        formData.append('product_barcode',$this.parents('tr').find('#product-barcode')[0].files[0]);
        formData.append('product_care_Instruction',$this.parents('tr').find('#product-care-Instruction')[0].files[0]);
        formData.append('product_logo',$this.parents('tr').find('#product-logo')[0].files[0]);
        formData.append('product_label',$this.parents('tr').find('#product-label')[0].files[0]);
        formData.append('product_cartoon_labelling',$this.parents('tr').find('#product-cartoon-labelling')[0].files[0]);
        formData.append('product_other_warning',$this.parents('tr').find('#product-other-warning')[0].files[0]);
        formData.append('product_id',$this.parents('tr').find('.product_id').val());
        formData.append('user_id',$this.parents('tr').find('.user_id').val());
        if(true){
                $.ajax({
                    url: "{{ route('upload_buyer_products') }}",
                    method: 'post',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(result){
                        if(result.data){
                            // alert('Data inserted successfully!');
                            window.location.href = "{{ route('view_buyer_products') }}";
                        }
                    },
                });
            } else {
                toastr.error('All Fields is required!');
            }
    })
</script>