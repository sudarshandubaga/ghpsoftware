<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Finish</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Finish</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <div class="card mb-5 mt-5">

        @if (Session::has('Success'))
            <div class="alert alert-success mb-5" role="alert">{!! Session::get('Success') !!}</div>
        @endif

        @if (Session::has('Danger'))
            <div class="alert alert-danger mb-5" role="alert">{!! Session::get('Danger') !!}</div>
        @endif

        <h3 class="card-title">Add Finish</h3>
        <form method="post" enctype="multipart/form-data">
            @csrf

            <div class="row">


                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>
                                Material
                            </label>
                        </div>

                        <div class="col-sm-9">
                            <div class="form-group">
                                <select name="record[material]" class="form-control" required=""
                                    onChange="LoadMaterialType(this.value)">
                                    <option value="">Select</option>
                                    @foreach ($AllMaterial as $ALM)
                                        <option @if (@$edit->material == $ALM->material_id) selected @endif
                                            value="{{ $ALM->material_id }}">{{ $ALM->material_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">


                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>
                                Material Type
                            </label>
                        </div>

                        <div class="col-sm-9">
                            <div class="form-group">
                                <select class="form-control" name="record[material_type]" required id="MaterialType">
                                    <option value="">Select</option>
                                    @foreach ($MaterialType as $MTY)
                                        <option @if (@$edit->material_type == $MTY->material_type_id) selected @endif
                                            value="{{ $MTY->material_type_id }}">{{ $MTY->material_type_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">

                    <div class="row">
                        <div class="col-sm-3">
                            <label>
                                Finish Code
                            </label>
                        </div>

                        <div class="col-sm-9">
                            <div class="form-group">
                                <input type="text" name="record[finish_no]" value="{{ @$edit->finish_no }}"
                                    class="form-control" required="">
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-3">
                            <label>
                                Finish Short Name
                            </label>
                        </div>

                        <div class="col-sm-9">
                            <div class="form-group">
                                <input type="text" name="record[finish_code]" value="{{ @$edit->finish_code }}"
                                    class="form-control" required="">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2"></div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary form-control">SAVE</button>
                    </div>
                </div>
            </div>
            <div class="row">


                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>
                                Finish Title
                            </label>
                        </div>

                        <div class="col-sm-9">
                            <div class="form-group">
                                <input type="text" name="record[finish_title]" value="{{ @$edit->finish_title }}"
                                    class="form-control" required="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>
                                Finish Process
                            </label>
                        </div>

                        <div class="col-sm-9">
                            <div class="form-group">
                                <input type="text" name="record[finish_process]" value="{{ @$edit->finish_process }}"
                                    class="form-control taginput">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">


                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>
                                Finish Image
                            </label>
                        </div>

                        <div class="col-sm-9">
                            <div class="form-group">
                                <input type="file" name="finish_image" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="card mb-5 mt-5">

        <h3 class="card-title">Search Finish</h3>
        <div class="row">
            <div class="col-9">
                <form>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>
                                        Name
                                    </label>
                                </div>

                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <input type="text" name="finish_title"
                                            value="{{ request('finish_title') }}" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary form-control">SEARCH</button>
                            </div>
                            <!-- <div class="form-group">
        <a href="{{ url('finish') }}" class="btn btn-default text-center form-control">ADD</a>
       </div> -->
                        </div>
                    </div>
                </form>

            </div>
            <div class="col-3">
                <div class="form-group">
                    <div class="col">

                        <form action="{{ route('exportFinish') }}" method="post">
                            {{ csrf_field() }}
                            <div class="">
                                <input type="hidden" name="finish_title" value="{{ request('finish_title') }}">

                                <input type="submit" class="btn btn-primary form-control" value="Export">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form method="post">
        <div class="card">
            <h3 class="card-title">
                <div class="mr-auto">View Finish</div>
                <a href="" class="ml-auto text-white" title="Remove Selected" data-toggle="tooltip">
                    <i class="icon-trash-o"></i>
                </a>
            </h3>
            @csrf
            @if (!$records->isEmpty())
                {{ $records->links() }}
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 50px;">
                                    <label class="animated-checkbox">
                                        <input type="checkbox" class="checkall">
                                        <span class="label-text"></span>
                                    </label>
                                </th>
                                <th style="width: 50px;">S.No.</th>
                                <th>Material</th>
                                <th>Mateiral Type</th>

                                <th>Finish Code</th>
                                <th>Finish Short Name</th>
                                <th>Finish Title</th>
                                <th>Process</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $sn = $records->firstItem(); @endphp
                            @foreach ($records as $rec)
                                <tr>
                                    <td>
                                        <label class="animated-checkbox">
                                            <input type="checkbox" name="check[]" value="{{ $rec->finish_id }}"
                                                class="check">
                                            <span class="label-text"></span>
                                        </label>
                                    </td>
                                    <td>{{ $sn++ }}</td>

                                    <td>{{ $rec->material_name }}</td>
                                    <td>{{ $rec->material_type_name }}</td>

                                    <td>{{ $rec->finish_no }}</td>
                                    <td>{{ $rec->finish_code }}</td>
                                    <td>{{ $rec->finish_title }}</td>
                                    <td>{{ $rec->finish_process }}</td>
                                    <td>

                                        @if ($rec->finish_image != '')
                                            <img src="{{ URL('FinishImage') }}/{{ $rec->finish_image }}"
                                                width="200" height="200">
                                        @endif

                                    </td>
                                    <td class="icon-cent">
                                        <a href="{{ url('finish/' . $rec->finish_id) }}" class="pencil"><i
                                                class="icon-pencil" title="Edit"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $records->links() }}
            @else
                <div class="no_records_found">
                    No records found yet.
                </div>
            @endif
        </div>
    </form>
</div>

<script>
    function LoadMaterialType(ID) {
        $("#MaterialType").attr('disabled', 'disabled').html($('<option />').val('').text('Loading'));
        $.ajax({
            url: "{{ URL('/') }}/ajax/LoadMaterialType",
            data: {
                id: ID
            },
            success: function(res) {
                $("#MaterialType").removeAttr('disabled').html($("<option />").text("Select Type").val(''));

                $.each(res.categories, function(i, row) {
                    $("#MaterialType").append($("<option />").text(row.material_type_name).val(row
                        .material_type_id));
                });

            }
        });
    }
</script>
