<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>View Logistics</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">View Logistics</li>
                </ul>
            </div>
            @if($profile->user_role == "logistic" || $profile->user_role == "admin")
            <div class="float-right">
                <a href="{{ URL('Logistics/add') }}" class="btn btn-default"> <i class="icon-plus"></i> Add Logistic</a>
            </div>
            @endif
        </div>
    </div>
</section>

<!--<form>-->
<!--<div class="card mb-5 mt-5">-->
<!--    <h3 class="card-title">Filter Product</h3>-->
<!--    <div class="row">-->
<!--        <div class="col">-->
<!--            <div class="form-group">-->
<!--                <label>PI Number</label>-->
<!--                <input type="text" name="SearchByPI" value="{{ @$_GET['SearchByPI'] }}" class="form-control">-->
<!--            </div>-->
<!--        </div>-->
        
<!--        <div class="col">-->
<!--            <div class="form-group">-->
<!--                <label>Buyer Name</label>-->
<!--                <input type="text" name="SearchByBuyer" value="{{ @$_GET['SearchByBuyer'] }}" class="form-control">-->
<!--            </div>-->
<!--        </div>-->
        
<!--        <div class="col">-->
<!--            <div class="form-group">-->
<!--                <label>GHP PO Number</label>-->
<!--                <input type="text" name="SearchByPO" value="{{ @$_GET['SearchByPO'] }}" class="form-control">-->
<!--            </div>-->
<!--        </div>-->
        
<!--        <div class="col">-->
<!--            <div class="form-group">-->
<!--                <label>Buyer Order Number</label>-->
<!--                <input type="text" name="SearchByGHP" value="{{ @$_GET['SearchByGHP'] }}" class="form-control">-->
<!--            </div>-->
<!--        </div>-->
        
<!--        <div class="col">-->
<!--            <div class="form-group">-->
<!--                <label>Status</label>-->
<!--                <select name="TrackStatus" class="form-control">-->
<!--                    <option value="">All</option>-->
<!--                    <option @if(@$_GET['TrackStatus'] == 1) selected @endif value="1">Open</option>-->
<!--                    <option @if(@$_GET['TrackStatus'] == 2) selected @endif value="2">Custom Clearance Process</option>-->
<!--                    <option @if(@$_GET['TrackStatus'] == 3) selected @endif value="3">Shipped</option>-->
<!--                </select>-->
<!--            </div>-->
<!--        </div>-->
        
<!--        <div class="col">-->
<!--            <div class="form-group">-->
<!--                <label>&nbsp;</label>-->
<!--                <button type="submit" class="btn btn-primary form-control">Filter</button>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--</form>-->

<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter Product</h3>    
    <div class="row">
        <div class="col-10">
        <form>
            <div class="row">
                <!-- <div class="col">
                    <div class="form-group">
                        <label>Customer Name</label>
                        <input type="text" name="SearchByName" value="{{ @$_GET['SearchByName'] }}" class="form-control">
                    </div>
                </div> -->
                <div class="col">
                    <div class="form-group">
                        <label>PI Number</label>
                        <input type="text" name="SearchByPI" value="{{ @$_GET['SearchByPI'] }}" class="form-control">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>Buyer Name</label>
                        <input type="text" name="SearchByBuyer" value="{{ @$_GET['SearchByBuyer'] }}" class="form-control">
                    </div>
                </div>
                
                <div class="col">
                    <div class="form-group">
                        <label>GHP PO Number</label>
                        <input type="text" name="SearchByPO" value="{{ @$_GET['SearchByPO'] }}" class="form-control">
                    </div>
                </div>
                
                <div class="col">
                    <div class="form-group">
                        <label>Buyer Order Number</label>
                        <input type="text" name="SearchByGHP" value="{{ @$_GET['SearchByGHP'] }}" class="form-control">
                    </div>
                </div>
                
                <div class="col">
                    <div class="form-group">
                        <label>Status</label>
                        <select name="TrackStatus" class="form-control">
                            <option value="">All</option>
                            <option @if(@$_GET['TrackStatus'] == 1) selected @endif value="1">Open</option>
                            <option @if(@$_GET['TrackStatus'] == 2) selected @endif value="2">Custom Clearance Process</option>
                            <option @if(@$_GET['TrackStatus'] == 3) selected @endif value="3">Shipped</option>
                        </select>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-primary form-control">Filter</button>
                    </div>
                </div>
                
            </div>
            
        </form>
        </div>
        <div class="col-2">
        <form action="{{ route('exportLogistic') }}" method="post">
            {{ csrf_field() }}          
            <div class="" >
                <input type="hidden" name="SearchByPI" value="{{request('SearchByPI')}}">
                <input type="hidden" name="SearchByBuyer" value="{{request('SearchByBuyer')}}">
                <input type="hidden" name="SearchByPO" value="{{request('SearchByPO')}}">
                <input type="hidden" name="SearchByGHP" value="{{request('SearchByGHP')}}">
                <input type="hidden" name="TrackStatus" value="{{request('TrackStatus')}}">
                <label>&nbsp;</label>
                <input type="submit" class="btn btn-primary form-control" value="Export">
            </div>       
        </form>
        </div>
    </div>
</div>

<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">List Logistics</div>
                    </h3>
                    <div class="basic-info-two">
                        
                        <!--<div class="bg-primary" style="float: left; width: 15px; margin-right: 5px; height: 15px;background-color: #cce5ff!important"></div>-->
                        <!--<div style="float: left; margin-right: 30px;">Open </div>-->
                        
                        <!--<div class="bg-danger" style="float: left; width: 15px; margin-right: 5px; height: 15px;"></div>-->
                        <!--<div style="float: left; margin-right: 30px;">Custom Clearance Process</div>-->
                        
                        <!--<div class="bg-success" style="float: left; width: 15px; margin-right: 5px; height: 15px;background-color: #d4edda!important;"></div>-->
                        <!--<div style="float: left; margin-right: 30px;">Shipped</div>-->
                        <!--<div class="clearfix"></div>-->
                        <hr>

                        @if(!$records->isEmpty())
                        <div class="table-responsive" style="height: 500px; border: #000 solid 0px;">
                            <table class="table table-bordered table-hover table-header-fix">
                                <thead>
                                    <tr>
                                        <th style="width: 80px;">Sr. No.</th> 
                                          <th>Actions</th>
                                        <th>Booking Invoice<br> No</th>
                                        <th style="min-width: 200px;">Buyer Name</th>
                                        <th style="min-width: 130px;">PI Number</th>
                                        <th style="min-width: 130px;">PO Number</th>
                                        <th style="min-width: 200px;">GHP Order No.</th>
                                        <th>CBM</th> 
                                        <th style="min-width: 200px;">Vendor</th>
                                    <th>Container Size</th>
                                    <th style="min-width: 120px;">Order Date</th>
                                    <th style="min-width: 120px;">Agreed ETD</th>
                                    <th>Forwarder Name</th>
                                    <th>Booking Request Received Date</th>
                                    <th>P/U Location</th>
                                    <th>H/O Location</th>
                                    <th>Planned Stuffing Date</th>
                                    <th>DO Shared With Vendor</th>
                                    <th>Container Pickup Date</th>
                                    <th style="min-width: 100px;">Container Nr.</th>
                                    <th>Stuffing Date</th>
                                    <th>SI Submission  by Shipper</th>
                                    <th>Container Dispatch Form ICD</th>
                                    <th>Container Gate IN at Port</th>
                                    <th>Draft BL shared with Shipper</th>
                                    <th>Draft BL approved with Shipper</th>
                                    <th>Debit Note shared with Shipper</th>
                                    <th>Debit Note Payment Detail received Date</th>
                                    <th>Remark for Vendor</th>
                                    <th>Booking Placed Date</th>
                                    <th>DO Received Date</th>
                                    <th>Booking Nr.</th>
                                    <th style="min-width: 150px;">Shipping Line</th>
                                    <th>DO Expiry Date</th>
                                    <th>Planned ETD DAte</th>
                                    <th>Planned ETA Date</th>
                                    <th style="min-width: 120px;">Port of Discharge</th>
                                    <th>2nd Rvsd ETD</th>
                                    <th>Do Received Date 2</th>
                                    <th>Booking Nr. 2</th>
                                    <th style="min-width: 150px;">Shipping Line 2</th>
                                    <th>Planned ETD Date 2</th>
                                    <th>Planned ETA Date 2</th>
                                    <th>SI Share With Forwarder</th>
                                    <th>Draft BL Received</th>
                                    <th>Draft BL Approved to Forwarder</th>
                                    <th>Debit Note Revd From Forwarder</th>
                                    <th>Debit Note Payment Detail</th>
                                    <th>Vessel Sailing Date(ATD)</th>
                                    <th>Vessel Sailing ETA</th>
                                    <th>Final BL Rcvd Date</th>
                                    <th>BL Surrender request date</th>
                                    <th>BL Surrender copy rcvd</th>
                                    <th style="min-width: 200px;">Remark Forwarder</th>
                                    <th>Draft Bl shared with Buyer</th>
                                    <th>Draft Bl Conf Rcvd From Buyer</th>
                                    <th>Final Bl Shared with Buyer</th>
                                    <th>Payment Received</th>
                                    <th>BL Surrender Copy Shared With</th>
                                    <th>Remarks for Buyer</th>
                                    @if($profile->user_role == "logistic" || $profile->user_role == "admin")
                                    <th>Actions</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @php $sn = $records->firstItem(); @endphp
                                @foreach($records as $rec)
                                    @php
                                    $GetBuyerName = \App\Models\UserModel::where('user_id', $rec->exs_logistics_buyer)->first();
                                    $GetPIData = \App\Models\PIModel::where("pi_id", $rec->exs_logistics_pi)->first();
                                    $GetPOData = \App\Models\POModel::where("po_id", $rec->exs_logistics_po)->first();
                                    $GetForwarder = \DB::table('forwarder')->where('forwarder_id', $rec->ForwarderName)->first();
                                    $ShippingLIne1 = DB::table('shipping_line')->where('shipping_line_id', $rec->ShippingLine)->first();
                                    $ShippingLIne2 = DB::table('shipping_line')->where('shipping_line_id', $rec->ShippingLine2)->first();
                                    
                                    $Color = "";
                                    if($rec->track_status == 1){
                                        $Color = "alert alert-primary";
                                    }
                                    
                                    if($rec->track_status == 2){
                                        $Color = "alert alert-danger";
                                    }
                                    
                                    if($rec->track_status == 3){
                                        $Color = "alert alert-success";
                                    }
                                    @endphp
                                    <tr class="{{ $Color }}">
                                    <td style="width: 80px;">{{ $sn++ }}</td>
                                     @if($profile->user_role == "logistic" || $profile->user_role == "admin")
                                    <td>
                                        <div style="margin-bottom: 10px;">
                                            <a href="{{ url('Logistics/add/'.$rec->exs_logistics_id) }}" title="Edit" data-toggle="tooltip"><i class="icon-pencil1"></i></a>
                                        </div>
                                    </td>
                                    @endif
                                    <td>{{ @$rec->exs_logistics_booking_no}}</td>
                                    <td>{{ $rec->exs_logistics_buyer }}</td>
                                    <td>{{ $rec->exs_logistics_pi }}</td>
                                    <td>{{ $rec->exs_logistics_po }}</td>
                                    <td>{{ $rec->GHPOrderNo }}</td>
                                    <td>{{ $rec->CBM }}</td>
                                    <td>{{ $rec->Vendor }}</td>
                                    <td>{{ $rec->container_size }}</td>
                                    <td>
                                        @if($rec->OrderDate != "")
                                        {{ date("d-M-Y", strtotime($rec->OrderDate)) }}
                                        @endif
                                    </td> 
                                    <td>
                                        @if($rec->AgreedETD != "")
                                        {{ date("d-M-Y", strtotime($rec->AgreedETD)) }}
                                        @endif 
                                        
                                    </td>
                                    <td>{{ @$GetForwarder->forwarder_company_name }}</td>
                                    <td>
                                        @if($rec->BookingRequestReceivedDate != "")
                                        {{ date("d-M-Y", strtotime($rec->BookingRequestReceivedDate)) }}
                                        @endif 
                                    </td>
                                    <td>{{ $rec->PULocation }}</td>
                                    <td>{{ $rec->HOLocation }}</td>
                                    <td>
                                        @if($rec->PlannedStuffingDate != "")
                                        {{ date("d-M-Y", strtotime($rec->PlannedStuffingDate)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->DOSharedWithVendor != "")
                                        {{ date("d-M-Y", strtotime($rec->DOSharedWithVendor)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->ContainerPickupDate != "")
                                        {{ date("d-M-Y", strtotime($rec->ContainerPickupDate)) }}
                                        @endif
                                    </td>
                                    <td>{{ $rec->ContainerNr }}</td>
                                    <td>
                                        @if($rec->StuffingDate != "")
                                        {{ date("d-M-Y", strtotime($rec->StuffingDate)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->SISubmissionDtbyVendor != "" && $rec->SISubmissionDtbyVendor != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->SISubmissionDtbyVendor)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->Dispatch != "" && $rec->Dispatch != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->Dispatch)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->GateIN != "" && $rec->GateIN != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->GateIN)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->DraftBLsharedwithshipper != "" && $rec->DraftBLsharedwithshipper != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->DraftBLsharedwithshipper)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->DraftBLapprovedwithshipper != "" && $rec->DraftBLapprovedwithshipper != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->DraftBLapprovedwithshipper)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->DebitNotesharedwithvendor != "" && $rec->DebitNotesharedwithvendor != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->DebitNotesharedwithvendor)) }}
                                        @endif 
                                    </td>
                                    <td>
                                        @if($rec->DebitNotePaymentDetailreceivedDate != "" && $rec->DebitNotePaymentDetailreceivedDate != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->DebitNotePaymentDetailreceivedDate)) }}
                                        @endif 
                                        
                                    </td>
                                    <td>
                                        {{ $rec->RemarkforVendor }}  
                                    </td>
                                    <td>
                                        @if($rec->BookingPlacedDate != "" && $rec->BookingPlacedDate != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->BookingPlacedDate)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->DOReceivedDate != "" && $rec->DOReceivedDate != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->DOReceivedDate)) }}
                                        @endif
                                    </td>
                                    <td>{{ $rec->BookingNr }}</td>
                                    <td style="white-space: nowrap;">{{ @$ShippingLIne1->shipping_line_name }}</td>
                                    <td>
                                        @if($rec->DOExpiryDate != "" && $rec->DOExpiryDate != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->DOExpiryDate)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->PlannedETDDAte != "" && $rec->PlannedETDDAte != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->PlannedETDDAte)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->PlannedETADate != "" && $rec->PlannedETADate != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->PlannedETADate)) }}
                                        @endif
                                    </td>
                                    @if($rec =="")
                                        <td>-</td>
                                    @else
                                        <td>{{$rec->PortofDischarge}}</td>
                                    @endif()
                                    <!--<td>-->
                                    <!--    @if($rec->PortofDischarge != "")-->
                                    <!--    {{ date("d-M-Y", strtotime($rec->PortofDischarge)) }}-->
                                    <!--    @endif-->
                                    <!--</td>-->
                                    <td>{{ $rec->T2ndRvsdETD }}</td>
                                    <td>
                                        @if($rec->DoReceivedDate2 != "" && $rec->DoReceivedDate2 != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->DoReceivedDate2)) }}
                                        @endif 
                                    </td>
                                    <td>{{ $rec->BookingNr2 }}</td>
                                    <td>{{ @$ShippingLIne2->shipping_line_name }}</td>
                                    <td>
                                        @if($rec->PlannedETDDate2 != "" && $rec->PlannedETDDate2 != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->PlannedETDDate2)) }}
                                        @endif  
                                    </td>
                                    <td>
                                        @if($rec->PlannedETADate2 != "" && $rec->PlannedETADate2 != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->PlannedETADate2)) }}
                                        @endif 
                                    </td>
                                    <td>
                                        @if($rec->SIShareWithForwarder != "" && $rec->SIShareWithForwarder != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->SIShareWithForwarder)) }}
                                        @endif 
                                    </td>
                                    <td>
                                        @if($rec->DraftBLReceived != "" && $rec->DraftBLReceived != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->DraftBLReceived)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->DraftBLApprovedbyForwarder != "" && $rec->DraftBLApprovedbyForwarder != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->DraftBLApprovedbyForwarder)) }}
                                        @endif 
                                    </td>
                                    <td>
                                        @if($rec->DebitNoteRevdFromForwarder != "" && $rec->DraftBLApprovedbyForwarder != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->DebitNoteRevdFromForwarder)) }}
                                        @endif  
                                    </td>
                                    <td>
                                        @if($rec->DebitNotePaymentDetail != "" && $rec->DebitNotePaymentDetail != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->DebitNotePaymentDetail)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->VesselSailingETD != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->VesselSailingETD)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->VesselSailingETA != "")
                                        {{ date("d-M-Y", strtotime($rec->VesselSailingETA)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->FinalBLRcvdDate != "")
                                        {{ date("d-M-Y", strtotime($rec->FinalBLRcvdDate)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->BLSurrenderrequestdate != "")
                                        {{ date("d-M-Y", strtotime($rec->BLSurrenderrequestdate)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->BLSurrendercopyrcvd != "")
                                        {{ date("d-M-Y", strtotime($rec->BLSurrendercopyrcvd)) }}
                                        @endif
                                    </td>
                                    <td>{{ $rec->RemarkForwarder }}</td>
                                    <td>
                                        @if($rec->DraftBlsharedwithLIZ != "")
                                        {{ date("d-M-Y", strtotime($rec->DraftBlsharedwithLIZ)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->DraftBlConfRcvdFromLIZ != "" && $rec->DraftBlConfRcvdFromLIZ != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->DraftBlConfRcvdFromLIZ)) }}
                                        @endif 
                                    </td>
                                    <td>
                                        @if($rec->FinalBlSharedwithLIZ != "" && $rec->FinalBlSharedwithLIZ != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->FinalBlSharedwithLIZ)) }}
                                        @endif  
                                    </td>
                                    <td>
                                        @if($rec->PaymentReceived != "")
                                        {{ date("d-M-Y", strtotime($rec->PaymentReceived)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($rec->BLSurrenderCopySharedWith != "")
                                        {{ date("d-M-Y", strtotime($rec->BLSurrenderCopySharedWith)) }}
                                        @endif
                                    </td>
                                    <td>{{ $rec->RemarksforLIZ }}</td>
                                    
                                    @if($profile->user_role == "logistic" || $profile->user_role == "admin")
                                    <td>
                                        <div style="margin-bottom: 10px;">
                                            <a href="{{ url('Logistics/add/'.$rec->exs_logistics_id) }}" title="Edit" data-toggle="tooltip"><i class="icon-pencil1"></i></a>
                                        </div>
                                    </td>
                                    @endif
                                    
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                         </div>
                        {{ $records->appends(request()->query())->links() }}
                        @else
                        <div class="no_records_found">
                          No records found yet.
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
