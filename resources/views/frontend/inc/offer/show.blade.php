@extends('frontend.layouts.app')

@section('main_section')
    <section class="page-header mb-3">
        <div class="container-fluid">
            <div class="float-right">
                <a href="{{ route('offer.create') }}" class="btn btn-default"> <i class="icon-plus"></i> Add Offer</a>
            </div>
            <div class="clearfix">
                <div>
                    <h1>Offer</h1>
                    <ul class="breadcrumbs clearfix">
                        <li><a href="">Dashboard</a></li>
                        <li class="active">Offer</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    {{ Form::open(['url' => route('offer.store'), 'method' => 'post', 'class' => 'container-fluid']) }}
    <!-- <div class="card mb-5 mt-5">
            <h3 class="card-title">Add New Offer</h3>
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div class="col-sm-3"><label>Name</label></div>
                        <div class="col-sm-9">
                            <div class="form-group">
                                {{ Form::text('name', null, ['placeholder' => 'Enter Name', 'class' => 'form-control', 'required' => 'required']) }}
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-3">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary form-control">Save</button>
                    </div>
                </div>
            </div>
        </div> -->
    {{ Form::close() }}

    <!--<form method="post">-->

    <div class="container-fluid">
        <div class="card mt-5">
            <h3 class="card-title">
                <div class="mr-auto">Offer Details</div>
            </h3>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th>Offer Number</th>
                            <td>{{ sprintf('%06d', $offer->id) }}</td>
                        </tr>
                        <tr>
                            <th>Buyer Name</th>
                            <td>{{ $offer->buyer_name }}</td>
                        </tr>
                        <tr>
                            <th>Refered By</th>
                            <td>{{ $offer->refered_by }}</td>
                        </tr>
                        <tr>
                            <th>Production Lead Time</th>
                            <td>{{ $offer->lead_time }}</td>
                        </tr>
                        <tr>
                            <th>Offer Price Terms</th>
                            <td>{{ $offer->terms }}</td>
                        </tr>
                        <tr>
                            <th>Remarks</th>
                            <td>{{ $offer->remarks }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="card mb-5">
            <h3 class="card-title">
                <div class="mr-auto">Products</div>
                <!-- <a href="" class="ml-auto text-white" title="Remove Selected" data-toggle="tooltip">
                            <i class="icon-trash-o"></i>
                        </a> -->
            </h3>
            <section class="add-block">
                <div>
                    <div class="basic-info-two">
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                {!! \Session::get('success') !!}</li>
                            </div>
                        @endif
                        @if (!$offer->offer_products->isEmpty())
                            <table class="table table-bordered table-hover table-header-fix">
                                <thead>
                                    <tr>
                                        <!-- <th></th> -->
                                        <th style="width: 80px;">Sr. No.</th>
                                        <th colspan="2">Item Description</th>
                                        <th>Code</th>
                                        <th>MRP</th>
                                        <th>Margin</th>
                                        <th>Offer Price</th>
                                        <!-- <th>Qty</th>
                                            <th>Subtotal</th> -->
                                        <th>MOQ</th>
                                        <th>Remarks</th>
                                        <!-- <th>Actions</th> -->
                                    </tr>
                                </thead>

                                <tbody>
                                    @php $sn = 1; @endphp
                                    @foreach ($offer->offer_products as $rec)
                                        <tr>
                                            <!-- <td>
                                                    <label class="animated-checkbox">
                                                        <input type="checkbox" name="check[]" value="{{ $rec->product_id }}" class="check">
                                                        <span class="label-text"></span>
                                                    </label>
                                                </td> -->
                                            <td>{{ $sn++ }}.</td>
                                            <td style="width: 50px;">
                                                <img src="{{ url('imgs/products/' . $rec->product_image) }}"
                                                    alt="${row.product_name}" class="image-thumb">
                                            </td>
                                            <td>{{ $rec->product_name }}</td>
                                            <td>{{ $rec->product_code }}</td>
                                            <td>{{ $rec->product_mrp }}</td>
                                            <td>{{ $rec->pivot->margin }}%</td>
                                            <td>{{ $rec->pivot->price }}</td>
                                            <!--  <td>{{ $rec->pivot->qty }}</td>
                                                <td>{{ $rec->pivot->price * $rec->pivot->qty }}</td> -->
                                            <td>{{ $rec->pivot->moq }}</td>
                                            <td>{{ $rec->pivot->remarks }}</td>
                                            <!-- <td>
                                                    <div class="dropdown dropleft">
                                                        <button class="btn btn-link" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="icon-ellipsis-v"></i>
                                                        </button>
                                                        {{ Form::open(['url' => route('offer.destroy', [$rec->id]), 'method' => 'delete', 'id' => 'deleteForm']) }}
                                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                <a class="dropdown-item" href="{{ route('offer.edit', [$rec->id]) }}">
                                                                    <i class="icon-pencil"></i> Edit
                                                                </a>
                                                                <button type="submit" class="dropdown-item"><i class="icon-trash"></i> Delete</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </td> -->
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                    </div>
                @else
                    <div class="no_records_found">
                        No records found yet.
                    </div>
                    @endif
                </div>
            </section>
        </div>
    </div>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>
        $(function() {
            $(document).on('submit', '#deleteForm', function(e) {
                if (!confirm("Are you sure to delete this record?")) {
                    e.preventDefault();
                }
            });
        });
    </script>
@endsection
