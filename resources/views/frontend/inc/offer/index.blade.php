@extends('frontend.layouts.app')

@section('main_section')
    <section class="page-header mb-3">
        <div class="container-fluid">
            <div class="float-right">
                <a href="{{ route('offer.create') }}" class="btn btn-default"> <i class="icon-plus"></i> Add Offer</a>
            </div>
            <div class="clearfix">
                <div >
                    <h1>Offer</h1>
                    <ul class="breadcrumbs clearfix">
                        <li><a href="">Dashboard</a></li>
                        <li class="active">Offer</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    {{ Form::open(['url' => route('offer.store'), 'method' => 'post', 'class' => 'container-fluid']) }}
    <!-- <div class="card mb-5 mt-5">
        <h3 class="card-title">Add New Offer</h3>
        <div class="row">
            <div class="col">
                <div class="row">
                    <div class="col-sm-3"><label>Name</label></div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            {{ Form::text('name', null, ['placeholder' => 'Enter Name', 'class' => 'form-control', 'required' => 'required']) }}
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-3">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary form-control">Save</button>
                </div>
            </div>
        </div>
    </div> -->
    {{ Form::close() }}
        
    <!--<form method="post">-->
        
        <div class="container-fluid">
            <div class="card mb-5 mt-5">
                <h3 class="card-title">
                    <div class="mr-auto">View Offers</div>
                    <!-- <a href="" class="ml-auto text-white" title="Remove Selected" data-toggle="tooltip">
                        <i class="icon-trash-o"></i>
                    </a> -->
                </h3>
                <section class="add-block">
                    <div>
                        <div class="basic-info-two">
                            @if (\Session::has('success'))
                                <div class="alert alert-success">
                                    {!! \Session::get('success') !!}</li>
                                </div>

                                @endif
                                @if(!$offers->isEmpty())
                                {{ $offers->appends($_GET)->links() }}
                                <table class="table table-bordered table-hover table-header-fix">
                                    <thead>
                                    <tr>
                                        <!-- <th></th> -->
                                        <th style="width: 80px;">Sr. No.</th>
                                        <th>Buyer Name</th>
                                        <th>Refered By</th>
                                        <th>Date</th>
                                        <th></th>
                                        <!-- <th>Actions</th> -->
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @php $sn = $offers->firstItem(); @endphp
                                    @foreach($offers as $rec)
                                        <tr>
                                            <!-- <td>
                                                <label class="animated-checkbox">
                                                    <input type="checkbox" name="check[]" value="{{ $rec->product_id  }}" class="check">
                                                    <span class="label-text"></span>
                                                </label>
                                            </td> -->
                                            <td>{{ $sn++ }}.</td>
                                            <td>{{ $rec->buyer_name }}</td>
                                            <td>{{ $rec->refered_by }}</td>
                                            <td>{{ date('d M Y',  strtotime($rec->created_at)) }}</td>
                                            <td>
                                                <a href="{{ route('offer.show', $rec->id) }}">View Products</a><br>
                                                <a href="{{ route('offer.print', $rec->id) }}" target="_blank">Print</a>
                                            </td>
                                            <!-- <td>
                                                <div class="dropdown dropleft">
                                                    <button class="btn btn-link" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="icon-ellipsis-v"></i>
                                                    </button>
                                                    {{ Form::open(['url' => route('offer.destroy', [$rec->id]), 'method' => 'delete', 'id' => 'deleteForm']) }}
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item" href="{{ route('offer.edit', [$rec->id]) }}">
                                                                <i class="icon-pencil"></i> Edit
                                                            </a>
                                                            <button type="submit" class="dropdown-item"><i class="icon-trash"></i> Delete</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </td> -->
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{ $offers->appends($_GET)->links() }}
                            @else
                            <div class="no_records_found">
                            No records found yet.
                            </div>
                            @endif
                        </div>
                </section>
            </div>
        </div>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <script>
            $(function () {
                $(document).on('submit', '#deleteForm', function (e) {
                    if(!confirm("Are you sure to delete this record?")) {
                        e.preventDefault();
                    }
                });
            });
        </script>
@endsection