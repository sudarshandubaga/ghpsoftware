@extends('frontend.layouts.app')

@section('extra_styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.min.css"
        integrity="sha512-ZKX+BvQihRJPA8CROKBhDNvoc2aDMOdAlcm7TUQY+35XYtrd3yh95QOOhsPDQY9QnKE0Wqag9y38OIgEvb88cA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection

@section('main_section')
    <section class="page-header mb-3">
        <div class="container-fluid">
            <div class="clearfix">
                <div>
                    <h1>Create Offer</h1>
                    <ul class="breadcrumbs clearfix">
                        <li><a href="">Dashboard</a></li>
                        <li class="active">Create Offer</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    {{ Form::open(['url' => route('offer.store'), 'method' => 'post', 'class' => 'container-fluid']) }}
    <div class="row">
        <div class="col-sm-9">
            <div class="card">
                <h3 class="card-title">Create Offer</h3>
                <div class="row">
                    <div class="col-4">
                        <div>
                            <div class="form-group">
                                {{ Form::label('Customer Name') }}
                                {{ Form::text('buyer_name', null, ['placeholder' => 'Enter Customer Name', 'class' => 'form-control', 'required' => 'required']) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div>
                            <div class="form-group">
                                {{ Form::label('Refered By') }}
                                {{ Form::select('refered_by', ['ghp' => 'GHP', 'fng' => 'FNG', 'phi' => 'PHI'], null, ['placeholder' => 'Select Reference From', 'class' => 'form-control', 'required' => 'required']) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div>
                            <div class="form-group">
                                {{ Form::label('Price Validity') }}
                                {{ Form::text('price_validity', null, ['placeholder' => 'Price Validity Period (In Days)', 'class' => 'form-control', 'required' => 'required']) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div>
                            <div class="form-group">
                                {{ Form::label('Production Lead Time') }} (Weeks Ex Factory)
                                {{ Form::text('lead_time', null, ['placeholder' => 'Production Lead Time', 'class' => 'form-control', 'required' => 'required']) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div>
                            <div class="form-group">
                                {{ Form::label('Offer Price Terms') }}
                                {{ Form::select('terms', ['fob-mundra,in' => 'FOB-Mundra,IN', 'exw' => 'EXW', 'cif' => 'CIF'], null, ['class' => 'form-control', 'required' => 'required']) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('remarks') }}
                    {{ Form::textarea('remarks', null, ['placeholder' => 'Remarks', 'class' => 'form-control']) }}
                </div>
            </div>
            <div class="card">
                <h3 class="card-title">Offer Products</h3>
                <div class="table-responsive cartItems">

                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div>
                <button type="submit" class="btn btn-primary btn-block px-5">Save</button>
            </div>
            <div class="card mt-3">
                <h3 class="card-title">Select Products</h3>
                <div class="mb-3">
                    {{ Form::search('search', null, ['placeholder' => 'Search', 'class' => 'form-control', 'id' => 'search']) }}
                </div>
                <div class="product-list">

                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}
@endsection

@section('extra_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js"
        integrity="sha512-k2GFCTbp9rQU412BStrcD/rlwv1PYec9SNrkbQlo6RZCf75l6KcC3UwDY8H5n5hl4v77IDtIPwOk9Dqjs/mMBQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        function fetchAllProducts() {
            var search = $('#search').val();
            $('.product-list').html(`
                <div class="text-center">
                    <div class="spinner-border" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            `);
            $.ajax({
                url: "{{ route('api.product.index') }}",
                data: {
                    search
                },
                success: function(res) {
                    $('.product-list').html('');
                    res.forEach(function(row) {
                        // var cartHtml = row.cart && row.cart.id ? `
                    //     <div class="input-group">
                    //         <div class="input-group-prepend">
                    //             <button 
                    //                 class="btn btn-sm btn-primary rounded-left qty-minus" type="button"
                    //             >
                    //                 <i class="icon-minus"></i>
                    //             </button>
                    //         </div>
                    //         <input class="form-control" style="width: 50px" data-id="${row.cart.id}" value="${row.cart.qty}" type="number" readonly>
                    //         <div class="input-group-append">
                    //             <button
                    //                 class="btn btn-sm btn-primary rounded-right qty-plus" type="button"
                    //             >
                    //                 <i class="icon-plus"></i>
                    //             </button>
                    //         </div>
                    //     </div>
                    // ` : `
                    //     <button type="button" class="btn btn-sm btn-primary addToOfferBtn rounded btn-block" data-id="${row.product_id}">Add</button>
                    // `;
                        var cartHtml = `
                            <button type="button" class="btn btn-sm btn-primary addToOfferBtn rounded btn-block" data-id="${row.product_id}">Add</button>
                        `;
                        var html = `
                            <div class="row mb-2">
                                <div class="col-8">
                                    <div class="d-flex">
                                        <div>
                                            <a href="{{ url('imgs/products/${row.product_image}') }}" data-lightbox="image-${row.product_id}" data-title="${row.product_name}">
                                                <img src="{{ url('imgs/products/${row.product_image}') }}" alt="${row.product_name}" class="image-thumb">
                                            </a>
                                        </div>
                                        <div class="pl-3">
                                            <strong>${row.product_name}</strong><br>
                                            ${row.product_code} <big>$${row.product_mrp}</big>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4 product-list-btn" data-product=${row.product_id}>
                                    ${cartHtml}
                                </div>
                            </div>
                        `;
                        $('.product-list').append(html);
                    });
                }
            });
        }

        function deleteFromCart(id) {
            if (confirm('Are you sure to delete from offer product?')) {
                $.ajax({
                    url: `{{ route('api.cart.index') }}/${id}`,
                    method: 'DELETE',
                    success: function(res) {
                        cartIntoTable(res);
                    }
                });
            }
        }

        function cartIntoTable(cartItems) {
            if (!cartItems.length) {
                $('.cartItems').html(`
                    <div>No cart items added yet.</div>
                `);
            } else {
                var html = '';

                cartItems.forEach((row, i) => {
                    var new_price = (parseFloat(row.product.product_mrp) + parseFloat(row.product.product_mrp * row
                        .margin / 100)).toFixed(2);
                    var subtotal = parseFloat(new_price * row.qty).toFixed(2);
                    // var total = parseFloat(subtotal + (subtotal * row.margin / 100)).toFixed(2);
                    html += `
                        <tr data-id="${row.id}">
                            <td>${i + 1}</td>
                            <td>
                                <img src="{{ url('imgs/products/${row.product.product_image}') }}" class="image-thumb" />
                            </td>
                            <td>
                                <strong>${row.product.product_name}</strong><br> Code : 
                                ${row.product.product_code}<br> Type : ${row.product.product_type}<br>L ${row.product.product_l_cm} x W ${row.product.  product_w_cm} x H ${row.product.product_h_cm}
                                <div>
                                    <textarea placeholder="Remarks" class="form-control cart-remarks">${String(row.remarks) != 'null' ? row.remarks : ''}</textarea>
                                </div>
                            </td>
                            <td class="old_price">${row.product.product_mrp}</td>
                            <td>
                                <input class="form-control cart-margin" min="0" name="margin[1]" type="number" value="${row.margin}">
                            </td>
                            <td class="price">${new_price}</td>
                              
                            <td style="width: 200px;">
                                <input type="number" min="1" class="form-control cart-moq" placeholder="MOQ" value="${row.moq}" />
                            </td>
                            <td>
                                <a href="javascript: deleteFromCart(${row.id})">&times;</a>
                            </td>
                        </tr>
                    `;
                    /*
                        <td>
                            <input class="form-control cart-qty" min="1" name="qty[1]" type="number" value="${row.qty}">
                          </td>
                        <td class="subtotal">${subtotal}</td>
                    */
                });

                $('.cartItems').html(`
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th colspan="2">Item Description</th>
                                <th>Price<br> (USD)</th>
                                <th style="width: 80px;">
                                    {{ Form::label('margin', 'Price Variation (%)') }}
                                    {{ Form::number('margin', 0, ['class' => 'form-control', 'min' => 0]) }}
                                </th>
                                <th>Offer Price<br> (USD)</th>
                                
                                <th>MOQ</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            ${html}
                        </tbody>
                    </table>
                `);

                /*

                    <th style="width: 100px;">Qty</th>
                                <th>Subtotal</th>
                */
            }
        }

        function fetchCart() {
            $('.cartItems').html(`
                <div class="text-center">
                    <div class="spinner-border" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            `);
            $.ajax({
                url: "{{ route('api.cart.index') }}",
                data: {
                    type: 'offer',
                    userId: "{{ auth()->user()->user_id }}"
                },
                success: function(res) {
                    cartIntoTable(res);
                }
            });
        }

        function updateCart(cartId, data) {
            $.ajax({
                url: `{{ route('api.cart.index') }}/${cartId}`,
                method: 'PUT',
                data,
                success: function(res) {

                }
            });
        }
        $(function() {
            setTimeout(function() {
                fetchAllProducts();
                fetchCart();
            }, 100);
            $(window).on('load', function() {


            });

            $(document).on('keyup change', '.cart-qty, .cart-margin, .cart-moq, .cart-remarks', function() {
                var parent = $(this).closest('tr');
                var qty = parent.find('.cart-qty').val();
                var moq = parent.find('.cart-moq').val();
                var remarks = parent.find('.cart-remarks').val();
                var old_price = parent.find('.old_price').text();
                var margin = parseFloat(parent.find('.cart-margin').val());

                if (margin == '') {
                    margin = 0;
                }
                if (qty == '') {
                    qty = 1;
                }
                qty = 1;

                var price = (parseFloat(old_price) + (parseFloat(old_price) * parseFloat(margin) / 100))
                    .toFixed(2);
                var subtotal = parseFloat(price * qty).toFixed(2);
                // var total = parseFloat(subtotal) + parseFloat(subtotal * margin / 100);

                parent.find('.subtotal').text(subtotal);
                parent.find('.price').text(price);

                var data = {
                    qty,
                    margin,
                    moq,
                    remarks,
                    userId: "{{ auth()->user()->user_id }}"
                };

                updateCart(parent.data('id'), data);
            });

            $(document).on('keyup  search', '#search', function() {
                fetchAllProducts();
            });

            $(document).on('keyup', '#margin', function() {
                var margin = parseInt($(this).val()) > 0 ? $(this).val() : 0;
                $('.cart-margin').val(margin).trigger('keyup');
            });

            $(document).on('click', '.qty-minus', function() {
                var parent = $(this).closest('.product-list-btn');
                var input = parent.find('input[type=number]');
                var newValue = input.val() - 1;
                input.val(newValue);

                updateCart(input.data('id'), newValue, parent);
            });

            $(document).on('click', '.qty-plus', function() {
                var parent = $(this).closest('.product-list-btn');
                var input = parent.find('input[type=number]');
                var newValue = parseInt(input.val()) + 1;
                input.val(newValue);

                updateCart(input.data('id'), newValue, parent);
            });

            $(document).on('click', '.addToOfferBtn', function() {
                let self = this;
                var productId = $(this)
                $.ajax({
                    url: "{{ route('api.cart.store') }}",
                    method: 'POST',
                    data: {
                        id: $(this).data('id'),
                        userId: "{{ auth()->user()->user_id }}"
                    },
                    success: function(res) {
                        // var cartHtml = `
                    //     <button type="button" class="btn btn-sm btn-primary addToOfferBtn rounded btn-block" data-id="${productId}">Add</button>
                    // `;
                        // var cartHtml = `
                    //     <div class="input-group">
                    //         <div class="input-group-prepend">
                    //             <button 
                    //                 class="btn btn-sm btn-primary rounded-left qty-minus" 
                    //                 type="button"
                    //             >
                    //                 <i class="icon-minus"></i>
                    //             </button>
                    //         </div>
                    //         <input class="form-control" style="width: 50px" data-id="${res.cart.id}" value="1" type="number" readonly>
                    //         <div class="input-group-append">
                    //             <button
                    //                 class="btn btn-sm btn-primary rounded-right qty-plus" 
                    //                 type="button"
                    //             >
                    //                 <i class="icon-plus"></i>
                    //             </button>
                    //         </div>
                    //     </div>`;

                        // $(self).closest('.product-list-btn').html(cartHtml);

                        cartIntoTable(res.carts);

                    }
                });
            });
        });
    </script>
@endsection
