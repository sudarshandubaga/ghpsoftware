@extends('frontend.layouts.app')

@section('main_section')
    <section class="page-header mb-3">
        <div class="container-fluid">
            <div class="clearfix">
                <div>
                    <h1>Edit Role</h1>
                    <ul class="breadcrumbs clearfix">
                        <li><a href="">Dashboard</a></li>
                        <li class="active">Edit Role</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    {{ Form::open(['url' => route('role.update', [$role->id]), 'method' => 'put', 'class' => 'container-fluid']) }}
    <div class="card mt-5">
        <h3 class="card-title">Edit Role</h3>
        <div class="row">
            <div class="col">
                <div class="row">
                    <div class="col-sm-3"><label>Name</label></div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            {{ Form::text('name', null, ['placeholder' => 'Enter Name', 'class' => 'form-control', 'required' => 'required']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <h3 class="card-title">Permissions</h3>
        @foreach ($permissions as $p)
        <div class="checkbox">
            <label>
                <!-- <input name="permission[]" type="checkbox" value="{{ $p }}"> -->
                {{ Form::checkbox('permission[]', $p) }}
                {{ $p }}
            </label>
        </div>
        @endforeach
        <div>
            <button type="submit" class="btn btn-primary px-5">Update</button>
        </div>
    </div>
    {{ Form::close() }}
@endsection