<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>View Quotes</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">View  Quotes</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<form>
<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter Vendor Quotes</h3>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label>Quote ID</label>
                <input type="text" name="SearchQuery" value="{{ @$_GET['SearchQuery'] }}" class="form-control">
            </div>
        </div>

        <div class="col">
            <div class="form-group">
                <label>Buyer Name</label>
                <input type="text" name="SearchName" value="{{ @$_GET['SearchName'] }}" class="form-control">
            </div>
        </div>
        
        <div class="col">
            <div class="form-group">
                <label>Filter By Status</label>
                <select name="PiCreated" class="form-control">
                    <option value="">All</option>
                    <option @if(@$_GET['PiCreated'] == "1") selected @endif value="1">Quotation - CLOSE</option>
                    <option @if(@$_GET['PiCreated'] == "0") selected @endif value="0">Quotation - OPEN</option>
                </select>
            </div>
        </div>

        <div class="col">
            <div class="form-group">
                <label>&nbsp;</label>
                <button type="submit" class="btn btn-primary form-control">Filter</button>
            </div>
        </div>
    </div>
</div>
</form>

<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">List Quotes</div>
                        <div class="ml-auto">
                            <a href="" class="text-white" title="Remove" data-toggle="tooltip"> <i class="icon-trash-o"></i> </a>
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i class="icon-refresh"></i> </a>
                        </div>
                    </h3>
                    <div class="basic-info-two">

                        @if(!$records->isEmpty())
                        <div class="table-responsive" style="height: 600px; border: #000 solid 0px;">
                            <table class="table table-bordered table-hover table-header-fix">
                                <thead>
                                <tr>
                                    <th style="width: 50px;">
                                        <label class="animated-checkbox">
                                            <input type="checkbox" class="checkall">
                                            <span class="label-text"></span>
                                        </label>
                                    </th>
                                    <th style="width: 80px;">Sr. No.</th>
                                    <th>Quote ID</th>
                                    <th>Revision No.</th>
                                    <th>Buyer Name</th>
                                    <th>Total Amount</th>
                                    <th>Total CBM</th>
                                    <th>Date</th>
                                    <th>Last Modified</th>
                                    <th>Added By</th>
                                    <th style="width: 150px;">Actions</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $sn = $records->firstItem(); @endphp
                                @foreach($records as $rec)

                                @php
                                $CountPI = \DB::table('purchase_invoices')->where("pi_qid", $rec->quote_id)->count();
                                @endphp
                                    
                                    
                                    <tr @if($rec->status == 1) class="alert alert-success" @else class="alert alert-danger" @endif>
                                        <td>
                                            <label class="animated-checkbox">
                                                <input type="checkbox" name="check[]" value="{{ $rec->quote_enq_id  }}" class="check">
                                                <span class="label-text"></span>
                                            </label>
                                        </td>
                                        <td>{{ $sn++ }}.</td>
                                        <td>{{ sprintf("%s%06d", $site->setting_quote_prefix, $rec->quote_number) }}</td>
                                        <td>{{ $rec->quote_version }}</td>
                                        <td>
                                            <div class="row mb-1">
                                                <!--<div class="col-5">-->
                                                <!--    <strong>Name:</strong>-->
                                                <!--</div>-->
                                                <div class="col-">
                                                    {{ $rec->user_name }}
                                                </div>
                                            </div>
                                            <!--<div class="row mb-1">-->
                                            <!--    <div class="col-5">-->
                                            <!--        <strong>Mobile:</strong>-->
                                            <!--    </div>-->
                                            <!--    <div class="col-7">-->
                                            <!--        {{ $rec->user_mobile }}-->
                                            <!--    </div>-->
                                            <!--</div>-->
                                            <!--<div class="row mb-1">-->
                                            <!--    <div class="col-5">-->
                                            <!--        <strong>Email:</strong>-->
                                            <!--    </div>-->
                                            <!--    <div class="col-7">-->
                                            <!--        {{ $rec->user_email }}-->
                                            <!--    </div>-->
                                            <!--</div>-->
                                        </td>
                                        <td>{{ $rec->currency_sign }} {{ number_format($rec->quote_total,2) }}</td>
                                        <td>{{ $rec->quote_total_cbm }}</td>
                                        <td>{{ date("d/m/Y", strtotime($rec->quote_date)) }}</td>
                                        <td>{{ date("d/m/Y h:i A", strtotime($rec->quote_updated_on)) }}</td>
                                        <td>{{ ucwords($rec->quote_added_by) }}</td>
                                        <td>
                                            <div class="mb-1">
                                                <a href="{{ url('quote/info/'.$rec->quote_id) }}" title="View Details" data-toggle="tooltip"><i class="icon-eye"></i> View Details</a>
                                            </div>

                                            @if($CountPI > 0)
                                                @php
                                                    $PI = \DB::table('purchase_invoices')->where("pi_qid", $rec->quote_id)->first();
                                                @endphp

                                                <div class="mb-1 bg-danger" style="padding: 5px; color: #FFF">PI# Already Created <br>{{ sprintf("%s-%03d", 'GHP-202122', $PI->pi_id + 100) }}  </div>
                                            @else
                                            <div class="mb-1">
                                                <a href="{{ url('performa-invoice/add/'.$rec->quote_id) }}"><i class="icon-print"></i> Create PI</a>
                                            </div>
                                            @endif

                                            <!-- <div class="mb-1">
                                                <a href="{{ url('quote/revisions/'.$rec->quote_id) }}" title="View Revision Details" data-toggle="tooltip"><i class="icon-file-text"></i> View Revision Details</a>
                                            </div> -->
                                            <!--<div class="mb-1">-->
                                            <!--    <a href="#" title="View Revision Details" data-toggle="tooltip"><i class="icon-file-text"></i> View Revision Details</a>-->
                                            <!--</div>-->
                                            <div class="mb-1">
                                                <a href="{{ url('quote/add/'.$rec->quote_enq_id.'/'.$rec->quote_id) }}" title="View Details" data-toggle="tooltip"><i class="icon-repeat"></i> Revise</a>
                                            </div>
                                            <div class="mb-1">
                                                <a href="{{ url('quote/send/'.$rec->quote_id) }}" title="View Details" data-toggle="tooltip"><i class="icon-send"></i>Send Enq to vendor</a>
                                            </div>
                                        </td>
                                        <td>
                                            @if($CountPI > 0)
                                                Close
                                            @else
                                                Open
                                            @endif
                                        </td>
                                    </tr>
                                    
                                    
                                @endforeach
                                </tbody>
                            </table>
                         </div>
                        {{ $records->appends(request()->query())->links() }}
                        @else
                        <div class="no_records_found">
                          No records found yet.
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
