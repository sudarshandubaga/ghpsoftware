<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>{{ !empty($edit->quote_id) ? 'Revise' : 'Add' }} Quote</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="{{ url('quote') }}">Quote</a></li>
                    <li class="active">{{ !empty($edit->quote_id) ? 'Revise' : 'Add' }} Quote</li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <a href="{{ url('quote') }}" title="View Quote" class="btn btn-default" data-toggle="tooltip"> <i
                            class="icon-eye"></i> View Quote</a>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">

    @if (\Session::has('danger'))
        <div class="alert alert-danger">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-8">
            <form data-session="quote_cart" method="post" enctype="multipart/form-data" id="quote_cart">
                @csrf
                <input type="hidden" id="updateFlag" value="0">
                <div class="card">
                    <h3 class="card-title">
                        <div class="mr-auto">Quotation Details</div>
                        <div class="ml-auto">
                            <a href="#save-data" class="text-white" title="Save" data-toggle="tooltip"> <i
                                    class="icon-save"></i> Save</a>
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i
                                    class="icon-refresh"></i> </a>
                        </div>
                    </h3>
                    @if (empty($edit->quote_id))
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Buyer</label>
                                    <select class="form-control" name="record[quote_uid]" required
                                        onChange="UpdatePrice(this.value)" title="Select the Buyer Name">
                                        <option value="">Choose an option</option>
                                        @foreach ($buyers as $c)
                                            <option value="{{ $c->user_id }}">{{ $c->user_main_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Quotation Date</label>
                                    <input type="text" name="record[quote_date]"
                                        value="{{ !empty($edit->quote_date) ? $edit->quote_date : '' }}"
                                        title="Select Quotation Date" placeholder="yyyy-mm-dd"
                                        class="form-control datepicker_no_future" required autocomplete="new-password"
                                        readonly>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Currency</label>
                                    <select class="form-control" name="record[quote_currency]"
                                        title="Select the Currency">
                                        @foreach ($currencies as $c)
                                            <option value="{{ $c->currency_id }}" required
                                                @if ($c->currency_id == @$edit->quote_currency) selected @endif>
                                                {{ $c->currency_short_name . ' (' . $c->currency_sign . ')' }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Price Term</label>
                                    <select class="form-control" name="record[quote_price_term]" required
                                        title="Select the Price Terms for the Quote">
                                        <option value="FOB" @if (@$edit->quote_price_term == 'FOB') selected @endif>FOB
                                        </option>
                                        <option value="FOR" @if (@$edit->quote_price_term == 'FOR') selected @endif>FOR
                                        </option>
                                        <option value="CIF" @if (@$edit->quote_price_term == 'CIF') selected @endif>CIF
                                        </option>
                                        <option value="FCA" @if (@$edit->quote_price_term == 'FCA') selected @endif>FCA
                                        </option>
                                        <option value="CPT" @if (@$edit->quote_price_term == 'CPT') selected @endif>CPT
                                        </option>
                                        <option value="CIP" @if (@$edit->quote_price_term == 'CIP') selected @endif>CIP
                                        </option>
                                        <option value="DAT" @if (@$edit->quote_price_term == 'DAT') selected @endif>DAT
                                        </option>
                                        <option value="DDP" @if (@$edit->quote_price_term == 'DDP') selected @endif>DDP
                                        </option>
                                        <option value="FAS" @if (@$edit->quote_price_term == 'FAS') selected @endif>FAS
                                        </option>
                                        <option value="CFR" @if (@$edit->quote_price_term == 'CFR') selected @endif>CFR
                                        </option>
                                        <option value="DAF" @if (@$edit->quote_price_term == 'DAF') selected @endif>DAF
                                        </option>
                                        <option value="DDU" @if (@$edit->quote_price_term == 'DDU') selected @endif>DDU
                                        </option>
                                        <option value="DES" @if (@$edit->quote_price_term == 'DES') selected @endif>DES
                                        </option>
                                        <option value="DEQ" @if (@$edit->quote_price_term == 'DEQ') selected @endif>DEQ
                                        </option>
                                        <option value="EXW" @if (@$edit->quote_price_term == 'EXW') selected @endif>EXW
                                        </option>
                                        <option value="AP" @if (@$edit->quote_price_term == 'AP') selected @endif>AP
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Place of Delivery</label>
                                    <select class="form-control" name="record[quote_delivery_port]"
                                        title="Select the Port">
                                        <option value="">Select Place</option>
                                        @foreach ($ports as $p)
                                            <option value="{{ $p->port_name }}"
                                                @if ($p->port_name == @$edit->quote_delivery_port) selected @endif>{{ $p->port_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Delivery Days</label>
                                    <select class="form-control" id="days_dropdown" required
                                        title="Select the Standred Delivery Days">
                                        <option value="45 Days" @if (@$edit->quote_delivery_days == '45 Days') selected @endif>45
                                            Days</option>
                                        <option value="55 Days" @if (@$edit->quote_delivery_days == '55 Days') selected @endif>55
                                            Days</option>
                                        <option value="65 Days" @if (@$edit->quote_delivery_days == '65 Days') selected @endif>65
                                            Days</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <input type="text" name="record[quote_delivery_days]"
                                        value="{{ !empty($edit->quote_delivery_days) ? $edit->quote_delivery_days : '45 Days' }}"
                                        class="form-control" id="days_text" title="Type Delivery Days">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Advance Payment Price in % (Type in only Numeric Value)</label>
                                    <input type="text" class="form-control" name="record[quote_payment]"
                                        title="Type in only Numeric Value">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Country of Origin of goods</label>
                                    <select class="form-control" name="record[quote_origin_country]"
                                        title="Select the Country of Origin of Good ">
                                        <option value="">Select Country</option>
                                        @foreach ($countries as $c)
                                            <option value="{{ $c->country_name }}"
                                                @if (@$edit->quote_origin_country == $c->country_name) selected @endif>
                                                {{ $c->country_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Country of the Final Destination</label>
                                    <select class="form-control" name="record[quote_first_destination_country]"
                                        title="Select the Country of the Final Destination">
                                        <option value="">Select Country</option>
                                        @foreach ($countries as $c)
                                            <option value="{{ $c->country_name }}"
                                                @if (@$edit->quote_first_destination_country == $c->country_name) selected @endif>
                                                {{ $c->country_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Port of Loading</label>
                                    <select class="form-control" name="record[quote_loading_port]"
                                        title="Select the Port for the Loading">
                                        <option value="">Select Port</option>
                                        @foreach ($ports as $p)
                                            <option value="{{ $p->port_name }}"
                                                @if ($p->port_name == @$edit->quote_loading_port) selected @endif>{{ $p->port_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Port of Discharge</label>
                                    <select class="form-control" name="record[quote_discharge_port]"
                                        title="Select the Port for the Discharge">
                                        <option value="">Select Port</option>
                                        @foreach ($ports as $p)
                                            <option value="{{ $p->port_name }}"
                                                @if ($p->port_name == @$edit->quote_discharge_port) selected @endif>{{ $p->port_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="">Place of Receipt</label>
                                    <select class="form-control" name="record[quote_reciept_port]"
                                        title="Select the Place of Receipt">
                                        <option value="">Select Country</option>
                                        @foreach ($countries as $c)
                                            <option value="{{ $c->country_name }}"
                                                @if (@$edit->quote_reciept_port == $c->country_name) selected @endif>
                                                {{ $c->country_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Quotation ID</label>
                                    <input type="text" name="record[quote_number]"
                                        value="{{ $edit->quote_number }}" readonly class="form-control"
                                        title="Quotation ID">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Revised From [Revision No.]</label>
                                    <div class="form-control" readonly>
                                        {{ $edit->quote_version }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Revision Comment</label>
                                    <input type="text" name="record[quote_revision_comment]" class="form-control"
                                        placeholder="Revision Comment" title="Type Here Comment">
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="card">
                    <h3 class="card-title">Product / Item Details</h3>
                    <div id="cartResponse">
                        @php
                            $total_field = 'quote_total';
                            $total_cbm_field = 'quote_total_cbm';
                            $cart_name = 'quote_cart';
                        @endphp
                        @include('frontend.template.cart_table')
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-4">

            <div class="card">
                <h3 class="card-title">Add Product </h3>
                <form class="cart_form" id="NewProductForm" action="{{ url('ajax/add_to_cart/quote_cart') }}"
                    method="post">
                    @csrf
                    <input type="hidden" name="BuyerDetail" id="BuyerDetail" value="">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Category *</label>
                                <select class="form-control category" id="quote-category"
                                    data-target="#quote-sub-category">
                                    <option value="">Choose a Category</option>
                                    @foreach ($categories as $c)
                                        <option value="{{ $c->category_id }}">{{ $c->category_name }}</option>
                                    @endforeach;
                                </select>
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label>Sub category *</label>
                                <select class="form-control" id="quote-sub-category">
                                    <option value="">Choose a Sub Category</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Finish *</label>
                                <select class="form-control" id="quote-finish">
                                    <option value="">Choose a Finish</option>
                                    @foreach ($finishes as $f)
                                        <option value="{{ $f->finish_title }}">{{ $f->finish_title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label>Range *</label>
                                <select class="form-control" id="quote-range">
                                    <option value="">Choose a Range</option>
                                    @foreach ($ranges as $range)
                                        <option value="{{ $range->range_code }}">{{ $range->range_title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Product</label>
                        <select class="form-control" id="quote-product" name="cart[cart_pid]" required>
                            <option value="">Select Product</option>
                            @foreach ($allproducts as $p)
                                <option value="{{ $p->product_id }}">{{ $p->product_name . ' ' . $p->product_code }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Qty</label>
                        <input type="number" name="cart[cart_qty]" value="1" min="1"
                            class="form-control" required>
                    </div>
                    <button type="submit" name="button" class="btn btn-primary btn-block">Add To Quote</button>
                </form>
            </div>
            <div id="buyerProducts" class="card"></div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $(document).on("submit", "#quote_cart", function(e) {

            if ($("#updateFlag").val() == 0) {
                e.preventDefault();

                alert("Warning! Please click on update first.");
            }
        });

        $(document).on("click", ".btn-cart-update", function() {
            $('#updateFlag').val(1);
        });
    });

    function addToQuoteProduct(pid) {
        $('#quote-product').val(pid);
        $('#NewProductForm').trigger('submit');
    }

    function UpdatePrice(ID) {
        $("#BuyerDetail").val(ID);
        $('#buyerProducts').html('Loading...');
        $.ajax({
            url: "{{ route('ajax.quote.products') }}",
            method: 'POST',
            data: {
                id: ID,
            },
            success: function(res) {
                // console.log('response: ', res);
                if (res.data.length) {
                    $('#buyerProducts').html(`
                    <h3 class="card-title">Buyer's Products</h3>
                    <div style="max-height: 300px; overflow: auto;">
                        <table class="table table-striped table-sm">
                            
                        </table>
                    </div>
                `);

                    var table_sel = $('#buyerProducts').find('table');
                    res.data.forEach(function(row, i) {
                        table_sel.append(`
                        <tr>
                            <td>
                                <img src="{{ url('imgs/products') }}/${row.product.product_image}" alt="${row.product.product_name}" style="width: 30px; height: 30px; object-fit: contain;" />
                            </td>
                            <td>
                                <div><strong>${row.product.product_name}</strong></div>
                                <div>${row.product.product_code}</div>
                            </td>
                            <td>
                                <button class="btn btn-primary btn-sm" onclick="addToQuoteProduct(${row.product.product_id})">Add</button>
                            </td>
                        </tr>
                    `);
                    });
                } else {
                    $('#buyerProducts').html(`
                    <h3 class="card-title">Buyer's Products</h3>
                    <div>No products found.</div>
                `);
                }
            }
        });
    }
</script>
