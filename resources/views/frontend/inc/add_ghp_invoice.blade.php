<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Generate GHP Invoice</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="{{ url('quote') }}">Generate GHP Invoice</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">

    @if (\Session::has('danger'))
        <div class="alert alert-danger">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <form data-session="pi_cart" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                	<h3 class="card-title">
                		<div class="mr-auto">Details</div>
                        <div class="ml-auto">
                            <a href="#save-data" class="text-white" title="Save" data-toggle="tooltip"> <i class="icon-save"></i> Save</a>
                        </div>
                	</h3>
                	<div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Invoice No.</label>
                                <input type="text" name="record[exs_ghp_invoice_invoice_no]" value="" class="form-control" required>
                            </div>
                        </div>
                        
                        <div class="col">
                            <div class="form-group">
                                <label>Date</label>
                                <input type="date" name="record[exs_ghp_invoice_invoce_date]" value="" class="form-control" required>
                            </div>
                        </div>
                        
                        <div class="col">
                            <div class="form-group">
                                <label>Place or Receipt</label>
                                <select class="form-control" name="record[exs_ghp_invoice_place_receipt]" required>
                                    <option value="">Select Receipt</option>
                                    @foreach($ports as $p)
                                    <option value="{{ $p->port_name }}">{{ $p->port_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="col">
                            <div class="form-group">
                                <label>PRE-CARRIAGE BY</label>
                                <select name="record[exs_ghp_invoice_pre_carriage]" value="" class="form-control datepicker_no_future" required>
                                    <option value="">Select</option>
                                    <option>Road</option>
                                    <option>Air</option>
                                    <option>Rail</option>
                                    <option>Ship</option>
                                    <option>Road/Rail</option>
                                </select>
                            </div>
                        </div>
                	</div>
                </div>
            </form>
        </div>
    </div>
</div>
