<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>State</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">State</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
	<div class="card mb-5 mt-5">
	    
	    @if(Session::has('Success'))
        <div class="alert alert-success mb-5" role="alert">{!!Session::get('Success')!!}</div>
        @endif
        
        @if(Session::has('Danger'))
        <div class="alert alert-danger mb-5" role="alert">{!!Session::get('Danger')!!}</div>
        @endif
        
		<h3 class="card-title">Add State</h3>
	     <form method="post">
	     	@csrf
		     <div class="row">
		         <div class="col-sm-6">
		             <div class="row">
		             	<div class="col-sm-3">
		                     <label>
		                         Country
		                     </label>
		                 </div>

		                 <div class="col-sm-9">
		                     <div class="form-group">
		                         <select name="record[state_country]" class="form-control">
		                         	<option value="">Select Country</option>
		                         	@foreach($countries as $con)
		                         	<option value="{{ $con->country_id }}" @if(!empty($edit->state_country) && $edit->state_country == $con->country_id) selected @endif>{{ $con->country_name.' ('.$con->country_short_name.')' }}</option>
		                         	@endforeach
		                         </select>
		                     </div>
		                 </div>
		             </div>
		         </div>

		         <div class="col-sm-2">

		         </div>

		         <div class="col-sm-2">
		             
		         </div>

		         <div class="col-sm-2">

		         </div>

		         <div class="col-sm-6">
		             <div class="row">
		             	<div class="col-sm-3">
		                     <label>
		                         State Name
		                     </label>
		                 </div>

		                 <div class="col-sm-9">
		                      <div class="form-group">
		                          <input type="text" name="record[state_name]" value="{{ @$edit->state_name }}" class="form-control">
		                      </div>
		                 </div>
		             </div>
		         </div>

		         <div class="col-sm-2">

		         </div>

		         <div class="col-sm-2">
		             <div class="form-group">
		                 <button type="submit" class="btn btn-primary form-control">SAVE</button>
		             </div>
		         </div>

		         <div class="col-sm-2">

		         </div>

		         <div class="col-sm-6">
		             <div class="row">
		                 <div class="col-sm-3">
		                     <label>
		                         State Short Name
		                     </label>
		                 </div>

		                 <div class="col-sm-9">
		                     <div class="form-group">
		                         <input type="text" name="record[state_short_name]" value="{{ @$edit->state_short_name }}" class="form-control">
		                     </div>
		                 </div>
		             </div>
		         </div>

		         <div class="col-sm-2">
		         </div>

		         <div class="col-sm-2">

		         </div>

		         <div class="col-sm-2">

		         </div>
		     </div>
		 </form>
	</div>

	<div class="card mb-5 mt-5">    
        
		<h3 class="card-title">Search State</h3>
		<div class="row">
			<div class="col-9">

				<form>
					<div class="row">
						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-3">
									<label>
										State Name
									</label>
								</div>
	   
								<div class="col-sm-9">
									 <div class="form-group">
										 <input type="text" name="state_name" value="{{ request('state_name') }}" class="form-control" >
									 </div>
								</div>
							</div>
						</div>
	   
						<div class="col-sm-4">
							<div class="form-group">
								<button type="submit" class="btn btn-primary form-control">SEARCH</button>
							</div>
							<!-- <div class="form-group">
								 <a href="{{ url('finish') }}" class="btn btn-default text-center form-control">ADD</a>
							</div> -->
						</div>
					</div>
				</form>
			</div>
			<div class="col-3">
			<div class="form-group">
						<div class="col">
							
							<form action="{{ route('exportState') }}" method="post">
								{{ csrf_field() }}          
								<div class="" >
									<input type="hidden" name="state_name" value="{{request('state_name')}}">
									
									<input type="submit" class="btn btn-primary form-control" value="Export">
								</div>       
							</form>
						</div>
					</div>
			</div>
		</div>
	</div>

	<div class="card">
		<h3 class="card-title">
			<div class="mr-auto">View Country</div>
	        <a href="" class="ml-auto text-white" title="Remove Selected" data-toggle="tooltip">
	            <i class="icon-trash-o"></i>
	        </a>
		</h3>
		<form method="post">
	    	@csrf
		    @if(!$records->isEmpty())
		    {{ $records->links() }}
		    <div class="table-responsive">
			    <table class="table table-bordered">
			          <thead>
			               <tr>
			                    <th style="width: 50px;">
			                        <label class="animated-checkbox">
			                            <input type="checkbox" class="checkall">
			                            <span class="label-text"></span>
			                        </label>
			                    </th>
			                   <th style="width: 50px;">S.No.</th>
			                   <th>Country</th>
			                   <th>State Name</th>
			                   <th>State Short Name</th>
			                   <th>Action</th>
			               </tr>
			          </thead>

			          <tbody>
			          		@php $sn = $records->firstItem(); @endphp
			          		@foreach($records as $rec)
			               	<tr>
		                        <td>
		                            <label class="animated-checkbox">
		                                <input type="checkbox" name="check[]" value="{{ $rec->state_id  }}" class="check">
		                                <span class="label-text"></span>
		                            </label>
		                        </td>
								<td>{{ $sn++ }}</td>
								<td>{{ $rec->country_name }}</td>
								<td>{{ $rec->state_name }}</td>
								<td>{{ $rec->state_short_name }}</td>
								<td class="icon-cent">
									<a href="{{ url('state/'.$rec->state_id) }}" class="pencil"><i class="icon-pencil" title="Edit"></i></a>
								</td>
			               	</tr>
			               @endforeach
			          </tbody>
			    </table>
			</div>
			{{ $records->links() }}
		    @else
		    <div class="no_records_found">
		      No records found yet.
		    </div>
			@endif
		</form>
	</div>
</div>
