<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Forwarder Detail</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Forwarder Detail</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
	<div class="card mb-5 mt-5">
	    
	    @if(Session::has('Success'))
        <div class="alert alert-success mb-5" role="alert">{!!Session::get('Success')!!}</div>
        @endif
        
        @if(Session::has('Danger'))
        <div class="alert alert-danger mb-5" role="alert">{!!Session::get('Danger')!!}</div>
        @endif
        
        
        
		<h3 class="card-title">Add Forwarder Detail</h3>
	     <form method="post">
	     	@csrf
		     <div class="row">
		         <div class="col-sm-6">
		             
		               <div class="row">
		                 <div class="col-sm-3"><label>Company Name</label></div>
		                 <div class="col-sm-9">
		                      <div class="form-group">
		                          <input type="text" name="record[forwarder_company_name]" value="{{ @$edit->forwarder_company_name }}" class="form-control"  required="">
		                      </div>
		                 </div>
		             </div>
		             
		             
		             <div class="row">
		                 <div class="col-sm-3"><label>Phone Number</label></div>
		                 <div class="col-sm-9">
		                      <div class="form-group"><input type="text" name="record[forwarder_phone_no]" value="{{ @$edit->forwarder_phone_no }}" class="form-control"  required=""></div>
		                 </div>
		             </div>
		         </div>

		         <div class="col-sm-2">

		         </div>

		         <div class="col-sm-2">
		         	<div class="form-group">
		                <button type="submit" class="btn btn-primary form-control">SAVE</button>
		            </div>
		         </div>
		     </div>
		     
		     <div class="row">
		         <div class="col-lg-3"><label></label></div>
		         <div class="col-lg-3"><label>Name</label></div>
		         <div class="col-lg-3"><label>Email</label></div>
		         <div class="col-lg-3"><label>Mobile No</label></div>
		     </div>
		     
		     <div class="row">
		         <div class="col-lg-3"><label>Contact Person 1</label></div>
		         <div class="col-lg-3"><div class="form-group"><input type="text" name="record[forwarder_contact_name1]" value="{{ @$edit->forwarder_contact_name1 }}" class="form-control"  required=""></div></div>
		         <div class="col-lg-3"><div class="form-group"><input type="text" name="record[forwarder_contact_email1]" value="{{ @$edit->forwarder_contact_email1 }}" class="form-control"  required=""></div></div>
		         <div class="col-lg-3"><div class="form-group"><input type="text" name="record[forwarder_contact_mobile1]" value="{{ @$edit->forwarder_contact_mobile1 }}" class="form-control"  required=""></div></div>
		     </div>
		     
		     <div class="row">
		         <div class="col-lg-3"><label>Contact Person 2</label></div>
		         <div class="col-lg-3"><div class="form-group"><input type="text" name="record[forwarder_contact_name2]" value="{{ @$edit->forwarder_contact_name2 }}" class="form-control"></div></div>
		         <div class="col-lg-3"><div class="form-group"><input type="text" name="record[forwarder_contact_email2]" value="{{ @$edit->forwarder_contact_email2 }}" class="form-control"></div></div>
		         <div class="col-lg-3"><div class="form-group"><input type="text" name="record[forwarder_contact_mobile2]" value="{{ @$edit->forwarder_contact_mobile2 }}" class="form-control"></div></div>
		     </div>
		     
		     <div class="row">
		         <div class="col-lg-3"><label>Contact Person 3</label></div>
		         <div class="col-lg-3"><div class="form-group"><input type="text" name="record[forwarder_contact_name3]" value="{{ @$edit->forwarder_contact_name3 }}" class="form-control"></div></div>
		         <div class="col-lg-3"><div class="form-group"><input type="text" name="record[forwarder_contact_email3]" value="{{ @$edit->forwarder_contact_email3 }}" class="form-control"></div></div>
		         <div class="col-lg-3"><div class="form-group"><input type="text" name="record[forwarder_contact_mobile3]" value="{{ @$edit->forwarder_contact_mobile3 }}" class="form-control"></div></div>
		     </div>
		     
		     <div class="row">
		         <div class="col-lg-3"><label>Contact Person 4</label></div>
		         <div class="col-lg-3"><div class="form-group"><input type="text" name="record[forwarder_contact_name4]" value="{{ @$edit->forwarder_contact_name4 }}" class="form-control"></div></div>
		         <div class="col-lg-3"><div class="form-group"><input type="text" name="record[forwarder_contact_email4]" value="{{ @$edit->forwarder_contact_email4 }}" class="form-control"></div></div>
		         <div class="col-lg-3"><div class="form-group"><input type="text" name="record[forwarder_contact_mobile4]" value="{{ @$edit->forwarder_contact_mobile4 }}" class="form-control"></div></div>
		     </div>
		 </form>
	</div>

	<div class="card mb-5 mt-5">    
        
		<h3 class="card-title">Search Forwarder</h3>
		<div class="row">
			<div class="col-9">

				<form>
					<div class="row">
						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-3">
									<label>
										Name
									</label>
								</div>
	   
								<div class="col-sm-9">
									 <div class="form-group">
										 <input type="text" name="forwarder_company_name" value="{{ request('forwarder_company_name') }}" class="form-control" >
									 </div>
								</div>
							</div>
						</div>
	   
						<div class="col-sm-4">
							<div class="form-group">
								<button type="submit" class="btn btn-primary form-control">SEARCH</button>
							</div>
							<!-- <div class="form-group">
								 <a href="{{ url('finish') }}" class="btn btn-default text-center form-control">ADD</a>
							</div> -->
						</div>
					</div>
				</form>
			</div>
			<div class="col-3">
			<div class="form-group">
						<div class="col">
							
							<form action="{{ route('exportForwarder') }}" method="post">
								{{ csrf_field() }}          
								<div class="" >
									<input type="hidden" name="forwarder_company_name" value="{{request('forwarder_company_name')}}">
									
									<input type="submit" class="btn btn-primary form-control" value="Export">
								</div>       
							</form>
						</div>
					</div>
			</div>
		</div>
	</div>

	<form method="post">
		<div class="card">
			<h3 class="card-title">
				<div class="mr-auto">View Forwarder</div>
		        <a href="" class="ml-auto text-white" title="Remove Selected" data-toggle="tooltip">
		            <i class="icon-trash-o"></i>
		        </a>
			</h3>
	    	@csrf
		    @if(!$records->isEmpty())
		    <div class="table-responsive">
			    <table class="table table-bordered">
			          <thead>
			               <tr>
			                    <th style="width: 50px;">
			                        <label class="animated-checkbox">
			                            <input type="checkbox" class="checkall">
			                            <span class="label-text"></span>
			                        </label>
			                    </th>
			                   <th style="width: 50px;">S.No.</th>
			                   <th>Company Name</th>
			                   <th>Phone No.</th>
			                   <th>Contact Detail</th>
			                   <th>Action</th>
			               </tr>
			          </thead>

			          <tbody>
			          		@php $sn = $records->firstItem(); @endphp
			          		@foreach($records as $rec)
			               	<tr>
		                        <td>
		                            <label class="animated-checkbox">
		                                <input type="checkbox" name="check[]" value="{{ $rec->forwarder_id  }}" class="check">
		                                <span class="label-text"></span>
		                            </label>
		                        </td>
								<td>{{ $sn++ }}</td>
								<td>{{ $rec->forwarder_company_name }}</td>
								<td>{{ $rec->forwarder_phone_no }}</td>
								<td>
								    Contact Name 1 : {{ $rec->forwarder_contact_name1 }}<br>
								    Contact Email 1 : {{ $rec->forwarder_contact_email1 }}<br>
								    Contact Mobile 1 : {{ $rec->forwarder_contact_mobile1 }}<br>
								    
								    Contact Name 2 : {{ $rec->forwarder_contact_name2 }}<br>
								    Contact Email 2 : {{ $rec->forwarder_contact_email2 }}<br>
								    Contact Mobile 2 : {{ $rec->forwarder_contact_mobile2 }}<br>
								    
								    Contact Name 3 : {{ $rec->forwarder_contact_name3 }}<br>
								    Contact Email 3 : {{ $rec->forwarder_contact_email3 }}<br>
								    Contact Mobile 3 : {{ $rec->forwarder_contact_mobile3 }}<br>
								    
								    Contact Name 4 : {{ $rec->forwarder_contact_name4 }}<br>
								    Contact Email 4 : {{ $rec->forwarder_contact_email4 }}<br>
								    Contact Mobile 4 : {{ $rec->forwarder_contact_mobile4 }}<br>
								</td>
								<td class="icon-cent">
									<a href="{{ url('forwarder/'.$rec->forwarder_id) }}" class="pencil"><i class="icon-pencil" title="Edit"></i></a>
								</td>
			               	</tr>
			               @endforeach
			          </tbody>
			    </table>
			</div>
			{{ $records->links() }}
		    @else
		    <div class="no_records_found">
		      No records found yet.
		    </div>
			@endif
		</div>
	</form>
</div>