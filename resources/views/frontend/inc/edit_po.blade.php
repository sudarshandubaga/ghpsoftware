<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Edit Purchase Order</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="{{ url('quote') }}">Quote</a></li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <a href="{{ url('quote') }}" title="View Quote" class="btn btn-default" data-toggle="tooltip"> <i
                            class="icon-eye"></i> View Quote</a>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">

    @if (\Session::has('danger'))
        <div class="alert alert-danger">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            {!! Form::open(['url' => 'purchase-order/edit-save', 'enctype' => 'multipart/form-data', 'method' => 'post']) !!}
            @csrf

            <input type="hidden" name="POID" value="{{ $record->po_id }}">

            <div class="card">
                <!-- <h3 class="card-title">Update PO Details</h3> -->
                <h3 class="card-title">
                    <div class="mr-auto">Update PO Details</div>
                    <div class="ml-auto">
                        <!-- <a href="#save-data" class="text-white" title="Save" data-toggle="tooltip"> <i class="icon-save"></i> Save</a> -->
                        <button class="text-white" style="background:none;border:none;font-weight:bold"
                            type="submit"><i class="icon-save"></i> save</button>
                        &nbsp;
                        <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i
                                class="icon-refresh"></i> </a>
                    </div>
                </h3>
                <div class="row">
                    <div class="col-lg-6">
                        <label>Vendor Name</label>
                        <input type="text" readonly class="form-control"
                            value="{{ @$record->vender->vendor_org_name }}">
                        <!-- <select class="form-control" name="vendor" readonly>
                                @foreach ($vendor as $v)
<option @if ($record->po_vendor == $v->vendor_uid) selected @endif value="{{ $v->vendor_uid }}">{{ $v->vendor_org_name }} ( {{ $v->vendor_code }} )</option>
@endforeach()
                            </select> -->
                    </div>

                    <div class="col-lg-6">
                        <label>Date</label>
                        <input class="form-control" name="PoDate" type="date" value="{{ $record->po_date }}">
                    </div>
                    <!-- <div style="margin-top:10px;">
                            <button class="btn btn-primary" type="submit">save</button>
                        </div> -->
                </div>
            </div>



            <div class="card">
                <h3 class="card-title">Product / Item Details</h3>
                <div class="table-responsive">
                    <table class="table table-bordered" style="background: #FFF">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Delete</th>
                                <th>Product Image</th>
                                <th>Product CBM</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Total CBM</th>
                                <th>Total Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $sn = $totQty = $totPrice = $totCBM = 0;
                            @endphp
                            @foreach ($products as $Pindex => $p)
                                @php
                                    $dir = 'imgs/products/';
                                    $image = 'imgs/no-image.png';
                                    if (!empty($p->product_image)) {
                                        $image = $dir . $p->product_image;
                                    }
                                @endphp
                                <tr class="tr_parent">
                                    <td>{{ ++$sn }}</td>
                                    <td><input type="checkbox" value="{{ $p->popro_id }}" name="PoDelPro[]"></td>
                                    <td>
                                        <img src="{{ url($image) }}" alt="{{ $p->product_name }}"
                                            style="max-width: 70px;">
                                        {{ $p->product_name }}
                                    </td>
                                    <td><input type="number" name=""
                                            value="{{ number_format($p->product_cbm, 2) }}" readonly
                                            id="PrdQty{{ $Pindex }}" class="product_cbm"></td>
                                    <td><input type="number" name="InvoiceProducts[{{ $p->popro_id }}][qty]"
                                            value="{{ $p->popro_qty }}" required id="PrdQty{{ $Pindex }}"
                                            onBlur="UpdateAll('{{ $Pindex }}')" class="product_qty"></td>
                                    <td><input type="number" value="{{ number_format($p->popro_price, 2) }}"
                                            name="InvoiceProducts[{{ $p->popro_id }}][price]" required
                                            id="PrdPrice{{ $Pindex }}" onBlur="UpdateAll('{{ $Pindex }}')"
                                            class="product_price"></td>
                                    <td><input type="text"
                                            value="{{ number_format($p->product_cbm * $p->popro_qty, 2) }}"
                                            name="InvoiceProducts[{{ $p->popro_id }}][cbm_subtotal]" readonly
                                            class="cbm_subtotal"></td>
                                    <td><input type="text"
                                            value="{{ number_format($p->popro_price * $p->popro_qty, 2) }}"
                                            name="InvoiceProducts[{{ $p->popro_id }}][subtotal]" readonly
                                            class="subtotal"></td>
                                    <!-- <td><input type="text" value="{{ $p->popro_price * $p->popro_qty }}" name="InvoiceProducts[{{ $p->popro_id }}][subtotal]" readonly id="PrdSubTotal{{ $Pindex }}" onBlur="UpdateAll('{{ $Pindex }}')"></td> -->
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <!-- <tr>
                        <th colspan="6">TOTAL</th>
                        <th>{{ $totQty }}</th>
                        <th>{{ number_format($totCBM, 6) }}</th>
                        <th>{{ $totPrice }}</th>
                        <th colspan="2"></th>
                    </tr> -->
                        </tfoot>
                    </table>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script>
    $('.product_qty').on('keyup', function() {
        var $this = $(this).parents('.tr_parent');
        var product_cmb = $this.find('.product_cbm').val();
        var product_price = $this.find('.product_price').val();
        var product_qty = $(this).val();
        if (product_qty === "") {
            product_qty = 0;
        }
        // console.log(product_cmb,, parseInt(product_qty));
        var cbm_subtotal = product_cmb * parseInt(product_qty);
        cbm_subtotal = cbm_subtotal.toFixed(2);
        var subtotal = product_price * parseInt(product_qty);
        subtotal = subtotal.toFixed(2);
        $(this).parents('.tr_parent').find('.cbm_subtotal').val(cbm_subtotal);
        $(this).parents('.tr_parent').find('.subtotal').val(subtotal);
    })
    $('.product_price').on('keyup', function() {
        var $this = $(this).parents('.tr_parent');
        var product_price = $(this).val();
        var product_qty = $this.find('.product_qty').val();
        if (product_qty === "") {
            product_qty = 0;
        }
        // console.log(product_cmb,, parseInt(product_qty));
        var subtotal = product_price * parseInt(product_qty);
        subtotal = subtotal.toFixed(2);
        $(this).parents('.tr_parent').find('.subtotal').val(subtotal);
    })
</script>
