<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Order Tracking Details</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Order Tracking Details</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@php
$GrandTotal = 0;
$GrandCBM = 0;
$products = \App\Models\PIProduct::join('products AS p', 'p.product_id', 'pipro_pid')->join('quote_products AS qpro', 'pipro_pid', 'qpro_pid')->where('pipro_pi_id', $records->pi_id)->where('qpro_qid', $records->quote_id)->get();

foreach($products as $p){
    $SubTotal = $p->qpro_price * $p->qpro_qty;
    $SubCBM = $p->product_cbm * $p->qpro_qty;
    $GrandTotal += $SubTotal;
    $GrandCBM += $SubCBM;
}


$ST40HQ = 0;
$ST40SD = 0;
$ST20SD = 0;
$LCL = 0;

$total_cbmC = ceil($GrandCBM);
$ST40HQ = floor($total_cbmC / 68);

$RemianingCBM = $GrandCBM - ($ST40HQ * 68);

if($RemianingCBM > 0){
    $ST40SD = floor($RemianingCBM / 58);
    $RemianingCBM = $RemianingCBM - ($ST40SD * 58);
}

if($RemianingCBM > 0){
    $ST20SD = floor($RemianingCBM / 28);
    $RemianingCBM = $RemianingCBM - ($ST20SD * 28);
}

if($RemianingCBM > 0){
    $LCL = floor($RemianingCBM / 15);
    $RemianingCBM = $RemianingCBM - ($LCL * 15);
}
@endphp

@php
$GetAllPo = App\Models\POModel::join('vendor_quotes AS q', 'q.vquote_id', 'purchase_orders.po_qid')->join('users AS u', 'q.vquote_uid', 'u.user_id')->where("po_pi_id", $records->pi_id)->join('vendors AS ven', 'q.vquote_uid', 'ven.vendor_uid')->get();
@endphp

@php
$Logistic = DB::table('logistics')->where("exs_logistics_pi", sprintf("%s-%03d", 'GHP-201819', $records->pi_id).",")->count();
$Logistic = DB::table('logistics')->where("exs_logistics_pi", sprintf("%s-%03d", 'GHP-201819', $records->pi_id).",")->first();

@endphp
<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card accordion" id="accordionExample">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">#PI</div>
                        <div class="text-right"><i class="fa fa-sort-desc" aria-hidden="false" style="color:#fff;" data-toggle="collapse" data-target="#collapseOne"
                                aria-expanded="true" aria-controls="collapseOne"></i></div>
                    </h3>
                    <div class="basic-info-two collapse show"  id="collapseOne"  aria-labelledby="headingOne" data-parent="#accordionExample">
                        <table class="table table-bordered">
                            <tr>
                                <td><b>Buyer : </b> {{ $records->user_name }}</td>
                                <td><b>PI# No. : </b> {{ sprintf("%s-%03d", 'GHP-201819', $records->pi_id) }}</td>
                                <td><b>PI# Date : </b> {{ date("d-M-Y", strtotime($records->pi_date)) }}</td>
                            </tr>
                            <tr>
                                <td><b>PO Date : </b>
                                @if(isset($GetAllPo[0]->po_date))
                                {{ @date("d-M-Y", strtotime(@$GetAllPo[0]->po_date)) }}
                                @endif
                                </td>
                                <td>
                                    <b>PI Conf. Date : </b>
                                    
                                    @if($records->pi_confirm == 1 && $records->pi_confirm_date != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($records->pi_confirm_date)) }}
                                    @endif
                                    
                                </td>
                                <td><b>Cont Size : </b>
                                    @if($records->qty1 != "")
                                    {{ $records->qty1 }} x {{ $records->container1 }}
                                    @endif
                                    
                                    @if($records->qty2 != "")
                                    {{ $records->qty2 }} x {{ $records->container2 }}
                                    @endif
                                    
                                    @if($records->qty3 != "")
                                    {{ $records->qty3 }} x {{ $records->container3 }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><b>Payment Term : </b> {{ $records->quote_payment }}%</td>
                                <td><b>Delivery Term : </b> {{ $records->quote_price_term }} - {{ $records->quote_loading_port }}</td>
                                <td><b>Agreed ETD : </b>
                                @if(@$Logistic->AgreedETD != "")
                                {{ @date("d-M-Y", strtotime($Logistic->AgreedETD)) }}
                                @endif
                                </td>
                            </tr>
                            @php
                            
                            $DelayDays = "";
                            
                            if(@$Logistic->VesselSailingETD != "" && @$Logistic->AgreedETD != ""){
                                $VaisalDAte = @$Logistic->VesselSailingETD;
                                $Agree = @$Logistic->AgreedETD;
                                
                                $date1 = new DateTime($VaisalDAte);
                                $date2 = new DateTime($Agree);
                                $days  = $date2->diff($date1);
                                
                                $DelayDays = $days->days . " Days";
                            }
                            
                            @endphp
                            <tr>
                                <td><b>CBM : </b> {{ $GrandCBM }}</td>
                                <td style="background: #ead9d9;"><b>Delay : </b> {{ $DelayDays }} </td>
                                <td><b>Revised ETD (if any) : </b>
                                @if(@$Logistic->T2ndRvsdETD != "")
                                {{ @date("d-M-Y", strtotime($Logistic->PlannedETDDate2)) }}
                                @endif
                                </td>
                            </tr>
                            <tr>
                                <td><b>PI# Value : </b> $ {{ number_format($GrandTotal, 2) }}</td>
                                <td><b>DP / Adv Amount : </b>  ${{ $records->quote_payment == "" ? 0 : number_format(($GrandTotal * $records->quote_payment) / 100, 2) }}</td>
                                <td><b>DP Date : </b>
                                    @if($records->pi_confirm == 1 && $records->dp_date != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($records->dp_date)) }}
                                    @endif
                                </td>
                            </tr>
                            @php
                                $BalancePayment = $GrandTotal;
                                $AdvPer = 0;
                                if($records->quote_payment != ""){
                                    $AdvPer = $records->quote_payment;
                                }
                                
                                $AdvanceAmt = ($GrandTotal * $AdvPer) / 100;
                                $BalancePayment = $GrandTotal - $AdvanceAmt;
                            @endphp
                            <tr>
                                <td><b>Surcharge : </b> </td>
                                <td><b>Balance Amount : </b> ${{ number_format($BalancePayment, 2) }}</td>
                                <td><b>Product / Remarks : </b> {{ $records->product_remark }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                
                
                @foreach($GetAllPo as $Indxe => $GAPO)
                
                @php
                $PoInvoice = App\Models\PoInvoice::where("po_invoice_po", $GAPO->po_id)->first();
                @endphp
                
                <div class="clearfix">&nbsp;</div>
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">#Purchase Order {{ $Indxe +1 }}</div>
                        <div class="text-right"><i class="fa fa-sort-desc" aria-hidden="false" style="color:#fff;" data-toggle="collapse" data-target="#collapse2{{ $Indxe +1 }}"
                                aria-expanded="true" aria-controls="collapse2"></i></div>
                    </h3>
                    
                    <div class="basic-info-two collapse show"  id="collapse2{{ $Indxe +1 }}"  aria-labelledby="headingOne" data-parent="#accordionExample">
                        <table class="table table-bordered">
                            <tr>
                                <td><b>Vendor : </b> {{ $GAPO->user_name }}</td>
                                <td><b>Vendor Short Name </b> : {{ @$GAPO->vendor_short_name }}</td> 
                                <td><b>Vendoe Code : </b> {{ @$GAPO->vendor_code }}</td>
                            </tr>
                            
                            <tr>
                                <td><b>Vendor PO# / Invoice No: </b> 18050{{ $GAPO->po_id }} / {{ @$PoInvoice->po_invoice_invoice_no }}</td>
                                <td><b>PO# Date  : </b> {{ date("d-M-Y", strtotime($GAPO->po_date)) }}</td>
                                <td><b>Vendor ETD : </b> {{ date("d-M-Y", strtotime($GAPO->vquote_delivery_days)) }}</td>
                            </tr>
                            
                            @php
                            $VaisalDAte = @$Logistic->VesselSailingETD;
                            $Agree = @$GAPO->vquote_delivery_days;
                            
                            $date1 = new DateTime($VaisalDAte);
                            $date2 = new DateTime($Agree);
                            $days  = $date2->diff($date1);
                            @endphp
                            <tr>
                                <td><b>Revised ETD : </b> 
                                
                                @if(@$Logistic->T2ndRvsdETD != "")
                                {{ @date("d-M-Y", strtotime($Logistic->PlannedETDDate2)) }}
                                @endif
                                
                                @if(@$Logistic->T2ndRvsdETD == 1)
                                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#exampleModal" style="width: 80px;float: right;">Details</button>
                                @endif
                                </td>
                                <td style="background: #ead9d9;"><b>Delay : </b> {{ $days->days }} Days</td>
                                <td><b>PO Value : </b> {{ $GAPO->currency_sign }} {{ number_format($GAPO->vquote_total,2) }}</td>
                            </tr>
                            @php
                            $Adanvace = ($GAPO->po_payment * $GAPO->vquote_total) / 100;
                            @endphp
                            <tr>
                                <td><b>Advance/DP Amount : </b> ${{ number_format($Adanvace, 2) }}</td>
                                <td><b>DP Date : </b> </td>
                                <td><b>Surchage Forwarder Charges : </b> </td>
                            </tr>
                            
                            <tr>
                                <td><b>Balance Amount : </b> ${{ number_format($GAPO->vquote_total - $Adanvace, 2) }}</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
                @endforeach
                
                <div class="clearfix">&nbsp;</div>
                <!--<div class="card">-->
                <!--    <h3 class="card-title clearfix">-->
                <!--        <div class="mr-auto">#Quality</div>-->
                <!--    </h3>-->
                    
                <!--    <div class="basic-info-two">-->
                <!--        <table class="table table-bordered">-->
                <!--            <tr>-->
                <!--                <td><b>Counter Sample Packaging Approve Date Visit #1 : </b> </td>-->
                <!--                <td><b>Counter Sample Packaging Approve Date Visit #2 : </b> </td>-->
                <!--                <td><b>Raw Production I(nline) Insecpection Date Visit #1 : </b> </td>-->
                <!--            </tr>-->
                            
                <!--            <tr>-->
                <!--                <td><b>Raw Production I(nline) Insecpection Date Visit #2 : </b> </td>-->
                <!--                <td><b>Mid Line Inspection Visit #3 : </b> </td>-->
                <!--                <td><b>Visit #3 Mid Line Isepection : </b> </td>-->
                <!--            </tr>-->
                            
                <!--            <tr>-->
                <!--                <td><b>Visit #4 Final Inspection : </b> </td>-->
                <!--            </tr>-->
                <!--        </table>-->
                <!--    </div>-->
                <!--</div>-->
                <div class="clearfix">&nbsp;</div>
                <!--<div class="card">-->
                <!--    <h3 class="card-title clearfix">-->
                <!--        <div class="mr-auto">#Quality</div>-->
                <!--    </h3>-->
                    
                <!--    <div class="basic-info-two"> -->
                <!--        <table class="table table-bordered">-->
                <!--            <thead>-->
                <!--              <tr>-->
                <!--                <th><b>Level</b></th>-->
                <!--                <th><b>No. of. Visit</b></th>-->
                <!--                <th><b>Date</b></th>-->
                <!--                <th><b>Remark</b></th>-->
                <!--              </tr>-->
                <!--            </thead>-->
                <!--            <tbody>-->
                <!--              <tr>-->
                <!--                <td><b>Counter Sample Packaging :</b></td>-->
                <!--                <td>Visit 1</td>-->
                <!--                <td>07-05-2020</td>-->
                <!--                <td>Process Due</td>-->
                <!--              </tr>-->
                <!--              <tr>-->
                <!--                <td><b>Counter Sample Packaging :</b></td>-->
                <!--                <td>Visit 2</td>-->
                <!--                <td>08-05-2020</td>-->
                <!--                <td>Approved</td>-->
                <!--              </tr>-->
                <!--              <tr>-->
                <!--                <td><b>Raw Production (Inline) Insecpection :</b></td>-->
                <!--                <td>Visit 1</td>-->
                <!--                <td>07-05-2020</td>-->
                <!--                <td>Process Due</td>-->
                <!--              </tr>-->
                <!--              <tr>-->
                <!--                <td><b>Raw Production (Inline) Insecpection :</b></td>-->
                <!--                <td>Visit 2</td>-->
                <!--                <td>08-05-2020</td>-->
                <!--                <td>Approved</td>-->
                <!--              </tr>-->
                <!--              <tr>-->
                <!--                <td><b>Mid Line Inspection : </b></td>-->
                <!--                <td>Visit 1</td>-->
                <!--                <td>07-05-2020</td>-->
                <!--                <td>Process Due</td>-->
                <!--              </tr>-->
                <!--              <tr>-->
                <!--                <td><b>Mid Line Inspection : </b></td>-->
                <!--                <td>Visit 2</td>-->
                <!--                <td>08-05-2020</td>-->
                <!--                <td>Approved</td>-->
                <!--              </tr>-->
                <!--              <tr>-->
                <!--                <td><b>Final Inspection : </b></td>-->
                <!--                <td>Visit 2</td>-->
                <!--                <td>08-05-2020</td>-->
                <!--                <td>Approved</td>-->
                <!--              </tr>-->
                <!--            </tbody>-->
                <!--          </table>-->
                <!--        </div>-->
                <!--    </div> -->
                  
                 
                @php
                $Logistic = DB::table('logistics')->where("exs_logistics_pi", sprintf("%s-%03d", 'GHP-201819', $records->pi_id).",")->count();
                @endphp
                
                @if($Logistic > 0)
                     @php
                        $Logistic = DB::table('logistics')->where("exs_logistics_pi", sprintf("%s-%03d", 'GHP-201819', $records->pi_id).",")->first();
                        $users = DB::table('users')->where("user_id",$Logistic->exs_logistics_buyer )->first();
                        $frwd = DB::table('forwarder')->where("forwarder_id",$Logistic->ForwarderName )->first();
                        
                        $ship = DB::table('shipping_line')->where("shipping_line_id",$Logistic->ShippingLine )->first();
                        $ship2 = DB::table('shipping_line')->where("shipping_line_id",$Logistic->ShippingLine2 )->count();
                    @endphp

                    <div class="clearfix">&nbsp;</div>
                    <div class="card">
                        <h3 class="card-title clearfix">
                            <div class="mr-auto">Logistic </div>
                            <div class="text-right"><i class="fa fa-sort-desc" aria-hidden="false" style="color:#fff;" data-toggle="collapse" data-target="#collapse3"
                                aria-expanded="true" aria-controls="collapse3"></i></div>
                            
                        </h3>
                        <div class="basic-info-two collapse show"  id="collapse3"  aria-labelledby="headingOne" data-parent="#accordionExample">
                            <table class="table table-bordered">
                                <tr>
                                    <td><b>Forwarder Name : </b> {{$frwd->forwarder_company_name}}</td>
                                     <td><b>Booking Placed Date :</b> {{ date("d-M-Y", strtotime($Logistic->BookingPlacedDate)) }}</td>
                                     <td><b>DO Received Date :</b> {{ date("d-M-Y", strtotime($Logistic->DOReceivedDate)) }}</td>
                                    
                                </tr>
                                <tr>
                                    <td><b>DO Shared With Vendor : </b>{{ date("d-M-Y", strtotime($Logistic->DOSharedWithVendor)) }}</td>
                                    <td><b>Booking Nr. : </b> {{$Logistic->BookingNr}}</td>
                                    <td><b>Shipping Line : </b>{{$ship->shipping_line_name}} </td>
                                    
                                </tr>

                                <tr>
                                    <td><b>DO Expiry Date : </b> {{ date("d-M-Y", strtotime($Logistic->DOExpiryDate)) }}</td>
                                    <td><b>Planned ETD Date : </b>{{ date("d-M-Y", strtotime($Logistic->PlannedETDDAte)) }}</td>
                                    <td><b>Port of Discharge : </b>{{$Logistic->PortofDischarge}} </td>
                                </tr>
                                <tr>
                                    <td><b>2nd Rvsd ETD : </b>{{$Logistic->T2ndRvsdETD}} </td>
                                    <td><b>Container Nr : </b> {{$Logistic->ContainerNr}}</td>
                                    <td><b>Container Pickup Date : </b>  {{ date("d-M-Y", strtotime($Logistic->ContainerPickupDate)) }}</td>
                                    
                                </tr>
                                
                                 <tr>
                                     <td><b>Stuffing Date : </b>  {{ date("d-M-Y", strtotime($Logistic->StuffingDate)) }} </td>
                                     <td><b>SI Submission Dt. by Vendor : </b>   {{ date("d-M-Y", strtotime($Logistic->SISubmissionDtbyVendor)) }} </td>
                                     <td><b>SI Share With Forwarder : </b>  {{ date("d-M-Y", strtotime($Logistic->SIShareWithForwarder)) }} </td>

                                    
                                </tr>
                                <tr>
                                    <td><b>Dispatch : </b> {{$Logistic->Dispatch}}</td>
                                    <td><b>Gate IN : </b> {{$Logistic->GateIN}}</td>
                                    <td><b>Draft BL Received : </b>{{ date("d-M-Y", strtotime($Logistic->DraftBLApprovedbyForwarder)) }}</td>
                                    
                                </tr>
                                <tr>
                                    <td><b>Draft BL Shared with shipper :</b> </td>
                                    <!--<td><b>H/O Location :</b>{{$Logistic->HOLocation}}</td>-->
                                    <td><b>Draft Bl shared with Buyer : </b> {{ date("d-M-Y", strtotime($Logistic->DraftBlsharedwithLIZ)) }}     </td>
                                    <td><b>Draft BL approved with shipper : </b> {{ date("d-M-Y", strtotime($Logistic->DraftBLapprovedwithshipper)) }} </td>
                                    
                                </tr>
                                <tr>
                                    <td><b>Draft Bl Conf Rcvd From Buyer : </b>{{ date("d-M-Y", strtotime($Logistic->DraftBlConfRcvdFromLIZ)) }}    </td> 
                                    <td><b>Draft BL Approved to Forwarder :</b>{{ date("d-M-Y", strtotime($Logistic->DebitNoteRevdFromForwarder)) }}   </td>
                                    <td><b>Debit Note Revd From Forwarder :</b> {{ date("d-M-Y", strtotime($Logistic->DebitNotePaymentDetail)) }}  </td>
                                </tr>
                                 
                                <tr>
                                    <td><b>Debit Note shared with vendor : </b>  {{ date("d-M-Y", strtotime($Logistic->DebitNotesharedwithvendor)) }}   </td>
                                    <td><b>Debit Note Payment Detail received Date : </b>  {{ date("d-M-Y", strtotime($Logistic->DebitNotePaymentDetailreceivedDate)) }}  </td>
                                     <td><b>D. N. Payment Detils Sahred with Forwarder :</b> {{ date("d-M-Y", strtotime($Logistic->DebitNotePaymentDetail)) }}  </td>
                                </tr>
                                
                                <tr>
                                    <td><b>Final BL Rcvd Date : </b>{{ date("d-M-Y", strtotime($Logistic->FinalBLRcvdDate)) }}  </td>
                                    <td><b>Final Bl Shared with Buyer : </b>{{ date("d-M-Y", strtotime($Logistic->FinalBlSharedwithLIZ)) }}  </td>
                                    <td><b>Payment Confirmation Recevied Date : </b></td> 
                                </tr>
                                
                                 <tr>
                                    <td><b>BL Surrender request date : </b>{{ date("d-M-Y", strtotime($Logistic->BLSurrenderrequestdate)) }} </td>
                                    <td><b>BL Surrender copy rcvd : </b>{{ date("d-M-Y", strtotime($Logistic->BLSurrendercopyrcvd)) }}  </td>
                                    <td><b>Final Bl Shared with Buyer : </b>{{ date("d-M-Y", strtotime($Logistic->FinalBlSharedwithLIZ)) }} </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                @else
                
                @endif()
                    
                
                <!--<div class="clearfix">&nbsp;</div>
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">GHP #Accounts</div>
                        <div class="text-right"><i class="fa fa-sort-desc" aria-hidden="false" style="color:#fff;" data-toggle="collapse" data-target="#collapse4"
                                aria-expanded="true" aria-controls="collapse4"></i></div>
                    </h3>
                    
                    <div class="basic-info-two collapse show"  id="collapse4"  aria-labelledby="headingOne" data-parent="#accordionExample">
                        <table class="table table-bordered">
                            <tr>
                                <td><b>GHP Inv Nr. : </b> </td>
                                <td><b>Inv Value : </b> </td>
                                <td><b>Balance Payment : </b> </td>
                            </tr>
                            
                            <tr>
                                <td><b>Discount : </b> </td>
                                <td><b>Receivable Amount : </b> </td>
                                <td><b>Bal. Pay Rcvd Dt. : </b> </td>
                            </tr>
                        </table>
                    </div>
                </div>
                
                
                 <div class="clearfix">&nbsp;</div>
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">Vendor #Accounts</div>
                        <div class="text-right"><i class="fa fa-sort-desc" aria-hidden="false" style="color:#fff;" data-toggle="collapse" data-target="#collapse5"
                                aria-expanded="true" aria-controls="collapse5"></i></div>
                    </h3>
                    
                    <div class="basic-info-two collapse show"  id="collapse5"  aria-labelledby="headingOne" data-parent="#accordionExample">
                        <table class="table table-bordered">
                            <tr>
                                <td><b>GHP Inv Nr. : </b> </td>
                                <td><b>Inv Value : </b> </td>
                                <td><b>Balance Payment : </b> </td>
                            </tr>
                            
                            <tr>
                                <td><b>Discount : </b> </td>
                                <td><b>Receivable Amount : </b> </td>
                                <td><b>Bal. Pay Rcvd Dt. : </b> </td>
                            </tr>
                        </table>
                    </div>
                </div>-->
                
                <!--<div class="clearfix">&nbsp;</div>-->
                <!--<div class="card">-->
                <!--    <h3 class="card-title clearfix">-->
                <!--        <div class="mr-auto">Remarks</div>-->
                <!--    </h3>-->
                    
                <!--    <div class="basic-info-two">-->
                <!--        <table class="table table-bordered">-->
                <!--            <tr>-->
                <!--                <td><b>Wk 01 : </b> </td>-->
                <!--                <td><b>Wk 002 : </b> </td>-->
                <!--                <td><b>Wk 03 : </b> </td>-->
                <!--            </tr>-->
                            
                <!--            <tr>-->
                <!--                <td><b>Wk 04 : </b> </td>-->
                <!--                <td><b>Wk 05 : </b> </td>-->
                <!--                <td><b>Wk 06 : </b> </td>-->
                <!--            </tr>-->
                            
                <!--            <tr>-->
                <!--                <td><b>Wk 07 - Update as on 17/02 : </b> </td>-->
                <!--                <td><b>Wk 08 update : </b> </td>-->
                <!--                <td><b>Wk 09 update : </b> </td>-->
                <!--            </tr>-->
                            
                <!--            <tr>-->
                <!--                <td><b>Wk 10 Update : </b> </td>-->
                <!--                <td><b>Wk 11 Update : </b> </td>-->
                <!--                <td><b>Wk 12 Update (15/03-21/03) : </b> </td>-->
                <!--            </tr>-->
                            
                <!--            <tr>-->
                <!--                <td><b>Wk 13 Update (22/03-28/03) : </b> </td>-->
                <!--                <td><b>Wk 14 Update (29/03 - 04/04) : </b> </td>-->
                <!--                <td><b>Wk 15 Update (05/04-11/04) : </b> </td>-->
                <!--            </tr>-->
                            
                <!--            <tr>-->
                <!--                <td><b>Wk 16 Update (13/04-19/04) : </b> </td>-->
                <!--                <td><b>Wk 17 Update (20/04-25/04) : </b> </td>-->
                <!--                <td><b>Wk 18 Update (27/04-02/05) : </b> </td>-->
                <!--            </tr>-->
                            
                <!--            <tr>-->
                <!--                <td><b>Wk 19 Update (04/05-09/05) : </b> </td>-->
                <!--                <td><b>Wk 20 Update (11/05-16/05) : </b> </td>-->
                                
                <!--            </tr>-->
                             
                <!--        </table>-->
                <!--    </div>-->
                <!--</div>-->
                <div class="clearfix">&nbsp;</div>
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">Remark</div>
                        <div class="text-right"><i class="fa fa-sort-desc" aria-hidden="false" style="color:#fff;" data-toggle="collapse" data-target="#collapse6"
                                aria-expanded="true" aria-controls="collapse6"></i></div>
                        
                    </h3>
                    
                    <div class="basic-info-two  collapse show"  id="collapse6"  aria-labelledby="headingOne" data-parent="#accordionExample">
                        <table class="table table-bordered" id="Remark" class="collapse">
                            <thead>
                              <tr>
                                <th>Date</th>
                                <th>Update</th>
                                <th>Buyer Comment</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>05-02-2020</td>
                                <td>Changes</td>
                                <td>top size change</td>
                              </tr>
                            </tbody>
                          </table>
                    </div>
                </div>
                        
                <div class="clearfix">&nbsp;</div>
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">Comment</div>
                        <div class="text-right"><i class="fa fa-sort-desc"  aria-hidden="false" style="color:#fff;" data-toggle="collapse" data-target="#collapse7"
                                aria-expanded="true" aria-controls="collapse7"></i></div>
                    </h3>
                    <div class="basic-info-two collapse show"  id="collapse7"  aria-labelledby="headingOne" data-parent="#accordionExample">
                        <table class="table table-bordered">
                            <tr>
                                <td><b>Buyer Comments 7/2/20  : </b></td>
                                <td><b>CURRENT STATUS : </b> </td>
                            </tr>
                        </table>
                    </div>
                </div>
               
            </div>
        </div>
    </form>
</div>




<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Revised</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <b>Date : </b> {{ @date("d-M-Y", strtotime(@$Logistic->PlannedETDDate2)) }}
        <p>{{ @$Logistic->T3rdRvsdETDComment }}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>