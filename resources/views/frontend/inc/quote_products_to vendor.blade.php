<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }
</style>
<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Assign Products</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="./quote">Quotation</a></li>
                    <li class="active">Assign Products</li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <!-- <a href="{{ url('user/add/') }}" title="Add New" class="btn btn-default" data-toggle="tooltip"> <i class="icon-plus-circle"></i> Add New</a> -->
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <form method="post" id="vendor_list">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <span>Assign Products</span>
                        <div class="ml-auto"></div>
                        <a href="#save-data" class="ml-auto text-white" title="Assign Products to Vendor"
                            data-toggle="tooltip">
                            <i class="icon-save"></i> Save
                        </a>
                    </h3>
                    <div class="basic-info-two">
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                {!! \Session::get('success') !!}
                            </div>;
                        @endif
                        <div class="table-responsive" style="height: 500px; border: #000 solid 0px;">
                            <table class="table table-bordered table-hover table-header-fix">
                                <thead>
                                    <tr>
                                        <!-- <th style="width: 50px;">
                                            <label class="animated-checkbox">
                                                <input type="checkbox" class="quote_checkall">
                                                <span class="label-text">All</span>
                                            </label>
                                        </th> -->
                                        <th></th>
                                        <th class="text-center" colspan="{{ count($vendors) }}"
                                            style="text-align: center;text-transform: uppercase;">Vendors</th>
                                    </tr>
                                    <tr style="text-align: center;text-transform: uppercase;">
                                        <th>Products Details</th>
                                        @foreach ($vendors as $key => $each)
                                            <th>{{ $each->user_name }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($products as $key => $pro)
                                        @php
                                            $ordered = $pro->qpro_qty;
                                            $dir = 'imgs/products/';
                                            $image = 'imgs/no-image.png';
                                            if (!empty($pro->product_image)) {
                                                $image = $dir . $pro->product_image;
                                            }
                                            $assigned_qty = 0;
                                        @endphp
                                        @foreach ($vendors as $key2 => $each)
                                            @php
                                                $is_assigned = DB::connection('mysql')
                                                    ->table('vendor_enqproducts')
                                                    ->where('vpro_enqid', $each->venq_id)
                                                    ->where('vpro_pid', $pro->product_id)
                                                    ->first();
                                                
                                                $val = $is_assigned ? $is_assigned->vpro_qty : 0;
                                                $assigned_qty += $val;
                                                
                                            @endphp
                                        @endforeach

                                        <tr>
                                            <td>
                                                <img src="{{ url($image) }}" style="max-width: 80px;">
                                                <strong>{{ $pro->product_name }}</strong>
                                                <p><strong>Code: {{ $pro->product_code }}</strong></p>
                                                <div class="row mb-1">
                                                    <div class="col-6">
                                                        <strong>Total Order :</strong>
                                                    </div>
                                                    <div class="col-6">{{ $pro->qpro_qty }}</div>
                                                </div>
                                                <div class="row mb-1">
                                                    <div class="col-6">
                                                        <strong>Total CBM :</strong>
                                                    </div>
                                                    <div class="col-6">{{ $pro->qpro_qty * $pro->product_cbm }}</div>
                                                </div>
                                                <div class="row mb-1">
                                                    <div class="col-6">
                                                        <strong>Order Place :</strong>
                                                    </div>
                                                    <div class="col-6">{{ $assigned_qty }}</div>
                                                </div>
                                                <div class="row mb-1">
                                                    <div class="col-6">
                                                        <strong>Pending Order :</strong>
                                                    </div>
                                                    <div class="col-6 pending_qty_show">
                                                        {{ $pro->qpro_qty - $assigned_qty }}</div>
                                                </div>
                                                <input type="hidden" class="pending_qty"
                                                    value="{{ $pro->qpro_qty - $assigned_qty }}">
                                                <input type="hidden" class="total_qty" value="{{ $pro->qpro_qty }}">
                                            </td>

                                            @foreach ($vendors as $key2 => $each)
                                                @php
                                                    $is_assigned = DB::connection('mysql')
                                                        ->table('vendor_enqproducts')
                                                        ->where('vpro_enqid', $each->venq_id)
                                                        ->where('vpro_pid', $pro->product_id)
                                                        ->first();
                                                    
                                                    $val = $is_assigned ? $is_assigned->vpro_qty : 0;
                                                @endphp
                                                <td class="text-center">
                                                    <label class="animated-checkbox">
                                                        <input type="checkbox" name="check[{{ $pro->product_id }}][]"
                                                            value="{{ $each->user_id }}"
                                                            @if ($is_assigned) checked @endif
                                                            class="quote_check">
                                                        <span class="label-text"></span>
                                                    </label>
                                                    <input style="margin-top: 8px;" type="number" min="0"
                                                        name="pro_qty[{{ $pro->product_id }}][{{ $each->user_id }}][]"
                                                        value="{{ $val }}" class="form-control qty_assigned"
                                                        placeholder="Quantity">
                                                </td>
                                            @endforeach
                                        </tr>

                                        @php
                                            $ParentProduct = DB::table('products')
                                                ->where('parent_product', $pro->product_id)
                                                ->orderBy('product_name')
                                                ->get();
                                        @endphp


                                        @foreach ($ParentProduct as $key => $PrntPro)
                                            @php
                                                $ordered = $pro->qpro_qty;
                                                
                                                $dir = 'imgs/products/';
                                                $image = 'imgs/no-image.png';
                                                if (!empty($PrntPro->product_image)) {
                                                    $image = $dir . $PrntPro->product_image;
                                                }
                                                $assigned_qty = 0;
                                            @endphp
                                            @foreach ($vendors as $key2 => $each)
                                                @php
                                                    $is_assigned = DB::connection('mysql')
                                                        ->table('vendor_enqproducts')
                                                        ->where('vpro_enqid', $each->venq_id)
                                                        ->where('vpro_pid', $PrntPro->product_id)
                                                        ->first();
                                                    
                                                    $val = $is_assigned ? $is_assigned->vpro_qty : 0;
                                                    $assigned_qty += $val;
                                                    
                                                @endphp
                                            @endforeach

                                            <tr>
                                                <td>
                                                    <img src="{{ url($image) }}" style="max-width: 80px;">
                                                    <strong>{{ $PrntPro->product_name }}</strong>
                                                    <p><strong>Code: {{ $PrntPro->product_code }}</strong></p>
                                                    <div class="row mb-1">
                                                        <div class="col-6">
                                                            <strong>Total Order :</strong>
                                                        </div>
                                                        <div class="col-6">{{ $ordered }}</div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <div class="col-6">
                                                            <strong>Total CBM :</strong>
                                                        </div>
                                                        <div class="col-6">{{ $ordered * $PrntPro->product_cbm }}
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <div class="col-6">
                                                            <strong>Order Place :</strong>
                                                        </div>
                                                        <div class="col-6">{{ $assigned_qty }}</div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <div class="col-6">
                                                            <strong>Pending Order :</strong>
                                                        </div>
                                                        <div class="col-6 pending_qty_show">
                                                            {{ $ordered - $assigned_qty }}</div>
                                                    </div>
                                                    <input type="hidden" class="pending_qty"
                                                        value="{{ $ordered - $assigned_qty }}">
                                                </td>

                                                @foreach ($vendors as $key2 => $each)
                                                    @php
                                                        $is_assigned = DB::connection('mysql')
                                                            ->table('vendor_enqproducts')
                                                            ->where('vpro_enqid', $each->venq_id)
                                                            ->where('vpro_pid', $PrntPro->product_id)
                                                            ->first();
                                                        
                                                        $val = $is_assigned ? $is_assigned->vpro_qty : 0;
                                                    @endphp
                                                    <td class="text-center">
                                                        <label class="animated-checkbox">
                                                            <input type="checkbox"
                                                                name="check[{{ $PrntPro->product_id }}][]"
                                                                value="{{ $each->user_id }}"
                                                                @if ($is_assigned) checked @endif
                                                                class="quote_check">
                                                            <span class="label-text"></span>
                                                        </label>
                                                        <input style="margin-top: 8px;" type="text"
                                                            name="pro_qty[{{ $PrntPro->product_id }}][{{ $each->user_id }}][]"
                                                            value="{{ $val }}"
                                                            class="form-control qty_assigned" placeholder="Quantity">
                                                    </td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function() {

        $(".qty_assigned").on('keyup', function() {
            var totalInput = 0;
            var foo = $(this);
            var inputVal = parseInt(foo.val());
            if (foo.val() == '') {
                inputVal = 0;
            }
            foo.val(inputVal);

            var row = foo.closest('tr');
            $(row.find(".qty_assigned")).each(function() {
                totalInput += parseInt($(this).val());
            });
            var pending_qty = parseInt(row.find('.total_qty').val());

            var balance = 0;
            // $.each(row.)
            if (totalInput > pending_qty) {
                inputVal = 0;
                foo.val(0);
            } else {

                balance = pending_qty - totalInput;
                row.find('.pending_qty_show').text(parseInt(balance));

            }
        });
    });
</script>
