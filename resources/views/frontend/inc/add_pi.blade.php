<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>{{ !empty($edit->pi_id) ? 'Edit' : 'Add' }} Performa Invoice</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="{{ url('quote') }}">Quote</a></li>
                    <li class="active">{{ !empty($edit->pi_id) ? 'Revise' : 'Add' }} Quote</li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <a href="{{ url('quote') }}" title="View Quote" class="btn btn-default" data-toggle="tooltip"> <i class="icon-eye"></i> View Quote</a>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">

    @if (\Session::has('danger'))
        <div class="alert alert-danger">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <form data-session="pi_cart" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                	<h3 class="card-title">
                		<div class="mr-auto">PI Details</div>
                        <div class="ml-auto">
                            <a href="#save-data" class="text-white" title="Save" data-toggle="tooltip"> <i class="icon-save"></i> Save</a>
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i class="icon-refresh"></i> </a>
                        </div>
                	</h3>
                	<div class="row">
                		<div class="col">
                			<div class="form-group">
                                <label>Quotation</label>
                    			<select class="form-control" name="record[pi_qid]">
                                    <option value="">Select Quotation</option>
                                    @if(!$quotes->isEmpty())
                                        @foreach($quotes as $q)
                                            <option value="{{ $q->quote_id }}" @if($q->quote_id == $quote_id) selected @endif>{{ sprintf("GHP_QT_%06d", $q->quote_id) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                		</div>
                        <div class="col">
                            <div class="form-group">
                                <label>PI Date</label>
                                <input type="date" name="record[pi_date]" value="{{ !empty($edit->pi_date) ? $edit->pi_date : '' }}" class="form-control" required >
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Delivery Date</label>
                                <input type="date" name="record[pi_delivery_date]" value="{{ !empty($edit->pi_delivery_date) ? $edit->pi_delivery_date : '' }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Order Ref.</label>
                                <input type="text" name="record[pi_order_ref]" value="{{ @$edit->pi_order_ref }}" class="form-control">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <div class="d-block">
                                    <label>
                                        <input type="checkbox" name="record[pi_show_nw]" value="Y" {{ !empty($edit->pi_show_nw) ? $edit->pi_show_nw == 'Y' ? 'checked' : ''  : '' }}>
                                        Show NW
                                    </label>
                                    <span>&nbsp; &nbsp;</span>
                                    <label>
                                        <input type="checkbox" name="record[pi_show_gw]" value="Y" {{ !empty($edit->pi_show_gw) ? $edit->pi_show_gw == 'Y' ? 'checked' : ''  : '' }}>
                                        Show GW
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Buyer Note</label>
                                <input type="text" name="record[buyer_note]" value="{{ !empty($edit->buyer_note) ? $edit->buyer_note : '' }}" class="form-control" required>
                            </div>
                        </div>
                        
                        <div class="col">
                            <div class="form-group">
                                <label>Product Remark</label>
                                <input type="text" name="record[product_remark]" value="{{ !empty($edit->product_remark) ? $edit->product_remark : '' }}" class="form-control" required>
                            </div>
                        </div>
                	</div>
                	
                	
                	<div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Container 1</label>
                                <input type="text" name="record[container1]" value="{{ !empty($edit->container1) ? $edit->container1 : '' }}" class="form-control">
                            </div>
                        </div>
                        
                        <div class="col">
                            <div class="form-group">
                                <label>Quantity 1</label>
                                <input type="text" name="record[qty1]" value="{{ !empty($edit->qty1) ? $edit->qty1 : '' }}" class="form-control">
                            </div>
                        </div>
                        
                        <div class="col">
                            <div class="form-group">
                                <label>Container 2</label>
                                <input type="text" name="record[container2]" value="{{ !empty($edit->container2) ? $edit->container2 : '' }}" class="form-control">
                            </div>
                        </div>
                        
                        <div class="col">
                            <div class="form-group">
                                <label>Quantity 2</label>
                                <input type="text" name="record[qty2]" value="{{ !empty($edit->qty2) ? $edit->qty2 : '' }}" class="form-control">
                            </div>
                        </div>
                        
                        
                        <div class="col">
                            <div class="form-group">
                                <label>Container 3</label>
                                <input type="text" name="record[container3]" value="{{ !empty($edit->container3) ? $edit->container3 : '' }}" class="form-control">
                            </div>
                        </div>
                        
                        <div class="col">
                            <div class="form-group">
                                <label>Quantity 3</label>
                                <input type="text" name="record[qty3]" value="{{ !empty($edit->qty3) ? $edit->qty3 : '' }}" class="form-control">
                            </div>
                        </div>
                	</div>
                	
                </div>
                <div class="card">
                	<h3 class="card-title">Product / Item Details</h3>
                    <div id="cartResponse">
                        @php $total_field = 'pi_total'; $total_cbm_field = 'pi_total_cbm'; $cart_name = 'pi_cart'; $uid = $quote->quote_uid @endphp
                        @include('frontend.template.pi_cart_table')
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
