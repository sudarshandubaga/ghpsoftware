<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Add {{ $role_name }}</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="{{ url('user/'.$role) }}">{{ $role_name }}</a></li>
                    <li class="active">Add {{ $role_name }}</li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <a href="{{ url('user/'.$role) }}" title="View {{ $role_name }}" class="btn btn-default" data-toggle="tooltip"> <i class="icon-eye"></i> View {{ $role_name }}</a>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <form method="post" enctype="multipart/form-data">
        @csrf
        @if (\Session::has('danger'))
            <div class="alert alert-danger">
                {!! \Session::get('danger') !!}</li>
            </div>
        @endif
        <div class="card">
        	<h3 class="card-title">
        		<div class="mr-auto">Login Details</div>
                <div class="ml-auto">
                    <a href="#save-data" class="text-white" title="Save" data-toggle="tooltip"> <i class="icon-save"></i> Save</a>
                    &nbsp;
                    <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i class="icon-refresh"></i> </a>
                </div>
        	</h3>
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>Username</label>
            			<input type="text" name="user[user_login]" placeholder="Username" class="form-control" required autocomplete="off" value="{{ @$edit->user_login }}" @if(!empty($edit->user_login)) readonly @endif>
                    </div>
        		</div>
                <div class="col">
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="user[password]" placeholder="Password" class="form-control" @if(empty($edit->password)) required @endif autocomplete="new-password">
                    </div>
                </div>
        	</div>

            <div class="row">
                <div class="col">

                    <div class="form-group">
                        <label>Vendor Code</label>
                        <input type="text" name="vendor[vendor_code]" placeholder="Vendor Code" class="form-control" required autocomplete="off" value="{{ @$vendor_edit->vendor_code }}">
                    </div>
                </div>

                <div class="col">
                    <div class="form-group">
                        <label>Vendor Short Name</label>
                        <input type="text" name="vendor[vendor_short_name]" placeholder="Vendor Short Name" class="form-control" required autocomplete="off" value="{{ @$vendor_edit->vendor_short_name }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
        	<h3 class="card-title">Company Information</h3>
            <div class="row">
            	<div class="col-3">
            		<label>Organisation Name</label>
            	</div>
            	<div class="col-9">
            		<div class="form-group">
            			<input type="text" name="vendor[vendor_org_name]" value="{{ @$vendor_edit->vendor_org_name }}" class="form-control">
            		</div>
            	</div>
            </div>
            <div class="row">
            	<div class="col-3">
            		<label>Address</label>
            	</div>
            	<div class="col-9">
            		<div class="form-group">
            			<input type="text" name="vendor[vendor_address]" value="{{ @$vendor_edit->vendor_address }}" class="form-control">
            		</div>
            	</div>
            </div>
            <div class="row">
            	<div class="col-3">
            		<label>City</label>
            	</div>
            	<div class="col-3">
            		<div class="form-group">
            			<input type="text" name="vendor[vendor_city]" value="{{ @$vendor_edit->vendor_city }}" class="form-control">
            		</div>
            	</div>
            	<div class="col-3 text-center">
            		<label>State / Region</label>
            	</div>
            	<div class="col-3">
            		<div class="form-group">
            			<input type="text" name="vendor[vendor_state]" value="{{ @$vendor_edit->vendor_state }}" class="form-control">
            		</div>
            	</div>
            </div>
            <div class="row">
            	<div class="col-3">
            		<label>Telephone Number</label>
            	</div>
            	<div class="col-9">
            		<div class="form-group">
            			<input type="text" name="vendor[vendor_tel]" value="{{ @$vendor_edit->vendor_tel }}" class="form-control">
            		</div>
            	</div>
            </div>
            <div class="row">
            	<div class="col-3">
            		<label>E-mail Address</label>
            	</div>
            	<div class="col-9">
            		<div class="form-group">
            			<input type="text" name="vendor[vendor_email]" value="{{ @$vendor_edit->vendor_email }}" class="form-control">
            		</div>
            	</div>
            </div>
            <div class="row">
            	<div class="col-3">
            		<label>Company Type</label>
            	</div>
            	<div class="col-9">
            		<div class="form-group">
            			<select name="vendor[vendor_company_type]" class="form-control" placeholder="COMPANY TYPE" required>
                            <option value="">COMPANY TYPE</option>
                            <option value="Partnership" @if(@$vendor_edit->vendor_company_type == "Partnership") selected @endif>Partnership</option>
                            <option value="Limited" @if(@$vendor_edit->vendor_company_type == "Limited") selected @endif>Limited</option>
                            <option value="Proprietorship" @if(@$vendor_edit->vendor_company_type == "Proprietorship") selected @endif>Proprietorship</option>
                        </select>
            		</div>
            	</div>
            </div>
            <div class="row">
            	<div class="col-3">
            		<label>Website</label>
            	</div>
            	<div class="col-9">
            		<div class="form-group">
            			<input type="text" name="vendor[vendor_website]" value="{{ @$vendor_edit->vendor_website }}" class="form-control">
            		</div>
            	</div>
            </div>
        </div>
        <div class="card">
        	<h3 class="card-title">Contact Person</h3>
        	<div class="row">
        		<div class="col-1">1</div>
        		<div class="col">
        			<div class="form-group">
        				<input type="text" name="vendor[vendor_contact_name1]" value="{{ @$vendor_edit->vendor_contact_name1 }}" class="form-control" placeholder="Contact Person Name">
        			</div>
        		</div>
        		<div class="col">
        			<div class="form-group">
        				<input type="text" name="vendor[vendor_contact_designation1]" value="{{ @$vendor_edit->vendor_contact_designation1 }}" class="form-control" placeholder="Designation">
        			</div>
        		</div>
        		<div class="col">
        			<div class="form-group">
        				<input type="text" name="vendor[vendor_contact_mobile1]" value="{{ @$vendor_edit->vendor_contact_mobile1 }}" class="form-control" placeholder="Mobile No.">
        			</div>
        		</div>
        		<div class="col">
        			<div class="form-group">
        				<input type="text" name="vendor[vendor_contact_email1]" value="{{ @$vendor_edit->vendor_contact_email1 }}" class="form-control" placeholder="E-mail Address">
        			</div>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-1">2</div>
        		<div class="col">
        			<div class="form-group">
        				<input type="text" name="vendor[vendor_contact_name2]" value="{{ @$vendor_edit->vendor_contact_name2 }}" class="form-control" placeholder="Contact Person Name">
        			</div>
        		</div>
        		<div class="col">
        			<div class="form-group">
        				<input type="text" name="vendor[vendor_contact_designation2]" value="{{ @$vendor_edit->vendor_contact_designation2 }}" class="form-control" placeholder="Designation">
        			</div>
        		</div>
        		<div class="col">
        			<div class="form-group">
        				<input type="text" name="vendor[vendor_contact_mobile2]" value="{{ @$vendor_edit->vendor_contact_mobile2 }}" class="form-control" placeholder="Mobile No.">
        			</div>
        		</div>
        		<div class="col">
        			<div class="form-group">
        				<input type="text" name="vendor[vendor_contact_email2]" value="{{ @$vendor_edit->vendor_contact_email2 }}" class="form-control" placeholder="E-mail Address">
        			</div>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-1">3</div>
        		<div class="col">
        			<div class="form-group">
        				<input type="text" name="vendor[vendor_contact_name3]" value="{{ @$vendor_edit->vendor_contact_name3 }}" class="form-control" placeholder="Contact Person Name">
        			</div>
        		</div>
        		<div class="col">
        			<div class="form-group">
        				<input type="text" name="vendor[vendor_contact_designation3]" value="{{ @$vendor_edit->vendor_contact_designation3 }}" class="form-control" placeholder="Designation">
        			</div>
        		</div>
        		<div class="col">
        			<div class="form-group">
        				<input type="text" name="vendor[vendor_contact_mobile3]" value="{{ @$vendor_edit->vendor_contact_mobile3 }}" class="form-control" placeholder="Mobile No.">
        			</div>
        		</div>
        		<div class="col">
        			<div class="form-group">
        				<input type="text" name="vendor[vendor_contact_email3]" value="{{ @$vendor_edit->vendor_contact_email3 }}" class="form-control" placeholder="E-mail Address">
        			</div>
        		</div>
        	</div>
        </div>
        <div class="card">
        	<h3 class="card-title">Production Facilities</h3>
        	<div class="row">
        		<div class="col">
        			<h6 class="text-center">Unit - 01</h6>
        			<hr>
        			<textarea name="vendor[vendor_facility_unit1]" rows="7" class="form-control">{{ @$vendor_edit->vendor_facility_unit1 }}</textarea>
        		</div>
        		<div class="col">
        			<h6 class="text-center">Unit - 02</h6>
        			<hr>
        			<textarea name="vendor[vendor_facility_unit2]" rows="7" class="form-control">{{ @$vendor_edit->vendor_facility_unit2 }}</textarea>
        		</div>
        	</div>
        </div>
        <div class="card">
        	<h3 class="card-title">Legal Information</h3>
            <div class="row">
            	<div class="col-3">
            		<label>Import-Export License No.</label>
            	</div>
            	<div class="col-9">
            		<div class="form-group">
            			<input type="text" name="vendor[vendor_iel_no]" value="{{ @$vendor_edit->vendor_iel_no }}" class="form-control">
            		</div>
            	</div>
            </div>
            <div class="row">
            	<div class="col-3">
            		<label>VAT / GST Registration No.</label>
            	</div>
            	<div class="col-9">
            		<div class="form-group">
            			<input type="text" name="vendor[vendor_gstin]" value="{{ @$vendor_edit->vendor_gstin }}" class="form-control">
            		</div>
            	</div>
            </div>
            <div class="row">
            	<div class="col-3">
            		<label>TIN / TAN Number</label>
            	</div>
            	<div class="col-9">
            		<div class="form-group">
            			<input type="text" name="vendor[vendor_tin]" value="{{ @$vendor_edit->vendor_tin }}" class="form-control">
            		</div>
            	</div>
            </div>
            <div class="row">
            	<div class="col-3">
            		<label>REX No</label>
            	</div>
            	<div class="col-9">
            		<div class="form-group">
            			<input type="text" name="vendor[vendor_other]" value="{{ @$vendor_edit->vendor_other }}" class="form-control">
            		</div>
            	</div>
            </div>
        </div>
        <div class="card">
        	<h3 class="card-title">Annual Turnover</h3>
        	<div class="row">
    			<div class="col">
    				<div class="form-group">
    					<label>INR</label>
    					<input type="text" name="vendor[vendor_turnover_inr]" value="{{ @$vendor_edit->vendor_turnover_inr }}" class="form-control">
    				</div>
    			</div>
    			<div class="col">
    				<div class="form-group">
    					<label>USD</label>
    					<input type="text" name="vendor[vendor_turnover_usd]" value="{{ @$vendor_edit->vendor_turnover_usd }}" class="form-control">
    				</div>
    			</div>
    			<div class="col">
    				<div class="form-group">
    					<label>EURO</label>
    					<input type="text" name="vendor[vendor_turnover_eur]" value="{{ @$vendor_edit->vendor_turnover_eur }}" class="form-control">
    				</div>
    			</div>
    			<div class="col">
    				<div class="form-group">
    					<label>POUND</label>
    					<input type="text" name="vendor[vendor_turnover_pound]" value="{{ @$vendor_edit->vendor_turnover_pound }}" class="form-control">
    				</div>
    			</div>
    		</div>
        </div>
        <div class="card">
        	<h3 class="card-title">Production Capacity</h3>
            <div class="row">
            	<div class="col-3">
            		<label>Annual Production Capaciy (Volume)</label>
            	</div>
            	<div class="col-9">
            		<div class="form-group">
            			<input type="text" name="vendor[vendor_annual_prod_capacity]" value="{{ @$vendor_edit->vendor_annual_prod_capacity }}" class="form-control">
            		</div>
            	</div>
            </div>
            <div class="row">
            	<div class="col-3">
            		<label>Monthly Production Capaciy (Volume)</label>
            	</div>
            	<div class="col-9">
            		<div class="form-group">
            			<input type="text" name="vendor[vendor_monthly_prod_capacity]" value="{{ @$vendor_edit->vendor_monthly_prod_capacity }}" class="form-control">
            		</div>
            	</div>
            </div>
        </div>
        <div class="card">
        	<h3 class="card-title">Equipment Facility</h3>
        	<div class="row">
        		<div class="col-1"></div>
        		<div class="col text-center">
        			<div class="form-group">RAW Production</div>
        		</div>
        		<div class="col text-center">
        			<div class="form-group">Polish Finishing</div>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-1">1</div>
        		<div class="col">
        			<div class="row">
        				<div class="col">
        					<div class="form-group">
        						<input type="text" name="vendor[vendor_raw_process1]" value="{{ @$vendor_edit->vendor_raw_process1 }}" class="form-control" placeholder="Process">
        					</div>
        				</div>
        				<div class="col">
        					<div class="form-group">
            					<select name="vendor[vendory_raw_machinery_no1]" class="form-control">
                                    <option value="">No. of machinery</option>
            						@for($i = 1; $i <= 20; $i++)
            						<option value="{{ $i }}" @if(@$vendor_edit->vendory_raw_machinery_no1 == $i) selected @endif>{{ $i }}</option>
            						@endfor
            					</select>
            				</div>
        				</div>
        			</div>
        		</div>
        		<div class="col">
        			<div class="row">
        				<div class="col">
        					<div class="form-group">
        					   <input type="text" name="vendor[vendor_polish_process1]" value="{{ @$vendor_edit->vendor_polish_process1 }}" class="form-control" placeholder="Process">
                            </div>
        				</div>
        				<div class="col">
        					<div class="form-group">
            					<select name="vendor[vendory_polish_machinery_no1]" class="form-control">
                                    <option value="">No. of machinery</option>
            						@for($i = 1; $i <= 20; $i++)
            						<option value="{{ $i }}" @if(@$vendor_edit->vendory_polish_machinery_no1 == $i) selected @endif>{{ $i }}</option>
            						@endfor
            					</select>
                            </div>
        				</div>
        			</div>
        		</div>
        	</div>
            <div class="row">
                <div class="col-1">2</div>
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input type="text" name="vendor[vendor_raw_process2]" value="{{ @$vendor_edit->vendor_raw_process2 }}" class="form-control" placeholder="Process">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <select name="vendor[vendory_raw_machinery_no2]" class="form-control">
                                    <option value="">No. of machinery</option>
                                    @for($i = 1; $i <= 20; $i++)
                                    <option value="{{ $i }}" @if(@$vendor_edit->vendory_raw_machinery_no2 == $i) selected @endif>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                               <input type="text" name="vendor[vendor_polish_process2]" value="{{ @$vendor_edit->vendor_polish_process2 }}" class="form-control" placeholder="Process">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <select name="vendor[vendory_polish_machinery_no2]" class="form-control">
                                    <option value="">No. of machinery</option>
                                    @for($i = 1; $i <= 20; $i++)
                                    <option value="{{ $i }}" @if(@$vendor_edit->vendory_polish_machinery_no2 == $i) selected @endif>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-1">3</div>
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input type="text" name="vendor[vendor_raw_process3]" value="{{ @$vendor_edit->vendor_raw_process3 }}" class="form-control" placeholder="Process">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <select name="vendor[vendory_raw_machinery_no3]" class="form-control">
                                    <option value="">No. of machinery</option>
                                    @for($i = 1; $i <= 20; $i++)
                                    <option value="{{ $i }}" @if(@$vendor_edit->vendory_raw_machinery_no3 == $i) selected @endif>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                               <input type="text" name="vendor[vendor_polish_process3]" value="{{ @$vendor_edit->vendor_polish_process3 }}" class="form-control" placeholder="Process">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <select name="vendor[vendory_polish_machinery_no3]" class="form-control">
                                    <option value="">No. of machinery</option>
                                    @for($i = 1; $i <= 20; $i++)
                                    <option value="{{ $i }}" @if(@$vendor_edit->vendory_polish_machinery_no3 == $i) selected @endif>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-1">4</div>
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input type="text" name="vendor[vendor_raw_process4]" value="{{ @$vendor_edit->vendor_raw_process4 }}" class="form-control" placeholder="Process">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <select name="vendor[vendory_raw_machinery_no4]" class="form-control">
                                    <option value="">No. of machinery</option>
                                    @for($i = 1; $i <= 20; $i++)
                                    <option value="{{ $i }}" @if(@$vendor_edit->vendory_raw_machinery_no4 == $i) selected @endif>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                               <input type="text" name="vendor[vendor_polish_process4]" value="{{ @$vendor_edit->vendor_polish_process4 }}" class="form-control" placeholder="Process">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <select name="vendor[vendory_polish_machinery_no4]" class="form-control">
                                    <option value="">No. of machinery</option>
                                    @for($i = 1; $i <= 20; $i++)
                                    <option value="{{ $i }}" @if(@$vendor_edit->vendory_polish_machinery_no4 == $i) selected @endif>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <h3 class="card-title">Certificate Held</h3>
            <div class="row">
                <div class="col-lg-2 col-sm-3">
                    <label><input type="checkbox" name="vendor[vendor_certificate_fsc]" value="Y" @if(@$vendor_edit->vendor_certificate_fsc == "Y") checked @endif> FSC</label>
                    <div class="form-group"><input type="file" name="vendor_certificate_fsc_file" class="form-control"></div>
                    @if(!empty($vendor_edit->vendor_certificate_fsc_file))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_certificate_fsc_file) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
                <div class="col-lg-2 col-sm-3">
                    <label><input type="checkbox" name="vendor[vendor_certificate_iso]" value="Y" @if(@$vendor_edit->vendor_certificate_iso == "Y") checked @endif> ISO</label>
                    <div class="form-group"><input type="file" name="vendor_certificate_iso_file" class="form-control"></div>
                    @if(!empty($vendor_edit->vendor_certificate_iso_file))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_certificate_iso_file) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
                <div class="col-lg-2 col-sm-3">
                    <label><input type="checkbox" name="vendor[vendor_certificate_vriksh]" value="Y" @if(@$vendor_edit->vendor_certificate_vriksh == "Y") checked @endif> VRIKSH</label>
                    <div class="form-group"><input type="file" name="vendor_certificate_vriksh_file" class="form-control"></div>
                    @if(!empty($vendor_edit->vendor_certificate_vriksh_file))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_certificate_vriksh_file) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
                <div class="col-lg-2 col-sm-3">
                    <label><input type="checkbox" name="vendor[vendor_certificate_bsi]" value="Y" @if(@$vendor_edit->vendor_certificate_bsi == "Y") checked @endif> BSI</label>
                    <div class="form-group"><input type="file" name="vendor_certificate_bsi_file" class="form-control"></div>
                    @if(!empty($vendor_edit->vendor_certificate_bsi_file))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_certificate_bsi_file) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
                <div class="col-lg-2 col-sm-3">
                    <label><input type="checkbox" name="vendor[vendor_certificate_reach]" value="Y" @if(@$vendor_edit->vendor_certificate_reach == "Y") checked @endif> REACH</label>
                    <div class="form-group"><input type="file" name="vendor_certificate_reach_file" class="form-control"></div>
                    @if(!empty($vendor_edit->vendor_certificate_reach_file))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_certificate_reach_file) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
                <div class="col-lg-2 col-sm-3">
                    <label><input type="checkbox" name="vendor[vendor_certificate_ce]" value="Y" @if(@$vendor_edit->vendor_certificate_ce == "Y") checked @endif> CE</label>
                    <div class="form-group"><input type="file" name="vendor_certificate_ce_file" class="form-control"></div>
                    @if(!empty($vendor_edit->vendor_certificate_ce_file))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_certificate_ce_file) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
                <div class="col-lg-2 col-sm-3">
                    <label><input type="checkbox" name="vendor[vendor_certificate_cites]" value="Y" @if(@$vendor_edit->vendor_certificate_cites == "Y") checked @endif> CITES</label>
                    <div class="form-group"><input type="file" name="vendor_certificate_cites_file" class="form-control"></div>
                    @if(!empty($vendor_edit->vendor_certificate_cites_file))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_certificate_cites_file) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
                <div class="col-lg-2 col-sm-3">
                    <label><input type="checkbox" name="vendor[vendor_certificate_pefc]" value="Y" @if(@$vendor_edit->vendor_certificate_pefc == "Y") checked @endif> PEFC</label>
                    <div class="form-group"><input type="file" name="vendor_certificate_pefc_file" class="form-control"></div>
                    @if(!empty($vendor_edit->vendor_certificate_pefc_file))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_certificate_pefc_file) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
                <div class="col-lg-2 col-sm-3">
                    <label><input type="checkbox" name="vendor[vendor_certificate_lacey_act]" value="Y" @if(@$vendor_edit->vendor_certificate_lacey_act == "Y") checked @endif> LACEY ACT</label>
                    <div class="form-group"><input type="file" name="vendor_certificate_lacey_act_file" class="form-control"></div>
                    @if(!empty($vendor_edit->vendor_certificate_lacey_act_file))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_certificate_lacey_act_file) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
                <div class="col-lg-2 col-sm-3">
                    <label><input type="checkbox" name="vendor[vendor_certificate_phytosanitary]" value="Y" @if(@$vendor_edit->vendor_certificate_phytosanitary == "Y") checked @endif> Phytosanitary Certificate</label>
                    <div class="form-group"><input type="file" name="vendor_certificate_phytosanitary_file" class="form-control"></div>
                    @if(!empty($vendor_edit->vendor_certificate_phytosanitary_file))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_certificate_phytosanitary_file) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
            </div>
        </div>
        <div class="card">
            <h3 class="card-title">RCMC - Registration-Cum-Membership Certificate</h3>
            <div class="row">
                <div class="col-1">
                    <input type="checkbox" name="vendor[vendor_epch]" value="Y" @if(@$vendor_edit->vendor_epch == "Y") checked @endif>
                </div>
                <div class="col">
                    EPCH - Export Promotion Council for Handicrafts
                </div>
                <div class="col">
                    <div class="form-group">
                        <input type="file" name="vendor_epch_file" class="form-control">
                    </div>
                    @if(!empty($vendor_edit->vendor_epch_file))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_epch_file) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-1">
                    <input type="checkbox" name="vendor[vendor_ceph]" value="Y" @if(@$vendor_edit->vendor_ceph == "Y") checked @endif>
                </div>
                <div class="col">
                    CEPH - Carpet Export Promotion Council
                </div>
                <div class="col">
                    <div class="form-group">
                        <input type="file" name="vendor_ceph_file" class="form-control">
                    </div>
                    @if(!empty($vendor_edit->vendor_ceph_file))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_ceph_file) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-1">
                    <input type="checkbox" name="vendor[vendor_cle]" value="Y" @if(@$vendor_edit->vendor_cle == "Y") checked @endif>
                </div>
                <div class="col">
                    CLE - Council for Leather Exports
                </div>
                <div class="col">
                    <div class="form-group">
                        <input type="file" name="vendor_cle_file" class="form-control">
                    </div>
                    @if(!empty($vendor_edit->vendor_cle_file))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_cle_file) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-1">
                    <input type="checkbox" name="vendor[vendor_fieo]" value="Y" @if(@$vendor_edit->vendor_fieo == "Y") checked @endif>
                </div>
                <div class="col">
                    FIEO - Federation of Indian Export Organisations
                </div>
                <div class="col">
                    <div class="form-group">
                        <input type="file" name="vendor_fieo_file" class="form-control">
                    </div>
                    @if(!empty($vendor_edit->vendor_fieo_file))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_fieo_file) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-1">
                    <input type="checkbox" name="vendor[vendor_gjepc]" value="Y" @if(@$vendor_edit->vendor_gjepc == "Y") checked @endif>
                </div>
                <div class="col">
                    GJEPC - Gem & Jewellery Export Promotion Council
                </div>
                <div class="col">
                    <div class="form-group">
                        <input type="file" name="vendor_gjepc_file" class="form-control">
                    </div>
                    @if(!empty($vendor_edit->vendor_gjepc_file))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_gjepc_file) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-1">
                    OTHER -
                </div>
                <div class="col">
                    <input type="text" name="vendor[vendor_other_auth_name1]" class="form-control" value="{{ @$vendor_edit->vendor_other_auth_name1 }}" placeholder="Name of issuing authority"></input>
                </div>
                <div class="col">
                    <div class="form-group">
                        <input type="file" name="vendor_other_auth_file1" class="form-control">
                    </div>
                    @if(!empty($vendor_edit->vendor_other_auth_file1))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_other_auth_file1) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-1">

                </div>
                <div class="col">
                    <input type="text" name="vendor[vendor_other_auth_name2]" class="form-control" value="{{ @$vendor_edit->vendor_other_auth_name2 }}" placeholder="Name of issuing authority"></input>
                </div>
                <div class="col">
                    <div class="form-group">
                        <input type="file" name="vendor_other_auth_file2" class="form-control">
                    </div>
                    @if(!empty($vendor_edit->vendor_other_auth_file2))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_other_auth_file2) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
            </div>
        </div>
        <div class="card">
            <h3 class="card-title">Compliance</h3>
            <div class="row">
                <div class="col-1">
                    1.
                </div>
                <div class="col">
                    BSCI - The Business Social Compliance Initiative
                </div>
                <div class="col">
                    <div class="form-group">
                        <input type="file" name="vendor_compliance_bsci" class="form-control">
                    </div>
                    @if(!empty($vendor_edit->vendor_compliance_bsci))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_compliance_bsci) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-1">
                    2.
                </div>
                <div class="col">
                    SEDEX - The Supplier Ethical Data Exchange (SMETA)
                </div>
                <div class="col">
                    <div class="form-group">
                        <input type="file" name="vendor_compliance_sedex" class="form-control">
                    </div>
                    @if(!empty($vendor_edit->vendor_compliance_sedex))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_compliance_sedex) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-1">
                    3.
                </div>
                <div class="col">
                    ISO Certification
                </div>
                <div class="col">
                    <div class="form-group">
                        <input type="file" name="vendor_compliance_iso" class="form-control">
                    </div>
                    @if(!empty($vendor_edit->vendor_compliance_iso))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_compliance_iso) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-1">
                    Other
                </div>
                <div class="col">
                    <input type="text" name="vendor_compliance_auth_name" class="form-control" value="{{ @$vendor_edit->vendor_compliance_auth_name }}" placeholder="Name of issuing authority">
                </div>
                <div class="col">
                    <div class="form-group">
                        <input type="file" name="vendor_compliance_other" class="form-control">
                    </div>
                    @if(!empty($vendor_edit->vendor_epch_file))
                        <div><a href="{{ url('files/vendors/'.$vendor_edit->vendor_epch_file) }}" target="_blank">VIEW / DOWNLOAD</a></div>
                    @endif
                </div>
            </div>
        </div>
        <div class="card">
            <h3 class="card-title">Bank Details</h3>
            <div class="row">
                <div class="col">
                    <div>
                        <h5>Company Bank Detail For Remittance</h5>
                        <div class="form-group">
                            <label>Bank Name</label>
                            <input type="text" name="vendor[vendor_bank_name]" placeholder="Bank Name" value="{{ @$vendor_edit->vendor_bank_name }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Bank Address</label>
                            <input type="text" name="vendor[vendor_bank_addr]" placeholder="Bank Address" value="{{ @$vendor_edit->vendor_bank_addr }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Bank A/C No.</label>
                            <input type="text" name="vendor[vendor_bank_acc]" placeholder="Bank A/C No." value="{{ @$vendor_edit->vendor_bank_acc }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Bank Country Code</label>
                            <input type="text" name="vendor[vendor_bank_country_code]" value="{{ @$vendor_edit->vendor_bank_country_code }}" placeholder="Bank Country Code" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Bank Swift Code</label>
                            <input type="text" name="vendor[vendor_bank_swift_code]" value="{{ @$vendor_edit->vendor_bank_swift_code }}" placeholder="Bank Swift Code" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Holder Name</label>
                            <input type="text" name="vendor[vendor_bank_holder_name]" value="{{ @$vendor_edit->vendor_bank_holder_name }}" placeholder="Holder Name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Holder Address</label>
                            <input type="text" name="vendor[vendor_bank_holder_addr]" value="{{ @$vendor_edit->vendor_bank_holder_addr }}" placeholder="Holder Address" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div>
                        <h5>
                            <label> <input type="checkbox" name="vendor_inter_bank" value="Y"> Intermediate Bank Details</label>
                        </h5>
                        <div class="form-group">
                            <label>Currency</label>
                            @php
                                $curr_arr = !empty( $vendor_edit->vendor_ib_currency ) ? explode(",", $vendor_edit->vendor_ib_currency) : [];
                            @endphp
                            <div class="row">
                                <div class="col">
                                    <label> <input type="checkbox" name="vendor[vendor_ib_currency][]" value="USD" @if(in_array("USD", $curr_arr)) checked @endif> USD </label>
                                </div>
                                <div class="col">
                                    <label> <input type="checkbox" name="vendor[vendor_ib_currency][]" value="EURO" @if(in_array("USD", $curr_arr)) checked @endif> EURO </label>
                                </div>
                                <div class="col">
                                    <label> <input type="checkbox" name="vendor[vendor_ib_currency][]" value="GBP" @if(in_array("USD", $curr_arr)) checked @endif> GBP </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Bank Name</label>
                            <input type="text" name="vendor[vendor_ib_name]" value="{{ @$vendor_edit->vendor_ib_name }}" placeholder="Bank Name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Bank Address</label>
                            <input type="text" name="vendor[vendor_ib_addr]" value="{{ @$vendor_edit->vendor_ib_addr }}" placeholder="Bank Address" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Bank Swift Code</label>
                            <input type="text" name="vendor[vendor_ib_swift_code]" value="{{ @$vendor_edit->vendor_ib_swift_code }}" placeholder="Bank Swift Code" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Remark</label>
                            <textarea style="height: 210px" name="vendor[vendor_ib_remark]" value="{{ @$vendor_edit->vendor_ib_remark }}" placeholder="Remark" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
