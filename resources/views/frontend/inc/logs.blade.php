<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Logs</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Logs</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid">
	
	<!-- <div class="card mb-5 mt-5">    
        
		<h3 class="card-title">Search Logs</h3>
		<div class="row">
			<div class="col-sm-10">

				<form>
					<div class="row">

						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-3">
									<label>
										Module Name
									</label>
								</div>
	   
								<div class="col-sm-9">
									 <div class="form-group">
										 <input type="text" name="country_name" value="{{ request('country_name') }}" class="form-control" >
									 </div>
								</div>
							</div>
						</div>
			  
						<div class="col-sm-4">
							<div class="form-group">
								<button type="submit" class="btn btn-primary form-control">SEARCH</button>
							</div>
						</div>
					</div>
				</form>
			</div>
				<div class="col-sm-2">
					<div class="form-group">
						<div class="col">
							
							<form action="{{ route('exportCountry') }}" method="post">
								{{ csrf_field() }}          
								<div class="" >
									<input type="hidden" name="country_name" value="{{request('country_name')}}">
									
									<input type="submit" class="btn btn-primary form-control" value="Export">
								</div>       
							</form>
						</div>
					</div>
				</div>
		     </div>
	</div> -->

	<form method="post">
		<div class="card">
			<h3 class="card-title">
				<div class="mr-auto">View Logs</div>
		        <!-- <a href="" class="ml-auto text-white" title="Remove Selected" data-toggle="tooltip">
		            <i class="icon-trash-o"></i>
		        </a> -->
			</h3>
	    	@csrf
		    @if(!$records->isEmpty())
		    {{ $records->links() }}
		    <div class="table-responsive">
			    <table class="table table-bordered">
			          <thead>
			               <tr>
			                    <!-- <th style="width: 50px;">
			                        <label class="animated-checkbox">
			                            <input type="checkbox" class="checkall">
			                            <span class="label-text"></span>
			                        </label>
			                    </th> -->
			                   <th style="width: 50px;">S.No.</th>
			                   <th>User</th>
			                   <th>Module</th>
			                   <th>Message</th>
			                   <th>Changed On</th>
			                   <!-- <th>Action</th> -->
			               </tr>
			          </thead>

			          <tbody>
			          		@php $sn = $records->firstItem(); @endphp
			          		@foreach($records as $rec)
			               	<tr>
		                        <!-- <td>
		                            <label class="animated-checkbox">
		                                <input type="checkbox" name="check[]" value="{{ $rec->country_id  }}" class="check">
		                                <span class="label-text"></span>
		                            </label>
		                        </td> -->
								<td>{{ $sn++ }}</td>
								<td>{{ !empty($rec->user) ? (!empty($rec->user->user_name) ? $rec->user->user_name : @$rec->user->user_login) : ''}}</td>
								<td>{{ $rec->module }}</td>
								<td>{{ $rec->message }}</td>
								<td>{{ !empty($rec->created_at) ? $rec->created_at : '' }}</td>
								<!-- <td class="icon-cent"></td> -->
			               	</tr>
			               @endforeach
			          </tbody>
			    </table>
			</div>
			{{ $records->links() }}
		    @else
		    <div class="no_records_found">
		      No records found yet.
		    </div>
			@endif
		</div>
	</form>
</div>