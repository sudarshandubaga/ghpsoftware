<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Commercial Invoice Cum Packing List</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="{{ url('') }}">Commercial Invoice Cum Packing List</a></li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <a href="{{ url('quote') }}" title="View Quote" class="btn btn-default" data-toggle="tooltip"> <i
                            class="icon-eye"></i> View Quote</a>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">

    @if (\Session::has('danger'))
        <div class="alert alert-danger">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <form data-session="pi_cart" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{ $BookingID }}" name="record[po_invoice_booking_id]">
                <input type="hidden" value="{{ $GetBookingData->po_invoice_po }}" name="record[po_invoice_po]">
                <button class="btn btn-primary">Save</button>

                <div class="clearfix">&nbsp;</div>

                <div class="card">
                    <h3 class="card-title">
                        <div class="mr-auto">PI Details</div>
                    </h3>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Invoice No.</label>
                                <input type="text" name="record[po_invoice_invoice_no]" class="form-control"
                                    required>
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label>Invoice Date</label>
                                <input type="date" name="record[po_invoice_invoice_date]" class="form-control"
                                    required value="{{ $GetBookingData->po_invoice_invoice_date }}">
                                <!--<input type="date" name="record[po_invoice_invoice_date]" class="form-control" readonly required>-->
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Statement on Origin</label>
                                <input type="text" name="record[po_invoice_statement_origin]" class="form-control"
                                    required value="{{ $GetBookingData->po_invoice_statement_origin }}">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Container Size</label>
                                <input type="text" name="record[po_invoice_container_size]" class="form-control"
                                    required value="{{ $GetBookingData->po_invoice_container_size }}">
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>FSC License Number</label>
                                <input type="text" name="record[po_invoice_fsc_license]" class="form-control"
                                    required value="{{ $GetBookingData->po_invoice_fsc_license }}">
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label>FSC Certificate Code</label>
                                <input type="text" name="record[po_invoice_fsc_certificate_code]"
                                    class="form-control" required
                                    value="{{ $GetBookingData->po_invoice_fsc_certificate_code }}">
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label>Container Number</label>
                                <input type="text" name="record[po_invoice_container]" class="form-control" required
                                    value="{{ $GetBookingData->po_invoice_container }}">
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label>Line Seal Number</label>
                                <input type="text" name="record[po_invoice_line_seel]" class="form-control" required
                                    value="{{ $GetBookingData->po_invoice_line_seel }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Custom Seal Number</label>
                                <input type="text" name="record[po_invoice_custom_seal]" class="form-control"
                                    required value="{{ $GetBookingData->po_invoice_custom_seal }}">
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label>Vessel / Voyage</label>
                                <input type="text" name="record[po_invoice_voyage]" class="form-control" required
                                    value="{{ $GetBookingData->po_invoice_voyage }}">
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label>ETD</label>
                                <input type="text" name="record[po_invoice_etd]" class="form-control datepicker"
                                    readonly required value="{{ $GetBookingData->po_invoice_etd }}">
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label>ETA</label>
                                <input type="text" name="record[po_invoice_eta]" class="form-control datepicker"
                                    readonly required value="{{ $GetBookingData->po_invoice_eta }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <h3 class="card-title">Product / Item Details</h3>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-header-fix"
                            style="background: #FFF;height: 500px; border: #000 solid 0px;">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Product Image</th>
                                    <th>Case No.</th>
                                    <th>Invoiced Qty</th>
                                    <th>Unit Price</th>
                                    <th>Total Price</th>
                                    <th>Pcs / ctn</th>
                                    <th>Total Pkt</th>
                                    <th>N. W. Wd</th>
                                    <th>N. W. Irn</th>
                                    <th>T.Nw Kg</th>
                                    <th>GW/Pkt Kgs</th>
                                    <th>Ttl Gw Kg</th>
                                    <th>Pkg Dim L</th>
                                    <th>Pkg Dim W</th>
                                    <th>Pkg Dim H</th>
                                    <th>Meas</th>
                                    <th>Ttl Meas</th>
                                    <th>HS Code</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $sn = $totQty = $totPrice = $totCBM = 0;
                                @endphp
                                @foreach ($products as $Pindex => $p)
                                    @php
                                        $dir = 'imgs/products/';
                                        $image = 'imgs/no-image.png';
                                        if (!empty($p->product_image)) {
                                            $image = $dir . $p->product_image;
                                        }
                                    @endphp
                                    <tr>
                                        <input type="hidden" value="{{ $p->exs_po_invoice_products_product_id }}"
                                            id="PrdInnerPkt{{ $Pindex }}">
                                        <input type="hidden" value="{{ $p->exs_po_invoice_products_product_id }}"
                                            name="InvoiceProducts[{{ $Pindex }}][id]">
                                        <input type="hidden" value="{{ $p->po_invoice_qid }}"
                                            name="InvoiceProducts[{{ $Pindex }}][po_invoice_qid]">
                                        <input type="hidden" value="{{ $p->invoice_po_no }}"
                                            name="InvoiceProducts[{{ $Pindex }}][invoice_po_no]">


                                        <td>{{ ++$sn }}</td>
                                        <td>
                                            <img src="{{ url($image) }}" alt="{{ $p->product_name }}"
                                                style="max-width: 70px;">
                                            {{ $p->product_name }}
                                        </td>
                                        <td><input type="text" name="InvoiceProducts[{{ $Pindex }}][caseno]"
                                                required value="{{ $p->pip_case_no }}"></td>
                                        <td><input type="text" name="InvoiceProducts[{{ $Pindex }}][qty]"
                                                value="{{ $p->pip_qty }}" required id="PrdQty{{ $Pindex }}"
                                                onBlur="UpdateAll('{{ $Pindex }}')"></td>
                                        <td><input type="text" value="{{ $p->pip_price }}"
                                                name="InvoiceProducts[{{ $Pindex }}][price]" readonly
                                                id="PrdPrice{{ $Pindex }}"
                                                onBlur="UpdateAll('{{ $Pindex }}')"></td>
                                        <td><input type="text" value="{{ $p->pip_subtotal }}"
                                                name="InvoiceProducts[{{ $Pindex }}][subtotal]" readonly
                                                id="PrdSubTotal{{ $Pindex }}"
                                                onBlur="UpdateAll('{{ $Pindex }}')"></td>
                                        <td><input type="text" name="InvoiceProducts[{{ $Pindex }}][Pcsctn]"
                                                value="{{ $p->pip_Pcsctn }}" required
                                                id="PrdPcsCtn{{ $Pindex }}"
                                                onBlur="UpdateAll('{{ $Pindex }}')"></td>
                                        <td><input type="text"
                                                name="InvoiceProducts[{{ $Pindex }}][TotalPackets]"
                                                value="{{ $p->pip_TotalPackets }}" required readonly
                                                id="PrdTotalPacket{{ $Pindex }}"
                                                onBlur="UpdateAll('{{ $Pindex }}')"></td>
                                        <td><input type="text" name="InvoiceProducts[{{ $Pindex }}][NWwWd]"
                                                value="{{ $p->pip_NWwWd }}" required
                                                id="PrdNwwWd{{ $Pindex }}"
                                                onBlur="UpdateAll('{{ $Pindex }}')"></td>
                                        <td><input type="text" name="InvoiceProducts[{{ $Pindex }}][NWIrn]"
                                                value="{{ $p->pip_NWIrn }}" required
                                                id="PrdNwIrn{{ $Pindex }}"
                                                onBlur="UpdateAll('{{ $Pindex }}')"></td>
                                        <td><input type="text"
                                                name="InvoiceProducts[{{ $Pindex }}][TotalNetWeightKG]"
                                                value="{{ $p->pip_TotalNetWeightKG }}" required readonly
                                                id="PrdTotalNWKg{{ $Pindex }}"
                                                onBlur="UpdateAll('{{ $Pindex }}')"></td>
                                        <td><input type="text"
                                                name="InvoiceProducts[{{ $Pindex }}][GWPktKgs]"
                                                value="{{ $p->pip_GWPktKgs }}" required
                                                id="PrdGwPktKg{{ $Pindex }}"
                                                onBlur="UpdateAll('{{ $Pindex }}')"></td>
                                        <td><input type="text"
                                                name="InvoiceProducts[{{ $Pindex }}][TotalGrossWeight]"
                                                value="{{ $p->pip_TotalGrossWeight }}" required readonly
                                                id="PrdTotalGwWg{{ $Pindex }}"
                                                onBlur="UpdateAll('{{ $Pindex }}')"></td>

                                        <td><input type="text"
                                                name="InvoiceProducts[{{ $Pindex }}][PkgDimL]"
                                                value="{{ $p->pip_PkgDimL }}" required
                                                id="PrdPkgDimL{{ $Pindex }}"
                                                onBlur="UpdateAll('{{ $Pindex }}')"></td>
                                        <td><input type="text"
                                                name="InvoiceProducts[{{ $Pindex }}][PkgDimW]"
                                                value="{{ $p->pip_PkgDimW }}" required
                                                id="PrdPkgDimW{{ $Pindex }}"
                                                onBlur="UpdateAll('{{ $Pindex }}')"></td>
                                        <td><input type="text"
                                                name="InvoiceProducts[{{ $Pindex }}][PkgDimH]"
                                                value="{{ $p->pip_PkgDimH }}" required
                                                id="PrdPkgDimH{{ $Pindex }}"
                                                onBlur="UpdateAll('{{ $Pindex }}')"></td>
                                        <td><input type="text" name="InvoiceProducts[{{ $Pindex }}][Meas]"
                                                value="{{ $p->pip_Meas }}" required readonly
                                                id="PrdMeas{{ $Pindex }}"
                                                onBlur="UpdateAll('{{ $Pindex }}')"></td>

                                        <td><input type="text"
                                                name="InvoiceProducts[{{ $Pindex }}][TotalMeas]"
                                                value="{{ $p->pip_TotalMeas }}" required readonly
                                                id="PrdTotalCBM{{ $Pindex }}"
                                                onBlur="UpdateAll('{{ $Pindex }}')"></td>
                                        <td><input type="text" name="InvoiceProducts[{{ $Pindex }}][HSCode]"
                                                value="{{ $p->pip_HSCode }}"></td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <!-- <tr>
                        <th colspan="6">TOTAL</th>
                        <th>{{ $totQty }}</th>
                        <th>{{ number_format($totCBM, 6) }}</th>
                        <th>{{ $totPrice }}</th>
                        <th colspan="2"></th>
                    </tr> -->
                            </tfoot>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function UpdateAll(id) {
        Qty = parseInt($("#PrdQty" + id).val());
        Price = parseFloat($("#PrdPrice" + id).val());

        MasterCarton = parseInt($("#PrdPcsCtn" + id).val());
        InnerCarton = parseInt($("#PrdInnerPkt" + id).val());

        NWWood = parseFloat($("#PrdNwwWd" + id).val());
        NWIrn = parseFloat($("#PrdNwIrn" + id).val());

        GWPktKG = parseFloat($("#PrdGwPktKg" + id).val());

        PkgDimL = parseFloat($("#PrdPkgDimL" + id).val());
        PkgDimW = parseFloat($("#PrdPkgDimW" + id).val());
        PkgDimH = parseFloat($("#PrdPkgDimH" + id).val());

        CalculateSubPrice = Qty * Price;
        $("#PrdSubTotal" + id).val(CalculateSubPrice);

        CalculateTotalPacket = Qty * MasterCarton;
        if (InnerCarton > 1) {
            CalculateTotalPacket = InnerCarton / Qty;
        }

        $("#PrdTotalPacket" + id).val(CalculateTotalPacket);

        TotalNWKg = (NWWood + NWIrn) * Qty;
        $("#PrdTotalNWKg" + id).val(TotalNWKg);

        TotalGwKG = GWPktKG * CalculateTotalPacket;
        $("#PrdTotalGwWg" + id).val(TotalGwKG);

        Meas = ((PkgDimL * PkgDimW * PkgDimH) / 1000000).toFixed(5);
        $("#PrdMeas" + id).val(Meas);

        TotalMeas = (Meas * CalculateTotalPacket).toFixed(5);
        $("#PrdTotalCBM" + id).val(TotalMeas)
    }
</script>
