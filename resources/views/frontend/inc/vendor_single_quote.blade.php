<section class="page-header mb-3">
    <div class="container-fluid">
        <div>
            <div>
                <h1>Quote ID #{{ sprintf('%06d', $record->vquote_number) }}</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="{{ url('/') }}">Dashboard</a></li>
                    <li><a href="{{ url('quote') }}">Quotes</a></li>
                    <li class="active">Quote ID #{{ sprintf('%06d', $record->vquote_number) }}</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <div class="card">
        <h3 class="card-title">Quotation Details for Quote ID #{{ sprintf('%06d', $record->vquote_number) }}</h3>
        @php
            $added_by = '';
        @endphp
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>S.No.</th>
                        <th>Product Image</th>
                        <th width="400">Product Description</th>
                        <th class="text-center">Dimension [LxWxH]<br><small>(in cms)</small></th>
                        <th>CBM Per Pc.</th>
                        @foreach ($quotes as $q)
                            <th class="text-center">
                                Revision {{ $q->vquote_version }} <br>
                                <small>({{ ucwords($q->vquote_added_by) }})</small>
                                @php
                                    $added_by = $q->vquote_added_by;
                                @endphp
                            </th>
                            <!-- <th>Qty. {{ $q->quote_version }}</th> -->
                            <!-- <th>Total CBM {{ $q->quote_version }}</th>
                        <th>Subtotal</th>
                        <th>Special Remark</th> -->
                        @endforeach
                        <th>Approval</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $sn = $totQty = $totPrice = $totCBM = 0;
                    @endphp
                    @foreach ($products as $p)
                        @php
                            $dir = 'imgs/products/';
                            $image = 'imgs/no-image.png';
                            if (!empty($p->product_image)) {
                                $image = $dir . $p->product_image;
                            }
                            $subtotal = $p->vqpro_price * $p->vqpro_qty;
                            $cbm_total = $p->product_cbm * $p->vqpro_qty;
                            $totQty += $p->vqpro_qty;
                            $totPrice += $subtotal;
                            $totCBM += $cbm_total;
                        @endphp
                        <tr>
                            <td>{{ ++$sn }}</td>
                            <td><img src="{{ url($image) }}" alt="{{ $p->product_name }}" style="max-width: 70px;">
                            </td>
                            <td>{{ $p->product_name }}</td>
                            <td class="nowrap text-center">
                                {{ "{$p->product_l_cm} x {$p->product_w_cm} x {$p->product_h_cm}" }} cms</td>
                            <td>{{ $p->product_cbm }}</td>
                            @if (!empty($p->qpros))
                                @foreach ($p->qpros as $qp)
                                    @php
                                        $cbm_total = $p->product_cbm * $qp->vqpro_qty;
                                        $subtotal = $qp->vqpro_price * $qp->vqpro_qty;
                                    @endphp
                                    <td @if ($qp->vqpro_is_approved == 'Y') class="bg-success text-white" @endif>
                                        <div class="nowrap">
                                            <strong>Price:</strong> {{ $qp->vqpro_price }}
                                        </div>
                                        <div class="nowrap">
                                            <strong>Qty:</strong> {{ $qp->vqpro_qty }}
                                        </div>
                                        <div class="nowrap">
                                            <strong>CBM:</strong> {{ number_format($cbm_total, 6) }}
                                        </div>
                                        <div>
                                            <strong>Remarks:</strong><br>{{ $qp->vqpro_remark }}
                                            {{ $qp->vqpro_is_approved }}
                                        </div>
                                    </td>
                                    <!-- <td>{{ number_format($cbm_total, 6) }}</td>
                            <td>{{ $subtotal }}</td>
                            <td>{{ $qp->vqpro_remark }}</td> -->
                                @endforeach
                            @endif
                            <th>
                                <label class="switch">
                                    <input type="checkbox" rel="approve_qpro"
                                        data-url="{{ url('ajax/approve_vqpro/' . $p->vqpro_qid . '/' . $p->vqpro_pid) }}"
                                        @if ($added_by == $profile->user_role) disabled @endif
                                        @if ($p->vqpro_is_approved == 'Y') checked disabled @endif>
                                    <span class="slider"></span>
                                </label>
                            </th>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <!-- <tr>
                        <th colspan="6">TOTAL</th>
                        <th>{{ $totQty }}</th>
                        <th>{{ number_format($totCBM, 6) }}</th>
                        <th>{{ $totPrice }}</th>
                        <th colspan="2"></th>
                    </tr> -->
                </tfoot>
            </table>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <p>&nbsp;</p>
                <p>We consider following volume parameter to accept any order:</p>
                <!-- <div> 1 x 40 HC = 65 - 67 cbms </div>
                <div> 1 x 40 Std = 50 - 55 cbms </div>
                <div> 1 x 20 ft = 28 - 30 cbms </div> -->
                <table class="table table-bordered">
                    <tr>
                        <th width="50">Case I.</th>
                        <td>Upto 68 cbms</td>
                    </tr>
                    <tr>
                        <th>Case II. LCL Ship</th>
                        <td>Less than 20 cbms</td>
                    </tr>
                    <tr>
                        <th>Case III. 20 ft. std.</th>
                        <td>20 - 28 cbms</td>
                    </tr>
                    <tr>
                        <th>Case IV. 40 ft. std.</th>
                        <td>28 - 58 cbms</td>
                    </tr>
                    <tr>
                        <th>Case V. 40 HC. std.</th>
                        <td>58 - 68 cbms</td>
                    </tr>
                </table>
                <p>&nbsp;</p>
            </div>
            <div class="col-sm-2">

            </div>
            <div class="col-sm-6">
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <table class="table table-bordered">
                    <tr>
                        <th colspan="2">Desirable Terms</th>
                    </tr>
                    <tr>
                        <th>Price Term</th>
                        <td>{{ $record->vquote_price_term }}</td>
                    </tr>
                    <tr>
                        <th>Port Of Delivery</th>
                        <td>{{ $record->vquote_delivery_port }}</td>
                    </tr>
                    <tr>
                        <th>Currency</th>
                        <td>{{ $record->currency_short_name }} ({{ $record->currency_sign }})</td>
                    </tr>
                    <tr>
                        <th>Delivery</th>
                        <td>{{ $record->vquote_delivery_days }}</td>
                    </tr>
                    <tr>
                        <th>Payment</th>
                        <td>{{ $record->vquote_payment }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
