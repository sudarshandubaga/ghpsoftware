<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Currency</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="{{ url('/') }}">Dashboard</a></li>
                    <li class="active">Currency</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <div class="card">
        
        @if(Session::has('Success'))
        <div class="alert alert-success mb-5" role="alert">{!!Session::get('Success')!!}</div>
        @endif
        
        @if(Session::has('Danger'))
        <div class="alert alert-danger mb-5" role="alert">{!!Session::get('Danger')!!}</div>
        @endif
        
        
        <h3 class="card-title">
            Add Currency
        </h3>
        <form method="post">
            @csrf
            <div class="row">

                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>
                                 Currency Name
                            </label>
                        </div>

                        <div class="col-sm-9">
                            <div class="form-group">
                                <input type="text" name="record[currency_name]" value="{{ @$edit->currency_name }}" class="form-control" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2"> </div>

                <div class="col-sm-2"> </div>

                <div class="col-sm-2"> </div>

                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>
                                Currency Short Name
                            </label>
                        </div>

                        <div class="col-sm-9">
                            <div class="form-group">
                                <input type="text" name="record[currency_short_name]" value="{{ @$edit->currency_short_name }}" class="form-control" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2"> </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-primary">SAVE</button>
                    </div>
                </div>

                <div class="col-sm-2"> </div>

                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>
                                Currency Symbol
                            </label>
                        </div>

                        <div class="col-sm-9">
                            <div class="form-group">
                                <input type="text" name="record[currency_sign]" value="{{ @$edit->currency_sign }}" class="form-control" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6"> </div>
            </div>
        </form>
    </div>
    <div class="card mb-5 mt-5">    
        
		<h3 class="card-title">Search Currency</h3>
		<div class="row">
			<div class="col-9">

				<form>
					<div class="row">
						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-3">
									<label>
										Name
									</label>
								</div>
	   
								<div class="col-sm-9">
									 <div class="form-group">
										 <input type="text" name="currency_name" value="{{ request('currency_name') }}" class="form-control" >
									 </div>
								</div>
							</div>
						</div>
	   
						<div class="col-sm-4">
							<div class="form-group">
								<button type="submit" class="btn btn-primary form-control">SEARCH</button>
							</div>
							<!-- <div class="form-group">
								 <a href="{{ url('finish') }}" class="btn btn-default text-center form-control">ADD</a>
							</div> -->
						</div>
					</div>
				</form>
			</div>
			<div class="col-3">
			<div class="form-group">
						<div class="col">
							
							<form action="{{ route('exportCurrency') }}" method="post">
								{{ csrf_field() }}          
								<div class="" >
									<input type="hidden" name="currency_name" value="{{request('currency_name')}}">
									
									<input type="submit" class="btn btn-primary form-control" value="Export">
								</div>       
							</form>
						</div>
					</div>
			</div>
		</div>
	</div>
    <div class="card mt-5">
        <h3 class="card-title">
            <div class="mr-auto">View Currency</div>
            <a href="" class="ml-auto text-white">
                <i class="icon-trash-o"></i>
            </a>
        </h3>
        <form method="post">
            @csrf
            @if(!$records->isEmpty())
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 50px;">
                                <label class="animated-checkbox">
                                    <input type="checkbox" class="checkall">
                                    <span class="label-text"></span>
                                </label>
                            </th>
                            <th>S.no</th>
                            <th>Currency Name</th>
                            <th>Currency Short Name</th>
                            <th>Currency Symbol</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @php $sn = $records->firstItem(); @endphp
                        @foreach($records as $rec)
                        <tr>
                            <td>
                                <label class="animated-checkbox">
                                    <input type="checkbox" name="check[]" value="{{ $rec->currency_id  }}" class="check">
                                    <span class="label-text"></span>
                                </label>
                            </td>
                            <td>{{ $sn++ }}</td>
                            <td>{{ $rec->currency_name }}</td>
                            <td>{{ $rec->currency_short_name }}</td>
                            <td>{{ $rec->currency_sign }}</td>
                            <td class="icon-cent">
                                <a href="{{ url('currency/'.$rec->currency_id) }}" class="pencil"><i class="icon-pencil" title="Edit"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $records->links() }}
            @else
            <div class="no_records_found">
              No records found yet.
            </div>
            @endif
        </form>
    </div>
</div>
