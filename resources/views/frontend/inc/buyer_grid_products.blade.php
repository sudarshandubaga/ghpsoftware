@if(COUNT($products) > 0)

<div class="table-responsive table-header-fix" style="height: 600px;">
    <table class="table table-bordered" id="table_Data">
        <thead>
            <tr>
              <th>
                Sr. No.
              </th>
              <th>
                Buyer Name
              </th>
              <th>
                Order Reference
              </th>
              <th>
                GHP PI#
              </th>
              <th>
                GHP PO#
              </th>
              <th>
                Product Code
              </th>
              <th>
                Porduct Details
              </th>
              <th>
                Buyer SKU / Barcode
              </th>
              <th>
                Required Ex Factory (PI# ETD)
              </th>
              <th>
                Order Type (Consigee Cout Name)
              </th>
              <th>
                Delay Day
              </th>
              <th>
                Ramarks
              </th>
              <th>
                Product CBM
              </th>
              <?php for($i = 1; $i<=52; $i++){ ?>
                <th> Week <?php echo $i?></th>
                <th> Total CBM</th>
            <?php } ?>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $key => $each)
            <tr>
                <td>{{ $loop->iteration }}</td>
              <td>
                {{ $each->user_name }}
              </td>
              <td>
                 {{ $each->pi_order_ref }}
              </td>
              <td>
                {{ sprintf("%s-%03d", 'GHP-202122', $each->pi_id) }}
              </td>
              <td>
                2021{{(int) $each->po_id+100 }}
              </td>
              <td>
                {{ $each->product_code }}
                </td>
                <td>
                    {{ $each->product_name }}
                </td>
                <td>
                    @if(!empty($each->pipro_sku) && !empty($each->pipro_barcode))
                        {{ $each->pipro_sku  }} <strong>/</strong> {{ $each->pipro_barcode }}
                    @elseif(!empty($each->pipro_sku))
                        {{ $each->pipro_sku }}
                    @elseif(!empty($each->pipro_barcode))
                        {{ $each->pipro_barcode }}
                    @endif
                </td>
              <td>
                {{ date('d-M-Y', strtotime($each->pi_date)) }}
              </td>
              <td>
                {{ !empty($each->country_short_name) ? $each->country_short_name : "N/A" }}
              </td>
              <td>
                @php
                    // $to = new DateTime('today');
                    $to = strtotime("now");
                    $from = strtotime($each->pi_date);

                    $interval = $to - $from;
                    $interval = ceil($interval/(60 * 60 * 24));
                    // dd($interval);
                @endphp
                {{ !empty($interval) ? $interval : '0' }}
              </td>
              <td>
                  <input type="text" onblur="myFunction(this)" data-id="{{ $each->pipro_id }}" clsss="update_remark" value="{{ $each->grid_remark }}">
              </td>
              <td>
                {{  $each->product_cbm }}
              </td>
                <?php for($i = 1; $i<=52; $i++){
                    $weekNo = date("W", strtotime($each->pi_date));
                    if($i == $weekNo){ ?>
                        <td><?php echo $each->pipro_qty ?></td>
                        <td><?php echo round($each->product_cbm * $each->pipro_qty, 4) ?></td>


                    <?php } else { ?>
                        <td> - </td>
                        <td> - </td>
                    <?php } ?>

                <?php } ?>


            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<script>
    function myFunction(foo){
        console.log(foo);
        var text = foo.value;
        var id = foo.getAttribute('data-id');
        $.ajax({
            url: '{{ route("updateGrid") }}',
            method:"post",
            data: {id: id, message: text, "_token": "{{ csrf_token() }}" },
            success: function(response){

            }
        })
    }

    $('#table_Data').DataTable({
        "pageLength": 1000,
        "dom": 'Bfrtip',
        "buttons": [
            'excel'
        ]
    });

</script>
@else
    <h4 class="text-center" style="display:none;">No Data Found!</h4>
@endif
