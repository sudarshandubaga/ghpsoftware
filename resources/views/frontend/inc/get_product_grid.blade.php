
@if(COUNT($products) > 0)
    <div class="table-responsive table-header-fix" style="height: 600px;">
        <table class="table table-bordered" id="Data_table">
            <thead>
                <tr>
                    <th>
                        Sr. No.
                    </th>
                    <th>
                        Vendor Name
                    </th>
                    <th>
                        GHP PO#
                    </th>
                    <th>
                        GHP PI#
                    </th>
                    <th>
                        Product Code
                    </th>
                    <th>
                        Porduct Details
                    </th>
                    <th>
                        Buyer SKU / Barcode
                    </th>
                    <th>
                        Required Ex Factory<br>(PO# ETD)
                    </th>
                    <th>
                        Consigee Country
                    </th>
                    <th>
                        Delay Day
                    </th>
                    <th>
                        Ramarks
                    </th>
                    <th>
                        CBM
                    </th>

                    <?php for($i = 1; $i<=52; $i++){ ?>
                        <th> Week <?php echo $i?></th>
                        <th> Total CBM</th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
            @foreach ($products as $key => $each )
                <tr>
                    <td>
                        {{ $key+1 }}
                    </td>
                    <td>
                        {{  $each->vendor_org_name }}
                    </td>
                    <td>
                            2021{{(int) $each->po_id+100 }}

                    </td>
                    <td>
                        {{ sprintf("%s-%03d", 'GHP-202122', $each->pi_id) }}
                    </td>
                    <td>
                        {{ $each->product_code }}
                    </td>
                    <td>
                        {{ $each->product_name }}
                    </td>
                    <td>
                        @if(!empty($each->popro_sku) && !empty($each->popro_barcode))
                            {{ $each->popro_sku  }} <strong>/</strong> {{ $each->popro_barcode }}
                        @elseif(!empty($each->popro_sku))
                            {{ $each->popro_sku }}
                        @elseif(!empty($each->popro_barcode))
                            {{ $each->popro_barcode }}
                        @endif
                    </td>
                    <td>
                        {{ date('d-M-Y', strtotime($each->po_delivery_days)) }}
                    </td>
                    <td>
                        {{ !empty($each->country_short_name) ? $each->country_short_name : "N/A" }}
                    </td>
                    <td>
                        @php
                            // $to = new DateTime('today');
                            $to = strtotime("now");
                            $from = strtotime($each->po_delivery_days);

                            $interval = $to - $from;
                            $interval = ceil($interval/(60 * 60 * 24));
                            // dd($interval);
                        @endphp
                        {{ !empty($interval) ? $interval : '0' }}
                    </td>
                    <td>
                        <input type="text" data-id="{{ $each->popro_id }}" name="" onblur="myFunction(this)" value="{{ $each->grid_remark }}">
                        {{ $each->grid_remark }}
                    </td>
                    <td>
                        {{ $each->product_cbm }}
                    </td>
                    <?php for($i = 1; $i<=52; $i++){
                        $weekNo = date("W", strtotime($each->po_delivery_date));
                        if($i == $weekNo){ ?>
                            <td><?php echo $each->popro_qty ?></td>
                            <td><?php echo round($each->product_cbm * $each->popro_qty, 4) ?></td>


                        <?php } else { ?>
                            <td> - </td>
                            <td> - </td>
                        <?php } ?>

                    <?php } ?>

                </tr>
            @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="10">
                        Total
                    </td>

                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
    <script>
        function myFunction(foo){
            console.log(foo);
            var text = foo.value;
            var id = foo.getAttribute('data-id');
            $.ajax({
                url: '{{ route("updateVenderGrid") }}',
                method:"post",
                data: {id: id, message: text, "_token": "{{ csrf_token() }}" },
                success: function(response){

                }
            })
        }

        $('#Data_table').DataTable({
            "pageLength": "20",
            "dom": 'Bfrtip',
            "buttons": [
                'excel', 'print'
            ]
        });
    </script>
@else
    <h4 class="text-center" >No Data Found!</h4>
@endif
