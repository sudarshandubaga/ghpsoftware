<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Order Tracking</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Order Tracking</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!--<form>-->
<!--<div class="card mb-5 mt-5">-->
<!--    <h3 class="card-title">Filter Product</h3>-->
<!--    <div class="row">-->
<!--        <div class="col">-->
<!--            <div class="form-group">-->
<!--                <label>Status</label>-->
<!--                <select name="TrackStatus" class="form-control">-->
<!--                    <option value="">All</option>-->
<!--                    <option @if(@$_GET['TrackStatus'] == "Open") selected @endif>Open</option>-->
<!--                    <option @if(@$_GET['TrackStatus'] == "Shipped on Water") selected @endif>On Water</option>-->
<!--                    <option @if(@$_GET['TrackStatus'] == "Shipped on Board") selected @endif>Ship on Board</option>-->
<!--                </select>-->
<!--            </div>-->
<!--        </div>-->
        
<!--        <div class="col">-->
<!--            <div class="form-group">-->
<!--                <label>&nbsp;</label>-->
<!--                <button type="submit" class="btn btn-primary form-control">Filter</button>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--</form>-->

<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter Product</h3>    
    <div class="row">
        <div class="col-10">
        <form>
            <div class="row">
                <!-- <div class="col">
                    <div class="form-group">
                        <label>Customer Name</label>
                        <input type="text" name="SearchByName" value="{{ @$_GET['SearchByName'] }}" class="form-control">
                    </div>
                </div> -->
                <div class="col">
                <div class="form-group">
                    <label>Status</label>
                    <select name="TrackStatus" class="form-control">
                        <option value="">All</option>
                        <option @if(@$_GET['TrackStatus'] == "Open") selected @endif>Open</option>
                        <option @if(@$_GET['TrackStatus'] == "Shipped on Water") selected @endif>On Water</option>
                        <option @if(@$_GET['TrackStatus'] == "Shipped on Board") selected @endif>Ship on Board</option>
                    </select>
                </div>
                </div>



                <div class="col">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-primary form-control">Filter</button>
                    </div>
                </div>
                
                
            </div>
            
        </form>
        </div>
        <div class="col-2">
        <form action="{{ route('exportOrderTracking') }}" method="post">
            {{ csrf_field() }}          
            <div class="" >
                <input type="hidden" name="TrackStatus" value="{{request('TrackStatus')}}">
                <label>&nbsp;</label>
                <input type="submit" class="btn btn-primary form-control" value="Export">
            </div>       
        </form>
        </div>
    </div>
</div>

<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">List Order Tracking</div>
                    </h3>
                    <div class="basic-info-two">

                        @if(!$records->isEmpty())
                        <div class="table-responsive" style="height: 500px; border: #000 solid 0px;">
                            <table class="table table-bordered table-hover table-header-fix">
                                <thead>
                                <tr>
                                    <th style="width: 80px;">Sr. No.</th>
                                    <th>Status</th>
                                    <!--<th style="min-width: 130px;">Buyer Name</th>-->
                                    <th style="min-width: 130px;">PI Number</th>
                                    <th style="min-width: 100px;">PI Date</th>
                                    <th style="min-width: 100px;">PI Confirm Date</th>
                                    <th style="min-width: 100px;">Agreed ETD</th>
                                    <th>CBM</th>
                                    <th>Container No.</th>
                                    <th>Stuffing Date</th>
                                    @if($profile->user_role == "admin")
                                    <th>DO Exp. Date</th>
                                    <th style="min-width: 110px;">POD</th>
                                    <th>Planned ETD</th>
                                    @endif
                                    
                                    
                                    
                                    <th>Planned ETA</th>
                                    <th>Vessel Sailing Date</th>
                                    <th>Vessel ETA</th>
                                    <!--<th>Revised ETD</th>-->
                                    
                                    @if($profile->user_role == "admin")
                                    <th style="min-width: 100px;">PI Value</th>
                                    @endif
                                    
                                    <th>Updated Remark</th>
                                    <th>Noted Remark</th>
                                    <th>Detail</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $sn = $records->firstItem(); @endphp
                                 
                                @foreach($records as $rec)
                                    @php
                                    $GrandTotal = 0;
                                    $GrandCBM = 0;
                                    $products = \App\Models\PIProduct::join('products AS p', 'p.product_id', 'pipro_pid')->join('quote_products AS qpro', 'pipro_pid', 'qpro_pid')->where('pipro_pi_id', $rec->pi_id)->where('qpro_qid', $rec->quote_id)->get();
                                    
                                    foreach($products as $p){
                                        $SubTotal = $p->qpro_price * $p->qpro_qty;
                                        $SubCBM = $p->product_cbm * $p->qpro_qty;
                                        $GrandTotal += $SubTotal;
                                        $GrandCBM += $SubCBM;
                                    }
                                    @endphp
                                    @php
                                        $PINumber = sprintf("%s-%03d", 'GHP-201819', $rec->pi_id);
                                        $Logistic = DB::table('logistics')->where("exs_logistics_pi", "like", "%".$PINumber."%")->count();
                                        if($Logistic > 0){
                                            $Logistic = DB::table('logistics')->where("exs_logistics_pi", "like", "%".$PINumber."%")->first();
                                        }else{
                                            $Logistic = "";
                                        }
                                       
                                    @endphp
                                    <td style="width: 80px;">{{ $sn++ }}</td>
                                    <td>
                                        @if(isset($Logistic->StuffingDate))
                                            @if(date("Y-m-d") < $Logistic->StuffingDate)
                                                Open
                                            @elseif($Logistic->StuffingDate < date("Y-m-d") && $Logistic->PlannedETDDAte > date("Y-m-d"))
                                                Shipped on Water
                                            @elseif($Logistic->PlannedETADate < date("Y-m-d"))
                                                Shipped on Board
                                            @endif
                                        @endif
                                    </td>
                                    <!--<td>{{ $rec->user_name }}</td>-->
                                    <td>{{ sprintf("%s-%03d", 'GHP-201819', $rec->pi_id) }}</td>
                                    <td>{{ date("d-M-Y", strtotime($rec->pi_date)) }}</td>
                                    <td>
                                        @if($rec->pi_confirm_date != "0000-00-00")
                                        {{ date("d-M-Y", strtotime($rec->pi_confirm_date)) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$Logistic->AgreedETD != "")
                                        {{ date("d-M-Y", strtotime(@$Logistic->AgreedETD)) }}
                                        @endif
                                    </td>
                                    <td><b>{{ round($GrandCBM, 3) }}</b></td>
                                    @if($Logistic =="")
                                        <td>-</td>
                                    @else
                                        <td>{{$Logistic->ContainerNr}}</td>
                                    @endif()
                                    
                                    @if($Logistic =="")
                                        <td>-</td>
                                    @else
                                        <td>{{date("d-M-Y", strtotime($Logistic->PlannedStuffingDate)) }}</td>
                                    @endif()
                                    
                                    
                                    @if($profile->user_role == "admin")
                                    @if($Logistic =="")
                                        <td>-</td>
                                    @else
                                        <td>{{date("d-M-Y", strtotime($Logistic->DOExpiryDate))}}</td>
                                    @endif()
                                    
                                    <td>{{ $rec->quote_loading_port }}</td>
                                    
                                    
                                    @if($Logistic =="")
                                        <td>-</td>
                                    @else
                                        <td>{{date("d-M-Y", strtotime($Logistic->PlannedETDDAte))}}</td>
                                    @endif()
                                    
                                    @endif
                                    
                                    @if($Logistic =="")
                                        <td>-</td>
                                    @else
                                        <td>{{date("d-M-Y", strtotime($Logistic->PlannedETADate))}}</td>
                                    @endif()
                                    
                                    @if($Logistic =="")
                                        <td>-</td>
                                    @else
                                        <td>{{date("d-M-Y", strtotime($Logistic->VesselSailingETD))}}</td>
                                        
                                    @endif()
                                    
                                    @if($Logistic =="")
                                        <td>-</td>
                                    @else
                                        <td>{{date("d-M-Y", strtotime($Logistic->VesselSailingETA))}}</td> 
                                    @endif()    
                                    <!--<td></td>-->
                                    
                                    @if($profile->user_role == "admin")
                                    <td align="right">$ {{ number_format($GrandTotal, 2) }}</td>
                                    @endif
                                    
                                    <td></td>
                                    <td></td>
                                    <td><a href="{{ URL('order-track-detail') }}/{{ $rec->pi_id }}" target="_blank">View</a></td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                         </div>
                        {{ $records->links() }}
                        @else
                        <div class="no_records_found">
                          No records found yet.
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
