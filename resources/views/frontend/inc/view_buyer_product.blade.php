<style rel="css">
    #searchVendor {
        background-image: url(https://www.w3schools.com/css/searchicon.png);
        background-position: 10px 10px;
        background-repeat: no-repeat;
        height: 46px;
        padding-left: 40px;
    }
</style>
<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Buyer Products</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">View Buyer Products</li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <!-- <a href="{{ url('user/add/') }}" title="Add New" class="btn btn-default" data-toggle="tooltip"> <i class="icon-plus-circle"></i> Add New</a> -->
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <form method="post" id="vendor_list" onkeydown="return event.key != 'Enter';">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <span>Buyer Products</span>
                        <div class="ml-auto"></div>
                    </h3>
                    <div class="basic-info-two">
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                            {!! \Session::get('success') !!}
                        </div>
                        @endif
                        <div class="row">
                              <div class="col-sm-4">
                                <div class="form-group ui-widget" >
                                    <select name="" id="" class="form-control get_products select2">
                                        <option value="" >Select</option>
                                        @if(!empty($vendors))
                                          @foreach($vendors as $vendor)
                                            <option value="{{ $vendor->user_id }}">{{ $vendor->user_name }}</option>
                                          @endforeach
                                        @endif
                                    </select>
                                </div>
                              </div>
                        </div>
                        <div class="table-responsive table-header-fix" style="height: 600px;">
                            <table class="table table-bordered" id="table_Data" style="display:none;">
                                <thead>
                                    <tr>
                                        <!-- <th style="width: 50px;">
                                            <label class="animated-checkbox">
                                                <input type="checkbox" class="quote_checkall">
                                                <span class="label-text">All</span>
                                            </label>
                                        </th> -->
                                        <th class="text-center" style="min-width: 300px;">Products Details</th>
                                        <th class="text-center" colspan="" style="text-align: center;text-transform: uppercase;">Product Drawing</th>
                                        <th class="text-center" colspan="" style="text-align: center;text-transform: uppercase;">Product Data File</th>
                                        <th class="text-center" colspan="" style="text-align: center;text-transform: uppercase;">Product Assembly</th>
                                        <th class="text-center" colspan="" style="text-align: center;text-transform: uppercase;">Product Shipping Marks</th>
                                        <th class="text-center" colspan="" style="text-align: center;text-transform: uppercase;">Product Barcode/Tag</th>
                                        <th class="text-center" colspan="" style="text-align: center;text-transform: uppercase;">Product Care Instruction</th>
                                        <th class="text-center" colspan="" style="text-align: center;text-transform: uppercase;">Product Logo</th>
                                        <th class="text-center" colspan="" style="text-align: center;text-transform: uppercase;">Product Label</th>
                                        <th class="text-center" colspan="" style="text-align: center;text-transform: uppercase;">Product Cartoon Labelling</th>
                                        <th class="text-center" colspan="" style="text-align: center;text-transform: uppercase;">Other Warning Label Safety Label</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                            <h4 class="text-center" style="display:none;">No Data Found!</h4>
                         </div> 
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
       <h5>No File Uploaded!</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
  
  $(document).on('change','.get_products',function(){
        var id = $(this).val();
        if(id != ''){
                $.ajax({
                    url: "{{ route('get_buyer_products') }}",
                    method: 'post',
                    data: { id:id,"_token": "{{ csrf_token() }}" },
                    success: function(result){
                        if(result){
                            if(result.length>0){
                              $('#table_Data').css({'display':'block'});
                              $('.table-responsive h4').css({ 'display':'none' });
                            }else{
                              $('#table_Data').css({'display':'none'});
                              $('.table-responsive h4').css({ 'display':'block' });
                            }
                            var html='';
                            $("#table_Data tbody tr").remove();
                            for(var key in result) {
                            var value = result[key];
                                html+=`<tr>
                                        <td>
                                           <div class="text-center">
                                            <img src="${value.product_image}" style="max-width: 80px;">
                                            <strong class="d-block mt-2">${value.product_name}</strong>
                                            <strong>${value.product_code}</strong>
                                            <input type="hidden" value="${value.product_id}" class="product_id">
                                           </div>
                                        </td>
                                        <td>
                                          <div class="text-center">${(value.bpro_drawing==null) ? '<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><img src="{{ asset("public/admin/images/uncheck.jpg") }}" width="50px"></a>' : '<a href='+value.bpro_drawing+' target="_blank"><img src="{{ asset("public/admin/images/check.jpg") }}" width="50px"></a>'}</div>
                                        </td>
                                        <td>
                                          <div class="text-center">${(value.bpro_data_file==null) ? '<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><img src="{{ asset("public/admin/images/uncheck.jpg") }}" width="50px"></a>' : '<a href='+value.bpro_data_file+' target="_blank"><img src="{{ asset("public/admin/images/check.jpg") }}" width="50px"></a>'}</div>
                                        </td>
                                        <td>
                                          <div class="text-center">${(value.bpro_assembly==null) ? '<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><img src="{{ asset("public/admin/images/uncheck.jpg") }}" width="50px"></a>' : '<a href='+value.bpro_assembly+' target="_blank"><img src="{{ asset("public/admin/images/check.jpg") }}" width="50px"></a>'}</div>
                                        </td>
                                        <td>
                                          <div class="text-center">${(value.bpro_shipping_marks==null) ? '<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><img src="{{ asset("public/admin/images/uncheck.jpg") }}" width="50px"></a>' : '<a href='+value.bpro_shipping_marks+' target="_blank"><img src="{{ asset("public/admin/images/check.jpg") }}" width="50px"></a>'}</div>
                                        </td>
                                        <td>
                                          <div class="text-center">${(value.bpro_barcode_new==null) ? '<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><img src="{{ asset("public/admin/images/uncheck.jpg") }}" width="50px"></a>' : '<a href='+value.bpro_barcode_new+' target="_blank"><img src="{{ asset("public/admin/images/check.jpg") }}" width="50px"></a>'}</div>
                                        </td>
                                        <td>
                                          <div class="text-center">${(value.bpro_care_instruction==null) ? '<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><img src="{{ asset("public/admin/images/uncheck.jpg") }}" width="50px"></a>' : '<a href='+value.bpro_care_instruction+' target="_blank"><img src="{{ asset("public/admin/images/check.jpg") }}" width="50px"></a>'}</div>
                                        </td>
                                        <td>
                                          <div class="text-center">${(value.bpro_logo==null) ? '<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><img src="{{ asset("public/admin/images/uncheck.jpg") }}" width="50px"></a>' : '<a href='+value.bpro_logo+' target="_blank"><img src="{{ asset("public/admin/images/check.jpg") }}" width="50px"></a>'}</div>
                                        </td>
                                        <td>
                                          <div class="text-center">${(value.bpro_label==null) ? '<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><img src="{{ asset("public/admin/images/uncheck.jpg") }}" width="50px"></a>' : '<a href='+value.bpro_label+' target="_blank"><img src="{{ asset("public/admin/images/check.jpg") }}" width="50px"></a>'}</div>
                                        </td>
                                        <td>
                                          <div class="text-center">${(value.bpro_cartoon_labelling==null) ? '<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><img src="{{ asset("public/admin/images/uncheck.jpg") }}" width="50px"></a>' : '<a href='+value.bpro_cartoon_labelling+' target="_blank"><img src="{{ asset("public/admin/images/check.jpg") }}" width="50px"></a>'}</div>
                                        </td>
                                        <td>
                                          <div class="text-center">${(value.bpro_other_warning_label_safety_label==null) ? '<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><img src="{{ asset("public/admin/images/uncheck.jpg") }}" width="50px"></a>' : '<a href='+value.bpro_other_warning_label_safety_label+' target="_blank"><img src="{{ asset("public/admin/images/check.jpg") }}" width="50px"></a>'}</div>
                                        </td>
                                    </tr>`;
                            }
                            $("#table_Data tbody").append(html);
                            
                        } else {

                        }
                    }
                });
            } else {
                toastr.error('id is required!');
            }
    });

</script>