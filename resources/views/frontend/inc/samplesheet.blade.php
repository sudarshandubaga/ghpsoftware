<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


@if ($profile->user_role == 'admin')
    <section class="page-header mb-3">
        <div class="container-fluid">
            <div class="clearfix">
                <div class="float-left">
                    <h1>Sample Sheet </h1>
                    <ul class="breadcrumbs clearfix">
                        <li><a href="">Dashboard</a></li>
                        <li class="active">Sample Sheet Upload</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <div class="container-fluid">
        <form method="post" action="SaveFiles" enctype="multipart/form-data">
            @csrf
            <div class="card">
                <h3 class="card-title" style="position: sticky; top: 0; z-index: 999;">
                    <div class="mr-auto">Add Sheet</div>
                    <a href="#save-data" class="ml-auto text-white" title="Save Data" data-toggle="tooltip">
                        <i class="icon-save"></i> Save
                    </a>
                </h3>
                <section class="add-block">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Select Buyer</label>
                        <select class="form-control" id="exampleFormControlSelect1" name="BuyerName" value=""
                            required>
                            <option value="">Select Buyer</option>
                            @foreach ($GetBuyer as $GB)
                                <option value="{{ $GB->user_id }}">{{ $GB->user_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Sheet Title*</label>
                        <input type="text" class="form-control" id="exampleFormControlTextarea1" name="Title"
                            value="" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Sheet Remark*</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" value="" name="remark"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Sheet Keywords</label>
                        <input type="text" name="keywords" value="" id="exampleFormControlTextarea1"
                            class="form-control taginput">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Planted ETD*</label>
                        <input type="date" class="form-control" id="exampleFormControlTextarea1" name="planetd"
                            value="" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Requested ETD*</label>
                        <input type="date" class="form-control" id="exampleFormControlTextarea1" name="requested"
                            value="" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">TRacking URL*</label>
                        <input type="text" class="form-control" id="exampleFormControlTextarea1" name="url"
                            value="" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlFile1">File Upload (only .pdf)*</label>
                        <input type="file" class="form-control-file" id="exampleFormControlFile1" value=""
                            name="PDFFile" required>
                    </div>
                </section>
            </div>
        </form>
    </div>


    <div style="margin:30px 0px 30px 0px;"></div>
@endif

<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>View Sample Sheet </h1>
                <!--<ul class="breadcrumbs clearfix">-->
                <!--    <li><a href="">Dashboard</a></li>-->
                <!--    <li class="active">View Sample Sheet Uploaded</li>-->
                <!--</ul>-->
            </div>
        </div>
    </div>
</section>


<div class="container-fluid">
    <form method="post" enctype="multipart/form-data">
        @csrf
        <div class="card">
            <h3 class="card-title" style="position: sticky; top: 0; z-index: 999;">
                <div class="mr-auto">View Sample Sheet</div>
                <!--<a href="#save-data" class="ml-auto text-white" title="Save Data" data-toggle="tooltip">-->
                <!--    <i class="icon-save"></i> Save-->
                <!--</a>-->
            </h3>
            <section class="add-block">
                <label for="exampleFormControlSelect1">Sample Sheet's</label>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Sr. No.</th>
                            <th scope="col">Buyer Name</th>
                            <th scope="col">Sheet Title</th>
                            <th scope="col">Sheet Remark </th>
                            <th scope="col">Request Date</th>
                            <th scope="col">Planted ETD</th>
                            <th scope="col">View</th>
                            <th scope="col">Track Url</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($GetFiles as $In => $GF)
                            <tr>
                                <th scope="row">{{ $In + 1 }}</th>
                                <th scope="row">{{ $GF->user_name }}</th>
                                <td>{{ $GF->files_title }}</td>
                                <td>
                                    {{ $GF->files_remark }}<br><br>
                                    <b><u>Keywords</u></b><br><br>
                                    {{ $GF->keywords }}
                                </td>
                                <td>{{ date('d-M-Y', strtotime($GF->requested)) }}</td>
                                <td>{{ date('d-M-Y', strtotime($GF->planetd)) }}</td>
                                <td><a href="{{ URL('files/BuyerFiles') }}/{{ $GF->files_file }}"
                                        target="blank">Click Here</a></td>
                                <td><a href="{{ $GF->url }}" target="blank">Click Here</a></td>
                                <td><a href="{{ URL('Samplesheet/Delete') }}/{{ $GF->files_id }}"
                                        onClick="return Conf()">Delete</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </section>
        </div>
    </form>
</div>

<script>
    function Conf() {
        if (confirm("Are You Sure to Delete This Record")) {
            return true;
        }
        return false;
    }
</script>
