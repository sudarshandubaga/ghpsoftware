<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>View PI</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">View Proforma Invoice</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter PI</h3>
    
    <div class="row">
        <div class="col-10">
        <form>
            <div class="row">
                <!-- <div class="col">
                    <div class="form-group">
                        <label>Customer Name</label>
                        <input type="text" name="SearchByName" value="{{ @$_GET['SearchByName'] }}" class="form-control">
                    </div>
                </div> -->
                <div class="col">
                    <div class="form-group">
                        <label>Select Buyer</label>
                        <select name="SearchBuyer" id="" class="select2 form-control">
                            <option value="">Select All Buyer</option>
                            @if(!empty($buyers))
                                @foreach($buyers as $buyer)
                                <option value="{{ $buyer->user_id }}" @if(@$_GET['SearchBuyer'] == $buyer->user_id) selected @endif >{{ ucwords(strtolower($buyer->user_name)) }}</option>
                                @endforeach
                            @endif
                        </select>
                        {{-- <input type="text" name="SearchByName" value="{{ @$_GET['SearchByName'] }}" class="form-control"> --}}
                    </div>
                </div>



                <div class="col">
                    <div class="form-group">
                        <label>Order Ref.</label>
                        <input type="text" name="SearchByOrderRef" value="{{ @$_GET['SearchByOrderRef'] }}" class="form-control">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>PI Number</label>
                        <input type="text" name="SearchByPI" value="{{ @$_GET['SearchByPI'] }}" class="form-control">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>Status</label>
                        <select name="SearchSttaus" class="form-control">
                            <option value="">All</option>
                            <option @if(@$_GET['SearchSttaus'] == "0") selected @endif value="0">Open</option>
                            <option @if(@$_GET['SearchSttaus'] == "1") selected @endif value="1">Shipped</option>
                        </select>
                    </div>
                </div>

                <div class="col">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-primary form-control">Filter</button>
                    </div>
                </div>
                
            </div>
            
        </form>
        </div>
        <div class="col-2">
        <form action="{{ route('exportPi') }}" method="post">
            {{ csrf_field() }}          
            <div class="" >
                <input type="hidden" name="SearchBuyer" value="{{request('SearchBuyer')}}">
                <input type="hidden" name="SearchSttaus" value="{{request('SearchSttaus')}}">
                <label>&nbsp;</label>
                <input type="submit" class="btn btn-primary form-control" value="Export">
            </div>       
        </form>
        </div>
    </div>
</div>



<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">List Proforma Invoice (PI)</div>
                        <div class="ml-auto">
                            <a href="" class="text-white" title="Remove" data-toggle="tooltip"> <i class="icon-trash-o"></i> </a>
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i class="icon-refresh"></i> </a>
                        </div>
                    </h3>
                    <div class="basic-info-two">

                        @if(!$records->isEmpty())
                        <div class="table-responsive"  style="height: 600px; border: #000 solid 0px;">
                            <table class="table table-bordered table-hover table-header-fix">
                                <thead>
                                <tr>
                                    <th style="width: 50px;">
                                        <label class="animated-checkbox">
                                            <input type="checkbox" class="checkall" >
                                            <span class="label-text"></span>
                                        </label>
                                    </th>
                                    <th style="width: 40px;">Sr. No.</th>
                                    <th style="width: 120px;">Quote ID</th> 
                                    <th style="width: 285px;">Order Ref./ PI No.</th>
                                    <th style="width: 150px;">Buyer Info</th>
                                    <th>Total Amount</th>
                                    <th>Total CBM</th>
                                    <th style="min-width: 100px;">PI Date</th>
                                    <th style="min-width: 100px;">Delivery Date</th>
                                    <th style="min-width: 100px;">Created At</th>
                                    <th style="min-width: 140px;">Actions</th>
                                    <th style="min-width: 100px;">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $sn = $records->firstItem(); @endphp
                                @foreach($records as $rec)
                                

                                    <tr @if($rec->pi_status == "0") class="alert alert-success" @else class="alert alert-danger" @endif>
                                        <td>

                                            @if($rec->pi_confirm == 0)
                                            <label class="animated-checkbox">
                                                <input type="checkbox" name="check[]" value="{{ $rec->user_id  }}" class="check">
                                                <span class="label-text"></span>
                                            </label>
                                            @endif

                                        </td>
                                        <td>{{ $sn++ }}.</td>
                                        <td>{{ sprintf("%s%04d", $site->setting_quote_prefix, $rec->quote_number) }}</td>  
                                        <td>
                                            <div class="row mb-1">
                                                <div class="col-4">
                                                    <strong>O.R.:</strong>
                                                </div>
                                                <div class="col-8">
                                                    @if($rec->pi_order_ref != "")
                                                        {{ $rec->pi_order_ref }}
                                                    @endif

                                                    @if($rec->pi_order_ref == "")
                                                        {{ sprintf("%s-%04d", 'GHP-202122', $rec->pi_id+100) }}
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="row mb-1">
                                                <div class="col-4">
                                                    <strong>PI# :</strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ sprintf("%s-%03d", 'GHP-202122', $rec->pi_id+100) }}
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="row mb-1">
                                                <div class="col-1">

                                                </div>
                                                <div class="col-11">
                                                    <strong>{{ $rec->user_name }}</strong>
                                                </div>
                                            </div>
                                            <div class="row mb-1">
                                                <div class="col-1">

                                                </div>
                                                <div class="col-11">
                                                    {{ $rec->user_mobile }}
                                                </div>
                                            </div>
                                            <!--<div class="row mb-1">-->
                                            <!--    <div class="col-1">-->

                                            <!--    </div>-->
                                            <!--    <div class="col-11">-->
                                            <!--        {{ $rec->user_email }}-->
                                            <!--    </div>-->
                                            <!--</div>-->
                                        </td>
                                        <td>{{ $rec->currency_sign }} {{ number_format($rec->quote_total,2) }}</td>
                                        <td>{{ number_format($rec->totalCbm,2) }}</td>
                                        <td>{{ date("d-M-Y", strtotime($rec->pi_date)) }}</td>
                                        <td>{{ date("d-M-Y", strtotime($rec->pi_delivery_date)) }}</td>
                                        <td>{{ date("d-M-Y h:i A", strtotime($rec->pi_created_on)) }}</td>
                                        <td>
                                            <div class="mb-1">
                                                <a href="{{ url('performa-invoice/print/'.$rec->pi_id) }}" title="Print PI" data-toggle="tooltip" target="_blank"><i class="icon-print"></i> Print</a>
                                            </div>
                                            @if($rec->pi_confirm == 0)
                                            <div class="mb-1">
                                                <a style="cursor: pointer" onClick="OpenModel({{ $rec->pi_id }})"><i class="icon-check"></i> Confirm PI</a>
                                            </div>
                                            <a href="{{ url('performa-invoice/edit/'.$rec->pi_qid.'/'.$rec->pi_id) }}"><i class="icon-pencil"></i> Edit</a>
                                            @else
                                                <div class="mb-1">
                                                    <b>Confirm Date : </b><br> {{ date("d M, Y", strtotime($rec->pi_confirm_date)) }}</a><br>
                                                </div>
                                                @php
                                                    $po = \App\Models\POModel::where("po_pi_id", $rec->pi_id)->first();
                                                    $inc = 100;
                                                    foreach ($rec->porders as $a => $each) {
                                                        echo "<b>PO# NO</b> - 2021-".($each->po_id + $inc);
                                                        echo '<br>';
                                                    }
                                                    // if(!empty($po->po_id)){

                                                    // }
                                                @endphp
                                            @endif
                                             <!--<a href="{{ url('performa-invoice/edit/'.$rec->pi_qid.'/'.$rec->pi_id) }}"><i class="icon-pencil"></i> Edit</a>-->

                                        </td>
                                        <td>
                                            @if($rec->pi_status == 0)
                                                Open
                                            @else
                                                Close
                                            @endif
                                            <br><br>
                                            @if($rec->pi_ship_status == 0)
                                                Not Shipped
                                            @else
                                                Shipped
                                            @endif


                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                         </div>
                        {{ $records->appends(request()->query())->links() }}
                        @else
                        <div class="no_records_found">
                          No records found yet.
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirm PI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="PIIDD">
        <label>Confirm Date</label>
        <input type="date" id="ConfirmDate" class="form-control" value="{{ date('Y-m-d') }}">
        <br>
        <label>DP Date</label>
        <input type="date" id="DPDate" class="form-control" value="{{ date('Y-m-d') }}">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" onClick="SavePI()">Save changes</button>
      </div>
    </div>
  </div>
</div>


<script>
function OpenModel(ID){
    $("#PIIDD").val(ID);
    $('#exampleModal').modal('toggle')
}

function SavePI(){
    PIID = $("#PIIDD").val();
    PIDate = $("#ConfirmDate").val();
    DPDate = $("#DPDate").val();

    if(PIDate == ""){
        alert("Please Enter Confirmation Date");
        return;
    }

    if(DPDate == ""){
        alert("Please Enter DP Date");
        return;
    }

    $.ajax({
        url: "{{ URL('ConfirmPI') }}/"+PIID+"/"+PIDate+"/"+DPDate,
        type: "GET",
        contentType: false,
        cache: false,
        processData:false,
        success: function( data, textStatus, jqXHR ) {
            window.location.reload()
        },
        error: function( jqXHR, textStatus, errorThrown ) {

        }
    });
}
</script>
