<!--<h4>Admin</h4>-->
<div style="margin:30px 0px 30px 0px;"></div>
<!--<div class="container-fluid">-->
<!--	<div class="row">-->
<!--		<div class="col-lg-4">-->
<!--		    <div class="card mt-3">-->
<!--		        <h3 class="card-title1">Total Buyer</h3>-->
<!--		        <div>-->
<!--    				<div class="text-center dash-icon">-->
<!--    					<i class="icon-users" style="color: #fea11e;"></i>-->
<!--    				</div>-->
<!--    			</div>-->
<!--		        <ul class="list-group">-->
					@php
    					$TotalBuyer = App\Models\UserModel::where('user_role', 'buyer')->where('user_is_deleted', 'N')->count();
    					$TotalActiveBuyer = App\Models\UserModel::where('user_role', 'buyer')->where('user_is_deleted', 'N')->where('user_is_enabled', 'Y')->count();
    					$TotalDeactiveBuyer = App\Models\UserModel::where('user_role', 'buyer')->where('user_is_deleted', 'N')->where('user_is_enabled', 'N')->count();
					@endphp
		<!--		  	<li class="list-group-item">-->
		<!--				<a href="user/buyer" class="list-item-link">Total Buyers</a><br>-->
		<!--				<small>(Total Buyer)</small> : <b>{{ $TotalBuyer }}</b>-->
		<!--			</li>-->
					
		<!--			<li class="list-group-item">-->
		<!--				<a href="#" class="list-item-link">Active Buyers</a><br>-->
		<!--				<small>(Total Active Buyer)</small> : <b>{{ $TotalActiveBuyer }}</b>-->
		<!--			</li>-->
		<!--			<li class="list-group-item">-->
		<!--				<a href="#" class="list-item-link">Deactive Buyers</a><br>-->
		<!--				<small>(Total Deactive Buyer)</small> : <b>{{ $TotalDeactiveBuyer }}</b>-->
		<!--			</li>-->
					
		<!--		</ul>-->
		<!--    </div>-->
		<!--</div>-->
		<!--<div class="col-lg-4">-->
		<!--    <div class="card mt-3">-->
		<!--        <h3 class="card-title2">Total Vendors</h3>-->
		<!--        <div>-->
  <!--  				<div class="text-center dash-icon">-->
  <!--  					<i class="icon-users" style="color: #6acf6f;"></i>-->
  <!--  				</div>-->
  <!--  			</div>-->
		<!--        <ul class="list-group">-->
					@php
    					$TotalVendor = App\Models\UserModel::where('user_role', 'vendor')->where('user_is_deleted', 'N')->count();
    					$TotalActiveVendor = App\Models\UserModel::where('user_role', 'vendor')->where('user_is_deleted', 'N')->where('user_is_enabled', 'Y')->count();
    					$TotalDeactiveVendor = App\Models\UserModel::where('user_role', 'vendor')->where('user_is_deleted', 'N')->where('user_is_enabled', 'N')->count();
					@endphp
		<!--		  	<li class="list-group-item">-->
		<!--				<a href="user/vendor" class="list-item-link">Total Vendors</a><br>-->
		<!--				<small>(Total Buyer)</small> : <b>{{ $TotalVendor }}</b>-->
		<!--			</li>-->
					
		<!--			<li class="list-group-item">-->
		<!--				<a href="#" class="list-item-link">Active Vendors</a><br>-->
		<!--				<small>(Total Active Vendors)</small> : <b>{{ $TotalActiveVendor }}</b>-->
		<!--			</li>-->
		<!--			<li class="list-group-item">-->
		<!--				<a href="#" class="list-item-link">Deactive Vendors</a><br>-->
		<!--				<small>(Total Deactive Vendors)</small> : <b>{{ $TotalDeactiveVendor }}</b>-->
		<!--			</li>-->
		<!--		</ul>-->
		<!--    </div>-->
		<!--</div>-->
		<!--<div class="col-lg-4">-->
		<!--    <div class="card mt-3">-->
		<!--        <h3 class="card-title3">Details</h3>-->
		<!--        <div>-->
  <!--  				<div class="text-center dash-icon">-->
  <!--  					<i class="icon-file-text" style="color: #e47164;"></i>-->
  <!--  				</div>-->
  <!--  			</div>-->
    			@php
    			
    			$TotalPI = App\Models\PIModel::where('pi_is_deleted', 'N')->count();
    			$TotalPIOpen = App\Models\PIModel::where('pi_is_deleted', 'N')->where('pi_status',0)->count();
    			$TotalPIShipped = App\Models\PIModel::where('pi_is_deleted', 'N')->where('pi_status',1)->count();
    			$TotalPI = App\Models\PIModel::where('pi_is_deleted', 'N')->count();
				$TotalPIRevenue = App\Models\PIModel::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')->sum('quote_total');
				$LatestPI = App\Models\PIModel::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
						->join('users AS u', 'q.quote_uid', 'u.user_id')
            			->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            			->orderBy('pi_id', 'DESC')
            			->where('pi_is_deleted', 'N')->paginate(10);
            	$agent = ['FNG','PHI','BWU','LARS','MENI'];
				$agentData = $agentDataRevenue = [];
				foreach($agent as $key => $ag) {
					$agentData[$ag] = App\Models\PIModel::where('pi_is_deleted', 'N')
						->whereHas('quote.buyer.user', function ($q) use ($key) {
							$q->where('agent_group', $key + 1);
						})
						->count();
					$agentDataRevenue[$ag] = App\Models\PIModel::where('pi_is_deleted', 'N')
						->whereHas('quote.buyer.user', function ($q) use ($key) {
							$q->where('agent_group', $key + 1);
						})
						->join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
						->sum('q.quote_total');
				}

				$agentrevenueData = [];
				foreach($agent as $key => $ag) {
					$agentrevenueData[$ag] = App\Models\PIModel::where('pi_is_deleted', 'N')
						->whereHas('quote.buyer.user', function ($q) use ($key) {
							$q->where('agent_group', $key + 1);
						})
						->sum('pi_total');
					}
				$delayedPI = App\Models\PIModel::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
						->join('users AS u', 'q.quote_uid', 'u.user_id')
            			->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            			->orderBy('pi_id', 'DESC')
						->where('pi_delivery_date','<',date('Y-m-d'))
            			->where('pi_is_deleted', 'N')->count();
				$orderPlacesPI = App\Models\PIModel::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
						->join('users AS u', 'q.quote_uid', 'u.user_id')
            			->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
						->with('porder')
						->whereHas('porder')
            			->orderBy('pi_id', 'DESC')
						->where('pi_delivery_date','<',date('Y-m-d'))
            			->where('pi_is_deleted', 'N')->count();	
				$orderPendingPI = App\Models\PIModel::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
						->join('users AS u', 'q.quote_uid', 'u.user_id')
            			->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
						->with('porder')
						->doesntHave('porder')
            			->orderBy('pi_id', 'DESC')
						->where('pi_delivery_date','<',date('Y-m-d'))
            			->where('pi_is_deleted', 'N')->count();
				foreach($LatestPI as $pi){
					$piProdcuts = App\Models\PIProduct::where('pipro_pi_id',$pi->pi_id)->get();
					// dd($pipro);
					$totalCbm = 0;
					foreach($piProdcuts as $pipro) {
						$totalCbm += $pipro->products()->sum('product_cbm') * $pipro->pipro_qty;
					}
					$pi->totalCbm = $totalCbm;
				}

				$GHPInvoiceRevenue = App\Models\GHPInvoice::join('purchase_invoices AS PI', 'PI.pi_id', 'exs_ghp_invoice_pi_id')
                    ->join('quotes AS q', 'q.quote_id', 'PI.pi_qid')
                    ->sum('quote_total');
				$GHPInvoiceCount = App\Models\GHPInvoice::join('purchase_invoices AS PI', 'PI.pi_id', 'exs_ghp_invoice_pi_id')
                    ->join('quotes AS q', 'q.quote_id', 'PI.pi_qid')
                    ->count();

    			$TotalPO = App\Models\POModel::where('po_is_deleted', 'N')->count();
    			$TotalPOShipped = App\Models\POModel::where('po_is_deleted', 'N')->has('po_invoice')->count();
    			$TotalPORun = App\Models\POModel::where('po_is_deleted', 'N')->has('po_invoice','<',1)->count();
    			@endphp
<!--		        <ul class="list-group">-->
<!--					<li class="list-group-item">-->
<!--						<a href="performa-invoice" class="list-item-link">Total PI# : </a><b>{{ $TotalPI }}</b><br>-->
<!--						<small>Total Open </small> : <b>{{ $TotalPIOpen }}</b><br>-->
<!--						<small>Total Shipped </small> : <b>{{ $TotalPIShipped }}</b><br>-->
<!--					</li>-->
<!--					<li class="list-group-item">-->
<!--						<a href="purchase-order" class="list-item-link">Total PO#</a> : <b>{{ $TotalPO }}</b><br>-->
<!--						<small>Total Running</small> : <b>{{ $TotalPORun }}</b><br>-->
<!--						<small>Total Shipped</small> : <b>{{ $TotalPOShipped }}</b>-->
<!--					</li>-->
<!--				</ul>-->
<!--		    </div>-->
<!--		</div>-->
<!--	</div>-->
<!--</div>-->
<!--<div style="margin:30px 0px 30px 0px;"></div>-->
<!--<div class="container-fluid">-->
<!--	<div class="row">-->
<!--		<div class="col-lg-4">-->
<!--		    <div class="card mt-4">-->
<!--		        <h3 class="card-title4">Product's</h3>-->
<!--		        <div>-->
<!--    				<div class="text-center dash-icon">-->
<!--    					<i class="icon-product-hunt" style="color: #d37fe8;"></i>-->
<!--    				</div>-->
<!--    			</div>-->
    			@php
    			$TotalProduct = \DB::table('products')->where('product_is_deleted', 'N')->count();
    			$TotalProductActive = \DB::table('products')->where('product_is_deleted', 'N')->where('product_is_visible','Y')->count();
    			$TotalProductDeactive = \DB::table('products')->where('product_is_deleted', 'N')->where('product_is_visible','N')->count();
    			@endphp
		<!--        <ul class="list-group">-->
		<!--            <li class="list-group-item">-->
		<!--				<a href="#" class="list-item-link">Total Product</a><br>-->
		<!--				<small>(Total Added Product)</small> : <b>{{ $TotalProduct }}</b>-->
		<!--			</li>-->
		<!--            <li class="list-group-item">-->
		<!--				<a href="product/add" class="list-item-link">Add Products </a><br>-->
		<!--				<small>(Add Products)</small>-->
		<!--			</li>-->
		<!--		  	<li class="list-group-item">-->
		<!--				<a href="product" class="list-item-link">View Products</a><br>-->
						<!-- <small>(Total Add Products)</small> -->
		<!--				<small>Total Active Products</small> : <b>{{$TotalProductActive}}</b><br>-->
		<!--				<small>Total Deactive Products</small> : <b>{{$TotalProductDeactive}}</b>-->
		<!--			</li>-->
		<!--		</ul>-->
		<!--    </div>-->
		<!--</div>-->
		<!--<div class="col-lg-4">-->
		<!--    <div class="card mt-3">-->
		<!--        <h3 class="card-title5">Logistic</h3>-->
		<!--        <div>-->
  <!--  				<div class="text-center dash-icon">-->
  <!--  					<i class="icon-web" style="color: #fdbeb6;"></i>-->
  <!--  				</div>-->
  <!--  			</div>-->
    			@php
    			$TotalLogistic = App\Models\Logistics::count();
    			$TotalOpenLogistic = App\Models\Logistics::where('track_status',1)->count();
    			$TotalOpenClearanceLogistic = App\Models\Logistics::where('track_status',2)->count();
    			$TotalShippedLogistic = App\Models\Logistics::where('track_status',3)->count();
    			@endphp
		<!--        <ul class="list-group">-->
		<!--		  	<li class="list-group-item">-->
		<!--				<a href="view-logistic" class="list-item-link">Total Logistic</a> : <b>{{ $TotalLogistic }}</b><br>-->
		<!--				<small>(Total Web Logistic)</small>-->
		<!--			</li>-->
		<!--			<li class="list-group-item">-->
		<!--				<a href="view-logistic" class="list-item-link">View Logistic</a><br>-->
		<!--				<small>Total Open Logistic</small> : <b>{{ $TotalOpenLogistic }}</b><br>-->
		<!--				<small>Total Custom Clearance Process Logistic</small> : <b>{{ $TotalOpenClearanceLogistic }}</b><br>-->
		<!--				<small>Total Shipped Logistic</small> : <b>{{ $TotalShippedLogistic }}</b><br>-->
		<!--			</li> -->
		<!--		</ul>-->
		<!--    </div>-->
		<!--</div>-->
		<!--<div class="col-lg-4">-->
		<!--    <div class="card mt-3">-->
		<!--        <h3 class="card-title5">Enquiries</h3>-->
		<!--        <div>-->
  <!--  				<div class="text-center dash-icon">-->
  <!--  					<i class="icon-web" style="color: #fdbeb6;"></i>-->
  <!--  				</div>-->
  <!--  			</div>-->
    			@php
    			$TotalQuotes = App\Models\QuoteModel::where('quote_is_deleted', 'N')->count();
    			@endphp
		<!--        <ul class="list-group">-->
		<!--		  	<li class="list-group-item">-->
		<!--				<a href="enquiry" class="list-item-link">Web Enquiries</a><br>-->
		<!--				<small>(Total Web Enquiries)</small>-->
		<!--			</li>-->
		<!--			<li class="list-group-item">-->
		<!--				<a href="quote" class="list-item-link">Quotes</a><br>-->
		<!--				<small>(Total created Quotes)</small> : <b>{{ $TotalQuotes }}</b>-->
		<!--			</li> -->
		<!--		</ul>-->
		<!--    </div>-->
		<!--</div>-->
		<!--<div class="col-lg-4">-->
		<!--    <div class="card mt-3">-->
		<!--        <h3 class="card-title6">Invoices</h3>-->
		<!--        <div>-->
  <!--  				<div class="text-center dash-icon">-->
  <!--  					<i class="icon-print1" style="color: #15a7aa;"></i>-->
  <!--  				</div>-->
  <!--  			</div>-->
    			@php
    			$TotalVendorInvoice = App\Models\PoInvoice::count();
    			$TotalGHPInvoice = App\Models\GHPInvoice::count();
    			@endphp
<!--		        <ul class="list-group">-->
<!--					<li class="list-group-item">-->
<!--						<a href="" class="list-item-link">Total Vendor Invoices</a><br>-->
<!--						<small>(Last Created Vendor Invoices) : {{ $TotalVendorInvoice }}</small>-->
<!--					</li>-->
<!--					<li class="list-group-item">-->
<!--						<a href="ghp-invoice-list" class="list-item-link">Total GHP# Invoices</a><br>-->
<!--						<small>(Last Created GHP# Invoices No) : {{ $TotalGHPInvoice }}</small>-->
<!--					</li>-->
<!--				</ul>-->
<!--		    </div>-->
<!--		</div>-->
<!--	</div>-->
<!--</div>-->


<div style="margin:30px 0px 30px 0px;"></div>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-6">
		    <div class="card mt-3">
		    <h3 class="card-title9"> Sales Order</h3>		    	 
			<div class="row">
				<div class="col-lg-4">
					<ul class="list-group" style="margin: 0 -15px 0px -15px;">
					  	<li class="list-group-item" style="border-top: none;">
							<a href="#" class="list-item-link">Unconfirmed Order</a><br>
							<small>(Till not Confirmed)</small>
						</li>
						
						<li class="list-group-item">
							<a href="#" class="list-item-link">Shipped</a><br>
							<small>(Total shipped)</small><b>: {{@$TotalPIShipped}}</b>
						</li>
						<li class="list-group-item">
							<a href="#" class="list-item-link">Open</a><br>
							<small>(Total Open)</small><b>: {{@$TotalPIOpen}}</b>
						</li> 
					</ul>
				</div>
				<div class="col-lg-4">
					<ul class="list-group" style="margin: 0 -15px 0px -15px;">
					  	<li class="list-group-item" style="border-top: none;"> 
						</li>
						<li class="list-group-item" style="border-top: none;">
							<a href="#" class="list-item-link">Total Count</a><br>
							<small>(Total shipped)</small><b> : {{@$TotalPI}}</b>
						</li>
						<li class="list-group-item"> 
						</li> 
					</ul>
				</div>
				<div class="col-lg-4">
					<ul class="list-group" style="margin: 0 -15px 0px -15px;">
					  	<li class="list-group-item" style="border-top: none;"></li>
						<li class="list-group-item" style="border-top: none;" ></li> 
						<li class="list-group-item" style="border-top: none;">
							<a href="quote" class="list-item-link">Total Revenue</a><br>
							<small>(Total Revenue)</small>: ${{ number_format($TotalPIRevenue,2) }}</b>
						</li>
					</ul>
				</div>
			</div>
		    <div style="margin:10px 0px 10px 0px;"></div>
				<table class="table-bordered">
					  <tr>
					    <th>Order Placed</th>
					    <th>Order Pending</th> 
					    <th>Delayed</th>
					  </tr>
					  <tr>
					    <td>{{$orderPlacesPI}}</td>
					    <td>{{$orderPendingPI}}</td>
					    <td>{{$delayedPI}}</td> 
					  </tr>
					  <tr> 
				</table>
		    </div>
		</div>
		<div class="col-lg-6">
		    <div class="card mt-3" style="min-height: 0px;">
		    	<h3 class="card-title7">Sales Invoice Summery</h3>	
		    	<div class="row">	    	 
			    	 <div class="col-lg-4" style="min-height: 155px;">
						<ul class="list-group" style="margin: 0 -15px 0px -15px;">
						  	<li class="list-group-item" style="border-top: none;">
								 
							</li>
							<li class="list-group-item" style="border-top: none;">
								 
							</li>
							<li class="list-group-item" style="border-top: none;">
								 
							</li> 
						</ul>
					</div>
					<div class="col-lg-4">
						<ul class="list-group" style="margin: 0 -15px 0px -15px;">
						  	<li class="list-group-item" style="border-top: none;"> 
							</li>
							<li class="list-group-item" style="border-top: none;">
								<a href="#" class="list-item-link">Total Count</a><br>
								<small>(Total GHP Invoice)</small><b> :</b> {{$GHPInvoiceCount}}
							</li>
							<li class="list-group-item"> 
							</li> 
						</ul>
					</div>
					<div class="col-lg-4">
						<ul class="list-group" style="margin: 0 -15px 0px -15px;">
						  	<li class="list-group-item" style="border-top: none;"></li>
							<li class="list-group-item" style="border-top: none;" ></li> 
							<li class="list-group-item" style="border-top: none;">
								<a href="quote" class="list-item-link">Total Revenue</a><br>
								<small>(Total Revenue)</small>: ${{number_format($GHPInvoiceRevenue,2)}}</b>
							</li>
						</ul>
					</div>
				</div>
				<div style="margin:45px 0px 45px 0px;"></div>
				<!--<table class="table-bordered">-->
				<!--	  <tr>-->
				<!--	    <th>Total Order</th>-->
				<!--	    <th>Placed Order</th> -->
				<!--	    <th>Delayed</th>-->
				<!--	  </tr>-->
				<!--	  <tr>-->
				<!--	    <td>100</td>-->
				<!--	    <td>60</td>-->
				<!--	    <td>40</td> -->
				<!--	  </tr>-->
				<!--	  <tr> -->
				<!--</table>-->
		    </div>
		</div>
	</div>
</div>
<div style="margin:30px 0px 30px 0px;"></div>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-4">
			 <div class="card mt-3">
			    <h3 class="card-title8"> Sales Invoice  </h3>		    	 
				<div class="row" style="max-height:230px;overflow:auto">
					<table class="table table-hover table-header-fix">
						<thead>
							<tr>
							  <th>Invoice No</th>
							  <th>Customer Info</th> 
							  <th>Total Amount</th>
							  <th>Total CBM</th>  
							</tr>
						</thead>
						<tbody >
							@if(!empty($LatestPI))
							@foreach($LatestPI as $key => $pi)
							  <tr>
								<td>{{ sprintf("%s-%04d", 'GHP-202122', $pi->pi_id+100) }}</td>
								<td>{{ $pi->user_name }}</td>
								<td>{{ $pi->currency_sign }} {{ number_format($pi->quote_total,2) }}</td> 
								<td>{{ $pi->totalCbm }}</td>  
							  </tr>
							@endforeach
							@endif
						</tbody>
					</table>
				</div>			    	  
		    </div>
		</div>		
		<div class="col-lg-4">
		    <div class="card mt-3" style="min-height: 288px;">
		    	<h3 class="card-title5">Sales Agent Summery</h3>	
		    	<div class="row">	
		    		<!-- <div id="myPlot" style="width:100%;max-height: 230px;"></div>	  -->
					<canvas id="myChart" style="width:100%;max-height: 230px;"></canvas>
				</div> 
		    </div>
		</div>
		<div class="col-lg-4">
		    <div class="card mt-3">
		    <h3 class="card-title4"> Customer Wise Revenue Summery</h3>		    	 
			<div class="row">
				 <!-- <div id="myPlot1" style="width:100%;max-height: 230px;"></div> -->
				 <canvas id="piChart" style="width:100%;max-height: 230px;"></canvas>
			</div>    
 		    </div>
		</div>
	</div>
</div>




<!--PO# wise summery -->
 


@php

        $TotalPO = App\Models\POModel::where('po_is_deleted', 'N')->count();
		$TotalPOShipped = App\Models\POModel::where('po_is_deleted', 'N')->has('po_invoice')->count();
		$TotalPORun = App\Models\POModel::where('po_is_deleted', 'N')->has('po_invoice','<',1)->count();
		
		$TotalPORevenue = App\Models\POModel::sum('po_total');
		
		$TotalVendorInvoice = App\Models\PoInvoice::count();
		
		$totalVendorRevenue = App\Models\POModel::has('po_invoice')->sum('po_total');
    			
@endphp

<div style="margin:30px 0px 30px 0px;"></div>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-6">
		    <div class="card mt-3">
		    <h3 class="card-title9">Purchase Order</h3>		    	 
			<div class="row">
				<div class="col-lg-4">
					<ul class="list-group" style="margin: 0 -15px 0px -15px;">
					 <!-- 	<li class="list-group-item" style="border-top: none;">-->
						<!--	<a href="#" class="list-item-link">Unconfirmed Order</a><br>-->
						<!--	<small>(Till not Confirmed)</small>-->
						<!--</li>-->
						
						<li class="list-group-item">
							<a href="#" class="list-item-link">Running</a><br>
							<small>(Total Running)</small><b>: {{@$TotalPORun}}</b>
						</li>
						<li class="list-group-item">
							<a href="#" class="list-item-link">Shipped</a><br>
							<small>(Total Shipped)</small><b>: {{@$TotalPOShipped}}</b>
						</li> 
					</ul>
				</div>
				<div class="col-lg-4">
					<ul class="list-group" style="margin: 0 -15px 0px -15px;">
					  	<li class="list-group-item" style="border-top: none;"> 
						</li>
						<li class="list-group-item" style="border-top: none;">
							<a href="#" class="list-item-link">Total Count</a><br>
							<small>(Total shipped)</small><b> : {{@$TotalPO}}</b>
						</li>
						<li class="list-group-item"> 
						</li> 
					</ul>
				</div>
				<div class="col-lg-4">
					<ul class="list-group" style="margin: 0 -15px 0px -15px;">
					  	<li class="list-group-item" style="border-top: none;"></li>
						<li class="list-group-item" style="border-top: none;" ></li> 
						<li class="list-group-item" style="border-top: none;">
							<a href="quote" class="list-item-link">Total Revenue</a><br>
							<small>(Total Revenue)</small>: ${{ number_format($TotalPORevenue,2) }}</b>
						</li>
					</ul>
				</div>
			</div>
		    <div style="margin:10px 0px 10px 0px;"></div>
				<table class="table-bordered">
					  <tr>
					    <th>Order Running</th>
					    <th>Order Shipped</th> 
					    <th>Delayed</th>
					  </tr>
					  <tr>
					    <td></td>
					    <td></td>
					    <td></td> 
					  </tr>
					  <tr> 
				</table>
		    </div>
		</div>
		<div class="col-lg-6">
		    <div class="card mt-3" style="min-height: 0px;">
		    	<h3 class="card-title7">Purchase Invoice Summery</h3>	
		    	<div class="row">	    	 
			    	 <div class="col-lg-4" style="min-height: 155px;">
						<ul class="list-group" style="margin: 0 -15px 0px -15px;">
						  	<li class="list-group-item" style="border-top: none;">
								 
							</li>
							<li class="list-group-item" style="border-top: none;">
								 
							</li>
							<li class="list-group-item" style="border-top: none;">
								 
							</li> 
						</ul>
					</div>
					<div class="col-lg-4">
						<ul class="list-group" style="margin: 0 -15px 0px -15px;">
						  	<li class="list-group-item" style="border-top: none;"> 
							</li>
							<li class="list-group-item" style="border-top: none;">
								<a href="#" class="list-item-link">Total Count</a><br>
								<small>(Total PO# Invoice)</small><b> :{{ $TotalVendorInvoice }}</b> 
							</li>
							<li class="list-group-item"> 
							</li> 
						</ul>
					</div>
					<div class="col-lg-4">
						<ul class="list-group" style="margin: 0 -15px 0px -15px;">
						  	<li class="list-group-item" style="border-top: none;"></li>
							<li class="list-group-item" style="border-top: none;" ></li> 
							<li class="list-group-item" style="border-top: none;">
								<a href="quote" class="list-item-link">Total Revenue</a><br>
								<small>(Total Revenue)</small>: ${{ number_format($totalVendorRevenue,2) }}</b>
							</li>
						</ul>
					</div>
				</div>
				<div style="margin:45px 0px 45px 0px;"></div>
				<!--<table class="table-bordered">-->
				<!--	  <tr>-->
				<!--	    <th>Total Order</th>-->
				<!--	    <th>Placed Order</th> -->
				<!--	    <th>Delayed</th>-->
				<!--	  </tr>-->
				<!--	  <tr>-->
				<!--	    <td>100</td>-->
				<!--	    <td>60</td>-->
				<!--	    <td>40</td> -->
				<!--	  </tr>-->
				<!--	  <tr> -->
				<!--</table>-->
		    </div>
		</div>
	</div>
</div> 
@php
	$LatestPO       = App\Models\POModel::join('vendor_quotes AS q', 'q.vquote_id', 'purchase_orders.po_qid')
			->join('users AS u', 'purchase_orders.po_vendor', 'u.user_id')
			->join('currencies AS c', 'q.vquote_currency', 'c.currency_id')
			->orderBy('po_id', 'DESC')
			->where('po_is_deleted', 'N')
			->take(10)
			->skip(0)
			->get();
			
	$cities = \App\Models\Vendor::groupBy('vendor_city')->where('vendor_city', '!=', '')->pluck('vendor_city');
	
	$cityData = $cityDataRevenue = [];
	foreach($cities as $key => $ct) {
		$cityData[$ct] = App\Models\POModel::where('po_is_deleted', 'N')
			->whereHas('quote.vendor', function ($q) use ($ct) {
				$q->where('vendor_city', $ct);
			})
			->count();
			
		$cityDataRevenue[$ct] = App\Models\POModel::where('po_is_deleted', 'N')
			->whereHas('quote.vendor', function ($q) use ($ct) {
				$q->where('vendor_city', $ct);
			})
			->sum('po_total');
	}
    
	$agentrevenueData = [];
	foreach($agent as $key => $ag) {
		$agentrevenueData[$ag] = App\Models\PIModel::where('pi_is_deleted', 'N')
			->whereHas('quote.buyer.user', function ($q) use ($key) {
				$q->where('agent_group', $key + 1);
			})
			->sum('pi_total');
		}
	$delayedPI = App\Models\PIModel::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
			->join('users AS u', 'q.quote_uid', 'u.user_id')
			->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
			->orderBy('pi_id', 'DESC')
			->where('pi_delivery_date','<',date('Y-m-d'))
			->where('pi_is_deleted', 'N')->count();
	$orderPlacesPI = App\Models\PIModel::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
			->join('users AS u', 'q.quote_uid', 'u.user_id')
			->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
			->with('porder')
			->whereHas('porder')
			->orderBy('pi_id', 'DESC')
			->where('pi_delivery_date','<',date('Y-m-d'))
			->where('pi_is_deleted', 'N')->count();	
	$orderPendingPI = App\Models\PIModel::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
			->join('users AS u', 'q.quote_uid', 'u.user_id')
			->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
			->with('porder')
			->doesntHave('porder')
			->orderBy('pi_id', 'DESC')
			->where('pi_delivery_date','<',date('Y-m-d'))
			->where('pi_is_deleted', 'N')->count();

	$GHPInvoiceRevenue = App\Models\GHPInvoice::join('purchase_invoices AS PI', 'PI.pi_id', 'exs_ghp_invoice_pi_id')
        ->join('quotes AS q', 'q.quote_id', 'PI.pi_qid')
        ->sum('quote_total');
	$GHPInvoiceCount = App\Models\GHPInvoice::join('purchase_invoices AS PI', 'PI.pi_id', 'exs_ghp_invoice_pi_id')
        ->join('quotes AS q', 'q.quote_id', 'PI.pi_qid')
        ->count();

	$TotalPO = App\Models\POModel::where('po_is_deleted', 'N')->count();
	$TotalPOShipped = App\Models\POModel::where('po_is_deleted', 'N')->has('po_invoice')->count();
	$TotalPORun = App\Models\POModel::where('po_is_deleted', 'N')->has('po_invoice','<',1)->count();
@endphp
<div style="margin:30px 0px 30px 0px;"></div>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-4">
			 <div class="card mt-3">
			    <h3 class="card-title8"> Purchase Invoice (PO)  </h3>		    	 
				<div class="row" style="max-height:230px;overflow:auto">
					<table class="table table-hover table-header-fix">
						<thead>
							<tr>
							  <th>Invoice No</th>
							  <th>Customer Info</th> 
							  <th>Total Amount</th>
							  <th>Total CBM</th>  
							</tr>
						</thead>
						<tbody >
							@if(!empty($LatestPO))
							@foreach($LatestPO as $key => $po)
							  <tr>
								<td>{{ sprintf("%s-%04d", 'GHP-202122', $po->po_id+100) }}</td>
								<td>{{ $po->user_name }}</td>
								<td>{{ $po->currency_sign }} {{ number_format($po->po_total,2) }}</td> 
								<td>{{ $po->po_cbm }}</td>  
							  </tr>
							@endforeach
							@endif
						</tbody>
					</table>
				</div>			    	  
		    </div>
		</div>		
		<div class="col-lg-4">
		    <div class="card mt-3" style="min-height: 288px;">
		    	<h3 class="card-title5">Sales Agent Summery</h3>	
		    	<div class="row">
					<canvas id="poBarChart" style="width:100%;max-height: 230px;"></canvas>
				</div> 
		    </div>
		</div>
		<div class="col-lg-4">
		    <div class="card mt-3">
		    <h3 class="card-title4"> Customer Wise Revenue Summery</h3>		    	 
			<div class="row">
				 <canvas id="poPieChart" style="width:100%;max-height: 230px;"></canvas>
			</div>    
 		    </div>
		</div>
	</div>
</div>


<div style="margin:30px 0px 30px 0px;"></div>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-4">
			 <div class="card mt-3">
			    <h3 class="card-title8">Logistic Summary</h3>		    	 
				 <div class="row">
				<div class="col-lg-6">
					<ul class="list-group" style="margin: 0 -15px 0px -15px;">
					  	<li class="list-group-item" style="border-top: none;">
							<a href="#" class="list-item-link">Open</a><br>
							<small>(Total Open)</small> <b>: {{ $TotalOpenLogistic }}</b>
						</li>
						<li class="list-group-item">
							<a href="#" class="list-item-link">Custom Clearance Process</a><br>
							<small>(Total Custom Clearance Process)</small><b>:</b>
						</li>
						<li class="list-group-item">
							<a href="#" class="list-item-link">Shipped</a><br>
							<small>(Total Shipped)</small> <b>: {{ $TotalOpenClearanceLogistic }}</b>
						</li> 
					</ul> 
				</div>
				<div class="col-lg-6">
					<ul class="list-group" style="margin: 0 -15px 0px -15px;">
						  	<li class="list-group-item" style="border-top: none;"> 
							</li>
							<li class="list-group-item" style="border-top: none;">
								<a href="#" class="list-item-link">Total Count</a><br>
								<small>(Total Logistic)</small> <b> : {{ $TotalShippedLogistic }}</b>
							</li>
							<li class="list-group-item"> 
							</li> 
						</ul>
				</div> 
			</div>			    	  
		    </div>
		</div>		
		<div class="col-lg-4">
		    <div class="card mt-3" style="min-height: 0px;">
		    	<h3 class="card-title5">Shipment Chart</h3>	
		    	<div class="row">	
		    		<div id="myPlot4" style="width:100%;max-width:420px;max-height: 230px;"></div>	 
				</div> 
		    </div>
		</div>
		<div class="col-lg-4">
		    <div class="card mt-3">
		    <h3 class="card-title4">Customer Wise Staus</h3>		    	 
			<div class="row">
				 <div id="myPlot5" style="width:100%;max-width:420px;max-height: 230px;"></div>
			</div>    
 		    </div>
		</div>
	</div>
</div>
 


<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js" integrity="sha512-QSkVNOCYLtj73J4hbmVoOV6KVZuMluZlioC+trLpewV8qMjsWqlIQvkn1KGX2StWvPMdWGBqim1xlC8krl1EKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: {!! json_encode(array_keys($agentData)) !!},
        datasets: [{
            label: '',
            data: {!! json_encode(array_values($agentData)) !!},
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

var ctx = document.getElementById('piChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: {!! json_encode(array_keys($agentDataRevenue)) !!},
        datasets: [{
            label: '',
            data: {!! json_encode(array_values($agentDataRevenue)) !!},
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

// Purchase Order

var ctx = document.getElementById('poBarChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: {!! json_encode(array_keys($cityData)) !!},
        datasets: [{
            label: '',
            data: {!! json_encode(array_values($cityData)) !!},
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

var ctx = document.getElementById('poPieChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: {!! json_encode(array_keys($cityDataRevenue)) !!},
        datasets: [{
            label: '',
            data: {!! json_encode(array_values($cityDataRevenue)) !!},
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});
</script>

