<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Vendor Commercial Invoice</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">View Vendor Commercial Invoice</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter VENDOR COMMERCIAL INVOICE</h3>
        <div class="row">
            <div class="col-12">
            <form>
                <div class="row">                
                    
                    <div class="col-lg">
                        <div class="form-group">
                            <label>Select Vendor</label>
                            <select name="SearchVendor" id="" class="select2 form-control" >
                                <option value="">Select Vendor</option>
                                @if(!empty($vendors))
                                    @foreach($vendors as $vendor)
                                    <option value="{{ $vendor->user_id }}" @if(@$_GET['SearchVendor'] == $vendor->user_id) selected @endif >{{ ucwords(strtolower($vendor->user_name)) }}</option>
                                    @endforeach
                                @endif
                            </select>
                            {{-- <input type="text" name="SearchByName" value="{{ @$_GET['SearchByName'] }}" class="form-control"> --}}
                        </div>
                    </div>
                    
                    <div class="col-lg">
                            <div class="form-group">
                                <div class="col-sm-5"><label> </label></div>
                                <button type="submit" class="btn btn-primary form-control">Filter</button>
                            </div>                        
                    </div>
                </div>
            </form>
        </div>
        <!-- <div class="col-2">

            <div class="form-group">
                <div class="col">
                    
                    <form action="{{ route('exportPo') }}" method="post">
                        {{ csrf_field() }}          
                        <div class="" >
                            <input type="hidden" name="SearchBypno" value="{{request('SearchBypno')}}">
                            <input type="hidden" name="SearchVendor" value="{{request('SearchVendor')}}">
                            <input type="hidden" name="SearchByPINo" value="{{request('SearchByPINo')}}">
                            <input type="hidden" name="SearchStatus" value="{{request('SearchStatus')}}">
                            <label>&nbsp;</label>
                            <input type="submit" class="btn btn-primary form-control" value="Export">
                        </div>       
                    </form>
                </div>
            </div>
        </div> -->
    </div>
</div>
<div class="container-fluid">   
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">Vendor Commercial Invoice</div>
                    </h3>
                    <div class="basic-info-two">

                        @if(!$records->isEmpty())
                        <div class="table-responsive" style="height: 600px; border: #000 solid 0px;">
                            <table class="table table-bordered table-hover table-header-fix">
                                <thead>
                                <tr>
                                    <th style="width: 50px;">
                                        <label class="animated-checkbox">
                                            <input type="checkbox" class="checkall">
                                            <span class="label-text"></span>
                                        </label>
                                    </th>
                                    <th style="width: 80px;">Sr. No.</th>
                                    <th>Invoice No</th>
                                    <th>Vendor Name</th>
                                    <th>Order Ref./ PI No.</th>
                                    <th>Total Amount</th>
                                    <th>Total CBM</th>
                                    <th>Date</th>
                                    <th>Created At</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $sn = $records->firstItem(); @endphp
                                @foreach($records as $rec)
                                @php
                                    $GetPOData = \App\Models\POModel::find($rec->po_invoice_po);
                                    
                                    $VendorInq = \App\Models\Vendor::where('vendor_uid',$GetPOData->po_vendor)->first();
                                    
                                    $GetPIData = \App\Models\PIModel::find($GetPOData->po_pi_id);
                                    $ghpInvoice = \App\Models\GHPInvoice::where('exs_ghp_invoice_po_invoice_id', $rec->po_invoice_id)->first();
                                @endphp
                                    <tr>
                                        <td>
                                            <label class="animated-checkbox">
                                                <input type="checkbox" name="check[]" value="{{ $rec->user_id  }}" class="check">
                                                <span class="label-text"></span>
                                            </label>
                                        </td>
                                        <td>{{ $sn++ }}.</td> 
                                        <td>{{ $rec->po_invoice_invoice_no }}</td>
                                        <td>{{ @$VendorInq->vendor_org_name }}</td>
                                        <td>
                                            <div class="row mb-1">
                                                <div class="col-4">
                                                    <strong>Orfer Ref. :</strong>
                                                </div>
                                                <div class="col-8">
                                                    @if($GetPIData->pi_order_ref != "")
                                                        {{ $GetPIData->pi_order_ref }}
                                                    @endif

                                                    @if($GetPIData->pi_order_ref == "")
                                                        <!--{{ sprintf("%s-%03d", 'GHP-201819', $rec->pi_id+100) }}-->
                                                         {{ sprintf("%s-%03d", 'GHP-202122', $GetPIData->pi_id+100) }}
                                                    @endif
                                                </div>
                                            </div>
                                            
                                            <div class="row mb-1">
                                                <div class="col-4">
                                                    <strong>PI No. :</strong>
                                                </div>
                                                <div class="col-8">
                                                    {{ sprintf("%s-%03d", 'GHP-202122', $GetPIData->pi_id+100) }}
                                                </div>
                                            </div>       
                                        </td>
                                        @php
                                            $TotalAmount = \App\Models\PoInvoiceProduct::where("exs_po_invoice_products_inv_id", $rec->po_invoice_id)->sum("pip_subtotal");
                                            $TotalCBM = \App\Models\PoInvoiceProduct::where("exs_po_invoice_products_inv_id", $rec->po_invoice_id)->sum("pip_TotalMeas");
                                        @endphp
                                        <td>{{ $rec->currency_sign }} {{ number_format($TotalAmount,2) }}</td>
                                        <td>{{ $TotalCBM }}</td>
                                        <td>{{ date("d/m/Y", strtotime($rec->po_invoice_invoice_date)) }}</td>
                                        <td>{{ date("d/m/Y h:i A", strtotime($rec->po_invoice_created_date)) }}</td>
                                        <td>
                                            <div class="mb-1">
                                                <a href="{{ url('commercial-invoice/print/'.$rec->po_invoice_id) }}" title="Print PI" data-toggle="tooltip" target="_blank"><i class="icon-print"></i> Print</a>
                                            </div>
                                            
                                            @if(!empty($ghpInvoice))
                                                <div class="text-danger">GHP Invoice Already created</div>
                                                <div><b>GHP Invoice No : </b>{{ $ghpInvoice->exs_ghp_invoice_invoice_no}}</div>
                                            @else
                                                @if($profile->user_role == "admin")
                                                <div class="mb-1">
                                                    <a href="{{ url('create-ghp-invoice/'.$rec->po_invoice_id) }}"><i class="icon-print"></i> Create GHP Invoice</a>
                                                </div>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                         </div>
                        {{ $records->links() }}
                        @else
                        <div class="no_records_found">
                          No records found yet.
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
