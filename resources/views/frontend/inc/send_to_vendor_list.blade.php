<style rel="css">
    #searchVendor {
        background-image: url(https://www.w3schools.com/css/searchicon.png);
        background-position: 10px 10px;
        background-repeat: no-repeat;
        height: 46px;
        padding-left: 40px;
    }
</style>
<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Vender List</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="./quote">Quotation</a></li>
                    <li class="active">Vendor List</li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <!-- <a href="{{ url('user/add/') }}" title="Add New" class="btn btn-default" data-toggle="tooltip"> <i class="icon-plus-circle"></i> Add New</a> -->
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <form method="post" id="vendor_list" onkeydown="return event.key != 'Enter';">
        @csrf
        <div class="row">
            <div class="col-sm-8">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <span>Vendor List</span>
                        <div class="ml-auto"></div>
                    </h3>
                    <div class="basic-info-two">
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                            {!! \Session::get('success') !!}
                        </div>
                        @endif
                        <div class="table-responsive">
                            <div class="form-group ui-widget" >
                                <input type="text"  id="searchVendor" placeholder="Enter keyword to search" class="form-control">
                            </div>
                            <table class="table table-bordered " id="VendorTable">
                                <thead>
                                    <tr>
                                        <th style="width: 50px;">
                                            <label class="animated-checkbox">
                                                <input type="checkbox" class="quote_checkall">
                                                <span class="label-text">All</span>
                                            </label>
                                        </th>
                                        <th>Name</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $selected = false; @endphp
                                    @foreach($vendors as $key => $each)
                                    @php
                                        $is_selected = DB::connection('mysql')
                                                    ->table('vendor_enquiries AS ve')
                                                    ->where('ve.venq_enqid', $id)
                                                    ->where('ve.venq_uid', $each->user_id)
                                                    ->first();
                                        $selected = true;

                                    @endphp
                                    <tr>
                                        <td>
                                            <label class="animated-checkbox">
                                                <input type="checkbox" name="quote_check[]" value="{{ $each->user_id }}" @if( !empty($is_selected)) checked @endif class="quote_check">
                                                <span class="label-text"></span>
                                            </label>
                                        </td>
                                        <td>{{ $each->user_name}}</td>
                                        <td>{{ $each->vendor->vendor_tel }}</td>
                                        <td>{{ $each->vendor->vendor_email}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                         </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <button class="btn btn-primary">Select</button>
                </div>
                @if( $is_selected == true )
                <div class="card">
                    <a href="{{ url('assign-vendor/'.$id)}}" class="btn btn-default">Skip</a>
                </div>
                @endif
            </div>
        </div>
    </form>
</div>
<script>

    var myTableArray = [];

    $("table#VendorTable tr").each(function() {
        var arrayOfThisRow = [];
        var tableData = $(this).find('td');
        if (tableData.length > 0) {
            tableData.each(function(key, value) {
                if(key > 0){
                    arrayOfThisRow.push($(this).text());
                    myTableArray.push($(this).text());
                }
            });
        }
    });
    console.log(myTableArray);
    $("#searchVendor").on("keyup", function(){
        if($(this).val() == ''){
            myFunction();
        }
    });
    $('#searchVendor').autocomplete({
        source: myTableArray,
        focus: myFunction,
        select: myFunction,
        change: myFunction
    })

    function showLabel(event, ui) {
        $('#cityLabel').text(ui.item.label)
    }
    function myFunction() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("searchVendor");
        filter = input.value.toUpperCase();
        table = document.getElementById("VendorTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1]; // for column one
            td1 = tr[i].getElementsByTagName("td")[2]; // for column two
            td2 = tr[i].getElementsByTagName("td")[3]; // for column two
        /* ADD columns here that you want you to filter to be used on */
            if (td) {
            if ( (td.innerHTML.toUpperCase().indexOf(filter) > -1) || (td1.innerHTML.toUpperCase().indexOf(filter) > -1) || (td2.innerHTML.toUpperCase().indexOf(filter) > -1) )  {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
            }
        }
    }
</script>
