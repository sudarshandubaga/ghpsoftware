<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Send to Vender</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Send to Vender</li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <!-- <a href="{{ url('user/add/') }}" title="Add New" class="btn btn-default" data-toggle="tooltip"> <i class="icon-plus-circle"></i> Add New</a> -->
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <form method="post" id="send_to_vendor">
        @csrf
        <div class="row">
            <div class="col-sm-8">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <!-- <div class="mr-auto">List Enquiries</div> -->
                        <span>Enquiry Detail</span>
                        <div class="ml-auto">
                            <!-- <a href="" class="text-white" title="Remove" data-toggle="tooltip"> <i class="icon-trash-o"></i> </a> -->
                            &nbsp;
                            <!-- <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i class="icon-refrsh"></i> </a> -->
                        </div>
                    </h3>
                    <div class="basic-info-two">
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                            {!! \Session::get('success') !!}
                        </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Quote ID</th>
                                    <th>Customer Name</th>
                                    <th>Customer Mobile</th>
                                    <th>Customer Email</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td> {{ sprintf("GHP_QT_%06d",$records->quote_id) }} </td>
                                        <td> {{ $records->user_name }} </td>
                                        <td>{{ $records->user_mobile }}</td>
                                        <td>{{ $records->user_email }}</td>
                                        <td>{{ 'DT.'.date("d.m.Y h:i A", strtotime($records->quote_created_on)) }}</td>
                                    </tr>
                                    @php
                                        $quote_products = DB::connection('mysql')
                                            ->table('products AS p')
                                            ->join('quote_products AS qp', 'p.product_id', 'qp.qpro_pid')
                                            ->where('qp.qpro_qid', $records->quote_id)
                                            ->where('qp.qpro_is_approved', 'Y')
                                            ->get();
                                    @endphp
                                    <tr id="oproduct_{{ $records->quote_id }}" >
                                        <td colspan="9">
                                            @if( !empty($quote_products))
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 50px;">
                                                            <label class="animated-checkbox">
                                                                <input type="checkbox" class="quote_checkall">
                                                                <span class="label-text">All</span>
                                                            </label>
                                                        </th>
                                                        <th>Image</th>
                                                        <th>Name</th>
                                                        <th>CBM</th>
                                                        <th>Qty</th>
                                                        <th>CBM Subtotal</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                    $totCBM = $totQty = 0;
                                                    @endphp
                                                    @foreach($quote_products as $p)
                                                    <tr>
                                                        <td>
                                                            <label class="animated-checkbox">
                                                                <input type="checkbox" name="quote_check[]" value="{{ $p->product_id  }}" class="quote_check">
                                                                <span class="label-text"></span>
                                                            </label>
                                                        </td>
                                                        <td width="120">
                                                            <img src="{{ $p->product_image }}" alt="{{ $p->product_name }}" style="width: 100px;">
                                                        </td>
                                                        <td>{{ $p->product_name }}</td>
                                                        <td>{{ $p->product_cbm }}</td>
                                                        <td width="150"><input type="text"  class="form-control" name="quote_qty[{{$p->product_id}}]" value="{{ $p->qpro_qty }}"></td>
                                                        <td>{{ $cbm = $p->product_cbm * $p->qpro_qty }}</td>
                                                    </tr>
                                                        @php
                                                        $totCBM += $cbm;
                                                        $totQty += $p->qpro_qty;
                                                        @endphp
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="4">TOTAL</td>
                                                        <td>{{ $totQty }}</td>
                                                        <td>{{ $totCBM }}</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            @else
                                            <div class="no_records_found">
                                                Quotation is not approved yet.
                                            </div>
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                         </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <a href="#save-data" class="ml-auto text-white" title="Send Enquiry" data-toggle="tooltip">
                            <i class="icon-save"></i> Send
                        </a>
                    </h3>
                    <div class="basic-info-two">
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                            {!! \Session::get('success') !!}
                        </div>
                        @endif
                        @if(!$vendors->isEmpty())
                        <table class="table table-bordered">
                            <thead>
                                 <tr>
                                    <th style="width: 50px;">
                                        <label class="animated-checkbox" style="margin-bottom: 0">
                                            <input type="checkbox" class="checkall">
                                            <span class="label-text">All</span>
                                        </label>
                                    </th>
                                    <th>Name</th>
                                 </tr>
                            </thead>
                            @foreach($vendors as $rec)
                            <tbody>
                                <tr>
                                    <td>
                                        <label class="animated-checkbox" style="margin-bottom: 0">
                                            <input type="checkbox" name="check[]"  value="{{ $rec->user_id  }}" class="check">
                                            <span class="label-text"></span>
                                        </label>
                                    </td>
                                    <td>{{ $rec->user_name }}</td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                        @endif
                    </div>
                </div>
            </div>
            <!-- <div class="col-sm-4">
                <div class="card warning">
                    <h3 class="card-title">Instructions</h3>
                    <div>
                        <ul>
                            <li><i class="icon-check-square"></i> Admin can view users details</li>
                            <li><i class="icon-check-square"></i> Admin can go to add user page</li>
                            <li><i class="icon-check-square"></i> Admin can go to edit user's information</li>
                        </ul>
                    </div>
                </div>
            </div> -->
        </div>
    </form>
</div>
