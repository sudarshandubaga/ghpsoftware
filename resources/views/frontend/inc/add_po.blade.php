<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>{{ !empty($edit->pi_id) ? 'Edit' : 'Add' }} Purchase Order</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="{{ url('quote') }}">Quote</a></li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <a href="{{ url('quote') }}" title="View Quote" class="btn btn-default" data-toggle="tooltip"> <i
                            class="icon-eye"></i> View Quote</a>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">

    @if (\Session::has('danger'))
        <div class="alert alert-danger">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <form data-session="pi_cart" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <h3 class="card-title">
                        <div class="mr-auto">PI Details</div>
                        <div class="ml-auto">
                            <a href="#save-data" class="text-white" title="Save" data-toggle="tooltip"> <i
                                    class="icon-save"></i> Save</a>
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i
                                    class="icon-refresh"></i> </a>
                        </div>
                    </h3>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>PI</label>
                                <select class="form-control" name="record[po_pi_id]">
                                    <option value="">Select PI</option>
                                    @if (!$AllPI->isEmpty())
                                        @foreach ($AllPI as $q)
                                            <option value="{{ $q->pi_id }}">
                                                {{ sprintf('GHP-202122-%03d', $q->pi_id + 100) }} ({{ $q->user_name }} :
                                                {{ $q->pi_order_ref }})</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>PO Date</label>
                                <input type="date" name="record[po_date]"
                                    value="{{ !empty($edit->po_date) ? $edit->po_date : '' }}" class="form-control"
                                    required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <h3 class="card-title">Product / Item Details</h3>
                    <div id="cartResponse">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Product Image</th>
                                    <th width="400">Product Description</th>
                                    <th class="text-center">Dimension [LxWxH]<br><small>(in cms)</small></th>
                                    <th>CBM Per Pc.</th>
                                    @foreach ($quotes as $q)
                                        <th class="text-center">
                                            Revision {{ $q->vquote_version }} <br>
                                            <small>({{ ucwords($q->vquote_added_by) }})</small>
                                            @php
                                                $added_by = $q->vquote_added_by;
                                            @endphp
                                        </th>
                                        <!-- <th>Qty. {{ $q->quote_version }}</th> -->
                                        <!-- <th>Total CBM {{ $q->quote_version }}</th>
                        <th>Subtotal</th>
                        <th>Special Remark</th> -->
                                    @endforeach
                                    <th>Approval</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $sn = $totQty = $totPrice = $totCBM = 0;
                                @endphp
                                @foreach ($products as $p)
                                    @php
                                        $dir = 'imgs/products/';
                                        $image = 'imgs/no-image.png';
                                        if (!empty($p->product_image)) {
                                            $image = $dir . $p->product_image;
                                        }
                                        $subtotal = $p->vqpro_price * $p->vqpro_qty;
                                        $cbm_total = $p->product_cbm * $p->vqpro_qty;
                                        $totQty += $p->vqpro_qty;
                                        $totPrice += $subtotal;
                                        $totCBM += $cbm_total;
                                    @endphp
                                    <tr>
                                        <td>{{ ++$sn }}</td>
                                        <td><img src="{{ url($image) }}" alt="{{ $p->product_name }}"
                                                style="max-width: 70px;"> </td>
                                        <td>{{ $p->product_name }}</td>
                                        <td class="nowrap text-center">
                                            {{ "{$p->product_l_cm} x {$p->product_w_cm} x {$p->product_h_cm}" }} cms
                                        </td>
                                        <td>{{ $p->product_cbm }}</td>
                                        @if (!empty($p->qpros))
                                            @foreach ($p->qpros as $qp)
                                                @php
                                                    $cbm_total = $p->product_cbm * $qp->vqpro_qty;
                                                    $subtotal = $qp->vqpro_price * $qp->vqpro_qty;
                                                @endphp
                                                <td
                                                    @if ($qp->vqpro_is_approved == 'Y') class="bg-success text-white" @endif>
                                                    <div class="nowrap">
                                                        <strong>Price:</strong> {{ $qp->vqpro_price }}
                                                    </div>
                                                    <div class="nowrap">
                                                        <strong>Qty:</strong> {{ $qp->vqpro_qty }}
                                                    </div>
                                                    <div class="nowrap">
                                                        <strong>CBM:</strong> {{ number_format($cbm_total, 3) }}
                                                    </div>
                                                    <div>
                                                        <strong>Remarks:</strong><br>{{ $qp->vqpro_remark }}
                                                        {{ $qp->vqpro_is_approved }}
                                                    </div>
                                                </td>
                                                <!-- <td>{{ number_format($cbm_total, 3) }}</td>
                            <td>{{ $subtotal }}</td>
                            <td>{{ $qp->vqpro_remark }}</td> -->
                                            @endforeach
                                        @endif
                                        <th>
                                            <label class="switch">
                                                <input type="checkbox" rel="approve_qpro"
                                                    data-url="{{ url('ajax/approve_vqpro/' . $p->vqpro_qid . '/' . $p->vqpro_pid) }}"
                                                    @if ($added_by == $profile->user_role) disabled @endif
                                                    @if ($p->vqpro_is_approved == 'Y') checked disabled @endif>
                                                <span class="slider"></span>
                                            </label>
                                        </th>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <!-- <tr>
                        <th colspan="6">TOTAL</th>
                        <th>{{ $totQty }}</th>
                        <th>{{ number_format($totCBM, 6) }}</th>
                        <th>{{ $totPrice }}</th>
                        <th colspan="2"></th>
                    </tr> -->
                            </tfoot>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
