<table class="table table-bordered">
    <thead>
        <tr>
            <th></th>
            <th>PO Number</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($AllPo as $QU)
            <tr>
                <td>
                    <input type="checkbox" name="pos[]" value="{{ $QU }}" class="pos_check"
                        data-label="2021 - {{ $QU + 100 }}">
                </td>
                <td>2021 - {{ $QU + 100 }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
