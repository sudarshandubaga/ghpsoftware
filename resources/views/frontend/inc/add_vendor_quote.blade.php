<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>{{ !empty($edit->vquote_id) ? 'Revise' : 'Add' }} Quote</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="{{ url('vendor-enquiry') }}">Enquiry</a></li>
                    <li class="active">{{ !empty($edit->vquote_id) ? 'Revise' : 'Add' }} Quote</li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <a href="{{ url('vendor-enquiry') }}" title="View Quote" class="btn btn-default" data-toggle="tooltip"> <i class="icon-eye"></i> View Quote</a>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    @if (\Session::has('danger'))
        <div class="alert alert-danger">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <form data-session="vquote_cart" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                	<h3 class="card-title">
                		<div class="mr-auto">Quotation Details</div>
                        <div class="ml-auto">
                            <a href="#save-data" class="text-white" title="Save" data-toggle="tooltip"> <i class="icon-save"></i> Save</a>
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i class="icon-refresh"></i> </a>
                        </div>
                	</h3>
                    @if(empty($edit->vquote_id))
                    	<div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Quotation Date</label>
                                    <input type="text" name="record[vquote_date]" value="{{ !empty($edit->vquote_date) ? $edit->vquote_date : '' }}" placeholder="yyyy-mm-dd" class="form-control datepicker_no_future" required autocomplete="new-password" readonly>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Currency</label>
                                    <select class="form-control" name="record[vquote_currency]">
                                        @foreach($currencies as $c)
                                        <option value="{{ $c->currency_id }}" @if($c->currency_id == @$edit->vquote_currency) selected @endif>{{ $c->currency_short_name.' ('.$c->currency_sign.')' }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Price Term</label>
                                    <select class="form-control" name="record[vquote_price_term]">
                                        <option value="FOB" @if(@$edit->vquote_price_term == 'FOB') selected @endif>FOB</option>
                                        <option value="FOR" @if(@$edit->vquote_price_term == 'FOR') selected @endif>FOR</option>
                                        <option value="CIF" @if(@$edit->vquote_price_term == 'CIF') selected @endif>CIF</option>
                                    </select>
                                </div>
                            </div>
                    	</div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Port of Delivery</label>
                                    <select class="form-control" name="record[vquote_delivery_port]" required="">
                                        <option value="">Select Port</option>
                                        @foreach($ports as $p)
                                        <option value="{{ $p->port_name }}" @if($p->port_name == @$edit->vquote_delivery_port) selected @endif>{{ $p->port_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Estimate Date of Delivery</label>
                                    <input type="date" name="record[vquote_delivery_days]" value="{{ !empty($edit->vquote_delivery_days) }}" class="form-control">
                                </div>
                            </div>
                            
                            <div class="col">
                                <div class="form-group">
                                    <label>Container Type</label>
                                    <input type="text" name="record[vquote_container_type]" value="{{ !empty($edit->vquote_delivery_days) }}" class="form-control">
                                </div>
                            </div>
                            
                            <div class="col">
                                <div class="form-group">
                                    <label>Payment</label>
                                    <input class="form-control" name="record[vquote_payment]" value="{{ @$edit->vquote_payment }}" required="">
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Quotation ID</label>
                                    <input type="text" name="record[vquote_number]" value="{{ $edit->vquote_number }}" readonly class="form-control">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Revised From [Revision No.]</label>
                                    <div class="form-control" readonly>
                                        {{ $edit->vquote_version }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Revision Comment</label>
                                    <input type="text" name="record[vquote_revision_comment]" class="form-control" placeholder="Revision Comment">
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="card">
                    <h3 class="card-title">Product / Item Details</h3>
                	<div class="status"></div>
                    <div id="cartResponse">
                        @php $total_field = 'vquote_total'; $total_cbm_field = 'vquote_total_cbm'; $cart_name = 'vquote_cart'; @endphp
                        @include('frontend.template.cart_table')
                    </div>
                </div>
            </form>
        </div>
        <!-- <div class="col-sm-4">
            <div class="card">
                <h3 class="card-title">Add More Product</h3>
                <form class="cart_form" action="{{ url('ajax/add_to_cart/vquote_cart') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label>Product</label>
                        <select class="form-control" name="cart[cart_pid]" required>
                            <option value="">Select Product</option>
                            @foreach($allproducts as $p)
                            <option value="{{ $p->product_id }}">{{ $p->product_name.' '.$p->product_code }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Qty</label>
                        <input type="number" name="cart[cart_qty]" value="1" min="1" class="form-control" required>
                    </div>
                    <button type="submit" name="button" class="btn btn-primary btn-block">Add To Quote</button>
                </form>
            </div>
        </div> -->
    </div>
</div>
