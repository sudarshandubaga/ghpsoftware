<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Vendor Enquiries</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Vendor Enquiries</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">List Quote Enquiry</div>
                        <div class="ml-auto">
                            <!--  <a href="" class="text-white" title="Remove" data-toggle="tooltip"> <i class="icon-trash-o"></i> </a> -->
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i
                                    class="icon-refresh"></i> </a>
                        </div>
                    </h3>
                    <div class="basic-info-two">

                        @if ($products)
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead style="text-align: center;text-transform: uppercase;">
                                        <tr>
                                            <th rowspan="2">Products Details</th>
                                            <th class="text-center" colspan="{{ count($vendors) * 2 }}">Revisation
                                                Information</th>
                                        </tr>
                                        <tr style="text-transform: uppercase;">
                                            @foreach ($vendors as $key => $each)
                                                <th>GHP</th>
                                                <th>{{ $each->user_name }}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $sn = 0; @endphp
                                        @foreach ($products as $key => $each)
                                            @php $sn++; @endphp
                                            @php
                                                $dir = 'imgs/products/';
                                                $image = 'imgs/no-image.png';
                                                if (!empty($each->product_image)) {
                                                    $image = $dir . $each->product_image;
                                                }
                                                
                                            @endphp
                                            <tr>
                                                <td style="white-space: nowrap;">
                                                    <img src="{{ url($image) }}" style="max-width: 60px;">
                                                    <strong>{{ $each->product_name }}</strong>
                                                    <p><strong>Code: {{ $each->product_code }}</strong></p>
                                                    <p><strong>Dimension: {{ $each->product_l_cm }} x
                                                            {{ $each->product_w_cm }} x
                                                            {{ $each->product_h_cm }}</strong></p>
                                                </td>
                                                @foreach ($vendors as $key2 => $rec)
                                                    @php
                                                        
                                                        $assigned = DB::connection('mysql')
                                                            ->table('vendor_enquiries AS ve')
                                                            ->join('vendor_enqproducts AS veq', 'veq.vpro_enqid', 've.venq_id')
                                                            ->where('ve.venq_enqid', $id)
                                                            ->where('ve.venq_uid', $rec->user_id)
                                                            ->where('veq.vpro_pid', $each->product_id)
                                                            ->select('venq_id')
                                                            ->first();
                                                        
                                                        $quote = DB::connection('mysql')
                                                            ->table('vendor_enquiries AS ve')
                                                            ->join('vendor_quotes AS vq', 've.venq_id', 'vq.vquote_enq_id')
                                                            ->join('currencies AS c', 'c.currency_id', 'vq.vquote_currency')
                                                            ->join('vquote_products AS vqp', 'vq.vquote_id', 'vqp.vqpro_qid')
                                                            ->where('ve.venq_enqid', $id)
                                                            ->where('ve.venq_uid', $rec->user_id)
                                                            ->where('vqp.vqpro_pid', $each->product_id)
                                                            ->where('vq.vquote_added_by', 'vendor')
                                                            ->orderBy('vq.vquote_id', 'desc')
                                                            ->select('vqpro_price', 'vqpro_qty', 'vquote_added_by', 'currency_sign')
                                                            ->first();
                                                        
                                                        $ghp_quote = DB::connection('mysql')
                                                            ->table('vendor_enquiries AS ve')
                                                            ->join('vendor_quotes AS vq', 've.venq_id', 'vq.vquote_enq_id')
                                                            ->join('currencies AS c', 'c.currency_id', 'vq.vquote_currency')
                                                            ->join('vquote_products AS vqp', 'vq.vquote_id', 'vqp.vqpro_qid')
                                                            ->where('ve.venq_enqid', $id)
                                                            ->where('ve.venq_uid', $rec->user_id)
                                                            ->where('vqp.vqpro_pid', $each->product_id)
                                                            ->where('vq.vquote_added_by', 'admin')
                                                            ->orderBy('vq.vquote_id', 'desc')
                                                            ->select('vqpro_price', 'vqpro_qty', 'currency_sign')
                                                            ->first();
                                                        
                                                        $price = $ghp_price = 'N/A';
                                                        if (!empty($quote)) {
                                                            $price = $quote->currency_sign . ' ' . $quote->vqpro_price * $quote->vqpro_qty;
                                                        }
                                                        if (!empty($ghp_quote)) {
                                                            $ghp_price = $ghp_quote->currency_sign . ' ' . $ghp_quote->vqpro_price * $ghp_quote->vqpro_qty;
                                                        }
                                                    @endphp
                                                    @if ($assigned)
                                                        <td style="white-space: nowrap;">{{ $ghp_price }}</td>
                                                        <td style="white-space: nowrap;">{{ $price }}</td>
                                                    @else
                                                        <td class="text-center" colspan="2">Product Not Assigned</td>
                                                    @endif
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <div class="no_records_found">
                                No records found yet.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
