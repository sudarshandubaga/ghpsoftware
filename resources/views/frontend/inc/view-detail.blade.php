@php
    $gross_weight_kg = !empty($record->product_gross_weight_kg) ? unserialize(html_entity_decode($record->product_gross_weight_kg)) : [];
    $gross_weight_lbs = !empty($record->product_gross_weight_lbs) ? unserialize(html_entity_decode($record->product_gross_weight_lbs)) : [];
    
    $dir = 'imgs/products/';
    $image = $image1 = $image2 = $image3 = $image4 = $image5 = 'imgs/no-image.png';
    if (!empty($record->product_image)) {
        $image = $dir . $record->product_image;
    }
    if (!empty($record->product_image1)) {
        $image1 = $dir . $record->product_image1;
    }
    if (!empty($record->product_image2)) {
        $image2 = $dir . $record->product_image2;
    }
    if (!empty($record->product_image3)) {
        $image3 = $dir . $record->product_image3;
    }
    if (!empty($record->product_image4)) {
        $image4 = $dir . $record->product_image4;
    }
    if (!empty($record->product_image5)) {
        $image5 = $dir . $record->product_image5;
    }
@endphp
<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>{{ $record->product_name }}</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="{{ url('') }}">Dashboard</a></li>
                    <li><a href="{{ url('product') }}">Product</a></li>
                    <li class="active">{{ $record->product_name }}</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ url('product/add') }}" class="btn btn-default"> <i class="icon-plus"></i> Add Product</a>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <div class="card">
        <div class="row">
            <div class="col-xl-4">
                <div class="form-group">
                    <img src="{{ url($image) }}"
                        style="width: 100%; border: 1px solid #ccc; height: 430px; object-fit: cover" id="mainImage" />
                </div>

                <div class="row product-thumbnail">
                    <div class="col-2">
                        <img src="{{ url($image) }}"
                            style="width: 100%; border: 1px solid #ccc; object-fit: cover;" />
                    </div>
                    <div class="col-2">
                        <img src="{{ url($image1) }}"
                            style="width: 100%; height: 38px; border: 1px solid #ccc; object-fit: cover;" />
                    </div>
                    <div class="col-2">
                        <img src="{{ url($image2) }}"
                            style="width: 100%; height: 38px; border: 1px solid #ccc; object-fit: cover;" />
                    </div>
                    <div class="col-2">
                        <img src="{{ url($image3) }}"
                            style="width: 100%; height: 38px; border: 1px solid #ccc; object-fit: cover;" />
                    </div>
                    <div class="col-2">
                        <img src="{{ url($image4) }}"
                            style="width: 100%; height: 38px; border: 1px solid #ccc; object-fit: cover;" />
                    </div>
                    <div class="col-2">
                        <img src="{{ url($image5) }}"
                            style="width: 100%; height: 38px; border: 1px solid #ccc; object-fit: cover;" />
                    </div>
                </div>
            </div>

            <div class="col-xl-8">
                <h3 style="margin-bottom: 15px; margin-top: 0px;">Specification</h3>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>Type</th>
                            <td>{{ $record->product_type }}</td>
                        </tr>
                        <tr>
                            <th>USD</th>
                            <td>{{ $record->product_mrp }}</td>
                        </tr>
                        <tr>
                            <th>CBM</th>
                            <td>{{ $record->product_cbm }}</td>
                        </tr>
                        <tr>
                            <th>CFT</th>
                            <td>{{ $record->product_cft }}</td>
                        </tr>
                        <tr>
                            <th>Product Dimension (In CMS)</th>
                            <td>L {{ $record->product_l_cm }} X W {{ $record->product_w_cm }} X H
                                {{ $record->product_h_cm }}</td>
                        </tr>
                        <tr>
                            <th>Product Dimension (In Inch)</th>
                            <td>L {{ $record->product_l_inch }} X W {{ $record->product_w_inch }} X H
                                {{ $record->product_h_inch }}</td>
                        </tr>
                        <tr>
                            <th>Inner Packet (IP)</th>
                            <td>{{ $record->product_ip }}</td>
                        </tr>
                        <tr>
                            <th>Master Packet (MP)</th>
                            <td>{{ $record->product_mp }}</td>
                        </tr>
                        <tr>
                            <th>Buyer Refrence </th>
                            <td>{{ $record->product_text }}</td>
                        </tr>
                        <tr>
                            <th>Specification </th>
                            <td>{{ $record->product_dim_extra }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <h3 class="view-head">Other Specifications</h3>

        <div class="row">
            <div class="col-xl-7">
                <div class="row form-group">
                    <div class="col-xl-12">
                        <h4>Net Weight</h4>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th class="text-center" colspan="2">Wood</th>
                                    <th class="text-center" colspan="2">Iron</th>
                                    <th class="text-center" colspan="2">Other</th>
                                    <th class="text-center" colspan="2">Overall</th>
                                </tr>
                                <tr>
                                    <th class="text-center">Kg</th>
                                    <th class="text-center">LBS</th>
                                    <th class="text-center">Kg</th>
                                    <th class="text-center">LBS</th>
                                    <th class="text-center">Kg</th>
                                    <th class="text-center">LBS</th>
                                    <th class="text-center">Kg</th>
                                    <th class="text-center">LBS</th>
                                </tr>
                                <tr>
                                    <td class="text-center">{{ $record->product_net_weight_wooden_kg }}</td>
                                    <td class="text-center">{{ $record->product_net_weight_wooden_lbs }}</td>
                                    <td class="text-center">{{ $record->product_net_weight_iron_kg }}</td>
                                    <td class="text-center">{{ $record->product_net_weight_iron_lbs }}</td>
                                    <td class="text-center">{{ $record->product_net_weight_other_kg }}</td>
                                    <td class="text-center">{{ $record->product_net_weight_other_lbs }}</td>
                                    <td class="text-center">{{ $record->product_net_weight_kg }}</td>
                                    <td class="text-center">{{ $record->product_net_weight_lbs }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5">
                <h4>Gross Weight</h4>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th class="text-center" colspan="2">Details</th>
                            <th class="text-center" colspan="2">Overall</th>
                        </tr>
                        <tr>
                            <th class="text-center">Kg</th>
                            <th class="text-center">LBS</th>
                            <th class="text-center">Kg</th>
                            <th class="text-center">LBS</th>
                        </tr>
                        <tr>
                            <td class="text-center">{{ implode(' + ', $gross_weight_kg) }}</td>
                            <td class="text-center">{{ implode(' + ', $gross_weight_lbs) }}</td>
                            <td class="text-center">{{ $record->product_tot_gross_weight_kg }}</td>
                            <td class="text-center">{{ $record->product_tot_gross_weight_lbs }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4">
                <h4>Packing</h4>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        @foreach ($packings as $pack)
                            <tr>
                                <th>{{ $pack->packing_name }}</th>
                                <td>{{ $pack->ppkg_value }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <h4>Hardware Specification</h4>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        @foreach ($hardwares as $hw)
                            <tr>
                                <th>{{ $hw->phw_hw_name }}</th>
                                <td>{{ $hw->phw_value }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="col-xl-4">
                <h4>Loadability</h4>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>CBM</th>
                            <td>{{ $record->product_cbm }}</td>
                        </tr>
                        <tr>
                            <th>20' Cont</th>
                            <td>{{ $record->product_load_20cont }}</td>
                        </tr>
                        <tr>
                            <th>40 Std Cont</th>
                            <td>{{ $record->product_load_40std_cont }}</td>
                        </tr>
                        <tr>
                            <th>40 HC Cont</th>
                            <td>{{ $record->product_load_40hc_cont }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            @php
                $MatType1 = \DB::table('material_type')
                    ->where('material_type_id', $record->materialtype1)
                    ->first();
                $MatType2 = \DB::table('material_type')
                    ->where('material_type_id', $record->materialtype2)
                    ->first();
                $Mtrl1 = \DB::table('material')
                    ->where('material_id', $record->material1)
                    ->first();
                $Mtrl2 = \DB::table('material')
                    ->where('material_id', $record->material2)
                    ->first();
                $Fnsh1 = \DB::table('finish')
                    ->where('finish_id', $record->finish1)
                    ->first();
                $Fnsh2 = \DB::table('finish')
                    ->where('finish_id', $record->finish2)
                    ->first();
            @endphp
            <div class="col-xl-4">
                <h4>Materials</h4>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th></th>
                            <th>Primary</th>
                            <th>Secondary</th>
                        </tr>
                        <tr>
                            <th>Material</th>
                            <td>{{ @$Mtrl1->material_name }}</td>
                            <td>{{ @$Mtrl2->material_name }}</td>
                        </tr>
                        <tr>
                            <th>Material Type</th>
                            <td>{{ @$MatType1->material_type_name }}</td>
                            <td>{{ @$MatType2->material_type_name }}</td>
                        </tr>
                        <tr>
                            <th>Finish</th>
                            <td>{{ @$Fnsh1->finish_title }}</td>
                            <td>{{ @$Fnsh2->finish_title }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <h4>File / Documentaion</h4>
                <table class="table table-striped table-bordered">
                    <tr>
                        <th>File 1</th>
                        <td>
                            @if (!empty($record->product_pdf))
                                <a href="{{ url('files/products/' . $record->product_pdf) }}" target="_blank">Download
                                    PDF</a>
                            @else
                                N/A
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>File 2</th>
                        <td>
                            @if (!empty($record->product_pdf2))
                                <a href="{{ url('files/products/' . $record->product_pdf) }}" target="_blank">Download
                                    PDF</a>
                            @else
                                N/A
                            @endif
                        </td>
                    </tr>
                </table>

            </div>

            @php
                $buyers = !empty($record->product_buyers) ? json_decode($record->product_buyers) : [];
            @endphp

            <div class="col-sm-6">
                <h3>Buyers Price</h3>
                <table class="table table-bordered table-striped">
                    @if (!empty($buyers))
                        @foreach ($buyers as $key => $b)
                            @if (!empty($b->name))
                                <tr>
                                    <th>{{ $key + 1 }}. {{ $b->name }}</th>
                                    <td>{{ $b->price }}</td>
                                </tr>
                            @endif
                        @endforeach
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
