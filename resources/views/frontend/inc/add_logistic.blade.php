<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Add Logistic</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Add Logistic</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <form method="post">
        @csrf
        @if (\Session::has('danger'))
            <div class="alert alert-danger">
                {!! \Session::get('danger') !!}</li>
            </div>
        @endif
        <div class="card" id="Form1">
        	<h3 class="card-title">
        		<div class="mr-auto">PI# Form</div>
                <div class="ml-auto">
                    <a href="#save-data" class="text-white" title="Save" data-toggle="tooltip"> <i class="icon-save"></i> Save</a>
                </div>
        	</h3>
        	@php
        	    $Invoiceno = App\Models\PoBookingInvoice::get(); 
        	@endphp
            <div class="row">
                <div class="col">
                <label>Booking No</label>
            	<select name="user[exs_logistics_booking_no]" class="form-control" required id="booking_id" required onChange="LoadPiData(this.value)">
                    <option >Select</option>
            			  @foreach($Invoiceno as $no)
            			        <option @if(@$edit->exs_logistics_booking_no == $no->po_invoice_id) selected @endif value="{{ $no->po_invoice_id }}">{{$no->po_invoice_invoice_no}}</option>
            			    @endforeach
            			</select>
            	</div>
            </div>
            <div id="LoadPI">
            	<div class="row">
            		<div class="col-lg-4">
            			<div >
                            <label>Buyer Name</label>
                            <input type="text" name="exs_logistics_buyer"  class="form-control" required id="BuyerName" value="{{@$edit->exs_logistics_buyer}}">
                        </div>
            		</div>
                    <div class="col-lg-4">
                            	<div >
                                    <label>PI Number</label>
                                      <input type="text" name="exs_logistics_pi"  class="form-control" required id="PiNumber" onChange="LoadPO(this.value)"
                                      value="{{@$edit->exs_logistics_pi}}">
                                       <!-- <select name="user[exs_logistics_pi]" class="form-control" required id="PiNumber" required onChange="LoadPO(this.value)">
                                            <option value="">Select</option>
                                            @foreach($PIData as $RQ)
                                                <option @if(@$edit->exs_logistics_pi == $RQ->pi_id) selected @endif value="{{ $RQ->pi_id }}">GHP-201819-{{ sprintf('%03d', $RQ->pi_id) }}</option>
                                            @endforeach
                                        </select>-->
                                </div>
                            </div>
                    <div class="col-lg-4">
                                <div>
                                    <label>PO Number</label>
                                    <div id="LoadPO">
                                        <input type="text" name="exs_logistics_po"  class="form-control" required id="PiNumber" value="{{@$edit->exs_logistics_po}}" >
                                       <!-- <select name="user[exs_logistics_po]" class="form-control" required id="PoNumber" required onChange="LoadOtherData(this.value)">
                                            <option value="">Select</option>
                                            @foreach($PoData as $RQ)
                                                <option @if(@$edit->exs_logistics_po == $RQ->po_id) selected @endif value="{{ $RQ->po_id }}">1508{{ str_pad($RQ->po_id, 2, "0", STR_PAD_LEFT) }}</option>
                                            @endforeach
                                        </select>-->
                                    </div>
                                </div>
                            </div>
                </div>  
            	
            	<div class="row" >
            		<div class="col">
            			<div class="form-group">
                            <label>GHP Order No.</label>
                			<input type="text" name="user[GHPOrderNo]" class="form-control" id="GHPOrderNo" value="{{ @$edit->GHPOrderNo }}">
                        </div>
            		</div>
                    <div class="col">
            			<div class="form-group">
                            <label>CBM</label>
                			<input type="text" name="user[CBM]" class="form-control" id="CBM" value="{{ @$edit->CBM }}">
                        </div>
            		</div>
                    <div class="col">
                        <div class="form-group">
                            <label>Vendor</label>
                            <input type="text" name="user[Vendor]" class="form-control" id="Vendor" value="{{ @$edit->Vendor }}">
                        </div>
                    </div>
            	</div>
            </div>            	
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>Container Size</label>
            			<select name="user[container_size]" class="form-control">
            			    <option value="">Select</option>
            			    <option @if(@$edit->container_size == "20 FT") selected @endif>20 FT</option>
            			    <option @if(@$edit->container_size == "40 Standard") selected @endif>40 Standard</option>
            			    <option @if(@$edit->container_size == "40 HQ") selected @endif>40 HQ</option>
            			</select>
                    </div>
        		</div>
                <div class="col">
        			<div class="form-group">
                        <label>Order Date</label>
            			<input type="date" name="user[OrderDate]" class="form-control" id="OrderDate" value="{{ @$edit->OrderDate }}">
                    </div>
        		</div>
                <div class="col">
                    <div class="form-group">
                        <label>Agreed ETD</label>
                        <input type="date" name="user[AgreedETD]" class="form-control" value="{{ @$edit->AgreedETD }}">
                    </div>
                </div>
        	</div>
        	
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>Forwarder Name</label>
            			<select name="user[ForwarderName]" class="form-control">
            			    <option value="">Select</option>
            			    @foreach($Forwarder as $FRD)
            			        <option @if(@$edit->ForwarderName == $FRD->forwarder_id) selected @endif value="{{ $FRD->forwarder_id }}">{{ $FRD->forwarder_company_name }}</option>
            			    @endforeach
            			</select>
                    </div>
        		</div>
        		<div class="col">
                    <div class="form-group">
                        <label>Tracking URL</label>
                        <input type="url" name="user[tracking_url]" class="form-control" value="{{ @$edit->tracking_url }}">
                    </div>
                </div>
        		
        	</div>
        	
        	<button class="btn btn-primary" type="button" onClick="ValidateForm()">Next</button>
        </div>
        
        
        <div class="card" id="Form2" style="display:none">
        	<h3 class="card-title">
        		<div class="mr-auto">Vendor Form</div>
        		<div class="ml-auto">
                    <a href="#save-data" class="text-white" title="Save" data-toggle="tooltip"> <i class="icon-save"></i> Save</a>
                </div>
        	</h3>
        	
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>Booking Request Received Date</label>
            			<input type="date" name="user[BookingRequestReceivedDate]" class="form-control" value="{{ @$edit->BookingRequestReceivedDate }}">
                    </div>
        		</div>
                <div class="col">
        			<div class="form-group">
                        <label>P/U Location</label>
            			<input type="text" name="user[PULocation]" class="form-control" value="{{ @$edit->PULocation }}">
                    </div>
        		</div>
                <div class="col">
                    <div class="form-group">
                        <label>H/O Location</label>
                        <input type="text" name="user[HOLocation]" class="form-control" value="{{ @$edit->HOLocation }}">
                    </div>
                </div>
        	</div>
        	
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>Planned Stuffing Date</label>
            			<input type="date" name="user[PlannedStuffingDate]" class="form-control" value="{{ @$edit->PlannedStuffingDate }}">
                    </div>
        		</div>
                <div class="col">
        			<div class="form-group">
                        <label>DO Shared With Vendor</label>
            			<input type="date" name="user[DOSharedWithVendor]" class="form-control" value="{{ @$edit->DOSharedWithVendor }}">
                    </div>
        		</div>
                <div class="col">
                    <div class="form-group">
                        <label>Container Pickup Date</label>
                        <input type="date" name="user[ContainerPickupDate]" class="form-control" value="{{ @$edit->ContainerPickupDate }}">
                    </div>
                </div>
        	</div>
        	
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>Container Nr.</label>
            			<input type="text" name="user[ContainerNr]" class="form-control" value="{{ @$edit->ContainerNr }}" maxlength="11">
                    </div>
        		</div>
                <div class="col">
        			<div class="form-group">
                        <label>Stuffing Date</label>
            			<input type="date" name="user[StuffingDate]" class="form-control" value="{{ @$edit->StuffingDate }}">
                    </div>
        		</div>
                <div class="col">
                    <div class="form-group">
                        <label>SI Submited  by Shipper</label>
                        <input type="date" name="user[SISubmissionDtbyVendor]" class="form-control" value="{{ @$edit->SISubmissionDtbyVendor }}">
                    </div>
                </div>
        	</div>
        	
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>Container  Dispatch Form ICD</label>
            			<input type="date" name="user[Dispatch]" class="form-control" value="{{ @$edit->Dispatch }}">
                    </div>
        		</div>
        		
        		<div class="col">
        			<div class="form-group">
                        <label>Container  Gate IN at Port</label>
            			<input type="date" name="user[GateIN]" class="form-control" value="{{ @$edit->GateIN }}">
                    </div>
        		</div>
        		
        		<div class="col">
        			<div class="form-group">
                        <label>Draft BL shared with shipper</label>
            			<input type="date" name="user[DraftBLsharedwithshipper]" class="form-control" value="{{ @$edit->DraftBLsharedwithshipper }}">
                    </div>
        		</div>
        	</div>
        	
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>Draft BL approved by shipper</label>
            			<input type="date" name="user[DraftBLapprovedwithshipper]" class="form-control" value="{{ @$edit->DraftBLapprovedwithshipper }}">
                    </div>
        		</div>
        		
        		<div class="col">
        			<div class="form-group">
                        <label>Debit Note shared with Shipper</label>
            			<input type="date" name="user[DebitNotesharedwithvendor]" class="form-control" value="{{ @$edit->DebitNotesharedwithvendor }}">
                    </div>
        		</div>
        		
        		<div class="col">
        			<div class="form-group">
                        <label>Debit Note Payment Detail received Date From Shipper</label>
            			<input type="date" name="user[DebitNotePaymentDetailreceivedDate]" class="form-control" value="{{ @$edit->DebitNotePaymentDetailreceivedDate }}">
                    </div>
        		</div>
        	</div>
        	
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>Remark for Shipper</label>
            			<input type="text" name="user[RemarkforVendor]" class="form-control" value="{{ @$edit->RemarkforVendor }}">
                    </div>
        		</div>
        	</div>
        	
        	<div class="row">
        	    <div class="col"><button class="btn btn-primary" type="button" onClick="ShowForm('Form2', 'Form1')">Back</button></div>
        		<div class="col"><button class="btn btn-primary" type="button" onClick="ShowForm('Form2', 'Form3')">Next</button></div>
        	</div>
        </div>
        
        <div class="card" id="Form3" style="display:none">
        	<h3 class="card-title">
        		<div class="mr-auto">Shipping Form</div>
        		<div class="ml-auto">
                    <a href="#save-data" class="text-white" title="Save" data-toggle="tooltip"> <i class="icon-save"></i> Save</a>
                </div>
        	</h3>
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>Booking Placed Date</label>
            			<input type="date" name="user[BookingPlacedDate]" class="form-control" value="{{ @$edit->BookingPlacedDate }}">
                    </div>
        		</div>
                <div class="col">
        			<div class="form-group">
                        <label>DO Received Date</label>
            			<input type="date" name="user[DOReceivedDate]" class="form-control" value="{{ @$edit->DOReceivedDate }}">
                    </div>
        		</div>
                <div class="col">
                    <div class="form-group">
                        <label>Booking Nr.</label>
                        <input type="text" name="user[BookingNr]" class="form-control" value="{{ @$edit->BookingNr }}">
                    </div>
                </div>
        	</div>
        	
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>Shipping Line</label>
            			<select name="user[ShippingLine]" class="form-control">
            			    <option value="">Select</option>
            			    @foreach($ShippingLIne as $ShLi)
            			        <option @if(@$edit->ShippingLine == $ShLi->shipping_line_id) selected @endif value="{{ $ShLi->shipping_line_id }}">{{ $ShLi->shipping_line_name }}</option>
            			    @endforeach
            			</select>
                    </div>
        		</div>
                <div class="col">
        			<div class="form-group">
                        <label>DO Expiry Date</label>
            			<input type="date" name="user[DOExpiryDate]" class="form-control" value="{{ @$edit->DOExpiryDate }}">
                    </div>
        		</div>
                <div class="col">
                    <div class="form-group">
                        <label>Planned ETD Date</label>
                        <input type="date" name="user[PlannedETDDAte]" class="form-control" value="{{ @$edit->PlannedETDDAte }}">
                    </div>
                </div>
        	</div>
        	
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>Planned ETA Date</label>
            			<input type="date" name="user[PlannedETADate]" class="form-control" value="{{ @$edit->PlannedETADate }}">
                    </div>
        		</div>
                <div class="col">
        			<div class="form-group">
                        <label>Port of Discharge</label>
                        <select name="user[PortofDischarge]" class="form-control">
                            <option value="">Select Port</option>
                            @foreach($AllPorts as $APP)
                                <option @if(@$edit->PortofDischarge == $APP->port_name) selected @endif value="{{ $APP->port_name }}">{{ $APP->port_name }}</option>
                            @endforeach
                        </select>
                    </div>
        		</div>
               
        	</div>
        	
        	<div class="row">  
        	        <div class="col-6" style="border: #ccc solid 1px;border-radius: 5px;">
        	        <div class="col">
                    <div class="form-group">
                        <label><h5>2nd Rvsd ETD</h5></label>
                        <input type="checkbox" name="user[T2ndRvsdETD]" value="1" class="form-control" @if(@$edit->T2ndRvsdETD == 1) checked @endif>
                    </div>
                </div>
                <div class="col">
        			<div class="form-group">
                        <label>Do Received Date</label>
            			<input type="date" name="user[DoReceivedDate2]" class="form-control" value="{{ @$edit->DoReceivedDate2 }}">
                    </div>
        		</div>
                <div class="col">
        			<div class="form-group">
                        <label>Booking Nr.</label>
            			<input type="text" name="user[BookingNr2]" class="form-control" value="{{ @$edit->BookingNr2 }}">
                    </div>
        		</div>
                <div class="col">
                    <div class="form-group">
                        <label>Shipping Line</label>
                        <select name="user[ShippingLine2]" class="form-control">
            			    <option value="">Select</option>
            			    @foreach($ShippingLIne as $ShLi)
            			        <option @if(@$edit->ShippingLine2 == $ShLi->shipping_line_id) selected @endif value="{{ $ShLi->shipping_line_id }}">{{ $ShLi->shipping_line_name }}</option>
            			    @endforeach
            			</select>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>Planned  ETD Date</label>
                        <input type="date" name="user[PlannedETDDate2]" class="form-control" value="{{ @$edit->PlannedETDDate2 }}">
                    </div>
                </div>
                
                <div class="col">
                    <div class="form-group">
                        <label>Planned  ETA Date</label>
                        <input type="date" name="user[PlannedETADate2]" class="form-control" value="{{ @$edit->PlannedETADate2 }}">
                    </div>
                </div>
        	        
        	        
        	    </div>
        	        <div class="col-6"  style="border: #ccc solid 1px;border-radius: 5px;">
        	          <div class="col">
                    <div class="form-group">
                        <label><h5>3rd Rvsd ETD</h5></label>
                        <input type="checkbox" name="user[T3rdRvsdETD]" class="form-control" value="1" @if(@$edit->T3rdRvsdETD == 1) checked @endif>
                    </div>
                </div> 
        	        
        	    <!--<div class="col">-->
                <!--    <div class="form-group">-->
                <!--        <label>Comment</label>-->
                <!--        <input type="text" class="form-control" name="user[T3rdRvsdETDComment]" value="{{ @$edit->T3rdRvsdETDComment }}">-->
                <!--    </div>-->
                <!--</div>-->
        	        
        	        
        	        
        	    </div> 
        	     
        	</div>
        	 
 
        	
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>SI Share With Forwarder</label>
            			<input type="date" name="user[SIShareWithForwarder]" class="form-control" value="{{ @$edit->SIShareWithForwarder }}">
                    </div>
        		</div>
        		
        		<div class="col">
        			<div class="form-group">
                        <label>Draft BL Received</label>
            			<input type="date" name="user[DraftBLReceived]" class="form-control" value="{{ @$edit->DraftBLReceived }}">
                    </div>
        		</div>
        		
        		<div class="col">
        			<div class="form-group">
                        <label>Draft BL Approved to Forwarder</label>
            			<input type="date" name="user[DraftBLApprovedbyForwarder]" class="form-control" value="{{ @$edit->DraftBLApprovedbyForwarder }}">
                    </div>
        		</div>
        	</div>
        	
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>Debit Note Revd From Forwarder</label>
            			<input type="date" name="user[DebitNoteRevdFromForwarder]" class="form-control" value="{{ @$edit->DebitNoteRevdFromForwarder }}">
                    </div>
        		</div>
        		
        		<div class="col">
        			<div class="form-group">
                        <label>Debit Note Payment Detail Forwarder</label>
            			<input type="date" name="user[DebitNotePaymentDetail]" class="form-control" value="{{ @$edit->DebitNotePaymentDetail }}">
                    </div>
        		</div>
        		
        		<div class="col">
        			<div class="form-group">
                        <label>Vessel Sailing Date(ATD)</label>
            			<input type="date" name="user[VesselSailingETD]" class="form-control" value="{{ @$edit->VesselSailingETD }}">
                    </div>
        		</div>
        	</div>
        	
        	<div class="row">
        	    <div class="col">
        			<div class="form-group">
                        <label>ETA</label>
            			<input type="date" name="user[VesselSailingETA]" class="form-control" value="{{ @$edit->VesselSailingETA }}">
                    </div>
        		</div>
        		<div class="col">
        			<div class="form-group">
                        <label>Final BL Rcvd Date</label>
            			<input type="date" name="user[FinalBLRcvdDate]" class="form-control" value="{{ @$edit->FinalBLRcvdDate }}">
                    </div>
        		</div>
        		<div class="col">
        			<div class="form-group">
                        <label>BL Surrender request date</label>
            			<input type="date" name="user[BLSurrenderrequestdate]" class="form-control" value="{{ @$edit->BLSurrenderrequestdate }}">
                    </div>
        		</div>
        	</div>
        	
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>BL Surrender copy rcvd</label>
            			<input type="date" name="user[BLSurrendercopyrcvd]" class="form-control" value="{{ @$edit->BLSurrendercopyrcvd }}">
                    </div>
        		</div>
        		
        		<div class="col">
        			<div class="form-group">
                        <label>Remark Forwarder</label>
            			<textarea type="date" name="user[RemarkForwarder]" class="form-control">{{ @$edit->RemarkForwarder }}</textarea>
                    </div>
        		</div>
        	</div>
        	
        	<div class="row">
        	    <div class="col"><button class="btn btn-primary" type="button" onClick="ShowForm('Form3', 'Form2')">Back</button></div>
        		<div class="col"><button class="btn btn-primary" type="button" onClick="ShowForm('Form3', 'Form4')">Next</button></div>
        	</div>
        </div>
        
        <div class="card" id="Form4" style="display:none">
        	<h3 class="card-title">
        		<div class="mr-auto">Buyer Form</div>
        		<div class="ml-auto">
                    <a href="#save-data" class="text-white" title="Save" data-toggle="tooltip"> <i class="icon-save"></i> Save</a>
                </div>
        	</h3>
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>Draft Bl shared with Buyer</label>
            			<input type="date" name="user[DraftBlsharedwithLIZ]" class="form-control" value="{{ @$edit->DraftBlsharedwithLIZ }}">
                    </div>
        		</div>
                <div class="col">
        			<div class="form-group">
                        <label>Draft Bl Conf Rcvd From Buyer</label>
            			<input type="date" name="user[DraftBlConfRcvdFromLIZ]" class="form-control" value="{{ @$edit->DraftBlConfRcvdFromLIZ }}">
                    </div>
        		</div>
                <div class="col">
                    <div class="form-group">
                        <label>Final Bl Shared with Buyer</label>
                        <input type="date" name="user[FinalBlSharedwithLIZ]" class="form-control" value="{{ @$edit->FinalBlSharedwithLIZ }}">
                    </div>
                </div>
        	</div>
        	
        	<div class="row">
        		<div class="col">
        			<div class="form-group">
                        <label>Payment Received</label>
            			<input type="date" name="user[PaymentReceived]" class="form-control" value="{{ @$edit->PaymentReceived }}">
                    </div>
        		</div>
                <div class="col">
        			<div class="form-group">
                        <label>BL Surrender Copy Shared With Buyer</label>
            			<input type="date" name="user[BLSurrenderCopySharedWith]" class="form-control" value="{{ @$edit->BLSurrenderCopySharedWith }}">
                    </div>
        		</div>
                <div class="col">
                    <div class="form-group">
                        <label>Remarks for Buyer</label>
                        <input type="date" name="user[RemarksforLIZ]" class="form-control" value="{{ @$edit->RemarksforLIZ }}">
                    </div>
                </div>
        	</div>
        	
        	<div class="row">
        	    <div class="col"><button class="btn btn-primary" type="button" onClick="ShowForm('Form4', 'Form3')">Back</button></div>
        		<div class="col"><button class="btn btn-primary" type="submit">Save</button></div>
        	</div>
        </div>
    </form>
</div>

<script>
function ShowForm(Current, Target){
    $("#"+Current).slideUp("fast");
    $("#"+Target).slideDown("fast");
}

function ValidateForm(){
    if($("#BuyerName").val() == ""){
        alert("Please Select Buyer Name");
        return false;
    }
    
    if($("#PiNumber").val() == ""){
        alert("Please Select PI");
        return false;
    }
    
    if($("#PoNumber").val() == ""){
        alert("Please Select PO");
        return false;
    }
    
    ShowForm("Form1", "Form2");
}

function LoadPiData(ID){
    $("#LoadPI").html("Loading..");
    $.ajax({
        url: "{{ URL('LoadPIData') }}/"+ID,
        type: "GET",
        contentType: false,
        cache: false,
        processData:false,
        success: function( data, textStatus, jqXHR ) {
            $("#LoadPI").html(data);
        },
        error: function( jqXHR, textStatus, errorThrown ) {
        
        }
    });
}

function LoadPO(ID){
    $("#LoadPO").html("Loading..");
    $.ajax({
        url: "{{ URL('LoadPOData') }}/"+ID,
        type: "GET",
        contentType: false,
        cache: false,
        processData:false,
        success: function( data, textStatus, jqXHR ) {
            $("#LoadPO").html(data);
        },
        error: function( jqXHR, textStatus, errorThrown ) {
        
        }
    });
}

function LoadOtherData(ID){
    $("#LoadOtherData").html("Loading..");
    $.ajax({
        url: "{{ URL('LoadOtherData') }}/"+ID,
        type: "GET",
        contentType: false,
        cache: false,
        processData:false,
        success: function( data, textStatus, jqXHR ) {
            $("#LoadOtherData").html(data);
        },
        error: function( jqXHR, textStatus, errorThrown ) {
        
        }
    });
}
</script>