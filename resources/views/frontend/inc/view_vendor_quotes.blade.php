<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>View Quotes</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">View Vendor Quotes</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<form>
<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter Vendor Quote</h3>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label>Quote ID</label>
                <label>(Type only Numeric Valus of ID)</label>
                <input type="text" name="SearchByQuote" value="{{ @$_GET['SearchByQuote'] }}" class="form-control">
            </div>
        </div>
        
        <div class="col">
            <div class="form-group">
                <label>Vendor Name</label>
                <input type="text" name="SearchByVname" value="{{ @$_GET['SearchByVname'] }}" class="form-control">
            </div>
        </div>
        
        <div class="col">
            <div class="form-group">
                <label>&nbsp;</label>
                <button type="submit" class="btn btn-primary form-control">Filter</button>
            </div>
        </div>
    </div>
</div>
</form>

<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">List Vendor Quotes</div>
                        <div class="ml-auto">
                           <!--  <a href="" class="text-white" title="Remove" data-toggle="tooltip"> <i class="icon-trash-o"></i> </a> -->
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i class="icon-refresh"></i> </a>
                        </div>
                    </h3>
                    <div class="basic-info-two">

                        @if(!$records->isEmpty())
                        <div class="table-responsive" style="height: 500px; border: #000 solid 0px;">
                            <table class="table table-bordered table-hover table-header-fix">
                                <thead>
                                <tr>
                                    <!-- <th style="width: 50px;">
                                        <label class="animated-checkbox">
                                            <input type="checkbox" class="checkall">
                                            <span class="label-text"></span>
                                        </label>
                                    </th> -->
                                    <th style="width: 80px;">Sr. No.</th>
                                    <th>Quote ID</th>
                                    <th>Revision No.</th>
                                    <th>Total Amount</th>
                                    <th>Total CBM</th>
                                    <th>Date</th>
                                    <th>Last Modified</th>
                                    <th>Added By</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $sn = $records->firstItem(); @endphp
                                @foreach($records as $rec) 
                                @php
                                $CountPO = \DB::table('purchase_orders')->where("po_qid", $rec->vquote_id)->count();
                                @endphp
                                    <tr>
<!--                                         <td>
                                            <label class="animated-checkbox">
                                                <input type="checkbox" name="check[]" value="{{ $rec->user_id  }}" class="check">
                                                <span class="label-text"></span>
                                            </label>
                                        </td> -->
                                        <td>{{ $sn++ }}.</td>
                                        <td>{{ sprintf("%s%06d", $site->setting_quote_prefix, $rec->vquote_number) }}</td>
                                        <td>{{ $rec->vquote_version }}</td>
                                        <td>{{ $rec->currency_sign }} {{ number_format($rec->vquote_total,2) }}</td>
                                        <td>{{ $rec->vquote_total_cbm }}</td>
                                        <td>{{ date("d/m/Y", strtotime($rec->vquote_date)) }}</td>
                                        <td>{{ date("d/m/Y h:i A", strtotime($rec->vquote_updated_on)) }}</td>
                                        <td>{{ ucwords($rec->user_name) }}</td>
                                        <td>
                                            <div class="mb-1">
                                                <a href="{{ url('vquote/info/'.$rec->vquote_id) }}" title="View Details" data-toggle="tooltip"><i class="icon-eye"></i> View Details</a>
                                            </div>
                                            <!-- <div class="mb-1">
                                                <a href="{{ url('quote/revisions/'.$rec->vquote_id) }}" title="View Revision Details" data-toggle="tooltip"><i class="icon-file-text"></i> View Revision Details</a>
                                            </div> -->
                                            <!-- <div class="mb-1">
                                                <a href="#" title="View Revision Details" data-toggle="tooltip"><i class="icon-file-text"></i> View Revision Details</a>
                                            </div> -->
                                            <div class="mb-1">
                                                <a href="{{ url('vquote/add/'.$rec->vquote_enq_id.'/'.$rec->vquote_id) }}" title="View Details" data-toggle="tooltip"><i class="icon-repeat"></i> Revise</a>
                                            </div>

                                            @if($profile->user_role == "admin")
                                            @if($CountPO > 0)
                                                <div class="mb-1 bg-danger" style="padding: 5px; color: #FFF">PO already created</div>
                                            @endif
                                            
                                            <div class="mb-1">
                                                <a href="{{ url('purchase-order/add/'.$rec->vquote_enq_id.'/'.$rec->vquote_id) }}" title="Create PO" data-toggle="tooltip"><i class="icon-print"></i> Create PO</a>
                                            </div>
                                            @endif
                                            
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                         </div>
                        {{ $records->links() }}
                        @else
                        <div class="no_records_found">
                          No records found yet.
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
