@php
$gross_weight_kg = !empty($record->product_gross_weight_kg) ? unserialize( html_entity_decode( $record->product_gross_weight_kg ) ) : array();
$gross_weight_lbs = !empty($record->product_gross_weight_lbs) ? unserialize( html_entity_decode( $record->product_gross_weight_lbs ) ) : array();

$dir = "imgs/products/";
$image = $image1 = $image2 = $image3 = $image4 = $image5 = "imgs/no-image.png";
if(!empty($record->product_image)) {
$image = $dir.$record->product_image;
}
if(!empty($record->product_image1)) {
$image1 = $dir.$record->product_image1;
}
if(!empty($record->product_image2)) {
$image2 = $dir.$record->product_image2;
}
if(!empty($record->product_image3)) {
$image3 = $dir.$record->product_image3;
}
if(!empty($record->product_image4)) {
$image4 = $dir.$record->product_image4;
}
if(!empty($record->product_image5)) {
$image5 = $dir.$record->product_image5;
}
@endphp
<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title>{{ $record->product_name }}</title>
    <style>
        body {
            background: #ccc;
            font-family: Arial;
        }

        * {
            box-sizing: border-box;
            font-size: 10px;
        }

        @page {
            margin: 0;
        }

        .print_block {
            width: 210mm;
            margin: 0 auto;
            background: #fff;
            padding: 10px;
        }

        .print_btn {
            position: fixed;
            bottom: 50px;
            right: 50px;
            border-radius: 50%;
            width: 50px;
            height: 50px;
            line-height: 50px;
            text-align: center;
            font-size: 16px;
            border: 1px solid green;
            cursor: pointer;
            background: green;
            color: #fff;
        }

        @media print {
            body {
                margin: 0;
                background: #fff;
            }

            .print_block {
                width: 100%;
                margin: 0;
            }

            .print_btn {
                display: none;
            }
        }

        .table {
            width: 100%;
            border-collapse: collapse;
        }

        .table th,
        .table td {
            border: 1px solid #000;
            padding: 5px;
            text-align: left;
        }

        .main-image {
            width: 60mm;
            height: 60mm;
            object-fit: contain;
        }

        .heading {
            font-weight: bold;
            font-size: 14px;
            text-transform: uppercase;
        }

        .row:after {
            content: "";
            clear: both;
            display: block;
        }

        .col-2 {
            width: 20%;
            float: left;
        }

        .thumbnail-image img {
            width: calc(100% - 4px);
            height: 120px;
            object-fit: contain;
            border: 1px solid #ccc;
            margin: 0 2px;
        }

        .text-center,
        .text-center * {
            text-align: center !important;
        }

        img {
            max-width: 100%;
        }
    </style>
</head>

<body>
    <div class="print_block">
        <table class="table">
            <tr>
                <td colspan="2"><img src="{{ url('imgs/logo1.png') }}" alt="" class="logo" style="width: 100px;"></td>
                <td colspan="14" class="text-center">
                    <div class="heading">
                        Global Home Products Pte. Ltd.
                    </div>
                    <p>1, RAFFLES PLACE, #44-01A, ONE RAFFLES PLACE, SINGAPORE (048616)</p>
                </td>
            </tr>
            <tr>
                <td colspan="4" rowspan="12" style="width: 60mm;">
                    <img src="{{ url($image) }}" alt="{{ $record->product_name }}" class="main-image">
                </td>
                <td colspan="12" style="background: #ebebeb;">
                    <div class="heading">Product Specification</div>
                </td>
            </tr>
            <tr>
                <th colspan="4">Name</th>
                <td colspan="8">{{ $record->product_name }}</td>
            </tr>
            <tr>
                <th colspan="4">Product Code</th>
                <td colspan="8">{{ $record->product_code }}</td>
            </tr>
            <tr>
                <th colspan="4">Product Dimension (In CMS)</th>
                <td colspan="8"><strong>L</strong> {{ $record->product_l_cm }} X <strong>W</strong> {{ $record->product_w_cm }} X <strong>H</strong> {{ $record->product_h_cm }}</td>
            </tr>
            <tr>
                <th colspan="4">Carton Dimension (In CMS)</th>
                <td colspan="8">L {{ $record->product_l_inch }} X W {{ $record->product_w_inch }} X H {{ $record->product_h_inch }}</td>
            </tr>
            <tr>
                <th colspan="4">Type</th>
                <td colspan="8">{{ $record->product_type }}</td>
            </tr>
            <tr>
                <th colspan="4">CBM / CFT</th>
                <td colspan="8">{{ number_format( $record->product_cbm , 3) }} / {{ number_format( $record->product_cft , 3) }}</td>
            </tr>
            <tr>
                <th colspan="4">Inner/Master Packet (IP/MP)</th>
                <td colspan="8">{{ $record->product_ip }} /{{ $record->product_mp }}</td>
            </tr>
            <tr>
                <th colspan="4">MRP</th>
                <td colspan="8">${{ $record->product_mrp }}</td>
            </tr>

            <tr>
                <th colspan="4">Other Specification</th>
                <td colspan="8">{{ $record->product_dim_extra }}</td>
            </tr>
            <tr>
                <th colspan="4">Reference</th>
                <td colspan="8">{{ $record->product_text }}</td>
            </tr>
            <tr>
                <td colspan="16" class="thumbnail-image">
                    <div class="row">
                        <div class="col-2">
                            <img src="{{ url($image1) }}" alt="">
                        </div>
                        <div class="col-2">
                            <img src="{{ url($image2) }}" alt="">
                        </div>
                        <div class="col-2">
                            <img src="{{ url($image3) }}" alt="">
                        </div>
                        <div class="col-2">
                            <img src="{{ url($image4) }}" alt="">
                        </div>
                        <div class="col-2">
                            <img src="{{ url($image5) }}" alt="">
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th colspan="8" style="background: #ebebeb;" class="heading">Weight Inforamtion</th>
                <th colspan="8" style="background: #ebebeb;" class="heading">Material / Finish</th>
            </tr>
            <tr>
                <th colspan="8" style="width: 50%;">Net Weight</th>
                <th colspan="8" style="width: 50%;">Material / Material Type / Finish
            </tr>

            <tr>
                <td colspan="2" class="text-center">Wood</td>
                <td colspan="2" class="text-center">Iron</td>
                <td colspan="2" class="text-center">Other</td>
                <td colspan="2" class="text-center">Overall</td>
                <td colspan="2" class="text-center"></td>
                <td colspan="3" class="text-center">Primary</td>
                <td colspan="3" class="text-center">Secondary</td>

            </tr>
            <tr>
                <th class="text-center">Kg</th>
                <th class="text-center">LBS</th>
                <th class="text-center">Kg</th>
                <th class="text-center">LBS</th>
                <th class="text-center">Kg</th>
                <th class="text-center">LBS</th>
                <th class="text-center">Kg</th>
                <th class="text-center">LBS</th>
                <th class="text-center">Kg</th>
                <th class="text-center" colspan="2">LBS</th>
                <th class="text-center">Kg</th>
                <th class="text-center" colspan="2">LBS</th>
            </tr>
            <tr>
                <td class="text-center">{{ $record->product_net_weight_wooden_kg }}</td>
                <td class="text-center">{{ $record->product_net_weight_wooden_lbs }}</td>
                <td class="text-center">{{ $record->product_net_weight_iron_kg }}</td>
                <td class="text-center">{{ $record->product_net_weight_iron_lbs }}</td>
                <td class="text-center">{{ $record->product_net_weight_other_kg }}</td>
                <td class="text-center">{{ $record->product_net_weight_other_lbs }}</td>
                <td class="text-center">{{ $record->product_net_weight_kg }}</td>
                <td class="text-center">{{ $record->product_net_weight_lbs }}</td>
            </tr>
            <tr>
                <th colspan="8" style="width: 50%;">Gross Weight</th>
                <th colspan="8" style="width: 50%;">Material / Finish</th>
            </tr>
            <tr>
                <th colspan="5" class="heading" style="background: #ebebeb;">Packing</th>
                <th colspan="5" class="heading" style="background: #ebebeb;">Loadability</th>
                <th colspan="6" class="heading" style="background: #ebebeb;">Materials</th>
            </tr>
            <tr>
                <td colspan="5">
                    <table class="table">
                        @foreach($packings as $pack)
                        <tr>
                            <th>{{ $pack->packing_name }}</th>
                            <td>{{ $pack->ppkg_value }}</td>
                        </tr>
                        @endforeach
                    </table>
                </td>
                <td rowspan="3" colspan="5">
                    <table class="table">
                        <tr>
                            <th>CBM</th>
                            <td>{{ $record->product_cbm }}</td>
                        </tr>
                        <tr>
                            <th>20' Cont</th>
                            <td>{{ $record->product_load_20cont }}</td>
                        </tr>
                        <tr>
                            <th>40 Std Cont</th>
                            <td>{{ $record->product_load_40std_cont }}</td>
                        </tr>
                        <tr>
                            <th>40 HC Cont</th>
                            <td>{{ $record->	product_load_40hc_cont }}</td>
                        </tr>
                    </table>
                </td>
                <td rowspan="3" colspan="6">
                    <table class="table">
                        <tr>
                            <th>Wood</th>
                            <td>{{ $record->product_material_wood }}</td>
                        </tr>
                        <tr>
                            <th>Metal</th>
                            <td>{{ $record->product_metal }}</td>
                        </tr>
                        <tr>
                            <th>Glass</th>
                            <td>{{ $record->product_glass }}</td>
                        </tr>
                        <tr>
                            <th>Fabric</th>
                            <td>{{ $record->product_fabric }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th colspan="5" class="heading" style="background: #ebebeb; height: 1em;">Hardware Specification</th>
            </tr>
            <tr>
                <td colspan="5">
                    <table class="table">
                        @foreach($hardwares as $hw)
                        <tr>
                            <th>{{ $hw->phw_hw_name }}</th>
                            <td>{{ $hw->phw_value }}</td>
                        </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                <th colspan="16" class="heading" style="background: #ebebeb;">File / Documentaion</th>
            </tr>
            <tr>
                <th colspan="2">File 1</th>
                <td colspan="6">{{ !empty($record->product_pdf) ? "YES" : "NO" }}</td>
                <th colspan="2">File 2</th>
                <td colspan="6">{{ !empty($record->product_pdf2) ? "YES" : "NO" }}</td>
            </tr>
            <tr>
                <th colspan="8" class="heading" style="background: #ebebeb;">Weight Information</th>
                <th colspan="8" class="heading" style="background: #ebebeb;">Material / Finish</th>
            </tr>
            <td rowspan="3" colspan="8">
                <table class="table">
                    <tr>
                        <th colspan="12">Net Weight</th>
                    </tr>
                    <tr>
                        <th>Wood</th>
                        <th>Iron</th>
                        <th>Other</th>
                        <th>Total </th>
                    </tr>
                    <tr>
                        <td>Iron</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <table class="table">
                    <tr>
                        <th colspan="12">Gross Weight</th>
                    </tr>
                    <tr>
                        <th>Wood</th>
                        <th>Iron</th>
                        <th>Total </th>
                    </tr>
                    <tr>
                        <td>Iron</td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </td>
            @php
            $MatType1 = \DB::table('material_type')->where("material_type_id", $record->materialtype1)->first();
            $MatType2 = \DB::table('material_type')->where("material_type_id", $record->materialtype2)->first();
            $Mtrl1 = \DB::table('material')->where("material_id", $record->material1)->first();
            $Mtrl2 = \DB::table('material')->where("material_id", $record->material2)->first();
            $Fnsh1 = \DB::table('finish')->where("finish_id", $record->finish1)->first();
            $Fnsh2 = \DB::table('finish')->where("finish_id", $record->finish2)->first();
            @endphp
            <td rowspan="3" colspan="8">
                <table class="table table-striped table-bordered">
                    <tr>
                        <th></th>
                        <th>Primary</th>
                        <th>Secondary</th>
                    </tr>
                    <tr>
                        <th>Material</th>
                        <td>{{ @$Mtrl1->material_name }}</td>
                        <td>{{ @$Mtrl2->material_name }}</td>
                    </tr>
                    <tr>
                        <th>Material Type</th>
                        <td>{{ @$MatType1->material_type_name }}</td>
                        <td>{{ @$MatType2->material_type_name }}</td>
                    </tr>
                    <tr>
                        <th>Finish</th>
                        <td>{{ @$Fnsh1->finish_title }}</td>
                        <td>{{ @$Fnsh2->finish_title }}</td>
                    </tr>
                </table>
            </td>

        </table>
    </div>

    <div class="print_btn" onclick="window.print()">
        Print
    </div>
</body>

</html>