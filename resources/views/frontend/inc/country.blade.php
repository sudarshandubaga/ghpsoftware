<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Country</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Country</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid">
	<div class="card mb-5 mt-5">
	    
	    @if(Session::has('Success'))
        <div class="alert alert-success mb-5" role="alert">{!!Session::get('Success')!!}</div>
        @endif
        
        @if(Session::has('Danger'))
        <div class="alert alert-danger mb-5" role="alert">{!!Session::get('Danger')!!}</div>
        @endif
        
		<h3 class="card-title">Add Country</h3>
	     <form method="post">
	     	@csrf
		     <div class="row">
		         <div class="col-sm-6">
		             <div class="row">
		                 <div class="col-sm-3">
		                     <label>
		                         Country Name
		                     </label>
		                 </div>

		                 <div class="col-sm-9">
		                      <div class="form-group">
		                          <input type="text" name="record[country_name]" value="{{ @$edit->country_name }}" class="form-control" required="">
		                      </div>
		                 </div>
		             </div>
		         </div>

		         <div class="col-sm-2">

		         </div>

		         <div class="col-sm-2">
		         	<div class="form-group">
		                 <button type="submit" class="btn btn-primary form-control">SAVE</button>
		             </div>
		             <!-- <div class="form-group">
		                  <a href="{{ url('finish') }}" class="btn btn-default text-center form-control">ADD</a>
		             </div> -->
		         </div>

		         <div class="col-sm-2">

		         </div>

		         <div class="col-sm-6">
		             <div class="row">
		                 <div class="col-sm-3">
		                     <label>
		                         Country Short Name
		                     </label>
		                 </div>

		                 <div class="col-sm-9">
		                     * Allow Max. 2 Character of the Short Name *
		                     <div class="form-group">
		                         <input type="text" name="record[country_short_name]" value="{{ @$edit->country_short_name }}"  maxlength = "2" class="form-control" required="">
		                     </div>
		                 </div>
		             </div>
		         </div>

		         <div class="col-sm-2">

		         </div>

		         <div class="col-sm-2">
		             
		         </div>

		         <div class="col-sm-2">

		         </div>

		         <div class="col-sm-6">
		             <div class="row">
		                 <div class="col-sm-3">
		                     <label>
		                         Country Code
		                     </label>
		                 </div>

		                 <div class="col-sm-9">
		                     <div class="form-group">
		                         <input type="text" name="record[country_code]" value="{{ @$edit->country_code }}" class="form-control" placeholder="Like 1,91 etc." required="">
		                     </div>
		                 </div>
		             </div>
		         </div>

		         <div class="col-sm-2">
		         </div>

		         <div class="col-sm-2">

		         </div>

		         <div class="col-sm-2">

		         </div>
		     </div>
		 </form>
	</div>
	<div class="card mb-5 mt-5">    
        
		<h3 class="card-title">Search Country</h3>
		<div class="row">
			<div class="col-sm-10">

				<form>
					<div class="row">

						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-3">
									<label>
										Country Name
									</label>
								</div>
	   
								<div class="col-sm-9">
									 <div class="form-group">
										 <input type="text" name="country_name" value="{{ request('country_name') }}" class="form-control" >
									 </div>
								</div>
							</div>
						</div>
			  
						<div class="col-sm-4">
							<div class="form-group">
								<button type="submit" class="btn btn-primary form-control">SEARCH</button>
							</div>
							<!-- <div class="form-group">
								<a href="{{ url('finish') }}" class="btn btn-default text-center form-control">ADD</a>
							</div> -->
						</div>
					</div>
				</form>
			</div>
				<div class="col-sm-2">
					<div class="form-group">
						<div class="col">
							
							<form action="{{ route('exportCountry') }}" method="post">
								{{ csrf_field() }}          
								<div class="" >
									<input type="hidden" name="country_name" value="{{request('country_name')}}">
									
									<input type="submit" class="btn btn-primary form-control" value="Export">
								</div>       
							</form>
						</div>
					</div>
				</div>
		     </div>
	</div>

	<form method="post">
		<div class="card">
			<h3 class="card-title">
				<div class="mr-auto">View Country</div>
		        <a href="" class="ml-auto text-white" title="Remove Selected" data-toggle="tooltip">
		            <i class="icon-trash-o"></i>
		        </a>
			</h3>
	    	@csrf
		    @if(!$records->isEmpty())
		    {{ $records->links() }}
		    <div class="table-responsive">
			    <table class="table table-bordered">
			          <thead>
			               <tr>
			                    <th style="width: 50px;">
			                        <label class="animated-checkbox">
			                            <input type="checkbox" class="checkall">
			                            <span class="label-text"></span>
			                        </label>
			                    </th>
			                   <th style="width: 50px;">S.No.</th>
			                   <th>Country Code</th>
			                   <th>Country Name</th>
			                   <th>Country Short Name</th>
			                   <th>Action</th>
			               </tr>
			          </thead>

			          <tbody>
			          		@php $sn = $records->firstItem(); @endphp
			          		@foreach($records as $rec)
			               	<tr>
		                        <td>
		                            <label class="animated-checkbox">
		                                <input type="checkbox" name="check[]" value="{{ $rec->country_id  }}" class="check">
		                                <span class="label-text"></span>
		                            </label>
		                        </td>
								<td>{{ $sn++ }}</td>
								<td>+ {{ $rec->country_code }}</td>
								<td>{{ $rec->country_name }}</td>
								<td>{{ $rec->country_short_name }}</td>
								<td class="icon-cent">
									<a href="{{ url('country/'.$rec->country_id) }}" class="pencil"><i class="icon-pencil" title="Edit"></i></a>
								</td>
			               	</tr>
			               @endforeach
			          </tbody>
			    </table>
			</div>
			{{ $records->links() }}
		    @else
		    <div class="no_records_found">
		      No records found yet.
		    </div>
			@endif
		</div>
	</form>
</div>