<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Vendor Booking Invoice</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">View Vendor Booking Invoice</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter Vendor Booking Invoice</h3>
    <div class="row">
        <div class="col-12">
            <form>
                <div class="row">
                    <div class="col-lg">
                        <div class="form-group">
                            <label>Invoice No</label>
                            <input type="text" name="SearchByInvoiceNo" value="{{ @$_GET['SearchByInvoiceNo'] }}"
                                class="form-control">
                        </div>
                    </div>

                    @if ($profile->user_role == 'admin')
                        <div class="col-lg">
                            <div class="form-group">
                                <label>Select Vendor</label>
                                <select name="SearchVendor" id="" class="select2 form-control">
                                    <option value="">Select Vendor</option>
                                    @if (!empty($vendors))
                                        @foreach ($vendors as $vendor)
                                            <option value="{{ $vendor->user_id }}"
                                                @if (@$_GET['SearchVendor'] == $vendor->user_id) selected @endif>
                                                {{ ucwords(strtolower($vendor->user_name)) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                {{-- <input type="text" name="SearchByName" value="{{ @$_GET['SearchByName'] }}" class="form-control"> --}}
                            </div>
                        </div>
                    @endif

                    <div class="col-lg">
                        <div class="form-group">
                            <div class="col-sm-5"><label> </label></div>
                            <button type="submit" class="btn btn-primary form-control">Filter</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- <div class="col-2">

            <div class="form-group">
                <div class="col">
                    
                    <form action="{{ route('exportPo') }}" method="post">
                        {{ csrf_field() }}
                        <div class="" >
                            <input type="hidden" name="SearchBypno" value="{{ request('SearchBypno') }}">
                            <input type="hidden" name="SearchVendor" value="{{ request('SearchVendor') }}">
                            <input type="hidden" name="SearchByPINo" value="{{ request('SearchByPINo') }}">
                            <input type="hidden" name="SearchStatus" value="{{ request('SearchStatus') }}">
                            <label>&nbsp;</label>
                            <input type="submit" class="btn btn-primary form-control" value="Export">
                        </div>
                    </form>
                </div>
            </div>
        </div> -->
    </div>
</div>
<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">Vendor Booking Invoice</div>
                    </h3>
                    <div class="basic-info-two">
                        @if (!$records->isEmpty())
                            <div class="table-responsive" style="height: 600px; border: #000 solid 0px;">
                                <table class="table table-bordered table-hover table-header-fix">
                                    <thead>
                                        <tr>
                                            <th style="width: 50px;">
                                                <label class="animated-checkbox">
                                                    <input type="checkbox" class="checkall">
                                                    <span class="label-text"></span>
                                                </label>
                                            </th>
                                            <th style="width: 80px;">Sr. No.</th>
                                            <th>Invoice No</th>
                                            @if ($profile->user_role == 'admin')
                                                <th>Vendor Name</th>
                                            @endif
                                            <th>Order Ref./ PI No.</th>
                                            <th>Total Amount</th>
                                            <th>Total CBM</th>
                                            <th>Date</th>
                                            <th>Created At</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $sn = $records->firstItem(); @endphp
                                        @foreach ($records as $rec)
                                            @php
                                                $GetPOData = \App\Models\POModel::find($rec->po_invoice_po);
                                                $GetPIData = \App\Models\PIModel::find($GetPOData->po_pi_id);
                                            @endphp
                                            <tr>
                                                <td>
                                                    <label class="animated-checkbox">
                                                        <input type="checkbox" name="check[]"
                                                            value="{{ $rec->user_id }}" class="check">
                                                        <span class="label-text"></span>
                                                    </label>
                                                </td>
                                                <td>{{ $sn++ }}.</td>
                                                <td>{{ $rec->po_invoice_invoice_no }}</td>
                                                @if ($profile->user_role == 'admin')
                                                    <td>{{ @$rec->vendor->vendor_org_name }}</td>
                                                @endif
                                                <td>
                                                    <div class="row mb-1">
                                                        <div class="col-5">
                                                            <strong>Orfer Ref. :</strong>
                                                        </div>
                                                        <div class="col-7">
                                                            {{ @$GetPIData->pi_order_ref }}
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <div class="col-5">
                                                            <strong>PI No. :</strong>
                                                        </div>
                                                        <div class="col-7">
                                                            {{ @sprintf('%s-%03d', 'GHP-202122', $GetPIData->pi_id + 100) }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>{{ $rec->currency_sign }}
                                                    {{ number_format($rec->po_invoice_total_amount, 2) }}</td>
                                                <td>{{ number_format($rec->po_invoice_total_cbm, 2) }}</td>
                                                <td>{{ date('d/m/Y', strtotime($rec->po_invoice_invoice_date)) }}</td>
                                                <td>{{ date('d/m/Y h:i A', strtotime($rec->po_invoice_created_date)) }}
                                                </td>
                                                <td>
                                                    <div class="mb-1">
                                                        <a href="{{ url('booking-commercial-invoice/print/' . $rec->po_invoice_id) }}"
                                                            target="_blank"><i class="icon-print"></i> Print</a>
                                                    </div>

                                                    @if ($profile->user_role == 'vendor')
                                                        <div class="mb-1">
                                                            <a href="{{ url('purchase-order/create-final-invoice/?id=' . $rec->po_invoice_id) }}"
                                                                target="_blank"><i class="icon-print"></i> Create Final
                                                                Invoice</a>
                                                        </div>
                                                    @endif

                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{ $records->links() }}
                        @else
                            <div class="no_records_found">
                                No records found yet.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
