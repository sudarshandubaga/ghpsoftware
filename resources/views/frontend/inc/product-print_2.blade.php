<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
@php
    $gross_weight_kg = !empty($record->product_gross_weight_kg) ? unserialize(html_entity_decode($record->product_gross_weight_kg)) : [];
    $gross_weight_lbs = !empty($record->product_gross_weight_lbs) ? unserialize(html_entity_decode($record->product_gross_weight_lbs)) : [];
    
    $dir = 'imgs/products/';
    $image = $image1 = $image2 = $image3 = $image4 = $image5 = 'imgs/no-image.png';
    if (!empty($record->product_image)) {
        $image = $dir . $record->product_image;
    }
    if (!empty($record->product_image1)) {
        $image1 = $dir . $record->product_image1;
    }
    if (!empty($record->product_image2)) {
        $image2 = $dir . $record->product_image2;
    }
    if (!empty($record->product_image3)) {
        $image3 = $dir . $record->product_image3;
    }
    if (!empty($record->product_image4)) {
        $image4 = $dir . $record->product_image4;
    }
    if (!empty($record->product_image5)) {
        $image5 = $dir . $record->product_image5;
    }
    
@endphp
<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title>{{ $record->product_name }}</title>
    <style>
        body {
            background: #ccc;
            font-family: Arial;
        }

        * {
            box-sizing: border-box;
            font-size: 11px;
        }

        .print_block {
            width: 210mm;
            margin: 0 auto;
            background: #fff;
            padding: 10px;
            page-break-after: always;
        }

        .print_btn {
            position: fixed;
            bottom: 50px;
            right: 50px;
            border-radius: 50%;
            width: 50px;
            height: 50px;
            line-height: 50px;
            text-align: center;
            font-size: 16px;
            border: 1px solid green;
            cursor: pointer;
            background: green;
            color: #fff;
        }

        @page {
            size: a4;
            margin: 0px;
        }

        @media print {
            body {
                margin: 30px;
                background: #fff;
            }

            .print_block {
                width: calc(100% - 30px);
                margin: 0;
            }

            .print_btn {
                display: none;
            }
        }

        .table {
            width: 100%;
            border-collapse: collapse;
        }

        .table th,
        .table td {
            border: 1px solid #000;
            /*padding: 5px;*/
            text-align: left;
        }

        .main-image {
            width: 60mm;
            height: 60mm;
            object-fit: contain;
        }

        .heading {
            font-weight: bold;
            font-size: 24px;
            text-transform: uppercase;
        }

        .heading1 {
            font-weight: bold;
            font-size: 17px;
            text-transform: uppercase;
            background: #000;
            padding: 5px;
            text-align: left;
            border: 1px solid #000;
            color: #fff;
        }

        .row:after {
            content: "";
            clear: both;
            display: block;
        }

        .col-2 {
            width: 20%;
            float: left;
        }

        .thumbnail-image img {
            width: calc(100% - 4px);
            height: 120px;
            object-fit: contain;
            border: 1px solid #ccc;
            margin: 0 2px;
        }

        .text-center,
        .text-center * {
            text-align: center !important;
        }

        img {
            max-width: 100%;
        }
    </style>
</head>

<body>

    <div class="print_block">
        <table class="table">
            <tr>
                <td colspan="2"><img src="{{ url('imgs/logo1.png') }}" alt="" class="logo"></td>
                <td colspan="6" class="text-center" style="vertical-align: middle;">
                    <div class="heading">
                        Global Home Products Pte. Ltd.
                    </div>
                    <p>20 CECIL STREET # 05 - 03 PLUS, SINGAPORE 049705</p>
                </td>
            </tr>
            <tr>
                <td colspan="3" rowspan="11" style="text-align: center;vertical-align: middle;">
                    <img src="{{ url($image) }}" alt="{{ $record->product_name }}" class="main-image"
                        style="width:290px;height:290px;vertical-align: middle;">
                </td>
                <td colspan="5" style="background: #000;">
                    <div class="heading1">{{ $record->product_name }}</div>
                </td>
            </tr>
            <tr>
                <th colspan="2">Product Code</th>
                <td colspan="3">{{ $record->product_code }}</td>
            </tr>
            <!--<tr>-->
            <!--    <th colspan="2">Name</th>-->
            <!--    <td colspan="3"><strong>{{ $record->product_name }}</strong></td>-->
            <!--</tr>-->

            <tr>
                <th colspan="2">Product Dimension <br>(In CMS)</th>
                <td colspan="3"><strong>L</strong> {{ $record->product_l_cm }} X <strong>W</strong>
                    {{ $record->product_w_cm }} X <strong>H</strong> {{ $record->product_h_cm }}</td>
            </tr>

            <tr>
                <th colspan="2">Product Dimension <br>(In INCH)</th>
                <td colspan="3"><strong>L</strong> {{ $record->product_l_inch }} X <strong>W</strong>
                    {{ $record->product_w_inch }} X <strong>H</strong> {{ $record->product_h_inch }}</td>
            </tr>


            <tr>
                <th colspan="2">Carton Dimension <br>(In CMS)</th>
                <td colspan="3"><strong>L</strong> {{ $record->product_box_l_cm }} X <strong>W</strong>
                    {{ $record->product_box_w_cm }} X <strong>H</strong> {{ $record->product_box_h_cm }}</td>
            </tr>

            <tr>
                <th colspan="2">Carton Dimension <br>(In INCH)</th>
                <td colspan="3"><strong>L</strong> {{ $record->product_box_l_inch }} X <strong>W</strong>
                    {{ $record->product_box_w_inch }} X <strong>H</strong> {{ $record->product_box_h_inch }}</td>
            </tr>

            <tr>
                <th colspan="2">Assembly Status</th>
                <td colspan="3">{{ $record->product_type }}</td>
            </tr>
            <tr>
                <th colspan="2">CBM / CFT</th>
                <td colspan="3">{{ number_format($record->product_cbm, 3) }} /
                    {{ number_format($record->product_cft, 3) }}</td>
            </tr>
            <tr>
                <th colspan="2">Inner Packet (IP/MP)</th>
                <td colspan="3">{{ $record->product_ip }}</td>
            </tr>

            <tr>
                <th colspan="2">Master Packet (IP)</th>
                <td colspan="3">{{ $record->product_mp }}</td>
            </tr>
            <tr>
                <th colspan="2">Offer Price</th>
                <td colspan="3"><strong>$</strong>{{ $record->pivot->price }}</td>
            </tr>
            <!-- <tr>
                        <th colspan="2">Other Specification</th>
                        <td colspan="3">{{ $record->product_dim_extra }}</td>
                    </tr>  -->
            <!--  <tr>
                        <th colspan="2">Reference</th>
                        <td colspan="3">{{ $record->product_text }}</td>
                    </tr> -->
            <tr>

                <td colspan="2" style="width:25%;"><img src="{{ url($image1) }}" alt=""
                        style="width:100%;height:200px;object-fit:contain"></td>
                <td colspan="2" style="width:25%;"><img src="{{ url($image2) }}" alt=""
                        style="width:100%;height:200px;object-fit:contain"></td>
                <td colspan="2" style="width:25%;"><img src="{{ url($image3) }}" alt=""
                        style="width:100%;height:200px;object-fit:contain"></td>
                <td colspan="2" style="width:25%;"><img src="{{ url($image4) }}" alt=""
                        style="width:100%;height:200px;object-fit:contain"></td>
            </tr>
            @php
                $MatType1 = \DB::table('material_type')
                    ->where('material_type_id', $record->materialtype1)
                    ->first();
                $MatType2 = \DB::table('material_type')
                    ->where('material_type_id', $record->materialtype2)
                    ->first();
                $MatType3 = \DB::table('material_type')
                    ->where('material_type_id', $record->materialtype3)
                    ->first();
                $Mtrl1 = \DB::table('material')
                    ->where('material_id', $record->material1)
                    ->first();
                $Mtrl2 = \DB::table('material')
                    ->where('material_id', $record->material2)
                    ->first();
                $Mtrl3 = \DB::table('material')
                    ->where('material_id', $record->material3)
                    ->first();
                $Fnsh1 = \DB::table('finish')
                    ->where('finish_id', $record->finish1)
                    ->first();
                $Fnsh2 = \DB::table('finish')
                    ->where('finish_id', $record->finish2)
                    ->first();
                $Fnsh3 = \DB::table('finish')
                    ->where('finish_id', $record->finish3)
                    ->first();
            @endphp
            <tr>
                <th class="heading1" colspan="4">Weight Information</th>
                <th class="heading1" colspan="4">MATERIAL / FINISH </th>
            </tr>
            <tr>
                <th colspan="4">Net Weight</th>
                <th></th>
                <th>Primary</th>
                <th>Secondary</th>
                <th>Other</th>
            </tr>
            <tr>
                <th class="text-center">Wood</th>
                <th class="text-center">Iron</th>
                <th class="text-center">Other</th>
                <th class="text-center">Total</th>
                <th>Material</th>
                <td>{{ @$Mtrl1->material_name }}</td>
                <td>{{ @$Mtrl2->material_name }}</td>
                <td>{{ @$Mtrl3->material_name }}</td>


            </tr>
            <tr>
                <td class="text-center">{{ $record->product_net_weight_wooden_kg }} <strong>(KG)</strong></td>
                <td class="text-center">{{ $record->product_net_weight_iron_kg }} <strong>(KG)</strong></td>
                <td class="text-center">{{ $record->product_net_weight_other_kg }} <strong>(KG)</strong></td>
                <td class="text-center">{{ $record->product_net_weight_kg }} <strong>(KG)</strong></td>
                <th>Material Type</th>
                <td>{{ @$MatType1->material_type_name }}</td>
                <td>{{ @$MatType2->material_type_name }}</td>
                <td>{{ @$MatType3->material_type_name }}</td>

            </tr>

            <tr>
                <td class="text-center">{{ $record->product_net_weight_wooden_lbs }}<strong>(LBS)</strong></td>
                <td class="text-center">{{ $record->product_net_weight_iron_lbs }} <strong>(LBS)</strong></td>
                <td class="text-center">{{ $record->product_net_weight_other_lbs }} <strong>(LBS)</strong></td>
                <td class="text-center">{{ $record->product_net_weight_lbs }} <strong>(LBS)</strong></td>
                <th>Finish</th>
                <td>{{ @$Fnsh1->finish_title }}</td>
                <td>{{ @$Fnsh2->finish_title }}</td>
                <td>{{ @$Fnsh3->finish_title }}</td>

            </tr>
            <tr>
                <th class="heading1" colspan="4" class="bg-gray">Gross Weight </th>
                <th class="heading1" colspan="4" class="bg-gray">Loadability Details </th>

            </tr>
            <tr>
                <th colspan="2" class="text-center">Weight</th>
                <th colspan="2" class="text-center">Overall</th>
                <td colspan="2" class="text-center"><strong> 20' Cont</strong></td>
                <td colspan="2" class="text-center"><strong> {{ $record->product_load_20cont }}</strong></td>

            </tr>
            <tr>
                <td colspan="2" class="text-center">{{ implode(' + ', $gross_weight_kg) }} <strong>(KG)</strong>
                </td>
                <td colspan="2" class="text-center">{{ $record->product_tot_gross_weight_kg }}
                    <strong>(KG)</strong>
                </td>
                <td colspan="2" class="text-center"><strong>40 Std Cont</strong></td>
                <td colspan="2" class="text-center"><strong> {{ $record->product_load_40std_cont }}</strong></td>
            </tr>
            <tr>
                <td colspan="2" class="text-center">{{ implode(' + ', $gross_weight_lbs) }}<strong>(LBS)</strong>
                </td>
                <td colspan="2" class="text-center">
                    {{ $record->product_tot_gross_weight_lbs }}<strong>(LBS)</strong></td>
                <td colspan="2" class="text-center"><strong> 40 HC Cont</strong></td>
                <td colspan="2" class="text-center"><strong> {{ $record->product_load_40hc_cont }}</strong>
                </td>

            </tr>
            <tr>
                <th class="heading1" colspan="8">Other information </th>
            </tr>
            <tr>
                <th colspan="2" class="text-center">Other specification</th>
                <td colspan="6" class="text-center">{{ $record->product_dim_extra }}</td>
            </tr>
            <tr>
                <th colspan="2" class="text-center">Production Lead Time</th>
                <td colspan="6" class="text-center" style="text-transform: uppercase;">{{ $offer->lead_time }}
                </td>
            </tr>
            <tr>
                <th colspan="2" class="text-center">MOQ</th>
                <td colspan="6" class="text-center">{{ $record->pivot->moq }}</td>
            </tr>
            <tr>
                <th colspan="2" class="text-center">Price Validity</th>
                <td colspan="6" class="text-center" style="text-transform: uppercase;">
                    {{ $offer->price_validity }}</td>
            </tr>
            <tr>
                <th colspan="2" class="text-center">Inco Term</th>
                <td colspan="6" class="text-center" style="text-transform: uppercase;">{{ $offer->terms }}</td>
            </tr>


        </table>
    </div>
    <div class="print_btn" onclick="window.print()">
        Print
    </div>
</body>

</html>
