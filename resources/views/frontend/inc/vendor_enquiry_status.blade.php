<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Vendor Enquiries Status</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">View Vendor Enquiries Status</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<form>
<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter </h3>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label>Quote ID</label>
                <input type="text" name="SearchByQuote" value="{{ @$_GET['SearchByQuote'] }}" class="form-control">
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label>&nbsp;</label>
                <button type="submit" class="btn btn-primary form-control">Filter</button>
            </div>
        </div>
    </div>
</div>
</form>

<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">List Vendor Enquiry</div>
                        <div class="ml-auto">
                           <!--  <a href="" class="text-white" title="Remove" data-toggle="tooltip"> <i class="icon-trash-o"></i> </a> -->
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i class="icon-refresh"></i> </a>
                        </div>
                    </h3>
                    <div class="basic-info-two">

                        @if(!$records->isEmpty())
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <!-- <th style="width: 50px;">
                                        <label class="animated-checkbox">
                                            <input type="checkbox" class="checkall">
                                            <span class="label-text"></span>
                                        </label>
                                    </th> -->
                                    <th style="width: 80px;">Sr. No.</th>
                                    <th>Quotation ID</th>
                                    <th>Date</th>
                                    <th>Vendors</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php $sn = 0; @endphp
                                    @foreach($records as $key => $each) 
                                    @php $sn++; 

                                    $vendors = DB::table('vendor_enquiries as ve' )
                                        ->join('vendor_enqproducts AS vep', 'vep.vpro_enqid', 've.venq_id')
                                        ->join('users AS u', 've.venq_uid', 'u.user_id')
                                        ->where('ve.venq_is_deleted', 'N')
                                        ->where('ve.venq_enqid', $each->venq_enqid)
                                        ->select('u.user_name')
                                        ->groupBy('ve.venq_uid')
                                        ->get();

                                    @endphp
                                    <tr>
                                        <td>{{ $sn }}</td>
                                        <td>{{ sprintf("%s%06d", $site->setting_quote_prefix, $each->venq_enqid) }}</td>
                                        <td>{{date("d/m/Y h:i A", strtotime($each->venq_created_on)) }}</td>
                                        <td>
                                            @foreach($vendors as $key2 => $rec)
                                            <div>{{ $rec->user_name}}</div>
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="mb-1">
                                                <a href="{{ url('vquotestatus/'.$each->venq_enqid) }}" title="View Details" data-toggle="tooltip"><i class="icon-eye"></i> View Details</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                         </div>
                        {{ $records->links() }}
                        @else
                        <div class="no_records_found">
                          No records found yet.
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
