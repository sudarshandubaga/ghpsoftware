<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>View Enquiries</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">View Enquiries</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<form>
<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter </h3>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label>Customer Name</label>
                <input type="text" name="SearchByName" value="{{ @$_GET['SearchByName'] }}" class="form-control">
            </div>
        </div>
        
        <div class="col">
            <div class="form-group">
                <label>&nbsp;</label>
                <button type="submit" class="btn btn-primary form-control">Filter</button>
            </div>
        </div>
    </div>
</div>
</form>

<div class="container-fluid">
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">List Enquiries</div>
                        <div class="ml-auto">
                            <a href="{{ url('quote/new-add') }}" class="text-white"><i class="icon-plus"></i></a> &nbsp;
                            <a href="" class="text-white" title="Remove" data-toggle="tooltip"> <i class="icon-trash-o"></i> </a>
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i class="icon-refresh"></i> </a>
                        </div>
                    </h3>
                    <div class="basic-info-two">
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                            {!! \Session::get('success') !!}
                        </div>
                        @endif
                        @if(!$records->isEmpty())
                        <div class="table-responsive" style="height: 600px; border: #000 solid 0px;">
                            <table class="table table-bordered table-hover table-header-fix">
                                <thead>
                                <tr>
                                    <th style="width: 50px;">
                                        <label class="animated-checkbox">
                                            <input type="checkbox" class="checkall">
                                            <span class="label-text"></span>
                                        </label>
                                    </th>
                                    <th style="width: 80px;">Sr. No.</th>
                                    <th>Quote ID</th>
                                    <th>Customer Details</th> 
                                    <th>Date</th>
                                    <th>More</th>
                                    <th>Action</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $sn = $records->firstItem(); @endphp
                                @foreach($records as $rec)
                                    <tr @if($rec->quote_is_created == 0) class="alert alert-danger" @else class="alert alert-success" @endif>
                                        <td>
                                            <label class="animated-checkbox">
                                                <input type="checkbox" name="check[]" value="{{ $rec->order_id  }}" class="check">
                                                <span class="label-text"></span>
                                            </label>
                                        </td>
                                        <td>{{ $sn++ }}.</td>
                                        <td> {{ sprintf("GHP_QT_%06d",$rec->order_id) }} </td>
                                        <td> 
                                                <b>Customer Name</b> : {{ $rec->user_name }}<br>
                                                <b>Customer Phn </b> : {{ $rec->user_mobile }}<br>
                                                <b>Customer Email</b> : {{ $rec->user_email }}
                                        </td> 
                                        <td>{{ 'DT.'.date("d-M-Y h:i A", strtotime($rec->order_created_on)) }}</td>
                                        <td class="text-center">
                                            <a href="#oproduct_{{ $rec->order_id }}" rel="product_toggle"><i class="icon-plus-circle text-success" style="font-size: 24px;"></i></a>
                                        </td>
                                        <td>
                                            @if($rec->quote_is_created == 0)
                                                <a href="{{ url('quote/add/'.$rec->order_id) }}" class="btn btn-sm btn-info">Create Quote</a>
                                            @else
                                                <span class="text-success">Quotation Created</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($rec->quote_is_created == 0)
                                                Open
                                            @else
                                                Close
                                            @endif
                                        </td>
                                    </tr>
                                    @php
                                        $products = DB::connection('mysql2')
                                            ->table('products AS p')
                                            ->join('order_products AS op', 'p.product_id', 'op.opro_pid')
                                            ->where('opro_oid', $rec->order_id)
                                            ->get();
                                    @endphp
                                    <tr id="oproduct_{{ $rec->order_id }}" style="display: none;">
                                        <td colspan="9">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Image</th>
                                                        <th>Name</th>
                                                        <th>CBM</th>
                                                        <th>Qty</th>
                                                        <th>CBM Subtotal</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                    $totCBM = $totQty = 0;
                                                    @endphp
                                                    @foreach($products as $p)
                                                    <tr>
                                                        <td width="120">
                                                            <img src="{{ $p->product_image }}" alt="{{ $p->product_name }}" style="width: 100px; height: 100px; object-fit: contain;">
                                                        </td>
                                                        <td>{{ $p->product_name }}</td>
                                                        <td>{{ $p->product_cbm }}</td>
                                                        <td>{{ $p->opro_qty }}</td>
                                                        <td>{{ $cbm = $p->product_cbm * $p->opro_qty }}</td>
                                                    </tr>
                                                        @php
                                                        $totCBM += $cbm;
                                                        $totQty += $p->opro_qty;
                                                        @endphp
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="3">TOTAL</td>
                                                        <td>{{ $totQty }}</td>
                                                        <td>{{ $totCBM }}</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                         </div>
                        {{ $records->appends(request()->query())->links() }}
                        @else
                        <div class="no_records_found">
                          No records found yet.
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- <div class="col-sm-4">
                <div class="card warning">
                    <h3 class="card-title">Instructions</h3>
                    <div>
                        <ul>
                            <li><i class="icon-check-square"></i> Admin can view users details</li>
                            <li><i class="icon-check-square"></i> Admin can go to add user page</li>
                            <li><i class="icon-check-square"></i> Admin can go to edit user's information</li>
                        </ul>
                    </div>
                </div>
            </div> -->
        </div>
    </form>
</div>
