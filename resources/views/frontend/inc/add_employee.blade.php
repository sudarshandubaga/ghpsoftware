<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Add Employee</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="{{ url('user/buyer') }}">Employee</a></li>
                    <li class="active">Add Employee</li>
                </ul>
            </div>
            <div class="float-right">
                <div>
                    <a href="{{ url('user/employee') }}" title="View Employee" class="btn btn-default" data-toggle="tooltip"> <i class="icon-eye"></i> View Employee</a>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <form method="post">
        @csrf
        
        <div class="row">
            <div class="col-3">
                <div class="card">
                    <h3 class="card-title">Profile Image</h3>
                    <div class="form-group">
                        <label class="upload_image mb-3">
                            <img src="{{ url('imgs/user_default.png') }}" class="img-fluid">
                            <input type="file" name="user_image" id="profileImage">
                        </label>
                        <label class="btn btn-block btn-primary" for="profileImage">Edit Picture</label>
                    </div>
                </div>
            </div>
            <div class="col-9">
            	<div class="card">
		        	<h3 class="card-title">
		        		<div class="mr-auto">Login Details</div>
		                <div class="ml-auto">
		                    <a href="#save-data" class="text-white" title="Save" data-toggle="tooltip"> <i class="icon-save"></i> Save</a>
		                    &nbsp;
		                    <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i class="icon-refresh"></i> </a>
		                </div>
		        	</h3>
		        	<div class="row">
		        		<div class="col">
		        			<div class="form-group">
		                        <label>Username</label>
		            			<input type="text" name="user[user_login]" placeholder="Username" class="form-control" required autocomplete="off">
		                    </div>
		        		</div>
		                <div class="col">
		                    <div class="form-group">
		                        <label>Password</label>
		                        <input type="password" name="user[user_password]" placeholder="Password" class="form-control" required autocomplete="new-password">
		                    </div>
		                </div>
		        	</div>
		        </div>
                <div class="card mb-3">
                    <h3 class="card-title">Profile Information</h3>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" name="user[user_fname]" class="form-control">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="user[user_lname]" class="form-control">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Employee Code</label>
                                <input type="text" name="employee[employee_code]" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Designation / Job Title</label>
                                <input type="text" name="employee[employee_designation]" class="form-control">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Date Of Birth</label>
                                <input type="text" name="user[user_dob]" class="form-control datepicker" readonly>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Level</label>
                                <div>
                                    <label><input name="employee[employee_level]" type="radio" name="level"> Executive </label>
                                    <label><input name="employee[employee_level]" type="radio" name="level"> Mid </label>
                                    <label><input name="employee[employee_level]" type="radio" name="level"> Senior </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Contact / Mobile No.</label>
                                <input type="text" name="user[user_mobile]" class="form-control">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>E-mail ID</label>
                                <input type="text" name="user[user_email]" class="form-control">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Gender</label>
                                <div>
                                    <label><input type="radio" name="user[user_gender]" value="Male"> Male </label>
                                    <label><input type="radio" name="user[user_gender]" value="Female"> Female </label>
                                    <label><input type="radio" name="user[user_gender]" value="Other"> Other </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Marital Status</label>
                                <select name="employee[employee_marital_status]" class="form-control">
                                    <option value="">Marital Status</option>
                                    <option value="Single">Single</option>
                                    <option value="Married">Married</option>
                                    <option value="Divorced">Divorced</option>
                                    <option value="Widowed">Widowed</option>
                                    <option value="In relationship">In relationship</option>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>ID-Proof</label>
                                <select name="employee[employee_id_proof]" class="form-control">
                                    <option value="">ID-Proof</option>
                                    <option value="DL">Driving Licence (DL)</option>
                                    <option value="UID">UID</option>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <div >
                                    <input name="employee_id_file" type="file" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" name="employee[employee_address]" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>State</label>
                                <input type="text" name="employee[employee_state]" class="form-control">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>City</label>
                                <input type="text" name="employee[employee_city]" class="form-control">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Pincode</label>
                                <input type="text" name="employee[employee_pincode]" class="form-control">
                            </div>
                        </div>
                    </div>
                    <p></p>
                    <h3 class="card-title">Salary</h3>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Basic</label>
                                    <input type="text" name="employee[employee_basic_salary]" class="form-control">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Allowance</label>
                                    <input type="text" name="employee[employee_allowance]" class="form-control">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>ESI</label>
                                    <input type="text" name="employee[employee_esi]" class="form-control">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>PF</label>
                                    <input type="text" name="employee[employee_pf]" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Gross Salary</label>
                                    <input type="text" name="employee[employee_gross_salary]" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-3">
            <h3 class="card-title">Bank Details</h3>
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Bank Name</label>
                                <input type="text" name="employee[employee_bank_name]" class="form-control">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Bank Branch</label>
                                <input type="text" name="employee[employee_bank_branch]" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Bank A/C No.</label>
                                <input type="text" name="employee[employee_bank_account]" class="form-control">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Bank IFSC Code</label>
                                <input type="text" name="employee[employee_bank_ifsc]" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Bank Address</label>
                        <input type="text" name="employee[employee_bank_address]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Bank Holder Name</label>
                        <input type="text" name="employee[employee_bank_holder]" class="form-control">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>Remarks</label>
                        <textarea name="employee[employee_bank_remarks]" class="form-control" style="height: 295px;"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>