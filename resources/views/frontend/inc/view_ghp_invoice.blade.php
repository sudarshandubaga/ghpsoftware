<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>View GHP Invoices</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">View GHP Invoice</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<form>
<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter </h3>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label>Invoice No</label>
                <input type="text" name="SearchByInvoice" value="{{ @$_GET['SearchByInvoice'] }}" class="form-control">
            </div>
        </div>
        
        <div class="col">
            <div class="form-group">
                <label>PI</label>
                <input type="text" name="SearchByPI" value="{{ @$_GET['SearchByPI'] }}" class="form-control">
            </div>
        </div>
        
        <div class="col">
            <div class="form-group">
                <label>Customer</label>
                <input type="text" name="SearchByCustomer" value="{{ @$_GET['SearchByCustomer'] }}" class="form-control">
            </div>
        </div>
        
        <div class="col">
            <div class="form-group">
                <label>&nbsp;</label>
                <button type="submit" class="btn btn-primary form-control">Filter</button>
            </div>
        </div>
    </div>
</div>
</form>

<div class="container-fluid">   
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <h3 class="card-title clearfix">
                        <div class="mr-auto">List GHP Invoices</div>
                        <div class="ml-auto">
                            <a href="" class="text-white" title="Remove" data-toggle="tooltip"> <i class="icon-trash-o"></i> </a>
                            &nbsp;
                            <a href="#refresh" class="text-white" title="Reload" data-toggle="tooltip"> <i class="icon-refresh"></i> </a>
                        </div>
                    </h3>
                    <div class="basic-info-two">

                        @if(!$records->isEmpty())
                        <div class="table-responsive" style="height: 600px; border: #000 solid 0px;">
                            <table class="table table-bordered table-hover table-header-fix">
                                <thead>
                                <tr>
                                    <th style="width: 50px;">
                                        <label class="animated-checkbox">
                                            <input type="checkbox" class="checkall">
                                            <span class="label-text"></span>
                                        </label>
                                    </th>
                                    <th style="width: 60px;">Sr. No.</th>
                                    <th style="width: 120px;">Invoice No</th>
                                    <th>Quote ID</th>
                                    <th>Order Ref./ PI No.</th>
                                    <th>Customer Info</th>
                                    <th>Total Amount</th>
                                    <th>Total CBM</th>
                                    <th>Date</th>
                                    <th>Created At</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $sn = $records->firstItem(); @endphp
                                @foreach($records as $rec)
                                    <tr>
                                        <td>
                                            <label class="animated-checkbox">
                                                <input type="checkbox" name="check[]" value="{{ $rec->user_id  }}" class="check">
                                                <span class="label-text"></span>
                                            </label>
                                        </td>
                                        <td>{{ $sn++ }}.</td> 
                                        <td>{{ $rec->exs_ghp_invoice_invoice_no }}</td>
                                        <td>{{ sprintf("%s%04d", $site->setting_quote_prefix, $rec->quote_number) }}</td>
                                        <td>
                                            <div class="row mb-1">
                                                <div class="col-4">
                                                    <strong>Orfer Ref. :</strong>
                                                </div>
                                                <div class="col-8">
                                                    @if($rec->pi_order_ref != "")
                                                        {{ $rec->pi_order_ref }}
                                                    @endif

                                                    @if($rec->pi_order_ref == "")
                                                        <!--{{ sprintf("%s-%03d", 'GHP-201819', $rec->pi_id+100) }}-->
                                                         {{ sprintf("%s-%03d", 'GHP-202122', $rec->pi_id+100) }}
                                                    @endif
                                                </div>
                                            </div>
                                            
                                            <div class="row mb-1">
                                                <div class="col-4">
                                                    <strong>PI No. :</strong>
                                                </div>
                                                <div class="col-8">
                                                    <!--{{ sprintf("%s-%03d", 'GHP-201819', $rec->pi_id) }}-->
                                                    {{ sprintf("%s-%03d", 'GHP-202122', $rec->pi_id+100) }}
                                                </div>
                                            </div>       
                                        </td>
                                        <td>
                                            <div class="row mb-1">
                                                <div class="col-5">
                                                    <strong>Name:</strong>
                                                </div>
                                                <div class="col-7">
                                                    {{ $rec->user_name }}
                                                </div>
                                            </div>
                                            <div class="row mb-1">
                                                <div class="col-5">
                                                    <strong>Mobile:</strong>
                                                </div>
                                                <div class="col-7">
                                                    {{ $rec->user_mobile }}
                                                </div>
                                            </div>
                                            <div class="row mb-1">
                                                <div class="col-5">
                                                    <strong>Email:</strong>
                                                </div>
                                                <div class="col-7">
                                                    {{ $rec->user_email }}
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{ $rec->currency_sign }} {{ number_format($rec->quote_total,2) }}</td>
                                        <td>{{ number_format( $rec->quote_total_cbm , 3) }}</td>
                                        <td>{{ date("d/m/Y", strtotime($rec->exs_ghp_invoice_invoce_date)) }}</td>
                                        <td>{{ date("d/m/Y h:i ", strtotime($rec->created_at)) }}</td>
                                        <td>
                                            <div class="mb-1">
                                                <a href="{{ url('ghp-commercial-invoice/print/'.$rec->exs_ghp_invoice_id) }}" title="Print PI" data-toggle="tooltip" target="_blank"><i class="icon-print"></i> Print</a>
                                            </div>
                                            <div class="mb-1">
                                                <a href="{{ url('packing-list/print/'.$rec->exs_ghp_invoice_id) }}" title="Print PI" data-toggle="tooltip" target="_blank"><i class="icon-print"></i>Packing Slip</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                         </div>
                        {{ $records->appends(request()->query())->links() }}
                        @else
                        <div class="no_records_found">
                          No records found yet.
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
