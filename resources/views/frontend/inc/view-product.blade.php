<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Product</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Product</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ url('product/add') }}" class="btn btn-default"> <i class="icon-plus"></i> Add Product</a>
            </div>
        </div>
    </div>
</section>

<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter Product</h3>
    <div class="row">
        <div class="col-10">
            <form>
                <div class="row">
                    <div class="col">
                        <div class="row">
                            <div class="col-sm-3"><label>Product Name</label></div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="text" name="SearchByName" value="{{ @$_GET['SearchByName'] }}"
                                        class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col">

                        <div class="row">
                            <div class="col-sm-3"><label>Product Code</label></div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="text" name="SearchByCode" value="{{ @$_GET['SearchByCode'] }}"
                                        class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="row">
                            <div class="col-sm-3"><label>Buyer Refrence</label></div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="text" name="SearchByText" value="{{ @$_GET['SearchByText'] }}"
                                        class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary form-control">Filter</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-2">
            <div class="form-group">
                <div class="col">

                    <form action="{{ route('exportProduct') }}" method="post">
                        {{ csrf_field() }}
                        <div class="">
                            <input type="hidden" name="SearchByName" value="{{ request('SearchByName') }}">
                            <input type="hidden" name="SearchByCode" value="{{ request('SearchByCode') }}">
                            <input type="hidden" name="SearchByText" value="{{ request('SearchByText') }}">
                            <!-- <label>&nbsp;</label> -->
                            <input type="submit" class="btn btn-primary form-control" value="Export">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--<form method="post">-->
@csrf

<div class="container-fluid">
    <div class="card mb-5 mt-5">
        <h3 class="card-title">
            <div class="mr-auto">View Product</div>
            <a href="" class="ml-auto text-white" title="Remove Selected" data-toggle="tooltip">
                <i class="icon-trash-o"></i>
            </a>
        </h3>
        <section class="add-block">
            <div>
                <div class="basic-info-two">
                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            {!! \Session::get('success') !!}</li>
                        </div>
                    @endif
                    @if (!$records->isEmpty())
                        {{ $records->appends($_GET)->links() }}
                        <table class="table table-bordered table-hover table-header-fix">
                            <thead>
                                <tr>
                                    <!--         <th style="width: 50px;">-->
                                    <!--    <label class="animated-checkbox">-->
                                    <!--        <input type="checkbox" class="checkall">-->
                                    <!--        <span class="label-text"></span>-->
                                    <!--    </label>-->
                                    <!--</th>-->
                                    <th></th>
                                    <th style="width: 80px;">Sr. No.</th>
                                    <th>Image</th>
                                    <th>Product Details</th>
                                    <th>Dimension</th>
                                    <!-- <th>Others</th> -->
                                    <th>Extra Details</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $sn = $records->firstItem(); @endphp
                                @foreach ($records as $rec)
                                    @php
                                        $MatType1 = \DB::table('material_type')
                                            ->where('material_type_id', $rec->materialtype1)
                                            ->first();
                                        $MatType2 = \DB::table('material_type')
                                            ->where('material_type_id', $rec->materialtype2)
                                            ->first();
                                        $Mtrl1 = \DB::table('material')
                                            ->where('material_id', $rec->material1)
                                            ->first();
                                        $Mtrl2 = \DB::table('material')
                                            ->where('material_id', $rec->material2)
                                            ->first();
                                        $Fnsh1 = \DB::table('finish')
                                            ->where('finish_id', $rec->finish1)
                                            ->first();
                                        $Fnsh2 = \DB::table('finish')
                                            ->where('finish_id', $rec->finish2)
                                            ->first();
                                        
                                        $buyers = !empty($rec->product_buyers) ? json_decode($rec->product_buyers) : [];
                                    @endphp
                                    <tr>
                                        <td>
                                            <label class="animated-checkbox">
                                                <input type="checkbox" name="check[]" value="{{ $rec->product_id }}"
                                                    class="check">
                                                <span class="label-text"></span>
                                            </label>
                                        </td>
                                        <td>{{ $sn++ }}.</td>
                                        <td style="width: 100px;"><img
                                                src="{{ url('imgs/products/' . $rec->product_image) }}" alt=""
                                                style="max-width: 100px;"> </td>
                                        <td style="width: 450px;">
                                            <u>{{ $rec->product_name }}</u><br>
                                            <strong>Code:</strong> {{ $rec->product_code }}<br>
                                            <strong>Type:</strong> {{ $rec->product_type }} Version<br>
                                            <!--<strong>MRP:</strong> $ {{ $rec->product_mrp }}<br>-->
                                            <!--<strong>Wood:</strong> {{ $rec->product_material_wood }}<br>-->
                                            <!--<strong>Glass:</strong> {{ $rec->product_glass }}<br>-->
                                            <!--<strong>Other:</strong> {{ $rec->product_fabric }}<br>-->

                                            <strong>Material:</strong> {{ @$Mtrl1->material_name }} /
                                            {{ @$Mtrl2->material_name }}<br>
                                            <strong>Material Type:</strong> {{ @$MatType1->material_type_name }} /
                                            {{ @$MatType2->material_type_name }}<br>
                                            <strong>Finish :</strong> {{ @$Fnsh1->finish_title }} /
                                            {{ @$Fnsh2->finish_title }}<br>
                                            <strong>Buyer Refrence :{{ @$rec->product_text }}</strong><br>
                                            <strong>Specification:</strong> {{ $rec->product_dim_extra }}<br>
                                            <br>
                                            <strong><u>Buyers:</u></strong> <br />

                                            @if (!empty($buyers))
                                                @foreach ($buyers as $b)
                                                    @if (!empty($b->name))
                                                        <strong>{{ $b->name }}:</strong> {{ $b->price }} <br>
                                                    @endif
                                                @endforeach
                                            @endif

                                        </td>
                                        <td>
                                            <strong><b>Dimension</b></strong><br>
                                            L {{ $rec->product_l_cm }} X W {{ $rec->product_w_cm }} X H
                                            {{ $rec->product_h_cm }} (CMS)<br>
                                            L {{ $rec->product_l_inch }} X W {{ $rec->product_w_inch }} X H
                                            {{ $rec->product_h_inch }} (Inch)<br><br>
                                            <strong><b>Cartoon Dimension</b></strong><br>
                                            L {{ $rec->product_box_l_cm }} X W {{ $rec->product_box_w_cm }} X H
                                            {{ $rec->product_box_h_cm }} (CMS)<br>
                                            L {{ $rec->product_box_l_inch }} X W {{ $rec->product_box_w_inch }} X H
                                            {{ $rec->product_box_h_inch }} (Inch)
                                        </td>
                                        <td><strong>CBM</strong><br>{{ number_format($rec->product_cbm, 3) }}<br>
                                            <strong>CFT</strong><br>{{ number_format($rec->product_cft, 3) }}<br>
                                            <strong>N. Wet</strong><br>{{ $rec->product_net_weight_kg }}<br>
                                            <strong>G. Wet</strong><br>{{ $rec->product_tot_gross_weight_kg }}
                                        </td>
                                        <td>
                                            <div class="mb-3">
                                                <a href="{{ url('product/add/' . $rec->product_id) }}"><i
                                                        class="icon-pencil"></i> Edit</a>
                                            </div>
                                            <div class="mb-3">
                                                <a href="{{ url('product/single/' . $rec->product_code) }}"><i
                                                        class="icon-library_books"></i> Details</a>
                                            </div>
                                            <div class="mb-3">
                                                <a href="{{ url('product/print/' . $rec->product_code) }}"><i
                                                        class="icon-print"></i> Print</a>
                                            </div>
                                            <div class="mb-3">
                                                <a href="{{ url('product/delete/' . $rec->product_id) }}"
                                                    onClick="return CheckDel()"><i class="icon-trash"></i> Delete</a>
                                            </div>
                                            <!--<div class="mb-3">-->
                                            <!--    <a href="{{ url('product/Copy/' . $rec->product_id) }}" onClick="return CheckCopy()"><i class="icon-file"></i> Copy</a>-->
                                            <!--</div>-->
                                            <div>
                                                <form class="cart_form"
                                                    action="{{ url('ajax/add_to_cart/quote_cart') }}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="cart[cart_qty]" value="1">
                                                    <input type="hidden" name="cart[cart_pid]" required
                                                        value="{{ $rec->product_id }}">
                                                    <div class="replace_status">

                                                        <button
                                                            data-url="{{ url('product/status/' . $rec->product_id) }}"
                                                            type="button"
                                                            class="btn btn-sm text-white btn-{{ $rec->product_is_visible == 'Y' ? 'success' : 'danger' }} mb-1 change_status_btn">
                                                            {{ $rec->product_is_visible == 'Y' ? 'Active' : 'Deactive' }}
                                                        </button>

                                                    </div>
                                                    <button><i class="icon-cart"></i> Add To Quote</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                </div>
                {{ $records->appends($_GET)->links() }}
            @else
                <div class="no_records_found">
                    No records found yet.
                </div>
                @endif
            </div>
        </section>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script>
    function CheckDel() {
        if (confirm("Are You Sure to Delete This Record")) {
            return true;
        }

        return false;
    }

    function CheckCopy() {
        if (confirm("Are You Sure to Copy This Record")) {
            return true;
        }

        return false;
    }
    $(document).on('click', '.change_status_btn', function() {
        let self = this;
        var url = $(self).attr('data-url');
        $.ajax({
            url,
            success: function(response) {
                $(self).toggleClass('btn-success btn-danger');

                if (!response.result) {
                    $(self).text('Deactive');
                } else {
                    $(self).text('Active');
                }
            },
            error: function(error) {

            }
        });
    })
</script>
