<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Add Product</h1>
                <ul class="breadcrumbs clearfix">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Add Product</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ url('product') }}" class="btn btn-default"> <i class="icon-plus"></i> View Product</a>
            </div>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show">

            {!! implode('', $errors->all('<div >:message</div>')) !!}
        </div>
    @endif
</section>

<div class="container-fluid">
    <form method="post" enctype="multipart/form-data">
        @csrf
        <div class="card">
            <h3 class="card-title" style="position: sticky; top: 0; z-index: 999;">
                <div class="mr-auto">Add Product</div>
                <a href="#save-data" class="ml-auto text-white" title="Save Data" data-toggle="tooltip">
                    <i class="icon-save"></i> Save
                </a>
            </h3>
            <section class="add-block">
                <div>
                    <div class="row">
                        <div class="col">
                            <div class="basic-info same-height">
                                <h4 class="bi">Basic Information</h4>
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Parent Product *</label>
                                                <select name="record[parent_product]" class="form-control"
                                                    {{ isset($edit->parent_product) ? 'readonly disabled' : null }}>
                                                    <option value="0">Main Product</option>
                                                    @foreach ($ParentProduct as $PPPr)
                                                        <option @if (@$edit->parent_product == $PPPr->product_id) selected @endif
                                                            value="{{ $PPPr->product_id }}"> {{ $PPPr->product_code }} -
                                                            {{ $PPPr->product_name }} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Product Name *</label>
                                                <input type="text" name="record[product_name]" class="form-control"
                                                    value="{{ @$edit->product_name }}"
                                                    {{ isset($edit->parent_product) ? '  ' : null }} required
                                                    id="product-name">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Product Range *</label>
                                                <!-- <input type="text" name="record[product_range]" class="form-control" value="{{ @$edit->product_range }}" id="product-range" > -->
                                                <select name="record[product_range]" class="form-control"
                                                    id="product-range"
                                                    {{ isset($edit->parent_product) ? 'readonly disabled' : null }}
                                                    required>
                                                    <option value="">Select Range</option>
                                                    @foreach ($ranges as $range)
                                                        <option value="{{ $range->range_code }}"
                                                            @if (@$edit->product_range == $range->range_code) selected @endif>
                                                            {{ $range->range_title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Country *</label>
                                            <select class="form-control" name="record[product_country]"
                                                id="product-country"
                                                {{ isset($edit->parent_product) ? 'readonly disabled' : null }}
                                                required>
                                                <option value="">Choose Country</option>
                                                @foreach ($countries as $c)
                                                    <option
                                                        value="{{ $c->country_short_name }}"@if (@$edit->product_country == $c->country_short_name) selected @endif>
                                                        {{ $c->country_name . ' (' . $c->country_short_name . ')' }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Category *</label>
                                                <select class="form-control category" name="web[product_category]"
                                                    id="product-category" data-target="#product-sub-category"
                                                    {{ isset($edit->parent_product) ? 'readonly disabled' : null }}
                                                    required>
                                                    <option value="">Select Category</option>
                                                    @foreach ($categories as $c)
                                                        <option data-short-code="{{ $c->category_code }}"
                                                            value="{{ $c->category_id }}"
                                                            @if (@$web_edit->product_category == $c->category_id) selected @endif>
                                                            {{ $c->category_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="hidden" name="record[product_no]" id="product-number"
                                        value="{{ $product_no }}" />

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Subcategory</label>
                                                <select class="form-control subcategory" name="web[product_subcategory]"
                                                    id="product-sub-category" data-target="#proSubCategory2"
                                                    {{ isset($edit->parent_product) ? 'readonly disabled' : null }}
                                                    required>
                                                    <option value="">Select Subcategory</option>
                                                    @foreach ($subcategories as $c)
                                                        <option data-short-code="{{ $c->category_code }}"
                                                            value="{{ $c->category_id }}"
                                                            @if (@$web_edit->product_subcategory == $c->category_id) selected @endif>
                                                            {{ $c->category_name }}</option>
                                                    @endforeach;
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Subcategory2</label>
                                                <select class="form-control" name="web[product_subcategory2]"
                                                    id="proSubCategory2"
                                                    {{ isset($edit->parent_product) ? 'readonly disabled' : null }}
                                                    required>
                                                    <option value="">Select Subcategory2</option>
                                                    @foreach ($subcategories2 as $c)
                                                        <option data-short-code="{{ $c->category_code }}"
                                                            value="{{ $c->category_id }}"
                                                            @if (@$web_edit->product_subcategory2 == $c->category_id) selected @endif>
                                                            {{ $c->category_name }}</option>
                                                    @endforeach;
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Product Code *</label>
                                        <input type="text" name="record[product_code]" class="form-control"
                                            value="{{ @$edit->product_code }}" id="product-code" readonly
                                            {{ isset($edit->parent_product) ? 'readonly disabled' : null }} required>
                                    </div>


                                    <div class="form-group">
                                        <label>Search Keywords</label>
                                        <input type="text" name="record[product_search_keywords]"
                                            value="{{ @$edit->product_search_keywords }}"
                                            class="form-control taginput" required>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Product Type * <small>(If you select K/D, upload PDF.)</small>
                                                </label>
                                                <div class="clearfix">
                                                    <label class="box-radio">
                                                        <input type="radio" name="record[product_type]"
                                                            class="product_type" value="Fixed"
                                                            @if (empty($edit->product_type) || @$edit->product_type == 'Fixed') checked @endif>
                                                        <span class="label-text">Fixed</span>
                                                    </label>
                                                    <label class="box-radio">
                                                        <input type="radio" name="record[product_type]"
                                                            class="product_type" value="K/D"
                                                            @if (!empty($edit->product_type) && @$edit->product_type == 'K/D') checked @endif>
                                                        <span class="label-text">K/D</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <p style="margin: 0;">&nbsp;</p>
                                        </div>
                                        <div class="col-sm-12 upload_pdf"
                                            @if (empty($edit->product_type) || $edit->product_type == 'Fixed') style="display: none;" @endif>
                                            <label>Upload PDF</label>
                                            <div class="upload-file">
                                                <input type="file" name="product_pdf" accept="application/pdf">
                                            </div>
                                            @if (!empty($edit->product_pdf))
                                                <a href="{{ url('files/products/' . $edit->product_pdf) }}"
                                                    target="_blank">Download PDF</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="basic-info same-height">
                                <h4 class="bi">
                                    Product Dimension
                                </h4>
                                <div class="">
                                    <div>
                                        <label>Product Dimension (In CMS) *</label>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">L</span>
                                                        </div>
                                                        <input type="text" name="record[product_l_cm]"
                                                            class="form-control text-center"
                                                            value="{{ !empty($edit->product_l_cm) ? $edit->product_l_cm : null }}"
                                                            id="proLcm" autocomplete="off" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">W</span>
                                                        </div>
                                                        <input type="text" name="record[product_w_cm]"
                                                            class="form-control text-center"
                                                            value="{{ !empty($edit->product_w_cm) ? $edit->product_w_cm : null }}"
                                                            id="proWcm" autocomplete="off" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">H</span>
                                                        </div>
                                                        <input type="text" name="record[product_h_cm]"
                                                            class="form-control text-center"
                                                            value="{{ !empty($edit->product_h_cm) ? $edit->product_h_cm : null }}"
                                                            id="proHcm" autocomplete="off" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div>
                                            <label>Product Dimension (In Inch)</label>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">L</span>
                                                            </div>
                                                            <input type="text" name="record[product_l_inch]"
                                                                class="form-control text-center"
                                                                value="{{ !empty($edit->product_l_inch) ? $edit->product_l_inch : 0 }}"
                                                                id="proLinch" autocomplete="off" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">W</span>
                                                            </div>
                                                            <input type="text" name="record[product_w_inch]"
                                                                class="form-control text-center"
                                                                value="{{ !empty($edit->product_w_inch) ? $edit->product_w_inch : 0 }}"
                                                                id="proWinch" autocomplete="off" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">H</span>
                                                            </div>
                                                            <input type="text" name="record[product_h_inch]"
                                                                class="form-control text-center"
                                                                value="{{ !empty($edit->product_h_inch) ? $edit->product_h_inch : 0 }}"
                                                                id="proHinch" autocomplete="off" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h4>Specification</h4>
                                        <div class="form-group">
                                            <label>specification*</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                </div>
                                                <input type="text" name="record[product_dim_extra]"
                                                    class="form-control" value="{{ @$edit->product_dim_extra }}"
                                                    required>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="">
                                        <div>
                                            <label>Carton Dimension (In CMS) *</label>

                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">L</span>
                                                            </div>
                                                            <input type="text" name="record[product_box_l_cm]" class="form-control text-center" value="{{ !empty($edit->product_box_l_cm) ? $edit->product_box_l_cm : 0 }}" id="boxLcm" autocomplete="off">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">W</span>
                                                            </div>
                                                            <input type="text" name="record[product_box_w_cm]" class="form-control text-center" value="{{ !empty($edit->product_box_w_cm) ? $edit->product_box_w_cm : 0 }}" id="boxWcm" autocomplete="off">
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">H</span>
                                                            </div>
                                                            <input type="text" name="record[product_box_h_cm]" class="form-control text-center" value="{{ !empty($edit->product_box_h_cm) ? $edit->product_box_h_cm : 0 }}" id="boxHcm" autocomplete="off">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="">
                                        <div>
                                            <label>Carton Dimension (In Inch)</label>

                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">L</span>
                                                            </div>
                                                            <input type="text" name="record[product_box_l_inch]" class="form-control text-center" value="{{ !empty($edit->product_box_l_inch) ? $edit->product_box_l_inch : 0 }}" id="boxLinch" autocomplete="off" readonly>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">W</span>
                                                            </div>
                                                            <input type="text" name="record[product_box_w_inch]" class="form-control text-center" value="{{ !empty($edit->product_box_w_inch) ? $edit->product_box_w_inch : 0 }}" id="boxWinch" autocomplete="off" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">H</span>
                                                            </div>
                                                            <input type="text" name="record[product_box_h_inch]" class="form-control text-center" value="{{ !empty($edit->product_box_h_inch) ? $edit->product_box_h_inch : 0 }}" id="boxHinch" autocomplete="off" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                    <div class="">
                                        <h4>Packet Info</h4>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Inner Packet (IP)</label>
                                                    <input type="number" name="record[product_ip]" id="product_ip"
                                                        value="{{ !empty($edit->product_ip) ? $edit->product_ip : 0 }}"
                                                        min="0" placeholder="Inner Packet (IP)"
                                                        class="form-control" autocomplete="off"
                                                        title="Enter the Value for Inner Packet (IP)">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Master Packet (MP)</label>
                                                    <input type="number" name="record[product_mp]" id="product_mp"
                                                        value="{{ !empty($edit->product_mp) ? $edit->product_mp : 1 }}"
                                                        min="0" placeholder="Master Packet (MP)"
                                                        class="form-control" autocomplete="off"
                                                        title="Enter the Value for Master Packet (MP)">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>CBM Ratio</label>
                                                    <input type="number" name="record[cbm_ratio]" id="cbm_ratio"
                                                        value="{{ !empty($edit->cbm_ratio) ? $edit->cbm_ratio : 1 }}"
                                                        min="1" placeholder="Ration"
                                                        class="ratio form-control " autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="basic-info same-height-two">
                                <h4>Price Information</h4>
                                <div class="form-group">
                                    <label>USD*</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input type="number" min="0" name="record[product_mrp]"
                                            class="form-control" value="{{ @$edit->product_mrp }}"
                                            title="Please Enter Numeric Price">
                                    </div>
                                </div>
                                <h4>Reference</h4>
                                <div class="form-group">
                                    <!--<label>Text Conducted</label>-->
                                    <input type="text" name="record[product_text]" class="form-control taginput"
                                        value="{{ @$edit->product_text }}" style="width: 100% !important;">
                                </div>
                                <h4>Other Details</h4>
                                <div class="form-group">
                                    <label>Upload PDF</label>
                                    <div class="upload-file">
                                        <input type="file" name="product_pdf2" accept="application/pdf">
                                    </div>
                                    @if (!empty($edit->product_pdf2))
                                        <a href="{{ url('files/products/' . $edit->product_pdf2) }}"
                                            target="_blank">Download PDF</a>
                                    @endif
                                </div>

                                <h4>Buyer Price</h4>

                                @for ($i = 0; $i < 5; $i++)
                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <label>Buyer Name {{ $i + 1 }}</label>
                                            <input name="buyer[{{ $i }}][name]" type="text"
                                                class="form-control" value="{{ @$edit->buyer_details[$i]->name }}">
                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <label>Buyer Price {{ $i + 1 }}</label>
                                            <input name="buyer[{{ $i }}][price]" type="text"
                                                class="form-control" value="{{ @$edit->buyer_details[$i]->price }}">
                                        </div>
                                    </div>
                                @endfor

                            </div>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="row">
                        <div class="col">
                            <div class="basic-info same-height">
                                <h4 class="bi">
                                    Weight Information
                                </h4>
                                <h6>Net Weight</h6>
                                <div>
                                    <label>Net Weight (Wooden)</label>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="record[product_net_weight_wooden_kg]"
                                                        class="form-control text-center"
                                                        value="{{ !empty($edit->product_net_weight_wooden_kg) ? $edit->product_net_weight_wooden_kg : 0 }}"
                                                        id="netWgWoodKg" autocomplete="off"
                                                        title="Please Enter Net Weight (Wooden) Numeric Value">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">Kg</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="record[product_net_weight_wooden_lbs]"
                                                        class="form-control text-center"
                                                        value="{{ !empty($edit->product_net_weight_wooden_lbs) ? $edit->product_net_weight_wooden_lbs : 0 }}"
                                                        id="netWgWoodLbs" autocomplete="off" readonly>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">LBS</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <label>Net Weight (Iron)</label>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="record[product_net_weight_iron_kg]"
                                                        class="form-control text-center"
                                                        value="{{ !empty($edit->product_net_weight_iron_kg) ? $edit->product_net_weight_iron_kg : 0 }}"
                                                        id="netWgIronKg" autocomplete="off"
                                                        title="Please Enter Net Weight (Iron) Numeric Value">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">Kg</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="record[product_net_weight_iron_lbs]"
                                                        class="form-control text-center"
                                                        value="{{ !empty($edit->product_net_weight_iron_lbs) ? $edit->product_net_weight_iron_lbs : 0 }}"
                                                        id="netWgIronLbs" autocomplete="off" readonly>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">LBS</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <label>Net Weight (Other)</label>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="record[product_net_weight_other_kg]"
                                                        class="form-control text-center"
                                                        value="{{ !empty($edit->product_net_weight_other_kg) ? $edit->product_net_weight_other_kg : 0 }}"
                                                        id="netWgOtherKg" autocomplete="off"
                                                        title="Please Enter Net Weight (other) Numeric Value">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">Kg</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="record[product_net_weight_other_lbs]"
                                                        class="form-control text-center"
                                                        value="{{ !empty($edit->product_net_weight_other_lbs) ? $edit->product_net_weight_other_lbs : 0 }}"
                                                        id="netWgOtherLbs" autocomplete="off" readonly>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">LBS</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <label>Overall Net Weight (Wooden + Iron + Other)</label>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="record[product_net_weight_kg]"
                                                        class="form-control text-center"
                                                        value="{{ !empty($edit->product_net_weight_kg) ? $edit->product_net_weight_kg : 0 }}"
                                                        id="netWeightKg" autocomplete="off" readonly
                                                        title="Please Enter Overall Net Weight Numeric Value">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">Kg</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="record[product_net_weight_lbs]"
                                                        class="form-control text-center"
                                                        value="{{ !empty($edit->product_net_weight_lbs) ? $edit->product_net_weight_lbs : 0 }}"
                                                        id="netWeightLbs" autocomplete="off" readonly>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">LBS</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="basic-info same-height">
                                <h6>Gross Weight</h6>
                                <div>
                                    <div id="total_gross_weight">
                                        @if (!isset($edit->product_mp))
                                            <div>
                                                <label class="text-center d-block"><strong>Master Packet (MP) =
                                                    </strong>1</label>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text"
                                                                    name="record[product_gross_weight_kg][]"
                                                                    class="form-control text-center gw_kg"
                                                                    value="0" autocomplete="off"
                                                                    title="Master Packet Weight (MP)">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">Kg</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text"
                                                                    name="record[product_gross_weight_lbs][]"
                                                                    class="form-control text-center gw_lbs"
                                                                    value="0" autocomplete="off" readonly>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">LBS</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        @for ($mp = 0; $mp < @$edit->product_mp; $mp++)
                                            <div>
                                                <label class="text-center d-block">{!! $mp == 0 ? '<strong>Master Packet (MP) = </strong>' . $edit->product_mp : '+' !!}</label>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text"
                                                                    name="record[product_gross_weight_kg][]"
                                                                    class="form-control text-center gw_kg"
                                                                    value="<?php echo isset($gross_weight_kg[$mp]) ? $gross_weight_kg[$mp] : 0; ?>" autocomplete="off"
                                                                    title="Master Packet Weight (MP)">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">Kg</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text"
                                                                    name="record[product_gross_weight_lbs][]"
                                                                    class="form-control text-center gw_lbs"
                                                                    value="<?php echo isset($gross_weight_lbs[$mp]) ? $gross_weight_lbs[$mp] : 0; ?>" autocomplete="off"
                                                                    readonly>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">LBS</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endfor
                                    </div>
                                    <label>Total Gross Weight</label>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="record[product_tot_gross_weight_kg]"
                                                        class="form-control text-center"
                                                        value="{{ !empty($edit->product_tot_gross_weight_kg) ? $edit->product_tot_gross_weight_kg : 0 }}"
                                                        id="totGrossWeightKg" value="0" autocomplete="off"
                                                        readonly title="Total Gross Weight">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">Kg</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="record[product_tot_gross_weight_lbs]"
                                                        class="form-control text-center"
                                                        value="{{ !empty($edit->product_tot_gross_weight_lbs) ? $edit->product_tot_gross_weight_lbs : 0 }}"
                                                        id="totGrossWeightLbs" value="0" autocomplete="off"
                                                        readonly>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">LBS</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="basic-info same-height">
                                <h4>Carton Dimension</h4>
                                <div>
                                    @if (empty($edit->product_mp))
                                        <div>
                                            <label class="float-right">
                                                <input type="checkbox" id="sameAllCDim">
                                                Same All Carton Size
                                            </label>
                                            <label class="d-block"><strong>Main Packet</strong></label>
                                            <div class="">
                                                <div class="form-group">
                                                    <label>Carton Dimension (In CMS)</label>

                                                    <div class="row cbm_d_row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">L</span>
                                                                    </div>
                                                                    <input type="text"
                                                                        name="record[product_mp_carton_l_cm][]"
                                                                        value=""
                                                                        class="form-control text-center l_cm_dim cbm_l"
                                                                        autocomplete="off" required>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">W</span>
                                                                    </div>
                                                                    <input type="text"
                                                                        name="record[product_mp_carton_w_cm][]"
                                                                        value=""
                                                                        class="form-control text-center w_cm_dim cbm_w"
                                                                        autocomplete="off" required>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">H</span>
                                                                    </div>
                                                                    <input type="text"
                                                                        name="record[product_mp_carton_h_cm][]"
                                                                        class="form-control text-center  h_cm_dim cbm_h"
                                                                        value="" autocomplete="off" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="">
                                                <div class="form-group">
                                                    <label>Carton Dimension (In Inch)</label>

                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">L</span>
                                                                    </div>
                                                                    <input type="text"
                                                                        name="record[product_mp_carton_l_inch][]"
                                                                        class="form-control text-center l_inch_dim"
                                                                        value="0" autocomplete="off">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">W</span>
                                                                    </div>
                                                                    <input type="text"
                                                                        name="record[product_mp_carton_w_inch][]"
                                                                        class="form-control text-center w_inch_dim"
                                                                        value="0" autocomplete="off">
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">H</span>
                                                                    </div>
                                                                    <input type="text"
                                                                        name="record[product_mp_carton_h_inch][]"
                                                                        class="form-control text-center h_inch_dim"
                                                                        value="0" autocomplete="off">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div id="carton_mp_dimension">
                                        @for ($mp = 0; $mp < @$edit->product_mp; $mp++)
                                            @php
                                                $product_mp_carton_l_cm = !empty($edit->product_mp_carton_l_cm) ? unserialize($edit->product_mp_carton_l_cm) : '';
                                                $product_mp_carton_w_cm = !empty($edit->product_mp_carton_w_cm) ? unserialize($edit->product_mp_carton_w_cm) : '';
                                                $product_mp_carton_h_cm = !empty($edit->product_mp_carton_h_cm) ? unserialize($edit->product_mp_carton_h_cm) : '';
                                                
                                                $product_mp_carton_l_inch = !empty($edit->product_mp_carton_l_inch) ? unserialize($edit->product_mp_carton_l_inch) : '';
                                                $product_mp_carton_w_inch = !empty($edit->product_mp_carton_w_inch) ? unserialize($edit->product_mp_carton_w_inch) : '';
                                                $product_mp_carton_h_inch = !empty($edit->product_mp_carton_h_inch) ? unserialize($edit->product_mp_carton_h_inch) : '';
                                            @endphp
                                            <div>
                                                @if ($mp == 0)
                                                    <label class="float-right">
                                                        <input type="checkbox" id="sameAllCDim">
                                                        Same All Carton Size
                                                    </label>
                                                    <label class="d-block"><strong>Main Packet</strong></label>
                                                @else
                                                    <label class="text-center d-block">+</label>
                                                @endif
                                                <div class="">
                                                    <div class="form-group">
                                                        <label>Carton Dimension (In CMS)</label>

                                                        <div class="row cbm_d_row">
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">L</span>
                                                                        </div>
                                                                        <input type="text"
                                                                            name="record[product_mp_carton_l_cm][]"
                                                                            value="<?php echo isset($product_mp_carton_l_cm[$mp]) ? $product_mp_carton_l_cm[$mp] : 0; ?>"
                                                                            class="form-control text-center l_cm_dim cbm_l"
                                                                            autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">W</span>
                                                                        </div>
                                                                        <input type="text"
                                                                            name="record[product_mp_carton_w_cm][]"
                                                                            value="<?php echo isset($product_mp_carton_w_cm[$mp]) ? $product_mp_carton_w_cm[$mp] : 0; ?>"
                                                                            class="form-control text-center w_cm_dim cbm_w"
                                                                            autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">H</span>
                                                                        </div>
                                                                        <input type="text"
                                                                            name="record[product_mp_carton_h_cm][]"
                                                                            class="form-control text-center  h_cm_dim cbm_h"
                                                                            value="<?php echo isset($product_mp_carton_h_cm[$mp]) ? $product_mp_carton_h_cm[$mp] : 0; ?>"
                                                                            autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="">
                                                    <div class="form-group">
                                                        <label>Carton Dimension (In Inch)</label>

                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">L</span>
                                                                        </div>
                                                                        <input type="text"
                                                                            name="record[product_mp_carton_l_inch][]"
                                                                            class="form-control text-center l_inch_dim"
                                                                            value="<?php echo isset($product_mp_carton_l_inch[$mp]) ? $product_mp_carton_l_inch[$mp] : 0; ?>"
                                                                            autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">W</span>
                                                                        </div>
                                                                        <input type="text"
                                                                            name="record[product_mp_carton_w_inch][]"
                                                                            class="form-control text-center w_inch_dim"
                                                                            value="<?php echo isset($product_mp_carton_w_inch[$mp]) ? $product_mp_carton_w_inch[$mp] : 0; ?>"
                                                                            autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">H</span>
                                                                        </div>
                                                                        <input type="text"
                                                                            name="record[product_mp_carton_h_inch][]"
                                                                            class="form-control text-center h_inch_dim"
                                                                            value="<?php echo isset($product_mp_carton_h_inch[$mp]) ? $product_mp_carton_h_inch[$mp] : 0; ?>"
                                                                            autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endfor
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="basic-info same-height">
                                <h4>Inner Packet Dimension</h4>
                                <div>
                                    <div id="total_ip_dimension">
                                        @for ($ip = 0; $ip < @$edit->product_ip; $ip++)
                                            <div>
                                                @if ($ip == 0)
                                                    <label class="float-right">
                                                        <input type="checkbox" id="sameAllIPDim">
                                                        Same All Carton Size
                                                    </label>
                                                    <label
                                                        class="d-block">{{ '<strong>Inner Packet (IP) = </strong>' . $edit->product_ip }}</label>
                                                @else
                                                    <label class="text-center d-block">+</label>
                                                @endif
                                                <div class="">
                                                    <div class="form-group">
                                                        <label>Carton Dimension (In CMS)</label>

                                                        <div class="row ip_dim_row">
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">L</span>
                                                                        </div>
                                                                        <input type="text"
                                                                            name="record[product_carton_l_cm][]"
                                                                            value="<?php echo isset($carton_l_cm[$ip]) ? $carton_l_cm[$ip] : 0; ?>"
                                                                            class="form-control text-center l_cm_dim"
                                                                            autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">W</span>
                                                                        </div>
                                                                        <input type="text"
                                                                            name="record[product_carton_w_cm][]"
                                                                            value="<?php echo isset($carton_w_cm[$ip]) ? $carton_w_cm[$ip] : 0; ?>"
                                                                            class="form-control text-center w_cm_dim"
                                                                            autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">H</span>
                                                                        </div>
                                                                        <input type="text"
                                                                            name="record[product_carton_h_cm][]"
                                                                            class="form-control text-center h_cm_dim"
                                                                            value="<?php echo isset($carton_h_cm[$ip]) ? $carton_h_cm[$ip] : 0; ?>"
                                                                            autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="">
                                                    <div class="form-group">
                                                        <label>Carton Dimension (In Inch)</label>

                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">L</span>
                                                                        </div>
                                                                        <input type="text"
                                                                            name="record[product_carton_l_inch][]"
                                                                            class="form-control text-center l_inch_dim"
                                                                            value="<?php echo isset($carton_l_inch[$ip]) ? $carton_l_inch[$ip] : 0; ?>"
                                                                            autocomplete="off" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">W</span>
                                                                        </div>
                                                                        <input type="text"
                                                                            name="record[product_carton_w_inch][]"
                                                                            class="form-control text-center w_inch_dim"
                                                                            value="<?php echo isset($carton_w_inch[$ip]) ? $carton_w_inch[$ip] : 0; ?>"
                                                                            autocomplete="off" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">H</span>
                                                                        </div>
                                                                        <input type="text"
                                                                            name="record[product_carton_h_inch][]"
                                                                            class="form-control text-center h_inch_dim"
                                                                            value="<?php echo isset($carton_h_inch[$ip]) ? $carton_h_inch[$ip] : 0; ?>"
                                                                            autocomplete="off" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endfor
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="basic-info same-height">
                                <h4>CBM And Loadability</h4>
                                <div class="form-group">
                                    <label>CBM (Cubic Meter) <span id="cbmText"></span> </label>
                                    <input type="text" name="record[product_cbm]"
                                        value="{{ !empty($edit->product_cbm) ? $edit->product_cbm : 0 }}"
                                        class="form-control" placeholder="CBM (Cubic Meter)" id="productCbm"
                                        autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label>CFT <i>(CBM * 35.3147)</i></label>
                                    <input type="text" name="record[product_cft]" class="form-control"
                                        placeholder="CFT"
                                        value="{{ !empty($edit->product_cft) ? $edit->product_cft : 0 }}"
                                        id="productCft" autocomplete="off" readonly>
                                </div>
                                <div class="form-group">
                                    <label>20&apos; Cont <i>(28 / CBM)</i></label>
                                    <input type="text" name="record[product_load_20cont]"
                                        value="{{ !empty($edit->product_load_20cont) ? $edit->product_load_20cont : 0 }}"
                                        class="form-control" placeholder="20&apos; Cont" autocomplete="off"
                                        id="20Cont" readonly>
                                </div>
                                <div class="form-group">
                                    <label>40 Std Cont <i>(55 / CBM)</i></label>
                                    <input type="text" name="record[product_load_40std_cont]" class="form-control"
                                        placeholder="40 Std Cont"
                                        value="{{ !empty($edit->product_load_40std_cont) ? $edit->product_load_40std_cont : 0 }}"
                                        id="40StdCont" autocomplete="off" readonly>
                                </div>
                                <div class="form-group">
                                    <label>40 HC Cont <i>(67 / CBM)</i></label>
                                    <input type="text" name="record[product_load_40hc_cont]" class="form-control"
                                        placeholder="40 HC Cont"
                                        value="{{ !empty($edit->product_load_40hc_cont) ? $edit->product_load_40hc_cont : 0 }}"
                                        id="40HCCont" autocomplete="off" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <h4>Material & Finish</h4>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Primary Material</label>

                                        <select name="record[material1]" class="form-control"
                                            onChange="LoadMaterialType(this.value, 'LoadMaterialType1')" required>
                                            <option value="">Select</option>
                                            @foreach ($AllMaterial as $ALM)
                                                <option @if (@$edit->material1 == $ALM->material_id) selected @endif
                                                    value="{{ $ALM->material_id }}">{{ $ALM->material_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Secondary Material</label>

                                        <select name="record[material2]" class="form-control"
                                            onChange="LoadMaterialType(this.value, 'LoadMaterialType2')">
                                            <option value="">Select</option>
                                            @foreach ($AllMaterial as $ALM)
                                                <option @if (@$edit->material2 == $ALM->material_id) selected @endif
                                                    value="{{ $ALM->material_id }}">{{ $ALM->material_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Other1 Material</label>
                                        <select name="record[material3]" class="form-control"
                                            onChange="LoadMaterialType(this.value, 'LoadMaterialType3')">
                                            <option value="">Select</option>
                                            @foreach ($AllMaterial as $ALM)
                                                <option @if (@$edit->material3 == $ALM->material_id) selected @endif
                                                    value="{{ $ALM->material_id }}">{{ $ALM->material_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Other 2 Material</label>
                                        <select name="record[material4]" class="form-control"
                                            onChange="LoadMaterialType(this.value, 'LoadMaterialType4')">
                                            <option value="">Select</option>
                                            @foreach ($AllMaterial as $ALM)
                                                <option @if (@$edit->material4 == $ALM->material_id) selected @endif
                                                    value="{{ $ALM->material_id }}">{{ $ALM->material_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Other 3 Material</label>
                                        <select name="record[material5]" class="form-control"
                                            onChange="LoadMaterialType(this.value, 'LoadMaterialType5')">
                                            <option value="">Select</option>
                                            @foreach ($AllMaterial as $ALM)
                                                <option @if (@$edit->material5 == $ALM->material_id) selected @endif
                                                    value="{{ $ALM->material_id }}">{{ $ALM->material_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>Primary Material Type</label>
                                        <select name="record[materialtype1]" class="form-control"
                                            onChange="LoadFinish(this.value, 'SelFinish1')" id="LoadMaterialType1"
                                            required>
                                            <option value="">Select</option>
                                            @foreach ($MatType1 as $ALM)
                                                <option @if (@$edit->materialtype1 == $ALM->material_type_id) selected @endif
                                                    value="{{ $ALM->material_type_id }}">
                                                    {{ $ALM->material_type_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Secondary Material Type</label>
                                        <select name="record[materialtype2]" class="form-control"
                                            onChange="LoadFinish(this.value, 'SelFinish2')" id="LoadMaterialType2">
                                            <option value="">Select</option>
                                            @foreach ($MatType2 as $ALM)
                                                <option @if (@$edit->materialtype2 == $ALM->material_type_id) selected @endif
                                                    value="{{ $ALM->material_type_id }}">
                                                    {{ $ALM->material_type_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Other1 Material Type</label>
                                        <select name="record[materialtype3]" class="form-control"
                                            onChange="LoadFinish(this.value, 'SelFinish3')" id="LoadMaterialType3">
                                            <option value="">Select</option>
                                            @foreach ($MatType3 as $ALM)
                                                <option @if (@$edit->materialtype3 == $ALM->material_type_id) selected @endif
                                                    value="{{ $ALM->material_type_id }}">
                                                    {{ $ALM->material_type_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Other 2 Material Type</label>
                                        <select name="record[materialtype4]" class="form-control"
                                            onChange="LoadFinish(this.value, 'SelFinish4')" id="LoadMaterialType4">
                                            <option value="">Select</option>
                                            @foreach ($MatType4 as $ALM)
                                                <option @if (@$edit->materialtype4 == $ALM->material_type_id) selected @endif
                                                    value="{{ $ALM->material_type_id }}">
                                                    {{ $ALM->material_type_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Other 3 Material Type</label>
                                        <select name="record[materialtype5]" class="form-control"
                                            onChange="LoadFinish(this.value, 'SelFinish5')" id="LoadMaterialType5">
                                            <option value="">Select</option>
                                            @foreach ($MatType5 as $ALM)
                                                <option @if (@$edit->materialtype5 == $ALM->material_type_id) selected @endif
                                                    value="{{ $ALM->material_type_id }}">
                                                    {{ $ALM->material_type_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>Primary Finish</label>
                                        <select name="record[finish1]" class="form-control" id="SelFinish1" required>
                                            <option value="">Select</option>
                                            @foreach ($Finish1 as $ALM)
                                                <option @if (@$edit->finish1 == $ALM->finish_id) selected @endif
                                                    value="{{ $ALM->finish_id }}">{{ $ALM->finish_title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Secondary Finish</label>
                                        <select name="record[finish2]" class="form-control" id="SelFinish2">
                                            <option value="">Select</option>
                                            @foreach ($Finish2 as $ALM)
                                                <option @if (@$edit->finish2 == $ALM->finish_id) selected @endif
                                                    value="{{ $ALM->finish_id }}">{{ $ALM->finish_title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Other1 Finish</label>
                                        <select name="record[finish3]" class="form-control" id="SelFinish3">
                                            <option value="">Select</option>
                                            @foreach ($Finish3 as $ALM)
                                                <option @if (@$edit->finish3 == $ALM->finish_id) selected @endif
                                                    value="{{ $ALM->finish_id }}">{{ $ALM->finish_title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Other 2 Finish</label>
                                        <select name="record[finish4]" class="form-control" id="SelFinish4">
                                            <option value="">Select</option>
                                            @foreach ($Finish4 as $ALM)
                                                <option @if (@$edit->finish4 == $ALM->finish_id) selected @endif
                                                    value="{{ $ALM->finish_id }}">{{ $ALM->finish_title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Other 3 Finish</label>
                                        <select name="record[finish5]" class="form-control" id="SelFinish5">
                                            <option value="">Select</option>
                                            @foreach ($Finish5 as $ALM)
                                                <option @if (@$edit->finish5 == $ALM->finish_id) selected @endif
                                                    value="{{ $ALM->finish_id }}">{{ $ALM->finish_title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <div class="col">
                            <div class="basic-info same-height-two">
                                <h4>Packing</h4>
                                @foreach ($packings as $pack)
                                    <div class="form-group">
                                        <label class="animated-checkbox">
                                            <input type="checkbox" value="" class="packing_inp"
                                                @if (in_array($pack->packing_id, $pkg_ids)) checked @endif>
                                            <span class="label-text">{{ $pack->packing_name }}</span>
                                        </label>

                                        <input type="text" name="packing[{{ $pack->packing_id }}]"
                                            class="form-control packing_value"
                                            value="{{ @$pkg_arr[$pack->packing_id] }}"
                                            @if (!in_array($pack->packing_id, $pkg_ids)) readonly @endif>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col">
                            <div class="basic-info same-height-two">
                                <h4>Hardware Specification</h4>
                                @foreach ($hardware_arrs as $hname)
                                    <div class="form-group">
                                        <label class="animated-checkbox">
                                            <input type="checkbox" value="{{ str_replace(' ', '-', $hname) }}"
                                                class="packing_inp" @if (in_array(str_replace(' ', '-', $hname), $hw_names)) checked @endif>
                                            <span class="label-text">{{ $hname }}</span>
                                        </label>
                                        <input type="text" name="hardware[{{ str_replace(' ', '-', $hname) }}]"
                                            value="{{ @$hw_arr[str_replace(' ', '-', $hname)] }}"
                                            class="form-control packing_value"
                                            @if (!in_array(str_replace(' ', '-', $hname), $hw_names)) readonly @endif>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div>

                    @php
                        $dir = 'imgs/products/';
                        $image = $image1 = $image2 = $image3 = $image4 = $image5 = 'imgs/no-image.png';
                        $required = '';
                        if (!empty($edit->product_image)) {
                            $image = $dir . $edit->product_image;
                            $required = '';
                        }
                        if (!empty($edit->product_image1)) {
                            $image1 = $dir . $edit->product_image1;
                        }
                        if (!empty($edit->product_image2)) {
                            $image2 = $dir . $edit->product_image2;
                        }
                        if (!empty($edit->product_image3)) {
                            $image3 = $dir . $edit->product_image3;
                        }
                        if (!empty($edit->product_image4)) {
                            $image4 = $dir . $edit->product_image4;
                        }
                        if (!empty($edit->product_image5)) {
                            $image5 = $dir . $edit->product_image5;
                        }
                    @endphp

                    <div class="row">
                        <div class="col-2">
                            <div class="basic-info">
                                <h4>Feature Image</h4>
                                <label class="upload_image">
                                    <img src="{{ url($image) }}" id="product_image">
                                    <input type="file" name="product_image" accept="image/*" id="productImage">
                                </label>
                                <label for="productImage" class="btn btn-block btn-info">Select Image</label>
                                @if (isset($edit->product_id))
                                    <button type="button" class="btn btn-danger"
                                        onClick="return RemoveImage(1)">Remove Image</button>
                                @endif
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="basic-info">
                                <h4>Image 1</h4>
                                <label class="upload_image">
                                    <img src="{{ url($image1) }}" id="MainImage1">
                                    <input type="file" name="product_image1" accept="image/*" id="productImage1">
                                </label>
                                <label for="productImage1" class="btn btn-block btn-info">Select Image</label>
                                @if (isset($edit->product_id))
                                    <button type="button" class="btn btn-danger"
                                        onClick="return RemoveImage(2)">Remove Image</button>
                                @endif
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="basic-info">
                                <h4>Image 2</h4>
                                <label class="upload_image">
                                    <img src="{{ url($image2) }}" id="MainImage2">
                                    <input type="file" name="product_image2" accept="image/*" id="productImage2">
                                </label>
                                <label for="productImage2" class="btn btn-block btn-info">Select Image</label>
                                @if (isset($edit->product_id))
                                    <button type="button" class="btn btn-danger"
                                        onClick="return RemoveImage(3)">Remove Image</button>
                                @endif
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="basic-info">
                                <h4>Image 3</h4>
                                <label class="upload_image">
                                    <img src="{{ url($image3) }}" id="MainImage3">
                                    <input type="file" name="product_image3" accept="image/*" id="productImage3">
                                </label>
                                <label for="productImage3" class="btn btn-block btn-info">Select Image</label>
                                @if (isset($edit->product_id))
                                    <button type="button" class="btn btn-danger"
                                        onClick="return RemoveImage(4)">Remove Image</button>
                                @endif
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="basic-info">
                                <h4>Image 4</h4>
                                <label class="upload_image">
                                    <img src="{{ url($image4) }}" id="MainImage4">
                                    <input type="file" name="product_image4" accept="image/*" id="productImage4">
                                </label>
                                <label for="productImage4" class="btn btn-block btn-info">Select Image</label>
                                @if (isset($edit->product_id))
                                    <button type="button" class="btn btn-danger"
                                        onClick="return RemoveImage(5)">Remove Image</button>
                                @endif
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="basic-info">
                                <h4>Image 5</h4>
                                <label class="upload_image">
                                    <img src="{{ url($image5) }}" id="MainImage5">
                                    <input type="file" name="product_image5" accept="image/*" id="productImage5">
                                </label>
                                <label for="productImage5" class="btn btn-block btn-info">Select Image</label>
                                @if (isset($edit->product_id))
                                    <button type="button" class="btn btn-danger"
                                        onClick="return RemoveImage(6)">Remove Image</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </form>
</div>

<script>
    $('#save-data').on('click', function() {
        alert('ab');
    })

    function RemoveImage(index) {
        if (confirm("Are You Sure to Delete This Image")) {
            $.ajax({
                url: "{{ URL('RemoveProductImate') }}/{{ @$edit->product_id }}/" + index,
                type: "GET",
                contentType: false,
                cache: false,
                processData: false,
                success: function(data, textStatus, jqXHR) {
                    if (index == 1) {
                        document.getElementById("product_image").src =
                            "{{ URL('imgs/no-image.png') }}";
                    }
                    if (index == 2) {
                        document.getElementById("MainImage1").src =
                            "{{ URL('imgs/no-image.png') }}";
                    }

                    if (index == 3) {
                        document.getElementById("MainImage2").src =
                            "{{ URL('imgs/no-image.png') }}";
                    }

                    if (index == 4) {
                        document.getElementById("MainImage3").src =
                            "{{ URL('imgs/no-image.png') }}";
                    }

                    if (index == 5) {
                        document.getElementById("MainImage4").src =
                            "{{ URL('imgs/no-image.png') }}";
                    }

                    if (index == 6) {
                        document.getElementById("MainImage5").src =
                            "{{ URL('imgs/no-image.png') }}";
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {

                }
            });
        }
    }





    function UpdateSubCate(val) {
        console.log(val);

        $("#product-sub-category").attr('disabled', 'disabled').html($('<option />').val('').text('Loading'));
        $.ajax({
            url: "{{ URL('/') }}/ajax/get_category",
            data: {
                id: val
            },
            success: function(res) {
                $("#product-sub-category").removeAttr('disabled').html($("<option />").text(
                    "Select Subcategory").val(''));

                $.each(res.categories, function(i, row) {
                    $("#product-sub-category").append($("<option />").text(row.category_name).val(
                        row.category_id));
                });

            }
        });
    }



    function LoadMaterialType(ID, LoadID) {
        $("#" + LoadID).attr('disabled', 'disabled').html($('<option />').val('').text('Loading'));
        $.ajax({
            url: "{{ URL('/') }}/ajax/LoadMaterialType",
            data: {
                id: ID
            },
            success: function(res) {
                $("#" + LoadID).removeAttr('disabled').html($("<option />").text("Select Type").val(''));

                $.each(res.categories, function(i, row) {
                    $("#" + LoadID).append($("<option />").text(row.material_type_name).val(row
                        .material_type_id));
                });

            }
        });
    }

    function LoadFinish(ID, LoadID) {
        $("#" + LoadID).attr('disabled', 'disabled').html($('<option />').val('').text('Loading'));
        $.ajax({
            url: "{{ URL('/') }}/ajax/LoadFinish",
            data: {
                id: ID
            },
            success: function(res) {
                $("#" + LoadID).removeAttr('disabled').html($("<option />").text("Select Type").val(''));

                $.each(res.categories, function(i, row) {
                    $("#" + LoadID).append($("<option />").text(row.finish_title).val(row
                        .finish_id));
                });

            }
        });
    }
</script>
