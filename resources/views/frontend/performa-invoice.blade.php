<html>

<head>
    <title></title>

    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('icomoon/style.css') }}
    {{ HTML::style('css/style.css') }}

    <style>
        @page {
            margin: 0;
        }

        body {}

        * {
            font-size: 10px;
            color: #000;
            font-weight: 600;
        }

        .w150 {
            width: 150px;
        }

        .w250 {
            width: 250px;
        }

        .grey {
            color: #555;
        }

        .dt_grey_bg {
            background: #ff0;
            color: #f00;
            display: inline-block;
            padding: 2px;
        }

        .print_page {
            width: 270mm;
            height: 210mm;
            padding: 15px;
            box-sizing: border-box;
            /* border: 1px solid #000; */
            margin: auto;
            color: #000;
        }

        .print_table {
            width: 100%;
        }

        .print_table th,
        .print_table td {
            border: 1px solid #000;
            padding: 5px;
        }

        .logo {
            height: 80px;
        }

        .invoice_block {
            border: .5mm solid #000;
            border-radius: 5px;
            padding: 5px;
            overflow: hidden;
            color: #f00;
            background: #81ecec;
        }

        .invoice_block span {
            display: block;
            float: left;
            background: #ccc;
            margin: -5px 0 -5px -5px;
            padding: 10px;
            color: #fff;
        }

        .product_row * {
            font-size: 8px;
            font-weight: bold;
        }

        .product_row.product_heading * {
            text-align: center;
        }

        .product_row th,
        .product_row td {
            padding: 2px;
        }

        .footer_row td {
            border-right: 0;
            border-left: 0;
            border-bottom: 0;
        }

        @media print {
            @page {
                size: landscape
            }

            .print_page {
                margin: 0;
                width: 100%;
            }
        }
    </style>
</head>

<body style="background: #FFFFFF">

    <div class="print_page">
        <table class="print_table">
            <tr>
                <td colspan="22">
                    <div class="clearfix">
                        <div class="float-left">
                            <div class="grey">CONSIGNOR</div>
                            <div class="">
                                <strong>Global Home Products Pte. Ltd.</strong><br>
                                20 CECIL STREET, <br>
                                #05 - 03 PLUS, <br>
                                <!--ONE RAFFLES PLACE,-->
                            </div>
                        </div>
                        <div class="float-right">
                            <div style="font-size: 18px; color: #555;">PERFORMA INVOICE</div>
                            <div class="invoice_block" style="font-size: 16px;">
                                <span>No. #</span>
                                GHP-202122-{{ sprintf('%03d', $record->pi_id + 100) }}
                            </div>
                        </div>
                        <div class="float-right">
                            <img src="{{ url('imgs/logo1.png') }}" alt="" class="logo">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="float-left w250">
                            SINGAPORE (049705)
                        </div>
                        <div class="float-left w250" style="color: #2ecc71;">
                            FSC License CODE:- FSC-C141273
                        </div>
                        <div class="float-right">
                            Date:
                            <span
                                class="w150 text-center dt_grey_bg">{{ date('d-M-Y', strtotime($record->pi_date)) }}</span>
                        </div>
                        <div class="float-right w250">
                            Registration No.- 201810833W
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="22">
                    <div class="clearfix">
                        <div class="float-left" style="width: 30%;">
                            <div class="grey">CONSIGNEE / BUYER</div>
                            <div class="">
                                <strong>{{ $record->user_name }}</strong><br>
                                {{ nl2br($record->user_address) }}<br>
                                {{ $record->state_name }}, {{ $record->country_name }}
                            </div>
                        </div>
                        <div class="float-left" style="width: 30%;">
                            <div>NOTIFY PARTY (If Other Then Consignee)</div>
                            <p><br>&nbsp;<br></p>
                            <div class="text-center" style="width: 90%; margin: auto;">
                                <strong class="d-inline-block" style="width: 29%;">Order ref:</strong>
                                @if ($record->pi_order_ref != '')
                                    <span class="d-inline-block"
                                        style="width: 68%; background: #ccc; padding: 3px;">{{ $record->pi_order_ref }}</span>
                                @else
                                    <span class="d-inline-block"
                                        style="width: 68%; background: #ccc; padding: 3px;">GHP-202122-{{ sprintf('%03d', $record->pi_id) }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="float-right clearfix" style="width: 40%;">
                            <div class="clearfix">
                                <div class="float-left" style="width: 60%; font-size: 10px;">
                                    Country of Origin of goods
                                </div>
                                <div class="float-left" style="width: 40%; font-size: 10px;">
                                    {{ $record->quote_origin_country }}
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="float-left" style="width: 60%; font-size: 10px;">
                                    Country of the first Destination
                                </div>
                                <div class="float-left" style="width: 40%; font-size: 10px;">
                                    {{ $record->quote_first_destination_country }}
                                </div>
                            </div>
                            @php
                                $GetDataPortCountry = DB::table('ports')
                                    ->where('port_name', $record->quote_loading_port)
                                    ->first();
                                $GetLiveCountry = DB::table('countries')
                                    ->where('country_id', $GetDataPortCountry->port_country)
                                    ->first();
                            @endphp
                            <div class="clearfix">
                                <div class="float-left" style="width: 60%; font-size: 10px;">
                                    Port of Loading
                                </div>
                                <div class="float-left" style="width: 40%; font-size: 10px;">
                                    {{ $record->quote_loading_port }}, {{ $GetLiveCountry->country_short_name }}
                                </div>
                            </div>

                            @php
                                $GetDataPortCountry = DB::table('ports')
                                    ->where('port_name', $record->quote_discharge_port)
                                    ->first();
                                $GetLiveCountry = DB::table('countries')
                                    ->where('country_id', $GetDataPortCountry->port_country)
                                    ->first();
                            @endphp

                            <div class="clearfix">
                                <div class="float-left" style="width: 60%; font-size: 10px;">
                                    Port of Discharge
                                </div>
                                <div class="float-left" style="width: 40%; font-size: 10px;">
                                    {{ $record->quote_discharge_port }}, {{ $GetLiveCountry->country_short_name }}
                                </div>
                            </div>

                            @php
                                $GetDataPortCountry = DB::table('ports')
                                    ->where('port_name', $record->quote_delivery_port)
                                    ->first();
                                $GetLiveCountry = DB::table('countries')
                                    ->where('country_id', $GetDataPortCountry->port_country)
                                    ->first();
                            @endphp

                            <div class="clearfix">
                                <div class="float-left" style="width: 60%; font-size: 10px;">
                                    Place of Delivery
                                </div>
                                <div class="float-left" style="width: 40%; font-size: 10px;">
                                    {{ $record->quote_delivery_port }}, {{ $GetLiveCountry->country_short_name }}
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="float-left" style="width: 60%; font-size: 10px;">
                                    Port of Receipt
                                </div>
                                <div class="float-left" style="width: 40%; font-size: 10px;">
                                    {{ $record->quote_reciept_port }}
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            @php
                $GetDataPortCountry = DB::table('ports')
                    ->where('port_name', $record->quote_loading_port)
                    ->first();
                $GetLiveCountry = DB::table('countries')
                    ->where('country_id', $GetDataPortCountry->port_country)
                    ->first();
            @endphp
            <tr>
                <td class="text-center" colspan="4">DELIVERY DATE</td>
                <td class="text-center" colspan="2">TERM OF DELIVERY</td>
                <td class="text-center" colspan="8">TERM OF PAYMENT</td>
                <td class="text-center" colspan="4">CURRENCY</td>
                <td class="text-center" colspan="4">EQUIPMENT SIZE</td>
            </tr>


            @php
                $i = 0;
                $GrandTotal = 0;
                $GrandCBM = 0;
                $TotalQty = 0;
            @endphp
            @foreach ($products as $p)
                @php
                    
                    $SubTotal = (!empty($p->pipro_price) ? $p->pipro_price : $p->qpro_price) * $p->pipro_qty;
                    $SubCBM = $p->product_cbm * $p->pipro_qty;
                    $GrandTotal += $SubTotal;
                    $GrandCBM += $SubCBM;
                    $TotalQty += $p->pipro_qty;
                @endphp
            @endforeach


            @php
                $ST40HQ = 0;
                $ST40SD = 0;
                $ST20SD = 0;
                $LCL = 0;
                
                $total_cbmC = ceil($GrandCBM);
                $ST40HQ = floor($total_cbmC / 68);
                
                $RemianingCBM = $GrandCBM - $ST40HQ * 68;
                
                if ($RemianingCBM > 0) {
                    $ST40SD = floor($RemianingCBM / 58);
                    $RemianingCBM = $RemianingCBM - $ST40SD * 58;
                }
                
                if ($RemianingCBM > 0) {
                    $ST20SD = floor($RemianingCBM / 28);
                    $RemianingCBM = $RemianingCBM - $ST20SD * 28;
                }
                
                if ($RemianingCBM > 0) {
                    $LCL = floor($RemianingCBM / 15);
                    $RemianingCBM = $RemianingCBM - $LCL * 15;
                }
            @endphp



            <tr>
                <td class="text-center" colspan="4" style="background: #81ecec;">
                    <strong>{{ date('d-M-Y', strtotime($record->pi_delivery_date)) }}</strong> </td>
                <td class="text-center" colspan="2"><b>{{ $record->quote_price_term }} -
                        {{ $record->quote_loading_port }}, {{ $GetLiveCountry->country_short_name }}</b></td>
                @php
                    $RemaingAmount = 100 - $record->quote_payment;
                @endphp
                <td class="text-center" colspan="8" style="background: #fd79a8;"><strong style="font-size: 8px;">
                        @if ($record->quote_payment > 0)
                            {{ $record->quote_payment }}% Advance &
                        @endif

                        {{ $RemaingAmount }}% T.T. AGAINST MAIL COPY OF DOCS
                    </strong></td>
                <td class="text-center" colspan="4"><b>{{ $record->currency_short_name }}</b></td>
                <td class="text-center" colspan="4" style="background: #fd79a8;"><strong>
                        @if ($record->qty1 != '')
                            {{ $record->qty1 }} * {{ $record->container1 }}
                        @endif

                        @if ($record->qty2 != '')
                            <br>{{ $record->qty2 }} * {{ $record->container2 }}
                        @endif

                        @if ($record->qty3 != '')
                            <br>{{ $record->qty3 }} * {{ $record->container3 }}
                        @endif

                    </strong></td>
            </tr>


            <tr class="product_row product_heading">
                <td colspan="2" style="background: #ccc;">Total Number of SKU:</td>
                <td colspan="2" style="color: #f00; background: #ccc;">{{ count($products) }}</td>
                <td colspan="2" style="background: #ccc;">Total Ordered Quantity:</td>
                <td style="color: #f00; background: #ccc;">{{ $TotalQty }} Pcs/Set</td>
                <td style="background: #ccc;">Total CBM</td>
                <td style="color: #f00; background: #ccc;">{{ number_format($GrandCBM, 2) }}</td>
                <td style="background: #ccc;" colspan="6">Total Performa Invoice Value</td>
                <td style="background: #ccc;" colspan="3" style="color: #f00;">
                    ${{ number_format($GrandTotal, 2) }}</td>
                <td style="background: #ccc;" colspan="2" class="text-center">Value of Advance Amount</td>
                <td style="background: #ccc; color: #f00;" colspan="2">
                    @php
                        $AdvnaceAmont = ($record->quote_payment * $GrandTotal) / 100;
                    @endphp
                    ${{ number_format($AdvnaceAmont, 2) }}
                </td>
            </tr>
            <tr class="product_row product_heading">
                <th>S.No.</th>
                <th>Product Code</th>
                <th>SKU<br>(customer)</th>
                <th>Barcode</th>
                <th>Product Picture</th>
                <th>Description</th>
                <th>Buyer Description</th>
                <th>Material</th>
                <th>Finish</th>
                <th>FSC Status</th>
                <th>Species</th>
                <th colspan="3">PRODUCT SIZE IN<br>CM (HxWxD)</th>
                <th style="background: #ebebeb;">Qty</th>
                <th colspan="3">PACKED SIZE IN<br>CM (HxWxD)</th>
                <th>CBM</th>
                <th>USD</th>
                <th>Total in USD</th>
                <th>Total CBM</th>
                <!-- <th style="background: #ff0;">NW/Pc.</th>
                   <th style="background: #ff0;">GW/Pkt.</th> -->
            </tr>

            @foreach ($products as $p)
                @php
                    $SubTotal = (!empty($p->pipro_price) ? $p->pipro_price : $p->qpro_price) * $p->pipro_qty;
                    $SubCBM = $p->product_cbm * $p->pipro_qty;
                    
                @endphp
                @php
                    $GetProductSizel = [];
                    $GetProductSizew = [];
                    $GetProductSizeh = [];
                    
                    if ($p->product_mp_carton_l_cm != '0') {
                        $GetProductSizel = unserialize($p->product_mp_carton_l_cm);
                    }
                    
                    if ($p->product_mp_carton_w_cm != '0') {
                        $GetProductSizew = unserialize($p->product_mp_carton_w_cm);
                    }
                    if ($p->product_mp_carton_h_cm != '0') {
                        $GetProductSizeh = unserialize($p->product_mp_carton_h_cm);
                    }
                @endphp

                <tr class="product_row">
                    <td>{{ ++$i }}.</td>
                    <td class="text-center" style="color: #a29bfe;">{{ $p->product_code }}</td>
                    <td class="text-center" style="color: #f00;">{{ $p->pipro_sku }}</td>
                    <td>{{ $p->pipro_barcode }}</td>
                    <td>
                        <img src="{{ url('imgs/products/' . $p->product_image) }}" alt=""
                            style="width: 70px; height: 50px; object-fit: contain;">
                    </td>
                    <td>
                        {{ $p->product_name }}<br>
                        {{ $p->product_type }} Version<br>
                        {{ $p->product_mp }}xCarton<br>
                        <!--{{ $p->product_dim_extra }}-->

                        @if ($record->pi_show_nw == 'Y')
                            <br>NW : {{ $p->product_net_weight_kg }}
                        @endif

                        @if ($record->pi_show_gw == 'Y')
                            <br>GW: {{ $p->product_tot_gross_weight_kg }}
                        @endif
                    </td>
                    <td>
                        {{ nl2br($p->pipro_buyer_desc) }}
                    </td>
                    <td>
                        @php
                            $Matrl1 = DB::table('material_type')
                                ->where('material_type_id', $p['materialtype1'])
                                ->first();
                            $Matrl2 = DB::table('material_type')
                                ->where('material_type_id', $p['materialtype2'])
                                ->first();
                        @endphp

                        {{ @$Matrl1->material_type_name }}/<br>
                        {{ @$Matrl2->material_type_name }}

                    </td>
                    <td>
                        @php
                            $WoodFinish = DB::table('finish')
                                ->where('finish_id', $p['finish1'])
                                ->first();
                            $WoodFinish1 = DB::table('finish')
                                ->where('finish_id', $p['finish2'])
                                ->first();
                        @endphp
                        {{ @$WoodFinish->finish_title }}/<br>
                        {{ @$WoodFinish1->finish_title }}

                    </td>
                    <td align="center">{{ $p->pipro_fsc_status == '' ? '-' : $p->pipro_fsc_status }}</td>
                    <td align="center">{{ $p->pipro_species == '' ? '-' : $p->pipro_species }}</td>
                    <td>{{ $p->product_l_cm }}</td>
                    <td style="color: ;">{{ $p->product_w_cm }}</td>
                    <td>{{ $p->product_h_cm }}</td>
                    <td style="color: #f00;background: #ebebeb;">{{ $p->pipro_qty }}</td>
                    <td>
                        {{ implode(" /\n", $GetProductSizel) }}
                    </td>
                    <td>
                        {{ implode(" /\n", $GetProductSizew) }}
                    </td>
                    <td>
                        {{ implode(" /\n", $GetProductSizeh) }}
                    </td>
                    <td>{{ number_format($p->product_cbm, 2) }}</td>
                    <td>${{ !empty($p->pipro_price) ? $p->pipro_price : $p->qpro_price }}</td>
                    <td>${{ number_format($SubTotal, 2) }}</td>
                    <td>{{ number_format($SubCBM, 2) }} </td>
                    <!-- <td style="background: #ff0;">31.30</td>
                   <td style="background: #ff0;">35.20</td> -->
                </tr>
            @endforeach
            <tr>
                <td colspan="20"><strong>TOTAL</strong></td>
                <td style="background: #fd79a8;">${{ number_format($GrandTotal, 2) }}</td>
                <td style="background: #fd79a8;">{{ number_format($GrandCBM, 2) }}</td>
            </tr>
            <tr>
                <td colspan="15" style="background: #000; color: #fff; text-decoration: underline;">BANK DETAILS FOR
                    REMITTANCE</td>
                <td colspan="7" rowspan="4" valign="top" class="text-center">
                    For: Global Home Products Pte. Ltd.
                </td>
            </tr>
            <tr class="footer_row">
                <td colspan="5" valign="top" style="border-left: 1px solid #000;">
                    <strong>Beneficiary Name And Address</strong><br>
                    Global Home Products Pte. Ltd. <br>
                    20 CECIL STREET, <br>
                    #05 - 03 PLUS, <br>
                    SINGAPORE (049705) <br><br>
                    USD Account Number - 503434599301
                </td>
                <td colspan="3" valign="top">
                    <strong>Bank Name and Address -</strong><br>
                    OCBC Bank <br>
                    65, Chulia Street <br>
                    #11-01 OCBC Centre East <br>
                    Singapore 049513 <br><br>
                    SWIFT CODE - OCBCSGSG
                </td>
                <td colspan="7" valign="top">
                    <strong>INTERMIDIATORY BANK</strong><br>
                    JP Morgan Chase Bank,<br>
                    New York, USA<br><br>

                    SWIFT CODE - CHASUS33
                </td>
            </tr>
            <tr class="footer_row product_row">
                <td colspan="15" style="border-left: 1px solid; border-bottom: 1px solid; border-top: 0;">
                    <strong>Declaration:</strong><br>
                    We declare that the goods described in our invoice are actual in price and all information mentioned
                    are true and correct.
                </td>
            </tr>
        </table>
    </div>


    <script>
        // window.print();
    </script>
</body>

</html>
