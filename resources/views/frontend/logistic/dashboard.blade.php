<div class="container-fluid">
	<div class="row">
		<div class="col-lg-8">
			<div class="row dash-panel">
				<div class="col-sm-4">
					<div class="card mt-3">
						<h3 class="card-title">Enquiry</h3>
						<div>
							<div class="text-center dash-icon">
								<i class="icon-file-text"></i>
							</div>
						</div>
						<ul class="list-group">
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Check Enquiry</a>
							</li>
						  	<li class="list-group-item">
								<a href="" class="list-item-link">View Enquiry</a>
							</li>
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Upload Offer</a>
							</li>
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Upload new</a>
							</li>
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Product Offer</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card mt-3">
						<h3 class="card-title">Purchases</h3>
						<div>
							<div class="text-center dash-icon">
								<i class="icon-purchase"></i>
							</div>
						</div>
						<ul class="list-group">
						  	<li class="list-group-item">
								<a href="" class="list-item-link">View Purchase</a>
							</li>
							<li class="list-group-item">
								<a href="" class="list-item-link">View all Purchase</a>
							</li>
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Orders</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card mt-3">
						<h3 class="card-title">Inventory</h3>
						<div>
							<div class="text-center dash-icon">
								<i class="icon-inventory"></i>
							</div>
						</div>
						<ul class="list-group">
						  	<li class="list-group-item">
								<a href="" class="list-item-link">View Inventory</a>
							</li>
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Product List</a>
							</li>
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Currently on production</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card mt-3">
						<h3 class="card-title">Order Schedule</h3>
						<div>
							<div class="text-center dash-icon">
								<i class="icon-stats-bars"></i>
							</div>
						</div>
						<ul class="list-group">
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Check Order Schedule</a><br>
								<small>(Check your Order Schedule)</small>
							</li>
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Track Order Status</a><br>
								<small>(Track Order Status)</small>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card mt-3">
						<h3 class="card-title">Documents</h3>
						<div>
							<div class="text-center dash-icon">
								<i class="icon-print"></i>
							</div>
						</div>
						<ul class="list-group">
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Documents</a><br>
								<small>(Check Document for Shipped Order)</small>
							</li>
						</ul>
					</div>
				</div>

				<!-- <div class="col-sm-4">
					<div class="card mt-3">
						<h3 class="card-title">Reports</h3>
						<div>
							<div class="text-center dash-icon">
								<i class="icon-print"></i>
							</div>
						</div>
						<ul class="list-group">
						  	<li class="list-group-item">
								<a href="" class="list-item-link">View Reports</a><br>
								<small>(View inspection report)</small>
							</li>
						</ul>
					</div>
				</div> -->
			</div>
		</div>
		<div class="col-lg-4">
			<div class="card mt-3">
				<h3 class="card-title">Quick Links</h3>
				<div>
					<div class="text-center dash-icon">
						<i class="icon-link"></i>
					</div>
				</div>
				<ul class="list-group">
				  	<li class="list-group-item">
						<a href="" class="list-item-link"><i class="icon-file-text"></i> All Quotes</a>
					</li>
				  	<li class="list-group-item">
						<a href="" class="list-item-link"><i class="icon-purchase"></i> All Purchase Orders</a>
					</li>
				  	<li class="list-group-item">
						<a href="" class="list-item-link"><i class="icon-stats-bars"></i> Order Status</a>
					</li> 
				</ul>
			</div>
		</div>
	</div>
	
</div>