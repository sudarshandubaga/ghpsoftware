<div class="col">
	<div class="form-group">
        <label>GHP Order No.</label>
        @if($PIData->pi_order_ref != "")
		    <input type="text" name="user[GHPOrderNo]" class="form-control" id="GHPOrderNo" value="{{ $PIData->pi_order_ref }}">
		@else
		    <input type="text" name="user[GHPOrderNo]" class="form-control" id="GHPOrderNo" value="GHP-201819-{{ sprintf('%03d', $PIData->pi_id) }}">
		@endif
    </div>
</div>
<div class="col">
	<div class="form-group">
        <label>CBM</label>
		<input type="text" name="user[CBM]" class="form-control" id="CBM" value="{{ $TotalCBM }}">
    </div>
</div>
<div class="col">
    <div class="form-group">
        <label>Vendor</label>
        <input type="text" name="user[Vendor]" class="form-control" id="Vendor" value="{{ $query->user_name }}">
    </div>
</div>