@php
    $PINo = '';
    $PONO = '';
    
    // foreach ($query as $RQ) {
    //     $PINo .= 'GHP-201819-' . sprintf('%03d', $RQ->pi_id) . ',';
    // }
    // foreach ($query1 as $RQ) {
    //     $PONO .= '1508' . str_pad($RQ->po_id, 2, '0', STR_PAD_LEFT) . ',';
    // }
    
@endphp

<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <label>Buyer Name</label>
            <input type="text" name="user[exs_logistics_buyer]" class="form-control" required id="BuyerName"
                value="{{ @$PIData->quote->buyer->user->user_name }}">
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <label>PI Number</label>
            <input type="text" name="user[exs_logistics_pi]" class="form-control" required id="PiNumber"
                onChange="LoadPO(this.value)" value="{{ 'GHP-202122-' . ($PIData->pi_id + 100) }}">

        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <label>PO Number</label>
            <input type="text" name="user[exs_logistics_po]" class="form-control" required id="PONumber"
                value="{{ $poNos }}">
        </div>
    </div>
</div>
</div>

<div class="row">
    <div class="col">
        <div class="form-group">
            <label>GHP Order No.</label>
            @if ($PIData->pi_order_ref != '')
                <input type="text" name="user[GHPOrderNo]" class="form-control" id="GHPOrderNo"
                    value="{{ $PIData->pi_order_ref }}">
            @else
                <input type="text" name="user[GHPOrderNo]" class="form-control" id="GHPOrderNo"
                    value="GHP-201819-{{ sprintf('%03d', $PIData->pi_id) }}">
            @endif
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label>CBM</label>
            <input type="text" name="user[CBM]" class="form-control" id="CBM"
                value="{{ $GetBookingData->po_invoice_total_cbm }}">
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label>Vendor</label>
            <input type="text" name="user[Vendor]" class="form-control" id="Vendor"
                value="{{ $GetBookingData->vendor->vendor_org_name }}">
        </div>
    </div>
</div>
