<div class="container-fluid">
	<div class="row">
		<div class="col-lg-8">
			<div class="row dash-panel">
				<div class="col-sm-4">
					<div class="card mt-3">
						<h3 class="card-title">Enquiry</h3>
						<div>
							<div class="text-center dash-icon">
								<i class="icon-file-text"></i>
							</div>
						</div>
						<ul class="list-group">
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Current Enquiry</a><br>
								<small>(View your Enquiry)</small>
							</li>
						  	<li class="list-group-item">
								<a href="" class="list-item-link">View Enquiry</a><br>
								<small>(View all Enquiry)</small>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card mt-3">
						<h3 class="card-title">Purchases</h3>
						<div>
							<div class="text-center dash-icon">
								<i class="icon-purchase"></i>
							</div>
						</div>
						<ul class="list-group">
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Cart / Selection</a><br>
								<small>(Place/Create new order)</small>
							</li>
						  	<li class="list-group-item">
								<a href="" class="list-item-link">View Purchase</a><br>
								<small>(View all purchase)</small>
							</li>
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Reorder</a><br>
								<small>(Create Purchase from reorder quantities)</small>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card mt-3">
						<h3 class="card-title">Inventory</h3>
						<div>
							<div class="text-center dash-icon">
								<i class="icon-inventory"></i>
							</div>
						</div>
						<ul class="list-group">
						  	<li class="list-group-item">
								<a href="" class="list-item-link">View Inventory</a><br>
								<small>(View products currently on production)</small>
							</li>
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Product List</a><br>
								<small>(View list of your products)</small>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card mt-3">
						<h3 class="card-title">Order Tracking</h3>
						<div>
							<div class="text-center dash-icon">
								<i class="icon-stats-bars"></i>
							</div>
						</div>
						<ul class="list-group">	
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Track Order</a><br>
								<small>(Check your order status)</small>
							</li>
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Check Status</a><br>
								<small>(Check documents for shipped orders)</small>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card mt-3">
						<h3 class="card-title">Sales</h3>
						<div>
							<div class="text-center dash-icon">
								<i class="icon-stats-bars"></i>
							</div>
						</div>
						<ul class="list-group">
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Commercial Invoice</a><br>
								<small>(Check Commercial Invoice)</small>
							</li>
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Sales Summery</a><br>
								<small>(Check Summery)</small>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card mt-3">
						<h3 class="card-title">Documents</h3>
						<div>
							<div class="text-center dash-icon">
								<i class="icon-stats-bars"></i>
							</div>
						</div>
						<ul class="list-group">
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Document</a><br>
								<small>(Check Documents)</small>
							</li>
						  	<li class="list-group-item">
								<a href="" class="list-item-link">Certificate</a><br>
								<small>(Check Certificate)</small>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="card mt-3">
				<h3 class="card-title">Quick Links</h3>
				<div>
					<div class="text-center dash-icon">
						<i class="icon-link"></i>
					</div>
				</div>
				<ul class="list-group">
				  	<li class="list-group-item">
						<a href="" class="list-item-link"><i class="icon-file-text"></i> All Quotes</a>
					</li>
				  	<li class="list-group-item">
						<a href="" class="list-item-link"><i class="icon-purchase"></i> All Purchase Orders</a>
					</li>
				  	<li class="list-group-item">
						<a href="" class="list-item-link"><i class="icon-stats-bars"></i> Order Status</a>
					</li>
				  	<li class="list-group-item">
						<a href="" class="list-item-link"><i class="icon-print"></i> Reports</a>
					</li>
				</ul>
			</div>
			<div class="">
					<div class="card mt-3">
						<h3 class="card-title">Reports</h3>
						<div>
							<div class="text-center dash-icon">
								<i class="icon-print"></i>
							</div>
						</div>
						<ul class="list-group">
						  	<li class="list-group-item">
								<a href="" class="list-item-link">View Reports</a><br>
								<small>(View inspection report)</small>
							</li>
						</ul>
					</div>
				</div>
		</div>
	</div>
	
</div>