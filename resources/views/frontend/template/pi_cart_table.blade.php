@php
    
    $sn = $totalQty = $total = $total_cbm = 0;
    $pi_cart = session('pi_cart');
    
    $carts = session('pi_cart');
    
    $products = [];
    if (!empty($carts)) {
        foreach ($carts as $pid => $data) {
            extract($data);
    
            $product = DB::table('products')
                ->where('product_id', $pid)
                ->first();
            $product->pipro_qty = $pipro_qty;
            $product->pipro_price = $pipro_price;
            $product->pipro_remark = @$pipro_remark;
            $product->pipro_is_approved = @$pipro_is_approved;
            $product->pipro_fsc_status = @$pipro_fsc_status;
            $product->pipro_species = @$pipro_species;
            $products[] = $product;
        }
    }
@endphp
@if (!empty($products))
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>S.No.</th>
                    <th class="text-center">SKU <br> (Customer)</th>
                    <th>BARCODE</th>
                    <th>PICTURE</th>
                    <th>DESCRIPTION</th>
                    <th>BUYER DESCRIPTION</th>
                    <th>CBM</th>
                    <th>Price ($)</th>
                    <th>Qty</th>
                    <th>Subtotal</th>
                    <th>Total CBM</th>
                    <th>FSC Status</th>
                    <th>Species</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $p)
                    @php
                        
                        // $qpro = App\Models\QuoteProduct::with('product')->where('pipro_qid', $quote_id)->where('pipro_pid',$p->product_id)->where('pipro_is_approved', 'Y')->first();
                        
                        $pipro_price = $p->pipro_price;
                        
                        $pipro_qty = $p->pipro_qty;
                        
                        $cbm = @$p->product_cbm * $pipro_qty;
                        $subtotal = $pipro_price * $pipro_qty;
                        
                        $totalQty += $pipro_qty;
                        
                        $total_cbm += $cbm;
                        $total += $subtotal;
                        
                        $bpro = DB::table('buyer_products')
                            ->where('bpro_uid', $uid)
                            ->where('bpro_pid', $p->product_id)
                            ->first();
                        $piProduct = App\Models\PIProduct::where('pipro_pid', $p->product_id)
                            ->where('pipro_pi_id', $id)
                            ->first();
                        
                        if (!empty($piProduct)) {
                            $pipro_sku = !empty($piProduct->pipro_sku) ? $piProduct->pipro_sku : '';
                            $pipro_barcode = !empty($piProduct->pipro_barcode) ? $piProduct->pipro_barcode : '';
                            $pipro_buyer_desc = !empty($piProduct->pipro_buyer_desc) ? $piProduct->pipro_buyer_desc : '';
                        } else {
                            $pipro_sku = !empty($bpro->bpro_sku) ? $bpro->bpro_sku : '';
                            $pipro_barcode = !empty($bpro->bpro_barcode) ? $bpro->bpro_barcode : '';
                            $pipro_buyer_desc = !empty($bpro->bpro_description) ? $bpro->bpro_description : '';
                        }
                        
                        $pipro_fsc_status = $p->pipro_fsc_status;
                    @endphp
                    <tr>
                        <td>{{ ++$sn }}</td>
                        <td>
                            <input type="text" name="picart[{{ $p->product_id }}][bpro_sku]" value="{{ @$pipro_sku }}"
                                style="width: 100px" class="bpro_sku">
                        </td>
                        <td>
                            <input type="text" name="picart[{{ $p->product_id }}][bpro_barcode]"
                                value="{{ @$pipro_barcode }}" style="width: 100px" class="bpro_barcode">
                        </td>
                        <td width="70">
                            <img src="{{ url('imgs/products/' . @$p->product_image) }}" alt="{{ @$p->product_name }}"
                                style="width: 60px;">
                        </td>
                        <td>
                            <div class=""><strong>{{ @$p->product_name }}</strong></div>
                            <div class=""><small>{{ @$p->product_code }}</small></div>
                        </td>
                        <td>
                            <input type="text" name="picart[{{ $p->product_id }}][bpro_description]"
                                value="{{ @$pipro_buyer_desc }}" style="width: 100px" class="bpro_description">
                        </td>
                        <td class="product_cbm" data-val="{{ @$p->product_cbm }}">{{ @$p->product_cbm }}</td>
                        <td>
                            <input type="text" name="picart[{{ $p->product_id }}][pipro_price]"
                                value="{{ $pipro_price }}" style="width: 100px" class="pipro_price">
                        </td>
                        <td>
                            <input type="text" name="picart[{{ $p->product_id }}][pipro_qty]"
                                value="{{ $p->pipro_qty }}" class="pipro_qty" style="width: 100px">
                        </td>
                        <td>$<span class="subtotal">{{ $subtotal }}</span></td>
                        <td class="total_cbm"><span class="totalCBM">{{ number_format($cbm, 6) }}</span></td>
                        <td style="width: 150px;">
                            <select class="form-control" name="picart[{{ $p->product_id }}][pipro_fsc_status]"
                                style="width: 150px;">
                                <option value="">Select FSC Status</option>
                                <option @if (@$pipro_fsc_status == 'FSC 100%') selected @endif>FSC 100%</option>
                                <option @if (@$pipro_fsc_status == 'FSC Mix') selected @endif>FSC Mix</option>
                                <option @if (@$pipro_fsc_status == 'FSC Reclaimed') selected @endif>FSC Reclaimed</option>
                            </select>
                        </td>
                        <td style="width: 150px;">
                            <input type="text" name="picart[{{ $p->product_id }}][pipro_species]"
                                style="width: 150px;"
                                value="{{ !empty(@$pi_cart[$p->product_id]['pipro_species']) ? @$pi_cart[$p->product_id]['pipro_species'] : $p->pipro_species }}">
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="8">TOTAL</th>
                    <th><span class="totalQty">{{ $totalQty }}</span></th>
                    <th><span class="grandTotal">{{ $total }}</span></th>
                    <th><span class="grandTotalCBM">{{ number_format($total_cbm, 6) }}</span></th>
                    <th colspan="2">
                        <button type="button" class="btn btn-dark btn-sm btn-block btn-picart-update">Update</button>
                    </th>
                </tr>
            </tfoot>
        </table>
    </div>
@else
    <div class="no_records_found">
        No product(s) added yet.
    </div>
@endif
