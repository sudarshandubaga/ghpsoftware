@php
    $carts = session($cart_name);
    
    $products = [];
    if (!empty($carts)) {
        foreach ($carts as $pid => $data) {
            extract($data);
    
            $product = DB::table('products')
                ->where('product_id', $pid)
                ->first();
            $product->qty = $qty;
            $product->price = $price;
            $product->review = @$remark;
            $product->verified = @$verified;
            $product->LastPrice = @$LastPrice;
            $product->BuyerID = @$BuyerID;
            $products[] = $product;
        }
    }
@endphp

@php $sn = $totalQty = $total = $total_cbm = 0  @endphp
@if (!empty($products))
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>S.No.</th>
                    <th colspan="2">Item Description</th>
                    <th>Last Price</th>
                    <th>Price</th>
                    <th>Qty</th>
                    <th>Subtotal</th>
                    <th>Total CBM</th>
                    <!--<th>Remarks</th>-->
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $p)
                    @php
                        $cbm = $p->product_cbm * $p->qty;
                        $subtotal = $p->price * $p->qty;
                        
                        $totalQty += $p->qty;
                        $total_cbm += $cbm;
                        $total += $subtotal;
                    @endphp
                    <tr @if (!empty($p->verified) && $p->verified == 'Y') class="bg-success text-white" @endif>
                        <td>{{ ++$sn }}</td>
                        <td width="70">
                            <img src="{{ url('imgs/products/' . $p->product_image) }}" alt="{{ $p->product_name }}"
                                style="width: 60px;">
                        </td>
                        <td>
                            <div class=""><strong>{{ $p->product_name }}</strong></div>
                            <div class=""><strong>Code: </strong> {{ $p->product_code }}</div>
                            <div class=""><strong>CBM: </strong> {{ $p->product_cbm }}</div>
                        </td>
                        <td>{{ @$p->LastPrice }}</td>
                        <td>
                            <input type="number" name="cart[{{ $p->product_id }}][cart_price]"
                                value="{{ $p->price }}" class="form-control"
                                @if (!empty($p->verified) && $p->verified == 'Y') readonly @endif>
                        </td>
                        <td><input type="number" name="cart[{{ $p->product_id }}][cart_qty]"
                                value="{{ $p->qty }}" class="form-control"
                                @if (!empty($p->verified) && $p->verified == 'Y') readonly @endif></td>
                        <td>{{ $subtotal }}</td>
                        <td>{{ number_format($cbm, 6) }}</td>
                        <input type="hidden" name="cart[{{ $p->product_id }}][cart_review]"
                            value="{{ $p->review }}" class="form-control"
                            @if (!empty($p->verified) && $p->verified == 'Y') readonly @endif>
                        <td>
                            @if (empty($p->verified) || $p->verified == 'N')
                                <a href="#" class="remove_cart" data-id="{{ $p->product_id }}"
                                    data-url="{{ url('ajax/remove_cart/' . $cart_name) }}" title="Remove"
                                    data-toggle="tooltip"> <i class="icon-trash-o"></i> </a>
                            @else
                                Approved
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="5">TOTAL</th>
                    <th>{{ $totalQty }}</th>
                    <th>{{ $total }}</th>
                    <th>{{ number_format($total_cbm, 6) }}</th>
                    <th colspan="2">
                        <button type="button" class="btn btn-dark btn-sm btn-block btn-cart-update">Update</button>
                    </th>
                </tr>
                <!--<tr>
                    <th colspan="10">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Container</th>
                                    <th>Quantity</th>
                                </tr>
                            </thead>
                            
                        @php
                            $ST40HQ = 0;
                            $ST40SD = 0;
                            $ST20SD = 0;
                            $LCL = 0;
                            
                            $total_cbmC = ceil($total_cbm);
                            $ST40HQ = floor($total_cbmC / 68);
                            
                            $RemianingCBM = $total_cbm - $ST40HQ * 68;
                            
                            if ($RemianingCBM > 0) {
                                $ST40SD = floor($RemianingCBM / 58);
                                $RemianingCBM = $RemianingCBM - $ST40SD * 58;
                            }
                            
                            if ($RemianingCBM > 0) {
                                $ST20SD = floor($RemianingCBM / 28);
                                $RemianingCBM = $RemianingCBM - $ST20SD * 28;
                            }
                            
                            if ($RemianingCBM > 0) {
                                $LCL = floor($RemianingCBM / 15);
                                $RemianingCBM = $RemianingCBM - $LCL * 15;
                            }
                        @endphp

                            <tbody>
                                @if ($LCL > 0)
<tr>
                                    <td>LCL</td>
                                    <td>{{ $LCL }}</td>
                                </tr>
@endif

                                @if ($ST20SD > 0)
<tr>
                                    <td>20 ft. SD</td>
                                    <td>{{ $ST20SD }}</td>
                                </tr>
@endif

                                @if ($ST40SD > 0)
<tr>
                                    <td>40 ft. SD</td>
                                    <td>{{ $ST40SD }}</td>
                                </tr>
@endif

                                @if ($ST40HQ > 0)
<tr>
                                    <td>40 ft. HQ</td>
                                    <td>{{ $ST40HQ }}</td>
                                </tr>
@endif

                                <tr>
                                    <td>Remaining CBM</td>
                                    <td>{{ $RemianingCBM }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </th>
                </tr>-->
            </tfoot>
        </table>

        <input type="hidden" name="record[{{ $total_field }}]" value="{{ @$total }}">
        <input type="hidden" name="record[{{ $total_cbm_field }}]" value="{{ @$total_cbm }}">
    </div>
@else
    <div class="no_records_found">
        No product(s) added yet.
    </div>
@endif
