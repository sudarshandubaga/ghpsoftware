<html>

<head>
    <title></title>
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('icomoon/style.css') }}
    {{ HTML::style('css/style.css') }}
    <style>
        @page {
            margin: 0;
        }

        body {}

        * {
            font-size: 10px;
            color: #000;
            font-weight: 600;
        }

        .w150 {
            width: 150px;
        }

        .w250 {
            width: 250px;
        }

        .grey {
            color: #555;
        }

        .dt_grey_bg {
            background: #ff0;
            color: #f00;
            display: inline-block;
            padding: 2px;
        }

        .print_page {
            width: 270mm;
            padding: 15px;
            box-sizing: border-box;
            /* border: 1px solid #000; */
            margin: auto;
            color: #000;
        }

        .print_table {
            width: 100%;
        }

        .print_table th,
        .print_table td {
            border: 1px solid #000;
            padding: 5px;
        }

        .logo {
            height: 80px;
        }

        .invoice_block {
            border: .5mm solid #000;
            border-radius: 5px;
            padding: 5px;
            overflow: hidden;
            color: #f00;
            background: #81ecec;
        }

        .invoice_block span {
            display: block;
            float: left;
            background: #ccc;
            margin: -5px 0 -5px -5px;
            padding: 10px;
            color: #fff;
        }

        .product_row * {
            font-size: 8px;
            font-weight: bold;
        }

        .product_row.product_heading * {
            text-align: center;
        }

        .product_row th,
        .product_row td {
            padding: 2px;
        }

        .footer_row td {
            border-right: 0;
            border-left: 0;
            border-bottom: 0;
        }

        @media print {
            @page {
                size: landscape
            }

            .print_page {
                margin: 0;
                width: 100%;
            }
        }
    </style>
</head>

<body style="background: #FFFFFF">
    <div class="print_page">
        <table class="print_table">
            <tr>
                <td colspan="22">
                    <div class="float-left">
                        <img src="{{ url('imgs/logo1.png') }}" alt="GHP" class="logo">
                    </div>
                    <div class="float-right" style="padding-top: 15px;">
                        <div class="">
                            <strong>Global Home Products Pte. Ltd.</strong><br>
                            20 CECIL STREET, <br>
                            #05 - 03 PLUS,
                            SINGAPORE (049705)<br>
                            REGT. Nr. 201810833W
                        </div>
                    </div>
                    <div class="commercial-invoice float-right">
                        COMMERCIAL INVOICE
                    </div>

                </td>
            </tr>
            <tr>
                <td colspan="22">
                    <div class="clearfix">
                        <div class="float-left" style="width: 30%;">
                            <div>CONSIGNEE NAME AND ADDRESS</div><br>
                            <div class="">
                                <div class="">
                                    <strong>{{ $PIDATA->user_name }}</strong><br>
                                    {{ nl2br($PIDATA->user_address) }}<br>
                                    {{ $PIDATA->state_name }}, {{ $PIDATA->country_name }}
                                </div>
                            </div>
                        </div>
                        <div class="float-left" style="width: 30%;">
                            <div>BUYER NAME AND ADDRESS</div><br>
                            <div class="">
                                <div class="">
                                    <strong>{{ $PIDATA->user_name }}</strong><br>
                                    {{ nl2br($PIDATA->user_address) }}<br>
                                    {{ $PIDATA->state_name }}, {{ $PIDATA->country_name }}
                                </div>
                            </div>
                        </div>
                        <div class="float-right" style="width: 40%;">
                            <div>VENDOR CODE 712345</div>
                            <p><br>&nbsp;<br></p>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="text-center" colspan="4">INVOICE NUMBER</td>
                <td class="text-center" colspan="2">INVOICE DT.</td>
                <td class="text-center" colspan="3">PI/Order NUMBER</td>
                <td class="text-center" colspan="3">PI/PO DATE</td>
                <td class="text-center" colspan="4">CONTIANER NUMBER</td>
                <td class="text-center" colspan="3">CONTAINER TYPE</td>
                <td class="text-center" colspan="3">VESSEL / VOYAGE</td>
            </tr>
            <tr>
                <td class="text-center" colspan="4">
                    <strong>{{ $GHPInvoiceData->exs_ghp_invoice_invoice_no }}</strong> </td>
                <td class="text-center" colspan="2">
                    <b>{{ date('d-m-Y', strtotime($GHPInvoiceData->exs_ghp_invoice_invoce_date)) }}</b></td>
                <td class="text-center" colspan="3"><strong
                        style="font-size: 8px;">1508{{ str_pad($PODATA->po_id, 2, '0', STR_PAD_LEFT) }}</strong></td>
                <td class="text-center" colspan="3"><b>{{ date('d-M-Y', strtotime($PODATA->po_date)) }}</b></td>
                <td class="text-center" colspan="4"><strong>{{ $GetInvoiceData->po_invoice_container }}</strong>
                </td>
                <td class="text-center" colspan="3"><b>{{ $GetInvoiceData->po_invoice_container_size }}</b></td>
                <td class="text-center" colspan="3"><strong>{{ $GetInvoiceData->po_invoice_voyage }}</strong></td>
            </tr>

            <tr>
                <td class="text-center" colspan="4">PRE-CARRIAGE BY</td>
                <td class="text-center" colspan="3">PLACE OF RECEIPT</td>
                <td class="text-center" colspan="4">PORT OF LOADING</td>
                <td class="text-center" colspan="4">PORT OF DISCHARGE</td>
                <td class="text-center" colspan="4">FINAL DESTINATION</td>
                <td class="text-center" colspan="3">COUNTRY OF ORIGIN</td>
            </tr>
            @php
                $GetDataPortCountry = DB::table('ports')
                    ->where('port_name', $GHPInvoiceData->exs_ghp_invoice_place_receipt)
                    ->first();
                $GetLiveCountry = DB::table('countries')
                    ->where('country_id', $GetDataPortCountry->port_country)
                    ->first();
            @endphp
            <tr>
                <td class="text-center" colspan="4">
                    <strong>{{ $GHPInvoiceData->exs_ghp_invoice_pre_carriage }}</strong> </td>
                <td class="text-center" colspan="3"><b>{{ $GHPInvoiceData->exs_ghp_invoice_place_receipt }},
                        {{ $GetLiveCountry->country_short_name }}</b></td>
                @php
                    $GetDataPortCountry = DB::table('ports')
                        ->where('port_name', $PIDATA->quote_loading_port)
                        ->first();
                    $GetLiveCountry = DB::table('countries')
                        ->where('country_id', $GetDataPortCountry->port_country)
                        ->first();
                @endphp

                <td class="text-center" colspan="4"><strong
                        style="font-size: 8px;">{{ $PIDATA->quote_loading_port }},
                        {{ $GetLiveCountry->country_short_name }}</strong></td>

                @php
                    $GetDataPortCountry = DB::table('ports')
                        ->where('port_name', $PIDATA->quote_discharge_port)
                        ->first();
                    $GetLiveCountry = DB::table('countries')
                        ->where('country_id', $GetDataPortCountry->port_country)
                        ->first();
                @endphp

                <td class="text-center" colspan="4"><b>{{ $PIDATA->quote_discharge_port }},
                        {{ $GetLiveCountry->country_short_name }}</b></td>
                <td class="text-center" colspan="4"><strong>GERMANY</strong></td>
                <td class="text-center" colspan="3"><b>{{ $PIDATA->quote_origin_country }}</b></td>
            </tr>

            <tr>
                <td class="text-center" colspan="4">SHIPMENT TERM</td>
                <td class="text-center" colspan="3">PAYMENT TERM</td>
                <td class="text-center" colspan="3">CURRENCY</td>
                <td class="text-center" colspan="6">COUNTRY OF FINAL DESTINATION</td>
                <td class="text-center" colspan="6">
                    <img src="{{ URL('imgs/fsc.png') }}" width="300">
                </td>
            </tr>
            <tr>
                <td class="text-center" colspan="4"><strong>FOB - MUNDRA,IN</strong> </td>
                <td class="text-center" colspan="3">
                    @php
                        $RemaingAmount = 100 - $PIDATA->quote_payment;
                    @endphp
                    <strong style="font-size: 8px;">
                        @if ($PIDATA->quote_payment > 0)
                            {{ $PIDATA->quote_payment }}% Advance &
                        @endif

                        {{ $RemaingAmount }}% T.T. AGAINST MAIL COPY OF DOCS
                </td>
                <td class="text-center" colspan="3"><strong style="font-size: 8px;">US DOLLAR</strong></td>
                <td class="text-center" colspan="12"><b> {{ $PIDATA->quote_first_destination_country }}</b></td>
            </tr>




            <tr class="product_row product_heading" style="background: #e2dbdb;">
                <th class="text-center">Sr.No.</th>
                <th class="text-center" colspan="2">Case No.</th>
                <th class="text-center">Item<br>Code</th>
                <th class="text-center" colspan="2">SKU No.</th>
                <th class="text-center">BARCODE</th>
                <th class="text-center" colspan="4">PRODUCT <br> DESCRIPTION</th>
                <th class="text-center">MATERIAL <br>USED</th>
                <th class="text-center">FSC Status</th>
                <th class="text-center">QTY in<br>USED</th>
                <th class="text-center">RATE<br>(USD)</th>
                <th class="text-center">AMMOUNT<br>(USD)</th>
                <th class="text-center">HS<br>CODE</th>
                <th class="text-center">TOTAL N.<br>Wt.<br>(IN KGS.)</th>
                <th class="text-center">TOTAL Gr.<br>Wt.<br>(IN KGS.)</th>
                <th class="text-center">M3<br>PER<br>Pc.</th>
                <th class="text-center">TOTAL<br>M3</th>
                <!-- <th style="background: #ff0;">NW/Pc.</th>
                   <th style="background: #ff0;">GW/Pkt.</th> -->
            </tr>

            @php
                $TotalAmount = 0;
                $TotalQTY = 0;
                $TotalCBM = 0;
            @endphp

            @foreach ($InvoiceData as $SRN => $InvoicePcs)
                @php
                    $GetPOData = \App\Models\POModel::find($InvoicePcs->invoice_po_no);
                    $GetProductDtail = \App\Models\POProduct::where('popro_po_id', $GetInvoiceData->po_invoice_po)
                        ->where('popro_pid', $InvoicePcs->exs_po_invoice_products_product_id)
                        ->first();
                    $GetPIProductDtail = \App\Models\PIProduct::leftjoin('products AS p', 'p.product_id', 'pipro_pid')
                        ->leftjoin('quote_products AS qpro', 'pipro_pid', 'qpro_pid')
                        ->where('pipro_pi_id', $GetPOData->po_pi_id)
                        ->where('pipro_pid', $InvoicePcs->exs_po_invoice_products_product_id)
                        ->first();
                    
                    if (isset($GetPIProductDtail->qpro_price)) {
                        $TotalAmount += $InvoicePcs->pip_qty * $GetPIProductDtail->qpro_price;
                    }
                    
                    $TotalQTY += $InvoicePcs->pip_qty;
                    $TotalCBM += $InvoicePcs->pip_TotalMeas;
                @endphp

                <tr class="product_row">
                    <td class="text-center">{{ $SRN + 1 }}</td>
                    <td class="text-center" colspan="2">{{ $InvoicePcs->pip_case_no }}</td>
                    <td class="text-center">{{ $InvoicePcs->product_code }}</td>
                    <td class="text-center" colspan="2">{{ @$GetProductDtail->popro_sku }}</td>
                    <td>{{ @$GetProductDtail->popro_barcode }}</td>
                    <td><img src="{{ url('imgs/products/' . $InvoicePcs->product_image) }}" alt=""
                            style="width: 70px; height: 50px; object-fit: contain;"></td>
                    <td colspan="3">
                        {{ $InvoicePcs->product_name }}<br>
                        {{ $InvoicePcs->product_type }} Version<br>
                        {{ $InvoicePcs->product_mp }}xCarton<br>
                        {{ @$InvoicePcs->product_material_wood }}
                        <u>Packing Dimension</u><br>
                        {{ $InvoicePcs->pip_PkgDimL }} L x {{ $InvoicePcs->pip_PkgDimW }} W x
                        {{ $InvoicePcs->pip_PkgDimH }} H
                    </td>
                    <td class="text-center">
                        {{ @$GetPIProductDtail->product_material_wood }}
                        @if (@$GetPIProductDtail->product_metal != '' && @$GetPIProductDtail->product_metal != 'None')
                            <br>{{ $GetPIProductDtail->product_metal }}
                        @endif
                    </td>
                    <td class="text-center">
                        {{ @$GetPIProductDtail->pipro_fsc_status == '' ? '-' : @$GetPIProductDtail->pipro_fsc_status }}
                    </td>
                    <td class="text-center">{{ $InvoicePcs->pip_qty }}</td>
                    <td class="text-center">{{ number_format(@$GetPIProductDtail->qpro_price, 2) }}</td>
                    <td class="text-center">
                        {{ number_format(@$InvoicePcs->pip_qty * @$GetPIProductDtail->qpro_price, 2) }}</td>
                    <td class="text-center">{{ $InvoicePcs->pip_HSCode }}</td>
                    <td class="text-center">{{ round($InvoicePcs->pip_TotalNetWeightKG, 2) }}</td>
                    <td class="text-center">{{ round($InvoicePcs->pip_TotalGrossWeight, 2) }}</td>

                    <td class="text-center">
                        @php
                            $AB = json_decode($InvoicePcs->pip_Meas);
                        @endphp

                        {{ implode(',', $AB) }}
                    </td>
                    <td class="text-center">{{ number_format($InvoicePcs->pip_TotalMeas, 2) }}</td>
                </tr>
            @endforeach

            <tr>
                <td colspan="22" style="background: #e2dbdb;"></td>
            </tr>
            <div style="border: 1px solid #000">
                <tr>
                    <td class="border-none text-center " colspan="4">TOTAL QTY</td>
                    <td class="border-none text-center " colspan="4">TOTAL CBM</td>
                    <td class="border-none text-center " colspan="4">NET INVOICE VALUE</td>
                    <td class="border-none text-center " colspan="5">Less : ADVANCE PAID</td>
                    <td class="border-none text-center " colspan="5">BALANCE DUE AMOUNT</td>
                </tr>
                @php
                    $Advance = ($TotalAmount * $PIDATA->quote_payment) / 100;
                @endphp
                <tr>
                    <td class="border-none text-center" colspan="4"><strong>{{ $TotalQTY }} " Pcs./
                            Set"</strong> </td>
                    <td class="border-none text-center" colspan="4"><b>{{ round($TotalCBM, 2) }}</b></td>
                    <td class="border-none text-center" colspan="4"><strong style="font-size: 8px;">$
                            {{ number_format($TotalAmount, 2) }}</strong></td>
                    <td class="border-none text-center" colspan="5"><b>$ {{ number_format($Advance, 2) }}</b></td>
                    <td class="border-none text-center" colspan="5"><b>$
                            {{ number_format($TotalAmount - $Advance, 2) }}</b></td>
                </tr>
            </div>
            <tr>
                <td colspan="22" style="background: #e2dbdb;"><U>TOTAL AMMOUNT (IN WORD)</U> : USD FOURTEEN THOUSAND
                    THREE HUNDRED THIRTY FIVE</td>
            </tr>
            <!--
               <tr>
                   <td colspan="15" style="background: #000; color: #fff; text-decoration: underline;">BANK DETAILS FOR REMITTANCE</td>
                   <td colspan="7" rowspan="4" valign="top" class="text-center">
                       For: Global Home Products Pte. Ltd.
                   </td>
               </tr> -->

            <tr>
                <td colspan="15">
                    <b><u>Declaration: "STATEMENT ON ORIGIN"</u></b><br><br>
                    <br>
                    {{ $GetInvoiceData->po_invoice_statement_origin }} <br><br>
                    <!--<p>-->
                    <!--   The Exporter INDIANART FURNITURES PVT. LTD. & REX No. INREX1312000171EC024 of  the  products  covered  by  this<br>  document declares that, except where otherwise clearly indicated, these products are of Preferential origin<br> INDIAN according to rules of origin of the Generalised System of Preferences of the European Union and that the<br> origin of the Generalised System of Preferences of the European Union and that the origin criterion met is "P" ( 94036000 )                         -->
                    <!--</p>-->
                </td>
                <td colspan="7">
                    <strong>FOR <u>GLOBAL HOME PRODUCT PTE. LTD.</u></strong><br>
                    <br><br>
                    <br><br>

                    AUTHORISED SIGNATORY
                </td>
            </tr>
        </table>
    </div>
    <div class="print_page print_terms" style="page-break-before: always;">
        <table class="print_table">
            <tr>
                <td colspan="15">
                    <img src="{{ url('imgs/logo1.png') }}" alt="GHP" class="logo">
                </td>
                <td colspan="7" style="text-align: right;">
                    <strong>Global Home Products Pte. Ltd.</strong><br>
                    20 CECIL STREET,<br>
                    #05 - 03 PLUS,<br>
                    SINGAPORE (049705)<br>
                    REGT. Nr. 201810833W
                </td>
            </tr>

            <tr>
                <td class="text-center" colspan="22">Bank details of company for remittance of payment in <span
                        style="color: #3955dc">US Dollar</span></td>
            </tr>

            <tr>
                <td colspan="22">
                    <h3>Company Information</h3>
                </td>
            </tr>

            <tr>
                <td colspan="11"><b>Company Name</b></td>
                <td colspan="11">GLOBAL HOME PRODUCTS PTE. LTD.</td>
            </tr>
            <tr>
                <td colspan="11"><b>Full Address</b></td>
                <td colspan="11">
                    <!--1, RAFFLES PLACE,<br>-->
                    20 CECIL STREET,#05 - 03 PLUS,<br>
                    SINGAPORE (049705)
                </td>
            </tr>
            <tr>
                <td colspan="11"><b>Bank Account Number</b></td>
                <td colspan="11">503434599301</td>
            </tr>
            <tr>
                <td colspan="22">
                    <h3>Bank Information</h3>
                </td>
            </tr>
            <tr>
                <td colspan="11"><b>Bank Name</b></td>
                <td colspan="11">OCBC Bank</td>
            </tr>
            <tr>
                <td colspan="11"><b>Bank Address</b></td>
                <td colspan="11">65, Chulia Street,<br>
                    #11-01 OCBC Centre East<br>
                    Singapore ( 049513 )</td>
            </tr>
            <tr>
                <td colspan="11"><b>Bank Swift Code</b></td>
                <td colspan="11">OCBCSGSG</td>
            </tr>
            <tr>
                <td colspan="22">
                    <h3>Intermediary Bank Information</h3>
                </td>
            </tr>
            <tr>
                <td colspan="11"><b>Bank Name</b></td>
                <td colspan="11">JP Morgan Chase Bank</td>
            </tr>
            <tr>
                <td colspan="11"><b>Bank Address</b></td>
                <td colspan="11">New York,<br>USA</td>
            </tr>
            <tr>
                <td colspan="11"><b>Bank Swift Code</b></td>
                <td colspan="11">CHASUS33</td>
            </tr>
        </table>
    </div>
    {{ HTML::script('js/jquery.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/owl.carousel.min.js') }}
    {{ HTML::script('js/popper.min.js') }}
    {{ HTML::script('js/custom.js') }}
</body>

</html>
