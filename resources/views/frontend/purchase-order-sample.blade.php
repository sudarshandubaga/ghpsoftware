<html>

<head>
    <title>{{ $title }}</title>
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('icomoon/style.css') }}
    {{ HTML::style('css/style.css') }}
    <style>
        @page {
            margin: 0;
        }

        body {}

        * {
            font-size: 10px;
            color: #000;
            font-weight: 600;
        }

        .w150 {
            width: 150px;
        }

        .w250 {
            width: 250px;
        }

        .grey {
            color: #555;
        }

        .dt_grey_bg {
            background: #ff0;
            color: #f00;
            display: inline-block;
            padding: 2px;
        }

        .print_page {
            width: 270mm;

            padding: 15px;
            box-sizing: border-box;
            /* border: 1px solid #000; */
            margin: auto;
            color: #000;
        }

        .print_table {
            width: 100%;
        }

        .print_table th,
        .print_table td {
            border: 1px solid #000;
            padding: 5px;
        }

        .logo {
            height: 80px;
        }

        .invoice_block {
            border: .5mm solid #000;
            border-radius: 5px;
            padding: 5px;
            overflow: hidden;
            color: #f00;
            background: #81ecec;
        }

        .invoice_block span {
            display: block;
            float: left;
            background: #ccc;
            margin: -5px 0 -5px -5px;
            padding: 10px;
            color: #fff;
        }

        .product_row * {
            font-size: 8px;
            font-weight: bold;
        }

        .print_terms * {
            font-size: 7px;
        }

        .product_row.product_heading * {
            text-align: center;
        }

        .product_row th,
        .product_row td {
            padding: 2px;
        }

        .footer_row td {
            border-right: 0;
            border-left: 0;
            border-bottom: 0;
        }

        .vertical-text {
            writing-mode: tb-rl;
            text-align: center;
            margin: auto;
            white-space: nowrap;
        }

        @media print {
            @page {
                size: landscape
            }

            .print_page {
                margin: 0;
                width: 100%;
            }
        }
    </style>
</head>

<body style="background: #FFFFFF">

    <div class="print_page">
        <table class="print_table">
            <tr>
                <td colspan="15">
                    <div class="row">
                        <div class="col">
                            <img src="{{ url('imgs/logo1.png') }}" alt="" class="logo">
                        </div>
                        <div class="col text-center" style="font-size: 28px; padding: 20px 15px; color: #999;">
                            PURCHASE ORDER QC
                        </div>
                        <div class="col text-right" style="padding: 12px 15px;">
                            20 CECIL STREET,<br>
                            #05 - 03 PLUS, <br>
                            SINGAPORE (049705)

                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th colspan="2"><strong>VENDOR NAME:</strong></th>
                <td colspan="13">{{ $record->user_name }}</td>
            </tr>
            <tr>
                <th colspan="2" rowspan="2"><strong>ADDRESS:</strong></th>
                <td colspan="9" rowspan="2">
                    {{ $record->vendor_address }}
                </td>
                <th colspan="2"><strong>DATE</strong></th>
                <td colspan="2" class="text-center">{{ date('d-M-Y', strtotime($record->po_date)) }}</td>
            </tr>
            <tr>
                <th colspan="2"><strong>GHP PO#</strong></th>
                <td colspan="2" class="text-center">2021-{{ str_pad($record->po_id + 100, 3, '0', STR_PAD_LEFT) }}</td>
            </tr>
            <tr>
                <th colspan="2"><strong>CITY:</strong></th>
                <td colspan="9">{{ $record->vendor_city }}</td>
                <th colspan="2"><strong>GHP CONTACT</strong></th>
                <td colspan="2" class="text-center">info@ghp-sg.com</td>
            </tr>
            <tr>
                <th colspan="2"><strong>State / Rigion:</strong></th>
                <td colspan="3">{{ $record->vendor_state }}</td>
                <th colspan="2">
                    <!--<strong>COUNTRY:</strong>-->
                </th>
                <td colspan="4">
                    <!--{{ $record->country_name }}-->
                </td>
                <th colspan="2"><strong>ETD</strong></th>
                <td colspan="2" class="text-center" style="background: #fff;">
                    {{ date('d-M-Y', strtotime($record->po_delivery_days)) }}</td>
            </tr>

            <tr>
                <th colspan="2"><strong>PHONE:</strong></th>
                <td colspan="9">{{ $record->vendor_contact_mobile1 }}</td>
                <th colspan="2"><strong>POL</strong></th>
                <td colspan="2" class="text-center">{{ $record->po_delivery_port }}</td>
            </tr>
            <tr>
                <th colspan="2"><strong>Vendor Code:</strong></th>
                <td colspan="9">{{ $record->vendor_code }}</td>
                <th colspan="2"><strong>POD</strong></th>
                <td colspan="2" class="text-center">TO BE ADVISED</td>
            </tr>
            <tr>
                <th colspan="11" rowspan="5">
                    <strong>Consignee / Delivery TO:</strong><br>
                    {{ @$PIData->buyer_code }}
                </th>
                <th rowspan="2" colspan="2"><strong>TERM OF PAYMEMT</strong></th>
                <td rowspan="2" colspan="2" class="text-center">
                    <!--{{ @$record->po_payment }}% Advance And {{ 100 - @$record->po_payment }}% T.T. AGAINST MAIL COPY OF DOCS-->
                    TO BE ADVISED
                </td>
            </tr>
            <tr>

            </tr>

            @php
                $GetDataPortCountry = DB::table('ports')
                    ->where('port_name', $record->po_delivery_port)
                    ->first();
                
                $GetLiveCountry = DB::table('countries')
                    ->where('country_id', $GetDataPortCountry->port_country)
                    ->first();
            @endphp

            <tr>
                <th colspan="2"><strong>TERM OF DELIVERY</strong></th>
                <td colspan="2" class="text-center">
                    {{ $record->po_price_term }} - {{ $record->po_delivery_port }},
                    {{ $GetLiveCountry->country_short_name }}
                </td>
            </tr>
            <tr>
                <th colspan="2"><strong>CONTAINER TYPE</strong></th>
                <td colspan="2" class="text-center">
                    {{ $record->container_type }}
                </td>
            </tr>
            <tr>
                <th colspan="2"><strong>FORWARDER</strong></th>
                <td colspan="2" class="text-center">
                    TO BE ADVISED
                </td>
            </tr>

            <tr class="product_row product_heading">
                <th>ORDER REF. NO.</th>
                <th>GHP ITEM NO.</th>
                <th>BUYER ITEM NO.</th>
                <th>EAN CODE</th>
                <th>FSC Status</th>
                <th>ITEM PIC</th>
                <th style="width:200px">Description</th>
                <th>IC/OC</th>
                <th>CBM PER BOX</th>
                <th>ORDER QTY</th>
                <th>UNIT</th>
                <th>TOTAL CBM</th>
                <th>PRICE PER UNIT</th>
                <th>GROSS VALUE</th>
                <th>CURRENCY</th>
            </tr>
            @php
                $TotalCBM = 0;
                $TotalQTY = 0;
                $TotalAmt = 0;
            @endphp

            @foreach ($products as $p)
                @php
                    $WoodFinish = DB::table('finish')
                        ->where('finish_code', $p['product_material_wood_value'])
                        ->first();
                    $MetalFinish = '';
                    
                    if ($p['product_material_metal_value'] != '' && $p['product_material_metal_value'] != 'None') {
                        $MetalFinish = DB::table('finish')
                            ->where('finish_code', $p['product_material_metal_value'])
                            ->first();
                    }
                    
                    $GetProductSizel = [];
                    $GetProductSizew = [];
                    $GetProductSizeh = [];
                    
                    if ($p->product_mp_carton_l_cm != '0') {
                        $GetProductSizel = unserialize($p->product_mp_carton_l_cm);
                    }
                    
                    if ($p->product_mp_carton_w_cm != '0') {
                        $GetProductSizew = unserialize($p->product_mp_carton_w_cm);
                    }
                    if ($p->product_mp_carton_h_cm != '0') {
                        $GetProductSizeh = unserialize($p->product_mp_carton_h_cm);
                    }
                    
                    $SubTotal = $p->popro_price * $p->popro_qty;
                    $SubCBM = $p->product_cbm * $p->popro_qty;
                    
                    $TotalCBM += $p->product_cbm * $p->popro_qty;
                    $TotalQTY += $p->popro_qty;
                    $TotalAmt += $p->popro_price * $p->popro_qty;
                    // dd($PIData->pi_order_ref);
                @endphp
                <tr>
                    <td class="text-center">

                        @if (!empty($PIData->pi_order_ref))
                            {{ $PIData->pi_order_ref }}
                        @else
                            GHP-202122-{{ sprintf('%03d', $record->po_pi_id + 100) }}
                        @endif
                    </td>
                    <td class="text-center">
                        <div class="vertical-text"> {{ $p->product_code }} </div>
                    </td>
                    <td class="text-center">
                        <div class="vertical-text"> {{ $p->popro_sku }} </div>
                    </td>
                    <td class="text-center">
                        <div class="vertical-text"> {{ $p->popro_barcode }} </div>
                    </td>
                    <td class="text-center">-</td>
                    <td><img src="{{ url('imgs/products/' . $p->product_image) }}" alt=""
                            style="width: 70px; height: 50px; object-fit: contain;"></td>
                    <td>
                        {{ $p->product_name }}<br>
                        {{ $p->product_type }} Version<br>
                        {{ $p->product_mp }}xCarton<br>
                        {{ $p->product_material_wood }}
                        @php
                            $Matrl1 = DB::table('material_type')
                                ->where('material_type_id', $p['materialtype1'])
                                ->first();
                            $Matrl2 = DB::table('material_type')
                                ->where('material_type_id', $p['materialtype2'])
                                ->first();
                            $WoodFinish = DB::table('finish')
                                ->where('finish_id', $p['finish1'])
                                ->first();
                            $WoodFinish1 = DB::table('finish')
                                ->where('finish_id', $p['finish2'])
                                ->first();
                        @endphp

                        {{ @$Matrl1->material_type_name }}/
                        {{ @$WoodFinish->finish_title }}<br>
                        {{ @$Matrl2->material_type_name }}/
                        {{ @$WoodFinish1->finish_title }}<br>
                        <strong>Product Dimension</strong> <br>
                        {{ $p->product_l_cm }}cm x {{ $p->product_w_cm }}cm x {{ $p->product_h_cm }}cm<br>
                        <strong>Packing Dimension</strong> <br>
                        {{ implode(" /\n", $GetProductSizel) }}cm x {{ implode(" /\n", $GetProductSizew) }}cm x
                        {{ implode(" /\n", $GetProductSizeh) }}cm

                    </td>
                    <td>{{ $p->product_ip }}/{{ $p->product_mp }}</td>
                    <td>{{ number_format($p->product_cbm, 3) }}</td>
                    <td>{{ $p->popro_qty }}</td>
                    <td>Pcs.</td>
                    <td>{{ number_format($SubCBM, 3) }}</td>
                    <td>$0</td>
                    <td>$0</td>
                    <td>USD</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="9"></td>
                <td>{{ $TotalQTY }}</td>
                <td></td>
                <td>{{ number_format($TotalCBM, 3) }}</td>
                <td></td>
                <td>$o</td>
                <td></td>
            </tr>
            <tr>
                <td style="height: 100px; vertical-align: top;" colspan="15">
                    <strong>SHIPPING MARKS PROVIDED IN SEPARATE MAIL</strong>
                    <br>Add min. 2 silica Gel DMF Free into cartoon box.
                    <br>max. 12% moisture
                    <br>FSC/SVHC-REACH LOG required before Mass Productional
                </td>
            </tr>
        </table>
    </div>
    <div class="print_page print_terms" style="page-break-before: always;">
        <table class="print_table">
            <tr>
                <td colspan="15" class="print_boreder_none" style="font-size: 10px;">
                    Pre-Shipment Inspection required 385 /18<br>
                    At least 2 weeks before confirmed ETD delivery date, please advise the following information by
                    email to <strong><u>INFO@GHP-SG.COM</u></strong> for each item:<br>
                    1. Estimated inspection date (goods shall be 100% completed and >80% packed)<br>
                    2. Factory name and street address in English<br>
                    3. Inspection contact person, mobile phone no. & email address<br>
                    The inspection date requests have to be made at least 5 days before your desired date!<br>
                    The inspection report will be available on the next working day after the inspection. It will be
                    reviewed by the responsible buyer & quality management and you will be informed as soon<br>
                    as soon as possible concerning <strong><u>GHP</u></strong> acceptance of the goods.<br>
                    The goods shall not be loaded until the buyer’s confirmation has been received!<br>
                    We require an order acknowledgment for the following items:<br><br>

                    After receipt of the order, please make sure that it is treated with the adequate attention and that
                    our packing instructions for the inner carton (IC) and outer carton (OC) are adhered to.<br><br>

                    Please return scan copy of order stamped and signed on each side within 2 business days to
                    <strong><u>INFO@GHP-SG.COM</u></strong><br></br>

                    Please take notice that the adherence of the guidelines on contractual arrangements (general
                    guidelines on quality, packing and logistic delivery conditions as well as the general terms<br>
                    of purchase) is prerequisite for the contractual fulfilment. It is within your responsibility to
                    make sure that we are in possession of all the mentioned contracts signed.<br><br>

                    In label description you find necessary specifications required for the labelling of the ordered
                    articles. As for the package labelling, please refer only to the attachments you<br>
                    received from us when placing the order together with the EAN code of the order (any other
                    adjustments are not permitted).<br><br>

                    Furthermore, we ask you to make sure that the following will be transmitted no later than 2 weeks
                    before delivery<br>
                    1. Shipping Marks<br>
                    2. Swatches of Wooden & Metal Part<br>
                    3. If necessary: requested certificates for each ordered article<br>
                    4. HD Professional Images of the ordered article<br>
                    For the delivery, we ask you to use exclusively the templates for invoices and packing list we sent
                    to you. The data specified in the order has to be filled in.<br><br>
                    <strong><u>TERMS AND CONDITIONS -</u></strong><br><br>
                    <strong><u>1 Payment</u></strong><br>
                    <!---{{ @$record->po_payment }}% Advance And {{ 100 - @$record->po_payment }}%   Balance T/T Payment after mail copy of Post-shipment Docs (CI, PL, BL, FC, GSP & Other documents etc.)<br><br>-->
                    TO BE ADVISED<br>
                    <strong><u>2 Quality Standard</u></strong><br>
                    The goods has to be fulfil our quality standard as explained.<br><br>
                    <strong><u>3 Delivery</u></strong><br>
                    Supplier shall deliver the Goods at the delivery point and on the date specified in this Order
                    (Estimate Time Delivery)<br><br>
                    <strong><u>4 Inspection</u></strong><br>
                    - GHP will send quality Controller to check raw material, production process and shipment.<br>
                    - GHP will invite third party QC Inspector for as Buyer requirement<br>
                    - The goods will be inspected by third party if its ready for finish goods 100% and packed 80%<br>
                    - The goods failed by GHP QC must be repaired or replaced timely<br>
                    - The goods failed by GHP QC must be repaired or replaced, and also after third party inspection,
                    The fail goods must be repaired or replaced with no additional cost.<br><br>
                    - The goods can only be shipped after QC passed.<br><br>
                    - failed inspection: In case of second inspection or if there is a need for second QC inspection
                    Thethis inspection deducted from your invoice.<br><br>
                    <strong><u>5 Master sample checking</u></strong><br>
                    The Supplier has to prepare master sample to be a guidance for all requirements<br>
                    <strong><u>6 Packaging</u></strong><br>
                    - Each individual part of The item has to be packed In mail order packaging<br>
                    - The goods must have to be packed in 5 ply carton box and strong enough<br>
                    - Corrugated paper/single face to protect The item inside The packing<br>
                    - Silica gel is a must to absorb The humidity In The carton box<br>
                    - Put Corner protector foam<br><br>
                    - a perfect packing is very important requirements for mail order business,<br>
                    The final packing has to be confirmed by GHP In all its detail<br>
                    <strong><u>7 Shipping Marks</u></strong><br>
                    Shipping Marks will be provided separately for each Order. Please note that a penalty of 200 US$
                    will be charged in case of wrong shipping Marks.<br><br>
                    <strong><u>8 Wood Declaration</u></strong><br>
                    The New EU – Regulation EU 995/2010 implies that the supplier has to offer SVLK/FSC certificates for
                    every kind of wood which is used for the item to prove that<br>
                    wood is from legal origin.<br><br>
                    <strong><u>9 Penalties in case of non-fulfillment of T&C:</u></strong><br>
                    - Penalty for 1 week delay is 5%<br>
                    - Penalty for 2 weeks delay is 10%<br>
                    - More than 2 weeks delay, cancel order and return DP<br><br>
                    <strong><u>10 Confidentiality</u></strong><br>
                    - by signing this contract, Supplier agrees to abide by The Terms and Conditions stated herein.<br>
                    The Supplier is also bound that it shall not establish direct or indirect contact with our (GHP)
                    buyers without our consent.<br>
                    If by any means we come to know, the supplier will be liable to pay 50,000 EURO as a
                    penalty.<br><br>
                    All disputes are subject to Singapore's Law.
                </td>
            </tr>
            <tr>
                <td class="print_boreder_none" rowspan="2" colspan="10">
                    <div style="width: 150mm;">

                    </div>
                </td>
                <td class="print_boreder_none" colspan="5" style="border-bottom: 0;"><strong
                        style="font-size: 10px;">Read & Signed by:</strong></td>
            </tr>
            <tr>
                <td class="print_boreder_none" colspan="5" valign="bottom"
                    style="border-top: 0; width: 200px; height: 100px; font-size: 10px;">
                    Signature alog-with Factory Official Seal<br>
                    Date:
                </td>
            </tr>
        </table>
    </div>
</body>

</html>
