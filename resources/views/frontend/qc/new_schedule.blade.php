<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>New Inspection</h1>
            </div>
        </div>
    </div>
</section>

<form>
<div class="card mb-5 mt-5">
    <h3 class="card-title">Filter PO</h3>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label>PO Number</label>
                <input type="text" name="SearchPONumber" value="{{ @$_GET['SearchPONumber'] }}" class="form-control">
            </div>
        </div>
        
        <div class="col">
            <div class="form-group">
                <label>&nbsp;</label>
                <button type="submit" class="btn btn-primary form-control">Search</button>
            </div>
        </div>
    </div>
</div>
</form>

<div class="container-fluid">
    <div class="card">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>PO Number</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($records as $rec)
            <tr>
                <td>{{ $rec->po_id }}</td>
                <td>
                    <div class="mb-1">
                        <a href="{{ url('qc_inspection_po') }}">Select</a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</div>