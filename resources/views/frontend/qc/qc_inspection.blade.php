<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>New Inspection</h1>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid">
    <div class="card">
          <div class="container"> 
        <div class="row">            
            <div class="col-md-6">
                <div class="row">
                <div class="col-md-4">Inspection Level</div>
                    <div class="col-md-8">
                        <select class="form-control">
                            <option>Select Option</option>
                            <option>Counter Sample</option>
                            <option>In-Line Inspection</option>
                            <option>Mid-Line Inspection</option>
                            <option>Final Inspection</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                <div class="col-md-5">Visit No.</div>
                    <div class="col-md-5">
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option> 
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="mt-5 mb-5"></div>
        <div class="row">            
            <div class="col-md-6">
                <div class="row">
                <div class="col-md-4">Visit Date</div>
                    <div class="col-md-8">
                        <input type="text" name="" class="form-control" value="" placeholder="Date Enter Here">
                    </div>
                </div>
            </div>
        </div>

        <div class="mt-5 mb-5"></div>
        <div class="row">            
            <div class="col-md-6">
                <div class="row">
                <div class="col-md-4">Product </div>
                    <div class="col-md-8">
                        <select class="form-control">
                            <option>Select PO# Product</option>
                            <option>PO# Product 1</option>
                            <option>PO# Product 2</option>
                            <option>PO# Product 3</option>
                            <option>PO# Product 4</option>
                            <option>PO# Product 5</option>
                            <option>PO# Product 6</option>
                            <option>PO# Product 7</option> 
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-5 mb-5"></div>
        <div class="container">
            <table class="table">
                <tbody>
                  <tr>
                    <td>ITEM CODE</td>
                    <td>09876541111111</td>
                    <td>Buyer SKU#</td>
                    <td colspan="6">12234556678990</td> 
                  </tr>
                  <tr>
                    <td>Product Name</td>
                    <td colspan="6">PRODUCT NAME </td> 
                  </tr>
                  <tr>
                    <td>Product Dimension CMS.</td>
                    <td colspan="6">(L) xxxxx <b>x</b>(W) xxxxx <b>x</b>(H) xxxxx </td> 
                  </tr>
                  <tr>
                    <td>Meterial Used</td>
                    <td colspan="6">xxxxx Meterial </td> 
                  </tr>
                  <tr>
                    <td>Gross Weight</td>
                    <td>Wood</td>
                    <td>xxxxx Kgs.</td>
                    <td>Iron</td>
                    <td colspan="6">xxxxx Kgs</td> 
                  </tr>
                  <tr>
                    <td>Carton Dimension CMS.</td>
                    <td colspan="6">(L) xxxxx <b>x</b>(W) xxxxx <b>x</b>(H) xxxxx </td> 
                  </tr>
                  <tr>
                    <td>Finish</td>
                    <td colspan="8">Meterial </td> 
                  </tr>
                  <tr>
                    <td colspan="5">Numbering of the style / Component / Part for knok down furniture </td> 
                    <td>Numbering</td>
                  </tr>
                </tbody>
            </table>
            
            <a href="{{ URL('qc-checklist') }}"><button type="button">Submit</button></a>
        </div>
    </div>
</div>
</div>