<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Supporting Photos</h1>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid">
    <div class="card">
        <style type="text/css">
    .table-result-head{ background: #2c3e50; color: #ffffff;}
</style>

    <div class="mt-40"></div>
    <div class="container"> 
        <div class="row">
            <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="text-center table-result-head" colspan="6" >Result</th> 
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="text-center">A</td>
                    <td>Order Quantity</td>
                    <td>xxxxx</td>
                    <td class="text-center">B</td>
                    <td>Checked Pieces</td>
                    <td>xxxxx</td>
                  </tr>
                  <tr>
                    <td class="text-center">C</td>
                    <td>Pass Pices</td>
                    <td>xxxxx</td>
                    <td class="text-center">D</td>
                    <td>Rejected Pieces</td>
                    <td>xxxxx</td>
                  </tr>
                  <tr>
                    <td class="text-center">E</td>
                    <td class="qc-result">Result</td>
                    <td colspan="4">
                        <select>
                            <option>Select Result</option>
                            <option>YES</option>
                            <option>NO</option>
                        </select>
                    </td> 
                  </tr>                  
                </tbody>
            </table>            
        </div>
        <div class="mt-5"></div>
        <h6>QC Comment</h6>
        <div class="row">
            <div>
                <textarea></textarea>
            </div>
        </div>
        <button type="button" class="btn btn-primary"> SUBMIT</button>       
    </div>
</div>
</div>