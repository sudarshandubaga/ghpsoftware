<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Checklist</h1>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid">
    <div class="card">
        
        <table class="table table-bordered table-hover">
                <thead>
                  <tr class="text-center">
                    <th>Sr. No.</th>
                    <th>Inspection Check Point</th>
                    <th>Action</th> 
                  </tr>
                </thead>
                <tbody>
                    <tr>
                    <td class="text-center">1.</td> 
                    <td>Construction/Wood/Size/Design as per Counter Sample .</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">2.</td>
                    <td>Finish as matched with approved Counter Sample or swath .</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">3.</td>
                    <td>Drawers/Doors/Shelves (if folding) move smoothly .</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                    <tr>
                    <td class="text-center">4.</td>
                    <td>Bend Or Wraping in any part of Furniture .</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">5.</td>
                    <td>Bubbink/Waves/Uneven joint & shakiness/ Diagonally Checked .</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                   <tr>
                    <td class="text-center">6.</td>
                    <td>Loadability/ tilt/ Shaking Checked .</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">7.</td>
                    <td>Joint appropriate for durability & strength .</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">8.</td>
                    <td>Moisture Level should be between 8 to 12% .</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                 
                    <td class="text-center">9</td>
                    <td>Sharp edge /Open nails /Chip off/ grinder marks at any part of product.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">10.</td>
                    <td>Rust Corrosion test for Iron Parts.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">11.</td>
                    <td>Door Striking to the  frame and uniform gap around the door  and drawer.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td> 
                  </tr>
                  <tr>
                    <td class="text-center">12.</td>
                    <td>Dent/ Scratches/ Nail marks on the visible part checked.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td> 
                  </tr>
                  <tr>
                    <td class="text-center">13.</td>
                    <td>Wooden Planks between 8 to 20cm .</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">14.</td>
                    <td>Knot/ Hair Crack (1") should be matched with the product finish.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">15.</td>
                    <td>Inter changeability checked for KD Product.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">16.</td>
                    <td>Screw missing in the hinges (at the needed parts like Hinges, T angle) .</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">17.</td>
                    <td>L-Clip should be at undernath the floating tops.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">18.</td>
                    <td>V groov line / T-angle undernath the Tops.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">19.</td>
                    <td>Planks joints must be equally from start to end.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">20.</td>
                    <td>Product balance checked for both fixed and KD product on the floor.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">21.</td>
                    <td>Top Water Level Checked.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">22.</td>
                    <td>Product Assembly as per AI without any issue.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">23.</td>
                    <td>Assembly accessories / hardware as per specification.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">24.</td>
                    <td>Numbering on every KD parts (if required) .</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">25.</td>
                    <td>Glass Toughtened or normal as per approved thickness.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">26.</td>
                    <td>Product Sticker/ Hang tag as per buyer instruction.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">27.</td>
                    <td>Buffer/ Levellers on the legs bottom used.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">28.</td>
                    <td>Cartoon quality (7Ply/ 5Ply/ 3Ply) as agreed.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">29.</td>
                    <td>Silica gel used (if spacified as approved).</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">30.</td>
                    <td>Foam cornor used.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">31.</td>
                    <td>Polybag must be with suffocation warning (If used).</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">32.</td>
                    <td>There should not be movement in packed carton.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">33.</td>
                    <td>Barcode checked with scanner.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">34.</td>
                    <td>Cartoon Size of specification.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">35.</td>
                    <td>CBM Checked container stuffing.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">36.</td>
                    <td>Cartoon Marking as per buyer instruction.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">37.</td>
                    <td>AQL 2.5 %.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>
                  <tr>
                    <td class="text-center">38.</td>
                    <td>D-Nut.</td>
                    <td>
                        <select>
                            <option>Select Option</option>
                            <option>Applicable</option>
                            <option>Not Applicable</option>
                        </select>
                    </td>  
                  </tr>

                </tbody>
            </table>
            
            <a href="{{ URL('qc-image') }}"><button type="button">Upload Image</button></a>
</div>
</div>