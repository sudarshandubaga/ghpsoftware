<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Supporting Photos</h1>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid">
    <div class="card">
        
        <table class="table table-bordered table-hover">
                <tr>
                    <td>
                        <img src="https://software.ghp-sg.com/imgs/no-image.png" width="100" height="100"><br><br>
                        <input type="text" placeholder="QC Comment" class="form-control"><br>
                        <input type="text" placeholder="Buyer Comment" class="form-control">
                    </td>
                    
                    <td>
                        <img src="https://software.ghp-sg.com/imgs/no-image.png" width="100" height="100"><br><br>
                        <input type="text" placeholder="QC Comment" class="form-control"><br>
                        <input type="text" placeholder="Buyer Comment" class="form-control">
                    </td>
                    
                    <td>
                        <img src="https://software.ghp-sg.com/imgs/no-image.png" width="100" height="100"><br><br>
                        <input type="text" placeholder="QC Comment" class="form-control"><br>
                        <input type="text" placeholder="Buyer Comment" class="form-control">
                    </td>
                    
                    <td>
                        <img src="https://software.ghp-sg.com/imgs/no-image.png" width="100" height="100"><br><br>
                        <input type="text" placeholder="QC Comment" class="form-control"><br>
                        <input type="text" placeholder="Buyer Comment" class="form-control">
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <img src="https://software.ghp-sg.com/imgs/no-image.png" width="100" height="100"><br><br>
                        <input type="text" placeholder="QC Comment" class="form-control"><br>
                        <input type="text" placeholder="Buyer Comment" class="form-control">
                    </td>
                    
                    <td>
                        <img src="https://software.ghp-sg.com/imgs/no-image.png" width="100" height="100"><br><br>
                        <input type="text" placeholder="QC Comment" class="form-control"><br>
                        <input type="text" placeholder="Buyer Comment" class="form-control">
                    </td>
                    
                    <td>
                        <img src="https://software.ghp-sg.com/imgs/no-image.png" width="100" height="100"><br><br>
                        <input type="text" placeholder="QC Comment" class="form-control"><br>
                        <input type="text" placeholder="Buyer Comment" class="form-control">
                    </td>
                    
                    <td>
                        <img src="https://software.ghp-sg.com/imgs/no-image.png" width="100" height="100"><br><br>
                        <input type="text" placeholder="QC Comment" class="form-control"><br>
                        <input type="text" placeholder="Buyer Comment" class="form-control">
                    </td>
                </tr>
            </table>
            
            <a href="{{ URL('qc-final') }}"><button type="button">Submit</button></a>
</div>
</div>