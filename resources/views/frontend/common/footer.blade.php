 <!-- <div id="ajaxLoading">
  <div class="loading-bg"></div>
  <div class="loading-body">

  </div>
 </div> -->

 {{ HTML::script('js/popper.min.js') }}
 {{ HTML::script('js/bootstrap.min.js') }}
 {{ HTML::script('js/owl.carousel.min.js') }}
 {{ HTML::script('js/sweetalert.min.js') }}
 {{ HTML::script('js/bootstrap-tagsinput.min.js') }}
 {{ HTML::script('js/validation.js') }}
 <!-- {{ HTML::script('js/bootstrap-tagsinput-angular.min.js') }} -->
 {{ HTML::script('js/custom.js') }}



 <script type="text/javascript">
     $(function() {
         $(document).on('click', 'ul.main_navbar>li>a', function(e) {
             e.preventDefault();
             $('ul.main_navbar>li>a').not(this).next().slideUp();
             $(this).next().slideToggle();

             $('ul.main_navbar>li>a').not(this).parent().removeClass('active');
             $(this).parent().toggleClass('active');
         });
     });
 </script>
 </body>

 </html>
