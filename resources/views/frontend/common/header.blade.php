<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <base href="{{ url('/') . '/' }}">
    <title>{{ !empty($title) ? $title . ' | ' : '' }} {{ $site->setting_site_title }}</title>
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('css/select2.min.css') }}
    {{ HTML::style('icomoon/style.css') }}
    {{ HTML::style('css/bootstrap-tagsinput.css') }}
    {{ HTML::style('css/jquery-ui.css') }}
    {{ HTML::style('css/style.css') }}
    {{ HTML::script('js/jquery.min.js') }}
    {{ HTML::script('js/jquery-ui.js') }}
</head>

<body class="dash_body">
    <input type="hidden" id="base_url" value="{{ url('/') }}">
    <header>
        <div class="clearfix">
            <ul class="header-menu clearfix float-left">
                <li><a href="">Dashboard</a></li>
                @if ($profile->user_role == 'admin')
                    <li><a href="#">Master</a>
                        <ul>
                            <li><a href="#"><span class="float-right"><i class="icon-angle-right"></i></span>
                                    Location</a>
                                <ul>
                                    <li><a href="country">Country</a></li>
                                    <li><a href="state">State / County</a></li>
                                    <li><a href="city">City</a></li>
                                    <li><a href="{{ url('port') }}">Port</a></li>
                                </ul>
                            </li>

                            <li><a href="#"><span class="float-right"><i class="icon-angle-right"></i></span>
                                    Finish</a>
                                <ul>
                                    <li><a href="{{ URL('Material') }}">Material</a></li>
                                    <li><a href="{{ URL('MaterialType') }}">Material Type</a></li>
                                    <li><a href="{{ URL('finish') }}">Finish</a></li>
                                </ul>
                            </li>

                            <li><a href="{{ url('shipping-line') }}">Shipping Line</a></li>
                            <li><a href="{{ url('forwarder') }}">Forwarder</a></li>
                            <li><a href="{{ url('unit') }}">Unit</a></li>
                            <li><a href="{{ url('currency') }}">Currency</a></li>
                            <li><a href="#">Users <span class="float-right"><i
                                            class="icon-angle-right"></i></span></a>
                                <ul>
                                    <li><a href="{{ url('user/buyer') }}">Buyer</a></li>
                                    <li><a href="{{ url('user/vendor') }}">Vendor</a></li>
                                    <li><a href="{{ url('user/employee') }}">Employee</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="{{ url('product') }}">Product</a></li>
                    <li><a href="{{ route('offer.index') }}">Offer</a></li>
                    <li><a href="{{ url('enquiry') }}">Enquiry</a></li>
                    <li><a href="{{ url('quote') }}">Quotation</a></li>
                    <li><a href="{{ url('performa-invoice') }}">PI</a></li>
                    <li><a href="#">Vendor</a>
                        <ul>
                            <li><a href="{{ url('vendor-enquiry') }}">Enquiry</a></li>
                            <li><a href="{{ url('vquote') }}">Quotation</a></li>
                            <li><a href="{{ url('vquotestatus') }}">Enquiry Status</a></li>
                        </ul>
                    </li>

                    <li><a href="#">PO</a>
                        <ul>
                            <li><a href="{{ url('purchase-order') }}">View PO</a></li>
                            <li><a href="{{ url('vendor-booking-invoice') }}">Vendor Booking Invoice</a></li>
                            <li><a href="{{ url('vendor-invoice') }}">Vendor Invoice</a></li>
                        </ul>
                    </li>

                    <li><a href="{{ url('ghp-invoice-list') }}">GHP Invoice</a></li>
                    <!--<li><a href="">Purchase</a></li>-->
                    <li><a href="{{ URL('order-track') }}">Order Track</a></li>
                    <!--<li><a href="">Report</a></li>-->
                    <!--<li><a href="">Document</a></li>-->
                    <li><a href="{{ URL('inventory') }}">Inventory</a></li>
                    <li><a href="{{ url('view-logistic') }}">Logistic</a></li>
                @endif
                @if ($profile->user_role == 'buyer')
                    <li><a href="{{ url('quote') }}">Quotation</a></li>
                    <li><a href="{{ url('buyer-product/' . $profile->user_id) }}">Products</a></li>
                    <li><a href="{{ url('performa-invoice') }}">PI</a></li>
                    <li><a href="{{ url('view-logistic') }}">Logistic</a></li>
                    <li><a href="{{ URL('order-track') }}">Order Track</a></li>
                    <li><a href="{{ url('ghp-invoice-list') }}">GHP Invoice</a></li>
                    <li><a href="{{ url('user/profile/buyer') }}">Profile</a></li>
                @endif
                @if ($profile->user_role == 'vendor')
                    <li><a href="{{ url('vendor-enquiry') }}">Enquiry</a></li>
                    <li><a href="{{ url('vendor-quote') }}">Quotation</a></li>
                    <li><a href="#">PO</a>
                        <ul>
                            <li><a href="{{ url('purchase-order') }}">View PO</a></li>
                            <li><a href="{{ url('vendor-booking-invoice') }}">Vendor Booking Invoice</a></li>
                            <li><a href="{{ url('vendor-invoice') }}">Vendor Invoice</a></li>
                        </ul>
                    </li>
                @endif
                @if ($profile->user_role == 'logistic')
                    <li><a href="{{ url('view-logistic') }}">Logistic</a></li>
                @endif

                @if ($profile->user_role == 'qc')
                    <li><a href="{{ url('qc-new') }}">New</a></li>
                    <li><a href="{{ url('qc-schedule') }}">Schedule</a></li>

                    <li><a href="">Inspection</a></li>
                    <li><a href="">Upcoming</a></li>
                    <li><a href="">Recent</a></li>
                @endif

                <!-- <li><a href="{{ URL('Samplesheet') }}">Sample Sheet</a></li> -->
                <!-- @if ($profile->user_role == 'admin')
<li><a href="javascript:void(0)">Buyer Products</a>
      <ul>
       <li><a href="{{ url('buyer_product/add') }}">Add</a></li>
       <li><a href="{{ url('buyer_product/view') }}">View</a></li>
      </ul>
     </li>
@endif -->

                @if ($profile->user_role == 'admin')
                    <li><a href="javascript:void(0)">Buyer</a>
                        <ul>
                            <li><a href="{{ URL('Samplesheet') }}"> Buyer Sample Sheet</a></li>
                            <li><a href="#"><span class="float-right"><i class="icon-angle-right"></i></span>
                                    Buyer Pro. Sheet</a>
                                <ul>
                                    <li><a href="{{ url('buyer_product/add') }}">Add Buyer Product Sheets </a></li>
                                    <li><a href="{{ url('buyer_product/view') }}">View Buyer Product Sheets</a></li>
                                </ul>
                            </li>
                            <li> <a href="#"><span class="float-right"><i class="icon-angle-right"></i></span>
                                    Grid List</a>
                                <ul>
                                    <li><a href="{{ url('buyer_grid_reports') }}">Buyer Grid List</a></li>
                                    <li><a href="{{ url('vendor_grid_reports') }}">Vendor Grid List</a></li>
                                </ul>
                            </li>
                    </li>
            </ul>
            </li>
            <li>
                <a href="{{ route('logs') }}">Logs</a>
            </li>
            @endif

            </ul>
            <ul class="header-menu clearfix float-right">
                <!--<li><a href="">Back to Catalogue</a></li>-->
                <!--<li><a href="">Customer Profile</a></li>-->
                <li><a href="#" style="color: #e4cbc8">Welcome {{ $profile->user_name }}! </a></li>
                <li><a href="{{ url('user/logout') }}">Logout</a></li>
            </ul>
        </div>
    </header>
