@php
    $site = DB::table('settings')->first();
@endphp

@if(auth()->check())

    @php
        $profile = auth()->user();
        if($profile->user_role) :
            switch($profile->user_role) {
                case 'buyer' :
                    $profile_help = DB::table('buyers')->where('buyer_uid', $profile->user_id)->first();
                    break;

                case 'vendor' :
                    $profile_help = DB::table('vendors')->where('vendor_uid', $profile->user_id)->first();
                    break;

                case 'employee' :
                    $profile_help = DB::table('employees')->where('employee_uid', $profile->user_id)->first();
                    break;

                default :
                    $profile_help = [];
            }

            $profile = (object) array_merge( $profile->toArray(), (array) $profile_help );
        endif;
    @endphp

    @include('frontend.common.header')
    @yield('main_section')
    @include('frontend.common.footer')

    @yield('extra_scripts')

@else

    @include('frontend.login')

@endif
