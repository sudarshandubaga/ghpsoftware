@php
    $site = DB::table('settings')->first();
@endphp

@if(auth()->check())

    @php
        $profile = auth()->user();
        if($profile->user_role) :
            switch($profile->user_role) {
                case 'buyer' :
                    $profile_help = DB::table('buyers')->where('buyer_uid', $profile->user_id)->first();
                    break;

                case 'vendor' :
                    $profile_help = DB::table('vendors')->where('vendor_uid', $profile->user_id)->first();
                    break;

                case 'employee' :
                    $profile_help = DB::table('employees')->where('employee_uid', $profile->user_id)->first();
                    break;

                default :
                    $profile_help = [];
            }

            $profile = (object) array_merge( $profile->toArray(), (array) $profile_help );
        endif;
    @endphp

    @include('frontend.common.header')
    @if($profile->user_role == "admin" || $profile->user_role == "vendor" || $profile->user_role == "logistic")
        @include('frontend.inc.'.$page)
    @else
        @if($page == "view_logistic" || $page == "order_tracking" || $page == "order_tracking_detail" || $page == "view_ghp_invoice" || $page == 'samplesheet')
        @include('frontend.inc.'.$page)
        @else
        @include('frontend.'.$profile->user_role.'.'.$page)
        @endif
        
    @endif
    @include('frontend.common.footer')

@else

    @include('frontend.login')

@endif
