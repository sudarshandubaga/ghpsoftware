<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('user/logout', function () {
    auth()->logout();
    session()->forget('user_auth');
    return redirect('');
});
Route::group(['namespace' => 'App\Http\Controllers'], function () {
    Route::get('', 'DashboardController@index')->name('dashboard');
    Route::get('dashboard1', 'DashboardController@dashboard1');
    Route::any('login', 'Login@index');
    Route::any('currency/{nom?}', 'Currency@index');
    Route::post('/currency/export/csv', 'Currency@exportCsv')->name('exportCurrency');
    Route::get("order-track", "OrderTraking@View");
    Route::get("order-track-detail/{ID}", "OrderTraking@ViewDetail");
    Route::post('/order-track/export', 'OrderTraking@exportCsv')->name('exportOrderTracking');

    Route::any('product/CopyImages', 'Product@CopyImages');

    // Products

    Route::any('product/Image', 'Product@Image');

    Route::any('product/add/{nom?}', 'Product@add');
    Route::any('product/single/{nom}', 'Product@single');
    Route::any('product', 'Product@index');
    Route::post('/product/export', 'Product@exportCsv')->name('exportProduct');
    Route::any('product/status/{product_id}', 'Product@changeStatus');
    Route::any('RemoveProductImate/{ID}/{Index}', 'Product@RemoveImage');
    Route::any('product/print/{product_id}', 'Product@print');
    Route::any('product/delete/{product_id}', 'Product@delete');
    Route::any('buyer-product/{user_id}', 'BuyerProduct@index');
    Route::post('buyer-product/{user_id}/export', 'BuyerProduct@exportCsv')->name('exportBuyerProduct');
    Route::any('product/Copy/{product_id}', 'Product@CopyProduct');

    // Location
    Route::any('country/{nom?}', 'LocationController@country');
    Route::any('state/{nom?}', 'LocationController@state');
    Route::any('city/{nom?}', 'LocationController@city');
    Route::post('/country/export/csv', 'LocationController@countryexportCsv')->name('exportCountry');
    Route::post('/state/export/csv', 'LocationController@stateexportCsv')->name('exportState');
    Route::post('/city/export/csv', 'LocationController@cityexportCsv')->name('exportCity');

    Route::any('shipping-line/{nom?}', 'ShippingLine@index');
    Route::post('/shipping-line/export/csv', 'ShippingLine@exportCsv')->name('exportShippingLine');
    Route::any('finish/{nom?}', 'Finish@index');
    Route::post('/finish/export/csv', 'Finish@finishExportCsv')->name('exportFinish');

    Route::any('Material/{nom?}', 'MaterialController@index');
    Route::any('MaterialType/{nom?}', 'MaterialType@index');
    Route::post('/Material/export/csv', 'MaterialController@materialexportCsv')->name('exportMaterial');
    Route::post('/MaterialType/export/csv', 'MaterialType@materialTypeExportCsv')->name('exportMaterialType');


    Route::any('forwarder/{nom?}', 'Forwarder@index');
    Route::post('/forwarder/export/csv', 'Forwarder@exportCsv')->name('exportForwarder');
    Route::any('port/{nom?}', 'Port@index');
    Route::post('/port/export/csv', 'Port@portexportCsv')->name('exportPort');
    Route::any('unit/{nom?}', 'Unit@index');
    Route::post('/unit/export/csv', 'Unit@exportCsv')->name('exportUnit');
    Route::get('view-detail/{nom}', 'Viewdetail@index');

    Route::any('enquiry/{nom?}', 'Enquiry@index');
    Route::any('quote/add/{enq_id?}/{id?}', 'Quote@add');
    Route::any('quote/new-add', 'Quote@NewAdd');
    Route::any('quote/send/{id}', 'Quote@send');
    Route::any('quote', 'Quote@index');
    Route::any('quote/info/{id?}', 'Quote@single');
    Route::any('quote/revisions/{id?}', 'Quote@revisions');

    // Quote Ajax
    Route::post('get-quote-products', 'Quote@get_quote_products')->name('ajax.quote.products');
    Route::post('purchase-order/cart', 'PurchaseOrderController@cartStore')->name('purchase-order.cart.store');

    // Vendor Part
    Route::any('quote/send/{id}', 'Send_vendor@index');
    Route::any('assign-vendor/{id}', 'Send_vendor@send');

    Route::any('vquote', 'Vquote@index');
    Route::any('vquote/info/{id}', 'Vquote@single');
    Route::any('vquote/add/{enq_id}/{id?}', 'Vquote@add');
    Route::any('vquotestatus', 'Vquote@enquiry_status');
    Route::any('vquotestatus/{enq_id}', 'Vquote@enquiry_quote');

    Route::any('vendor-enquiry', 'Vendor_enquiry@index');
    Route::any('vendor-quote', 'Vendor_quote@index');
    Route::any('vendor-quote/info/{$id}', 'Vendor_quote@single');

    //   Invoice
    Route::any('performa-invoice/add/{quote_id}/{pi_id?}', 'PI_Controller@add');
    Route::any('performa-invoice/edit/{quote_id}/{pi_id?}', 'PI_Controller@update');
    Route::get('performa-invoice', 'PI_Controller@index');
    Route::get('performa-invoice/print/{pi_id}', 'PI_Controller@print');
    Route::get('ConfirmPI/{ID}/{Date}/{DP}', 'PI_Controller@ConfirmPI');
    Route::post('/performa-invoice/export', 'PI_Controller@exportCsv')->name('exportPi');


    Route::get('commercial-invoice/print/{pi_id}', 'PurchaseOrderController@CIPrint');
    Route::get('booking-commercial-invoice/print/{pi_id}', 'PurchaseOrderController@BookingCIPrint');


    Route::get('purchase-order/view-invoice/{pi_id}', 'PurchaseOrderController@ViewVendorInvoice');
    Route::any('purchase-order/choose-buyer', 'PurchaseOrderController@ChooseBuyer');
    Route::any('purchase-order/create-invoice', 'PurchaseOrderController@CreateVendorInvoice');
    Route::any('purchase-order/create-final-invoice', 'PurchaseOrderController@CreateFinalVendorInvoice');

    Route::get('purchase-order/view-invoice/{pi_id}', 'PurchaseOrderController@ViewVendorInvoice');

    Route::any('create-ghp-invoice/{po_id}', 'Pinvoice@CreateGHPInvoice');

    Route::get('purchase-order', 'PurchaseOrderController@index')->name('purchase-order.index');
    Route::post('/purchase-order/export', 'PurchaseOrderController@exportCsv')->name('exportPo');
    Route::get('purchase-order/transfer/{purchase_order}', 'PurchaseOrderController@transfer')->name('po.transfer');
    Route::post('purchase-order/transfer/{purchase_order}', 'PurchaseOrderController@storeTransfer')->name('po.transfer.store');



    Route::get('vendor-booking-invoice', 'PurchaseOrderController@AllVendorBookingInvoice');
    Route::get('vendor-invoice', 'PurchaseOrderController@AllVendorInvoice');
    Route::post('vendor-invoice-export', 'PurchaseOrderController@AllVendorInvoiceCsv')->name('CommercialInvoiceExport');

    Route::any('purchase-order/add/{enq_id}/{quto_id}', 'PO_Controller@add');
    Route::get('purchase-order/print/{pi_id}', 'PO_Controller@print');
    Route::get('purchase-order/copy/{pi_id}', 'PO_Controller@copy');
    Route::get('purchase-order/edit/{pi_id}', 'PO_Controller@edit');

    Route::get('purchase-order-sample/print/{pi_id}', 'PO_Controller_Sample@print');


    Route::post('purchase-order/edit-save', 'PO_Controller@saveedit');

    // test print offer 
    Route::any('offer/print/{offer}', 'OfferController@print')->name('offer.print');


    Route::any('vendor-enquiry', 'Vendor_enquiry@index');
    Route::any('vendor_quote/add/{num}', 'Vendor_quote@add');

    Route::any('user/add/buyer/{id?}', 'User@add_buyer');
    Route::any('user/change_status/{role}/{id?}', 'User@change_status');
    Route::any('user/add/employee/{id?}', 'User@add_employee');
    // Route::any('user/buyer', 'User@index');

    Route::any('user/add/{role}/{id?}', 'User@add');
    Route::any('user/vendor', 'User@vendor');
    Route::any('user/{role}', 'User@index');

    Route::any('user/profile/buyer', 'User@BuyerProfile');

    Route::any('ajax/user_login', 'Ajax@user_login');
    Route::any('ajax/get_states/{ID}', 'Ajax@get_states');
    Route::any('ajax/{action}', 'Ajax@index');
    Route::get('ajax/approve_qpro/{qid}/{pid}', 'Ajax@approve_qpro');
    Route::get('ajax/approve_vqpro/{qid}/{pid}', 'Ajax@approve_vqpro');
    Route::any('ajax/add_to_cart/{sess_name}', 'Ajax@add_to_cart');
    Route::any('ajax/update_cart/{sess_name}', 'Ajax@update_cart');
    Route::any('ajax/remove_cart/{sess_name}', 'Ajax@remove_cart');


    Route::get('ghp-invoice-list', 'Pinvoice@index');
    Route::get('ghp-commercial-invoice/print/{ID}', 'Pinvoice@GhpCommercialInvoicePrint');
    Route::get('packing-list/print/{ID}', 'Pinvoice@GhpPackingListPrint');


    Route::get('view-logistic', 'Logistic@ViewAll');
    Route::post('view-logistic/export', 'Logistic@exportCsv')->name('exportLogistic');
    Route::any('Logistics/add/{id?}', 'Logistic@Add');
    Route::get("LoadPIData/{ID}", 'Logistic@LoadPI');
    Route::get("LoadPOData/{ID}", 'Logistic@LoadPO');
    Route::get("LoadOtherData/{ID}", 'Logistic@LoadOther');

    Route::get("GetBuyerPrices/{ID}", 'Ajax@GetPrevQuotePrice');
    Route::get("LoadBuyerPO/{ID}", 'PurchaseOrderController@LoadBuyerPO');



    Route::get("inventory", 'Inventory@ViewAll');
    Route::post('/inventory/export', 'Inventory@exportCsv')->name('exportInventory');

    Route::get("qc-schedule", 'QC@Schedule');
    Route::get("qc-new", 'QC@New');
    Route::get("qc_inspection_po", 'QC@Inspectoin');
    Route::get("qc-checklist", 'QC@Checklist');
    Route::get("qc-image", 'QC@Image');
    Route::get("qc-final", 'QC@Final');


    Route::get("Samplesheet", 'DashboardController@Samplesheet');
    Route::get("Samplesheet/Delete/{ID}", 'DashboardController@DeleteSheet');
    Route::post("SaveFiles", 'DashboardController@SaveFiles');


    Route::get("buyer_product/add", 'Buyer_products@index');
    Route::post("buyer_product/get_products", 'Buyer_products@get_products')->name('get_products');
    Route::post("buyer_product/upload_buyer_products", 'Buyer_products@upload_buyer_products')->name('upload_buyer_products');
    Route::get("buyer_product/view", 'Buyer_products@view_buyer_products')->name('view_buyer_products');
    Route::post("buyer_product/get_buyer_products", 'Buyer_products@get_buyer_products')->name('get_buyer_products');

    Route::get("vendor_grid_reports", 'Grid_reports@vendor')->name('vendor_grid_reports');
    Route::post("vendor_grid/products", 'Grid_reports@grid_products')->name('vendor_grid_products');
    Route::post("get_excel_products", 'Buyer_products@get_excel_products')->name('get_excel_products');

    Route::get("buyer_grid_reports", 'Grid_reports@buyer')->name('buyer_grid_reports');
    Route::post("buyer_grid/products", 'Grid_reports@buyer_grid_products')->name('buyer_grid_products');
    Route::post("get_excel_products", 'Grid_reports@get_buyer_excel_products')->name('get_buyer_excel_products');
    Route::get('logs', 'LogController@index')->name('logs');
    // Update Grid Remarks
    Route::post("update_grid", 'Grid_reports@update_grid')->name('updateGrid');
    Route::post("update_vendor_grid", 'Grid_reports@update_vendor_grid')->name('updateVenderGrid');

    // Export to excel
    Route::post('/vendor/export_grid_list', 'Grid_reports@vendor_export')->name('export_vendor_grid_list');
    Route::resources([
        'role' => 'RoleController',
        'offer' => 'OfferController'
    ]);
});
