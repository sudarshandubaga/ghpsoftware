<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class LocationController extends BaseController
{
    public function country(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $input = $request->input('record');
            if (!empty($input)) {
                if (empty($id)) {
                    echo '<pre>';
                    print_r($input);


                    $CheckDuplicate = DB::table('countries')->where("country_name", $input["country_name"])->where("country_short_name", $input["country_short_name"])->count();

                    if ($CheckDuplicate > 0) {
                        Session::flash('Danger', 'Country Name & Code Already Exists');
                        return redirect('country');
                    }


                    DB::table('countries')->insert($input);

                    Session::flash('Success', 'Country Added Successfully');
                    $id = DB::getPdo()->lastInsertId();
                } else {
                    DB::table('countries')->where('country_id', $id)->update($input);
                    Session::flash('Success', 'Country Updated Successfully');
                }
            }

            $check = $request->input('check');
            if (!empty($check)) {
                DB::table('countries')->whereIn('country_id', $check)->update(['country_is_deleted' => 'Y']);
                Session::flash('Success', 'Country Deleted Successfully');
            }

            return redirect('country');
        }

        $edit = array();
        if (!empty($id)) {
            $edit = DB::table('countries')->where('country_id', $id)->first();
        }

        $records = DB::table('countries');
        if (!empty($request->country_name)) {
            $records->where('country_name', "like", "%" . $request->country_name . "%");
        }
        $records = $records->where('country_is_deleted', 'N')->paginate(10);

        $title     = "Country | GHP Software";
        $page     = "country";
        $data     = compact('page', 'title', 'records', 'edit');
        return view('frontend/layout', $data);
    }

    public function countryexportCsv(Request $request)
    {
        $fileName = 'country.csv';
        // $tasks = SurveyHistory::with('user','survey')->get()->toArray();
        $records = DB::table('countries');
        if (!empty($request->country_name)) {
            $records->where('country_name', "like", "%" . $request->country_name . "%");
        }
        $lists = $records->where('country_is_deleted', 'N')->get()->toArray();
        $listArr = [];

        foreach ($lists as $key => $l) {

            $listArr[$key]['Country Code'] = !empty($l->country_code) ? "+" . $l->country_code : '';
            $listArr[$key]['Country Name'] = @$l->country_name;
            $listArr[$key]['Country Short Name'] = @$l->country_short_name;
        }
        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("countryList" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }



    public function state(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $input = $request->input('record');
            if (!empty($input)) {
                if (empty($id)) {

                    $CheckDuplicate = DB::table('states')->where("state_country", $input["state_country"])->where("state_name", $input["state_name"])->where("state_short_name", $input["state_short_name"])->count();
                    if ($CheckDuplicate > 0) {
                        Session::flash('Danger', 'State Name & Code Already Exists');
                        return redirect('state');
                    }

                    DB::table('states')->insert($input);
                    $id = DB::getPdo()->lastInsertId();
                    Session::flash('Success', 'State Added Successfully');
                } else {

                    Session::flash('Success', 'State Updated Successfully');
                    DB::table('states')->where('state_id', $id)->update($input);
                }
            }

            $check = $request->input('check');
            if (!empty($check)) {
                Session::flash('Success', 'State Deleted Successfully');
                DB::table('states')->whereIn('state_id', $check)->update(['state_is_deleted' => 'Y']);
            }

            return redirect('state');
        }

        $edit = array();
        if (!empty($id)) {
            $edit = DB::table('states')->where('state_id', $id)->first();
        }

        $countries  = DB::table('countries')->where('country_is_deleted', 'N')->get();
        $records    = DB::table('states AS s')
            ->join('countries AS c', 's.state_country', 'c.country_id')
            ->where('state_is_deleted', 'N');
        if (!empty($request->state_name)) {
            $records->where('state_name', "like", "%" . $request->state_name . "%");
        }
        $records = $records->paginate(10);

        $title  = "State | GHP Software";
        $page   = "state";
        $data   = compact('page', 'title', 'records', 'edit', 'countries');
        return view('frontend/layout', $data);
    }

    public function stateexportCsv(Request $request)
    {
        $fileName = 'city.csv';
        // $tasks = SurveyHistory::with('user','survey')->get()->toArray();
        $countries  = DB::table('countries')->where('country_is_deleted', 'N')->get();
        $records    = DB::table('states AS s')
            ->join('countries AS c', 's.state_country', 'c.country_id')
            ->where('state_is_deleted', 'N');
        if (!empty($request->state_name)) {
            $records->where('state_name', "like", "%" . $request->state_name . "%");
        }
        $lists = $records->get()->toArray();
        $listArr = [];

        foreach ($lists as $key => $l) {
            $listArr[$key]['Country'] = !empty($l->country_name) ? $l->country_name : '';
            $listArr[$key]['State Name'] = @$l->state_name;
            $listArr[$key]['State Short Name'] = @$l->state_short_name;
        }
        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("stateList" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }


    public function city(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $input = $request->input('record');
            if (!empty($input)) {
                if (empty($id)) {

                    $CheckDuplicate = DB::table('cities')
                        ->where("city_country", $input["city_country"])
                        ->where("city_state", $input["city_state"])
                        ->where("city_name", $input["city_name"])
                        ->where("city_short_name", $input["city_short_name"])
                        ->count();
                    if ($CheckDuplicate > 0) {
                        Session::flash('Danger', 'City Name & Code Already Exists');
                        return redirect('city');
                    }

                    DB::table('cities')->insert($input);
                    $id = DB::getPdo()->lastInsertId();

                    Session::flash('Success', 'City Added Successfully');
                } else {
                    DB::table('cities')->where('city_id', $id)->update($input);
                    Session::flash('Success', 'City Updated Successfully');
                }
            }

            $check = $request->input('check');
            if (!empty($check)) {
                DB::table('cities')->whereIn('city_id', $check)->update(['city_is_deleted' => 'Y']);
                Session::flash('Success', 'City Deleted Successfully');
            }

            return redirect('city');
        }

        $edit = $states = array();
        $countries  = DB::table('countries')->where('country_is_deleted', 'N')->get();

        if (!empty($id)) {
            $edit = DB::table('cities')->where('city_id', $id)->first();
            if (!empty($edit->city_country)) {
                $states = DB::table('states')->where('state_is_deleted', 'N')->where('state_country', $edit->city_country)->get();
            }
        }


        $records = DB::table('cities AS ct')
            ->join('countries AS con', 'ct.city_country', 'con.country_id')
            ->leftJoin('states AS st', 'ct.city_state', 'st.state_id')
            ->where('city_is_deleted', 'N');
        if (!empty($request->city_name)) {
            $records->where('city_name', "like", "%" . $request->city_name . "%");
        }
        $records = $records->select('ct.*', 'con.country_name', 'st.state_name')
            ->paginate(10);

        $title  = "City | GHP Software";
        $page   = "city";
        $data   = compact('page', 'title', 'records', 'edit', 'countries', 'states');
        return view('frontend/layout', $data);
    }

    public function cityexportCsv(Request $request)
    {
        $fileName = 'city.csv';
        // $tasks = SurveyHistory::with('user','survey')->get()->toArray();
        $records = DB::table('cities AS ct')
            ->join('countries AS con', 'ct.city_country', 'con.country_id')
            ->leftJoin('states AS st', 'ct.city_state', 'st.state_id')
            ->where('city_is_deleted', 'N');
        if (!empty($request->city_name)) {
            $records->where('city_name', "like", "%" . $request->city_name . "%");
        }
        $records = $records->select('ct.*', 'con.country_name', 'st.state_name');
        $lists = $records->get()->toArray();
        $listArr = [];

        foreach ($lists as $key => $l) {
            $listArr[$key]['Country Name'] = !empty($l->country_name) ? $l->country_name : '';
            $listArr[$key]['State Name'] = @$l->state_name;
            $listArr[$key]['City Name'] = @$l->city_name;
            $listArr[$key]['City Short Name'] = @$l->city_short_name;
        }
        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("cityList" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }


    public  function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }
    public function download_send_headers($filename)
    {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
}
