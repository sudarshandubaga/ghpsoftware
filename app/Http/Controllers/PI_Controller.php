<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

use App\Models\Query;
use App\Models\QuoteModel as Quote;
use App\Models\UpdateLog;
use App\Models\QuoteProduct;
use App\Models\PIModel as PI;
use App\Exports\PiExport;
use App\Models\PIProduct;
use App\Models\Bproduct;
use App\Models\POModel;
use App\Models\UserModel;

use Maatwebsite\Excel\Facades\Excel;

class PI_Controller extends BaseController
{
    public function add(Request $req, $quote_id, $id = NULL)
    {
        // dd($quote_id);
        $quotes   = Quote::where('quote_is_deleted', 'N')->get();
        $pi_product = QuoteProduct::with('product')->where('qpro_qid', $quote_id)->where('qpro_is_approved', 'Y')->get();

        $pi_cart = session('pi_cart');
        if (empty($pi_cart)) {
            foreach ($pi_product as $key => $pi) {
                $pi_cart[$pi->qpro_pid] = [
                    'pipro_pid'         => $pi->qpro_pid,
                    'pipro_qty'         => $pi->qpro_qty,
                    // 'pipro_sku'         => '',// $pi->qpro_sku,
                    'pipro_price'       => $pi->qpro_price,
                    // 'pipro_barcode'     => $pi->qpro_barcode,
                    'pipro_buyer_desc'  => $pi->qpro_buyer_desc,
                    'pipro_fsc_status'  => '', // $pi->qpro_fsc_status,
                    'pipro_species'     => '', // $pi->qpro_species,
                    'grid_remark'       => '', // $pi->qpro_species,
                ]; //$pi->toArray();
            }

            session(['pi_cart' => $pi_cart]);
        }

        // dd($pi_cart);

        // dd($pi_product->toArray());
        $quote    = Quote::find($quote_id);

        if ($req->isMethod('post')) {
            // dd(session('pi_cart'));
            $input = $req->input('record');

            $input['pi_show_nw']    = !empty($input['pi_show_nw']) ? $input['pi_show_nw'] : 'N';
            $input['pi_show_gw']    = !empty($input['pi_show_gw']) ? $input['pi_show_gw'] : 'N';
            $input['pi_created_on'] = date("Y-m-d H:i:s", time());
            $id    = PI::insertGetId($input);
            $mess  = "A new PI has been created.";

            PIProduct::where('pipro_pi_id', $id)->delete();

            $pi_cart = session('pi_cart');
            foreach ($pi_cart as $pid => $p) {
                $bpro = Bproduct::where('bpro_pid', $pid)->first();
                $qpro = QuoteProduct::where('qpro_pid', $pid)->where('qpro_qid', $quote_id)->first();

                $qpro->qpro_price   = @$pi_cart[$pid]['pipro_price'];
                $qpro->qpro_qty     = @$pi_cart[$pid]['pipro_qty'];
                $qpro->save();

                $arr = [
                    'pipro_pi_id'       => $id,
                    'pipro_pid'         => $pid,
                    'pipro_price'       => @$pi_cart[$pid]['pipro_price'],
                    'pipro_qty'         => @$pi_cart[$pid]['pipro_qty'],
                    // 'pipro_sku'         => @$bpro->bpro_sku,
                    // 'pipro_barcode'     => @$bpro->bpro_barcode,
                    // 'pipro_buyer_desc'  => @$bpro->bpro_description,
                    'pipro_sku'         => @$pi_cart[$pid]['bpro_sku'],
                    'pipro_barcode'     => @$pi_cart[$pid]['bpro_barcode'],
                    'pipro_buyer_desc'  => @$pi_cart[$pid]['bpro_description'],
                    'pipro_fsc_status'  => @$pi_cart[$pid]['pipro_fsc_status'],
                    'pipro_species'     => @$pi_cart[$pid]['pipro_species'],
                ];
                PIProduct::insert($arr);
            }
            (new UpdateLog)->store('Pi', 'add');
            session()->forget('pi_cart');

            return redirect(url('performa-invoice'))->with('success', $mess);
        }


        $title     = "Create Performa Invoice";
        $page   = "add_pi";
        $data   = compact('page', 'title', 'quote_id', 'quotes', 'pi_product', 'quote', 'id');
        return view('frontend/layout', $data);
    }

    public function update(Request $req, $quote_id, $id = NULL)
    {

        $quotes   = Quote::where('quote_is_deleted', 'N')->get();
        // $products = QuoteProduct::with('product')->where('qpro_qid', $quote_id)->where('qpro_is_approved', 'Y')->get();

        $quote      = Quote::find($quote_id);

        $edit       = PI::find($id);
        $pi_product = PIProduct::with('products')->where('pipro_pi_id', $id)->get();

        $pi_cart = session('pi_cart');
        if (empty($pi_cart)) {
            if (!empty($pi_product)) {
                foreach ($pi_product as $key => $pi) {
                    $qpro = QuoteProduct::with('product')->where('qpro_qid', $quote_id)->where('qpro_pid', $pi->pipro_pid)->where('qpro_is_approved', 'Y')->first();
                    $pi_cart[$pi->pipro_pid] = [
                        'pipro_pid'         => $pi->pipro_pid,
                        'pipro_qty'         => $pi->pipro_qty,
                        'pipro_sku'         => $pi->pipro_sku,
                        'pipro_price'       => !empty($pi->pipro_price) ? $pi->pipro_price : $qpro->qpro_price,
                        'pipro_fsc_status'  => $pi->pipro_fsc_status,
                        'pipro_species'     => $pi->pipro_species,
                        'grid_remark'       => $pi->grid_remark,
                    ];
                }
            }

            session(['pi_cart' => $pi_cart]);
        }

        if ($req->isMethod('post')) {
            $input = $req->input('record');

            $input['pi_show_nw']    = !empty($input['pi_show_nw']) ? $input['pi_show_nw'] : 'N';
            $input['pi_show_gw']    = !empty($input['pi_show_gw']) ? $input['pi_show_gw'] : 'N';
            $input['pi_created_on'] = date("Y-m-d H:i:s", time());

            $pi    = PI::find($id);
            $pi->fill($input);
            $pi->save();

            (new UpdateLog)->store('Pi', 'edit');
            $mess  = "PI has been updated.";



            $pi_cart = session('pi_cart');

            if (!empty($pi_cart)) {
                PIProduct::where('pipro_pi_id', $id)->delete();
                foreach ($pi_cart as $p_id => $p) {
                    $bpro        = Bproduct::where('bpro_uid', $quote->quote_uid)->where('bpro_pid', $p_id)->first();
                    $qpro = QuoteProduct::where('qpro_pid', $p_id)->where('qpro_qid', $pi->pi_qid)->first();

                    $qpro->qpro_price   = @$pi_cart[$p_id]['pipro_price'];
                    $qpro->qpro_qty     = @$pi_cart[$p_id]['pipro_qty'];
                    $qpro->save();
                    $arr = [
                        'pipro_pi_id'       => $id,
                        'pipro_pid'         => $p_id,
                        'pipro_price'       => @$p['pipro_price'],
                        'pipro_qty'         => @$p['pipro_qty'],
                        // 'pipro_sku'         => @$bpro->bpro_sku,
                        // 'pipro_barcode'     => @$bpro->bpro_barcode,
                        // 'pipro_buyer_desc'  => @$bpro->bpro_description,
                        'pipro_sku'         => @$p['bpro_sku'],
                        'pipro_barcode'     => @$p['bpro_barcode'],
                        'pipro_buyer_desc'  => @$p['bpro_description'],
                        'pipro_fsc_status'  => @$p['pipro_fsc_status'],
                        'pipro_species'     => @$p['pipro_species'],
                    ];
                    PIProduct::insert($arr);
                }
            }

            session()->forget('pi_cart');

            return redirect(url('performa-invoice'))->with('success', $mess);
        }

        $title     = "Update Performa Invoice";
        $page   = "add_pi";
        $data   = compact('page', 'title', 'quote_id', 'quotes', 'edit', 'pi_product', 'quote', 'id');
        return view('frontend/layout', $data);
    }

    public function index(Request $request, $id = null)
    {
        session()->forget('pi_cart');
        $dbprefix = env('DB_PREFIX');

        foreach (PI::get() as $DKS) {
            $RowPINo = sprintf("%s-%03d", 'GHP-202122', $DKS->pi_id);
            $CheckStatus = DB::table('logistics')->where("exs_logistics_pi", "like", "%" . $RowPINo . "%")->count();

            $PISatus = 0;
            $ShipStatus = 0;

            if ($CheckStatus > 0) {
                $CheckStatus = DB::table('logistics')->where("exs_logistics_pi", "like", "%" . $RowPINo . "%")->first();
                if ($CheckStatus->VesselSailingETD < date("Y-m-d")) {
                    $PISatus = 1;
                }

                if ($CheckStatus->track_status == 3) {
                    $ShipStatus = 1;
                }
            }

            $PP = PI::find($DKS->pi_id);
            $PP->pi_status = $PISatus;
            $PP->pi_ship_status = $ShipStatus;
            $PP->save();
        }


        $query = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->orderBy('pi_id', 'DESC')
            ->where('pi_is_deleted', 'N');

        $profile = Query::get_profile();

        if ($profile->user_role == "buyer") {
            $query->where('q.quote_uid', $profile->user_id);
        }

        if (@$_GET['SearchByName'] != "") {
            $query->where('user_name', "like", "%" . $_GET['SearchByName'] . "%");
        }

        if (@$_GET['SearchByOrderRef'] != "") {
            $id = (int)str_replace("GHP-202122-", '', $_GET['SearchByOrderRef']);
            $query->where('pi_order_ref', "like", "%" . $id . "%");
        }

        if (@$_GET['SearchByPI'] != "") {
            $pi_order = $_GET['SearchByPI'] - 100;
            $id = (int)str_replace("GHP-202122-", '', $pi_order);
            // dd($id);
            $query->where('pi_id', "like", "%" . $id . "%");
        }

        // if (@$_GET['SearchByPI'] != "") {
        //     $id = (int)str_replace("GHP-202122-", '', $_GET['SearchByPI']);
        //     $query->where('pi_id', "like", "%" . $id . "%");
        // }

        if (@$_GET['SearchSttaus'] != "") {
            $query->where('pi_status', $_GET['SearchSttaus']);
        }

        $buyers = UserModel::where('user_role', 'buyer')->where('user_is_deleted', 'N')->get();
        if (@$_GET['SearchBuyer'] != "") {
            $query->where('user_id', "=", $_GET['SearchBuyer']);
        }



        $records = $query->paginate(15);


        if (COUNT($records) > 0) {
            foreach ($records as $key => $each) {

                $Porders = POModel::select('po_id')->where('po_pi_id', $each->pi_id)->get();
                $each->porders = $Porders;

                $piproduct_price = PIProduct::where('pipro_pi_id', $each->pi_id)->sum('pipro_price');
                $piproduct_qty = PIProduct::where('pipro_pi_id', $each->pi_id)->sum('pipro_qty');

                $piProdcuts = PIProduct::where('pipro_pi_id', $each->pi_id)->get();
                // dd($pipro);
                $totalCbm = 0;
                foreach ($piProdcuts as $pipro) {
                    $totalCbm += $pipro->products()->sum('product_cbm') * $pipro->pipro_qty;
                }
                $totalPrice = $piproduct_price > 0 ? $piproduct_price * $piproduct_qty : $each->quote_total;

                // dd($totalCbm);

                $each->piproduct_price = $piproduct_price;
                $each->piproduct_qty = $piproduct_qty;
                $each->totalPrice = $totalPrice;
                $each->totalCbm = $totalCbm;
            }
        }


        $title     = "View PI";
        $page     = "view_pi";
        $data = compact('page', 'title', 'records', 'buyers');
        return view('frontend/layout', $data);
    }

    public function print($id)
    {
        $record = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->leftJoin('countries AS con', 'u.user_country', 'con.country_id')
            ->leftJoin('states AS st', 'u.user_state', 'st.state_id')
            ->find($id);

        $totalPrice = PIProduct::where('pipro_pi_id', $id)->sum(\DB::raw('pipro_price * pipro_qty'));
        // dd($totalPrice);

        $products   = PIProduct::join('products AS p', 'p.product_id', 'pipro_pid')->join('quote_products AS qpro', 'pipro_pid', 'qpro_pid')->where('pipro_pi_id', $id)->where('qpro_qid', $record->quote_id)->get();

        $title         = "Performa Invoice | GHP Software";
        $page       = "performa-invoice";
        $data       = compact('page', 'title', 'record', 'products', 'totalPrice');
        return view('frontend/performa-invoice', $data);
    }

    public function ConfirmPI($ID, $Date, $DP)
    {
        $PI = PI::find($ID);
        $PI->pi_confirm = 1;
        $PI->pi_confirm_date = $Date;
        $PI->dp_date = $DP;
        $PI->save();
        (new UpdateLog)->store('Pi', 'confirm pi');
    }

    public function ExportPI(Request $request)
    {
        return Excel::download(new PiExport($request), 'PiExport.xlsx');
    }

    public function exportCsv(Request $request)
    {
        $fileName = 'reports.csv';
        // $tasks = SurveyHistory::with('user','survey')->get()->toArray();
        $query = PI::select('pi_id', 'quote_number', 'pi_order_ref', 'user_name', 'quote_total', 'quote_total_cbm', 'dp_date', 'pi_created_on', 'pi_confirm_date', 'pi_status as status', 'pi_ship_status as ship_status', 'currency_sign')
            ->join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->orderBy('pi_id', 'DESC')
            ->where('pi_is_deleted', 'N');
        if (!empty($request->SearchSttaus)) {
            $query->where('pi_status',  $request->SearchSttaus);
        }
        if (!empty($request->SearchBuyer)) {
            $query->where('user_id', "=", $request->SearchBuyer);
        }
        if (!empty($request->checked)) {
            // dd(array_values($request->checked));
            $checkArr = array_values($request->checked);
            // dd($checkArr);
            $query->whereIn('pi_id', $checkArr);
        }
        $lists = $query->get()->toArray();
        $listArr = [];

        $site = DB::table('settings')->first();
        foreach ($lists as $key => $l) {

            $listArr[] = $l;
            $listArr[$key]['quote_number'] = sprintf("%s%06d", $site->setting_quote_prefix, $l['quote_number']);
            $listArr[$key]['pi_order_ref'] = $l['pi_order_ref'] != "" ? $l['pi_order_ref'] : sprintf("%s-%03d", 'GHP-201819', $l->pi_id);
            $listArr[$key]['pi_no'] = sprintf("%s-%03d", 'GHP-202122', $l['pi_id'] + 100);
            $listArr[$key]['quote_total'] = !empty($l['currency_sign']) ? $l['currency_sign'] . '' . number_format($l['quote_total'], 2) : '';
            $listArr[$key]['pi_created_on'] = date("d-M-Y", strtotime($l['pi_created_on']));
            // $listArr[$key]['dp_date'] = $l['dp_date'] ? date("d-M-Y", strtotime($l['dp_date'])) : '';
            $listArr[$key]['pi_confirm_date'] = $l['pi_confirm_date'] != "0000-00-00" ? $l['pi_confirm_date'] : '-';
            $listArr[$key]['status'] = $l['status'] == 0 ? 'Open' : 'Close';
            $listArr[$key]['ship_status'] =  $l['ship_status'] == 0 ? 'Not Shipped' : 'Shipped';
            unset($listArr[$key]['currency_sign']);
        }
        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("pi_reports" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }
    public  function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }
    public function download_send_headers($filename)
    {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
}
