<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Query;
use App\Models\QuoteModel as Qmodel;
use App\Models\UserModel as Umodel;
use App\Models\VenqryModel as Vemodel;
use App\Models\VenqproModel as Vepmodel;
use App\Models\Bproduct;
use App\Models\QuoteProduct;
use App\Models\PIProduct;
use App\Models\PIModel as PI;
use DB;

class Quote extends BaseController
{

    public function add(Request $request, $enq_id, $id = null)
    {
        $edit = [];
        if (!empty($id)) {
            $edit = Qmodel::where('quote_id', $id)->orderBy('quote_id', 'DESC')->first();
        }

        $profile = Query::get_profile();



        // print_r($profile->user_role);

        if ($request->isMethod('post')) {
            $record = $request->input('record');
            $record['quote_updated_on'] = date('Y-m-d H:i:s', time());
            $record['quote_enq_id']     = $enq_id;
            $record['quote_added_by']   = $profile->user_role;

            if (empty($edit->quote_id)) {
                $last  = Qmodel::where('quote_is_deleted', 'N')->select(DB::raw('MAX(quote_number) AS max_id'))->first();
                $record['quote_number']     = $last->max_id + 1;
                $record['quote_created_on'] = date('Y-m-d H:i:s', time());

                $record = array_filter($record);
                Qmodel::insert($record);
                $id = DB::getPdo()->lastInsertId();
            } else {
                $last  = Qmodel::where('quote_is_deleted', 'N')
                    ->select('quote_uid', 'quote_enq_id', 'quote_enq_id', 'quote_number', 'quote_currency', 'quote_price_term', 'quote_delivery_port', 'quote_delivery_days', 'quote_payment', 'quote_date', 'quote_version', 'quote_origin_country', 'quote_first_destination_country', 'quote_loading_port', 'quote_discharge_port', 'quote_reciept_port')
                    ->where('quote_id', $edit->quote_id)
                    ->first()->toArray();

                // print_r($last); die;

                $record = array_merge($record, (array) $last);

                $record['quote_version'] = $last['quote_version'] + 1;
                $record['quote_number']  = $last['quote_number'];

                $record = array_filter($record);

                Qmodel::insert($record);
                $id = DB::getPdo()->lastInsertId();
            }

            DB::table('quote_products')->where('qpro_qid', $id)->delete();
            $carts = session('quote_cart');

            if (!empty($carts)) {
                foreach ($carts as $pid => $data) {
                    extract($data);
                    $is_approved = !empty($data['verified']) ? $data['verified'] : "N";
                    $arr = [
                        'qpro_qid'          => $id,
                        'qpro_pid'          => $pid,
                        'qpro_price'        => $data['price'],
                        'qpro_remark'       => @$data['remark'] . "",
                        'qpro_qty'          => $data['qty'],
                        'qpro_is_approved'  => $is_approved
                    ];

                    DB::table('quote_products')->insert($arr);

                    $bpro_exists = Bproduct::where('bpro_pid', $pid)->where('bpro_uid', $record['quote_uid'])->count();
                    if (!$bpro_exists) {
                        $bpro_arr = [
                            'bpro_pid'  => $pid,
                            'bpro_uid'  => $record['quote_uid']
                        ];

                        Bproduct::insert($bpro_arr);
                    }
                }
            }

            return redirect('quote');
        }

        $enquiry = DB::connection('mysql2')
            ->table('orders AS o')
            ->join('users AS u', 'o.order_uid', 'u.user_id')
            ->where('o.order_id', $enq_id)
            ->first();

        $uid = !empty($enquiry->user_id) ? $enquiry->user_id : 0;

        $buyers = DB::table('users')
            ->where('user_role', 'buyer')
            ->where('user_is_deleted', 'N')
            ->get();

        session()->forget('quote_cart');
        $carts = session('quote_cart');

        if (empty($carts) && empty($edit->quote_id)) {
            $products = DB::connection('mysql2')
                ->table('products AS p')
                ->join('order_products AS op', 'p.product_id', 'op.opro_pid')
                ->where('opro_oid', @$enquiry->order_id)
                ->get();

            $carts = [];
            if (!$products->isEmpty()) :
                foreach ($products as $p) {

                    $LastPrice = 0;
                    $approved = DB::table('quotes')->leftjoin("quote_products", "quote_products.qpro_qid", "quotes.quote_id")->where('quote_uid', $uid)->where("qpro_pid", $p->product_id)->where('qpro_is_approved', 'Y')->orderBy("qpro_id", "DESC")->first();
                    if ($approved != "") {
                        $LastPrice = $approved->qpro_price;
                    }

                    $carts[$p->product_id] = [
                        'qty'       => $p->opro_qty,
                        'price'     => $p->product_mrp,
                        'remark'    => '',
                        'verified'  => 'N',
                        'LastPrice' => $LastPrice,
                        'BuyerID' => $uid,
                    ];
                }
                session(['quote_cart' => $carts]);
            endif;
        } elseif (empty($carts)) {
            $products = DB::table('products AS p')
                ->join('quote_products AS qp', 'p.product_id', 'qp.qpro_pid', 'qp.qpro_remark')
                ->where('qpro_qid', $edit->quote_id)
                ->get();

            $carts = [];
            foreach ($products as $p) {

                $LastPrice = 0;
                $approved = DB::table('quotes')->leftjoin("quote_products", "quote_products.qpro_qid", "quotes.quote_id")->where('quote_uid', $uid)->where("qpro_pid", $p->product_id)->where('qpro_is_approved', 'Y')->orderBy("qpro_id", "DESC")->first();
                if ($approved != "") {
                    $LastPrice = $approved->qpro_price;
                }

                $carts[$p->product_id] = [
                    'qty'       => $p->qpro_qty,
                    'price'     => $p->qpro_price,
                    'remark'    => $p->qpro_remark,
                    'verified'  => $p->qpro_is_approved,
                    'LastPrice' => $LastPrice,
                    'BuyerID' => $uid,
                ];
            }
            session(['quote_cart' => $carts]);
        }

        $countries   = DB::table('countries')->where('country_is_deleted', 'N')->get();
        $allproducts = DB::table('products')->where('product_is_deleted', 'N')->get();
        $currencies = DB::table('currencies')->where('currency_is_deleted', 'N')->get();
        $ports      = DB::table('ports')->where('port_is_deleted', 'N')->get();


        $ranges     = DB::connection('mysql2')->table('ranges')->where('range_is_deleted', 'N')->get();
        $categories = DB::connection('mysql2')->table('categories')->where('category_parent', 0)->get();
        $finishes   = DB::table('finish')->where('finish_is_deleted', 'N')->get();

        $title      = "Add Quote";
        $page       = "add_quote";
        $data       = compact('page', 'title', 'buyers', 'countries', 'currencies', 'allproducts', 'ports', 'uid', 'edit', 'ranges', 'categories', 'finishes');
        return view('frontend/layout', $data);
    }


    public function NewAdd(Request $request)
    {

        $edit = [];
        if (!empty($id)) {
            $edit = Qmodel::where('quote_id', $id)->orderBy('quote_id', 'DESC')->first();
        }

        $profile = Query::get_profile();

        // print_r($profile->user_role)

        if ($request->isMethod('post')) {

            $record = $request->input('record');
            DB::connection('mysql2')->insert("INSERT INTO ghp_orders SET order_uid = '" . $record['quote_uid'] . "'");
            $resp = DB::connection('mysql2')->getPdo()->lastInsertId();

            $record['quote_updated_on'] = date('Y-m-d H:i:s', time());
            $record['quote_enq_id']     = $resp;
            $record['quote_added_by']   = $profile->user_role;

            if (empty($edit->quote_id)) {
                $last  = Qmodel::where('quote_is_deleted', 'N')->select(DB::raw('MAX(quote_number) AS max_id'))->first();
                $record['quote_number']     = $last->max_id + 1;
                $record['quote_created_on'] = date('Y-m-d H:i:s', time());

                $record = array_filter($record);
                Qmodel::insert($record);
                $id = DB::getPdo()->lastInsertId();
            } else {
                $last  = Qmodel::where('quote_is_deleted', 'N')
                    ->select('quote_uid', 'quote_enq_id', 'quote_enq_id', 'quote_number', 'quote_currency', 'quote_price_term', 'quote_delivery_port', 'quote_delivery_days', 'quote_payment', 'quote_date', 'quote_version', 'quote_origin_country', 'quote_first_destination_country', 'quote_loading_port', 'quote_discharge_port', 'quote_reciept_port')
                    ->where('quote_id', $edit->quote_id)
                    ->first()->toArray();

                // print_r($last); die;

                $record = array_merge($record, (array) $last);

                $record['quote_version'] = $last['quote_version'] + 1;
                $record['quote_number']  = $last['quote_number'];

                $record = array_filter($record);

                Qmodel::insert($record);
                $id = DB::getPdo()->lastInsertId();
            }

            DB::table('quote_products')->where('qpro_qid', $id)->delete();
            $carts = session('quote_cart');

            if (!empty($carts)) {
                foreach ($carts as $pid => $data) {
                    extract($data);

                    $arr = [
                        'qpro_qid'          => $id,
                        'qpro_pid'          => $pid,
                        'qpro_price'        => $data['price'],
                        'qpro_remark'       => @$data['remark'],
                        'qpro_qty'          => $data['qty'],
                        'qpro_is_approved'  => "Y"
                    ];

                    DB::table('quote_products')->insert($arr);

                    $bpro_exists = Bproduct::where('bpro_pid', $pid)->where('bpro_uid', $record['quote_uid'])->count();
                    if (!$bpro_exists) {
                        $bpro_arr = [
                            'bpro_pid'  => $pid,
                            'bpro_uid'  => $record['quote_uid']
                        ];

                        Bproduct::insert($bpro_arr);
                    }
                }
            }


            $products = QuoteProduct::with('product')->where('qpro_qid', $id)->where('qpro_is_approved', 'Y')->get();
            $quote    = Qmodel::find($id);


            /*$input['pi_qid']    = $id;
            $input['pi_date']    = date("Y-m-d");
            $input['pi_order_ref']    = '';

            $input['pi_show_nw']    = 'N';
            $input['pi_show_gw']    = 'N';
            $input['pi_created_on'] = date("Y-m-d H:i:s", time());
            $id    = PI::insertGetId( $input );
            $mess  = "A new PI has been created.";

            PIProduct::where('pipro_pi_id', $id)->delete();
            foreach($products as $p) {
                $bpro        = Bproduct::where('bpro_uid', $quote->quote_uid)->where('bpro_pid', $p->qpro_pid)->first();

                $arr = [
                    'pipro_pi_id'       => $id,
                    'pipro_pid'         => $p->qpro_pid,
                    'pipro_qty'         => $p->qpro_qty,
                    'pipro_sku'         => @$bpro->bpro_sku,
                    'pipro_barcode'     => @$bpro->bpro_barcode,
                    'pipro_buyer_desc'  => @$bpro->bpro_description,
                    'pipro_fsc_status'  => "",
                    'pipro_species'     => "",
                ];
                PIProduct::insert( $arr );
            }*/

            session()->forget('pi_cart');
            session()->forget('quote_cart');

            return redirect(url('quote'));
        }


        $buyers = DB::table('users')
            ->where('user_role', 'buyer')
            ->where('user_is_deleted', 'N')
            ->get();

        //session()->forget('quote_cart');
        $carts = session('quote_cart');
        $countries   = DB::table('countries')->where('country_is_deleted', 'N')->get();
        $allproducts = DB::table('products')->where('product_is_deleted', 'N')->get();
        $currencies = DB::table('currencies')->where('currency_is_deleted', 'N')->get();
        $ports      = DB::table('ports')->where('port_is_deleted', 'N')->get();


        $ranges     = DB::connection('mysql2')->table('ranges')->where('range_is_deleted', 'N')->get();
        $categories = DB::connection('mysql2')->table('categories')->where('category_parent', 0)->get();
        $finishes   = DB::table('finish')->where('finish_is_deleted', 'N')->get();

        $title = "Add Quote";
        $page  = "add_new_quote";
        $data  = compact('page', 'title', 'buyers', 'countries', 'currencies', 'allproducts', 'ports', 'ranges', 'categories', 'finishes');
        return view('frontend/layout', $data);
    }

    public function index(Request $request, $id = null)
    {

        $dbprefix = env('DB_PREFIX');

        if (isset($request->check)) {
            foreach ($request->check as $CHD) {
                $QModel = Qmodel::where("quote_enq_id", $CHD)->delete();
            }

            return redirect("quote");
        }

        $query = DB::table('quotes AS q')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->whereRaw($dbprefix . 'q.quote_version = (SELECT MAX(quote_version) FROM ' . $dbprefix . 'quotes WHERE `quote_number` = ' . $dbprefix . 'q.quote_number)')
            ->where('q.quote_is_deleted', 'N');

        $profile = Query::get_profile();

        if ($profile->user_role == "buyer") {
            $query = $query->where('quote_uid', $profile->user_id);
        }

        if (@$_GET['SearchQuery'] != "") {
            $id = (int)str_replace("GHP_QT_", '', $_GET['SearchQuery']);
            $query = $query->where('quote_number', "like", "%" . $id . "%");
        }

        if (@$_GET['SearchName'] != "") {
            $query = $query->where('user_name', "like", "%" . $_GET['SearchName'] . "%");
        }

        if (@$_GET['PiCreated'] != "") {
            $query = $query->where('status', $_GET['PiCreated']);
        }

        $NrC = $query->orderBy("quote_date", "DESC")->get();

        foreach ($NrC as $N) {
            $CountPI = DB::table('purchase_invoices')->where("pi_qid", $N->quote_id)->count();
            if ($CountPI > 0) {
                $AB = Qmodel::find($N->quote_id);
                $AB->status = 1;
                $AB->save();
            }
        }

        if ($profile->user_role == "buyer") {
            $query = $query->where('status', 0);
        }

        $records = $query->orderBy("quote_date", "DESC")->paginate(15);

        $title     = "View Quote";
        $page     = "view_quotes";
        $data = compact('page', 'title', 'records');
        return view('frontend/layout', $data);
    }

    public function single($id = null)
    {

        $record     = DB::table('quotes AS q')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->where('quote_id', $id)->first();

        $products   = DB::table('quote_products AS qp')
            ->join('products AS p', 'qp.qpro_pid', 'p.product_id')
            ->where('p.product_is_deleted', 'N')
            ->where('qp.qpro_qid', $id)
            ->get();

        $quotes = Qmodel::where('quote_number', $record->quote_number)->get();

        $proArr = [];
        foreach ($quotes as $q) {
            foreach ($products as $key => $pro) {
                $qprods = DB::table('quote_products')->where('qpro_qid', $q->quote_id)->where('qpro_pid', $pro->qpro_pid)->get();
                foreach ($qprods as $qp) {
                    $proArr[] = $qp;
                }
                $products[$key]->qpros = $proArr;
            }
        }

        // echo '<pre>';
        // print_r( $products );
        // echo '</pre>';

        $title         = "Quotation Info";
        $page       = "single-quote";
        $data       = compact('page', 'title', 'record', 'products', 'quotes');
        return view('frontend/layout', $data);
    }

    public function revisions($id = null)
    {

        $record     = DB::table('quotes AS q')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->where('quote_id', $id)->first();

        $products   = DB::table('quote_products AS qp')
            ->join('products AS p', 'qp.qpro_pid', 'p.product_id')
            ->where('p.product_is_deleted', 'N')
            ->where('qp.qpro_qid', $id)
            ->get();

        $quotes = Qmodel::where('quote_number', $record->quote_number)->get();

        $proArr = [];
        foreach ($quotes as $q) {
            foreach ($products as $key => $pro) {
                $qprods = DB::table('quote_products')->where('qpro_qid', $q->quote_id)->where('qpro_pid', $pro->qpro_pid)->get();
                foreach ($qprods as $qp) {
                    $proArr[] = $qp;
                }
                $products[$key]->qpros = $proArr;
            }
        }

        // echo '<pre>';
        // print_r( $products );
        // echo '</pre>';

        $title         = "Quotation Revisions";
        $page       = "quote_revisions";
        $data       = compact('page', 'title', 'record', 'products', 'quotes');
        return view('frontend/layout', $data);
    }

    public function send(Request $request, $id)
    {

        $records = DB::connection('mysql2')
            ->table('orders AS o')
            ->join('users AS u', 'o.order_uid', 'u.user_id')
            ->paginate(30);
        $vendors = Umodel::where('user_role', 'vendor')->get();
        $products = DB::connection('mysql2')
            ->table('products AS p')
            ->join('order_products AS op', 'p.product_id', 'op.opro_pid')
            ->where('opro_oid', $records[0]->order_id)
            ->get();

        if ($request->isMethod('post')) {

            $post = $request->input();

            $checks = $post['check'];

            foreach ($checks as $key => $value) {
                // print_r($value); die;

                $record['venq_uid'] = $value;
                $record['venq_enqid'] = $id;
                Vemodel::insert($record);
                $resp = DB::getPdo()->lastInsertId();

                if (!empty($resp)) {

                    $vpro['vpro_enqid'] = $resp;
                    foreach ($products as $key2 => $pro) {
                        $vpro['vpro_pid'] = $pro->product_id;
                        $vpro['vpro_qty'] = $pro->opro_qty;
                    }
                    Vepmodel::insert($vpro);
                }
            }
            return redirect('vendor-enquiry');
        }
        $title      = "Quotation Info";
        $page       = "send_to_vender";
        $data       = compact('page', 'title', 'records', 'vendors');
        return view('frontend/layout', $data);
    }

    public function get_quote_products(Request $request)
    {
        $quoteProducts = Bproduct::with('product')->where('bpro_uid', $request->id)->get();
        // $re = $request->all();
        $re = [
            'msg' => 'Buyer\'s products has been fetched.',
            'data' => $quoteProducts
        ];
        return response()->json($re);
    }
}
