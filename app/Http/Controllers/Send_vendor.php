<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Query;
use App\Models\QuoteModel as Qmodel;
use App\Models\UserModel as Umodel;
use App\Models\VenqryModel as Vemodel;
use App\Models\VenqproModel as Vepmodel;
use App\Models\Bproduct;
use DB;

class Send_vendor extends BaseController
{

    public function index(Request $request, $id = null)
    {

        $vendors = Umodel::with('vendor')->where('user_role', 'vendor')->get();
        if ($request->isMethod('post')) {

            $post = $request->input();
            $checks = $post['quote_check'];
            $check_ids = implode(',', $checks);
            //            DB::connection('mysql')->table('vendor_enquiries')->whereNotIn('venq_uid', $checks)->delete();
            DB::connection('mysql')->table('vendor_enquiries')->where("venq_enqid", $id)->whereNotIn('venq_uid', $checks)->delete();

            foreach ($checks as $key => $value) {
                $is_exists = DB::connection('mysql')->table('vendor_enquiries')->where('venq_enqid', $id)->where('venq_uid', $value)->count();
                // print_r($is_exists); die;
                if (!$is_exists) {
                    $record['venq_uid'] = $value;
                    $record['venq_enqid'] = $id;

                    Vemodel::updateOrInsert($record);
                }
            }
            return redirect('assign-vendor/' . $id);
        }

        $title     = "Vendor List";
        $page     = "send_to_vendor_list";
        $data = compact('page', 'title', 'vendors', 'id');
        return view('frontend/layout', $data);
    }

    public function send(Request $request, $id)
    {

        $vendors = DB::connection('mysql')
            ->table('vendor_enquiries AS ve')
            ->join('users AS u', 've.venq_uid', 'u.user_id')
            ->where('ve.venq_enqid', $id)
            ->paginate(30);
        // print_r($vendors); die;
        $products = DB::connection('mysql')
            ->table('quote_products AS qp')
            ->join('products AS p', 'qp.qpro_pid', 'p.product_id')
            ->where('qp.qpro_qid', $id)
            ->where('qp.qpro_is_approved', 'Y')
            ->get();

        if ($request->isMethod('post')) {
            $enquiry = DB::connection('mysql')
                ->table('vendor_enquiries')
                ->where('venq_enqid', $id)
                ->get();
            foreach ($enquiry as $rec => $enq) {
                $enqArr[] = $enq->venq_id;
            }

            DB::connection('mysql')
                ->table('vendor_enqproducts')
                ->whereIn('vpro_enqid', $enqArr)
                ->delete();

            $post = $request->input();

            $checks = $post['check'];
            $pro_qty = $post['pro_qty'];

            foreach ($checks as $key => $data) {

                $product_id = $key;
                foreach ($data as $key2 => $value) {

                    $qty = $pro_qty[$key][$value];
                    $qty = implode('', $qty);

                    $enquiry = DB::connection('mysql')
                        ->table('vendor_enquiries')
                        ->where('venq_uid', $value)
                        ->where('venq_enqid', $id)
                        ->first();

                    foreach ($pro_qty[$key][$value] as $Pars => $QTTY) {
                        $record['vpro_enqid'] = $enquiry->venq_id;
                        $record['vpro_pid'] = $id;
                        $record['vpro_pid'] = $product_id;
                        $record['vpro_qty'] = $QTTY;
                        $record['vpro_part'] = $Pars;
                        Vepmodel::insert($record);
                    }
                }

                $resp = DB::getPdo()->lastInsertId();
            }
            return redirect('vquotestatus');
        }
        $title      = "Quotation Info";
        $page       = "quote_products_to vendor";
        $data       = compact('page', 'title', 'id', 'vendors', 'products');
        return view('frontend/layout', $data);
    }
    public function single($id = null)
    {

        $record     = DB::table('quotes AS q')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->where('quote_id', $id)->first();

        $products   = DB::table('quote_products AS qp')
            ->join('products AS p', 'qp.qpro_pid', 'p.product_id')
            ->where('p.product_is_deleted', 'N')
            ->where('qp.qpro_qid', $id)
            ->get();

        $quotes = Qmodel::where('quote_number', $record->quote_number)->get();

        $proArr = [];
        foreach ($quotes as $q) {
            foreach ($products as $key => $pro) {
                $qprods = DB::table('quote_products')->where('qpro_qid', $q->quote_id)->where('qpro_pid', $pro->qpro_pid)->get();
                foreach ($qprods as $qp) {
                    $proArr[] = $qp;
                }
                $products[$key]->qpros = $proArr;
            }
        }

        // echo '<pre>';
        // print_r( $products );
        // echo '</pre>';

        $title         = "Quotation Info";
        $page       = "single-quote";
        $data       = compact('page', 'title', 'record', 'products', 'quotes');
        return view('frontend/layout', $data);
    }

    public function revisions($id = null)
    {

        $record     = DB::table('quotes AS q')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->where('quote_id', $id)->first();

        $products   = DB::table('quote_products AS qp')
            ->join('products AS p', 'qp.qpro_pid', 'p.product_id')
            ->where('p.product_is_deleted', 'N')
            ->where('qp.qpro_qid', $id)
            ->get();

        $quotes = Qmodel::where('quote_number', $record->quote_number)->get();

        $proArr = [];
        foreach ($quotes as $q) {
            foreach ($products as $key => $pro) {
                $qprods = DB::table('quote_products')->where('qpro_qid', $q->quote_id)->where('qpro_pid', $pro->qpro_pid)->get();
                foreach ($qprods as $qp) {
                    $proArr[] = $qp;
                }
                $products[$key]->qpros = $proArr;
            }
        }

        // echo '<pre>';
        // print_r( $products );
        // echo '</pre>';

        $title         = "Quotation Revisions";
        $page       = "quote_revisions";
        $data       = compact('page', 'title', 'record', 'products', 'quotes');
        return view('frontend/layout', $data);
    }
}
