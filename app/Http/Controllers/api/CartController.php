<?php

namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller;
use App\Models\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $carts = Cart::with('product')->where('type', $request->type)->latest()->get();
        return response()->json($carts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cart = new Cart();
        $cart->user_id = $request->userId;
        $cart->product_id = $request->id;
        $cart->qty = 1;
        $cart->save();

        $carts = Cart::with('product')->latest()->get();

        return response()->json(compact('cart', 'carts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        if ($cart->qty > 0) {
            $cart->user_id = $request->userId;
            $cart->qty = $request->qty;

            if (!empty($request->margin)) {
                $cart->margin = $request->margin;
            }

            if (!empty($request->moq)) {
                $cart->moq = $request->moq;
            }

            if (!empty($request->remarks)) {
                $cart->remarks = $request->remarks;
            }

            $cart->save();
        } else {
            //$cart->delete();
        }

        $carts = Cart::with('product')->latest()->get();
        return response()->json($carts);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        $cart->delete();

        $carts = Cart::with('product')->latest()->get();
        return response()->json($carts);
    }
}
