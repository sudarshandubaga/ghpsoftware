<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use Session;

class Unit extends BaseController
{
    public function index(Request $request, $id = null) {
        if($request->isMethod('post')) {
    		$input = $request->input('record');
    		if(!empty($input)) {
	    		if(empty($id)) {
	    		    
	    		    $CheckDuplicate = DB::table('units')
                                                    ->where("unit_name", $input["unit_name"])
                                                    ->where("unit_short_name", $input["unit_short_name"])
                                                    ->count();
                    
                    if($CheckDuplicate > 0){
                        Session::flash('Danger', 'Unit Name & Code Already Exists');
                        return redirect('unit');
                    }
                    
                    
		    		DB::table('units')->insert($input);
		    		$id = DB::getPdo()->lastInsertId();
		    		
		    		Session::flash('Success', 'Record Added Successfully');
		    		
		    	} else {
		    		DB::table('units')->where('unit_id', $id)->update($input);
		    		
		    		Session::flash('Success', 'Record Updated Successfully');
		    	}
		    }

		    $check = $request->input('check');
		    if(!empty($check)) {
		    	DB::table('units')->whereIn('unit_id', $check)->update(['unit_is_deleted' => 'Y']);
		    	Session::flash('Success', 'Record Deleted Successfully');
		    }
		    
		    return redirect('unit');
    	}

    	$records = DB::table('units AS p')
    				->where('unit_is_deleted', 'N');
		if(!empty($request->unit_name)){
			$records->where('unit_name' , "like", "%".$request->unit_name."%");
		}
    	$records = $records->paginate(10);

    	$edit = array();
    	if(!empty($id)) {
    		$edit = DB::table('units')->where('unit_id', $id)->first();
    	}

        $title 	= "Unit | GHP Software";
        $page 	= "unit";
        $data 	= compact('page', 'title', 'records', 'edit');
        return view('frontend/layout', $data);
    }

	public function exportCsv(Request $request) {
        $fileName = 'forwarder.csv';       
        // $tasks = SurveyHistory::with('user','survey')->get()->toArray();
        $records = DB::table('units AS p')
    				->where('unit_is_deleted', 'N');
		if(!empty($request->unit_name)){
			$records->where('unit_name' , "like", "%".$request->unit_name."%");
		}
		$lists = $records->get()->toArray();
        $listArr = [];
        
        foreach($lists as $key => $l){
            $listArr[$key]['Unit Name'] = !empty($l->unit_name) ? $l->unit_name : '';
            $listArr[$key]['Unit short name'] = @$l->unit_short_name;
            
        }
        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("unitList" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }


    public  function array2csv(array &$array)
    {
       if (count($array) == 0) {
         return null;
       }
       ob_start();
       $df = fopen("php://output", 'w');
       fputcsv($df, array_keys(reset($array)));
       foreach ($array as $row) {
          fputcsv($df, $row);
       }
       fclose($df);
       return ob_get_clean();
    }
    public function download_send_headers($filename) {
    // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
}

