<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use Session;

class ShippingLine extends BaseController
{
    public function index(Request $request, $id = null) {
    	if($request->isMethod('post')) {
    		$input = $request->input('record');
    		if(!empty($input)) {
	    		if(empty($id)) {
	    		    
	    		    
	    		    $CheckDuplicate = DB::table('shipping_line')
                                                    ->where("shipping_line_name", $input["shipping_line_name"])
                                                    ->count();
                    
                    if($CheckDuplicate > 0){
                        Session::flash('Danger', 'Shipping Line Name Already Exists');
                        return redirect('shipping-line');
                    }
                    
		    		DB::table('shipping_line')->insert($input);
		    		$id = DB::getPdo()->lastInsertId();
		    		
		    		Session::flash('Success', 'Record Added Successfully');
		    		
		    	} else {
		    		DB::table('shipping_line')->where('shipping_line_id', $id)->update($input);
		    		Session::flash('Success', 'Record Updated Successfully');
		    	}
		    }

		    $check = $request->input('check');
		    if(!empty($check)) {
		    	DB::table('shipping_line')->whereIn('shipping_line_id', $check)->update(['is_deleted' => 'Y']);
		    	Session::flash('Success', 'Record Deleted Successfully');
		    }

	    	return redirect('shipping-line');
    	}

    	$edit = array();
    	if(!empty($id)) {
    		$edit = DB::table('shipping_line')->where('shipping_line_id', $id)->first();
    	}

    	$records = DB::table('shipping_line')->where('is_deleted', 'N');
		if(!empty($request->shipping_line_name)){
			$records->where('shipping_line_name' , "like", "%".$request->shipping_line_name."%");
		}
		$records = $records->paginate(10);

        $title 	= "Shipping Line | GHP Software";
        $page 	= "shipping_line";
        $data 	= compact('page', 'title', 'records', 'edit');
        return view('frontend/layout', $data);
    }

	public function exportCsv(Request $request) {
        $fileName = 'shippingline.csv';       
        // $tasks = SurveyHistory::with('user','survey')->get()->toArray();
        $records = DB::table('shipping_line')->where('is_deleted', 'N');
		if(!empty($request->shipping_line_name)){
			$records->where('shipping_line_name' , "like", "%".$request->shipping_line_name."%");
		}
		$lists = $records->get()->toArray();
        $listArr = [];
        
        foreach($lists as $key => $l){
            $listArr[$key]['Name'] = !empty($l->shipping_line_name) ? $l->shipping_line_name : '';
            $listArr[$key]['Mail'] = @$l->shipping_line_mail;
            $listArr[$key]['Web Address'] = @$l->shipping_line_web_add;          
            
        }
        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("shippingLineList" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }


    public  function array2csv(array &$array)
    {
       if (count($array) == 0) {
         return null;
       }
       ob_start();
       $df = fopen("php://output", 'w');
       fputcsv($df, array_keys(reset($array)));
       foreach ($array as $row) {
          fputcsv($df, $row);
       }
       fclose($df);
       return ob_get_clean();
    }
    public function download_send_headers($filename) {
    // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
}

