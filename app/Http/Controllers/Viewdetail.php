<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class Viewdetail extends BaseController
{
    public function index(Request $request, $code = NULL) {
        $title 		= "View-detail | GHP Software";
        $record     = DB::table('products')->where('product_code', $code)->first();
        $page       = "view-detail";
        $data       = compact('page', 'title', 'record');
        return view('frontend/layout', $data);
    }
}

