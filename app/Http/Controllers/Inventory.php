<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Models\Query;
use App\Models\QuoteModel as Quote;
use App\Models\QuoteProduct;
use App\Models\PIModel as PI;
use App\Models\PIProduct;
use App\Models\POProduct;
use App\Models\Bproduct;
use App\Models\UserModel;
use App\Models\PoInvoiceProduct;
use DB;

class Inventory extends BaseController
{

    public function ViewAll(Request $request, $id = null)
    {
        $dbprefix = env('DB_PREFIX');

        $query = POProduct::leftjoin('purchase_orders AS PO', 'PO.po_id', 'po_products.popro_po_id')
            ->leftjoin('purchase_invoices', 'purchase_invoices.pi_id', 'PO.po_pi_id')
            ->leftjoin('quotes AS PIQ', 'PIQ.quote_id', 'purchase_invoices.pi_qid')
            ->leftjoin('products', 'products.product_id', 'po_products.popro_pid')
            ->leftjoin('vendor_quotes AS q', 'q.vquote_id', 'PO.po_qid')
            ->leftjoin('users AS u', 'q.vquote_uid', 'u.user_id')
            ->leftjoin('currencies AS c', 'q.vquote_currency', 'c.currency_id')
            ->where('po_is_deleted', 'N');

        $FileterApplied = 0;
        if (isset($request->SearchByItemCode) && $request->SearchByItemCode != "") {
            $FileterApplied = 1;
            $query = $query->where("product_code", $request->SearchByItemCode);
        }

        if (isset($request->SearchByProTitle) && $request->SearchByProTitle != "") {
            $FileterApplied = 1;
            $query = $query->where("product_name", "like", "%" . $request->SearchByProTitle . "%");
        }
        $buyers = UserModel::where('user_role', 'buyer')->where('user_is_deleted', 'N')->get();
        if (isset($request->SearchBuyer) && $request->SearchBuyer != "") {
            $FileterApplied = 1;
            $query->where('quote_uid', "=", $request->SearchBuyer);
        }

        /*if(isset($request->SearchByBuyer) && $request->SearchByBuyer != ""){
            $FileterApplied = 1;
            $query = $query->where("user_name", "like", "%".$request->SearchByBuyer."%");
        }*/

        if ($FileterApplied == 1) {
            $records = $query->get();
        } else {
            $records = $query->paginate(15);
        }



        $title     = "Inventory";
        $page     = "view_inventory";
        $data = compact('page', 'title', 'records', 'FileterApplied', 'buyers');
        return view('frontend/layout', $data);
    }
    public function exportCsv(Request $request)
    {
        $fileName = 'reports.csv';
        $query = POProduct::
            // select('product_code','product_name','product_cbm','product_mrp','pi_id','pi_order_ref','popro_sku','popro_barcode','user_name','po_id','popro_pid','product_finish_main')
            leftjoin('purchase_orders AS PO', 'PO.po_id', 'po_products.popro_po_id')
            ->leftjoin('purchase_invoices', 'purchase_invoices.pi_id', 'PO.po_pi_id')
            ->leftjoin('quotes AS PIQ', 'PIQ.quote_id', 'purchase_invoices.pi_qid')
            ->leftjoin('products', 'products.product_id', 'po_products.popro_pid')
            ->leftjoin('vendor_quotes AS q', 'q.vquote_id', 'PO.po_qid')
            ->leftjoin('users AS u', 'q.vquote_uid', 'u.user_id')
            ->leftjoin('currencies AS c', 'q.vquote_currency', 'c.currency_id')
            ->where('po_is_deleted', 'N');

        $FileterApplied = 0;
        if (isset($request->SearchByItemCode) && $request->SearchByItemCode != "") {
            $query = $query->where("product_code", $request->SearchByItemCode);
        }

        if (isset($request->SearchByProTitle) && $request->SearchByProTitle != "") {
            $query = $query->where("product_name", "like", "%" . $request->SearchByProTitle . "%");
        }
        $buyers = UserModel::where('user_role', 'buyer')->where('user_is_deleted', 'N')->get();
        if (isset($request->SearchBuyer) && $request->SearchBuyer != "") {
            $FileterApplied = 1;
            $query->where('quote_uid', "=", $request->SearchBuyer);
        }
        $lists = $query->get()->toArray();
        $listArr = [];
        foreach ($lists as $key => $list) {
            $PINumber = sprintf("%s-%03d", 'GHP-201819', $list['pi_id']);
            $web_edit = \DB::connection('mysql2')->table('products')->where('product_id', $list['popro_pid'])->first();
            $categories = \DB::connection('mysql2')->table('categories')->where('category_id', $web_edit->product_category)->first();
            $SubCategories = \DB::connection('mysql2')->table('categories')->where('category_id', $web_edit->product_subcategory)->first();
            $SubCategories1 = \DB::connection('mysql2')->table('categories')->where('category_id', $web_edit->product_subcategory2)->first();
            // $Finish = \DB::table('finish')->where('finish_id', $list['product_finish_main'])->first();
            $Buyer = \DB::table('users')->where('user_id', $list['quote_uid'])->first();
            $GHPInvoice = \DB::table('ghp_invoice')->where('exs_ghp_invoice_po_id', $list['po_id'])->first();
            $Logsitic = \DB::table('logistics')->where("exs_logistics_pi", "like", "%" . $PINumber . "%")->first();
            $BuyerProductsData = \DB::table('buyer_products')->where("bpro_uid", $list['quote_uid'])->where("bpro_pid", $list['popro_pid'])->first();
            $products = PIProduct::join('quote_products AS qpro', 'pipro_pid', 'qpro_pid')->where('pipro_pi_id', $list['pi_id'])->where('qpro_qid', $list['quote_id'])->where("pipro_pid", $list['popro_pid'])->first();
            $InvoicedProducts = PoInvoiceProduct::where('invoice_po_no', $list['po_id'])->where("exs_po_invoice_products_product_id", $list['popro_pid'])->first();
            $POproducts = POProduct::where('popro_po_id', $list['po_id'])->where("popro_pid", $list['popro_pid'])->first();
            $VenQuote = DB::table('vquote_products')->where('vqpro_pid', $POproducts['popro_pid'])->where('vqpro_qid', $list['po_qid'])->first();
            $Log = DB::table('logistics')->where("exs_logistics_pi", $list['pi_id'])->first();
            $ShipQty = PIProduct::where("pipro_pi_id", $list['pi_id'])->first();
            $MatType1 = \DB::table('material_type')->where("material_type_id", $list['materialtype1'])->first();
            $MatType2 = \DB::table('material_type')->where("material_type_id", $list['materialtype2'])->first();
            $Mtrl1 = \DB::table('material')->where("material_id", $list['material1'])->first();
            $Mtrl2 = \DB::table('material')->where("material_id", $list['material2'])->first();
            $Fnsh1 = \DB::table('finish')->where("finish_id", $list['finish1'])->first();
            $Fnsh2 = \DB::table('finish')->where("finish_id", $list['finish2'])->first();
            $status = '';
            if (!empty($Logsitic)) {
                if ($Logsitic->track_status == 1) {
                    $status = 'Open';
                } elseif ($Logsitic->track_status == 2) {
                    $status =  'Custom Clearance Process';
                } else {
                    $status =  'Shipped';
                }
            }
            $pip_qty = 0;
            if (!empty($InvoicedProducts)) {
                $pip_qty = $InvoicedProducts->pip_qty;
            }
            $listArr[$key]['product_code'] = $list['product_code'];
            $listArr[$key]['product_name'] = $list['product_name'];
            $listArr[$key]['product_cbm'] = number_format($list['product_cbm'], 3);
            $listArr[$key]['product_mrp'] = $list['product_mrp'];
            $listArr[$key]['Buyer Name'] = !empty($Buyer) ? $Buyer->user_name : '';
            $listArr[$key]['GHP Pi'] = sprintf("%s-%03d", 'GHP-202122', $list['pi_id'] + 100);
            $listArr[$key]['Order Reference'] =  $list['pi_order_ref'] != "" ? $list['pi_order_ref'] : '-';

            $listArr[$key]['Confirmation Date'] =  $list['pi_confirm_date'] != "" ? $list['pi_confirm_date'] : '-';

            $listArr[$key]['Delivery Date'] =  $list['pi_delivery_date'] != "" ? $list['pi_delivery_date'] : '-';


            $listArr[$key]['PI# QTY'] =  !empty($products) ? $products->qpro_qty : '';
            $listArr[$key]['Buyer Sku'] = $list['popro_sku'];
            $listArr[$key]['Buyer Barcode'] =  $list['popro_barcode'];
            $listArr[$key]['PI# Price'] = !empty($products) ? '$' . number_format($products->qpro_price, 2) : '';
            $listArr[$key]['Total CBM'] =  !empty($products) ? $list['product_cbm'] * $products->qpro_qty : '';
            $listArr[$key]['Amount Buyer PI#'] =  '$' . number_format((@$products->qpro_price * @$products->qpro_qty), 2);
            $listArr[$key]['Assign Vendor Name'] =  $list['user_name'];
            $listArr[$key]['GHP Vendor PO'] =  'GHP-202122-' . str_pad($list['po_id'] + 100, 2, "0", STR_PAD_LEFT);
            $listArr[$key]['Vendor Price PO'] =  !empty($vqpro_price) ? '$' . number_format($VenQuote->vqpro_price, 2) : '';
            $listArr[$key]['Shipped QTY'] =  $pip_qty;
            $listArr[$key]['Balance (Due) QTY.'] = !empty($products) ? $products->qpro_qty : 0 - $pip_qty;
            $listArr[$key]['Shipped CBM'] = !empty($InvoicedProducts) ? $InvoicedProducts->pip_TotalMeas : '';
            $listArr[$key]['Status'] = $status;
            $listArr[$key]['GHP Invoice No.'] = !empty($InvoicedProducts) ? $InvoicedProducts->exs_po_invoice_products_inv_id : '-';
            $listArr[$key]['Container No.'] = !empty($Logsitic) ? $Logsitic->ContainerNr : '-';
            $listArr[$key]['E.T.D.'] =  !empty($Logsitic) ? date("d-M-Y", strtotime($Logsitic->AgreedETD)) : '-';
            $listArr[$key]['E.T.A'] = !empty($Logsitic) ? date("d-M-Y", strtotime(@$Logsitic->VesselSailingETA)) : '-';
        }
        // dd($listArr);        
        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("inventory_reports" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }
    public  function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }
    public function download_send_headers($filename)
    {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
}
