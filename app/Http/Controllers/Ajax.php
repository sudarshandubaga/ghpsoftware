<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class Ajax extends BaseController
{
    public function index(Request $request, $action = NULL)
    {
        $post  = $request->input();

        $param = compact('post');
        $re = call_user_func_array(array($this, $action), $param);

        return response()->json($re);
    }

    public function upload_image(Request $request)
    {

        if ($request->hasFile('file')) {
            $image = $request->file('file');
            $name = uniqid() . '.' . $image->getClientOriginalExtension();
            $destinationPath = 'uploads';
            $image->move($destinationPath, $name);

            $re = array(
                'status'    => TRUE,
                'location' => url('/') . '/' . $destinationPath . '/' . $name
            );
        } else {
            $re = array(
                'status'    => FALSE,
                'message'   => 'Error'
            );
        }

        return response()->json($re);
    }

    // public function approve_qpro($qid, $pid) {
    //     DB::table('quote_products')->where('qpro_qid', $qid)->where('qpro_pid', $pid)->update(['qpro_is_approved' => 'Y']);
    //
    //     $re = array(
    //         'status'    => TRUE,
    //     );
    //
    //     return response()->json($re);
    // }

    public function save_bproducts($post)
    {
        $id = $post['bpro_id'];

        $arr = [
            'bpro_sku'          => $post['sku'],
            'bpro_barcode'      => $post['barcode'],
            'bpro_description'  => $post['desc'],
            'salesman1_comm'  => $post['comm1'],
            'salesman2_comm'  => $post['comm2'],
            'salesman3_comm'  => @$post['comm3'],
        ];

        DB::table('buyer_products')->where('bpro_id', $id)->update($arr);

        $re = [
            'status' => TRUE,
        ];

        return $re;
    }

    public function pi_cart_update($post)
    {

        session(["pi_cart" => $post['picart']]);

        $re = [
            'status' => TRUE,
        ];

        return $re;
    }

    public function approve_qpro($qid, $pid)
    {
        DB::table('quote_products')->where('qpro_qid', $qid)->where('qpro_pid', $pid)->update(['qpro_is_approved' => 'Y']);
        $count      = DB::table('quote_products')->where('qpro_qid', $qid)->count();
        $approved   = DB::table('quote_products')->where('qpro_qid', $qid)->where('qpro_is_approved', 'Y')->count();
        if ($count == $approved) {
            DB::table('quotes')->where('quote_id', $qid)->update(['quote_is_approved' => 'Y']);
        }
        $re = array(
            'status'    => TRUE,
        );

        return response()->json($re);
    }

    public function approve_vqpro($qid, $pid)
    {
        DB::table('vquote_products')->where('vqpro_qid', $qid)->where('vqpro_pid', $pid)->update(['vqpro_is_approved' => 'Y']);
        $count      = DB::table('vquote_products')->where('vqpro_qid', $qid)->count();
        $approved   = DB::table('vquote_products')->where('vqpro_qid', $qid)->where('vqpro_is_approved', 'Y')->count();

        if ($count == $approved) {
            DB::table('vendor_quotes')->where('vquote_id', $qid)->update(['vqpro_is_approved' => 'Y']);
        }
        $re = array(
            'status'    => TRUE,
        );

        return response()->json($re);
    }

    public function add_to_cart(Request $request, $sess_name)
    {
        $input = $request->input('cart');
        $carts = session($sess_name);


        if (!empty($input['cart_pid']) && !empty($input['cart_qty'])) {
            $p = DB::table('products')->where('product_id', $input['cart_pid'])->first();

            $LastPrice = 0;
            if ($request->BuyerDetail != "") {
                $approved = DB::table('quotes')->leftjoin("quote_products", "quote_products.qpro_qid", "quotes.quote_id")->where('quote_uid', $request->BuyerDetail)->where("qpro_pid", $input['cart_pid'])->where('qpro_is_approved', 'Y')->orderBy("qpro_id", "DESC")->first();

                if ($approved != "") {
                    $LastPrice = $approved->qpro_price;
                }
            }

            $qty    = !empty($carts[$input['cart_pid']]['qty'])     ? $carts[$input['cart_pid']]['qty'] + $input['cart_qty'] : $input['cart_qty'];
            $price  = !empty($carts[$input['cart_pid']]['price'])   ? $carts[$input['cart_pid']]['price'] : $p->product_mrp;
            // $remark  = $input['cart_review'];

            $BuyerID = $request->BuyerDetail;

            $carts[$input['cart_pid']] = compact('qty', 'price', "LastPrice", "BuyerID");
        }

        session([$sess_name => $carts]);

        $total_field        = 'quote_total';
        $total_cbm_field    = 'quote_total_cbm';
        $cart_name          = 'quote_cart';
        switch ($sess_name) {
            case 'quote_cart':
                $total_field        = 'quote_total';
                $total_cbm_field    = 'quote_total_cbm';
                $cart_name          = 'quote_cart';
                break;

            case 'vquote_cart':
                $total_field        = 'vquote_total';
                $total_cbm_field    = 'vquote_total_cbm';
                $cart_name          = 'vquote_cart';
                break;
        }



        return view('frontend/template/cart_table', compact('total_field', 'total_cbm_field', 'cart_name'));
    }

    public function update_cart(Request $request, $sess_name)
    {

        $input = $request->input('cart');

        $carts = session($sess_name);

        foreach ($input as $pid => $data) {
            if (!empty($data['cart_qty'])) {
                $carts[$pid]['qty'] = $data['cart_qty'];
            }
            if (!empty($data['cart_price'])) {
                $carts[$pid]['price'] = $data['cart_price'];
            }
            if (!empty($data['cart_review'])) {
                $carts[$pid]['remark'] = $data['cart_review'];
            }
        }

        if (!empty($input['cart_pid']) && !empty($input['cart_qty'])) {
            $qty    = $input['cart_qty'];
            $price  = $input['price'];
            $carts[$input['cart_pid']] = compact('qty', 'price');
        }

        session([$sess_name => $carts]);

        $total_field        = 'quote_total';
        $total_cbm_field    = 'quote_total_cbm';
        $cart_name          = 'quote_cart';
        switch ($sess_name) {
            case 'quote_cart':
                $total_field        = 'quote_total';
                $total_cbm_field    = 'quote_total_cbm';
                $cart_name          = 'quote_cart';
                break;
            case 'vquote_cart':
                $total_field        = 'vquote_total';
                $total_cbm_field    = 'vquote_total_cbm';
                $cart_name          = 'vquote_cart';
                break;
        }

        return view('frontend/template/cart_table', compact('total_field', 'total_cbm_field', 'cart_name'));
    }

    public function remove_cart(Request $request, $sess_name)
    {
        $pid = $request->input('pid');

        $carts = session($sess_name);

        if (!empty($pid) && isset($carts[$pid])) {
            unset($carts[$pid]);
        }
        session([$sess_name => $carts]);

        $total_field        = 'quote_total';
        $total_cbm_field    = 'quote_total_cbm';
        $cart_name          = 'quote_cart';

        switch ($sess_name) {
            case 'quote_cart':
                $total_field        = 'quote_total';
                $total_cbm_field    = 'quote_total_cbm';
                $cart_name          = 'quote_cart';
                break;
        }

        return view('frontend/template/cart_table', compact('total_field', 'total_cbm_field', 'cart_name'));
    }

    public function user_login(Request $request)
    {

        $post = $request->input('record');
        $is_exists = DB::table('users')
            ->select(DB::raw('COUNT(*) AS total, user_id'))
            ->where('user_login', $post['user_login'])
            ->where('user_is_deleted', 'N')
            ->first();

        if ($is_exists->total == 1) {
            $cred = [
                'user_login' => $post['user_login'],
                'password' => $post['user_password'],
            ];
            if (\Auth::attempt($cred)) {
                // if ( password_verify($post['user_password'], $is_exists->user_password)) {
                //     session(['user_auth' => $is_exists->user_id]);

                $re = array(
                    'status'    => TRUE,
                    'message'   => 'Login success! Redirecting, please wait...'
                );
            } else {
                $re = array(
                    'status'    => FALSE,
                    'message'   => 'Login failed! Passowrd is not matched.'
                );
            }
        } else {
            $re = array(
                'status'    => FALSE,
                'message'   => 'Login failed! Username doesn\'t exists.'
            );
        }

        return response()->json($re);
    }

    public function get_states($id)
    {

        $states = DB::table('states')->where('state_is_deleted', 'N')->where('state_country', $id)->get();
        $re = array(
            'data'  => $states
        );

        return $re;
    }
    public function get_cities(Request $request)
    {
        $states = '';
        if (!empty($request->country) && !empty($request->country)) {
            $states = DB::table('cities')->where('city_is_deleted', 'N')->where('city_country', $request->country)->where('city_state', $request->state)->get();
        }
        $re = array(
            'data'  => $states
        );

        return $re;
    }

    public function get_category($post)
    {

        $categories =  DB::connection('mysql2')->table('categories AS c')
            ->select('c.category_id', 'c.category_name', 'c.category_code')
            ->where('category_parent', $post['id'])
            ->get();

        // DB::table('subjects')->where('subject_course', $post['id'])->where('subject_is_deleted', 'N')->select('subject_id', 'subject_name')->get();

        $re = array(
            "status"        => TRUE,
            "categories"    => $categories
        );

        return $re;
    }

    public function get_topics($post)
    {

        $topics = DB::table('topics')->where('topic_subject', $post['id'])->where('topic_is_deleted', 'N')->select('topic_id', 'topic_name')->get();

        $re = array(
            "status"    => TRUE,
            "topics"    => $topics
        );

        return $re;
    }

    public function get_products($post)
    {
        $products = DB::connection('mysql2')->table('products')->where('product_category', $post['cat'])->where('product_finish', $post['finish'])->where('product_subcategory', $post['scat'])->where('product_range', $post['range'])->get();

        $re = [
            'status'    => TRUE,
            'data'      => $products
        ];

        return $re;
    }

    public function get_para($post)
    {
        $paras = DB::table('paragraphs')->where('para_topic', $post['id'])->where('para_is_deleted', 'N')->select('para_id', 'para_name')->get();

        $re = array(
            "status"    => TRUE,
            "paras"     => $paras
        );

        return $re;
    }

    public function GetPrevQuotePrice($ID)
    {
    }

    public function LoadMaterialType($post)
    {
        $categories =  DB::table('material_type')->where('material_type_mat_id', $post['id'])->get();
        $re = array(
            "status"        => TRUE,
            "categories"    => $categories
        );

        return $re;
    }

    public function LoadFinish($post)
    {
        $categories =  DB::table('finish')->where('material_type', $post['id'])->get();
        $re = array(
            "status"        => TRUE,
            "categories"    => $categories
        );

        return $re;
    }
}
