<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class Login extends BaseController
{
   public function index() {
    	$title 		= "Login | GHP Software";
    	$page = "login";
    	$data = compact('page', 'title', 'records');
    	return view('frontend/layout', $data);
    }
}

