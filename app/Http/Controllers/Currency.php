<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use Session;

class Currency extends BaseController
{
    public function index(Request $request, $id = null) {
       if($request->isMethod('post')) {
    		$input = $request->input('record');
    		if(!empty($input)) {
	    		if(empty($id)) {
	    		    
	    		    $CheckDuplicate = DB::table('currencies')
                                                    ->where("currency_name", $input["currency_name"])
                                                    ->where("currency_short_name", $input["currency_short_name"])
                                                    ->count();
                    
                    if($CheckDuplicate > 0){
                        Session::flash('Danger', 'Currency Record Already Exists');
                        return redirect('currency');
                    }
                    
		    		DB::table('currencies')->insert($input);
		    		$id = DB::getPdo()->lastInsertId();
		    		
		    		Session::flash('Success', 'Record Added Successfully');
		    		
		    	} else {
		    		DB::table('currencies')->where('currency_id', $id)->update($input);
		    		
		    		Session::flash('Success', 'Record Updated Successfully');
		    	}
		    }

		    $check = $request->input('check');
		    if(!empty($check)) {
		    	DB::table('currencies')->whereIn('currency_id', $check)->update(['currency_is_deleted' => 'Y']);
		    	Session::flash('Success', 'Record Deleted Successfully');
		    }
		    
		    return redirect('currency');
    	}

    	$records = DB::table('currencies AS p')
    				->where('currency_is_deleted', 'N');
		if(!empty($request->currency_name)){
			$records->where('currency_name' , "like", "%".$request->currency_name."%");
		}
    	$records = $records->paginate(10);

    	$edit = array();
    	if(!empty($id)) {
    		$edit = DB::table('currencies')->where('currency_id', $id)->first();
    	}

        $title 	= "Currency | GHP Software";
        $page 	= "currency";
        $data 	= compact('page', 'title', 'records', 'edit');
        return view('frontend/layout', $data);
    }

	public function exportCsv(Request $request) {
        $fileName = 'forwarder.csv';       
        // $tasks = SurveyHistory::with('user','survey')->get()->toArray();
		$records = DB::table('currencies AS p')
			->where('currency_is_deleted', 'N');
		if(!empty($request->currency_name)){
			$records->where('currency_name' , "like", "%".$request->currency_name."%");
		}
		$lists = $records->get()->toArray();
        $listArr = [];
        
        foreach($lists as $key => $l){
            $listArr[$key]['Currency Name'] = !empty($l->currency_name) ? $l->currency_name : '';
            $listArr[$key]['Currency short name'] = @$l->currency_short_name;
            $listArr[$key]['Currency sign'] = @$l->currency_sign;
            
        }
        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("currencyList" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }


    public  function array2csv(array &$array)
    {
       if (count($array) == 0) {
         return null;
       }
       ob_start();
       $df = fopen("php://output", 'w');
       fputcsv($df, array_keys(reset($array)));
       foreach ($array as $row) {
          fputcsv($df, $row);
       }
       fclose($df);
       return ob_get_clean();
    }
    public function download_send_headers($filename) {
    // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
}

