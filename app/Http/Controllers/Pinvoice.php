<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

use App\Models\Query;
use App\Models\PoInvoice;
use App\Models\GHPInvoice;
use App\Models\PoInvoiceProduct;
use App\Models\PIModel as PI;
use App\Models\POModel as PO;
use App\Models\UserModel;

class Pinvoice extends BaseController
{
    public function index()
    {
        $dbprefix = env('DB_PREFIX');

        $query = GHPInvoice::join('purchase_invoices AS PI', 'PI.pi_id', 'exs_ghp_invoice_pi_id')
            ->join('quotes AS q', 'q.quote_id', 'PI.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->orderBy('exs_ghp_invoice_id', 'DESC');

        $profile = Query::get_profile();

        if ($profile->user_role == "buyer") {
            $query->where('exs_ghp_invoice_buyer', $profile->user_id);
        }

        if (@$_GET['SearchByInvoice'] != "") {
            $query->where('exs_ghp_invoice_invoice_no', $_GET['SearchByInvoice']);
        }

        if (@$_GET['SearchByPI'] != "") {
            $query->where('pi_id', $_GET['SearchByPI']);
        }

        if (@$_GET['SearchByCustomer'] != "") {
            $query->where('user_name', "like", "%" . $_GET['SearchByCustomer'] . "%");
        }

        $records = $query->paginate(15);

        $title     = "View GHP Invoices";
        $page     = "view_ghp_invoice";
        $data = compact('page', 'title', 'records');
        return view('frontend/layout', $data);
    }

    public function GhpCommercialInvoicePrint(Request $Request, $id)
    {
        $GHPInvoiceData = GHPInvoice::find($id);
        $GetInvoiceData = PoInvoice::find($GHPInvoiceData->exs_ghp_invoice_po_invoice_id);
        $InvoiceData = PoInvoiceProduct::leftjoin('products AS p', 'p.product_id', 'exs_po_invoice_products_product_id')->where("exs_po_invoice_products_inv_id", $GHPInvoiceData->exs_ghp_invoice_po_invoice_id)->get();
        $GetVendorData = UserModel::leftjoin("vendors", "vendors.vendor_uid", "user_id")->find($GetInvoiceData->po_invoice_vendor_id);

        $PODATA     = PO::join('vendor_quotes AS q', 'q.vquote_id', 'purchase_orders.po_qid')
            ->join('users AS u', 'q.vquote_uid', 'u.user_id')
            ->join('vendors AS ven', 'q.vquote_uid', 'ven.vendor_uid')
            ->join('currencies AS c', 'q.vquote_currency', 'c.currency_id')
            ->leftJoin('countries AS con', 'u.user_country', 'con.country_id')
            ->leftJoin('states AS st', 'u.user_state', 'st.state_id')
            ->find($GetInvoiceData->po_invoice_po);



        $PIDATA     = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->leftJoin('countries AS con', 'u.user_country', 'con.country_id')
            ->leftJoin('states AS st', 'u.user_state', 'st.state_id')
            ->find($GHPInvoiceData->exs_ghp_invoice_pi_id);

        return view("frontend.GHPInvoicePrint", compact("GetInvoiceData", "InvoiceData", "GetVendorData", "PODATA", "PIDATA", "GHPInvoiceData"));
    }

    public function GhpPackingListPrint(Request $Request, $id)
    {
        $GHPInvoiceData = GHPInvoice::find($id);

        $GetInvoiceData = PoInvoice::find($GHPInvoiceData->exs_ghp_invoice_po_invoice_id);
        $InvoiceData = PoInvoiceProduct::leftjoin('products AS p', 'p.product_id', 'exs_po_invoice_products_product_id')->where("exs_po_invoice_products_inv_id", $GHPInvoiceData->exs_ghp_invoice_po_invoice_id)->get();
        $GetVendorData = UserModel::leftjoin("vendors", "vendors.vendor_uid", "user_id")->find($GetInvoiceData->po_invoice_vendor_id);

        $PODATA     = PO::join('vendor_quotes AS q', 'q.vquote_id', 'purchase_orders.po_qid')
            ->join('users AS u', 'q.vquote_uid', 'u.user_id')
            ->join('vendors AS ven', 'q.vquote_uid', 'ven.vendor_uid')
            ->join('currencies AS c', 'q.vquote_currency', 'c.currency_id')
            ->leftJoin('countries AS con', 'u.user_country', 'con.country_id')
            ->leftJoin('states AS st', 'u.user_state', 'st.state_id')
            ->find($GetInvoiceData->po_invoice_po);



        $PIDATA     = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->leftJoin('countries AS con', 'u.user_country', 'con.country_id')
            ->leftJoin('states AS st', 'u.user_state', 'st.state_id')
            ->find($GHPInvoiceData->exs_ghp_invoice_pi_id);

        return view("frontend.PackingListPrint", compact("GetInvoiceData", "InvoiceData", "GetVendorData", "PODATA", "PIDATA", "GHPInvoiceData"));
    }

    public function CreateGHPInvoice(Request $req, $id)
    {

        if ($req->isMethod('post')) {
            $GetInvoiceData = PoInvoice::find($id);

            $GetPoIDArray = explode(",", $GetInvoiceData->po_invoice_po);
            $GetPoData = PO::find($GetPoIDArray[0]);

            $query = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
                ->join('users AS u', 'q.quote_uid', 'u.user_id')
                ->find($GetPoData->po_pi_id);

            $input = $req->input('record');
            $input['exs_ghp_invoice_po_id'] = $GetInvoiceData->po_invoice_po;
            $input['exs_ghp_invoice_pi_id'] = $GetPoData->po_pi_id;
            $input['exs_ghp_invoice_po_invoice_id'] = $GetInvoiceData->po_invoice_id;
            $input['exs_ghp_invoice_buyer'] = $query->quote_uid;

            $id    = GHPInvoice::insertGetId($input);
            $mess  = "A new PO has been created.";
            return redirect(url('ghp-invoice-list'))->with('success', $mess);
        }

        $ports      = DB::table('ports')->where('port_is_deleted', 'N')->get();
        $title     = "Create GHP Invoice";
        $page   = "add_ghp_invoice";
        $data   = compact('page', 'title', 'id', "ports");
        return view('frontend/layout', $data);
    }
}
