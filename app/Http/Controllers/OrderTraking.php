<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Models\Query;
use App\Models\QuoteModel as Quote;
use App\Models\QuoteProduct;
use App\Models\PIModel as PI;
use App\Models\PIProduct;
use App\Models\Bproduct;

use DB;

class OrderTraking extends BaseController
{

    public function View(Request $request, $id = null)
    {
        $dbprefix = env('DB_PREFIX');

        $query = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->orderBy('pi_id', 'DESC')
            ->where('pi_is_deleted', 'N');

        $profile = Query::get_profile();

        if ($profile->user_role == "buyer") {
            $query->where('q.quote_uid', $profile->user_id);
        }


        if (isset($request->TrackStatus) && $request->TrackStatus != "") {
            $NewData = $query->get();

            $FilterIDs = array();

            foreach ($NewData as $ND) {
                $PINumber = sprintf("%s-%03d", 'GHP-201819', $ND->pi_id);
                $Logistic = DB::table('logistics')->where("exs_logistics_pi", "like", "%" . $PINumber . "%")->count();
                if ($Logistic > 0) {
                    $Logistic = DB::table('logistics')->where("exs_logistics_pi", "like", "%" . $PINumber . "%")->first();

                    $Status = "";

                    if (isset($Logistic->StuffingDate)) {
                        if (date("Y-m-d") < $Logistic->StuffingDate) {
                            $Status = "Open";
                        } elseif ($Logistic->StuffingDate < date("Y-m-d") && $Logistic->PlannedETDDAte > date("Y-m-d")) {
                            $Status = "Shipped on Water";
                        } elseif ($Logistic->PlannedETADate < date("Y-m-d")) {
                            $Status = "Shipped on Board";
                        }
                    }

                    if ($Status == $request->TrackStatus) {
                        $FilterIDs[] = $ND->pi_id;
                    }
                }
            }

            $query = $query->whereIn("pi_id", $FilterIDs);
        }

        $records = $query->paginate(15);

        $title     = "Order Traking";
        $page     = "order_tracking";
        $data = compact('page', 'title', 'records');
        return view('frontend/layout', $data);
    }

    public function ViewDetail(Request $Request)
    {
        $dbprefix = env('DB_PREFIX');

        $records = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->find($Request->ID);

        $profile = Query::get_profile();

        $title     = "Order Traking";
        $page     = "order_tracking_detail";
        $data = compact('page', 'title', 'records');
        return view('frontend/layout', $data);
    }
    public function exportCsv(Request $request)
    {
        $fileName = 'order-track.csv';
        // $tasks = SurveyHistory::with('user','survey')->get()->toArray();
        $query = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->orderBy('pi_id', 'DESC')
            ->where('pi_is_deleted', 'N');
        $profile = Query::get_profile();

        if ($profile->user_role == "buyer") {
            $query->where('q.quote_uid', $profile->user_id);
        }


        if (isset($request->TrackStatus) && $request->TrackStatus != "") {
            $NewData = $query->get();
            $FilterIDs = array();

            foreach ($NewData as $ND) {
                $PINumber = sprintf("%s-%03d", 'GHP-201819', $ND->pi_id);
                $Logistic = DB::table('logistics')->where("exs_logistics_pi", "like", "%" . $PINumber . "%")->count();
                if ($Logistic > 0) {
                    $Logistic = DB::table('logistics')->where("exs_logistics_pi", "like", "%" . $PINumber . "%")->first();

                    $Status = "";

                    if (isset($Logistic->StuffingDate)) {
                        if (date("Y-m-d") < $Logistic->StuffingDate) {
                            $Status = "Open";
                        } elseif ($Logistic->StuffingDate < date("Y-m-d") && $Logistic->PlannedETDDAte > date("Y-m-d")) {
                            $Status = "Shipped on Water";
                        } elseif ($Logistic->PlannedETADate < date("Y-m-d")) {
                            $Status = "Shipped on Board";
                        }
                    }

                    if ($Status == $request->TrackStatus) {
                        $FilterIDs[] = $ND->pi_id;
                    }
                }
            }

            $query = $query->whereIn("pi_id", $FilterIDs);
        }

        $lists = $query->get()->toArray();
        $listArr = [];
        $site = DB::table('settings')->first();
        foreach ($lists as $key => $l) {
            // dd($l);
            $GrandTotal = 0;
            $GrandCBM = 0;
            $products = PIProduct::join('products AS p', 'p.product_id', 'pipro_pid')->join('quote_products AS qpro', 'pipro_pid', 'qpro_pid')->where('pipro_pi_id', $l['pi_id'])->where('qpro_qid', $l['quote_id'])->get();

            foreach ($products as $p) {
                $SubTotal = $p->qpro_price * $p->qpro_qty;
                $SubCBM = $p->product_cbm * $p->qpro_qty;
                $GrandTotal += $SubTotal;
                $GrandCBM += $SubCBM;
            }
            $PINumber = sprintf("%s-%03d", 'GHP-201819', $l['pi_id']);
            $Logistic = DB::table('logistics')->where("exs_logistics_pi", "like", "%" . $PINumber . "%")->count();
            if ($Logistic > 0) {
                $Logistic = DB::table('logistics')->where("exs_logistics_pi", "like", "%" . $PINumber . "%")->first();
            } else {
                $Logistic = "";
            }

            $status = $pi_confirm_date = "";
            if (isset($Logistic->StuffingDate)) {

                if (date("Y-m-d") < $Logistic->StuffingDate) {
                    $status = "Open";
                } elseif ($Logistic->StuffingDate < date("Y-m-d") && $Logistic->PlannedETDDAte > date("Y-m-d")) {
                    $status = "Shipped on Water";
                } elseif ($Logistic->PlannedETADate < date("Y-m-d")) {
                    $status = "Shipped on Board";
                }
            }

            if (!empty($l['pi_confirm_date']) && $l['pi_confirm_date'] != "0000-00-00") {
                $pi_confirm_date = date("d-M-Y", strtotime($l['pi_confirm_date']));
            }
            // $listArr[] = $l;
            $listArr[$key]['status'] = $status;
            $listArr[$key]['PI Number'] = !empty($l['pi_id']) ? sprintf("%s-%03d", 'GHP-201819', $l['pi_id']) : '';
            $listArr[$key]['PI Date'] =  !empty($l['pi_date']) ? date("d-M-Y", strtotime($l['pi_date'])) : '';
            $listArr[$key]['PI Confirm Date'] =   $pi_confirm_date;
            $listArr[$key]['Agreed ETD'] = @$Logistic->AgreedETD != "" ? date("d-M-Y", strtotime(@$Logistic->AgreedETD)) : '';
            // $listArr[$key]['dp_date'] = $l['dp_date'] ? date("d-M-Y", strtotime($l['dp_date'])) : '';
            $listArr[$key]['CBM'] = !empty($GrandCBM) ? round($GrandCBM, 3) : 0;
            $listArr[$key]['Container No.'] = !empty($Logistic) ? $Logistic->ContainerNr : '';
            $listArr[$key]['Stuffing Date'] = !empty($Logistic) ? date("d-M-Y", strtotime($Logistic->PlannedStuffingDate)) : '';
            if ($profile->user_role == 'admin') {
                $listArr[$key]['DO Exp. Date'] = !empty($Logistic) ? date("d-M-Y", strtotime($Logistic->DOExpiryDate)) : '-';
                $listArr[$key]['POD'] =  !empty($l['quote_loading_port']) ? $l['quote_loading_port'] : '';
                $listArr[$key]['Planned ETD'] = !empty($Logistic) ? date("d-M-Y", strtotime($Logistic->PlannedETDDAte)) : '-';
                $listArr[$key]['Planned ETA'] = !empty($Logistic) ? date("d-M-Y", strtotime($Logistic->PlannedETADate)) : '-';
                $listArr[$key]['Vessel Sailing Date'] = !empty($Logistic) ? date("d-M-Y", strtotime($Logistic->VesselSailingETD)) : '-';
                $listArr[$key]['Vessel ETA'] = !empty($Logistic) ? date("d-M-Y", strtotime($Logistic->VesselSailingETA)) : '-';
                $listArr[$key]['PI Value'] = !empty($GrandTotal) ? number_format($GrandTotal, 2) : '';
            }
        }
        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("order_track_reports" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }
    public  function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }
    public function download_send_headers($filename)
    {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
}
