<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Query;
use DB;
use Hash;

class Review extends BaseController
{
	public function index()
	{

		$page 	= "reviews";
		$data 	= compact('page');
		return view('backend/layout', $data);
	}
}
