<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Query;
use DB;
use Hash;

class Users extends BaseController
{
	public function index()
	{

		$page 	= "users";
		$data 	= compact('page');
		return view('backend/layout', $data);
	}
}
