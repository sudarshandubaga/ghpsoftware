<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Models\Query;

class category extends BaseController
{
    public function index(Request $request, $id = NULL)
    {
        $q = new Query();
        if ($request->isMethod('post')) {
            $check = $request->input('check');

            if (!empty($check)) {
                $arr = array(
                    "category_is_deleted" => "Y"
                );
                DB::table('categories')->whereIn('category_id', $check)->update($arr);

                return redirect()->back()->with('success', 'Selected record(s) deleted.');
            }
        }

        // $records  = DB::table('categories')->where('category_is_deleted', 'N')->paginate(10);
        $records = DB::table('categories')
            ->leftJoin('categories AS c2', 'categories.category_parent', '=', 'c2.category_id')
            ->select('categories.*', 'c2.category_name AS parent')
            // ->where('category_is_deleted','N')
            ->paginate(10);
        $category = DB::table('categories')->where('category_parent', '0')->get();
        if ($request->isMethod('post')) {
            $input = $request->input('record');

            if (empty($id)) {
                DB::table('categories')->insert($input);
                $id = DB::getPdo()->lastInsertId();
                $mess = "Data inserted.";
            } else {
                DB::table('categories')->where('category_id', $id)->update($input);
                $mess = "Data updated";
            }

            $slug = $q->create_slug($input['category_name'], "categories", "category_slug", "category_id", $id);
            DB::table('categories')->where('category_id', $id)->update(array('category_slug' => $slug));
            return redirect()->back()->with('success', $mess);
        }

        $input = $request->input();
        if (!empty($input['id']) && is_numeric($input['id']) || !empty($input['status'])) {
            $status = $input['status'] == "Y" ? "N" : "Y";
            $arr = array(
                "category_is_visible" => $status
            );
            DB::table('categories')->where('category_id', $input['id'])->update($arr);
            return redirect('hb-panel/category');
        }



        $page     = "category";
        $data     = compact('page', 'records', 'category');
        return view('backend/layout', $data);
    }
}
