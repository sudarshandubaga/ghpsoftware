<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Query;
use DB;
use Hash;

class Product extends BaseController
{
    public function index(Request $request, $id = NULL)
    {

        $product_no = $request->input('products');
        $offset  = !empty($product_no) ? $product_no - 1 : 0;
        $records = DB::table('products')
            // ->join('courses', 'topics.topic_course', '=', 'courses.course_id')
            // ->join('subjects', 'topics.topic_subject', '=', 'subjects.subject_id')
            // ->select('topics.*','courses.course_name', 'subjects.subject_name')
            ->where('product_is_deleted', 'N')
            ->paginate(10);

        $input = $request->input();
        if (!empty($input['id']) && is_numeric($input['id']) || !empty($input['status'])) {
            $status = $input['status'] == "Y" ? "N" : "Y";
            $arr = array(
                "product_is_visible" => $status
            );
            DB::table('products')->where('product_id', $input['id'])->update($arr);
            return redirect('hb-panel/product');
        }

        if ($request->isMethod('post')) {
            $check = $request->input('check');

            foreach ($check as $id) {
                DB::table('products')->where('product_id', $id)->update(array('product_is_deleted' => 'Y'));
            }
            $mess = "Selected record(s) deleted successfully.";
            return redirect()->back()->with('success', $mess);
        }

        $page     = "product";
        $data     = compact('page', 'records', 'offset');
        return view('backend/layout', $data);
    }

    public function add(Request $request, $id = NULL)
    {
        $q     = new Query();
        $input = $request->input('record');

        $edit = $specs = array();
        if (!empty($id)) {
            $edit = DB::table('products')->where('product_id', $id)->first();
            $specs = unserialize($edit->product_specification);

            // print_r($seri);
        }

        $category = DB::table('categories')->where('category_parent', '0')->get();

        if ($request->isMethod('post')) {
            $input['product_specification'] = serialize($input['product_specification']);
            if (empty($id)) {
                DB::table('products')->insert($input);
                $id = DB::getPdo()->lastInsertId();
                $mess = "Data inserted.";
            } else {
                DB::table('products')->where('product_id', $id)->update($input);
                $mess = "Data updated";
            }

            if ($request->hasFile('product_image')) {
                if (!empty($edit->book_image) && file_exists(public_path() . '/imgs/product/' . $edit->book_image)) {
                    unlink(public_path() . '/imgs/product/' . $edit->book_image);
                }
                $image           = $request->file('product_image');
                $name            = 'img' . $id . '.' . $image->getClientOriginalExtension();
                $destinationPath = 'imgs/product';
                $image->move($destinationPath, $name);

                if (!empty($edit->book_image)) {
                    $name .= "?v=" . uniqid();
                }

                DB::table('products')->where('product_id', $id)->update(array('product_image' => $name));
            }

            // $mess = "New record inserted";
            return redirect('hb-panel/product');
        }

        $page   = "add_product";
        $data   = compact('page', 'edit', 'specs', 'category');
        return view('backend/layout', $data);
    }
}
