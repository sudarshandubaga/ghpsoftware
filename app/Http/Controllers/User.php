<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Models\UserModel as Umodel;
use App\Models\Vendor;
use App\Models\BuyerModel as Buyer;
use App\Models\Country;
use App\Models\State;
use App\Models\Query;
use DB;

class User extends BaseController
{
    public function index(Request $request, $role)
    {
        if ($request->isMethod('post')) {
            $ids = $request->input('check');

            if (!empty($ids)) {
                Umodel::whereIn('user_id', $ids)->update(['user_is_deleted' => 'Y']);
                return redirect(url('user/' . $role))->with('success', 'Record(s) has been deleted.');
            }
        }

        $title     = ucwords($role) . " | GHP Software";
        $page     = "view_user";

        $role_name = ucwords($role);

        $records = Umodel::where('user_role', $role)->where('user_is_deleted', 'N');

        if (@$_GET['SearchByName'] != "") {
            $records = $records->where("user_name", "like", "%" . $_GET['SearchByName'] . "%");
        }

        if (@$_GET['SearchByName1'] != "") {
            $records = $records->where("user_main_name", "like", "%" . $_GET['SearchByName1'] . "%");
        }


        $records = $records->paginate(10);

        $data     = compact('page', 'title', 'records', 'role_name', 'role');
        return view('frontend/layout', $data);
    }
    public function vendor(Request $request)
    {
        $role = "vendor";
        if ($request->isMethod('post')) {
            $ids = $request->input('check');

            if (!empty($ids)) {
                Umodel::whereIn('user_id', $ids)->update(['user_is_deleted' => 'Y']);
                return redirect(url('user/' . $role))->with('success', 'Record(s) has been deleted.');
            }
        }

        $title     = ucwords($role) . " | GHP Software";
        $page     = "view_vendor";

        $role_name = ucwords($role);

        $records = Umodel::with(['vendor'])->where('user_role', $role)->where('user_is_deleted', 'N');

        if (@$_GET['SearchByName'] != "") {
            $records = $records->where("user_name", "like", "%" . $_GET['SearchByName'] . "%");
        }
        if (@$_GET['SearchByCode'] != "") {
            $code = @$_GET['SearchByCode'];
            $records = $records->whereHas("vendor", function ($query) use ($code) {
                $query->where("vendor_code", "like", "%" . $code . "%");
            });
        }
        $records = $records->paginate(10);

        $data     = compact('page', 'title', 'records', 'role_name', 'role');
        return view('frontend/layout', $data);
    }
    public function add(Request $request, $role, $id = null)
    {
        $edit = [];
        $vendor_edit = "";
        if (!empty($id)) {
            $edit        = Umodel::find($id);
            $vendor_edit = Vendor::where('vendor_uid', $id)->first();
        }

        $title          = "Add " . ucwords($role) . " | GHP Software";
        $page          = "add_user";
        $role_name   = ucwords($role);

        if ($request->isMethod('post')) {
            $user       = $request->input('user');
            $vendor     = $request->input('vendor');

            $isExistsQuery = DB::connection('mysql')->table('users')->where('user_login', 'LIKE', $user['user_login']);

            if (!empty($id)) {
                $isExistsQuery->where('user_id', '!=', $id);
            }

            $isExists = $isExistsQuery->count();

            if (!$isExists) :
                $user['user_role']  = 'vendor';
                $user['user_name']  = !empty($vendor['vendor_org_name']) ? $vendor['vendor_org_name'] : "";

                if (!empty($user['password'])) {
                    $password               = password_hash($user['password'], PASSWORD_BCRYPT, ['cost' => 10]);
                    $user['password']  = $password;
                } else {
                    unset($user['password']);
                }

                $vendor['vendor_ib_currency'] = !empty($vendor['vendor_ib_currency']) ? implode(",", $vendor['vendor_ib_currency']) : "";

                $fileCheckArr = [
                    'vendor_certificate_fsc',
                    'vendor_certificate_iso',
                    'vendor_certificate_vriksh',
                    'vendor_certificate_bsi',
                    'vendor_certificate_reach',
                    'vendor_certificate_ce',
                    'vendor_certificate_cites',
                    'vendor_certificate_pefc',
                    'vendor_certificate_lacey_act',
                    'vendor_certificate_phytosanitary',
                    'vendor_epch',
                    'vendor_ceph',
                    'vendor_cle',
                    'vendor_fieo',
                    'vendor_gjepc',
                    'vendor_code',
                ];

                foreach ($fileCheckArr as $check) {
                    if (empty($vendor[$check])) $vendor[$check] = 'N';
                }

                foreach ($vendor as $k => $v) {
                    if (empty($vendor[$k])) $vendor[$k] = "";
                }

                if (empty($id)) {
                    $user['user_created_on']    = date('Y-m-d H:i:s', time());

                    // Software DB
                    DB::connection('mysql')->table('users')->insert($user);
                    $id = DB::getPdo()->lastInsertId();

                    $vendor['vendor_uid'] = $id;
                    DB::connection('mysql')->table('vendors')->insert($vendor);
                    // Software DB End

                } else {
                    DB::connection('mysql')->table('users')->where('user_id', $id)->update($user);
                    DB::connection('mysql')->table('vendors')->where('vendor_uid', $id)->update($vendor);
                }

                $fileArr = [
                    'vendor_certificate_fsc_file',
                    'vendor_certificate_iso_file',
                    'vendor_certificate_vriksh_file',
                    'vendor_certificate_bsi_file',
                    'vendor_certificate_reach_file',
                    'vendor_certificate_ce_file',
                    'vendor_certificate_cites_file',
                    'vendor_certificate_pefc_file',
                    'vendor_certificate_lacey_act_file',
                    'vendor_certificate_phytosanitary_file',
                    'vendor_epch_file',
                    'vendor_ceph_file',
                    'vendor_cle_file',
                    'vendor_fieo_file',
                    'vendor_gjepc_file',
                    'vendor_other_auth_file1',
                    'vendor_other_auth_file2',
                    'vendor_compliance_bsci',
                    'vendor_compliance_sedex',
                    'vendor_compliance_iso',
                    'vendor_compliance_other',
                ];

                // print_r($fileArr); die;

                foreach ($fileArr as $file) {
                    // echo $file;
                    // var_dump($request->hasFile("'".$file."'"));
                    // die;
                    if ($request->hasFile($file)) {
                        if (!empty($edit->{$file}) && file_exists(public_path() . '/files/vendors/' . $edit->{$file})) {
                            unlink(public_path() . '/files/vendors/' . $edit->{$file});
                        }
                        $image           = $request->file($file);
                        $name            = sprintf("%04d", $id) . $file . '.' . $image->getClientOriginalExtension();
                        $destinationPath = 'files/vendors';
                        $image->move($destinationPath, $name);

                        if (!empty($edit->{$file})) {
                            $name .= "?v=" . uniqid();
                        }

                        DB::table('vendors')->where('vendor_uid', $id)->update(array($file => $name));
                    }
                }

                return redirect('user/vendor')->with('success', "Success! New vendor account has been created.");

            else :
                return redirect()->back()->with('danger', "Failed! Username already exists, please try another.");
            endif;
        }

        $data     = compact('page', 'title', 'role_name', 'role', 'edit', 'vendor_edit');
        return view('frontend/layout', $data);
    }
    public function add_buyer(Request $request, $id = null)
    {
        $countries  = Country::where('country_is_deleted', 'N')->get();


        $edit       = $buyer_edit = $states = $np_states = [];
        if (!empty($id)) {
            $edit        = Umodel::find($id);
            $buyer_edit  = Buyer::where('buyer_uid', $id)->first();

            if (!empty($edit->user_country)) {
                $states      = State::where('state_is_deleted', 'N')->where('state_country', $edit->user_country)->get();
            }

            if (!empty($buyer_edit->buyer_np_country)) {
                $np_states   = State::where('state_is_deleted', 'N')->where('state_country', $buyer_edit->buyer_np_country)->get();
            }
        }

        $title  = "Add Buyer | GHP Software";
        $page   = "add_buyer";

        if ($request->isMethod('post')) {
            $user   = $request->input('user');
            $buyer  = $request->input('buyer');

            foreach ($buyer as $k => $v) {
                if (empty($buyer[$k])) $buyer[$k] = "";
            }

            $isExistsQuery = DB::connection('mysql')->table('users')->where('user_login', 'LIKE', $user['user_login']);

            if (!empty($id)) {
                $isExistsQuery->where('user_id', '!=', $id);
            }

            $isExists = $isExistsQuery->count();

            if (!$isExists) :

                if (!empty($user['password'])) {
                    $password               = password_hash($user['password'], PASSWORD_BCRYPT, ['cost' => 10]);
                    $user['password']  = $password;
                } else {
                    unset($user['password']);
                }

                $insert = FALSE;

                if (empty($id)) {
                    $insert = TRUE;
                    $user['user_created_on']    = date('Y-m-d H:i:s', time());

                    // Software DB
                    DB::connection('mysql')->table('users')->insert($user);
                    $id = DB::getPdo()->lastInsertId();

                    $buyer['buyer_uid'] = $id;
                    DB::connection('mysql')->table('buyers')->insert($buyer);
                    // Software DB End
                } else {
                    DB::connection('mysql')->table('users')->where('user_id', $id)->update($user);
                    DB::connection('mysql')->table('buyers')->where('buyer_uid', $id)->update($buyer);
                }


                // Website DB
                $webArr = [
                    'user_id'           => $id,
                    'user_main_name'    => $user['user_main_name'],
                    'user_fname'        => $user['user_name'],
                    'user_name'         => $user['user_name'],
                    'user_login'        => $user['user_email'],
                    'user_email'        => $user['user_email'],
                    'user_mobile'       => $user['user_mobile'],
                    'user_is_enabled'   => 'Y'
                ];

                if (!empty($user['user_created_on'])) {
                    $webArr['user_created_on'] = $user['user_created_on'];
                }

                if (!empty($user['password'])) {
                    $webArr['user_password']    = $user['password'];
                }

                if ($insert) {
                    DB::connection('mysql2')->table('users')->insert($webArr);
                } else {
                    DB::connection('mysql2')->table('users')->where('user_login', 'LIKE', $user['user_login'])->update($webArr);
                }
                // End Website DB

                return redirect('user/buyer')->with('success', "Success! New buyer account has been created.");

            else :
                return redirect()->back()->with('danger', "Failed! Username already exists, please try another.");
            endif;
        }

        // $records = Umodel::paginate(10);


        // $states      = DB::connection('mysql')->table('countries')->where('country_is_deleted', 'N')->get();

        $data   = compact('page', 'title', 'countries', 'states', 'np_states', 'edit', 'buyer_edit');
        return view('frontend/layout', $data);
    }

    public function BuyerProfile()
    {

        $countries  = Country::where('country_is_deleted', 'N')->get();
        $profile = Query::get_profile();
        $edit       = $buyer_edit = $states = $np_states = [];
        $edit        = Umodel::find($profile->user_id);
        $buyer_edit  = Buyer::where('buyer_uid', $profile->user_id)->first();

        if (!empty($edit->user_country)) {
            $states      = State::where('state_is_deleted', 'N')->where('state_country', $edit->user_country)->get();
        }

        if (!empty($buyer_edit->buyer_np_country)) {
            $np_states   = State::where('state_is_deleted', 'N')->where('state_country', $buyer_edit->buyer_np_country)->get();
        }
        $title  = "Add Buyer | GHP Software";
        $page   = "add_buyer";
        $data   = compact('page', 'title', 'countries', 'states', 'np_states', 'edit', 'buyer_edit');
        return view('frontend/layout', $data);
    }
    public function add_employee(Request $request, $id = null)
    {
        $title  = "Add Employee | GHP Software";
        $page   = "add_employee";

        if ($request->isMethod('post')) {
            $user       = $request->input('user');
            $employee   = $request->input('employee');

            $isExistsQuery = DB::connection('mysql')->table('users')->where('user_login', 'LIKE', $user['user_login']);

            if (!empty($id)) {
                $isExistsQuery->where('user_id', '!=', $id);
            }

            $isExists = $isExistsQuery->count();

            if (!$isExists) :
                $user['user_role']  = 'employee';
                $user['user_name']  = trim($user['user_fname'] . " " . $user['user_lname']);
                if (!empty($user['password'])) {
                    $password               = password_hash($user['password'], PASSWORD_BCRYPT, ['cost' => 10]);
                    $user['password']  = $password;
                } else {
                    unset($user['password']);
                }

                foreach ($employee as $k => $v) {
                    if (empty($employee[$k])) $employee[$k] = "";
                }

                if (empty($id)) {
                    $user['user_created_on']    = date('Y-m-d H:i:s', time());

                    // Software DB
                    DB::connection('mysql')->table('users')->insert($user);
                    $id = DB::getPdo()->lastInsertId();

                    $employee['employee_uid'] = $id;
                    DB::connection('mysql')->table('employees')->insert($employee);
                    // Software DB End
                } else {
                    DB::connection('mysql')->table('users')->where('user_id', $id)->update($user);
                    DB::connection('mysql')->table('employees')->where('employee_uid', $id)->update($employee);
                }

                if ($request->hasFile('user_image')) {
                    if (!empty($edit->user_image) && file_exists(public_path() . '/img/users/' . $edit->user_image)) {
                        unlink(public_path() . '/img/users/' . $edit->user_image);
                    }
                    $image           = $request->file('user_image');
                    $name            = $id . '.' . $image->getClientOriginalExtension();
                    $destinationPath = 'img/users';
                    $image->move($destinationPath, $name);

                    if (!empty($edit->user_image)) {
                        $name .= "?v=" . uniqid();
                    }

                    Umodel::where('user_id', $id)->update(array('user_image' => $name));
                }

                if ($request->hasFile('employee_id_file')) {
                    if (!empty($edit->employee_id_file) && file_exists(public_path() . '/files/employee/' . $edit->employee_id_file)) {
                        unlink(public_path() . '/files/employee/' . $edit->employee_id_file);
                    }
                    $image           = $request->file('employee_id_file');
                    $name            = $id . '.' . $image->getClientOriginalExtension();
                    $destinationPath = 'files/employee';
                    $image->move($destinationPath, $name);

                    if (!empty($edit->employee_id_file)) {
                        $name .= "?v=" . uniqid();
                    }

                    DB::table('employees')->where('employee_uid', $id)->update(array('employee_id_file' => $name));
                }

                return redirect('user/employee')->with('success', "Success! New employee account has been created.");

            else :
                return redirect()->back()->with('danger', "Failed! Username already exists, please try another.");
            endif;
        }

        $data   = compact('page', 'title');
        return view('frontend/layout', $data);
    }
    public function change_status(Request $request, $role, $id)
    {
        $user = Umodel::find($id);
        $user->user_is_enabled = $user->user_is_enabled == 'Y' ? 'N' : 'Y';
        $user->save();
        // Umodel::where('user_id',$id)->update(['is_active'=>$user->is_active == 1 ? 0 : 1]);        
        return redirect('user/' . $role)->with('success', "");
    }
}
