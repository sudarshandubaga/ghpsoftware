<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Models\UpdateLog;


class LogController extends BaseController
{
    public function index()
    {
        $records = UpdateLog::with('user')->latest()->paginate(10);
        foreach ($records as $r) {
            $r->created_at = date('d-m-Y g:i A', strtotime($r->created_at));
        }
        $title     = "Logs | GHP Software";
        $page     = "logs";
        $data     = compact('page', 'title', 'records');
        return view('frontend/layout', $data);
    }
}
