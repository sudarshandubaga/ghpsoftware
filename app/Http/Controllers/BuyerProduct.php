<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Query;
use App\Models\Product;
use App\Models\Bproduct;

class BuyerProduct extends BaseController
{
    public function index(Request $request, $uid = null)
    {
        if ($request->isMethod('post')) {
            $check = $request->input('check');

            if (!empty($check)) {
                Bproduct::whereIn('bpro_id', $check)->delete();
            }
        }

        $records = Bproduct::with(['product'])->where('bpro_uid', $uid);

        if (@$_GET['SearchByCode'] != "") {
            $records = $records->whereHas('product', function ($q) {
                $q->where("product_code", "like", "%" . $_GET['SearchByCode'] . "%");
            });
            // $records = $records->where("product_code", "like", "%" . $_GET['SearchByCode'] . "%");
        }

        if (@$_GET['SearchByName'] != "") {
            $records = $records->whereHas('product', function ($q) {
                $q->where("product_name", "like", "%" . $_GET['SearchByName'] . "%");
            });
            // $records = $records->where("product_name", "like", "%" . $_GET['SearchByName'] . "%");
        }
        if (@$_GET['SearchByText'] != "") {
            $records = $records->whereHas('product', function ($q) {
                $q->where("product_text", "like", "%" . $_GET['SearchByText'] . "%");
            });
            // $records = $records->where("product_text", "like", "%" . $_GET['SearchByText'] . "%");
        }
        $records = $records->paginate(30);
        $title     = "View Buyer Products";
        $page     = "view_buyer_product";
        $data = compact('page', 'title', 'records', 'uid');
        return view('frontend/layout', $data);
    }
    public function exportCsv(Request $request, $uid = null)
    {

        $fileName = 'reports.csv';
        // $tasks = SurveyHistory::with('user','survey')->get()->toArray();
        $records = Bproduct::with(['product'])->where('bpro_uid', $uid);

        if (@$request->SearchByCode != "") {
            $records = $records->whereHas('product', function ($q) use ($request) {
                $q->where("product_code", "like", "%" . $request->SearchByCode . "%");
            });
            // $records = $records->where("product_code", "like", "%" . $_GET['SearchByCode'] . "%");
        }

        if (@$request->SearchByName != "") {
            $records = $records->whereHas('product', function ($q) use ($request) {
                $q->where("product_name", "like", "%" . $request->SearchByName . "%");
            });
            // $records = $records->where("product_name", "like", "%" . $_GET['SearchByName'] . "%");
        }
        if (@$request->SearchByText != "") {
            $records = $records->whereHas('product', function ($q) use ($request) {
                $q->where("product_text", "like", "%" . $request->SearchByText . "%");
            });
            // $records = $records->where("product_text", "like", "%" . $_GET['SearchByText'] . "%");
        }
        $lists = $records->get()->toArray();
        $listArr = [];
        $site = \DB::table('settings')->first();
        foreach ($lists as $key => $l) {
            $listArr[$key]['Product Name'] = @$l['product']['product_name'];
            $listArr[$key]['Product Code'] = @$l['product']['product_code'];
            $listArr[$key]['Barcode'] = @$l['bpro_barcode'];
            $listArr[$key]['SKU'] = @$l['bpro_sku'];
            // $listArr[$key]['Buyer Refrence'] = @$l['product']['product_text'];
            $listArr[$key]['Buyer Description'] = @$l['bpro_description'];
            $listArr[$key]['FNG Com. in (%)'] = @$l['salesman1_comm'];
            $listArr[$key]['PHI Com. in (%)'] =  @$l['salesman2_comm'];
        }
        // dd($listArr);
        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("buyer_product_reports" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }
    public  function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }
    public function download_send_headers($filename)
    {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
}
