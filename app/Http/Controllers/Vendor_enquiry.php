<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\VenqryModel as Vemodel;
use App\Models\Query;
use App\Models\UserModel;
use DB;

class Vendor_enquiry extends BaseController
{
    public function index(Request $request)
    {
        $title     = "View Vendor Enquiry | GHP Software";
        $page     = "view_vendor_enquiry";

        if ($request->isMethod('post')) {
            $check = $request->input('check');
            Vemodel::whereIn('venq_id', $check)->update(['venq_is_deleted' => 'Y']);
            return redirect('vendor-enquiry');
        }


        $profile = Query::get_profile();
        $query = Vemodel::join('vendors as v', 'v.vendor_uid', 'vendor_enquiries.venq_uid')->where('venq_is_deleted', 'N');
        if ($profile->user_role == 'vendor') {
            $query->where('v.vendor_uid', $profile->user_id);
        }
        if (@$_GET['SearchVendor'] != "") {
            $records = $query->where("venq_uid", "=", $_GET['SearchVendor']);
        }
        $records = $query->paginate(30);
        $vendors = UserModel::where('user_role', 'vendor')->where('user_is_deleted', 'N')->get();

        $data = compact('page', 'title', 'records', 'vendors');
        return view('frontend/layout', $data);
    }
}
