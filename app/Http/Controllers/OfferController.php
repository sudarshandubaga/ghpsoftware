<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use App\Models\Offer;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offers = Offer::latest()->paginate(10);
        return view('frontend.inc.offer.index', compact('offers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::get();
        return view('frontend.inc.offer.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $offer = new Offer();
        $offer->buyer_name = $request->buyer_name;
        $offer->refered_by = $request->refered_by;
        $offer->price_validity = $request->price_validity;
        $offer->lead_time = $request->lead_time;
        $offer->remarks = $request->remarks;
        $offer->terms = $request->terms;
        $offer->save();

        $carts = Cart::with('product')->where('type', 'offer')->get();
        $offerProducts = [];
        foreach ($carts as $c) {
            $offerProducts[$c->product_id] = [
                'qty'   => $c->qty,
                'margin' => $c->margin,
                'remarks'   => $c->remarks,
                'price' => round($c->product->product_mrp + $c->product->product_mrp * $c->margin / 100, 2),
                'moq'       => $c->moq
            ];
        }
        $offer->offer_products()->sync($offerProducts);

        Cart::where('type', 'offer')->delete();

        return redirect(route('offer.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        // dd($offer->offer_products);
        return view('frontend.inc.offer.show', compact('offer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        //
    }

    public function print(Offer $offer)
    {
        $data       = compact('offer');

        return view('frontend.inc.offer.print', $data);
    }
}
