<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Models\UserModel as Umodel;
use App\Models\Bproduct;
use App\Models\PIProduct;
use App\Models\POModel;
use App\Models\POProduct;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;

class Grid_reports extends BaseController
{
    public function vendor()
    {
        $title = "Vendor Reoprts";
        $page = "vendor_grid_reports";

        $vendors = Umodel::where('user_role', 'vendor')->where('user_is_deleted', 'N')->get();
        $data = compact('title', 'page', 'vendors');
        return view('frontend/layout', $data);
    }

    public function buyer()
    {
        $title = "Buyer Reoprts";
        $page = "buyer_grid_reports";

        $buyers = Umodel::where('user_role', 'buyer')->where('user_is_deleted', 'N')->get();
        $data = compact('title', 'page', 'buyers');
        return view('frontend/layout', $data);
    }

    public function grid_products(Request $request)
    {

        $Data = [];
        if (!empty($request->id)) {

            $products = POProduct::select(
                'po.*',
                'popro_sku',
                'popro_id',
                'grid_remark',
                'popro_qty',
                'popro_barcode',
                'vendor_org_name',
                'product_name',
                'product_code',
                'pi_id',
                'po_delivery_date',
                'country_name',
                'product_cbm'
            )
                ->join('purchase_orders as po', 'po.po_id', 'popro_po_id')
                ->join('purchase_invoices as pi', 'pi_id', 'po_pi_id')
                ->leftjoin('buyer_products as bp', 'bp.bpro_pid', 'popro_pid')
                ->leftjoin('vendors', 'vendor_uid', 'po.po_vendor')
                ->leftjoin('products as p', 'product_id', 'popro_pid')
                ->leftJoin('quotes as q', 'pi_qid', 'quote_id')
                ->leftJoin('buyers', 'buyer_uid', 'quote_uid')
                ->leftJoin('countries', 'country_id', 'buyer_np_country')
                ->where('po.po_vendor', $request->id)
                ->groupBy("popro_pid")
                ->groupBy("popro_po_id")
                ->orderBy('po_created_on', 'asc')
                ->get();

            // dd($products);
            $html = view('frontend.inc.get_product_grid', compact('products'))->render();
            $Data['html'] = $html;
            $Data['products'] = $products;

            // $products = DB::connection('mysql')
            //     ->table('po_products as pop')
            //     ->join('purchase_orders as po', 'po.po_id', 'pop.popro_po_id')
            //     ->join('buyer_products as bp', 'bp.bpro_pid', 'pop.popro_pid')
            //     ->where('po.po_vendor', $request->id)
            //     ->groupBy("pop.popro_po_id")
            //     ->get();
        }

        return response()->json($Data);
    }

    public function vendor_export(Request $request)
    {

        $Data = [];
        if (!empty($request->id)) {

            $products = POProduct::select(
                'po.*',
                'popro_sku',
                'popro_qty',
                'popro_barcode',
                'vendor_org_name',
                'product_name',
                'product_code',
                'pi_id',
                'po_delivery_date',
                'country_name',
                'product_cbm'
            )
                ->join('purchase_orders as po', 'po.po_id', 'popro_po_id')
                ->join('purchase_invoices as pi', 'pi_id', 'po_pi_id')
                ->leftjoin('buyer_products as bp', 'bp.bpro_pid', 'popro_pid')
                ->leftjoin('vendors', 'vendor_uid', 'po.po_vendor')
                ->leftjoin('products as p', 'product_id', 'popro_pid')
                ->leftJoin('quotes as q', 'pi_qid', 'quote_id')
                ->leftJoin('buyers', 'buyer_uid', 'quote_uid')
                ->leftJoin('countries', 'country_id', 'buyer_np_country')
                ->where('po.po_vendor', $request->id)
                ->groupBy("popro_pid")
                ->groupBy("popro_po_id")
                ->orderBy('po_created_on', 'asc')
                ->get();

            $list = collect([
                ['id' => 1, 'name' => 'Jane'],
                ['id' => 2, 'name' => 'John'],
            ]);
            (new FastExcel($products))->export('file.xlsx');

            // Excel::create('myexcel', function ($excel) use ($response_array) {

            //     $excel->sheet('sheet1', function ($sheet) use ($response_array) {
            //         $sheet->fromArray($response_array, null, 'A1', false, false);
            //     });
            // })->export('xls');


            // $products = DB::connection('mysql')
            //     ->table('po_products as pop')
            //     ->join('purchase_orders as po', 'po.po_id', 'pop.popro_po_id')
            //     ->join('buyer_products as bp', 'bp.bpro_pid', 'pop.popro_pid')
            //     ->where('po.po_vendor', $request->id)
            //     ->groupBy("pop.popro_po_id")
            //     ->get();
        }



        return response()->json($Data);
    }

    public function buyer_grid_products(Request $request)
    {

        $Data = [];
        if (!empty($request->id)) {

            $products = PIProduct::select(
                'pi_products.*',
                'pi.*',
                'user_name',
                'po_id',
                // 'popro_sku',
                // 'popro_qty',
                // 'popro_barcode',
                // 'vendor_org_name',
                'product_name',
                'product_code',
                // 'po_delivery_date',
                'country_name',
                'product_cbm'
            )
                ->join('purchase_invoices as pi', 'pi_id', 'pipro_pi_id')
                ->leftjoin('purchase_orders as po', 'po_pi_id', 'pi_id')
                ->leftjoin('buyer_products as bp', 'bp.bpro_pid', 'pipro_pid')
                ->leftjoin('products as p', 'product_id', 'pipro_pid')
                ->leftJoin('quotes as q', 'pi_qid', 'quote_uid')
                ->leftJoin('buyers', 'buyer_uid', 'quote_uid')
                ->leftJoin('users', 'user_id', 'buyer_uid')
                ->leftJoin('countries', 'country_id', 'buyer_np_country')
                ->where('quote_uid', $request->id)
                ->groupBy("pipro_pid")
                ->groupBy("pipro_pi_id")
                ->orderBy('pi_created_on', 'asc')
                ->get();

            // dd($products);
            $html = view('frontend.inc.buyer_grid_products', compact('products'))->render();
            $Data['html'] = $html;
            $Data['products'] = $products;

            // $products = DB::connection('mysql')
            //     ->table('po_products as pop')
            //     ->join('purchase_orders as po', 'po.po_id', 'pop.popro_po_id')
            //     ->join('buyer_products as bp', 'bp.bpro_pid', 'pop.popro_pid')
            //     ->where('po.po_vendor', $request->id)
            //     ->groupBy("pop.popro_po_id")
            //     ->get();
        }

        return response()->json($Data);
    }

    public function get_buyer_excel_products(Request $request)
    {

        $Data = [];
        if (!empty($request->id)) {

            $products = POProduct::select(
                'po.*',
                'popro_sku',
                'popro_qty',
                'popro_barcode',
                'vendor_org_name',
                'product_name',
                'product_code',
                'pi_id',
                'po_delivery_date',
                'country_name',
                'product_cbm'
            )
                ->join('purchase_orders as po', 'po.po_id', 'popro_po_id')
                ->join('purchase_invoices as pi', 'pi_id', 'po_pi_id')
                ->leftjoin('buyer_products as bp', 'bp.bpro_pid', 'popro_pid')
                ->leftjoin('vendors', 'vendor_uid', 'po.po_vendor')
                ->leftjoin('products as p', 'product_id', 'popro_pid')
                ->leftJoin('quotes as q', 'pi_qid', 'quote_id')
                ->leftJoin('buyers', 'buyer_uid', 'quote_uid')
                ->leftJoin('countries', 'country_id', 'buyer_np_country')
                ->where('po.po_vendor', $request->id)
                ->groupBy("popro_pid")
                ->groupBy("popro_po_id")
                ->orderBy('po_created_on', 'asc')
                ->get();

            $list = collect([
                ['id' => 1, 'name' => 'Jane'],
                ['id' => 2, 'name' => 'John'],
            ]);
            (new FastExcel($products))->export('file.xlsx');

            // Excel::create('myexcel', function ($excel) use ($response_array) {

            //     $excel->sheet('sheet1', function ($sheet) use ($response_array) {
            //         $sheet->fromArray($response_array, null, 'A1', false, false);
            //     });
            // })->export('xls');


            // $products = DB::connection('mysql')
            //     ->table('po_products as pop')
            //     ->join('purchase_orders as po', 'po.po_id', 'pop.popro_po_id')
            //     ->join('buyer_products as bp', 'bp.bpro_pid', 'pop.popro_pid')
            //     ->where('po.po_vendor', $request->id)
            //     ->groupBy("pop.popro_po_id")
            //     ->get();
        }



        return response()->json($Data);
    }

    public function update_grid(Request $request)
    {

        $validator = $request->validate([
            'message' => 'required',
            'id' => 'required',
        ]);



        $record = PIProduct::find($request->id);

        $record->grid_remark = $request->message;
        $record->save();
        $re = array(
            'status'    => TRUE,
            'message'   => 'Remark Updated Successfully'
        );
        return response()->json($re);
    }
    public function update_vendor_grid(Request $request)
    {

        $validator = $request->validate([
            'message' => 'required',
            'id' => 'required',
        ]);



        $record = POProduct::find($request->id);

        $record->grid_remark = $request->message;
        $record->save();
        $re = array(
            'status'    => TRUE,
            'message'   => 'Remark Updated Successfully'
        );
        return response()->json($re);
    }
}
