<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\QuoteModel as Quote;
use DB;

class Enquiry extends BaseController
{
    public function index(Request $request, $id = null)
    {
        $title     = "View Enquiry | GHP Software";
        $page     = "view_enquiry";

        if ($request->isMethod('post')) {
            $input = $request->input('check');

            DB::connection('mysql2')->table('orders')->whereIn('order_id', $input)->delete();
            return redirect()->back()->with('success', "Selected Records Deleted Sucessfully");
        }

        $records = DB::connection('mysql2')
            ->table('orders AS o')
            ->join('users AS u', 'o.order_uid', 'u.user_id');

        if (@$_GET['SearchByName'] != "") {
            $records = $records->where("user_name", "like", "%" . $_GET['SearchByName'] . "%");
        }

        if (@$_GET['SearchByQuote'] != "") {
            $records = $records->where("order_id", $_GET['SearchByQuote']);
        }

        $records = $records->orderBy('order_id', 'DESC')->paginate(30);

        if (!$records->isEmpty()) :
            foreach ($records as $key => $rec) {
                $records[$key]->quote_is_created = Quote::where('quote_enq_id', $rec->order_id)->where('quote_is_deleted', 'N')->count();
            }
        endif;

        $data = compact('page', 'title', 'records');
        return view('frontend/layout', $data);
    }
}
