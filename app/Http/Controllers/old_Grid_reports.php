<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Models\UserModel as Umodel;
use App\Models\Bproduct;
use App\Models\POModel;
use App\Models\POProduct;

class Grid_reports extends BaseController
{
    public function vendor()
    {
        $title = "Vendor Reoprts";
        $page = "vendor_grid_reports";

        $vendors = Umodel::where('user_role', 'vendor')->where('user_is_deleted', 'N')->get();
        $data = compact('title', 'page', 'vendors');
        return view('frontend/layout', $data);
    }

    public function grid_products(Request $request)
    {

        $Data = [];
        if (!empty($request->id)) {

            $products = POProduct::select(
                'po.*',
                'popro_sku',
                'popro_qty',
                'popro_barcode',
                'vendor_org_name',
                'product_name',
                'product_code',
                'pi_id',
                'po_delivery_date',
                'country_name',
                'product_cbm'
            )
                ->join('purchase_orders as po', 'po.po_id', 'popro_po_id')
                ->join('purchase_invoices as pi', 'pi_id', 'po_pi_id')
                ->leftjoin('buyer_products as bp', 'bp.bpro_pid', 'popro_pid')
                ->leftjoin('vendors', 'vendor_uid', 'po.po_vendor')
                ->leftjoin('products as p', 'product_id', 'popro_pid')
                ->leftJoin('quotes as q', 'pi_qid', 'quote_id')
                ->leftJoin('buyers', 'buyer_uid', 'quote_uid')
                ->leftJoin('countries', 'country_id', 'buyer_np_country')
                ->where('po.po_vendor', $request->id)
                ->groupBy("popro_pid")
                ->groupBy("popro_po_id")
                ->orderBy('po_created_on', 'asc')
                ->get();

            // dd($products);
            $html = view('frontend.inc.get_product_grid', compact('products'))->render();
            $Data['html'] = $html;
            $Data['products'] = $products;

            // $products = DB::connection('mysql')
            //     ->table('po_products as pop')
            //     ->join('purchase_orders as po', 'po.po_id', 'pop.popro_po_id')
            //     ->join('buyer_products as bp', 'bp.bpro_pid', 'pop.popro_pid')
            //     ->where('po.po_vendor', $request->id)
            //     ->groupBy("pop.popro_po_id")
            //     ->get();
        }

        return response()->json($Data);
    }
}
