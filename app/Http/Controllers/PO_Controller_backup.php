<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Models\Query;
use App\Models\VenqryModel as Vemodel;
use App\Models\VquoteModel as Vqmodel;
use App\Models\VenqproModel as Venqpro;
use App\Models\ProductModel as Product;
use App\Models\Vqproducts;
use App\Models\Vendor;
use App\Models\PIModel as PI;
use App\Models\POModel as PO;
use App\Models\POProduct;
use App\Models\Bproduct;
use App\Models\QuoteModel as Quote;
use App\Models\BuyerModel as Buyer;
use DB;

class PO_Controller extends BaseController
{
    public function add(Request $req, $quote_id, $id = NULL)
    {

        $AllPI = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->orderBy('pi_id', 'DESC')
            ->where("pi_confirm", 1)
            ->where('pi_is_deleted', 'N')->get();


        $record     = DB::table('vendor_quotes AS q')
            ->join('currencies AS c', 'q.vquote_currency', 'c.currency_id')
            ->where('vquote_id', $id)->first();
        $products   = DB::table('vquote_products AS qp')
            ->join('products AS p', 'qp.vqpro_pid', 'p.product_id')
            ->where('p.product_is_deleted', 'N')
            ->where('qp.vqpro_qid', $id)
            ->get();

        $quotes = Vqmodel::where('vquote_number', $record->vquote_number)->get();

        foreach ($quotes as $q) {

            foreach ($products as $key => $pro) {
                $proArr = [];
                $qprods = DB::table('vquote_products')->where('vqpro_qid', $q->vquote_id)->where('vqpro_pid', $pro->vqpro_pid)->where("vqpro_is_approved", "Y")->get();
                foreach ($qprods as $qp) {
                    $proArr[] = $qp;
                }
                $products[$key]->qpros = $proArr;
            }
        }

        if ($req->isMethod('post')) {
            $input = $req->input('record');
            $GetPIData = PI::find($input['po_pi_id']);
            $GetQuote = Quote::find($GetPIData->pi_qid);

            $input['po_qid'] = $id;
            $input['po_vendor'] = $record->vquote_uid;
            $input['po_currency'] = $record->vquote_currency;
            $input['po_price_term'] = $record->vquote_price_term;
            $input['po_delivery_date'] = $record->vquote_date;
            $input['po_delivery_days'] = $record->vquote_delivery_days;
            $input['po_payment'] = $record->vquote_payment . "";
            $input['po_cbm'] = $record->vquote_total_cbm;
            $input['po_total'] = $record->vquote_total;
            $input['container_type'] = $record->vquote_container_type;
            $input['po_created_on'] = date("Y-m-d H:i:s", time());
            $input['po_delivery_port'] = $record->vquote_delivery_port . "";
            $input['po_origin_country'] = $GetQuote->quote_origin_country . "";
            $input['po_first_destination_country'] = $GetQuote->quote_first_destination_country . "";
            $input['po_loading_port'] = $GetQuote->quote_loading_port . "";
            $input['po_discharge_port'] = $GetQuote->quote_discharge_port . "";
            $input['po_reciept_port'] = $GetQuote->quote_reciept_port . "";

            $NewPoID    = PO::insertGetId($input);

            $POOjb = PO::find($NewPoID);
            $POOjb->pono = "202021" . $NewPoID;
            $POOjb->save();

            POProduct::where('popro_po_id', $NewPoID)->delete();

            foreach ($products as $p) {

                $GetQuoteProducts = Vqproducts::where("vqpro_qid", $id)->get();

                $GetPIData = PI::find($input['po_pi_id']);
                $GetQuote = Quote::find($GetPIData->pi_qid);

                $bpro = Bproduct::where('bpro_uid', $GetQuote->quote_uid)->where('bpro_pid', $p->vqpro_pid)->first();

                $arr = [
                    'popro_po_id'       => $NewPoID,
                    'popro_pid'         => $p->vqpro_pid,
                    'popro_qty'         => $p->vqpro_qty,
                    'popro_sku'         => @$bpro->bpro_sku,
                    'popro_barcode'     => @$bpro->bpro_barcode,
                    'popro_buyer_desc'  => @$bpro->bpro_description,
                    "popro_price"       => $p->vqpro_price,
                ];
                POProduct::insert($arr);
            }

            session()->forget('pi_cart');
            $mess  = "A new PO has been created.";
            return redirect(url('purchase-order'))->with('success', $mess);
        }


        $title     = "Create Purchase Order";
        $page   = "add_po";
        $data   = compact('page', 'title', 'quote_id', 'AllPI', 'products', 'record', 'quotes');
        return view('frontend/layout', $data);
    }


    public function edit(Request $req, $quote_id)
    {
        $record     = PO::find($quote_id);


        $products   = POProduct::leftjoin('products AS p', 'p.product_id', 'popro_pid')->where('popro_po_id', $quote_id)->get();

        $vendor =  Vendor::get();
        $page     = "edit_po";
        $data =  compact('vendor', 'page', 'products', "quote_id", "record");
        return view('frontend/layout', $data);
    }

    public function saveedit(Request $req, $quote_id, $id = NULL)
    {
        $record = PO::find($req->POID);
        $record->po_vendor = $req->vendor;
        $record->po_date = $req->PoDate;
        $record->save();

        foreach ($req->InvoiceProducts as $ProId => $AL) {
            $POP = POProduct::find($ProId);
            $POP->popro_qty = $AL["qty"];
            $POP->popro_price = $AL["price"];
            $POP->save();
        }

        foreach ($req->PoDelPro as $ID) {
            POProduct::find($ID)->delete();
        }

        return redirect('purchase-order');
    }


    public function index(Request $request, $id = null)
    {
        $dbprefix = env('DB_PREFIX');

        $query = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->orderBy('pi_id', 'DESC')
            ->where('pi_is_deleted', 'N');

        $profile = Query::get_profile();



        if ($profile->user_role == "buyer") {
            $query->where('q.quote_uid', $profile->user_id);
        }

        $records = $query->paginate(15);

        $title     = "View PI";
        $page     = "view_pi";
        $data = compact('page', 'title', 'records');
        return view('frontend/layout', $data);
    }

    public function print($id)
    {
        $record     = PO::join('users AS u', 'po_vendor', 'u.user_id')
            ->join('vendors AS ven', 'po_vendor', 'ven.vendor_uid')
            ->join('currencies AS c', 'po_currency', 'c.currency_id')
            ->leftJoin('countries AS con', 'u.user_country', 'con.country_id')
            ->leftJoin('states AS st', 'u.user_state', 'st.state_id')
            ->find($id);

        $products   = POProduct::leftjoin('products AS p', 'p.product_id', 'popro_pid')->where('popro_po_id', $id)->get();

        $PIData     = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->join('buyers', 'u.user_id', 'buyers.buyer_uid')
            ->find($record->po_pi_id);

        $title         = "Purchase Order | GHP Software";
        $data       = compact('title', 'record', 'products', "PIData");
        return view('frontend/purchase-order', $data);
    }

    public function copy(Request $req, $OldId)
    {
        $record     = PO::find($OldId);

        $NewPo = $record->replicate();
        $NewPo->save();

        $ID = $NewPo->po_id;


        $POOjb = PO::find($ID);
        $POOjb->pono = "1508" . $ID;
        $POOjb->save();

        $products   = POProduct::where('popro_po_id', $OldId)->get();

        foreach ($products as $p) {
            $PrOPo = POProduct::find($p->popro_id);
            $NewPro = $PrOPo->replicate();
            $NewPro->popro_po_id = $ID;
            $NewPro->save();
        }

        $mess  = "PO Copied Successfully";
        return redirect(url('purchase-order'))->with('success', $mess);
    }
}
