<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Models\Query;
// use App\Models\VenqryModel as Vemodel;
// use App\Models\VquoteModel as Vqmodel;
// use App\Models\VenqproModel as Venqpro;
use App\Models\ProductModel as Product;
// use App\Models\QuoteModel as Quote;
// use App\Models\Bproduct;
use App\Models\PIModel as PI;
use App\Models\POModel as PO;
use App\Models\POProduct;
use App\Models\BuyerModel;
use App\Models\CancelledPurchaseOrder;
use App\Models\PoBookingInvoice;
use App\Models\PoBookingInvoiceProduct;

use App\Models\PoInvoice;
use App\Models\PoInvoiceProduct;

// use DB;
use App\Models\UserModel;
use App\Models\Vendor;
use Illuminate\Support\Facades\DB;

class PurchaseOrderController extends BaseController
{
    public function index()
    {
        session()->forget("po_transfer_items");

        $dbprefix = env('DB_PREFIX');
        $query = PO::leftjoin('vendor_quotes AS q', 'q.vquote_id', 'purchase_orders.po_qid')
            ->leftjoin('users AS u', 'purchase_orders.po_vendor', 'u.user_id')
            ->leftjoin('currencies AS c', 'q.vquote_currency', 'c.currency_id')

            ->leftjoin('purchase_invoices AS pi', 'purchase_orders.po_pi_id', 'pi.pi_id')
            ->leftjoin('quotes AS mq', 'pi.pi_qid', 'mq.quote_id')


            ->leftjoin('buyers AS b', 'mq.quote_uid', 'b.buyer_uid')
            ->orderBy('po_id', 'DESC')
            ->where('po_is_deleted', 'N');

        $profile = Query::get_profile();
        if ($profile->user_role == "vendor") {
            $query->where('q.vquote_uid', $profile->user_id);
        }


        $vendors = UserModel::where('user_role', 'vendor')->where('user_is_deleted', 'N')->get();
        $buyers = BuyerModel::orderBy('buyer_code')->whereHas('quote.purchase_invoice.porder', function ($q) use ($profile) {
            $q->where('po_vendor', $profile->user_id);
        })->pluck('buyer_code', 'buyer_uid');

        if (@$_GET['SearchVendor'] != "") {
            $records = $query->where("user_id", "=", $_GET['SearchVendor']);
        }

        if (!empty($_GET['SearchBuyer'])) {
            $records = $query->where("buyer_uid", $_GET['SearchBuyer']);
        }

        if (@$_GET['SearchBypno'] != "") {
            $po_no = substr($_GET['SearchBypno'], 4) - 100;
            $records = $query->where("po_id", "like", "%" . $po_no . "%");
        }

        if (@$_GET['SearchByOrderNo'] != "") {
            $records = $query->where("po_order_ref", "like", "%" . $_GET['SearchByOrderNo'] . "%");
        }

        if (@$_GET['SearchByPINo'] != "") {
            $pi_no = $_GET['SearchByPINo'] - 100;
            $records = $query->where("po_pi_id", "like", "%" . $pi_no . "%");
        }
        if (@$_GET['SearchStatus'] != "") {
            if (@$_GET['SearchStatus'] == 1) {
                // $records = $query->has("po_invoice");
                $records = $query->whereRaw("po_id IN (SELECT po_invoice_po FROM exs_po_invoice)");
            } else {
                // $records = $query->has("po_invoice","<",1);
                $records = $query->whereRaw("po_id NOT IN (SELECT po_invoice_po FROM exs_po_invoice)");
            }
        }
        $records = $query->paginate(15);

        $title     = "View PO";
        $page     = "purchase-order.index";
        $data = compact('page', 'title', 'records', 'vendors', 'buyers');
        return view('frontend/layout', $data);
    }

    public function AllVendorInvoice(Request $Request)
    {
        $dbprefix = env('DB_PREFIX');
        $query = PoInvoice::orderBy('po_invoice_id', 'DESC')->with('po');
        $profile = Query::get_profile();
        if ($profile->user_role == "vendor") {
            $query->where('po_invoice_vendor_id', $profile->user_id);
        }
        if (@$_GET['SearchVendor'] != "") {
            $vendor_id = $_GET['SearchVendor'];
            $records = $query->whereHas("vendor", function ($q) use ($vendor_id) {
                $q->where('vendor_uid', $vendor_id);
            });
        }
        $records = $query->paginate(15);
        $vendors = UserModel::where('user_role', 'vendor')->where('user_is_deleted', 'N')->get();
        $title     = "Vendor Commercial Invoice";
        $page     = "ViewVendorPOInvoice";
        $data = compact('page', 'title', 'records', 'vendors');
        return view('frontend/layout', $data);
    }

    public function AllVendorBookingInvoice(Request $Request)
    {
        $dbprefix = env('DB_PREFIX');
        $query = PoBookingInvoice::orderBy('po_invoice_id', 'DESC')->with('vendor');
        $profile = Query::get_profile();
        if ($profile->user_role == "vendor") {
            $query->where('po_invoice_vendor_id', $profile->user_id);
        }
        if (@$_GET['SearchByInvoiceNo'] != "") {
            $records = $query->where("po_invoice_invoice_no", "=", $_GET['SearchByInvoiceNo']);
        }
        if (@$_GET['SearchVendor'] != "") {
            $vendor_id = $_GET['SearchVendor'];
            $records = $query->whereHas("vendor", function ($q) use ($vendor_id) {
                $q->where('vendor_uid', $vendor_id);
            });
        }
        $vendors = UserModel::where('user_role', 'vendor')->where('user_is_deleted', 'N')->get();
        $records = $query->paginate(15);
        $title     = "Vendor Booking Invoice";
        $page     = "ViewVendorBookingInvoice";
        $data = compact('page', 'title', 'records', 'vendors');
        return view('frontend/layout', $data);
    }

    public function ChooseBuyer()
    {
        $profile = Query::get_profile();
        $query = PO::leftjoin('vendor_quotes AS q', 'q.vquote_id', 'purchase_orders.po_qid')
            ->leftjoin('purchase_invoices', 'purchase_invoices.pi_id', 'purchase_orders.po_pi_id')
            ->leftjoin('quotes AS qs', 'qs.quote_id', 'purchase_invoices.pi_qid')
            ->leftjoin('buyers', 'buyers.buyer_uid', 'qs.quote_uid')
            ->where('po_is_deleted', 'N')
            ->where('q.vquote_uid', $profile->user_id)
            ->get();

        $BuyerArray = array();
        foreach ($query as $POO) {
            $BuyerArray[$POO->buyer_uid] = $POO->buyer_code;
        }

        $title     = "Create Invoice";
        $page     = "purchase-order.choose_buyer";
        $data = compact('page', 'title', 'BuyerArray');
        return view('frontend/layout', $data);
    }

    public function LoadBuyerPO(Request $req, $id)
    {
        $profile = Query::get_profile();
        $query = PO::leftjoin('vendor_quotes AS q', 'q.vquote_id', 'purchase_orders.po_qid')
            ->leftjoin('purchase_invoices', 'purchase_invoices.pi_id', 'purchase_orders.po_pi_id')
            ->leftjoin('quotes AS qs', 'qs.quote_id', 'purchase_invoices.pi_qid')
            ->leftjoin('buyers', 'buyers.buyer_uid', 'qs.quote_uid')
            ->where('po_is_deleted', 'N')
            ->where('buyers.buyer_uid', $id)
            ->where('q.vquote_uid', $profile->user_id)
            ->get();

        $AllPo = array();
        foreach ($query as $QR) {
            $products   = POProduct::leftjoin("purchase_orders", "purchase_orders.po_id", "popro_po_id")->where('popro_po_id', $QR->po_id)->get();

            $POFinishes = 0;
            foreach ($products as $Pindex => $p) {
                $GetInvoicePro = PoBookingInvoiceProduct::where("exs_po_invoice_products_product_id", $p->popro_pid)->where("po_invoice_qid", $p->po_qid)->sum("pip_qty");
                $RemaingQty = $p->popro_qty - $GetInvoicePro;

                if ($RemaingQty > 0) {
                    $POFinishes = 1;
                }
            }

            if ($POFinishes == 1) {
                $AllPo[] = $QR->po_id;
            }
        }

        return view('frontend.inc.BuyerPo', compact("AllPo"));
    }

    public function CreateVendorInvoice(Request $req)
    {
        $record     = PO::join('vendor_quotes AS q', 'q.vquote_id', 'purchase_orders.po_qid')
            ->join('users AS u', 'q.vquote_uid', 'u.user_id')
            ->join('vendors AS ven', 'q.vquote_uid', 'ven.vendor_uid')
            ->join('currencies AS c', 'q.vquote_currency', 'c.currency_id')
            ->leftJoin('countries AS con', 'u.user_country', 'con.country_id')
            ->leftJoin('states AS st', 'u.user_state', 'st.state_id')
            ->whereIn("po_id", $req->pos)->get();


        $products   = PoProduct::leftjoin("purchase_orders", "purchase_orders.po_id", "popro_po_id")->leftjoin('products AS p', 'p.product_id', 'popro_pid')->whereIn('popro_po_id', $req->pos)->get();


        if ($req->isMethod('post')) {
            $input = $req->input('record');
            $ProducrtsInput = $req->input('InvoiceProducts');

            $profile = Query::get_profile();
            $input['po_invoice_vendor_id'] = $profile->user_id;
            //$input['po_invoice_pi'] = $record->po_pi_id;
            $id    = PoBookingInvoice::insertGetId($input);




            $mess  = "A new PO has been created.";
            $totalCBM = $totalAmount = 0;
            foreach ($ProducrtsInput as $p) {
                if ($p['qty'] > 0) {
                    $arr = [
                        'exs_po_invoice_products_inv_id'            => $id,
                        'exs_po_invoice_products_product_id'        => $p['id'],
                        'po_invoice_qid'                            => $p['po_invoice_qid'],
                        'invoice_po_no'                             => $p['invoice_po_no'],
                        'pip_qty'                                   => $p['qty'],
                        'pip_price'                                 => $p['price'],
                        'pip_subtotal'                              => $p['subtotal'],
                        'pip_Pcsctn'                                => $p['Pcsctn'],
                        'pip_TotalPackets'                          => $p['TotalPackets'],
                        'pip_NWwWd'                                 => $p['NWwWd'],
                        'pip_NWIrn'                                 => $p['NWIrn'],
                        'pip_TotalNetWeightKG'                      => $p['TotalNetWeightKG'],
                        'pip_GWPktKgs'                              => $p['GWPktKgs'],
                        'pip_TotalGrossWeight'                      => $p['TotalGrossWeight'],
                        'pip_PkgDimL'                               => json_encode($p['PkgDimL']),
                        'pip_PkgDimW'                               => json_encode($p['PkgDimW']),
                        'pip_PkgDimH'                               => json_encode($p['PkgDimH']),
                        'pip_Meas'                                  => json_encode($p['Meas']),
                        'pip_TotalMeas'                             => $p['TotalMeas'],
                        'pip_HSCode'                                => $p['HSCode'] . "",
                        'pip_case_no'                               => $p['caseno'] . "",
                    ];

                    $totalCBM += $p['TotalMeas'];
                    $totalAmount += $p['subtotal'];

                    PoBookingInvoiceProduct::insert($arr);
                }
            }

            $poBookingInvoice = PoBookingInvoice::find($id);
            $poBookingInvoice->po_invoice_invoice_no        = sprintf("%s%04d", "BWS", $id);
            $poBookingInvoice->po_invoice_total_cbm         = $totalCBM;
            $poBookingInvoice->po_invoice_total_amount      = $totalAmount;
            $poBookingInvoice->save();

            return redirect(url('vendor-booking-invoice'))->with('success', "Booking Created Successfully");
        }

        $POIDS = $req->pos;

        $title     = "Booking Invoice Cum Packing List";
        $page   = "purchase-order.po-invoice.create";
        $data   = compact('page', 'title', 'products', 'record', 'POIDS');
        return view('frontend/layout', $data);
    }

    public function CreateFinalVendorInvoice(Request $req)
    {
        $GetBookingData = PoBookingInvoice::find($req->id);
        $products   = PoBookingInvoiceProduct::leftjoin('products AS p', 'p.product_id', 'exs_po_invoice_products_product_id')->where('exs_po_invoice_products_inv_id', $req->id)->get();
        $BookingID = $req->id;

        if ($req->isMethod('post')) {
            $input = $req->input('record');
            $ProducrtsInput = $req->input('InvoiceProducts');

            $profile = Query::get_profile();
            $input['po_invoice_vendor_id'] = $profile->user_id;
            $id    = PoInvoice::insertGetId($input);

            foreach ($ProducrtsInput as $p) {
                if ($p['qty'] > 0) {
                    $arr = [
                        'exs_po_invoice_products_inv_id'            => $id,
                        'exs_po_invoice_products_product_id'        => $p['id'],
                        'po_invoice_qid'                            => $p['po_invoice_qid'],
                        'invoice_po_no'                             => $p['invoice_po_no'],
                        'pip_qty'                                   => $p['qty'],
                        'pip_price'                                 => $p['price'],
                        'pip_subtotal'                              => $p['subtotal'],
                        'pip_Pcsctn'                                => $p['Pcsctn'],
                        'pip_TotalPackets'                          => $p['TotalPackets'],
                        'pip_NWwWd'                                 => $p['NWwWd'],
                        'pip_NWIrn'                                 => $p['NWIrn'],
                        'pip_TotalNetWeightKG'                      => $p['TotalNetWeightKG'],
                        'pip_GWPktKgs'                              => $p['GWPktKgs'],
                        'pip_TotalGrossWeight'                      => $p['TotalGrossWeight'],
                        'pip_PkgDimL'                               => $p['PkgDimL'],
                        'pip_PkgDimW'                               => $p['PkgDimW'],
                        'pip_PkgDimH'                               => $p['PkgDimH'],
                        'pip_Meas'                                  => $p['Meas'],
                        'pip_TotalMeas'                             => $p['TotalMeas'],
                        'pip_HSCode'                                => $p['HSCode'] . "",
                        'pip_case_no'                               => $p['caseno'] . "",
                    ];
                    PoInvoiceProduct::insert($arr);
                }
            }
            return redirect(url('vendor-invoice'))->with('success', "Invoice Created Successfully");
        }

        $POIDS = $req->pos;

        $title     = "Create Purchase Order";
        $page   = "CreateFinalPoInvoice";
        $data   = compact('page', 'title', 'products', 'GetBookingData', "BookingID");
        return view('frontend/layout', $data);
    }

    public function BookingCIPrint($id)
    {
        $GetInvoiceData = PoBookingInvoice::find($id);
        $InvoiceData = PoBookingInvoiceProduct::where("exs_po_invoice_products_inv_id", $id)->get();

        $GetVendorData = UserModel::leftjoin("vendors", "vendors.vendor_uid", "user_id")->find($GetInvoiceData->po_invoice_vendor_id);

        $PODATA     = PO::join('vendor_quotes AS q', 'q.vquote_id', 'purchase_orders.po_qid')
            ->join('users AS u', 'q.vquote_uid', 'u.user_id')
            ->join('vendors AS ven', 'q.vquote_uid', 'ven.vendor_uid')
            ->join('currencies AS c', 'q.vquote_currency', 'c.currency_id')
            ->leftJoin('countries AS con', 'u.user_country', 'con.country_id')
            ->leftJoin('states AS st', 'u.user_state', 'st.state_id')
            ->find($GetInvoiceData->po_invoice_po);

        $PIDATA     = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->leftJoin('countries AS con', 'u.user_country', 'con.country_id')
            ->leftJoin('states AS st', 'u.user_state', 'st.state_id')
            ->find($PODATA->po_pi_id);


        return view("frontend.VendorBookingInvoice", compact("GetInvoiceData", "InvoiceData", "GetVendorData", "PODATA", "id", "PIDATA"));
    }

    public function CIPrint($id)
    {
        $GetInvoiceData = PoInvoice::find($id);
        $InvoiceData = PoInvoiceProduct::where("exs_po_invoice_products_inv_id", $id)->get();

        $GetVendorData = UserModel::leftjoin("vendors", "vendors.vendor_uid", "user_id")->find($GetInvoiceData->po_invoice_vendor_id);

        $PODATA     = PO::join('vendor_quotes AS q', 'q.vquote_id', 'purchase_orders.po_qid')
            ->join('users AS u', 'q.vquote_uid', 'u.user_id')
            ->join('vendors AS ven', 'q.vquote_uid', 'ven.vendor_uid')
            ->join('currencies AS c', 'q.vquote_currency', 'c.currency_id')
            ->leftJoin('countries AS con', 'u.user_country', 'con.country_id')
            ->leftJoin('states AS st', 'u.user_state', 'st.state_id')
            ->find($GetInvoiceData->po_invoice_po);

        $id =  $id;
        return view("frontend.CommercialInvoicePrint", compact("GetInvoiceData", "InvoiceData", "GetVendorData", "PODATA", "id"));
    }
    public function exportCsv(Request $request)
    {
        $fileName = 'reports.csv';
        // $tasks = SurveyHistory::with('user','survey')->get()->toArray();
        $query = PO::leftjoin('vendor_quotes AS q', 'q.vquote_id', 'purchase_orders.po_qid')
            ->leftjoin('users AS u', 'q.vquote_uid', 'u.user_id')
            ->leftjoin('currencies AS c', 'q.vquote_currency', 'c.currency_id')
            // ->leftjoin('po_invoice AS poi', 'poi.po_invoice_po', 'purchase_orders.po_id')
            ->orderBy('po_id', 'DESC')
            // ->with('po_invoice')
            // ->withCount('po_invoice')
            ->where('po_is_deleted', 'N');

        $profile = Query::get_profile();
        if ($profile->user_role == "vendor") {
            $query->where('q.vquote_uid', $profile->user_id);
        }


        $vendors = UserModel::where('user_role', 'vendor')->where('user_is_deleted', 'N')->get();

        if (@$request->SearchVendor != "") {
            $records = $query->where("user_id", "=", $request->SearchVendor);
        }



        if (@$request->SearchBypno != "") {
            $po_no = substr($request->SearchBypno, 4) - 100;
            $records = $query->where("po_id", "like", "%" . $po_no . "%");
        }

        if (@$request->SearchByOrderNo != "") {
            $records = $query->where("po_order_ref", "like", "%" . $request->SearchByOrderNo . "%");
        }

        if (@$request->SearchByPINo != "") {
            $pi_no = $request->SearchByPINo - 100;
            $records = $query->where("po_pi_id", "like", "%" . $pi_no . "%");
        }
        if (@$request->SearchStatus != "") {
            if (@$request->SearchStatus == 1) {
                // $records = $query->has("po_invoice");
                $records = $query->whereRaw("po_id IN (SELECT po_invoice_po FROM exs_po_invoice)");
            } else {
                // $records = $query->has("po_invoice","<",1);
                $records = $query->whereRaw("po_id NOT IN (SELECT po_invoice_po FROM exs_po_invoice)");
            }
        }
        $lists = $query->get()->toArray();

        $listArr = [];

        $site = DB::table('settings')->first();
        foreach ($lists as $key => $l) {
            // dd($l);
            // $listArr[] = $l;
            $GetPIData = \App\Models\PIModel::find($l['po_pi_id']);
            $po_invoice_count = \App\Models\PoInvoice::where('po_invoice_po', $l['po_id'])->count();

            $buyerCode = $GetPIData->quote->buyer ? trim($GetPIData->quote->buyer->buyer_code) : '';

            //dd($GetPIData);


            $listArr[$key]['PO No'] = !empty($l['po_id']) ? '2021' . ($l['po_id'] + 100) : '';
            //$listArr[$key]['Quote ID'] = sprintf("%s%06d", $site->setting_quote_prefix, $l['vquote_number']);
            $listArr[$key]['Orfer Ref. '] = @$GetPIData->pi_order_ref;
            $listArr[$key]['PI No.'] = !empty($GetPIData) ? sprintf("%s-%03d", 'GHP-202122', $GetPIData->pi_id + 100) : '';
            $listArr[$key]['Customer Name'] = !empty($l['user_name']) ? $l['user_name'] : '';
            //$listArr[$key]['Customer Mobile No.'] = !empty($l['user_mobile']) ? $l['user_mobile'] : '';
            //$listArr[$key]['Customer Email'] = !empty($l['user_email']) ? $l['user_email'] : '';
            $listArr[$key]['Total Amount'] = !empty($l['vquote_total']) ? $l['currency_sign'] . '' . number_format($l['vquote_total'], 2) : '';
            $listArr[$key]['Total CBM'] =  !empty($l['vquote_total_cbm']) ? $l['vquote_total_cbm'] : '';
            $listArr[$key]['Date'] =  !empty($l['po_date']) ? date("d-M-Y", strtotime($l['po_date'])) : '';
            // $listArr[$key]['Created At'] =  !empty($l['po_created_on']) ?date("d-M-Y", strtotime($l['po_created_on'])) : '' ;
            $listArr[$key]['Status'] =  $po_invoice_count > 0 ? 'Shipped' : 'Running';
            $listArr[$key]['Consignee Name'] = $buyerCode;
            $listArr[$key]['ETD'] =  date("d-M-Y", strtotime($l['po_delivery_days']));

            // $listArr[$key]['dp_date'] = $l['dp_date'] ? date("d-M-Y", strtotime($l['dp_date'])) : '';


        }

        $this->download_send_headers("po_reports" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }
    public  function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

    public function transfer(PO $purchaseOrder)
    {
        $vendors = Vendor::orderBy('vendor_org_name')->where("vendor_org_name", "!=", "")->pluck('vendor_org_name', 'vendor_uid');

        // session()->forget("po_trasnfer_items");
        // session()->flush();
        $cartItems = session("po_transfer_items");
        // dd(session());


        $products = [];
        if (!empty($cartItems)) :
            foreach ($cartItems as $pid => $cartData) {
                $product = Product::where('product_id', $pid)->first();

                foreach ($cartData as $key => $cData)
                    $product->{$key} = $cData;

                $products[] = $product;
            }
        endif;

        return view("frontend.inc.purchase-order.transfer", compact('purchaseOrder', 'vendors', 'products'));
    }

    public function storeTransfer(Request $request, PO $purchaseOrder)
    {
        // $poDetails = $purchaseOrder->toArray();
        // $requests = $request->all();
        // dd(compact('poDetails', 'requests'));
        // $request->

        session()->forget("po_transfer_items");

        $totalCBM = $total = 0;
        foreach ($request->product as $p) {
            $totalCBM += $request->cbm[$p['popro_pid']] * $p['popro_qty'];
            $total    += $p['popro_price'] * $p['popro_qty'];

            $cancelledPurchaseOrder = new CancelledPurchaseOrder();
            $cancelledPurchaseOrder->purchase_order_id = $purchaseOrder->po_id;
            $cancelledPurchaseOrder->product_id = $p['popro_pid'];
            $cancelledPurchaseOrder->qty = $p['popro_qty'];
            $cancelledPurchaseOrder->save();

            $poProduct = POProduct::where("popro_pid", $p['popro_pid'])
                ->where('popro_po_id', $purchaseOrder->po_id)
                ->first();

            // dd($poProduct);

            $poProduct->popro_qty -= $p['popro_qty'];
            $poProduct->save();
        }

        $pOrder = new PO();
        $pOrder->po_vendor = $request->vendor_id;
        $pOrder->po_delivery_date = $request->po_delivery_date;
        $pOrder->po_delivery_days = $request->po_delivery_days;
        $pOrder->po_date = $request->po_date;

        $pOrder->po_cbm = $totalCBM;
        $pOrder->po_total = $total;

        $pOrder->po_qid = $purchaseOrder->po_qid;
        $pOrder->po_pi_id = $purchaseOrder->po_pi_id;
        $pOrder->po_currency = $purchaseOrder->po_currency;
        $pOrder->po_price_term   = $purchaseOrder->po_price_term;
        $pOrder->po_delivery_days = $request->po_delivery_days;
        $pOrder->po_payment = $purchaseOrder->po_payment;
        $pOrder->po_delivery_port = $purchaseOrder->po_delivery_port;
        $pOrder->po_origin_country = $purchaseOrder->po_origin_country;
        $pOrder->po_first_destination_country = $purchaseOrder->po_first_destination_country;
        $pOrder->po_loading_port = $purchaseOrder->po_loading_port;
        $pOrder->po_discharge_port = $purchaseOrder->po_discharge_port;
        $pOrder->container_type = $purchaseOrder->container_type;
        $pOrder->po_reciept_port = $purchaseOrder->po_reciept_port;
        $pOrder->po_order_ref = $purchaseOrder->po_order_ref;
        $pOrder->save();

        $pOrder->pono = "202021" . $pOrder->po_id;
        $pOrder->save();

        $pOrder->products()->sync($request->product);

        return redirect(route('purchase-order.index'))->with("success", "Success! Transfer has been done.");
    }

    public function cartStore(Request $request)
    {
        $cartItems = session()->get("po_transfer_items");
        // extract($request->item);

        if (empty($cartItems[$request->id])) :
            $cartItems[$request->id] = [
                'popro_qty'         => 1,
                'popro_sku'         => $request->item['popro_sku'],
                'popro_barcode'     => $request->item['popro_barcode'],
                'popro_buyer_desc'  => $request->item['popro_buyer_desc'],
                'popro_price'       => $request->item['popro_price'],
                'max_qty'           => $request->item['popro_qty'],
            ];
        endif;

        session()->put("po_transfer_items", $cartItems);

        return response()->json(["msg" => "success"]);
    }

    public function download_send_headers($filename)
    {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
}
