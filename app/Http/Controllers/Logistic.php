<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Models\QuoteModel as Quote;
use App\Models\UserModel as Umodel;
use App\Models\PIModel as PI;
use App\Models\POModel as PO;
use App\Models\POProduct;
use App\Models\Query;
use App\Models\PoBookingInvoice;
use App\Models\PoBookingInvoiceProduct;
use Illuminate\Support\Facades\DB;

class Logistic extends BaseController
{

    public function ViewAll(Request $request, $id = null)
    {
        $title     = "View Logistic | GHP Software";
        $page     = "view_logistic";

        $records = DB::table('logistics')->leftjoin("users", "logistics.exs_logistics_buyer", "users.user_id")->orderBy('track_status');

        if (@$_GET['SearchByPI'] != "") {
            $records = $records->where("exs_logistics_pi", $_GET['SearchByPI']);
        }

        if (@$_GET['SearchByBuyer'] != "") {
            $records = $records->where("user_name", "like", "%" . $_GET['SearchByBuyer'] . "%");
        }

        if (@$_GET['SearchByPO'] != "") {
            $records = $records->where("exs_logistics_po", $_GET['SearchByPO']);
        }

        if (@$_GET['SearchByGHP'] != "") {
            $records = $records->where("GHPOrderNo", "like", "%" . $_GET['SearchByGHP'] . "%");
        }

        if (@$_GET['TrackStatus'] != "") {
            $records = $records->where("track_status", $_GET['TrackStatus']);
        }

        $query = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->orderBy('pi_id', 'DESC')
            ->where('pi_is_deleted', 'N');

        $profile = Query::get_profile();

        if ($profile->user_role == "buyer") {
            $query->where('q.quote_uid', $profile->user_id);
        }

        $query = $query->get();

        $PInIDS = array();
        foreach ($query as $RQQR) {
            $PInIDS[] = "GHP-201819-" . sprintf('%03d', $RQQR->pi_id) . ",";
        }


        if ($profile->user_role == "buyer") {
            $records = $records->whereIn("exs_logistics_pi", $PInIDS);
        }

        $records = $records->paginate(30);

        $data = compact('page', 'title', 'records');
        return view('frontend/layout', $data);
    }

    public function Add(Request $request, $id = "")
    {
        $title     = "Add Logistic | GHP Software";
        $page     = "add_logistic";

        $edit = array();
        $PIData = array();
        $PoData = array();
        if (!empty($id)) {
            $edit = DB::table('logistics')->where("exs_logistics_id", $id)->first();
            $PIData = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
                ->orderBy('pi_id', 'DESC')
                ->where('pi_is_deleted', 'N')
                ->where('q.quote_uid', $edit->exs_logistics_pi)->get();


            $PoData = PO::orderBy('po_id', 'DESC')
                ->where('po_is_deleted', 'N')
                ->where('po_pi_id', $edit->exs_logistics_pi)->get();
        }


        if ($request->isMethod('post')) {
            $input = $request->input('user');
            if (!empty($input)) {
                if (empty($id)) {
                    DB::table('logistics')->insert($input);
                    $id = DB::getPdo()->lastInsertId();
                } else {
                    DB::table('logistics')->where('exs_logistics_id', $id)->update($input);
                }
            }
            return redirect('view-logistic');
        }

        $Forwarder = DB::table('forwarder')->where('is_deleted', 'N')->get();
        $ShippingLIne = DB::table('shipping_line')->where('is_deleted', 'N')->get();
        $Buyer = Umodel::where('user_role', "buyer")->where('user_is_deleted', 'N')->orderBy("user_name")->get();

        $AllPorts = DB::table('ports')->orderBy("port_name")->get();

        $data = compact('page', 'title', 'Forwarder', "ShippingLIne", "Buyer", "edit", "PIData", "PoData", "AllPorts");
        return view('frontend/layout', $data);
    }

    public function LoadPI(Request $request, $id)
    {

        $GetBookingData = PoBookingInvoice::with('vendor')->find($id);
        $GetPOData = PO::find($GetBookingData->po_invoice_po);

        $id = $GetPOData->po_pi_id;
        $PIData = PI::with('quote.buyer.user')->find($GetPOData->po_pi_id);

        $poNumbers = explode(",", $GetBookingData->po_invoice_po);

        $newPos = [];
        foreach ($poNumbers as $poNum) {
            $newPos[] = "2021" . ($poNum + 100);
        }
        $poNos = implode(", ", $newPos);

        // dd($GetPOData->toArray());

        // dd($GetBookingData->toArray());


        // $query = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
        //     ->orderBy('pi_id', 'DESC')
        //     ->where('pi_is_deleted', 'N')
        //     ->where('pi_id', $id)->get();

        // $query1 = PO::orderBy('po_id', 'DESC')
        //     ->where('po_is_deleted', 'N')
        //     ->where('po_pi_id', $id)->get();

        // $query3 = PO::join('vendor_quotes AS q', 'q.vquote_id', 'purchase_orders.po_qid')
        //     ->join('users AS u', 'q.vquote_uid', 'u.user_id')
        //     ->join('vendors AS ven', 'q.vquote_uid', 'ven.vendor_uid')->where('po_pi_id', $id)->first();

        // $products  = POProduct::leftjoin('products AS p', 'p.product_id', 'popro_pid')->where('popro_po_id', $id)->get();
        // $record     = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
        //     ->join('users AS u', 'q.quote_uid', 'u.user_id')
        //     ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
        //     ->leftJoin('countries AS con', 'u.user_country', 'con.country_id')
        //     ->leftJoin('states AS st', 'u.user_state', 'st.state_id')
        //     ->find($id);
        // $TotalCBM = 0;
        // $arr = [];
        // foreach ($products as $p) {
        //     $GetQuoteDetail = DB::table('vquote_products')->where('vqpro_pid', $p->popro_pid)->where('vqpro_qid', $query3->po_qid)->first();
        //     if (isset($GetQuoteDetail->vqpro_qty)) {
        //         $TotalCBM += $p->product_cbm * $GetQuoteDetail->vqpro_qty;
        //         $arr[] = [
        //             'id' => $p->id,
        //             'cbm' => $p->product_cbm,
        //             'qty' => $GetQuoteDetail->vqpro_qty
        //         ];
        //     }
        // }
        return view("frontend.logistic.LoadPi", compact(
            // 'query', 'query1', 'TotalCBM', 
            // 'query3',
            // 'record',

            'PIData',
            'GetBookingData',
            'poNos'
        ));
    }

    public function LoadPO(Request $request, $id)
    {
        $query = PO::orderBy('po_id', 'DESC')
            ->where('po_is_deleted', 'N')
            ->where('po_pi_id', $id)->get();

        return view("frontend.logistic.LoadPo", compact("query"));
    }

    public function LoadOther(Request $request, $id)
    {
        $query = PO::join('vendor_quotes AS q', 'q.vquote_id', 'purchase_orders.po_qid')
            ->join('users AS u', 'q.vquote_uid', 'u.user_id')
            ->join('vendors AS ven', 'q.vquote_uid', 'ven.vendor_uid')->find($id);
        $PIData = PI::find($query->po_pi_id);

        $products   = POProduct::leftjoin('products AS p', 'p.product_id', 'popro_pid')->where('popro_po_id', $id)->get();

        $TotalCBM = 0;
        foreach ($products as $p) {
            $GetQuoteDetail = DB::table('vquote_products')->where('vqpro_pid', $p->popro_pid)->where('vqpro_qid', $query->po_qid)->first();
            $TotalCBM += $p->product_cbm * $GetQuoteDetail->vqpro_qty;
        }

        return view("frontend.logistic.LoadOtherData", compact("query", "PIData", "TotalCBM"));
    }
    public function exportCsv(Request $request)
    {

        $fileName = 'order-track.csv';
        // $tasks = SurveyHistory::with('user','survey')->get()->toArray();
        $records = DB::table('logistics')->leftjoin("users", "logistics.exs_logistics_buyer", "users.user_id")->orderBy('track_status');

        if (!empty($request->SearchByPI)) {
            $records = $records->where("exs_logistics_pi", $request->SearchByPI);
        }

        if (!empty($request->SearchByBuyer)) {
            $records = $records->where("user_name", "like", "%" . $request->SearchByBuyer . "%");
        }

        if (!empty($request->SearchByPO)) {
            $records = $records->where("exs_logistics_po", $request->SearchByPO);
        }

        if (!empty($request->SearchByGHP)) {
            $records = $records->where("GHPOrderNo", "like", "%" . $request->SearchByGHP . "%");
        }

        if (!empty($request->TrackStatus)) {
            $records = $records->where("track_status", $request->TrackStatus);
        }

        $query = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->orderBy('pi_id', 'DESC')
            ->where('pi_is_deleted', 'N');

        $profile = Query::get_profile();

        if ($profile->user_role == "buyer") {
            $query->where('q.quote_uid', $profile->user_id);
        }

        $query = $query->get();

        $PInIDS = array();
        foreach ($query as $RQQR) {
            $PInIDS[] = "GHP-201819-" . sprintf('%03d', $RQQR->pi_id) . ",";
        }


        if ($profile->user_role == "buyer") {
            $records = $records->whereIn("exs_logistics_pi", $PInIDS);
        }

        $lists = $records->get()->toArray();
        // dd($lists);
        $listArr = [];
        $site = DB::table('settings')->first();
        foreach ($lists as $key => $l) {
            // dd($l);
            $GetBuyerName = Umodel::where('user_id', $l->exs_logistics_buyer)->first();
            $GetPIData = PI::where("pi_id", $l->exs_logistics_pi)->first();
            $GetPOData = PO::where("po_id", $l->exs_logistics_po)->first();
            $GetForwarder = DB::table('forwarder')->where('forwarder_id', $l->ForwarderName)->first();
            $ShippingLIne1 = DB::table('shipping_line')->where('shipping_line_id', $l->ShippingLine)->first();
            $ShippingLIne2 = DB::table('shipping_line')->where('shipping_line_id', $l->ShippingLine2)->first();
            // dd($GetBuyerName);
            $Color = "";
            if ($l->track_status == 1) {
                $Color = "alert alert-primary";
            }

            if ($l->track_status == 2) {
                $Color = "alert alert-danger";
            }

            if ($l->track_status == 3) {
                $Color = "alert alert-success";
            }
            // $listArr[] = $l;
            $listArr[$key]['Booking Invoice No'] = !empty($l->exs_logistics_booking_no) ? @$l->exs_logistics_booking_no : '-';
            $listArr[$key]['Buyer Name'] = !empty($l->exs_logistics_buyer) ? $l->exs_logistics_buyer : '-';
            $listArr[$key]['PI Number'] =  !empty($l->exs_logistics_pi) ? $l->exs_logistics_pi : '-';
            $listArr[$key]['PO Number'] =   !empty($l->exs_logistics_po) ? (int)$l->exs_logistics_po + 100 : '-';
            $listArr[$key]['GHP Order No.'] = !empty($l->GHPOrderNo) ? $l->GHPOrderNo : '';
            // $listArr[$key]['dp_date'] = $l['dp_date'] ? date("d-M-Y", strtotime($l['dp_date'])) : '';
            $listArr[$key]['CBM'] = !empty($l->CBM) ? $l->CBM : 0;
            $listArr[$key]['Vendor'] = !empty($l->Vendor) ? $l->Vendor : '';
            $listArr[$key]['Container Size'] = !empty($l->container_size) ? $l->container_size : '';
            $listArr[$key]['Order Date'] = !empty($l->OrderDate) ? date("d-M-Y", strtotime($l->OrderDate)) : '-';
            $listArr[$key]['Agreed ETD'] =  !empty($l->AgreedETD) ? date("d-M-Y", strtotime($l->AgreedETD)) : '';
            $listArr[$key]['Forwarder Name'] = !empty($GetForwarder) ? $GetForwarder->forwarder_company_name : '-';
            $listArr[$key]['Booking Request Received Date'] = !empty($l->BookingRequestReceivedDate) ? date("d-M-Y", strtotime($l->BookingRequestReceivedDate)) : '-';
            $listArr[$key]['P/U Location'] = !empty($l->PULocation) ? $l->PULocation : '-';
            $listArr[$key]['H/O Location'] = !empty($l->HOLocation) ? $l->HOLocation : '-';
            $listArr[$key]['Planned Stuffing Date'] = !empty($l->PlannedStuffingDate) ? date("d-M-Y", strtotime($l->PlannedStuffingDate)) : '-';
            $listArr[$key]['DO Shared With Vendor'] = !empty($l->DOSharedWithVendor) ? date("d-M-Y", strtotime($l->DOSharedWithVendor)) : '-';
            $listArr[$key]['Container Pickup Date'] = !empty($l->ContainerPickupDate) ?  date("d-M-Y", strtotime($l->ContainerPickupDate)) : '-';
            $listArr[$key]['Container Nr.'] = !empty($l->ContainerNr) ? $l->ContainerNr : '-';
            $listArr[$key]['Stuffing Date'] = !empty($l->StuffingDate) ? date("d-M-Y", strtotime($l->StuffingDate)) : '-';
            $listArr[$key]['SI Submission by Shipper'] = $l->SISubmissionDtbyVendor != "" && $l->SISubmissionDtbyVendor != "0000-00-00" ? date("d-M-Y", strtotime($l->SISubmissionDtbyVendor)) : '-';
            $listArr[$key]['Container Dispatch Form ICD'] = $l->Dispatch != "" && $l->Dispatch != "0000-00-00" ? date("d-M-Y", strtotime($l->Dispatch)) : '-';
            $listArr[$key]['Container Gate IN at Port'] = $l->GateIN != "" && $l->GateIN != "0000-00-00" ? date("d-M-Y", strtotime($l->GateIN)) : '-';
            $listArr[$key]['Draft BL shared with Shipper'] = $l->DraftBLsharedwithshipper != "" && $l->DraftBLsharedwithshipper != "0000-00-00" ? date("d-M-Y", strtotime($l->DraftBLsharedwithshipper)) : '-';
            $listArr[$key]['Draft BL approved with Shipper'] = $l->DraftBLapprovedwithshipper != "" && $l->DraftBLapprovedwithshipper != "0000-00-00" ? date("d-M-Y", strtotime($l->DraftBLapprovedwithshipper)) : '-';
            $listArr[$key]['Debit Note shared with Shipper'] = $l->DebitNotesharedwithvendor != "" && $l->DebitNotesharedwithvendor != "0000-00-00" ? date("d-M-Y", strtotime($l->DebitNotesharedwithvendor)) : '-';
            $listArr[$key]['Debit Note Payment Detail received Date'] = $l->DebitNotePaymentDetailreceivedDate != "" && $l->DebitNotePaymentDetailreceivedDate != "0000-00-00" ? date("d-M-Y", strtotime($l->DebitNotePaymentDetailreceivedDate)) : '-';
            $listArr[$key]['Remark for Vendor'] = !empty($l->RemarkforVendor) ? $l->RemarkforVendor : '-';
            $listArr[$key]['Booking Placed Date'] = $l->BookingPlacedDate != "" && $l->BookingPlacedDate != "0000-00-00" ? date("d-M-Y", strtotime($l->BookingPlacedDate)) : '-';
            $listArr[$key]['DO Received Date'] = $l->DOReceivedDate != "" && $l->DOReceivedDate != "0000-00-00" ? date("d-M-Y", strtotime($l->DOReceivedDate)) : '-';
            $listArr[$key]['Booking Nr.'] = !empty($l->BookingNr) ? $l->BookingNr : '-';
            $listArr[$key]['Shipping Line'] = !empty($ShippingLIne1->shipping_line_name) ? $ShippingLIne1->shipping_line_name : '';
            $listArr[$key]['DO Expiry Date'] = $l->DOExpiryDate != "" && $l->DOExpiryDate != "0000-00-00" ? date("d-M-Y", strtotime($l->DOExpiryDate)) : '';
            $listArr[$key]['Planned ETD Date'] = $l->PlannedETDDAte != "" && $l->PlannedETDDAte != "0000-00-00" ? date("d-M-Y", strtotime($l->PlannedETDDAte)) : '';
            $listArr[$key]['Planned ETA Date'] = $l->PlannedETADate != "" && $l->PlannedETADate != "0000-00-00" ? date("d-M-Y", strtotime($l->PlannedETADate)) : '';
            $listArr[$key]['Port of Discharge'] = !empty($l->PortofDischarge) ? $l->PortofDischarge : '';
            $listArr[$key]['2nd Rvsd ETD'] = !empty($l->T2ndRvsdETD) ?  $l->T2ndRvsdETD : '';
            $listArr[$key]['Do Received Date 2'] = $l->DoReceivedDate2 != "" && $l->DoReceivedDate2 != "0000-00-00" ?  date("d-M-Y", strtotime($l->DoReceivedDate2)) : '';
            $listArr[$key]['Booking Nr. 2'] = !empty($l->BookingNr2) ? $l->BookingNr2 : '';
            $listArr[$key]['Shipping Line 2'] = !empty($ShippingLIne2->shipping_line_name) ?  $ShippingLIne2->shipping_line_name : '';
            $listArr[$key]['Planned ETD Date 2'] = $l->PlannedETDDate2 != "" && $l->PlannedETDDate2 != "0000-00-00" ? date("d-M-Y", strtotime($l->PlannedETDDate2)) : '';
            $listArr[$key]['Planned ETA Date 2'] = $l->PlannedETADate2 != "" && $l->PlannedETADate2 != "0000-00-00" ? date("d-M-Y", strtotime($l->PlannedETADate2)) : '';
            $listArr[$key]['SI Share With Forwarder'] = $l->SIShareWithForwarder != "" && $l->SIShareWithForwarder != "0000-00-00" ? date("d-M-Y", strtotime($l->SIShareWithForwarder)) : '';
            $listArr[$key]['Draft BL Received'] = $l->DraftBLReceived != "" && $l->DraftBLReceived != "0000-00-00" ?  date("d-M-Y", strtotime($l->DraftBLReceived)) : '';
            $listArr[$key]['Draft BL Approved to Forwarder'] = $l->DraftBLApprovedbyForwarder != "" && $l->DraftBLApprovedbyForwarder != "0000-00-00" ?  date("d-M-Y", strtotime($l->DraftBLApprovedbyForwarder)) : '';
            $listArr[$key]['Debit Note Revd From Forwarder'] = $l->DebitNoteRevdFromForwarder != "" && $l->DraftBLApprovedbyForwarder != "0000-00-00" ?  date("d-M-Y", strtotime($l->DebitNoteRevdFromForwarder)) : '';
            $listArr[$key]['Debit Note Payment Detail'] = $l->DebitNotePaymentDetail != "" && $l->DebitNotePaymentDetail != "0000-00-00" ?  date("d-M-Y", strtotime($l->DebitNotePaymentDetail)) : '';
            $listArr[$key]['Vessel Sailing Date(ATD)'] = $l->VesselSailingETD != "0000-00-00" ?  date("d-M-Y", strtotime($l->VesselSailingETD)) : '';
            $listArr[$key]['Vessel Sailing ETA'] = !empty($l->VesselSailingETA) ? date("d-M-Y", strtotime($l->VesselSailingETA)) : '';
            $listArr[$key]['Final BL Rcvd Date'] = !empty($l->FinalBLRcvdDate) ?  date("d-M-Y", strtotime($l->FinalBLRcvdDate)) : '';
            $listArr[$key]['BL Surrender request date'] = !empty($l->BLSurrenderrequestdate) ?  date("d-M-Y", strtotime($l->BLSurrenderrequestdate)) : '';
            $listArr[$key]['BL Surrender copy rcvd'] = !empty($l->BLSurrendercopyrcvd) ?  date("d-M-Y", strtotime($l->BLSurrendercopyrcvd)) : '';
            $listArr[$key]['Remark Forwarder'] = !empty($l->RemarkForwarder) ?  $l->RemarkForwarder : '';
            $listArr[$key]['Draft Bl shared with Buyer'] = !empty($l->DraftBlsharedwithLIZ) ?  date("d-M-Y", strtotime($l->DraftBlsharedwithLIZ)) : '';
            $listArr[$key]['Draft Bl Conf Rcvd From Buyer'] = $l->DraftBlConfRcvdFromLIZ != "" && $l->DraftBlConfRcvdFromLIZ != "0000-00-00" ?  date("d-M-Y", strtotime($l->DraftBlConfRcvdFromLIZ)) : '';
            $listArr[$key]['Final Bl Shared with Buyer'] = $l->FinalBlSharedwithLIZ != "" && $l->FinalBlSharedwithLIZ != "0000-00-00" ?  date("d-M-Y", strtotime($l->FinalBlSharedwithLIZ)) : '';
            $listArr[$key]['Payment Received'] = !empty($l->PaymentReceived) ?  date("d-M-Y", strtotime($l->PaymentReceived)) : '';
            $listArr[$key]['BL Surrender Copy Shared With'] = !empty($l->BLSurrenderCopySharedWith) ?  date("d-M-Y", strtotime($l->BLSurrenderCopySharedWith)) : '';
            $listArr[$key]['Remarks for Buyer'] = !empty($l->RemarksforLIZ) ?  $l->RemarksforLIZ : '';
        }
        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("Logistic-Reports" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }
    public  function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }
    public function download_send_headers($filename)
    {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
}
