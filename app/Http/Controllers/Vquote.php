<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\VenqryModel as Vemodel;
use App\Models\VquoteModel as Vqmodel;
use App\Models\VenqproModel as Venqpro;
use App\Models\ProductModel as Product;
use App\Models\Query;
use DB;

class Vquote extends BaseController
{

    public function index(Request $request, $id = null)
    {

        $dbprefix = env('DB_PREFIX');
        $records = DB::table('vendor_quotes AS q')
            ->leftjoin('users AS u', 'q.vquote_uid', 'u.user_id')
            ->leftjoin('vendor_enquiries', 'venq_id', 'q.vquote_enq_id')
            ->leftjoin('currencies AS c', 'q.vquote_currency', 'c.currency_id')
            ->orderBy('q.vquote_id', 'DESC')
            ->whereRaw($dbprefix . 'q.vquote_version = (SELECT MAX(vquote_version) FROM ' . $dbprefix . 'vendor_quotes WHERE `vquote_number` = ' . $dbprefix . 'q.vquote_number)')
            ->where('q.vquote_is_deleted', 'N');

        if (@$_GET['SearchByQuote'] != "") {
            $records = $records->where("vquote_number", $_GET['SearchByQuote']);
        }

        if (@$_GET['SearchByVname'] != "") {
            $records = $records->where("user_name", "like", "%" . $_GET['SearchByVname'] . "%");
        }

        $records = $records->paginate(15);

        $title  = "View Quote";
        $page   = "view_vendor_quotes";
        $data = compact('page', 'title', 'records');
        return view('frontend/layout', $data);
    }

    public function add(Request $request, $enq_id, $id = null)
    {
        $title  = "Quotation for enquiry | GHP Software";
        $page   = "add_vendor_quote";

        $edit = [];
        if (!empty($id)) {
            $edit = Vqmodel::where('vquote_id', $id)->orderBy('vquote_id', 'DESC')->first();
        }

        $profile = Query::get_profile();
        $allproducts = DB::table('products')->where('product_is_deleted', 'N')->get();
        $currencies = DB::table('currencies')->where('currency_is_deleted', 'N')->get();
        $ports      = DB::table('ports')->where('port_is_deleted', 'N')->get();

        $carts = session('vquote_cart');

        $profile = Query::get_profile();
        if ($request->isMethod('post')) {
            $record = $request->input('record');
            // print_r($record); die; 
            $record['vquote_updated_on'] = date('Y-m-d H:i:s', time());
            $record['vquote_enq_id']     = $enq_id;
            $record['vquote_added_by']   = $profile->user_role;
            $record['vquote_uid']        = $profile->user_id;
            if (empty($edit->vquote_id)) {
                $last  = Vqmodel::where('vquote_is_deleted', 'N')->select(DB::raw('MAX(vquote_number) AS max_id'))->first();
                $record['vquote_number']     = $last->max_id + 1;
                $record['vquote_created_on'] = date('Y-m-d H:i:s', time());

                Vqmodel::insert($record);
                $id = DB::getPdo()->lastInsertId();
            } else {
                $last  = Vqmodel::where('vquote_is_deleted', 'N')->where('vquote_id', $id)
                    ->select('vquote_uid', 'vquote_enq_id', 'vquote_enq_id', 'vquote_number', 'vquote_currency', 'vquote_price_term', 'vquote_delivery_port', 'vquote_delivery_days', 'vquote_payment', 'vquote_date', 'vquote_version')
                    ->first()->toArray();

                $record = array_merge($record, $last);

                $record['vquote_version'] = $last['vquote_version'] + 1;
                $record['vquote_number']  = $last['vquote_number'];

                Vqmodel::insert($record);
                $id = DB::getPdo()->lastInsertId();
            }

            DB::table('vquote_products')->where('vqpro_qid', $id)->delete();

            if (!empty($carts)) {
                foreach ($carts as $pid => $data) {
                    extract($data);

                    $arr = [
                        'vqpro_qid'      => $id,
                        'vqpro_pid'      => $pid,
                        'vqpro_price'    => $price,
                        'vqpro_remark'   => @$review,
                        'vqpro_qty'      => $qty
                    ];

                    DB::table('vquote_products')->insert($arr);
                }
            }

            return redirect('vendor-enquiry');

            $check = $request->input('check');
            if (!empty($check)) {
                Vemodel::whereIn('venq_id', $check)->update(['venq_is_deleted' => 'Y']);
                return redirect('vendor-quote');
            }
        }

        session()->forget('vquote_cart');
        $carts = session('vquote_cart');
        if (empty($carts) && empty($edit->vquote_id)) {
            $products = Venqpro::with(['product'])->where('vpro_enqid', $enq_id)->get();

            // DB::enableQueryLog(); // Enable query log
            // dd(DB::getQueryLog()); // Show results of log

            $carts = [];
            foreach ($products as $p) {
                $carts[$p->product->product_id] = [
                    'qty'       => $p->vpro_qty,
                    'price'     => 0,
                    'remark'    => ''
                ];
            }
            session(['vquote_cart' => $carts]);
        } elseif (empty($carts)) {
            $products = DB::table('products AS p')
                ->join('vquote_products AS qp', 'p.product_id', 'qp.vqpro_pid')
                ->where('vqpro_qid', $edit->vquote_id)
                ->get();


            $carts = [];
            foreach ($products as $p) {
                $carts[$p->product_id] = [
                    'qty'   => $p->vqpro_qty,
                    'price' => $p->vqpro_price,
                    'remark' => $p->vqpro_remark
                ];
            }
            session(['vquote_cart' => $carts]);
        }

        $query = Vemodel::join('vendors as v', 'v.vendor_uid', 'vendor_enquiries.venq_uid')->where('venq_is_deleted', 'N');
        if ($profile->user_role == 'vendor') {
            $query->where('v.vendor_uid', $profile->user_id);
        }
        $records = $query->paginate(30);

        $data = compact('page', 'title', 'records', 'currencies', 'allproducts', 'ports', 'carts', 'edit');
        return view('frontend/layout', $data);
    }

    public function revisions($id = null)
    {

        $record     = DB::table('quotes AS q')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->where('quote_id', $id)->first();

        $products   = DB::table('quote_products AS qp')
            ->join('products AS p', 'qp.qpro_pid', 'p.product_id')
            ->where('p.product_is_deleted', 'N')
            ->where('qp.qpro_qid', $id)
            ->get();

        $quotes = Qmodel::where('quote_number', $record->quote_number)->get();

        $proArr = [];
        foreach ($quotes as $q) {
            foreach ($products as $key => $pro) {
                $qprods = DB::table('quote_products')->where('qpro_qid', $q->quote_id)->where('qpro_pid', $pro->qpro_pid)->get();
                foreach ($qprods as $qp) {
                    $proArr[] = $qp;
                }
                $products[$key]->qpros = $proArr;
            }
        }

        // echo '<pre>';
        // print_r( $products );
        // echo '</pre>';

        $title      = "Quotation Revisions";
        $page       = "quote_revisions";
        $data       = compact('page', 'title', 'record', 'products', 'quotes');
        return view('frontend/layout', $data);
    }

    public function single($id = null)
    {

        $record     = DB::table('vendor_quotes AS q')
            ->join('currencies AS c', 'q.vquote_currency', 'c.currency_id')
            ->where('vquote_id', $id)->first();
        $products   = DB::table('vquote_products AS qp')
            ->join('products AS p', 'qp.vqpro_pid', 'p.product_id')
            ->where('p.product_is_deleted', 'N')
            ->where('qp.vqpro_qid', $id)
            ->get();

        $quotes = Vqmodel::where('vquote_number', $record->vquote_number)->get();

        foreach ($quotes as $q) {

            foreach ($products as $key => $pro) {
                $proArr = [];
                $qprods = DB::table('vquote_products')->where('vqpro_qid', $q->vquote_id)->where('vqpro_pid', $pro->vqpro_pid)->get();
                foreach ($qprods as $qp) {
                    $proArr[] = $qp;
                }
                $products[$key]->qpros = $proArr;
            }
        }
        // echo "<pre>";
        // print_r($qprods);
        // echo "</pre>"; die;

        $title      = "Quotation Info";
        $page       = "vendor_single_quote";
        $data       = compact('page', 'title', 'record', 'products', 'quotes');
        return view('frontend/layout', $data);
    }

    public function enquiry_status()
    {

        $dbprefix = env('DB_PREFIX');
        $records = DB::table('vendor_enquiries')
            ->join('quotes', 'quote_id', 'venq_enqid')
            ->where('venq_is_deleted', 'N');

        if (@$_GET['SearchByQuote'] != "") {
            $records = $records->where("venq_enqid", $_GET['SearchByQuote']);
        }
        $records = $records->groupBy('venq_enqid')->paginate(50);

        $title  = "Vendor Enquiry Status";
        $page   = "vendor_enquiry_status";
        $data = compact('page', 'title', 'records');
        return view('frontend/layout', $data);
    }

    public function enquiry_quote($id)
    {

        $dbprefix = env('DB_PREFIX');
        $vendors = DB::table('vendor_enquiries as ve')
            ->join('users AS u', 've.venq_uid', 'u.user_id')
            ->where('ve.venq_is_deleted', 'N')
            ->where('ve.venq_enqid', $id)
            ->get();

        $products = DB::connection('mysql')
            ->table('quote_products AS qp')
            ->join('products AS p', 'qp.qpro_pid', 'p.product_id')
            ->where('qp.qpro_qid', $id)
            ->where('qp.qpro_is_approved', 'Y')
            ->get();

        // echo '<pre>';
        // print_r( $vendors );
        // echo '</pre>'; die;

        $title  = "Vendor Enquiry Status";
        $page   = "vendor_enquiry_revision_status";
        $data = compact('page', 'title', 'vendors', 'products', 'id');
        return view('frontend/layout', $data);
    }
}
