<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use Session;

class Finish extends BaseController
{
    public function index(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $input = $request->input('record');

            if ($request->hasFile('finish_image')) {
                $file           = $request->file('finish_image');
                $name            = 'finish' . date("ymdhisa") . '.' . $file->getClientOriginalExtension();
                $destinationPath = 'FinishImage';
                $file->move($destinationPath, $name);
                $input['finish_image'] = $name;
            }

            if (!empty($input)) {
                if (empty($id)) {

                    $CheckDuplicate = DB::table('finish')
                        ->where("finish_no", $input["finish_no"])
                        ->where("finish_code", $input["finish_code"])
                        ->count();

                    if ($CheckDuplicate > 0) {
                        Session::flash('Danger', 'Finish Number & Code Already Exists');
                        return redirect('finish');
                    }

                    DB::table('finish')->insert($input);
                    $id = DB::getPdo()->lastInsertId();

                    Session::flash('Success', 'Finish Added Successfully');
                } else {
                    DB::table('finish')->where('finish_id', $id)->update($input);

                    Session::flash('Success', 'Finish Updated Successfully');
                }
            }

            $check = $request->input('check');
            if (!empty($check)) {

                DB::table('finish')->whereIn('finish_id', $check)->update(['finish_is_deleted' => 'Y']);

                Session::flash('Success', 'Finish Deleted Successfully');
            }

            return redirect('finish');
        }

        $edit = array();
        $MaterialType = array();
        if (!empty($id)) {
            $edit = DB::table('finish')->where('finish_id', $id)->first();
            $MaterialType = DB::table('material_type')->where("material_type_mat_id", $edit->material)->where('is_deleted', 'N')->get();
        }

        $records = DB::table('finish')->leftjoin("material", "material.material_id", "finish.material")->leftjoin("material_type", "material_type.material_type_id", "finish.material_type")->where('finish_is_deleted', 'N');
        if (!empty($request->finish_title)) {
            $records = $records->where('finish_title', "like", "%" . $request->finish_title . "%");
        }
        $records = $records->paginate(10);

        $AllMaterial = DB::table('material')->where('is_deleted', 'N')->get();


        $title     = "Finish | GHP Software";
        $page     = "finish";
        $data     = compact('page', 'title', 'records', 'edit', 'AllMaterial', "MaterialType");
        return view('frontend/layout', $data);
    }
    public function finishExportCsv(Request $request)
    {
        $fileName = 'finish.csv';
        // $tasks = SurveyHistory::with('user','survey')->get()->toArray();
        $records = DB::table('finish')->leftjoin("material", "material.material_id", "finish.material")->leftjoin("material_type", "material_type.material_type_id", "finish.material_type")->where('finish_is_deleted', 'N');
        if (!empty($request->finish_title)) {
            $records = $records->where('finish_title', "like", "%" . $request->finish_title . "%");
        }
        $lists = $records->get()->toArray();
        $listArr = [];
        foreach ($lists as $key => $l) {
            $listArr[$key]['Finish title'] = !empty($l->finish_title) ? $l->finish_title : '';
            $listArr[$key]['Finish code'] = !empty($l->finish_code) ? $l->finish_code : '';
            $listArr[$key]['Finish no'] = !empty($l->finish_no) ? $l->finish_no : '';
            $listArr[$key]['Finish process'] = !empty($l->finish_process) ? $l->finish_process : '';
            $listArr[$key]['Materail name'] = !empty($l->material_name) ? $l->material_name : '';
            $listArr[$key]['Material Type Name'] = !empty($l->material_type_name) ? $l->material_type_name : '';
        }
        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("finishList" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }


    public  function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }
    public function download_send_headers($filename)
    {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
}
