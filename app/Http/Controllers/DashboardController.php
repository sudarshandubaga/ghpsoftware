<?php

namespace App\Http\Controllers;

use App\Models\Logistics as ModelLogistics;
use App\Models\PIModel;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\UserModel as Umodel;
use App\Models\BuyerFiles;
use App\Models\Query;
use Illuminate\Support\Facades\DB;

class DashboardController extends BaseController
{
    public function index()
    {
        $title = "Dashboard";
        $page = "dashboard";

        $records1 = DB::table('logistics')->get();
        foreach ($records1 as $RCD) {
            $records = ModelLogistics::find($RCD->exs_logistics_id);



            //if($RCD->StuffingDate == "" || ($RCD->StuffingDate >= date("Y-m-d"))){
            $records->track_status = 4;
            //}

            if ($RCD->StuffingDate != "" && $RCD->StuffingDate < date("Y-m-d")) {
                $records->track_status = 1;
            }
            if ($RCD->VesselSailingETD == "" || ($RCD->StuffingDate < date("Y-m-d") && $RCD->VesselSailingETD >= date("Y-m-d"))) {
                $records->track_status = 2;
            }
            if ($RCD->BLSurrenderCopySharedWith != "" && ($RCD->VesselSailingETD < date("Y-m-d") && $RCD->BLSurrenderCopySharedWith <= date("Y-m-d"))) {
                $records->track_status = 3;
            }

            $records->save();
        }

        $profile = Query::get_profile();

        $query = PIModel::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->orderBy('pi_id', 'DESC')
            ->where('pi_is_deleted', 'N');

        if (@$profile->user_role == "buyer") {
            $query->where('q.quote_uid', $profile->user_id);
        }

        $PIRecord = $query->get();

        $data = compact('page', 'title', "PIRecord");
        return view('frontend/layout', $data);
    }

    public function Samplesheet()
    {
        $title = "Dashboard";
        $page = "samplesheet";

        $GetBuyer = Umodel::where("user_role", "buyer")->get();

        $profile = Query::get_profile();

        if ($profile->user_role == "buyer") {
            $GetFiles = BuyerFiles::leftjoin("users", "users.user_id", "files.files_buyer")->where("files_buyer", $profile->user_id)->get();
        } else {
            $GetFiles = BuyerFiles::leftjoin("users", "users.user_id", "files.files_buyer")->get();
        }

        $data = compact('page', 'title', 'GetBuyer', "GetFiles");
        return view('frontend/layout', $data);
    }

    public function DeleteSheet(Request $request)
    {
        BuyerFiles::find($request->ID)->delete();
        return redirect("Samplesheet");
    }

    public function SaveFiles(Request $request)
    {
        if ($request->hasFile('PDFFile')) {
            $file = $request->file('PDFFile');
            $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
            $destinationPath = 'files/BuyerFiles';
            $file->move($destinationPath, $name);

            $Obj = new BuyerFiles();
            $Obj->files_buyer = $request->BuyerName;
            $Obj->files_title = $request->Title;
            $Obj->files_remark = $request->remark . "";
            $Obj->planetd = $request->planetd;
            $Obj->url = $request->url;
            $Obj->keywords = $request->keywords;
            $Obj->requested = $request->requested;
            $Obj->files_file = $name;
            $Obj->save();

            return redirect("Samplesheet");
        }
    }
}
