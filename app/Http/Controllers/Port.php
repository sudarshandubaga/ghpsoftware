<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use Session;

class Port extends BaseController {
    public function index(Request $request, $id = null) {
    	if($request->isMethod('post')) {
    		$input = $request->input('record');
    		if(!empty($input)) {
	    		if(empty($id)) {
	    		    
	    		    $CheckDuplicate = DB::table('ports')
                                                    ->where("port_country", $input["port_country"])
                                                    ->where("port_name", $input["port_name"])
                                                    ->where("port_short_name", $input["port_short_name"])
                                                    ->count();
                    
                    if($CheckDuplicate > 0){
                        Session::flash('Danger', 'Port Name & Code Already Exists');
                        return redirect('port');
                    }
                    
		    		DB::table('ports')->insert($input);
		    		$id = DB::getPdo()->lastInsertId();
		    		
		    		Session::flash('Success', 'Port Added Successfully');
		    		
		    	} else {
		    		DB::table('ports')->where('port_id', $id)->update($input);
		    		Session::flash('Success', 'Port Updated Successfully');
		    	}
		    }

		    $check = $request->input('check');
		    if(!empty($check)) {
		        Session::flash('Success', 'Port Deleted Successfully');
		    	DB::table('ports')->whereIn('port_id', $check)->update(['port_is_deleted' => 'Y']);
		    }
		    
		    return redirect('port');
    	}

    	$records = DB::table('ports AS p')
    				->join('countries AS c', 'p.port_country', 'c.country_id')
    				->where('port_is_deleted', 'N');
					if(!empty($request->port_name)){
                        $records->where('port_name' , "like", "%".$request->port_name."%");
                    } 
    				$records = $records->paginate(10);

    	$edit = array();
    	if(!empty($id)) {
    		$edit = DB::table('ports')->where('port_id', $id)->first();
    	}

    	$countries = DB::table('countries')->get();

        $title 	= "Port | GHP-Software";
        $page 	= "port";
        $data 	= compact('page', 'title', 'countries', 'records', 'edit');
        return view('frontend/layout', $data);
    }
	public function portexportCsv(Request $request) {
        $fileName = 'port.csv';       
        // $tasks = SurveyHistory::with('user','survey')->get()->toArray();
        $records = DB::table('ports AS p')
    				->join('countries AS c', 'p.port_country', 'c.country_id')
    				->where('port_is_deleted', 'N');
		if(!empty($request->port_name)){
			$records->where('port_name' , "like", "%".$request->port_name."%");
		} 
        $lists = $records->get()->toArray();
        $listArr = [];
        
        foreach($lists as $key => $l){
            $listArr[$key]['Country Name'] = !empty($l->country_name) ? $l->country_name : '';
            $listArr[$key]['Port Name'] = @$l->port_name;
            $listArr[$key]['Port Short Name'] = @$l->port_short_name;
           
            
        }
        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("portList" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }


    public  function array2csv(array &$array)
    {
       if (count($array) == 0) {
         return null;
       }
       ob_start();
       $df = fopen("php://output", 'w');
       fputcsv($df, array_keys(reset($array)));
       foreach ($array as $row) {
          fputcsv($df, $row);
       }
       fclose($df);
       return ob_get_clean();
    }
    public function download_send_headers($filename) {
    // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
}

