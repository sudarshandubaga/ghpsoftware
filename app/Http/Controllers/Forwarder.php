<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use Session;

class Forwarder extends BaseController
{
    public function index(Request $request, $id = null) {
    	if($request->isMethod('post')) {
    		$input = $request->input('record');
    		if(!empty($input)) {
	    		if(empty($id)) {
	    		    
	    		    $CheckDuplicate = DB::table('forwarder')
                                                    ->where("forwarder_company_name", $input["forwarder_company_name"])
                                                    ->count();
                    
                    if($CheckDuplicate > 0){
                        Session::flash('Danger', 'Forward Name Already Exists');
                        return redirect('forwarder');
                    }
                    
		    		DB::table('forwarder')->insert($input);
		    		$id = DB::getPdo()->lastInsertId();
		    		
		    		Session::flash('Success', 'Record Added Successfully');
		    		
		    	} else {
		    		DB::table('forwarder')->where('forwarder_id', $id)->update($input);
		    		
		    		Session::flash('Success', 'Record Updated Successfully');
		    	}
		    }

		    $check = $request->input('check');
		    if(!empty($check)) {
		    	DB::table('forwarder')->whereIn('forwarder_id', $check)->update(['is_deleted' => 'Y']);
		    	Session::flash('Success', 'Record Deleted Successfully');
		    }

	    	return redirect('forwarder');
    	}

    	$edit = array();
    	if(!empty($id)) {
    		$edit = DB::table('forwarder')->where('forwarder_id', $id)->first();
    	}

    	$records = DB::table('forwarder')->where('is_deleted', 'N');
		if(!empty($request->forwarder_company_name)){
			$records->where('forwarder_company_name' , "like", "%".$request->forwarder_company_name."%");
		}
		$records = $records->paginate(10);

        $title 	= "Forwarder | GHP Software";
        $page 	= "forwarder";
        $data 	= compact('page', 'title', 'records', 'edit');
        return view('frontend/layout', $data);
    }

	public function exportCsv(Request $request) {
        $fileName = 'forwarder.csv';       
        // $tasks = SurveyHistory::with('user','survey')->get()->toArray();
        $records = DB::table('forwarder')->where('is_deleted', 'N');
		if(!empty($request->forwarder_company_name)){
			$records->where('forwarder_company_name' , "like", "%".$request->forwarder_company_name."%");
		}
		$lists = $records->get()->toArray();
        $listArr = [];
        
        foreach($lists as $key => $l){
            $listArr[$key]['Company Name'] = !empty($l->forwarder_company_name) ? $l->forwarder_company_name : '';
            $listArr[$key]['Phone No.'] = @$l->forwarder_phone_no;
            $listArr[$key]['forwarder_contact_name1'] = @$l->forwarder_contact_name1;          
            $listArr[$key]['forwarder_contact_mobile1'] = @$l->forwarder_contact_mobile1;          
            $listArr[$key]['forwarder_contact_email1'] = @$l->forwarder_contact_email1;          
            $listArr[$key]['forwarder_contact_name2'] = @$l->forwarder_contact_name2;          
            $listArr[$key]['forwarder_contact_mobile2'] = @$l->forwarder_contact_mobile2;          
            $listArr[$key]['forwarder_contact_email2'] = @$l->forwarder_contact_email2;          
            $listArr[$key]['forwarder_contact_name3'] = @$l->forwarder_contact_name3;          
            $listArr[$key]['forwarder_contact_mobile3'] = @$l->forwarder_contact_mobile3;          
            $listArr[$key]['forwarder_contact_email3'] = @$l->forwarder_contact_email3;          
            $listArr[$key]['forwarder_contact_name4'] = @$l->forwarder_contact_name4;          
            $listArr[$key]['forwarder_contact_mobile4'] = @$l->forwarder_contact_mobile4;          
            $listArr[$key]['forwarder_contact_email4'] = @$l->forwarder_contact_email4;          
            
        }
        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("forwarderList" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }


    public  function array2csv(array &$array)
    {
       if (count($array) == 0) {
         return null;
       }
       ob_start();
       $df = fopen("php://output", 'w');
       fputcsv($df, array_keys(reset($array)));
       foreach ($array as $row) {
          fputcsv($df, $row);
       }
       fclose($df);
       return ob_get_clean();
    }
    public function download_send_headers($filename) {
    // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
}

