<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Models\QuoteModel as Quote;
use App\Models\UserModel as Umodel;
use App\Models\PIModel as PI;
use App\Models\POModel as PO;
use App\Models\POProduct;

use DB;

class QC extends BaseController
{

    public function Schedule()
    {
        $title     = "QC Schedule | GHP Software";
        $page     = "qc_schedule";

        $data = compact('page', 'title');
        return view('frontend/layout', $data);
    }

    public function Inspectoin()
    {
        $title     = "QC | GHP Software";
        $page     = "qc_inspection";

        $data = compact('page', 'title');
        return view('frontend/layout', $data);
    }

    public function Checklist()
    {
        $title     = "QC | GHP Software";
        $page     = "qc_checklist";

        $data = compact('page', 'title');
        return view('frontend/layout', $data);
    }

    public function Image()
    {
        $title     = "QC | GHP Software";
        $page     = "qc_image";

        $data = compact('page', 'title');
        return view('frontend/layout', $data);
    }

    public function Final()
    {
        $title     = "QC | GHP Software";
        $page     = "qc_final";

        $data = compact('page', 'title');
        return view('frontend/layout', $data);
    }

    public function New(Request $request)
    {
        $title     = "New Schedule | GHP Software";
        $page     = "new_schedule";

        $records = array();
        if ($request->SearchPONumber != "") {
            $dbprefix = env('DB_PREFIX');

            $records = PO::where("po_id", $request->SearchPONumber)->orderBy('po_id', 'DESC')->where('po_is_deleted', 'N')->get();
        }

        $data = compact('page', 'title', 'records');
        return view('frontend/layout', $data);
    }
}
