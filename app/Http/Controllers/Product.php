<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Query;
use App\Models\UpdateLog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Session\Session;
use App\Models\UserModel as Umodel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Product extends BaseController
{

    public function Image()
    {
        $AB = DB::connection('mysql2')->table('products')->get();
        foreach ($AB as $A) {
            $edit = DB::table('products')->where('product_id', $A->product_id)->first();

            if ($edit->product_image != "") {
                $URL = "https://software.ghp-sg.com/imgs/products/" . $edit->product_image;
                DB::connection('mysql2')->table('products')->where('product_id', $A->product_id)->update(array('product_image' => $URL));
            }

            if ($edit->product_image1 != "") {
                $URL = "https://software.ghp-sg.com/imgs/products/" . $edit->product_image1;
                DB::connection('mysql2')->table('products')->where('product_id', $A->product_id)->update(array('product_image1' => $URL));
            }

            if ($edit->product_image2 != "") {
                $URL = "https://software.ghp-sg.com/imgs/products/" . $edit->product_image2;
                DB::connection('mysql2')->table('products')->where('product_id', $A->product_id)->update(array('product_image2' => $URL));
            }

            if ($edit->product_image3 != "") {
                $URL = "https://software.ghp-sg.com/imgs/products/" . $edit->product_image3;
                DB::connection('mysql2')->table('products')->where('product_id', $A->product_id)->update(array('product_image3' => $URL));
            }

            if ($edit->product_image4 != "") {
                $URL = "https://software.ghp-sg.com/imgs/products/" . $edit->product_image4;
                DB::connection('mysql2')->table('products')->where('product_id', $A->product_id)->update(array('product_image4' => $URL));
            }

            if ($edit->product_image5 != "") {
                $URL = "https://software.ghp-sg.com/imgs/products/" . $edit->product_image5;
                DB::connection('mysql2')->table('products')->where('product_id', $A->product_id)->update(array('product_image5' => $URL));
            }
        }
    }

    public function RemoveImage(Request $request, $id, $Index)
    {
        $edit = DB::table('products')->where('product_id', $id)->first();
        if ($Index == 1) {
            DB::table('products')->where('product_id', $id)->update(array('product_image1' => ""));
            DB::connection('mysql2')->table('products')->where('product_id', $id)->update(array('product_image1' => ""));
        }
        if ($Index == 2) {
            DB::table('products')->where('product_id', $id)->update(array('product_image2' => ""));
            DB::connection('mysql2')->table('products')->where('product_id', $id)->update(array('product_image2' => ""));
        }
        if ($Index == 3) {
            DB::table('products')->where('product_id', $id)->update(array('product_image3' => ""));
            DB::connection('mysql2')->table('products')->where('product_id', $id)->update(array('product_image3' => ""));
        }
        if ($Index == 4) {
            DB::table('products')->where('product_id', $id)->update(array('product_image4' => ""));
            DB::connection('mysql2')->table('products')->where('product_id', $id)->update(array('product_image4' => ""));
        }
        if ($Index == 5) {
            DB::table('products')->where('product_id', $id)->update(array('product_image5' => ""));
            DB::connection('mysql2')->table('products')->where('product_id', $id)->update(array('product_image5' => ""));
        }
        echo "Image Deleted";
    }

    public function add(Request $request, $id = null)
    {

        $q          = new Query;
        $title         = "Add Product | GHP Software";

        $edit          = $web_edit = $gross_weight_kg = $gross_weight_lbs = $pkg_arr = $hw_arr = $pkg_ids = $hw_names = array();

        if (!empty($id)) {
            $edit     = DB::table('products')->where('product_id', $id)->first();
            $edit->buyer_details = !empty($edit->product_buyers) ? json_decode($edit->product_buyers) : [];

            // dd($edit);

            $web_edit = DB::connection('mysql2')->table('products')->where('product_id', $id)->first();
            $gross_weight_kg     = !empty($edit->product_gross_weight_kg) ? unserialize(html_entity_decode($edit->product_gross_weight_kg)) : array();
            $gross_weight_lbs     = !empty($edit->product_gross_weight_lbs) ? unserialize(html_entity_decode($edit->product_gross_weight_lbs)) : array();

            $packings     = DB::table('pro_packing')->where('ppkg_pid', $id)->get();
            $hardwares     = DB::table('pro_hardware')->where('phw_pid', $id)->get();

            foreach ($packings as $pkg) {
                $pkg_ids[]                     = $pkg->ppkg_pkg_id;
                $pkg_arr[$pkg->ppkg_pkg_id] = $pkg->ppkg_value;
            }

            foreach ($hardwares as $hw) {
                $hw_names[]                   = $hw->phw_hw_name;
                $hw_arr[$hw->phw_hw_name]     = $hw->phw_value;
            }

            $product_no = $edit->product_no;
        } else {
            $highest_no = DB::table('products')->max('product_no');
            $product_no = $highest_no + 1;
        }

        $categories = $subcategories = $subcategories2 = [];
        $categories = DB::connection('mysql2')->table('categories')->where('category_parent', 0)->get();
        $ranges     = DB::connection('mysql2')->table('ranges')->where('range_is_deleted', 'N')->get();
        if (!empty($web_edit->product_category)) {
            $subcategories  = DB::connection('mysql2')->table('categories')->where('category_parent', $web_edit->product_category)->get();
        }
        if (!empty($web_edit->product_subcategory)) {
            $subcategories2 = DB::connection('mysql2')->table('categories')->where('category_parent', $web_edit->product_subcategory)->get();
        }

        if ($request->isMethod('post')) {
            $validationArr = [
                'product_name' => 'required',
                'parent_product' => 'required',
                'product_range' => 'required',
                'product_country' => 'required',
                // 'product_subcategory' => 'required',
                'product_code' => 'required',
                'product_search_keywords' => 'required',
                'product_l_cm' => 'required',
                'product_w_cm' => 'required',
                'product_h_cm' => 'required',
                'product_mrp' => 'required',
                'material1' => 'required',
                'materialtype1' => 'required',
                'finish1' => 'required',
                //'product_image' => !$id ? 'required' : ''
            ];

            if (!empty($id)) {
                $validationArr = [
                    'product_search_keywords' => 'required',
                    'product_l_cm' => 'required',
                    'product_w_cm' => 'required',
                    'product_h_cm' => 'required',
                    'product_mrp' => 'required',
                    'material1' => 'required',
                    'materialtype1' => 'required',
                    'finish1' => 'required',
                    //'product_image' => !$id ? 'required' : ''
                ];
            }

            $validator = Validator::make($request->input('record'), $validationArr);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $input = $request->input('record');

            $input['product_box_l_cm']      = !empty($input['product_mp_carton_l_cm'][0]) ? $input['product_mp_carton_l_cm'][0] : 0;
            $input['product_box_w_cm']      = !empty($input['product_mp_carton_w_cm'][0]) ? $input['product_mp_carton_w_cm'][0] : 0;
            $input['product_box_h_cm']      = !empty($input['product_mp_carton_h_cm'][0]) ? $input['product_mp_carton_h_cm'][0] : 0;

            $input['product_box_l_inch']    = !empty($input['product_mp_carton_l_inch'][0]) ? $input['product_mp_carton_l_inch'][0]   : 0;
            $input['product_box_w_inch']    = !empty($input['product_mp_carton_w_inch'][0]) ? $input['product_mp_carton_w_inch'][0]   : 0;
            $input['product_box_h_inch']    = !empty($input['product_mp_carton_h_inch'][0]) ? $input['product_mp_carton_h_inch'][0]   : 0;

            $input["product_buyers"]         = json_encode($request->buyer);

            $webArr = $request->input('web');

            $webArr2 = [

                'product_w_in'          => $input['product_l_inch'],
                'product_d_in'          => $input['product_w_inch'],
                'product_h_in'          => $input['product_h_inch'],
                'product_w_cm'          => $input['product_l_cm'],
                'product_d_cm'          => $input['product_w_cm'],
                'product_h_cm'          => $input['product_h_cm'],
                'product_carton_l_cm'   => $input['product_mp_carton_l_cm'][0],
                'product_carton_w_cm'   => $input['product_mp_carton_w_cm'][0],
                'product_carton_h_cm'   => $input['product_mp_carton_h_cm'][0],
                'product_carton_l_inch' => $input['product_mp_carton_l_inch'][0],
                'product_carton_w_inch' => $input['product_mp_carton_w_inch'][0],
                'product_carton_h_inch' => $input['product_mp_carton_h_inch'][0],
                'product_cbm'           => $input['product_cbm'],
                'product_mrp'           => $input['product_mrp'],
                'product_search_keywords' => $input['product_search_keywords'],
                'product_pack'          => $input['product_mp'],
                "product_finish"        => $input['finish1'],

            ];

            if (empty($id)) {
                $webArr2['product_name']        = $input['product_name'];
                $webArr2['parent_product']       = $input['parent_product'];
                $webArr2['product_code']         = $input['product_code'];
                $webArr2['product_category']     = $webArr['product_category'];
                $webArr2['product_subcategory']  = $webArr['product_subcategory'];
                $webArr2['product_subcategory2'] = $webArr['product_subcategory2'];
                $webArr2['product_range'] = $input['product_range'];
            }

            if (!empty($webArr2['product_finish'])) {
                $finish = DB::table('finish')->where('finish_id', $webArr2['product_finish'])->first();
                $webArr2['product_finish'] = $finish->finish_title;
            }

            // echo '<pre>'; print_r($webArr2); die;

            if (!empty($input['product_gross_weight_kg'])) {
                $input['product_gross_weight_kg']  = serialize($input['product_gross_weight_kg']);
            }
            if (!empty($input['product_gross_weight_lbs'])) {
                $input['product_gross_weight_lbs'] = serialize($input['product_gross_weight_lbs']);
            }

            if (!empty($input['product_carton_l_cm'])) {
                $input['product_carton_l_cm'] = serialize($input['product_carton_l_cm']);
            }
            if (!empty($input['product_carton_w_cm'])) {
                $input['product_carton_w_cm'] = serialize($input['product_carton_w_cm']);
            }
            if (!empty($input['product_carton_h_cm'])) {
                $input['product_carton_h_cm'] = serialize($input['product_carton_h_cm']);
            }
            if (!empty($input['product_carton_l_inch'])) {
                $input['product_carton_l_inch'] = serialize($input['product_carton_l_inch']);
            }
            if (!empty($input['product_carton_w_inch'])) {
                $input['product_carton_w_inch'] = serialize($input['product_carton_w_inch']);
            }
            if (!empty($input['product_carton_h_inch'])) {
                $input['product_carton_h_inch'] = serialize($input['product_carton_h_inch']);
            }

            if (!empty($input['product_mp_carton_l_cm'])) {
                $input['product_mp_carton_l_cm'] = serialize($input['product_mp_carton_l_cm']);
            }
            if (!empty($input['product_mp_carton_w_cm'])) {
                $input['product_mp_carton_w_cm'] = serialize($input['product_mp_carton_w_cm']);
            }
            if (!empty($input['product_mp_carton_h_cm'])) {
                $input['product_mp_carton_h_cm'] = serialize($input['product_mp_carton_h_cm']);
            }
            if (!empty($input['product_mp_carton_l_inch'])) {
                $input['product_mp_carton_l_inch'] = serialize($input['product_mp_carton_l_inch']);
            }
            if (!empty($input['product_mp_carton_w_inch'])) {
                $input['product_mp_carton_w_inch'] = serialize($input['product_mp_carton_w_inch']);
            }
            if (!empty($input['product_mp_carton_h_inch'])) {
                $input['product_mp_carton_h_inch'] = serialize($input['product_mp_carton_h_inch']);
            }

            if (empty($id)) {
                DB::table('products')->insert($input);
                $id = DB::getPdo()->lastInsertId();
                $slug = $q->create_slug2($webArr2['product_name'], 'products', 'product_slug', 'product_id', $id);

                $webArr2['product_id']      = $id;
                $webArr2['product_slug']    = $slug;
                DB::connection('mysql2')->table('products')->insert($webArr2);
                (new UpdateLog)->store('Product', 'add');
            } else {
                (new UpdateLog)->store('Product', 'edit');
                DB::table('products')->where('product_id', $id)->update($input);
                DB::connection('mysql2')->table('products')->where('product_id', $id)->update($webArr2);
            }

            DB::table('pro_packing')->where('ppkg_pid', $id)->delete();
            $packings = $request->input('packing');
            foreach ($packings as $ppkg_pkg_id => $ppkg_value) {
                if (!empty($ppkg_value)) {
                    $arr = array(
                        'ppkg_pid' => $id,
                        'ppkg_pkg_id' => $ppkg_pkg_id,
                        'ppkg_value' => $ppkg_value
                    );
                    DB::table('pro_packing')->insert($arr);
                }
            }

            DB::table('pro_hardware')->where('phw_pid', $id)->delete();
            $hardwares = $request->input('hardware');
            foreach ($hardwares as $hw_name => $hw_value) {
                if (!empty($hw_value)) {
                    $arr = array(
                        'phw_pid' => $id,
                        'phw_hw_name' => $hw_name,
                        'phw_value' => $hw_value
                    );
                    DB::table('pro_hardware')->insert($arr);
                }
            }

            // PDF UPLOAD
            if ($request->hasFile('product_pdf')) {
                if (!empty($edit->product_pdf) && file_exists(public_path() . '/files/products/' . $edit->product_pdf)) {
                    unlink(public_path() . '/files/products/' . $edit->book_pdf);
                }
                $file           = $request->file('product_pdf');
                $name            = 'pdf1' . $id . '.' . $file->getClientOriginalExtension();
                $destinationPath = 'files/products';
                $file->move($destinationPath, $name);

                if (!empty($edit->product_pdf)) {
                    $name .= "?v=" . uniqid();
                }

                DB::table('products')->where('product_id', $id)->update(array('product_pdf' => $name));

                DB::connection('mysql2')->table('products')->where('product_id', $id)->update(array('product_pdf' => url('files/products/' . $name)));
            }

            if ($request->hasFile('product_pdf2')) {
                if (!empty($edit->product_pdf2) && file_exists(public_path() . '/public/files/products/' . $edit->product_pdf2)) {
                    unlink(public_path() . '/files/products/' . $edit->product_pdf2);
                }
                $file           = $request->file('product_pdf2');
                $name            = 'pdf2' . $id . '.' . $file->getClientOriginalExtension();
                $destinationPath = 'files/products';
                $file->move($destinationPath, $name);

                if (!empty($edit->product_pdf2)) {
                    $name .= "?v=" . uniqid();
                }

                DB::table('products')->where('product_id', $id)->update(array('product_pdf2' => $name));
            }
            // Images
            if ($request->hasFile('product_image')) {
                if (!empty($edit->product_image) && file_exists(public_path() . '/imgs/products/' . $edit->product_image)) {
                    unlink(public_path() . '/imgs/products/' . $edit->product_image);
                }
                $image           = $request->file('product_image');
                $name            = 'IMG0' . $id . '.' . $image->getClientOriginalExtension();
                $destinationPath = 'imgs/products';
                $image->move($destinationPath, $name);

                if (!empty($edit->product_image)) {
                    $name .= "?v=" . uniqid();
                }

                DB::table('products')->where('product_id', $id)->update(array('product_image' => $name));

                DB::connection('mysql2')->table('products')->where('product_id', $id)->update(array('product_image' => url('imgs/products/' . $name)));
            }

            // DB::connection('mysql2')->table('product_images')->where('pimg_pid', $id)->delete();

            if ($request->hasFile('product_image1')) {
                if (!empty($edit->product_image1) && file_exists(public_path() . '/imgs/products/' . $edit->product_image1)) {
                    unlink(public_path() . '/imgs/products/' . $edit->product_image1);
                }
                $image           = $request->file('product_image1');
                $name            = 'IMG1' . $id . '.' . $image->getClientOriginalExtension();
                $destinationPath = 'imgs/products';
                $image->move($destinationPath, $name);

                if (!empty($edit->product_image1)) {
                    $name .= "?v=" . uniqid();
                }

                DB::table('products')->where('product_id', $id)->update(array('product_image1' => $name));

                $arr = [
                    'pimg_pid'      => $id,
                    'pimg_image'    => url('imgs/products/' . $name)
                ];

                DB::connection('mysql2')->table('products')->where('product_id', $id)->update(array('product_image1' => url('imgs/products/' . $name)));
            }
            if ($request->hasFile('product_image2')) {
                if (!empty($edit->product_image2) && file_exists(public_path() . '/imgs/products/' . $edit->product_image2)) {
                    unlink(public_path() . '/imgs/products/' . $edit->product_image2);
                }
                $image           = $request->file('product_image2');
                $name            = 'IMG2' . $id . '.' . $image->getClientOriginalExtension();
                $destinationPath = 'imgs/products';
                $image->move($destinationPath, $name);

                if (!empty($edit->product_image2)) {
                    $name .= "?v=" . uniqid();
                }

                DB::table('products')->where('product_id', $id)->update(array('product_image2' => $name));

                $arr = [
                    'pimg_pid'      => $id,
                    'pimg_image'    => url('imgs/products/' . $name)
                ];

                DB::connection('mysql2')->table('products')->where('product_id', $id)->update(array('product_image2' => url('imgs/products/' . $name)));
            }
            if ($request->hasFile('product_image3')) {
                if (!empty($edit->product_image3) && file_exists(public_path() . '/imgs/products/' . $edit->product_image3)) {
                    unlink(public_path() . '/imgs/products/' . $edit->product_image3);
                }
                $image           = $request->file('product_image3');
                $name            = 'IMG3' . $id . '.' . $image->getClientOriginalExtension();
                $destinationPath = 'imgs/products';
                $image->move($destinationPath, $name);

                if (!empty($edit->product_image3)) {
                    $name .= "?v=" . uniqid();
                }

                DB::table('products')->where('product_id', $id)->update(array('product_image3' => $name));

                $arr = [
                    'pimg_pid'      => $id,
                    'pimg_image'    => url('imgs/products/' . $name)
                ];

                DB::connection('mysql2')->table('products')->where('product_id', $id)->update(array('product_image3' => url('imgs/products/' . $name)));
            }
            if ($request->hasFile('product_image4')) {
                if (!empty($edit->product_image4) && file_exists(public_path() . '/imgs/products/' . $edit->product_image4)) {
                    unlink(public_path() . '/imgs/products/' . $edit->product_image4);
                }
                $image           = $request->file('product_image4');
                $name            = 'IMG4' . $id . '.' . $image->getClientOriginalExtension();
                $destinationPath = 'imgs/products';
                $image->move($destinationPath, $name);

                if (!empty($edit->product_image4)) {
                    $name .= "?v=" . uniqid();
                }

                DB::table('products')->where('product_id', $id)->update(array('product_image4' => $name));

                $arr = [
                    'pimg_pid'      => $id,
                    'pimg_image'    => url('imgs/products/' . $name)
                ];

                DB::connection('mysql2')->table('products')->where('product_id', $id)->update(array('product_image4' => url('imgs/products/' . $name)));
            }
            if ($request->hasFile('product_image5')) {
                if (!empty($edit->product_image5) && file_exists(public_path() . '/imgs/products/' . $edit->product_image5)) {
                    unlink(public_path() . '/imgs/products/' . $edit->product_image5);
                }
                $image           = $request->file('product_image5');
                $name            = 'IMG5' . $id . '.' . $image->getClientOriginalExtension();
                $destinationPath = 'imgs/products';
                $image->move($destinationPath, $name);

                if (!empty($edit->product_image5)) {
                    $name .= "?v=" . uniqid();
                }

                DB::table('products')->where('product_id', $id)->update(array('product_image5' => $name));

                $arr = [
                    'pimg_pid'      => $id,
                    'pimg_image'    => url('imgs/products/' . $name)
                ];

                DB::connection('mysql2')->table('products')->where('product_id', $id)->update(array('product_image5' => url('imgs/products/' . $name)));
            }

            return redirect($request->session()->get('last_url'));
            // return redirect('product');
        } else {
            $request->session()->put('last_url', request()->headers->get('referer'));
        }


        $woods         = DB::table('woods')->get();
        $metals     = DB::table('metals')->get();
        $packings     = DB::table('packing')->get();

        $finishes   = DB::table('finish')->where('finish_is_deleted', 'N')->get();


        $hardware_arrs = array(
            'Fastner',
            'Bolt',
            'Washer',
            'Allen Key',
            'Wrench/Spanner',
            'Screw Driver',
        );

        $MatType1 = array();
        $MatType2 = array();
        $MatType3 = array();
        $MatType4 = array();
        $MatType5 = array();

        $Finish1 = array();
        $Finish2 = array();
        $Finish3 = array();
        $Finish4 = array();
        $Finish5 = array();

        if (!empty($id)) {
            $MatType1 = DB::table('material_type')->where("material_type_mat_id", $edit->material1)->where('is_deleted', 'N')->get();
            $MatType2 = DB::table('material_type')->where("material_type_mat_id", $edit->material2)->where('is_deleted', 'N')->get();
            $MatType3 = DB::table('material_type')->where("material_type_mat_id", $edit->material3)->where('is_deleted', 'N')->get();
            $MatType4 = DB::table('material_type')->where("material_type_mat_id", $edit->material4)->where('is_deleted', 'N')->get();
            $MatType5 = DB::table('material_type')->where("material_type_mat_id", $edit->material5)->where('is_deleted', 'N')->get();

            $Finish1 = DB::table('finish')->where("material_type", $edit->materialtype1)->where('finish_is_deleted', 'N')->get();
            $Finish2 = DB::table('finish')->where("material_type", $edit->materialtype2)->where('finish_is_deleted', 'N')->get();
            $Finish3 = DB::table('finish')->where("material_type", $edit->materialtype3)->where('finish_is_deleted', 'N')->get();
            $Finish4 = DB::table('finish')->where("material_type", $edit->materialtype4)->where('finish_is_deleted', 'N')->get();
            $Finish5 = DB::table('finish')->where("material_type", $edit->materialtype5)->where('finish_is_deleted', 'N')->get();
        }

        $countries = DB::table('countries')->where('country_is_deleted', 'N')->get();
        $ParentProduct = DB::table('products')->where('parent_product', "0")->orderBy("product_name")->get();

        $AllMaterial = DB::table('material')->where('is_deleted', 'N')->get();

        $page = "add-product";
        $data = compact(
            'page',
            'title',
            'edit',
            "ParentProduct",
            'web_edit',
            "AllMaterial",
            'woods',
            'metals',
            'packings',
            'gross_weight_kg',
            'gross_weight_lbs',
            'pkg_arr',
            'hw_arr',
            'pkg_ids',
            'hw_names',
            'hardware_arrs',
            'finishes',
            'categories',
            'subcategories',
            'subcategories2',
            'countries',
            'ranges',
            'product_no',
            'MatType1',
            'MatType2',
            'MatType3',
            'MatType4',
            'MatType5',
            'Finish1',
            'Finish2',
            'Finish3',
            'Finish4',
            'Finish5'
        );
        return view('frontend/layout', $data);
    }

    public function index(Request $request, $id = null)
    {
        $title     = "View Products";
        $page     = "view-product";

        if ($request->isMethod('post')) {
            $input = $request->input('check');

            DB::table('products')->whereIn('product_id', $input)->delete();
            DB::connection('mysql2')->table('products')->whereIn('product_id', $input)->delete();

            return redirect()->back()->with('success', "Selected Records Deletd Sucessfully");
        }

        $records = DB::table('products')->where('product_is_deleted', 'N');

        if (@$_GET['SearchByCode'] != "") {
            $records = $records->where("product_code", "like", "%" . $_GET['SearchByCode'] . "%");
        }

        if (@$_GET['SearchByName'] != "") {
            $records = $records->where("product_name", "like", "%" . $_GET['SearchByName'] . "%");
        }

        if (@$_GET['SearchByText'] != "") {
            $records = $records->where("product_text", "like", "%" . $_GET['SearchByText'] . "%");
        }

        $records = $records->orderBy("product_id", "DESC")->paginate(30);

        $data = compact('page', 'title', 'records');
        return view('frontend/layout', $data);
    }

    public function delete(Request $request, $code)
    {
        DB::table('products')->where("product_id", $code)->delete();
        DB::connection('mysql2')->table('products')->where('product_id', $code)->delete();
        return redirect('product');
        (new UpdateLog)->store('Product', 'delete');
    }

    public function single($code = null)
    {
        $title         = "View-detail | GHP Software";
        $record     = DB::table('products')->where('product_code', $code)->first();

        $profile = Query::get_profile();
        $profile        = Umodel::find($profile->user_id);
        // dd($profile->user_id);
        $packings   = DB::table('pro_packing AS ppkg')
            ->join('packing AS pkg', 'ppkg.ppkg_pkg_id', 'pkg.packing_id')
            ->where('ppkg_pid', $record->product_id)
            ->select('pkg.packing_name', 'ppkg.ppkg_value')
            ->get();
        $products = '';
        if (!empty($record->product_id)) {
            $products = DB::connection('mysql')
                ->table('buyer_products as qp')
                ->where('qp.bpro_pid', $record->product_id)
                ->where('qp.bpro_uid', $profile->user_id)
                ->join('products AS p', 'qp.bpro_pid', 'p.product_id')
                ->first();
        }
        if (!empty($products)) {
            $products->bpro_drawing =  ($products->bpro_drawing) ? "public/files/BuyerFiles/" . $products->bpro_drawing : '';
            $products->bpro_data_file = ($products->bpro_data_file) ? "public/files/BuyerFiles/" . $products->bpro_data_file : '';
            $products->bpro_assembly = ($products->bpro_assembly) ? "public/files/BuyerFiles/" . $products->bpro_assembly : '';
            $products->bpro_shipping_marks = ($products->bpro_shipping_marks) ? "public/files/BuyerFiles/" . $products->bpro_shipping_marks : '';
            $products->bpro_barcode = ($products->bpro_barcode) ? "public/files/BuyerFiles/" . $products->bpro_barcode : '';
            $products->bpro_care_instruction = ($products->bpro_care_instruction) ? "public/files/BuyerFiles/" . $products->bpro_care_instruction : '';
            $products->bpro_logo = ($products->bpro_logo) ? "public/files/BuyerFiles/" . $products->bpro_logo : '';
            $products->bpro_label = ($products->bpro_label) ? "public/files/BuyerFiles/" . $products->bpro_label : '';
            $products->bpro_cartoon_labelling = ($products->bpro_cartoon_labelling) ? "public/files/BuyerFiles/" . $products->bpro_cartoon_labelling : '';
            $products->bpro_other_warning_label_safety_label = ($products->bpro_other_warning_label_safety_label) ? "public/files/BuyerFiles/" . $products->bpro_other_warning_label_safety_label : '';
        }
        // dd($products);

        $hardwares   = DB::table('pro_hardware')
            ->where('phw_pid', $record->product_id)
            ->select('phw_hw_name', 'phw_value')
            ->get();

        $page       = "view-detail";
        $data       = compact('page', 'title', 'record', 'packings', 'hardwares', 'products');
        return view('frontend/layout', $data);
    }

    public function print($code = null)
    {
        $record     = DB::table('products')->where('product_code', $code)->first();

        $packings   = DB::table('pro_packing AS ppkg')
            ->join('packing AS pkg', 'ppkg.ppkg_pkg_id', 'pkg.packing_id')
            ->where('ppkg_pid', $record->product_id)
            ->select('pkg.packing_name', 'ppkg.ppkg_value')
            ->get();

        $hardwares   = DB::table('pro_hardware')
            ->where('phw_pid', $record->product_id)
            ->select('phw_hw_name', 'phw_value')
            ->get();

        $title         = $record->product_name . " | GHP Software";
        $page       = "product-print";
        $data       = compact('page', 'title', 'record', 'packings', 'hardwares');
        return view('frontend/inc/product-print', $data);
    }

    public function CopyProduct($code = null)
    {
        $q          = new Query;
        $MyProduct = DB::table('products')->where("product_id", $code)->first();
        $SiteProduct = DB::connection('mysql2')->table('products')->where('product_id', $code)->first();

        $highest_no = DB::table('products')->max('product_no');
        $product_no = $highest_no + 1;

        unset($MyProduct->product_id);
        $MyProduct->product_no = $product_no;

        unset($SiteProduct->product_id);


        DB::table('products')->insert((array)$MyProduct);
        $id = DB::getPdo()->lastInsertId();

        $slug = $q->create_slug2($MyProduct->product_name, 'products', 'product_slug', 'product_id', $id);
        $SiteProduct->product_slug = $slug;

        DB::connection('mysql2')->table('products')->insert((array)$SiteProduct);

        return redirect('product/add/' . $id);
    }


    public function CopyImages()
    {
        $MyProduct = DB::table('products')->get();
        foreach ($MyProduct as $AMD) {
            $AMD = (array)$AMD;
            $NewArry = array();

            $CheckUpdate = 0;
            if ($AMD['product_image1'] != "") {
                $CheckUpdate = 1;
                $NewArry['product_image1'] = url('imgs/products/' . $AMD['product_image1']);
            }

            if ($AMD['product_image2'] != "") {
                $CheckUpdate = 1;
                $NewArry['product_image2'] = url('imgs/products/' . $AMD['product_image2']);
            }

            if ($AMD['product_image3'] != "") {
                $CheckUpdate = 1;
                $NewArry['product_image3'] = url('imgs/products/' . $AMD['product_image3']);
            }

            if ($AMD['product_image4'] != "") {
                $CheckUpdate = 1;
                $NewArry['product_image4'] = url('imgs/products/' . $AMD['product_image4']);
            }

            if ($AMD['product_image5'] != "") {
                $CheckUpdate = 1;
                $NewArry['product_image5'] = url('imgs/products/' . $AMD['product_image5']);
            }

            if ($CheckUpdate == 1) {
                DB::connection('mysql2')->table('products')->where('product_id', $AMD['product_id'])->update($NewArry);
            }
        }
    }
    public function changeStatus(Request $request, $product_id)
    {
        // dd($product_id);
        $product = DB::table('products')->where("product_id", $product_id)->first();
        // dd($product);
        $result = $content = "";
        if ($product->product_is_visible == 'N') {
            $status = "Y";
            $result = 1;
        } else {
            $status = "N";
            $result = 0;
        }

        DB::table('products')->where("product_id", $product_id)->update(['product_is_visible' => $status]);
        DB::connection('mysql2')->table('products')->where('product_id', $product_id)->update(['product_is_visible' => $status]);

        return response()->json(['result' => $result]);
    }
    public function exportCsv(Request $request)
    {

        $fileName = 'reports.csv';
        // $tasks = SurveyHistory::with('user','survey')->get()->toArray();
        $records = DB::table('products')->where('product_is_deleted', 'N');

        if (@$request->SearchByCode != "") {
            $records = $records->where("product_code", "like", "%" . $request->SearchByCode . "%");
        }

        if (@$request->SearchByName != "") {
            $records = $records->where("product_name", "like", "%" . $request->SearchByName . "%");
        }

        if (@$request->SearchByText != "") {
            $records = $records->where("product_text", "like", "%" . $request->SearchByText . "%");
        }

        $lists = $records->orderBy("product_id", "DESC")->get()->toArray();
        $listArr = [];

        $site = DB::table('settings')->first();
        foreach ($lists as $key => $l) {

            $MatType1   = DB::table('material_type')->where("material_type_id", $l->materialtype1)->first();
            $MatType2   = DB::table('material_type')->where("material_type_id", $l->materialtype2)->first();
            $Mtrl1      = DB::table('material')->where("material_id", $l->material1)->first();
            $Mtrl2      = DB::table('material')->where("material_id", $l->material2)->first();
            $Fnsh1      = DB::table('finish')->where("finish_id", $l->finish1)->first();
            $Fnsh2      = DB::table('finish')->where("finish_id", $l->finish2)->first();


            $listArr[$key]['Product Name'] = !empty($l->product_name) ? $l->product_name : '';
            $listArr[$key]['Product Code'] = !empty($l->product_code) ? $l->product_code : '';
            $listArr[$key]['Product Type'] = !empty($l->product_type) ? $l->product_type . ' Version' : '';
            $listArr[$key]['Product Material'] = @$Mtrl1->material_name . '/' . @$Mtrl2->material_name;
            $listArr[$key]['Material Type'] = @$MatType1->material_type_name . '/' . @$MatType2->material_type_name;
            $listArr[$key]['Finish'] = @$Fnsh1->finish_title . '/' . @$Fnsh2->finish_title;
            $listArr[$key]['Buyer Refrence'] =  @$l->product_text;
            $listArr[$key]['Specification'] =  @$l->product_dim_extra;
            $listArr[$key]['Dimension CMS'] = 'L ' . $l->product_l_cm . ' X W ' . $l->product_w_cm . ' X H ' .  $l->product_h_cm . ' (CMS)';
            // $listArr[$key]['Dimension CMS'] = 'L '. $l->product_l_cm .' X W '. $l->product_w_cm .' X H '.  $l->product_h_cm .' (CMS) \r\n L '. $l->product_l_inch .' X W '. $l->product_w_inch .' X H '.  $l->product_h_inch .' (Inch)';
            $listArr[$key]['Dimension Inch'] = 'L ' . $l->product_l_inch . ' X W ' . $l->product_w_inch . ' X H ' .  $l->product_h_inch . ' (Inch)';

            // $listArr[$key]['Cartoon Dimension'] = 'L '. $l->product_box_l_cm .' X W '. $l->product_box_w_cm .' X H '.  $l->product_box_h_cm .' (CMS) \r\n L '. $l->product_box_l_inch .' X W '. $l->product_box_w_inch .' X H '.  $l->product_box_h_inch .' (Inch)';
            $listArr[$key]['Cartoon Dimension CMS'] = 'L ' . $l->product_box_l_cm . ' X W ' . $l->product_box_w_cm . ' X H ' .  $l->product_box_h_cm . ' (CMS)';
            $listArr[$key]['Cartoon Dimension Inch'] = 'L ' . $l->product_box_l_inch . ' X W ' . $l->product_box_w_inch . ' X H ' .  $l->product_box_h_inch . ' (Inch)';
            $listArr[$key]['Product CBM'] = !empty($l->product_cbm) ? $l->product_cbm : '';
            $listArr[$key]['Product CFT'] = !empty($l->product_cft) ? $l->product_cft : '';
            $listArr[$key]['Status'] = !empty($l->product_is_visible == "Y") ? 'Active' : 'Deactive';
            // dd($listArr);
        }
        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("product_reports" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }
    public  function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }
    public function download_send_headers($filename)
    {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
}
