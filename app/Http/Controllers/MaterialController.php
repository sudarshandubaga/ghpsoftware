<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class MaterialController extends BaseController
{
    public function index(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $input = $request->input('record');
            if (!empty($input)) {
                if (empty($id)) {

                    $CheckDuplicate = DB::table('material')->where("material_name", $input["material_name"])->count();
                    if ($CheckDuplicate > 0) {
                        Session::flash('Danger', 'Material Name Already Exists');
                        return redirect('Material');
                    }

                    DB::table('material')->insert($input);
                    $id = DB::getPdo()->lastInsertId();

                    Session::flash('Success', 'Material Added Successfully');
                } else {
                    DB::table('material')->where('material_id', $id)->update($input);

                    Session::flash('Success', 'Material Updated Successfully');
                }
            }

            $check = $request->input('check');
            if (!empty($check)) {
                DB::table('material')->whereIn('material_id', $check)->update(['is_deleted' => 'Y']);
                Session::flash('Success', 'Material Deleted Successfully');
            }
            return redirect('Material');
        }

        $edit = array();
        if (!empty($id)) {
            $edit = DB::table('material')->where('material_id', $id)->first();
        }


        $records = DB::table('material')->where('is_deleted', 'N');
        if (!empty($request->material_name)) {
            $records = $records->where('material_name', "like", "%" . $request->material_name . "%");
        }
        $records = $records->paginate(10);

        $title     = "Material | GHP Software";
        $page     = "material";
        $data     = compact('page', 'title', 'records', 'edit');
        return view('frontend/layout', $data);
    }

    public function materialexportCsv(Request $request)
    {
        $records = DB::table('material')->where('is_deleted', 'N');
        if (!empty($request->material_name)) {
            $records = $records->where('material_name', "like", "%" . $request->material_name . "%");
        }
        $lists = $records->get()->toArray();
        $listArr = [];

        foreach ($lists as $key => $l) {
            $listArr[$key]['Material Name'] = !empty($l->material_name) ? $l->material_name : '';
        }
        // echo "<pre>"; print_r($listArr); die;
        // dd($listArr);
        $this->download_send_headers("materialList" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();
    }


    public  function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }
    public function download_send_headers($filename)
    {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
}
