<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

use App\Models\Query;
use App\Models\QuoteModel as Quote;
use App\Models\QuoteProduct;
use App\Models\PIModel as PI;
use App\Models\PIProduct;
use App\Models\Bproduct;

class PI_Controller extends BaseController
{
    public function add(Request $req, $quote_id, $id = NULL)
    {
        $quotes   = Quote::where('quote_is_deleted', 'N')->get();
        $products = QuoteProduct::with('product')->where('qpro_qid', $quote_id)->where('qpro_is_approved', 'Y')->get();
        $quote    = Quote::find($quote_id);

        if ($req->isMethod('post')) {
            $input = $req->input('record');

            $input['pi_show_nw']    = !empty($input['pi_show_nw']) ? $input['pi_show_nw'] : 'N';
            $input['pi_show_gw']    = !empty($input['pi_show_gw']) ? $input['pi_show_gw'] : 'N';
            $input['pi_created_on'] = date("Y-m-d H:i:s", time());
            $id    = PI::insertGetId($input);
            $mess  = "A new PI has been created.";

            PIProduct::where('pipro_pi_id', $id)->delete();

            $pi_cart = session('pi_cart');
            foreach ($products as $p) {
                $bpro        = Bproduct::where('bpro_uid', $quote->quote_uid)->where('bpro_pid', $p->qpro_pid)->first();

                $arr = [
                    'pipro_pi_id'       => $id,
                    'pipro_pid'         => $p->qpro_pid,
                    'pipro_qty'         => $p->qpro_qty,
                    'pipro_sku'         => @$bpro->bpro_sku,
                    'pipro_barcode'     => @$bpro->bpro_barcode,
                    'pipro_buyer_desc'  => @$bpro->bpro_description,
                    'pipro_fsc_status'  => @$pi_cart[$p->qpro_pid]['pipro_fsc_status'],
                    'pipro_species'     => @$pi_cart[$p->qpro_pid]['pipro_species'],
                ];
                PIProduct::insert($arr);
            }

            session()->forget('pi_cart');

            return redirect(url('performa-invoice'))->with('success', $mess);
        }


        $title     = "Create Performa Invoice";
        $page   = "add_pi";
        $data   = compact('page', 'title', 'quote_id', 'quotes', 'products', 'quote');
        return view('frontend/layout', $data);
    }

    public function index(Request $request, $id = null)
    {
        $dbprefix = env('DB_PREFIX');

        foreach (PI::get() as $DKS) {
            $RowPINo = sprintf("%s-%03d", 'GHP-201819', $DKS->pi_id);
            $CheckStatus = DB::table('logistics')->where("exs_logistics_pi", "like", "%" . $RowPINo . "%")->count();

            $PISatus = 0;
            $ShipStatus = 0;

            if ($CheckStatus > 0) {
                $CheckStatus = DB::table('logistics')->where("exs_logistics_pi", "like", "%" . $RowPINo . "%")->first();
                if ($CheckStatus->VesselSailingETD < date("Y-m-d")) {
                    $PISatus = 1;
                }

                if ($CheckStatus->track_status == 3) {
                    $ShipStatus = 1;
                }
            }

            $PP = PI::find($DKS->pi_id);
            $PP->pi_status = $PISatus;
            $PP->pi_ship_status = $ShipStatus;
            $PP->save();
        }

        $query = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->orderBy('pi_id', 'DESC')
            ->where('pi_is_deleted', 'N');

        $profile = Query::get_profile();

        if ($profile->user_role == "buyer") {
            $query->where('q.quote_uid', $profile->user_id);
        }

        if (@$_GET['SearchByName'] != "") {
            $query->where('user_name', "like", "%" . $_GET['SearchByName'] . "%");
        }

        if (@$_GET['SearchByOrderRef'] != "") {
            $id = (int)str_replace("GHP-201819-", '', $_GET['SearchByOrderRef']);
            $query->where('pi_order_ref', "like", "%" . $id . "%");
        }

        if (@$_GET['SearchByPI'] != "") {
            $id = (int)str_replace("GHP-201819-", '', $_GET['SearchByPI']);
            $query->where('pi_id', "like", "%" . $id . "%");
        }

        if (@$_GET['SearchByPI'] != "") {
            $id = (int)str_replace("GHP-201819-", '', $_GET['SearchByPI']);
            $query->where('pi_id', "like", "%" . $id . "%");
        }

        if (@$_GET['SearchSttaus'] != "") {
            $query->where('pi_status', $_GET['SearchSttaus']);
        }

        $records = $query->paginate(15);

        $title     = "View PI";
        $page     = "view_pi";
        $data = compact('page', 'title', 'records');
        return view('frontend/layout', $data);
    }

    public function print($id)
    {
        $record     = PI::join('quotes AS q', 'q.quote_id', 'purchase_invoices.pi_qid')
            ->join('users AS u', 'q.quote_uid', 'u.user_id')
            ->join('currencies AS c', 'q.quote_currency', 'c.currency_id')
            ->leftJoin('countries AS con', 'u.user_country', 'con.country_id')
            ->leftJoin('states AS st', 'u.user_state', 'st.state_id')
            ->find($id);

        $products   = PIProduct::join('products AS p', 'p.product_id', 'pipro_pid')->join('quote_products AS qpro', 'pipro_pid', 'qpro_pid')->where('pipro_pi_id', $id)->where('qpro_qid', $record->quote_id)->get();

        $title         = "Performa Invoice | GHP Software";
        $page       = "performa-invoice";
        $data       = compact('page', 'title', 'record', 'products');
        return view('frontend/performa-invoice', $data);
    }

    public function ConfirmPI($ID, $Date, $DP)
    {
        $PI = PI::find($ID);
        $PI->pi_confirm = 1;
        $PI->pi_confirm_date = $Date;
        $PI->dp_date = $DP;
        $PI->save();
    }
}
