<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VenqryModel extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'vendor_enquiries';
    protected   $primaryKey     = 'venq_id';
}
