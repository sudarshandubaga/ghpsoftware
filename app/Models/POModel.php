<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class POModel extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'purchase_orders';
    protected   $primaryKey     = 'po_id';

    public function quote()
    {
        return $this->hasOne('App\Models\VquoteModel', 'vquote_id', 'po_qid');
    }

    public function vender()
    {
        return $this->hasOne('App\Models\Vendor', 'vendor_uid', 'po_vendor');
    }

    public function purchase_invoice()
    {
        return $this->hasOne('App\Models\PIModel', 'pi_id', 'po_pi_id');
    }

    public function po_invoice()
    {
        return $this->hasMany('App\Models\PoInvoice', 'po_invoice_po', 'po_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'po_products', 'popro_po_id', 'popro_pid')->withPivot('popro_price', 'popro_sku', 'popro_barcode', 'popro_buyer_desc', 'popro_qty');
    }
}
