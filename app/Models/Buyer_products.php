<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Models\UserModel as Umodel;
use App\Models\BuyerFiles;
use App\Models\Query;
use App\Models\PIModel as PI;
use App\Models\Logistics;
use App\Models\Bproduct;

class Buyer_products extends BaseController
{
    public function index()
    {
        $title = "Buyer products";
        $page = "add_buyer_product";

        $vendors = Umodel::where('user_role', 'buyer')->where('user_is_deleted', 'N')->get();
        $data = compact('title', 'page', 'vendors');
        return view('frontend/layout', $data);
    }

    public function get_products(Request $request)
    {

        $products = [];

        if (!empty($request->id)) {
            $products = DB::connection('mysql')
                ->table('buyer_products as bp')
                ->where('bp.bpro_uid', $request->id)
                ->join('products AS p', 'bp.bpro_pid', 'p.product_id')
                ->get();
            if (!empty($products)) {
                foreach ($products as $pro) {
                    $image = "imgs/no-image.png";
                    if (!empty($pro->product_image)) {
                        $dir   = "imgs/products/";
                        $pro->product_image = $dir . $pro->product_image;
                    } else {
                        $pro->product_image = $image;
                    }
                }
            }
        }

        return response()->json($products);
    }

    public function upload_buyer_products(Request $request)
    {

        // PDF UPLOAD
        $status = 400;
        if (!empty($request->all())) {
            $status = 200;
            $data = [];
            $Bproduct = Bproduct::where('bpro_uid', $request->user_id)->where('bpro_pid', $request->product_id)->first();

            if (empty($Bproduct)) {

                if ($request->hasFile('product_drawing')) {
                    $file           = $request->file('product_drawing');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath =  'files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $data['bpro_drawing'] = $name;
                    }
                }
                if ($request->hasFile('product_data_file')) {
                    $file           = $request->file('product_data_file');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $data['bpro_data_file'] = $name;
                    }
                }
                if ($request->hasFile('product_assembly')) {
                    $file           = $request->file('product_assembly');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $data['bpro_assembly'] = $name;
                    }
                }
                if ($request->hasFile('product_shipping_marks')) {
                    $file           = $request->file('product_shipping_marks');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $data['bpro_shipping_marks'] = $name;
                    }
                }
                if ($request->hasFile('product_barcode')) {
                    $file           = $request->file('product_barcode');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $data['bpro_barcode_new'] = $name;
                    }
                }
                if ($request->hasFile('product_care_Instruction')) {
                    $file           = $request->file('product_care_Instruction');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $data['bpro_care_instruction'] = $name;
                    }
                }
                if ($request->hasFile('product_logo')) {
                    $file           = $request->file('product_logo');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $data['bpro_logo'] = $name;
                    }
                }
                if ($request->hasFile('product_label')) {
                    $file           = $request->file('product_label');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $data['bpro_label'] = $name;
                    }
                }
                if ($request->hasFile('product_cartoon_labelling')) {
                    $file           = $request->file('product_cartoon_labelling');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $data['bpro_cartoon_labelling'] = $name;
                    }
                }
                if ($request->hasFile('product_other_warning')) {
                    $file           = $request->file('product_other_warning');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $data['bpro_other_warning_label_safety_label'] = $name;
                    }
                }
                $data['bpro_pid'] = $request->product_id;
                $data['bpro_uid'] = $request->user_id;
                DB::table('buyer_products')->insert($data);
            } else {

                if ($request->hasFile('product_drawing')) {
                    $file           = $request->file('product_drawing');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath =  'files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $Bproduct->bpro_drawing = $name;
                    }
                }
                if ($request->hasFile('product_data_file')) {
                    $file           = $request->file('product_data_file');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $Bproduct->bpro_data_file = $name;
                    }
                }
                if ($request->hasFile('product_assembly')) {
                    $file           = $request->file('product_assembly');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $Bproduct->bpro_assembly = $name;
                    }
                }
                if ($request->hasFile('product_shipping_marks')) {
                    $file           = $request->file('product_shipping_marks');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $Bproduct->bpro_shipping_marks = $name;
                    }
                }
                if ($request->hasFile('product_barcode')) {
                    $file           = $request->file('product_barcode');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $Bproduct->bpro_barcode_new = $name;
                    }
                }
                if ($request->hasFile('product_care_Instruction')) {
                    $file           = $request->file('product_care_Instruction');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $Bproduct->bpro_care_instruction = $name;
                    }
                }
                if ($request->hasFile('product_logo')) {
                    $file           = $request->file('product_logo');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $Bproduct->bpro_logo = $name;
                    }
                }
                if ($request->hasFile('product_label')) {
                    $file           = $request->file('product_label');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $Bproduct->bpro_label = $name;
                    }
                }
                if ($request->hasFile('product_cartoon_labelling')) {
                    $file           = $request->file('product_cartoon_labelling');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $Bproduct->bpro_cartoon_labelling = $name;
                    }
                }
                if ($request->hasFile('product_other_warning')) {
                    $file           = $request->file('product_other_warning');
                    $name = date("Ymdhisa") . "." . $file->getClientOriginalExtension();
                    $destinationPath = public_path() . '/files/BuyerFiles';
                    $file->move($destinationPath, $name);
                    if (!empty($file)) {
                        $Bproduct->bpro_other_warning_label_safety_label = $name;
                    }
                }
                $Bproduct->save();
            }
        }
        return response()->json(['data' => $status]);
    }

    public function view_buyer_products()
    {

        $title = "View Buyer products";
        $page = "view_buyer_product";

        $vendors = Umodel::where('user_role', 'buyer')->where('user_is_deleted', 'N')->get();

        $data = compact('title', 'page', 'vendors');

        return view('frontend/layout', $data);
    }


    public function get_buyer_products(Request $request)
    {

        $products = [];

        if (!empty($request->id)) {
            $products = DB::connection('mysql')
                ->table('buyer_products as qp')
                ->where('qp.bpro_uid', $request->id)
                ->join('products AS p', 'qp.bpro_pid', 'p.product_id')
                ->get();

            if (!empty($products)) {
                foreach ($products as $pro) {
                    if (!empty($pro->product_image)) {
                        $dir   = "imgs/products/";
                        $pro->product_image = $dir . $pro->product_image;
                    } else {
                        $pro->product_image = $image;
                    }
                    $image = "imgs/no-image.png";
                    $dir   = "public/files/BuyerFiles/";
                    if (!empty($pro->bpro_drawing))
                        $pro->bpro_drawing = $dir . $pro->bpro_drawing;
                    if (!empty($pro->bpro_data_file))
                        $pro->bpro_data_file = $dir . $pro->bpro_data_file;
                    if (!empty($pro->bpro_assembly))
                        $pro->bpro_assembly = $dir . $pro->bpro_assembly;
                    if (!empty($pro->bpro_shipping_marks))
                        $pro->bpro_shipping_marks = $dir . $pro->bpro_shipping_marks;
                    if (!empty($pro->bpro_care_instruction))
                        $pro->bpro_care_instruction = $dir . $pro->bpro_care_instruction;
                    if (!empty($pro->bpro_logo))
                        $pro->bpro_logo = $dir . $pro->bpro_logo;
                    if (!empty($pro->bpro_label))
                        $pro->bpro_label = $dir . $pro->bpro_label;
                    if (!empty($pro->bpro_cartoon_labelling))
                        $pro->bpro_cartoon_labelling = $dir . $pro->bpro_cartoon_labelling;
                    if (!empty($pro->bpro_other_warning_label_safety_label))
                        $pro->bpro_other_warning_label_safety_label = $dir . $pro->bpro_other_warning_label_safety_label;
                    if (!empty($pro->bpro_barcode_new))
                        $pro->bpro_barcode_new = $dir . $pro->bpro_barcode_new;
                }
            }
        }

        return response()->json($products);
    }
}
