<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuyerFiles extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'files';
    protected   $primaryKey     = 'files_id';
}
