<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UpdateLog extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'update_logs';

    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\Models\UserModel', 'user_id', 'user_id');
    }

    public function store($module = null, $action = null)
    {
        $data['module'] = $module;
        $data['user_id'] = auth()->user()->user_id;
        $name = !empty(auth()->user()->user_name) ? auth()->user()->user_name : auth()->user()->user_login;
        $message = "Last changed on $action $module by $name";
        $data['message'] = $message;
        UpdateLog::create($data);
    }
}
