<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VenqproModel extends Model
{
    // public      $timestamps     = false;
    protected   $table          = 'vendor_enqproducts';
    protected   $primaryKey     = 'vpro_id';

    public function product()
    {
        return $this->hasOne('App\Models\ProductModel', 'product_id', 'vpro_pid');
    }
}
