<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoInvoice extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'po_invoice';
    protected   $primaryKey     = 'po_invoice_id';
    public function vendor()
    {
        return $this->hasMany('App\Models\Vendor', 'vendor_uid', 'po_invoice_vendor_id');
    }
    public function po()
    {
        return $this->hasOne(POModel::class, 'po_id', 'po_invoice_po');
    }
}
