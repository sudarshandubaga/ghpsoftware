<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GHPInvoice extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'ghp_invoice';
    protected   $primaryKey     = 'exs_ghp_invoice_id';
}
