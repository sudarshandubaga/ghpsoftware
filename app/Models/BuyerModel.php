<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuyerModel extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'buyers';
    protected   $primaryKey     = 'buyer_id';

    public function user()
    {
        return $this->hasOne(UserModel::class, 'user_id', 'buyer_uid');
    }

    public function quote()
    {
        return $this->hasOne(QuoteModel::class, 'quote_uid', 'buyer_uid');
    }
}
