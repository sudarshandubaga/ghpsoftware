<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class POSampleModel extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'purchase_orders';
    protected   $primaryKey     = 'po_id';

    public function quote()
    {
        return $this->hasOne('App\Models\VquoteModel', 'vquote_id', 'po_qid');
    }
}
