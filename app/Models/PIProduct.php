<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PIProduct extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'pi_products';
    protected   $primaryKey     = 'pipro_id';

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'product_id', 'qpro_pid');
    }
    public function products()
    {
        return $this->hasOne('App\Models\Product', 'product_id', 'pipro_pid');
    }
    public function ghpInvoice()
    {
        return $this->hasMany(GHPInvoice::class, 'exs_ghp_invoice_pi_id', 'pipro_pi_id');
    }
}
