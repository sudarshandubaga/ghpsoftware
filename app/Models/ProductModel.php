<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    // public      $timestamps     = false;
    protected   $table          = 'products';
    protected   $primaryKey     = 'product_id';

    public function enqpro()
    {
        return $this->hasMany('App\Models\VenqproModel', 'vpro_pid');
    }
}
