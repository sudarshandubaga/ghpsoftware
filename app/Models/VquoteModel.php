<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VquoteModel extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'vendor_quotes';
    protected   $primaryKey     = 'vquote_id';

    public function product()
    {
        return $this->hasOne('App\Models\ProductModel', 'product_id', 'vpro_pid');
    }

    public function user()
    {
        return $this->hasOne(UserModel::class, 'user_id', 'vquote_uid');
    }

    public function vendor()
    {
        return $this->hasOne(Vendor::class, 'vendor_uid', 'vquote_uid');
    }
}
