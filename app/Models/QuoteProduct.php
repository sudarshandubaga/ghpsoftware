<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuoteProduct extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'quote_products';
    protected   $primaryKey     = 'qpro_id';

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'product_id', 'qpro_pid');
    }
}
