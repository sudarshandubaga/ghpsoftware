<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bproduct extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'buyer_products';
    protected   $primaryKey     = 'bpro_id';

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'product_id', 'bpro_pid');
    }
}
