<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuoteModel extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'quotes';
    protected   $primaryKey     = 'quote_id';

    public function buyer()
    {
        return $this->hasOne(BuyerModel::class, 'buyer_uid', 'quote_uid');
    }

    public function purchase_invoice()
    {
        return $this->hasOne(PIModel::class, 'pi_qid', 'quote_id');
    }
}
