<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoBookingInvoiceProduct extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'po_booking_invoice_product';
    protected   $primaryKey     = 'exs_po_invoice_products_id';
}
