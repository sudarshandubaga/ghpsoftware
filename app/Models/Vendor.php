<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'vendors';
    protected   $primaryKey     = 'vendor_id';
}
