<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'products';
    protected   $primaryKey     = 'product_id';

    public function cart()
    {
        return $this->hasOne(Cart::class, 'product_id', 'product_id');
    }
}
