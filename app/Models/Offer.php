<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    public function offer_products()
    {
        return $this->belongsToMany('App\Models\Product', 'offer_products', 'offer_id', 'product_id')->withPivot('qty', 'margin', 'price', 'moq', 'remarks');
    }
}
