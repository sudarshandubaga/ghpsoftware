<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'users';
    protected   $primaryKey     = 'user_id';

    public function vendor()
    {
        return $this->hasOne('App\Models\Vendor', 'vendor_uid', 'user_id');
    }
}
