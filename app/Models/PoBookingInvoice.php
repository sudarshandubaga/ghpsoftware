<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoBookingInvoice extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'po_booking_invoice';
    protected   $primaryKey     = 'po_invoice_id';

    public function vendor()
    {
        return $this->hasOne('App\Models\Vendor', 'vendor_uid', 'po_invoice_vendor_id');
    }
}
