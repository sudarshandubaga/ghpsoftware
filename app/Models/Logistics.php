<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Logistics extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'logistics';
    protected   $primaryKey     = 'exs_logistics_id';
}
