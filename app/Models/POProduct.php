<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class POProduct extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'po_products';
    protected   $primaryKey     = 'popro_id';

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'product_id', 'popro_pid');
    }
}
