<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vqproducts extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'vquote_products';
    protected   $primaryKey     = 'vqpro_id';
}
