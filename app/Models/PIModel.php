<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PIModel extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'purchase_invoices';
    protected   $primaryKey     = 'pi_id';

    protected $guarded = [];

    public function quote()
    {
        return $this->hasOne('App\Models\QuoteModel', 'quote_id', 'pi_qid');
    }
    public function porder()
    {
        return $this->hasMany('App\Models\POModel', 'po_pi_id', 'pi_id');
    }
}
