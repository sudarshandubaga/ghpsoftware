<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vqproducts extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'vquote_products';
    protected   $primaryKey     = 'vqpro_id';

    public function vquote()
    {
        return $this->hasOne(VquoteModel::class, 'vquote_id', 'vqpro_qid');
    }
}
