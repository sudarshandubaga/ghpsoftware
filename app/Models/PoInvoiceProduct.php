<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoInvoiceProduct extends Model
{
    public      $timestamps     = false;
    protected   $table          = 'po_invoice_products';
    protected   $primaryKey     = 'exs_po_invoice_products_id';
}
