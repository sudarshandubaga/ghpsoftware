$(function () {
    var baseurl   = $("#base_url").val();

    // datepicker
    $('.datepicker').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd'
    });
    $('.datepicker_no_past').datepicker({
        changeMonth: true,
        changeYear: true,
        minDate: 0,
        dateFormat: 'yy-mm-dd'
    });
    $('.datepicker_no_future').datepicker({
        changeMonth: true,
        changeYear: true,
        maxDate: 0,
        dateFormat: 'yy-mm-dd'
    });
    // End Datepicker

    // Ajax Request
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Admin - Buyer Quotation Approval
    $('[rel="approve_qpro"]').on('click', function(e) {
        e.preventDefault();
        var btn = $(this);
        if(btn.prop('checked')) {
            swal({
                title: "Are you sure?",
                text: "Once verified, you will not be able to undo record(s)!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((confirm) => {
                if (confirm) {
                    $.ajax({
                        'url'  : $(this).data('url'),
                        'success': function(res) {
                            btn.prop('checked','checked').attr('disabled', 'disabled');
                        }
                    });
                } else {
                    btn.removeAttr('checked');
                }
            });
        }
    });

    // Locations
    $('.country').on('change', function() {
        var target = $(this).data('target');
        $(target).attr('disabled', 'disabled').html( $('<option />').val('').text('Loading') );

        $.ajax({
            url: baseurl+'/ajax/get_states/'+$(this).val(),
            type: 'POST',
            data: {
                'id': $(this).val()
            },
            success: function(res) {
                $(target).removeAttr('disabled').html( $('<option>').val('').text('Select State') );

                $.each(res.data, function(i, row) {
                    $(target).append( $('<option>').val(row.state_id).text(row.state_name+' ('+row.state_short_name+')') );
                });
            }
        });
    });

    $('.cart_form').on('submit', function(e) {
        e.preventDefault();

        var ajax_url = $(this).attr('action');
        $.ajax({
            url: ajax_url,
            type: 'POST',
            data: $(this).serialize(),
            success: function(res) {
                $('#cartResponse').html(res);
                $("#NewProductForm").trigger("reset");
                toastr.success('Item Added in Cart');
            }
        });
    });

    $("#vendor_list").on("submit", function (e) {

        var form = $(this);
        if(form.find(".quote_check:checked").length == 0) {
            e.preventDefault();
            swal("Warning", "Select at least one vendor", "warning");
        }

    });

    $(document).on('click', '.btn-bpro-save', function(e) {
        e.preventDefault();

        var parent  = $(this).closest('tr'),
            bpro_id = parent.find('.check').val(),
            barcode = parent.find('.barcode').val(),
            sku     = parent.find('.sku').val(),
            desc    = parent.find('.desc').val(),
            comm1    = parent.find('.comm1').val(),
            comm2    = parent.find('.comm2').val(),
            comm3    = parent.find('.comm3').val(),
            btn     = $(this);

        btn.attr('disabled', 'disabled').html('Loading');

        $.ajax({
            url: baseurl+'/ajax/save_bproducts',
            type: 'POST',
            data: {
                bpro_id: bpro_id,
                barcode: barcode,
                sku: sku,
                desc: desc,
                comm1: comm1,
                comm2: comm2,
                comm3: comm3
            },
            success: function(res) {
                btn.removeAttr('disabled').html('Save');
                swal("Done!", "Records saved.", "success");
            }
        });
    });

    $(document).on('click', '.btn-picart-update', function(e) {
        e.preventDefault();

        var form = $(this).closest('form'),
            btn  = $(this);

        btn.attr('disabled', 'disabled').html('Loading');
        $.ajax({
            url: baseurl+'/ajax/pi_cart_update',
            type: 'POST',
            data: form.serialize(),
            success: function() {
                btn.removeAttr('disabled').html('Update');
                swal("Done!", "Cart updated.", "success");
                // $("#cartResponse").load(" #cartResponse");
                var price, qty, subtotal, totalCBM, cbm, grandTotal = 0, grandTotalCBM = 0, totalQty = 0;
                $('#cartResponse tbody tr').each(function () {
                    // console.log('table row: ', $(this).find('.pipro_price').val());
                    cbm = $(this).find('.product_cbm').data('val');
                    price = $(this).find('.pipro_price').val();
                    qty = $(this).find('.pipro_qty').val();

                    subtotal = (price * qty);
                    grandTotal += parseFloat(subtotal);

                    totalCBM = parseFloat(cbm * qty);

                    grandTotalCBM += parseFloat(totalCBM);
                    
                    totalQty += parseFloat(qty);

                    $(this).find('.subtotal').html( subtotal );
                    $(this).find('.totalCBM').html( totalCBM );

                    // price = $(this).find('.pipro_price').val();
                });
                
                $('.grandTotal').text(grandTotal);
                $('.grandTotalCBM').text(grandTotalCBM);
                $('.totalQty').text(totalQty);


            }
        });
    });

    $(document).on('click', '#sameAllCDim', function(e) {
        var checked = $(this).prop('checked');

        if(checked) {
            var i = 0, l = 0, w = 0, h = 0;
            $('.cbm_d_row').each(function() { i++;
                if(i == 1) {
                    l = $(this).find(".cbm_l").val();
                    w = $(this).find(".cbm_w").val();
                    h = $(this).find(".cbm_h").val();
                } else {
                    $(this).find(".cbm_l").val(l).trigger('keyup');
                    $(this).find(".cbm_w").val(w).trigger('keyup');
                    $(this).find(".cbm_h").val(h).trigger('keyup');
                }
            });
        }
    });

    $(document).on('click', '#sameAllIPDim', function(e) {
        var checked = $(this).prop('checked');

        if(checked) {
            var i = 0, l = 0, w = 0, h = 0;
            $('.ip_dim_row').each(function() { i++;
                if(i == 1) {
                    l = $(this).find(".l_cm_dim").val();
                    w = $(this).find(".w_cm_dim").val();
                    h = $(this).find(".h_cm_dim").val();
                } else {
                    $(this).find(".l_cm_dim").val(l).trigger('keyup');
                    $(this).find(".w_cm_dim").val(w).trigger('keyup');
                    $(this).find(".h_cm_dim").val(h).trigger('keyup');
                }
            });
        }
    });

    $(document).on('click', '.remove_cart', function(e) {
        e.preventDefault();

        var id = $(this).data('id');
        var url = $(this).data('url');

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover record(s)!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    'url': url,
                    'type': 'POST',
                    'data': {
                        "pid": id
                    },
                    'success': function(res) {
                        $('#cartResponse').html(res);
                    }
                });
            }
        });
    });

    $('input').on('focus', function() {
        $(this).select();
    });

    $(document).on('click', '.btn-cart-update', function() {
        var form = $(this).closest('form');
        var session  = form.data('session');
        var ajax_url = baseurl+'/ajax/update_cart/'+session;

        $.ajax({
            url: ajax_url,
            type: 'POST',
            data: form.serialize(),
            success: function(res) {
                $('#cartResponse').html(res);
            }
        });
    });

    $('#days_dropdown').on('change', function() {
        $('#days_text').val( $(this).val() );
    })

    $(".category").on("change", function() {
        var target = $(this).data("target");

        val = $(this).val();

        console.log(val);

        $(target).attr('disabled', 'disabled').html( $('<option />').val('').text('Loading') );
        $.ajax({
            url: baseurl+"/ajax/get_category/",
            data: {
                id: val
            },
            success: function(res) {
                $(target).removeAttr('disabled').html( $("<option />").text("Select Subcategory").val('') );

                $.each(res.categories, function(i, row) {
                    $(target).append( $("<option data-short-code='"+row.category_code+"' />").text(row.category_name).val(row.category_id) );
                });

            }
        });
    });

    $(".subcategory").on("change", function() {
        var target = $(this).data("target");

        val = $(this).val();
        console.log(val);

        $(target).attr('disabled', 'disabled').html( $('<option />').val('').text('Loading') );
        $.ajax({
            url: baseurl+"/ajax/get_category/",
            data: {
                id: $(this).val()
            },
            success: function(res) {
                $(target).removeAttr('disabled').html( $("<option />").text("Select Subcategory 2").val('') );

                $.each(res.categories, function(i, row) {
                    $(target).append( $("<option data-short-code='"+row.category_code+"' />").text(row.category_name).val(row.category_id) );
                });

            }
        });
    });

    $('a[rel="product_toggle"]').on('click', function(e) {
        e.preventDefault();

        var target = $(this).attr('href');
        $(target).slideToggle();

        $(this).find('i').toggleClass('icon-plus-circle icon-minus-circle text-success text-danger');
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('a[href="#refresh"]').on('click', function(e) {
        e.preventDefault();

        location.reload();
    });

    $('a[href="#save-data"]').on('click', function(e) {
        e.preventDefault();

        $(this).closest('form').trigger('submit');
    });

    $('.hide_show_toggle').on('click', function(e) {
        var sel = $(this).closest('.card').find('.card-body');
        if($(this).prop('checked')) {
            sel.removeClass('d-none');
        } else {
            sel.addClass('d-none');
        }
    });

    $('.hide_show_toggle2').on('click', function(e) {
        var sel = $(this).closest('.col').find('.toggle_body');
        if($(this).prop('checked')) {
            sel.removeClass('d-none');
        } else {
            sel.addClass('d-none');
        }
    });

    // Login form ajax
    $("#loginForm").on("submit", function(e) {
        e.preventDefault();
        var form     = $(this);
        var is_valid = form.is_valid();
        var fmsg     = form.find('.form-msg');
        var action   = form.attr('action');

        if(is_valid) {
            fmsg.addClass('alert alert-info').removeClass('alert-danger alert-success').html('Progressing, please wait...');

            $.ajax({
                url: action,
                type: 'POST',
                data: form.serialize(),
                success: function(res) {
                    if(res.status) {
                        fmsg.removeClass('alert-info').addClass('alert-success').html(res.message);
                        location.href = "";
                        // window.location.href = "";
                    } else {
                        fmsg.removeClass('alert-info').addClass('alert-danger').html(res.message);
                    }
                }
            });
        }
    });

    $(document).on('keyup blur', '#netWgWoodKg', function(e) {
        // 1 kilogram = 2.2046226218 pound (lbs)
        var wg_wood_kg  = $(this).val();
        var wg_wood_lbs = wg_wood_kg != '' && wg_wood_kg > 0 ? wg_wood_kg * 2.2046226218 : 0;
        wg_wood_lbs     = parseFloat( wg_wood_lbs ).toFixed(2);
        $('#netWgWoodLbs').val( wg_wood_lbs );

        calcNetWeight();
    });

    $(document).on('keyup blur', '#netWgWoodLbs', function(e) {
        // 1 kilogram = 2.2046226218 pound (lbs)
        var wg_wood_lbs     = $(this).val();
        var wg_wood_kg      = wg_wood_lbs != '' && wg_wood_lbs > 0 ? wg_wood_lbs * 0.453592 : 0;
        wg_wood_kg          = parseFloat( wg_wood_kg ).toFixed(2);
        $('#netWgWoodKg').val( wg_wood_kg );

        calcNetWeight();
    });

    $(document).on('keyup blur', '#netWgIronKg', function(e) {
        // 1 kilogram = 2.2046226218 pound (lbs)
        var wg_wood_kg  = $(this).val();
        var wg_wood_lbs = wg_wood_kg != '' && wg_wood_kg > 0 ? wg_wood_kg * 2.2046226218 : 0;
        wg_wood_lbs     = parseFloat( wg_wood_lbs ).toFixed(2);
        $('#netWgIronLbs').val( wg_wood_lbs );

        calcNetWeight();
    });

    $(document).on('keyup blur', '#netWgIronLbs', function(e) {
        // 1 kilogram = 2.2046226218 pound (lbs)
        var wg_wood_lbs     = $(this).val();
        var wg_wood_kg      = wg_wood_lbs != '' && wg_wood_lbs > 0 ? wg_wood_lbs * 0.453592 : 0;
        wg_wood_kg          = parseFloat( wg_wood_kg ).toFixed(2);
        $('#netWgIronKg').val( wg_wood_kg );

        calcNetWeight();
    });

    $(document).on('keyup blur', '#netWgOtherKg', function(e) {
        // 1 kilogram = 2.2046226218 pound (lbs)
        var wg_wood_kg  = $(this).val();
        var wg_wood_lbs = wg_wood_kg != '' && wg_wood_kg > 0 ? wg_wood_kg * 2.2046226218 : 0;
        wg_wood_lbs     = parseFloat( wg_wood_lbs ).toFixed(2);
        $('#netWgOtherLbs').val( wg_wood_lbs );

        calcNetWeight();
    });

    $(document).on('keyup blur', '#netWgOtherLbs', function(e) {
        // 1 kilogram = 2.2046226218 pound (lbs)
        var wg_wood_lbs     = $(this).val();
        var wg_wood_kg      = wg_wood_lbs != '' && wg_wood_lbs > 0 ? wg_wood_lbs * 0.453592 : 0;
        wg_wood_kg          = parseFloat( wg_wood_kg ).toFixed(2);
        $('#netWgOtherKg').val( wg_wood_kg );

        calcNetWeight();
    });

    $(document).on('keyup blur', '#productCbm', function(e) {
        var pro_cbm = $(this).val();
        pro_cbm     = pro_cbm != '' ? parseFloat( pro_cbm ) : 0;

        var cont20 = 0, cont40std = 0, cont40hc = 0, pro_cft = 0;
        if(pro_cbm > 0) {
            cont20      = 20 / pro_cbm;
            cont40std   = 55 / pro_cbm;
            cont40hc    = 67 / pro_cbm;
            pro_cft     = pro_cbm * 35.3147;
        }
        $('#20Cont').val( cont20.toFixed(2) );
        $('#40StdCont').val( cont40std.toFixed(2) );
        $('#40HCCont').val( cont40hc.toFixed(2) );
        $('#productCft').val( pro_cft.toFixed(4) );
    });

    $(document).on('click', '.product_type', function(e) {
        var ptype = $(".product_type:checked").val();

        if(ptype == "K/D") {
            $('.upload_pdf').show();
        } else {
            $('.upload_pdf').hide();
        }
    });

    $(document).on('keyup blur', '#proLcm', function(e) {
        var l_cm = $(this).val();
        l_cm = l_cm != '' ? parseFloat( l_cm ) : 0;
        $("#proLinch").val( (l_cm / 2.54).toFixed(2) );

        var box_l_cm = $("#boxLcm").val();

        $("#boxLcm").val( l_cm + 5 ).trigger('keyup');

        calc_cbm();
    });
    $(document).on('keyup blur', '#proWcm', function(e) {
        var w_cm = $(this).val();
        w_cm = w_cm != '' ? parseFloat( w_cm ) : 0;
        $("#proWinch").val( (w_cm / 2.54).toFixed(2) );

        var box_w_cm = $("#boxWcm").val();

        $("#boxWcm").val( w_cm + 5 ).trigger('keyup');

        calc_cbm();
    });
    $(document).on('keyup blur', '#proHcm', function(e) {
        var h_cm = $(this).val();
        h_cm = h_cm != '' ? parseFloat( h_cm ) : 0;
        $("#proHinch").val( (h_cm / 2.54).toFixed(2) );

        var box_h_cm = $("#boxHcm").val();

        $("#boxHcm").val( h_cm + 5 ).trigger('keyup');

        calc_cbm();

    });

    $(document).on('keyup blur', '#boxLcm', function(e) {
        var l_cm = $(this).val();
        l_cm = l_cm != '' ? parseFloat( l_cm ) : 0;
        $("#boxLinch").val( (l_cm / 2.54).toFixed(2) );
    });
    $(document).on('keyup blur', '#boxWcm', function(e) {
        var w_cm = $(this).val();
        w_cm = w_cm != '' ? parseFloat( w_cm ) : 0;
        $("#boxWinch").val( (w_cm / 2.54).toFixed(2) );
    });
    $(document).on('keyup blur', '#boxHcm', function(e) {
        var h_cm = $(this).val();
        h_cm = h_cm != '' ? parseFloat( h_cm ) : 0;
        $("#boxHinch").val( (h_cm / 2.54).toFixed(2) );
    });

    $(document).on('keyup blur', '.l_cm_dim', function(e) {
        var cm      = $(this).val(),
            target  = $(this).closest('.row').closest('.form-group').parent().next().find('.l_inch_dim');
        cm = cm != '' ? parseFloat( cm ) : 0;
        $(target).val( (cm / 2.54).toFixed(2) );
        console.log(target.val());
    });
    $(document).on('keyup blur', '.w_cm_dim', function(e) {
        var cm      = $(this).val(),
            target  = $(this).closest('.row').closest('.form-group').parent().next().find('.w_inch_dim');
        cm = cm != '' ? parseFloat( cm ) : 0;
        $(target).val( (cm / 2.54).toFixed(2) );
    });
    $(document).on('keyup blur', '.h_cm_dim', function(e) {
        var cm      = $(this).val(),
            target  = $(this).closest('.row').closest('.form-group').parent().next().find('.h_inch_dim');
        cm = cm != '' ? parseFloat( cm ) : 0;
        $(target).val( (cm / 2.54).toFixed(2) );
    });

    $(document).on('keyup blur', '.gw_kg', function(e) {
        var w_kg = $(this).val();
        w_kg = w_kg != '' ? parseFloat( w_kg ) : 0;

        $(this).closest('.row').find('.gw_lbs').val( (w_kg * 2.2046226218).toFixed(2) );

        calcGrossWeight();
    });

    $(document).on('keyup blur', '.gw_lbs', function(e) {
        var w_lbs = $(this).val();
        w_lbs = w_lbs != '' ? parseFloat( w_lbs ) : 0;

        $(this).closest('.row').find('.gw_kg').val( (w_lbs / 2.2046226218).toFixed(2) );

        calcGrossWeight();
    });

    $(document).on('click', '.packing_inp', function(e) {
        var checked = $(this).prop('checked'),
            target  = $(this).closest('.form-group').find('input.packing_value');

        if(checked) {
            target.removeAttr('readonly');
        } else {
            target.attr('readonly', 'readonly');
        }
    });

    $(document).on('keypress', '.bootstrap-tagsinput input', function (e) {
        console.log(e.keyCode);
        if (e.keyCode == 13) {
            e.keyCode = 188;
            e.preventDefault();
        }
    });

    $(document).on('click', '.product-thumbnail img', function(e) {
        var src = $(this).attr('src');

        $("#mainImage").attr('src', src);
    });

    $('.taginput').tagsinput({
        confirmKeys: [13, 188]
    });

    $(document).on('click', 'a[href="#gen_code"]', function(e) {
        e.preventDefault();
        var text = generateRandomString(8);

        $(this).closest('.form-group').find('input').val( text );
    });

    $(document).on('keyup blur change', '#product_mp', function() {
        var mp = $(this).val();

        var html = '';

        for(var i = 1; i <= mp; i++) {
            html += '<div>';
            if(i == 1)
                html += '<label class="text-center d-block"><strong>Master Packet (MP) = </strong>'+mp+'</label>';
            else
                html += '<label class="text-center d-block">+</label>';

            html += '<div class="row">';
            html += '<div class="col-sm-6">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<input type="text" name="record[product_gross_weight_kg][]" class="form-control text-center gw_kg" value="" autocomplete="off">';
            html += '<div class="input-group-append">';
            html += '<span class="input-group-text">Kg</span>'
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-sm-6">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<input type="text" name="record[product_gross_weight_lbs][]" class="form-control text-center gw_lbs" autocomplete="off">';
            html += '<div class="input-group-append">';
            html += '<span class="input-group-text">LBS</span>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
        }

        $("#total_gross_weight").html( html );


        html = '';

        for(i = 1; i <= mp; i++) {
            html += '<div class="">';

            html += '<label class="text-center d-block">+</label>';

            html += '<div class="">';
            html += '<div class="form-group">';
            html += '<label>Carton Dimension (In CMS)</label>';

            html += '<div class="row cbm_d_row">';
            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">L</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_mp_carton_l_cm][]" class="form-control text-center l_cm_dim cbm_l" value="0" autocomplete="off">';
            html += '</div>';
            html += '</div>';
            html += '</div>';

            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">W</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_mp_carton_w_cm][]" class="form-control text-center w_cm_dim cbm_w" value="0" autocomplete="off">';
            html += '</div>';
            html += '</div>';
            html += '</div>';


            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">H</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_mp_carton_h_cm][]" class="form-control text-center h_cm_dim cbm_h" value="0" autocomplete="off">';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '<div class="">';
            html += '<div class="form-group">';
            html += '<label>Carton Dimension (In Inch)</label>';

            html += '<div class="row">';
            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">L</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_mp_carton_l_inch][]" class="form-control text-center l_inch_dim" value="0" autocomplete="off" readonly>';
            html += '</div>';
            html += '</div>';
            html += '</div>';

            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">W</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_mp_carton_w_inch][]" class="form-control text-center w_inch_dim" value="0" autocomplete="off" readonly>';
            html += '</div>';
            html += '</div>';
            html += '</div>';

            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">H</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_mp_carton_h_inch][]" class="form-control text-center h_inch_dim" value="0" autocomplete="off" readonly>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
        }

        $("#carton_mp_dimension").html( html );

    });

    $(document).on('keyup blur change', '#product_ip', function() {
        var ip = $(this).val();

        var html = '';

        for(var i = 1; i <= ip; i++) {
            html += '<div>';
            if(i == 1)
                html += '<label class="float-right"><input type="checkbox" id="sameAllIPDim"> Same All Carton Size </label> <label class="d-block"><strong>Inner Packet (IP) = </strong>'+ip+'</label>';
            else
                html += '<label class="text-center d-block">+</label>';

            html += '<div class="">';
            html += '<div class="form-group">';
            html += '<label>Inner Packet Dimension (In CMS)</label>';

            html += '<div class="row ip_dim_row">';
            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">L</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_carton_l_cm][]" class="form-control text-center l_cm_dim" value="0" autocomplete="off">';
            html += '</div>';
            html += '</div>';
            html += '</div>';

            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">W</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_carton_w_cm][]" class="form-control text-center w_cm_dim" value="0" autocomplete="off">';
            html += '</div>';
            html += '</div>';
            html += '</div>';


            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">H</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_carton_h_cm][]" class="form-control text-center h_cm_dim" value="0" autocomplete="off">';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '<div class="">';
            html += '<div class="form-group">';
            html += '<label>Inner Packet Dimension (In Inch)</label>';

            html += '<div class="row">';
            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">L</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_carton_l_inch][]" class="form-control text-center l_inch_dim" value="0" autocomplete="off" readonly>';
            html += '</div>';
            html += '</div>';
            html += '</div>';

            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">W</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_carton_w_inch][]" class="form-control text-center w_inch_dim" value="0" autocomplete="off" readonly>';
            html += '</div>';
            html += '</div>';
            html += '</div>';


            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">H</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_carton_h_inch][]" class="form-control text-center h_inch_dim" value="0" autocomplete="off" readonly>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
        }

        $("#total_ip_dimension").html( html );

    });

    $(document).on('click', '.product_type', function(e) {
        var ptype = $(".product_type:checked").val();

        if(ptype == "K/D") {
            $('.upload_pdf').show();
        } else {
            $('.upload_pdf').hide();
        }
    });

    $(document).on('click', '.packing_inp', function(e) {
        var checked = $(this).prop('checked'),
            target  = $(this).closest('.form-group').find('input.packing_value');

        if(checked) {
            target.removeAttr('readonly');
        } else {
            target.attr('readonly', 'readonly');
        }
    });

    $(".upload_image input[type=file]").change(function() {
        var target = $(this).closest(".upload_image").find("img");
        readURL(this, target);
    });

    $('#product-name').on('change blur', function() {
        var short = $(this).val().substring(0, 3).toUpperCase();
        $('#product-short-name').val( short );
    });

    $('#product-range').on('change blur', function() {
        var short = $(this).val().substring(0, 3).toUpperCase();
        $('#product-short-range').val( short );
    });

    $('#product-country, #product-category, #product-sub-category, #product-number, #proSubCategory2').on('change blur', function() {
        $con    = $('#product-country').val();

        $cat = $('#product-category').find(':selected').data('short-code')
        $scat   = $('#product-sub-category').find(':selected').data('short-code')
        $scat2   = $('#proSubCategory2').find(':selected').data('short-code')
        $pcod   = $('#product-number').val();

        //if($con != '' && $cat != '' && $rag != '' && $scat != '' && $pcod != '' && $fin != '') {
            $('#product-code').val( $con + $cat + $scat + $scat2 + $pcod);
        //}
    });

    $('#quote-sub-category, #quote-finish, #quote-range').on('change', function(e) {
        $cat  = $('#quote-category').val();
        $scat = $('#quote-sub-category').val();
        $fin  = $('#quote-finish').val();
        $rag  = $('#quote-range').val();

        $('#quote-product').html('<option value="">Loading...</option>');

        if($cat != '' && $scat != '' && $rag != '' && $fin != '') {
            $.ajax({
                url: baseurl+'/ajax/get_products',
                type: 'POST',
                data: {
                    cat: $cat,
                    scat: $scat,
                    range: $rag,
                    finish: $fin
                },
                success: function(res) {
                    $('#quote-product').html('<option value="">Select Product</option>');

                    $.each(res.data, function(i, row) {
                        $('#quote-product').append('<option value="'+row.product_id+'">'+row.product_code+' - '+row.product_name+'</option>');
                    })
                }
            });
        }
    });

    $('#product-code')

	.keydown(function (e) {
		var key = e.which || e.charCode || e.keyCode || 0;
		$phone = $(this);

		// Auto-format- do not expose the mask as the user begins to type
		if (key !== 8 && key !== 9) {
			if ($phone.val().length === 3) {
				$phone.val($phone.val() + '-');
			}
			if ($phone.val().length === 10) {
				$phone.val($phone.val() + '-');
			}
		}
        console.log( key );
        if(($phone.val().length == 10 && key === 8) || ($phone.val().length === 15 && key !== 8) ) {
            return false;
        } else {
    		// Allow alpha-numeric (and tab, backspace, delete) keys only
    		return (key == 8 ||
    				key == 9 ||
    				key == 46 ||
    				(key >= 48 && key <= 57) ||
                    (key >= 65 && key <= 90) ||
    				(key >= 96 && key <= 105));
        }
	})

	.bind('focus click', function () {
		$phone = $(this);
        $con   = $('#product-country');

        var val = $phone.val();
        $phone.val('').val(val);

        if($con.val() == '') {
            $phone.val('');
            $con.focus();
        }

	})

	.blur(function () {
		$phone = $(this);

		if ($phone.val() === '(') {
			$phone.val('');
		}
	});

    $(document).on('keyup change', '.cbm_d_row .cbm_l, .cbm_d_row .cbm_h, .cbm_d_row .cbm_w', function() {
        calc_cbm();
    });

    $(".quote_checkall").on("click", function() {
        var form = $(this).closest("form");

        form.find(".quote_check:not([disabled])").prop( "checked", $(this).prop("checked") );
    });

    $('.quote_check').on('click', function() {
        var form = $(this).closest("form");
        var checked = form.find('.quote_check:checked').length == form.find('.quote_check').length;

        form.find('.quote_checkall').prop('checked', checked);
    });

    $("#send_to_vendor").on("submit", function (e) {

        var form = $(this);
        if(form.find(".quote_check:checked").length == 0) {
            e.preventDefault();
            swal("Warning", "Select at least one product", "warning");
        }
        if(form.find(".check:checked").length == 0) {
            e.preventDefault();
            swal("Warning", "Select at least one vendor", "warning");
        }
    });
});

// function calc_cbm() {
//     var l = 0, w = 0, h = 0, cbm = 0, subtotal = 0, i = 0;
//     var string = "";

//     $('.cbm_d_row').each(function(row) { i++;
//         // console.log( $(this).find('.cbm_l').val()+' x '+$(this).find('.cbm_w').val()+' x '+$(this).find('.cbm_h').val() );
//         l = $(this).find('.cbm_l').val();
//         w = $(this).find('.cbm_w').val();
//         h = $(this).find('.cbm_h').val();

//         subtotal = (l * w * h) / 1000000;
//         if(i > 1) {
//             string += " + ";
//         }
//         string  += subtotal;
//         cbm     += parseFloat( subtotal );
//     });

//     cbm = cbm.toFixed(5);
//     //

//     $("#cbmText").html( string );
//     $("#productCbm").val(cbm).trigger('keyup');
// }

function calc_cbm() {
    var l = 0, w = 0, h = 0, cbm = 0, subtotal = 0, i = 0, ratio=1;
    var string = "";

    $('.cbm_d_row').each(function(row) { i++;
        // console.log( $(this).find('.cbm_l').val()+' x '+$(this).find('.cbm_w').val()+' x '+$(this).find('.cbm_h').val() );
        l = $(this).find('.cbm_l').val();
        w = $(this).find('.cbm_w').val();
        h = $(this).find('.cbm_h').val();
        ratio = $('.ratio').val();

        subtotal = (l * w * h) / 1000000;
        if(i > 1) {
            string += " + ";
        }
        string  += subtotal;
        cbm     += parseFloat( subtotal );
    });
    string += " / " + ratio;
    cbm = cbm/ratio;

    console.log('cbm',cbm);
    cbm = cbm.toFixed(5);
    
    //

    $("#cbmText").html( string );
    $("#productCbm").val(cbm).trigger('keyup');
}

function readURL(input, target) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(target).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    } else {
        return;
    }
}


// function readURL(input, target) {

//     if (input.files && input.files[0]) {
//         var reader = new FileReader();
//         reader.onload = function (e) {
//             $(target).attr('src', e.target.result);
//         }
//         reader.readAsDataURL(input.files[0]);
//     } else {
//         return;
//     }
// }

function calcGrossWeight() {
    var totWeightKg = 0, totWeightLbs = 0;
    $(".gw_kg").each(function(){
        totWeightKg     += +$(this).val();
    });
    $(".gw_lbs").each(function(){
        totWeightLbs    += +$(this).val();
    });

    $("#totGrossWeightKg").val( totWeightKg.toFixed(2) );
    $("#totGrossWeightLbs").val( totWeightLbs.toFixed(2) );
}

function calcNetWeight() {
    var wood_kg     = $("#netWgWoodKg").val(),
        wood_lbs    = $("#netWgWoodLbs").val(),
        iron_kg     = $("#netWgIronKg").val(),
        iron_lbs    = $("#netWgIronLbs").val(),
        other_kg    = $("#netWgOtherKg").val(),
        other_lbs   = $("#netWgOtherLbs").val();

    wood_kg     = wood_kg != ''     ? parseFloat(wood_kg)   : 0;
    wood_lbs    = wood_lbs != ''    ? parseFloat(wood_lbs)  : 0;
    iron_kg     = iron_kg != ''     ? parseFloat(iron_kg)   : 0;
    iron_lbs    = iron_lbs != ''    ? parseFloat(iron_lbs)  : 0;
    other_kg     = other_kg != ''   ? parseFloat(other_kg)  : 0;
    other_lbs    = other_lbs != ''  ? parseFloat(other_lbs) : 0;

    $("#netWeightKg").val( wood_kg + iron_kg + other_kg );
    $("#netWeightLbs").val( wood_lbs + iron_lbs + other_lbs );
}

function generateRandomString(length) {
    var text = "";
    var possible = "ABC6DEF4GH1I2J3KL5MN7OP9QRS8TUV1WXYZ";

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

$(".checkall").on("click", function() {
    var form = $(this).closest("form");

    form.find(".check:not([disabled])").prop( "checked", $(this).prop("checked") );
});

$('.check').on('click', function() {
    var form = $(this).closest("form");
    var checked = form.find('.check:checked').length == form.find('.check').length;

    form.find('.checkall').prop('checked', checked);
});

$(".icon-trash-o").on("click", function (e) {
    e.preventDefault();

    var form = $(this).closest("form");
    if(form.find(".check:checked").length > 0) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover record(s)!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                form.trigger("submit");
            }
        });
    } else {
        swal("Warning", "Select at least one record to delete", "warning");
    }

});

